Invitation to submit comments on the draft Commission Regulation on the application of Articles 87 and 88 of the EC Treaty to de minimis aid
(2006/C 137/04)
Interested parties may submit their comments within one month of the date of publication of this draft Regulation to:
European Commission
Directorate-General for Competition
Consultation on de minimis (HT 368)
State aid Registry
B-1049 Brussels
Fax (32-2) 296 12 42
E-mail: stateaidgreffe@ec.europa.eu
The text will also be available on the following website:
http://ec.europa.eu/comm/competition/state_aid/overview/sar.html
Draft Regulation on the application of Articles 87 and 88 of the EC Treaty to de minimis aid
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 994/98 of 7 May 1998 on the application of Articles 92 and 93 of the Treaty establishing the European Community to certain categories of horizontal State aid [1], and in particular Article 2 thereof,
Having published a draft of this Regulation [2],
Having consulted the Advisory Committee on State aid,
Whereas:
(1) Regulation (EC) No 994/98 empowers the Commission to set out in a regulation a threshold under which aid measures are deemed not to meet all the criteria of Article 87(1) of the Treaty and therefore do not fall under the notification procedure provided for in Article 88(3) of the Treaty.
(2) The Commission has applied Articles 87 and 88 of the Treaty and has in particular clarified, in numerous decisions, the notion of aid within the meaning of Article 87(1) of the Treaty. The Commission has also stated its policy with regard to a de minimis ceiling, below which Article 87(1) can be considered not to apply, initially in its notice on the de minimis rule for State aid [3] and subsequently in Commission Regulation (EC) No 69/2001 of 12 January 2001 on the application of Articles 87 and 88 of the EC Treaty to de minimis aid [4]. In the light of the experience gained in applying that Regulation and in order to take account of the evolution of inflation and the gross domestic product in the Community, it appears appropriate to revise some of the conditions laid down in Regulation (EC) No 69/2001 and to replace that Regulation.
(3) In view of the special rules which apply in the sectors of agriculture, fisheries, aquaculture and transport [5], and of the risk that even small amounts of aid could fulfil the criteria of Article 87(1) of the Treaty in those sectors, it is appropriate that this Regulation should not apply to those sectors. Considering the similarities between the processing and marketing of agricultural products, on the one hand, and of non-agricultural products, on the other hand, this Regulation should however apply to the processing and marketing of agricultural products, provided certain conditions are met. The Court of Justice of the European Communities has established that, once the Community has legislated for the establishment of a common organisation of the market in a given sector of agriculture, Member States are under an obligation to refrain from taking any measure which might undermine or create exceptions to it. For this reason, this Regulation should not apply to aid, the amount of which is fixed on the basis of price or quantity of products purchased or put on the market, nor to de minimis support that would be linked to an obligation to share it with primary producers.
(4) This Regulation should not exempt export aid or aid favouring domestic over imported products. In addition, it should not apply to aid financing the establishment and operation of a distribution network in other Member States of the Community. Aid towards the cost of participating in trade fairs, or of studies or consultancy services needed for the launch of a new or existing product on a new market does not normally constitute export aid.
(5) In the light of the Commission's experience, it can be established that aid not exceeding a ceiling of EUR 200000 over any period of three years does not affect trade between Member States and/or does not distort or threaten to distort competition and therefore does not fall under Article 87(1) of the Treaty. The years to take into account for this purpose are the fiscal years as used for fiscal purposes in the Member State concerned. The relevant period of three years should be assessed on a rolling basis so that for each new grant of de minimis aid, the total amount of de minimis aid granted in the fiscal year concerned, as well as during the previous two fiscal years needs to be determined. Aid granted by any State authority or organ should be taken into account for this purpose even when financed entirely or partly from Community resources.
(6) Consistent with the principles governing aid falling within Article 87(1) of the Treaty, de minimis aid should be considered to be granted at the moment the legal right to receive the aid is conferred to the beneficiary under the applicable national legal regime. The de minimis rule is without prejudice to the possibility that undertakings receive State aid authorised by the Commission or covered by a group exemption Regulation.
(7) For the purpose of transparency, equal treatment and the correct application of the de minimis ceiling, it is appropriate that Member States should apply the same method of calculation. In order to facilitate this calculation and in accordance with the present practice of application of the de minimis rule, it is appropriate that aid amounts not taking the form of a cash grant should be converted into their gross grant equivalent. [Moreover, to ensure of effective monitoring, this Regulation should only apply to aid measures which are transparent in that the value of the aid can readily be quantified.] Calculation of the grant equivalent of transparent types of aid other than grants or payable in several instalments, requires the use of market interest rates prevailing at the time of granting such aid. With a view to a uniform, transparent and simple application of the State aid rules, the market rates for the purposes of this Regulation should be deemed to be the reference rates. The reference rates should be those which are periodically fixed by the Commission on the basis of objective criteria and published in the Official Journal of the European Union or on the Internet.
(8) The Commission has a duty to ensure that State aid rules are respected and in particular that aid granted under the de minimis rules adheres to the conditions thereof. In accordance with the cooperation principle laid down in Article 10 of the Treaty, Member States should facilitate the achievement of this task by establishing the necessary machinery in order to ensure that the total amount of aid, granted to the same beneficiary under the de minimis rule, does not exceed the ceiling of EUR 200000 over a period of three years. To that end, it is appropriate that Member States, when granting a de minimis aid, should inform the undertaking concerned of the de minimis character of the aid, by referring to the present Regulation, receive full information about other de minimis aid received during the last three fiscal years and carefully check that the de minimis ceiling will not be exceeded by the new de minimis aid. Alternatively respect of the ceiling may also be ensured by means of a central register.
(9) Regulation (EC) No 69/2001 expires on 31 December 2006. This Regulation should therefore apply from 1 January 2007.
(10) Having regard to the Commission's experience and in particular the frequency with which it is generally necessary to revise State aid policy, it is appropriate to limit the period of application of this Regulation. Should this Regulation expire without being extended, Member States should have an adjustment period of six months with regard to de minimis aid covered by this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
Scope
1. This Regulation applies to aid granted to undertakings in all sectors, with the exception of:
(a) the transport sector [6];
(b) activities linked to fishery and aquaculture products covered by Council Regulation (EC) No 104/2000 [7];
(c) activities linked to the primary production (farming) of agricultural products as listed in Annex I to the Treaty;
(d) aid granted to undertakings active in the processing and marketing of agricultural products as listed in Annex I to the Treaty the amount of which is fixed on the basis of price or quantity of such products purchased or put on the market, or conditional on being partly or entirely passed on to primary producers (farmers).
(e) aid to export-related activities, namely aid directly linked to the quantities exported, and aid contingent upon the use of domestic over imported goods
(f) the establishment and operation of a distribution network or to other current expenditure linked to the export activity in other Member States;
2. For the purpose of this Regulation,
(a) "agricultural products" means products listed in Annex I to the EC Treaty, with the exception of fishery products;
(b) "processing of agricultural products" means an operation on an agricultural product resulting in a product which is also an agricultural product;
(c) "marketing of agricultural products" means holding or display with a view to sale, offering for sale, delivery or any other manner of placing on the market.
3. This Regulation shall only apply to aid awarded in the form of grants and other types of aid in respect of which it is possible to calculate precisely the gross grant equivalent of the aid ex ante without need to undertake a risk assessment.
Aid comprised in loans, guarantees, risk capital measures and capital injections shall not be treated as de minimis aid, unless the total value of the transaction in question does not exceed the ceiling, as laid down in Article 2(2).
Article 2
De minimis aid
1. Aid measures shall be deemed not to meet all the criteria of Article 87(1) of the Treaty and shall therefore be exempt from the notification requirement of Article 88(3) of the Treaty, if they fulfil the conditions laid down in paragraphs 2 and 3.
2. The total de minimis aid granted to any one undertaking shall not exceed EUR 200000 gross over any period of three fiscal years. This ceiling shall apply irrespective of the form of the aid or the objective pursued and regardless of whether the aid granted by the Member State is financed entirely or partly by resources of Community origin. The period shall be determined by reference to the fiscal years used in the Member State concerned.
When an overall aid amount exceeds this ceiling, that aid cannot benefit from this Regulation, even for a fraction not exceeding that ceiling, either at the time it is granted or in respect of a later period.
3. The ceiling in paragraph 2 shall be expressed as a cash grant. All figures used shall be gross, that is, before any deduction of tax or other charge. Where aid is awarded in a form other than a grant, the aid amount shall be the gross grant equivalent of the aid.
Aid payable in several instalments shall be discounted to its value at the moment of its being granted. The interest rate to be used for discounting purposes and to calculate the gross grant equivalent shall be the reference rate applicable at the time of grant.
Article 3
Cumulation and monitoring
1. Where a Member State intends to grant de minimis aid to an undertaking, it shall inform that undertaking in writing about the amount of the aid (expressed as gross grant equivalent) and about its de minimis character, making express reference to this Regulation, and citing its title and publication reference in the Official Journal of the European Union. It shall also obtain from the undertaking concerned a written declaration about any other de minimis aid [or other State aid] received during the previous two fiscal years and the current fiscal year.
The Member State shall only grant the new de minimis aid after having checked that this will not raise the total amount of de minimis aid received during the relevant period of three years to a level above the ceiling laid down in Article 2(2).
2. Where a Member State has set up a central register of de minimis aid containing complete information on all de minimis aid granted by any authority within that Member State, the first subparagraph of paragraph 1 shall no longer apply from the moment the register covers a period of three fiscal years.
3. Member States shall record and compile all the information regarding the application of this Regulation. Such records shall contain all information necessary to demonstrate that the conditions of this Regulation have been complied with. Records regarding an individual de minimis aid shall be maintained for 10 fiscal years from the date on which it was granted and regarding a de minimis aid scheme, for 10 years from the date on which the last individual aid was granted under such scheme. On written request the Member State concerned shall provide the Commission, within a period of 20 working days, or such longer period as may be fixed in the request, with all the information that the Commission considers necessary for assessing whether the conditions of this Regulation have been complied with, in particular the total amount of de minimis aid received by any undertaking.
Article 4
Transitional measures
1. This Regulation shall apply to aid granted before its entry into force, if it fulfils all the conditions laid down in Articles 1, 2, and 3. Any aid which does not fulfil those conditions shall be assessed by the Commission in accordance with the relevant frameworks, guidelines, communications and notices.
2. Any de minimis aid granted between 2 February 2001 and 30 June 2007, which fulfils the conditions of Regulation (EC) No 69/2001, shall be deemed not to meet all the criteria of Article 87(1) of the Treaty and shall therefore be exempt from the notification requirement of Article 88(3) of the Treaty.
3. At the end of the period of validity of this Regulation, any de minimis aid which fulfils the conditions of this Regulation may be validly implemented for a period of six months.
Article 5
Entry into force and period of validity
1. This Regulation shall enter into force on the 20th day following that of its publication in the Official Journal of the European Union.
It shall apply from 1 January 2007 until 31 December 2013.
2. This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, …
For the Commission
…
Member of the Commission
[1] OJ L 142, 14.5.1998, p. 1.
[2] OJ C …
[3] OJ C 68, 6.3.1996, p. 9.
[4] OJ L 10, 13.1.2001, p. 30.
[5] As regards transport and coal, for the sake of simplicity, this draft at this stage replicates the scope of application of Regulation (EC) No 69/2001. This however does not prejudge the outcome of the ongoing consultation regarding the application of de minimis regulation to these sectors (2005/C 144/02).
[6] As regards transport and coal, for the sake of simplicity, this draft at this stage replicates the scope of application of Regulation (EC) No 69/2001Regulation. This however does not prejudge the outcome of the ongoing consultation regarding the application of de minimis regulation to these sectors (2005/C 144/02).
[7] OJ L 17, 21.1.2000, p. 22.
--------------------------------------------------
