COUNCIL DECISION of 10 November 1997 concluding the Agreement on scientific and technological cooperation between the European Community and the Republic of South Africa (97/763/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 130m, in conjunction with Article 228 (2), first sentence, and the first subparagraph of Article 228 (3) thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Whereas the European Community and South Africa are pursuing specific RTD programmes in areas of common interest;
Whereas on the basis of past experience, both sides have expressed a desire to establish a deeper and broader framework for the conduct of collaboration in science and technology;
Whereas this agreement to cooperate in the field of science and technology forms part of the global cooperation between the Community and its Member States, on the one hand, and South Africa, on the other hand;
Whereas by its Decision of 22 January 1996, the Council authorized the Commission to negotiate an agreement on scientific and technological cooperation between the Community and the Republic of South Africa;
Whereas by its Decision of 5 December 1996, the Council decided that the Agreement on scientific and technological cooperation be signed on behalf of the Community;
Whereas the Agreement on scientific and technological cooperation between the European Community and the Republic of South Africa was signed on 5 December 1996;
Whereas the Agreement on scientific and technological cooperation between the European Community and the Republic of South Africa should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement on scientific and technological cooperation between the European Community and the Republic of South Africa is hereby approved on behalf of the Community.
The text of the Agreement is attached to this Decision.
Article 2
Pursuant to Article 11 of the Agreement, the President of the Council shall give notification that the procedures necessary for the entry into force of the Agreement have been completed on the part of the European Community.
Done at Brussels, 10 November 1997.
For the Council
The President
E. HENNICOT-SCHOEPGES
(1) OJ C 134, 29. 4. 1997, p. 7.
(2) OJ C 304, 6. 10. 1997.
