Removal from the register of Case C-478/04 [1]
(2005/C 296/39)
(Language of the case: Italian)
By order of 24 June 2005, the President of the Court of Justice of the European Communities has ordered the removal from the register of Case C-478/04 Commission of the European Communities
v Italian Republic.
[1] OJ C 31, 5.2.2005.
--------------------------------------------------
