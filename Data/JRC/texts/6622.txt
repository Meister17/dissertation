Decision No 2/2005
of 30 March 2005
of the Committee established under the Agreement on Mutual Recognition between the EC and the Swiss Confederation on amending Chapter 3 of Annex 1
(2005/352/EC)
THE COMMITTEE,
Having regard to the Agreement between the European Community and the Swiss Confederation on Mutual Recognition in relation to Conformity Assessment (the Agreement) signed on 21 June 1999, and in particular Article 10(4)(e) and 18(2) thereof;
HAS DECIDED AS FOLLOWS:
1. Chapter 3, Toys, Annex I, Section I, to the Agreement is amended in accordance with the provisions set out in the Annex to this Decision.
2. This Decision, done in duplicate, shall be signed by the Co-Chairs or other persons authorised to act on behalf of the Parties. This Decision shall be effective from the date of the latter of these signatures.
Done at Bern, 30 March 2005.
On behalf of the Swiss Confederation
Heinz Hertig
Done at Brussels, 23 March 2005.
On behalf of the European Community
Joanna Kioussi
--------------------------------------------------
ANNEX
AMENDMENTS TO THE AGREEMENT
In Annex 1, Product Sectors, Chapter 3, (Toys), Section I, (Legislative, regulatory and administrative provisions), "Provisions covered by Article 1(1)" is deleted and replaced by "Provisions covered by Article 1(2)".
In Annex 1, Product Sectors, Chapter 3, Toys, Section I, in the list headed "Switzerland", the Swiss legal references should be deleted and replaced by the following list:
"Federal Law of 9 October 1992 on foodstuffs and commodities (RO 1995 1469) as last amended on 21 March 2003 (RO 2003 4803)
Ordinance of 1 March 1995 on commodities (RO 1995 1643) as last amended on 15 December 2003 (RO 2004 1111)
Ordinance of EDI of 27 March 2002 on the safety of toys (RO 2002 1082) as last amended on 2 October 2003 (RO 2003 3733)".
--------------------------------------------------
