Reference for a preliminary ruling from the Oberlandesgerichts Wien lodged on 7 April 2006 — Renate Ilsinger v Martin Dreschers (administrator in the insolvency of Schlank & Schick GmbH)
Referring court
Oberlandesgericht Wien
Parties to the main proceedings
Applicant: Renate Ilsinger
Defendant: Martin Dreschers (administrator in the insolvency of Schlank & Schick GmbH)
Questions referred
1. Does the provision in Paragraph 5j of the Konsumentenschutzgesetz (Law on consumer protection; KSchG), BGBl 1979/140, in the version of Art I, para. 2 of the Fernabsatz-Gesetz (Law on distance selling), BGBl I 1999/185, which entitles certain consumers to claim from undertakings in the courts prizes ostensibly won by them where the undertakings send (or have sent) them prize notifications or other similar communications worded so as to give the impression that they have won a particular prize, constitute, in circumstances where the claiming of that prize was not made conditional upon actually ordering goods or placing a trial order and where no goods were actually ordered but the recipient of the communication is nevertheless seeking to claim the prize, for the purposes of Council Regulation (EC) No 44/2001 of 22 December 2000 on jurisdiction and the recognition and enforcement of judgments in civil and commercial matters ("the regulation" [1] a contractual, or equivalent, claim under Article 15(1)(c) of the regulation?
If the answer to question 1 is in the negative:
2. Does a claim falling under Article 15(1)(c) of the regulation arise if the claim for payment of the prize was not made conditional upon ordering goods but the recipient of the communication has actually placed an order for goods?
[1] OJ L 12 of 16.1.2001, p. 1.
--------------------------------------------------
