Notice concerning a request in accordance with Article 30 of Directive 2004/17/EC
(2006/C 287/11)
Request made by a Member State
On 24 October 2006 the Commission received a request in accordance with Article 30(4) of Directive 2004/17/EC of the European Parliament and of the Council of 31 March 2004 coordinating the procurement procedures of entities operating in the water, energy, transport and postal services sectors [1].
This request, which comes from the United Kingdom, concerns the supply of electricity and gas in that country, with the exception of Northern Ireland (the request therefore concerns the supply of electricity and gas in England, Scotland and Wales). The request was published in OJ C 270 of 7 November 2006. The initial period expires on 25 January 2007.
Given that the Commission departments need to obtain and examine further information and in compliance with the provisions laid down in the third sentence of Article 30(6), the period within which the Commission must take a decision on this request is extended by one month.
The final period will therefore expire on 26 February 2006.
[1] OJ L 134, 30.4.2004, p. 1.
--------------------------------------------------
