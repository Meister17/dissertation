Council Decision
of 22 December 2000
establishing a European Police College (CEPOL)
(2000/820/JHA)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union, and in particular Article 30(1)(c) and Article 34(2)(c) thereof,
Having regard to the initiative of the Portuguese Republic(1),
Having regard to the opinion of the European Parliament(2),
Whereas:
(1) At its meeting in Tampere on 15 and 16 October 1999, the European Council agreed that a European Police College, hereinafter referred to as "CEPOL", should be established to train senior officers of police forces, such forces being understood to mean law enforcement officials as referred to in point 47 of the Presidency conclusions.
(2) The European Council in Tampere agreed that CEPOL should initially consist of a network of existing national training institutes, without precluding the establishment of a permanent institution at a later stage.
(3) There already exist national, European and international organisations and bodies active in the area of police training, on whose cooperation CEPOL should be able to rely in carrying out its tasks.
(4) It is desirable to develop quickly a relationship between CEPOL and national training institutes in applicant countries with which the European Union is conducting accession negotiations as well as those in Iceland and Norway, so that these institutes can have access to CEPOL activities.
(5) The European Union has been active in this field, in particular by adopting and implementing programmes under Title VI of the Treaty, such as the common programme for the exchange and training of, and cooperation between, law enforcement authorities (OISIN)(3) and the programme for exchanges, training and cooperation for persons responsible for action to combat organised crime (Falcone)(4).
(6) CEPOL should carry out its tasks by progressive stages in the light of the objectives set out in the annual work programmes and with due regard for available resources.
(7) It is necessary that this Decision be reviewed after a three-year period in order to decide on an extension of CEPOL's tasks, and on modifications to its institutional structure,
HAS DECIDED AS FOLLOWS:
TITLE I
Organisation
Article 1
1. A European Police College (CEPOL) is hereby established.
2. Without prejudice to the future developments recommended in Article 9, CEPOL shall be set up as a network, by bringing together the national training institutes for senior police officers in the Member States, which shall cooperate closely to that end.
3. CEPOL's task shall be to implement the programmes and initiatives decided upon by the governing board.
Article 2
1. The directors of the national training institutes for senior police officers shall form CEPOL's governing board. Where there are several directors from a single Member State, they shall together form a delegation.
2. The governing board shall be chaired by the director of a national training institute of the Member State holding the Presidency of the Council of the European Union. The governing board shall meet at least once per Presidency. It shall establish its rules of procedure by unanimous decision.
3. Each delegation shall have one vote on the governing board.
Representatives of the General Secretariat of the Council of the European Union, the Commission and Europol shall be invited to attend meetings as non-voting observers. Members of the governing board may be accompanied by experts.
Article 3
1. The governing board shall decide on the annual continuing-education programme (teaching content, type, number and length of training measures to be implemented). It shall adopt additional programmes and initiatives, where appropriate.
2. The governing board shall establish the annual report on CEPOL's activities.
3. The governing board's decisions referred to in paragraphs 1 and 2 shall be adopted unanimously and then passed on to the Council, which shall take note of them and endorse them. Due account shall be taken by the governing board of any comment made by the Council.
The annual report on CEPOL's activities shall also be forwarded to the European Parliament and the Commission for information.
Article 4
1. The governing board shall set up a permanent secretariat to assist CEPOL with the administrative tasks necessary for it to function and implement the annual programme and, where appropriate, the additional programmes and initiatives. The permanent secretariat may be set up within one of the national police academies. The Council shall decide on the location of the permanent secretariat's seat.
2. The secretariat shall be headed by an administrative director appointed by the governing board for a three-year term.
3. All decisions of the governing board concerning the secretariat shall be taken unanimously.
Article 5
1. CEPOL's budget shall be managed by the secretariat on the basis of a financial regulation.
2. The costs of implementing the measures in the annual programme referred to in Article 3, together with the administrative costs of CEPOL, shall be borne jointly by the Member States. To that end, the annual contribution from each Member State shall be established on the basis of the gross national product (GNP) according to the scale used for determining the GNP element in own resources for financing the general budget of the European Union. Each year the GNP of the previous year shall be taken as the reference basis for each Member State.
3. CEPOL's financial regulation and annual budget shall be drawn up by the governing board acting unanimously, and submitted for approval to the Governments of the Member States, meeting within the Council.
4. Expenditure on the following shall be borne by the budget of CEPOL:
(a) preparation, implementation and assessment of the annual programme;
(b) fees for external contributors;
(c) travelling expenses of governing board members attending board meetings, at the rate of two members per Member State;
(d) general operating costs of the secretariat, except for the remuneration of its members;
(e) costs for any other initiative adopted by the governing board or taken by the administrative director in accordance with the financial regulation;
(f) reimbursement, in proportion to Member States' contributions, of the costs incurred by the Member State(s) paying the remuneration of the secretariat's members.
5. Without prejudice to requests by Member States and acting on instructions from the governing board, the secretariat may submit to the Commission training projects or programmes for cofinancing that lie within the sphere of competence of budgetary programmes administered by the Commission.
TITLE II
Objectives and tasks
Article 6
1. The aim of CEPOL shall be to help train the senior police officers of the Member States by optimising cooperation between CEPOL's various component institutes. It shall support and develop a European approach to the main problems facing Member States in the fight against crime, crime prevention, and the maintenance of law and order and public security, in particular the cross-border dimensions of those problems.
2. CEPOL's objectives shall be as follows:
(a) to increase knowledge of the national police systems and structures of other Member States, of Europol and of cross-border police cooperation within the European Union;
(b) to strengthen knowledge of international instruments, in particular those which already exist at European Union level in the field of cooperation on combating crime;
(c) to provide appropriate training with regard to respect for democratic safeguards with particular reference to the rights of defence;
(d) to encourage cooperation between CEPOL and other police training institutes.
3. CEPOL shall also offer its infrastructure to senior police officers of applicant countries with which the European Union is conducting accession negotiations as well as those of Iceland and Norway.
Article 7
In order to achieve those objectives, CEPOL may, in particular, undertake the following actions:
(a) provide training sessions, based on common standards, for senior police officers;
(b) contribute to the preparation of harmonised programmes for the training of middle-ranking police officers, middle-ranking police officers in the field and police officers in the field with regard to cross-border cooperation between police forces in Europe, and help set up appropriate advanced training programmes;
(c) provide specialist training for police officers playing a key role in combating cross-border crime, with a particular focus on organised crime;
(d) develop and provide training for trainers;
(e) disseminate best practice and research findings;
(f) develop and provide training to prepare police forces of the European Union for participation in non-military crisis management;
(g) develop and provide training for police authorities from the States applying for membership of the European Union, including training for police officers with a key role;
(h) facilitate relevant exchanges and secondments of police officers in the context of training;
(i) develop an electronic network to provide back-up for CEPOL in the performance of its duties, ensuring that the necessary security measures are put in place;
(j) enable the senior police officers of the Member States to acquire relevant language skills.
TITLE III
Other provisions
Article 8
CEPOL shall consider on a case-by-case basis the possibility of admitting officials of the European Institutions and other European Union bodies.
CEPOL may cooperate with the national police training institutes of non-member States of the European Union. In particular, it shall establish relations with the national institutes of applicant countries with which the European Union is conducting accession negotiations as well as those of Iceland and Norway.
CEPOL shall also cooperate with relevant training bodies in Europe, such as the Nordic Baltic Police Academy (NBPA) and the Mitteleuropäische Polizeiakademie (MEPA).
Article 9
At the latest during the third year after this Decision takes effect, the governing board shall submit to the Council a report on the operation and future of the network, in accordance with the conclusions of the Tampere European Council.
Article 10
This Decision shall take effect on the day following that of its adoption by the Council.
It shall apply from 1 January 2001.
Done at Brussels, 22 December 2000.
For the Council
The President
C. Pierret
(1) OJ C 206, 19.7.2000, p. 3.
(2) Opinion delivered on 17 November 2000 (not yet published in the Official Journal).
(3) OJ L 7, 10.1.1997, p. 5.
(4) OJ L 99, 31.3.1998, p. 8.
