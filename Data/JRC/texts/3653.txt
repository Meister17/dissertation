Council Decision
of 20 September 2005
on the conclusion of the Agreement in the form of an Exchange of Letters concerning the provisional application of the Protocol setting out the fishing opportunities and financial contribution provided for in the Agreement between the European Economic Community and the Islamic Federal Republic of the Comoros on fishing off the Comoros for the period from 1 January 2005 to 31 December 2010
(2005/669/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 37 in conjunction with Article 300(2) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) In accordance with the Agreement between the European Economic Community and the Islamic Federal Republic of the Comoros on fishing off the Comoros [1], the Community and the Islamic Federal Republic of the Comoros held negotiations to determine amendments or additions to be made to the Agreement at the end of the period of application of the Protocol to the Agreement.
(2) As a result of those negotiations, a new Protocol was initialled on 24 November 2004.
(3) Under that Protocol, Community fishermen enjoy fishing opportunities in the waters falling within the sovereignty or jurisdiction of the Islamic Federal Republic of the Comoros for the period from 1 January 2005 to 31 December 2010.
(4) In order to ensure uninterrupted fishing activities by Community vessels, it is essential that the new Protocol be applied as quickly as possible. For this reason, the two parties have initialled an Agreement in the form of an Exchange of Letters providing for the provisional application of the initialled Protocol as from 1 January 2005.
(5) The Agreement in the form of an Exchange of Letters should be approved, subject to the conclusion of the Protocol by the Council.
(6) The method of allocating the fishing opportunities among the Member States should be defined on the basis of the traditional allocation of fishing opportunities under the Fisheries Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement in the form of an Exchange of Letters concerning the provisional application of the Protocol setting out the fishing opportunities and financial contribution provided for in the Agreement between the European Economic Community and the Islamic Federal Republic of the Comoros on fishing off the Comoros for the period from 1 January 2005 to 31 December 2010 is hereby approved on behalf of the Community.
The texts of the Agreement in the form of an Exchange of Letters and of the Protocol are attached to this Decision.
Article 2
The fishing opportunities set out in the Protocol shall be allocated among the Member States as follows:
(a) tuna seiners:
Spain: 21 vessels;
France: 18 vessels;
Italy: 1 vessel;
(b) surface longliners:
Spain: 12 vessels;
Portugal: 5 vessels.
If licence applications from these Member States do not cover all the fishing opportunities set out in the Protocol, the Commission may take into consideration licence applications from any other Member State.
Article 3
Member States whose vessels fish under this Protocol are obliged to notify the Commission of the quantities of each stock taken in the Comorian fishing zone in accordance with the arrangements laid down in Commission Regulation (EC) No 500/2001 [2].
Article 4
The President of the Council is hereby authorised to designate the persons empowered to sign the Agreement in order to bind the Community.
Done at Brussels, 20 September 2005.
For the Council
The President
M. Beckett
[1] OJ L 137, 2.6.1988, p. 19.
[2] OJ L 73, 15.3.2001, p. 8.
--------------------------------------------------
