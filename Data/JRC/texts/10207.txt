Commission Regulation (EC) No 1295/2006
of 31 August 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 1 September 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 31 August 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 31 August 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 87,3 |
068 | 147,1 |
999 | 117,2 |
07070005 | 052 | 92,8 |
999 | 92,8 |
07099070 | 052 | 60,4 |
999 | 60,4 |
08055010 | 388 | 72,5 |
524 | 51,0 |
528 | 57,4 |
999 | 60,3 |
08061010 | 052 | 83,7 |
220 | 123,4 |
999 | 103,6 |
08081080 | 388 | 88,1 |
400 | 93,0 |
508 | 83,8 |
512 | 89,0 |
528 | 43,0 |
720 | 89,7 |
800 | 143,8 |
804 | 104,0 |
999 | 91,8 |
08082050 | 052 | 115,2 |
388 | 90,2 |
999 | 102,7 |
08093010, 08093090 | 052 | 116,4 |
096 | 12,8 |
999 | 64,6 |
08094005 | 052 | 82,7 |
066 | 44,4 |
098 | 45,7 |
624 | 150,5 |
999 | 80,8 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
