Commission notice pursuant to Article 4(1)(a) of Council Regulation (EEC) No 2408/92
Imposition of public service obligations in respect of scheduled air services within Italy
(2006/C 112/04)
(Text with EEA relevance)
Pursuant to Article 4(1)(a) of Council Regulation (EEC) No 2408/92 of 23 July 1992 on access for Community air carriers to intra-Community air routes, the Italian Government, in accordance with the decisions taken at the Interdepartmental Conference chaired by the Region of Sicily, has decided to impose public service obligations in respect of scheduled air services on the following route:
1. Route
Pantelleria-Trapani and vice versa;
1.1. By virtue of Article 9 of Council Regulation (EEC) No 95/93 on common rules for the allocation of slots at Community airports, as amended by Commission Regulation (EC) No 793/2004, the competent authorities may reserve certain slots for the performance of services under the arrangements specified in this notice.
1.2. With a view to meeting the objectives being pursued through the imposition of the public service obligations, ENAC will check that accepting carriers have the appropriate structure and that they meet the minimum requirements for access to the service.
2. The public service obligations in question are as follows:
2.1 Minimum frequency
The minimum frequency on the above route is as follows:
Three outward flights and three return flights per day all year round with 44-seater aircraft.
If the aircraft normally used for the performance of the service concerned should break down, a usable aircraft must be made available within four hours.
The entire capacity of each aircraft must be offered for sale under the terms of the obligations.
2.2. Timetables
On the Pantelleria-Trapani route the timetables must provide for:
- 1 flight departing between 07.30 and 09.30
- 1 flight departing between 18.00 and 20.00
On the Trapani-Pantelleria route the timetables must provide for:
- 1 flight departing between 06.00 and 09.00
- 1 flight departing between 17.00 and 19.00
2.3. Type of aircraft used and capacity provided
The services specified in Point 2.1 must be operated with a pressurised twin-turboprop or twin-jet aircraft having a seating capacity of at least 44 on outward flights and at least 44 on return flights.
Should the market so require, a greater capacity will be offered by introducing additional flights which will not give rise to extra compensation or fares other than those specified in Point 2.4 below.
The services specified in Point 2.1 must hold three seats in reserve for use in medical emergencies or for the needs of the institutional bodies. Of the abovementioned three seats two must not be made available for reservation or sale until 24 hours before departure, and one until 12 hours before departure.
The carrier which accepts the obligations will, subject to safety considerations which may justify denied boarding, take all necessary measures to facilitate the carriage of disabled passengers and passengers with reduced mobility.
2.4. Fares
(a) The maximum fares to be applied on each route are as follows:
Pantelleria-Trapani: EUR 25,00
Trapani-Pantelleria: EUR 25,00
All the above fares are net of VAT, airport taxes and charges; no form of surcharge may be levied.
At least one form of ticket distribution and sale must be provided which is entirely free of charge and places no additional economic burden on passengers.
All the passengers travelling on the route Pantelleria-Trapani and vice versa shall be entitled to the above fares.
(b) Every year the competent authorities will adjust the maximum fares in accordance with the rate of inflation of the previous year calculated on the basis of the ISTAT/FOI general index of consumer prices. Adjustments will be notified to all carriers operating on the route in question, and to the European Commission for publication in the Official Journal of the European Union.
(c) If the average recorded in each half-year for the euro/US dollar exchange rate and/or the cost of aviation fuel changes by more than 5 %, fares must be adjusted in proportion to the change recorded and in proportion to the share of flight costs represented by fuel.
Six-monthly fare adjustments will be made, where appropriate, by the Minister for Infrastructure and Transport in agreement with the President of the Region of Sicily, on the basis of a report by a Joint Technical Committee composed of one representative appointed by ENAC and one representative appointed by the Region of Sicily, which must consult the carriers operating the routes concerned.
Any adjustments will enter into force as of the next half year.
Adjustments will be notified to all carriers operating on the route in question, and to the European Commission for publication in the Official Journal of the European Union.
2.5. Continuity of service
For the purpose of guaranteeing the continuation, regularity and punctuality of the service, the carrier which accepts these public service obligations must undertake to:
- provide the service for at least 12 consecutive months and must not suspend it without at least six months' notice;
- ensure that their conduct vis-à vis users conforms to the principles set out in the Charter of Passengers' Rights, so as to comply with the relevant national, Community and international regulations;
- provide a performance security for the purpose of guaranteeing the correct performance and continuation of the service. The security shall amount to at least EUR 800000,00 and must be guaranteed by an insurance surety payable to ENAC (the Italian National Civil Aviation Authority), which may use it to ensure the continuation of the services concerned;
- carry out at least 98 % of scheduled flights in any year, with the number of cancellations for reasons directly attributable to the carrier, excluding cases of force majeure, limited to a maximum of 2 %;
- pay the regulatory body a penalty of EUR 3000,00 for every flight cancelled over the 2 % limit. The sums received as penalties shall be reallocated to promote territorial continuity in Sicily.
--------------------------------------------------
