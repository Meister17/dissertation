Information communicated by Member States regarding State aid granted under Commission Regulation (EC) No 68/2001 of 12 January 2001 on the application of Articles 87 and 88 of the EC Treaty to training aid
(2005/C 151/07)
(Text with EEA relevance)
Aid No | XT 26/04 |
Member State | Belgium |
Region | Flanders |
Title of aid scheme or name of company receiving individual aid | VDL JONCKHEERE BUS & Coach NVSchoolstraat 508800 ROESELARE |
Legal basis | Besluit van de Vlaamse regering van 2.4.2004 |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | |
Loans guaranteed | |
Individual aid | Overall aid amount | EUR 0,09 million |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(6) of the Regulation | Yes | |
Date of implementation | 2.4.2004 |
Duration of scheme or individual aid award | Until 31.12.2004 |
Objective of aid | General training | Yes |
Specific training | Yes |
Economic sectors concerned | All sectors eligible for training aid | No |
Limited to specific sectors | Ad hoc case |
—Agriculture | |
—Fisheries and aquaculture | |
—Coalmining | |
—All manufacturing | |
or | |
Steel | |
Shipbuilding | |
Synthetic fibres | |
Motor vehicles | Construction of buses |
Other manufacturing | |
—All services | |
or | |
Maritime transport services | |
Other transport services | |
Financial services | |
Other services | |
Name and address of the granting authority | Name:Ministerie van de Vlaamse GemeenschapAdministratie EconomieAfdeling Economisch Ondersteuningsbeleid |
Address:Markiesstraat 11000 Brussel |
Large individual aid grants | In conformity with Article 5 of the RegulationThe measure excludes awards of aid or requires prior notification to the Commission of awards of aid, if the amount of aid granted to one enterprise for a single training project exceeds EUR 1 million. | Yes | |
Aid No | XT 30/04 |
Member State | Belgium |
Region | Flanders |
Title of aid scheme or name of company receiving individual aid | Corus Aluminium NVA. Stockletlaan 872570 Duffel |
Legal basis | Besluit van de Vlaamse regering van 2.4.2004 |
Annual expenditure planned or overall amount of aid granted to the company | Aid scheme | Annual overall amount | |
Loans guaranteed | |
Individual aid | Overall aid amount | EUR 0,2 million |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(6) of the Regulation | Yes | |
Date of implementation | 2.4.2004 |
Duration of scheme or individual aid award | Until 31.12.2004 |
Objective of aid | General training | Yes |
Specific training | Yes |
Economic sectors concerned | All sectors eligible for training aid | No |
Limited to specific sectors | Ad hoc case |
—Agriculture | |
—Fisheries and aquaculture | |
—Coalmining | |
—All manufacturing | |
or | |
Steel | |
Shipbuilding | |
Synthetic fibres | |
Motor vehicles | |
Other manufacturing | Aluminium plate |
—All services | |
or | |
Maritime transport services | |
Other transport services | |
Financial services | |
Other services | |
Name and address of the granting authority | Name:Ministerie van de Vlaamse GemeenschapAdministratie EconomieAfdeling Economisch Ondersteuningsbeleid |
Address:Markiesstraat 11000 Brussels |
Large individual aid grants | In conformity with Article 5 of the RegulationThe measure excludes awards of aid or requires prior notification to the Commission of awards of aid, if the amount of aid granted to one enterprise for a single training project exceeds EUR 1 million. | Yes | |
Aid No | XT 35/04 |
Member State | Italy |
Region | Throughout national territory |
Title of aid scheme or name of company receiving individual aid | Aid for the training of workers |
Legal basis | Regolamento (CE) n. 1260/1999 del Consiglio del 21.6.99 recante disposizioni generali sui Fondi strutturali;Regolamento (CE) n. 1784/1999 del Parlamento europeo e del Consiglio del 12.7.99 relativo al Fondo sociale europeo;Regolamento (CE) n. 1159/2000 relativo alle azioni informative e pubblicitarie a cura degli Stati membri sugli interventi dei Fondi strutturali;Regolamento (CE) n. 438/2001 della Commissione del 2 marzo 2001 recante modalità di applicazione del Regolamento (CE) n. 1260/1999 del Consiglio per quanto riguarda i sistemi di gestione e di controllo dei contributi concessi nell'ambito dei Fondi strutturali e le modifiche apportate dal Regolamento (CE) n. 2355/2002;Regolamento (CE) n. 448/2004 della Commissione del 10 marzo 2004 che modifica il Regolamento (CE) n. 1685/2000 recante disposizioni di applicazione del Regolamento (CE) n. 1260/1999 del Consiglio per quanto riguarda l'ammissibilità delle spese concernenti le operazioni cofinanziate dai Fondi strutturali e che revoca il Regolamento (CE) n. 1145/2003;Comunicazione della Commissione europea n. C(2000) 853 del 14.4.2000 che stabilisce gli orientamenti dell'Iniziativa Comunitaria EQUAL, relativa alla cooperazione transnazionale per promuovere nuove pratiche di lotta alle discriminazioni e alle disuguaglianze di ogni tipo in relazione al mercato del lavoro;Comunicazione della Commissione europea n. C(2003) 840 del 30.12.2003 che definisce gli orientamenti per la seconda fase dell'Iniziativa Comunitaria EQUAL, relativa alla cooperazione transnazionale per promuovere nuovi mezzi di lotta contro tutte le forme di discriminazioni e di disparità connesse al mercato del lavoro;Decisione della Commissione C(2001) 43 del 26.03.01 recante approvazione del programma di iniziativa comunitaria per la lotta contro le discriminazioni e le disuguaglianze in relazione al mercato del lavoro (EQUAL) in Italia;Documento Unico di Programmazione, approvato dal Comitato di Sorveglianza nella riunione del 12 marzo 2004 ed inviato alla Commissione europea per l'approvazione definitiva;Deliberazione n. 67 del 22 giugno 2000 del CIPE ("Definizione delle aliquote di cofinanziamento pubblico nazionale per i programmi di iniziativa comunitaria EQUAL, INTERREG III, LEADER + e URBAN II, relativi al periodo 2000-2006"). |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | EUR 3,5 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(7) of the Regulation | Yes | No |
Date of implementation | 10.5.2004 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | General training | Yes |
Specific Training | Yes |
Economic sectors concerned | All sectors eligible for training aid | Yes |
Limited to specific sectors | No |
—Agriculture | |
—Fisheries and aquaculture | |
—Coalmining | |
—All manufacturing | |
or | |
Steel | |
Shipbuilding | |
Synthetic fibres | |
Motor vehicles | |
Other manufacturing | |
—All services | |
or | |
Maritime transport services | |
Other transport services | |
Financial services | |
Other services | |
Name and address of the granting authority | Name:MINISTERO DEL LAVORO E DELLE POLITICHE SOCIALI — UFFICIO CENTRALE FORMAZIONE PROFESSIONALE DEI LAVORATORI — DIV. IV |
Address:VIA FORNOVO, 800192 ROME |
Large individual aid grants | In conformity with Article 5 of the Regulation | Yes | No |
Aid No | XT 36/04 |
Member State | Poland |
Region | Entire country |
Title of aid scheme or name of company receiving individual aid | Training aid scheme involving various tax breaks |
Legal basis | Art. 48 § 1 pkt 1 i 2 i art. 67 § 1 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (Dz. U. Nr 137, poz. 926 z późn. zm.)Rozporządzenie Rady Ministrów z dnia 21 kwietnia 2004 r. w sprawie szczegółowych warunków udzielania pomocy na szkolenia w zakresie niektórych ulg podatkowych (Dz. U. Nr 95, poz. 955), |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | EUR 10,39 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(7) of the Regulation | Yes | |
Date of implementation | 1.5.2004 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | General training | Yes |
Specific training | Yes |
Economic sectors concerned | All sectors eligible for training aid | Yes |
Limited to specific sectors | No |
—Agriculture | |
—Fisheries and aquaculture | |
—Coalmining | |
—All manufacturing | |
or | |
Steel | |
Shipbuilding | |
Synthetic fibres | |
Motor vehicles | |
Other manufacturing | |
—All services | |
or | |
Maritime transport services | |
Other transport services | |
Financial services | |
Other services | |
Name and address of the granting authority | Name:Tax-collection bodies:1.the head of the tax office, the head of the customs office, the head of the local authority, the mayor, the head of the district authority or the head of the voivodship — as the body of first instance,2.the head of the tax chamber and the head of the customs chamber — as:(a)the body repealing decisions issued by the head of the tax office or the head of the customs office respectively,(b)the body of first instance, on the basis of separate provisions,(c)the body repealing decisions issued by this body of first instance,3.the local government appeals board — as the body repealing decisions issued by the head of the local authority, the mayor, the head of the district authority or the head of the voivodship.4.the Minister for Public Finances is a tax collection body — as:(a)the body of first instance in cases involving the declaration of invalidity of a decision, renewal of proceedings, amendments to or repeal of a decision or a declaration that it has expired — in its official capacity.(b)the body repealing decisions issued in the cases referred to at a. |
Address:Whole country |
Large individual aid grants | In conformity with Article 5 of the Regulation | Yes | |
Aid No | XT 41/04 |
Member State | Germany |
Region | Bavaria |
Title of aid scheme or name of company receiving individual aid | Torhauswerkstatt GmbH |
Legal basis | BayHO, VO (EG) Nr. 1260/1999, VO (EG) Nr. 1784/1999, EPPD zu Ziel 3, Programmergänzung zu Ziel 3 |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | |
Loans guaranteed | |
Individual aid | Overall aid amount | EUR 118163 |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(7) of the Regulation | Yes | |
Date of implementation | 15.6.2004 |
Duration of the scheme or individual aid award | Until 31.12.2004 |
Objective of aid | General training | Yes |
Specific training | |
Economic sector concerned | All sectors eligible for training aid | Yes |
Limited to specific sectors | |
—Agriculture | |
—Fisheries and aquaculture | |
—Coalmining | |
—All manufacturing | |
or | |
Steel | |
Shipbuilding | |
Synthetic fibres | |
Motor vehicles | |
Other manufacturing | |
—All services | |
or | |
Maritime transport | |
Other transport services | |
Financial services | |
Other services | |
Name and address of the granting authority | Name:Bayerisches Landesamt für Versorgung und Familienförderung |
Address:Hegelstraße 4, 95447 Bayreuth |
Large individual aid grants | In conformity with Article 5 of the Regulation | Yes | |
Aid No | XT 45/04 |
Member State | Estonia |
Region | Estonia |
Title of aid scheme or name of company receiving individual aid | "The Estonian national development plan introducing the EU structural funds' single programming document for the years 2004-06"; Measure 1.2 "Development of enterprises' human resources to increase economic competitiveness", Section "Training" |
Legal basis | Majandus- ja kommunikatsiooniministri määrus 15.6.2004.a nr 154 |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | Estonia: EUR 0,32 million;ERDF: EUR 0,96 million;Total: EUR 1,28 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(7) of the Regulation | Yes | |
Date of implementation | 1.7.2004 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | General training | Yes |
Specific training | Yes |
Economic sectors concerned | All sectors eligible for training aid | |
Limited to specific sectors | Yes |
—Agriculture | No |
—Fisheries and aquaculture | No |
—Coalmining | No |
—All manufacturing | |
or | |
Steel | |
Shipbuilding | |
Synthetic fibres | |
Motor vehicles | |
Other manufacturing | Yes |
—All services | |
or | |
Maritime transport services | |
Other transport services | |
Financial services | |
Other services | Yes |
Name and address of the granting authority | Name:Ettevõtluse Arendamise Sihtasutus |
Address:Roosikrantsi 11, EE-10119 Tallinn, Eesti Vabariik |
Large individual aid grants | In conformity with Article 5 of the Regulation | Yes | |
Aid No | XT 53/04 |
Member State | United Kingdom |
Region | North East of England |
Title of aid scheme or name of company receiving individual aid | The Welding Institute Ltd (TWI) — Joining Forces North East Training 4 |
Legal basis | Section 11(1) of the Industrial Act 1982 |
Annual expenditure planned under the scheme or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | |
Loans guaranteed | |
Individual aid | Overall aid amount | GBP 67257 |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(7) of the Regulation | Yes | |
Date of implementation | From 01 January 2004 |
Duration of scheme or individual aid award | Until 31 December 2004 |
Objective of aid | General training | No |
Specific training | Yes |
Economic sectors concerned | All sectors eligible for training aid | Yes |
Limited to specific sectors | No |
—Agriculture | |
—Fisheries and aquaculture | |
—Coalmining | |
—All manufacturing | |
or | |
Steel | |
Shipbuilding | |
Synthetic fibres | |
Motor vehicles | |
Other manufacturing | |
—All services | |
or | |
Maritime transport services | |
Other transport services | |
Financial services | |
Other services | |
Name and address of the granting authority | Name:Government Office for the North EastEuropean Programmes Secretariat |
Address:Wellbar HouseGallowgateNewcastle upon TyneNE1 4TD(As of 9 August 2004, the following address applies:CitygateGallowgateNewcastle upon TyneNE1 4WH) |
Large individual aid grants | In conformity with Article 5 of the Regulation | Yes | |
Aid No | XT 54/04 |
Member State | Estonia |
Region | Estonia |
Title of aid scheme or name of company receiving individual aid | Estonian National Development Plan for the Implementation of the EU Structural Funds — Single Programming Document 2004-2006, Measure 1.3 "Inclusive Labour Market" |
Legal basis | Sotsiaalministri määrus nr 89 ( 7.7.2004) RAK meetme 1.3 "Võrdsed võimalused tööturul" tingimused ja toetuse seire läbiviimise eeskiri |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | EUR 8 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(7) of the Regulation | Yes | |
Date of implementation | 19.7.2004 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | General training | Yes |
Specific training | Yes |
Economic sectors concerned | All sectors eligible for training aid | |
Limited to specific sectors | Yes |
—Agriculture | |
—Fisheries and aquaculture | |
—Coalmining | No |
—All manufacturing | Yes |
or | |
Steel | |
Shipbuilding | |
Synthetic fibres | |
Motor vehicles | |
Other manufacturing | |
—All services | Yes |
or | |
Maritime transport services | |
Other transport services | |
Financial services | |
Other services | |
Name and address of the granting authority | Name:Tööturuamet |
Address:Luha 1610129 TallinnEstonia |
Large individual aid grants | In conformity with Article 5 of the Regulation | Yes | |
Aid No | XT 82/04 |
Member State | France |
Region | Aquitaine |
Title of aid scheme or name of company receiving individual aid | Law to promote the training of company employees |
Legal basis | Règlement (CE) no 68/2001 du 12 janvier 2001 de la Commission européenne sur les aides à la formationRèglement d'intervention sur la formation des salariés du Conseil régional d'Aquitaine du 26 mars 2001, actualisé le 24 mars 2003 |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | EUR 0,65 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(7) of the Regulation | Yes | |
Date of implementation | 26.3.2001 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | General training | Yes |
Specific training | Yes |
Economic sectors concerned | All sectors eligible for training aid | Yes |
Limited to specific sectors | |
—Agriculture | |
—Fisheries and aquaculture | |
—Coalmining | |
—All manufacturing | |
or | |
Steel | |
Shipbuilding | |
Synthetic fibres | |
Motor vehicles | |
Other manufacturing | |
—All services | |
or | |
Maritime transport services | |
Other transport services | |
Financial services | |
Other services | |
Name and address of the granting authority | Name:Monsieur le Préfet de région |
Adresse:4B, Esplanade Charles de Gaulle33077 Bordeaux Cedex |
Large individual aid grants | In conformity with Article 5 of the Regulation | Yes | |
--------------------------------------------------
