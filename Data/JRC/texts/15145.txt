Commission Decision
of 20 January 2006
establishing a high-level advisory group on social integration of ethnic minorities and their full participation in the labour market
(2006/33/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Whereas:
(1) Article 13 of the Treaty establishing the European Community confers powers on the Community to take appropriate action to combat discrimination based on sex, racial or ethnic origin, religion or belief, disability, age or sexual orientation.
(2) Pursuant to the Communication from the Commission entitled "Non-discrimination and equal opportunities for all" [1] adopted on 1 June 2005, which highlights the need for the European Union to develop a coherent and effective approach to the social integration of ethnic minorities and their full participation in the labour market, the Commission wishes to draw on the expertise of specialists meeting in an advisory group.
(3) The group should contribute to developing a coherent and effective approach to the social integration of disadvantaged ethnic minorities and their full participation in the labour market.
(4) The group should comprise experts from civil society, the world of research, business, the national and local authorities, as well as ethnic minorities and other stakeholders. It should be balanced, particularly in terms of geographical origin, gender, ethnic origin, and area of activity and expertise,
HAS DECIDED AS FOLLOWS:
Article 1
The Commission hereby establishes a high-level advisory group on social integration of ethnic minorities and their full participation in the labour market, hereinafter called "the group".
Article 2
Task
The group's task will be:
- to analyse how to achieve better social integration of ethnic minorities and their full participation in the labour market within the European Union,
- to submit, before the end of the "2007 European Year of Equal Opportunities for All", a report containing recommendations on the policies to be implemented in this connection.
The group will build on good practice in this field, and will focus more particularly on the following questions:
- the socioeconomic situation of ethnic minorities in today's European Union,
- the different situations and needs of minority groups, including recent migrants, established ethnic minorities, national minorities, the Roma and stateless persons,
- the impact of multiple discrimination and the influence of factors such as age, sex, disability and religion, as well as the impact of geographical isolation and level of education,
- the contribution of European Union policies and programmes to social integration of ethnic minorities and their full participation in the labour market,
- the impact of future developments, including possible new waves of accessions to the European Union (Romania, Bulgaria, Turkey, western Balkans).
The Chairman of the group may indicate to the Commission when it would be advisable to consult the group on a specific question.
Article 3
Composition — Appointment
1. The members of the group shall be appointed by the Commission from among specialists with knowledge of the fields referred to in Article 2.
2. The group shall comprise a maximum of 10 members.
3. The following provisions shall apply:
- members shall be appointed in a personal capacity and be required to advise the Commission independently of any outside influence,
- members of the group shall remain in office until such time as they are replaced or their mandate runs out,
- members who are no longer able to contribute effectively to the group’s deliberations, who resign or who do not respect the conditions set out in the first or second point of this Article or Article 287 of the Treaty establishing the European Community may be replaced for the remaining period of their mandate,
- members shall each year sign an undertaking to act in the public interest and a declaration indicating the absence of any interest which may undermine their objectivity,
- the names of members appointed individually shall be published on the Internet site of the Employment, Social Affairs and Equal Opportunities DG and in the Official Journal of the European Union, series C. The names of members shall be collected, processed and published in accordance with the provisions of Regulation (EC) No 45/2001 of the European Parliament and of the Council [2] on the protection and processing of personal data.
Article 4
Operation
1. The Commission shall appoint the Chairman of the group.
2. The Commission's representative may ask experts or observers with specific competence on a subject on the agenda to participate in the group's deliberations where appropriate and/or necessary.
3. Information obtained by participating in the group's deliberations may not be divulged where the Commission states that this relates to confidential matters.
4. The group shall normally meet on Commission premises in accordance with the procedures and schedule established by it. The Commission shall provide secretarial services. Other Commission officials with an interest in the proceedings may attend these meetings.
5. The group shall adopt its rules of procedure on the basis of the standard rules of procedure adopted by the Commission [3].
6. The Commission may publish on the Internet, in the original language of the document concerned, any résumé, conclusion, partial conclusion or working document of the group.
Article 5
Meeting expenses
The Commission shall reimburse travel and, where appropriate, subsistence expenses for members, experts and observers in connection with the group’s activities in accordance with the provisions in force at the Commission. The members shall not be paid for their duties.
Meeting expenses shall be reimbursed within the limits of the appropriations allocated to the departments concerned under the annual procedure for allocating resources.
Article 6
Entry into force
This Decision shall take effect on the day of its publication in the Official Journal of the European Union. It shall be applicable until 31 December 2007. The Commission shall decide on a possible extension before that date.
Done at Brussels, 20 January 2006.
For the Commission
Vladimír Špidla
Member of the Commission
[1] COM(2005) 224 final.
[2] OJ L 8, 12.1.2001, p. 1.
[3] OJ C 38, 6.2.2001, p. 3.
--------------------------------------------------
