Council Regulation (EC) No 1105/2001
of 30 May 2001
amending Regulation (EEC) No 1911/91 on the application of the provisions of Community law to the Canary Islands
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to the Act of Accession of Spain and Portugal, and in particular the first subparagraph of Article 25(4) thereof,
Having regard to the proposal from the Commission(1),
Having regard to the Opinion of the European Parliament(2),
Having regard to the Opinion of the Economic and Social Committee(3),
Whereas:
(1) Article 5(1) of Regulation (EEC) No 1911/91(4) established a transitional period which authorised the Spanish authorities to impose a tax on production and imports (APIM) on all products entering or produced in the Canary Islands.
(2) Article 6(1) of that Regulation established a transitional period to introduce progressively the Common Customs Tariff (CCT) in the Canary Islands.
(3) Both transitional periods expired on 31 December 2000.
(4) In October and November 2000 the Spanish authorities requested that the said transitional periods and the measures introduced on the basis of Regulation (EEC) No 1911/91 be extended.
(5) In July and October 2000, the Spanish authorities gave notification of a new tax which would apply to the Canary Islands and is intended to compensate for the handicaps referred to in Article 299(2) of the Treaty.
(6) The request contained documentation which indicated that even though the economic situation on the Canary Islands had improved during the transitional period, the full integration of the region would lead to a decline in industrial and commercial activity, and thus in employment, in the different sectors concerned.
(7) However, in the short period available it was not possible to evaluate the full impact of the termination or modification of the existing measures on the economic and social situation of the Canary Islands.
(8) In order to ensure that the economic operators concerned are afforded a certain continuity in the legal framework affecting their business, it is appropriate to extend the said transitional periods by one year.
(9) Once the abovementioned evaluation has been completed, the Commission will make, if necessary, a new proposal taking into account the aims of Article 299(2) of the Treaty,
HAS ADOPTED THIS REGULATION:
Article 1
In Article 5(1) and (6) and Article 6(1) of Regulation (EEC) No 1911/91 the date "31 December 2000" shall be replaced by "31 December 2001".
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
It shall apply from 1 January 2001.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 30 May 2001.
For the Council
The President
B. Lejon
(1) Proposal of 9 February 2001 (not yet published in the Official Journal).
(2) Opinion delivered on 3 April 2001 (not yet published in the Official Journal).
(3) Opinion delivered on 1 April 2001 (not yet published in the Official Journal).
(4) OJ L 171, 29.6.1991, p. 1. Regulation as last amended by Regulation (EC) No 2674/99 (OJ L 326, 18.12.1988, p. 3).
