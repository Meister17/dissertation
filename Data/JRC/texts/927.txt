Commission Decision
of 20 August 2001
amending Decision 95/454/EC laying down special conditions governing imports of fishery and aquaculture products originating in the Republic of Korea
(notified under document number C(2001) 2554)
(Text with EEA relevance)
(2001/641/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/493/EEC of 22 July 1991 laying down the health conditions for the production and the placing on the market of fishery products(1), as last amended by the Directive 97/79/EC(2), and in particular Article 11(5) thereof,
Whereas:
(1) Article 1 of Commission Decision 95/454/EC of 23 October 1995 laying down special conditions governing imports of fishery and aquaculture products originating in the Republic of Korea(3), as last amended by Decision 1999/401/EC(4), states that the "Ministry of Maritime Affairs and Fisheries - National Fisheries Products Inspection Station (NFPIS)" shall be the competent authority in the Republic of Korea for verifying and certifying compliance of fishery and aquaculture products with the requirements of Directive 91/493/EEC.
(2) Following a restructuring of the Korean administration, the competent authority for issuing health certificates for fishery products has changed to the "National Fisheries Products Quality Inspection Service (NFPQIS)". This new authority is capable of effectively verifying the application of the laws in force. It is therefore necessary to modify the nomination of the Competent Authority mentioned in Decision 95/454/EC and the model of health certificate included in Annex A to this Decision.
(3) The measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
Decision 95/454/EC is amended as follows:
1. Article 1 is replaced by the following: "Article 1
The 'National Fisheries Products Quality Inspection Service (NFPQIS)' shall be the competent authority in the Republic of Korea for verifying and certifying compliance of fishery and aquaculture products with the requirements of Directive 91/493/EEC.";
2. point 2 of Article 3 is replaced by the following: "2. Certificates must bear the name, capacity and signature of the representative of the NFPQIS and the latter's official stamp in a colour different from that of other endorsements.";
3. Annex A is replaced by the Annex hereto.
Article 2
This Decision shall come into effect after 45 days of its publication in the Official Journal of the European Communities.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 20 August 2001.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 268, 24.9.1991, p. 15.
(2) OJ L 24, 30.1.1998, p. 31.
(3) OJ L 264, 7.11.1995, p. 37.
(4) OJ L 151, 18.6.1999, p. 27.
ANNEX
"ANNEX A
>PIC FILE= "L_2001224EN.001203.TIF">
>PIC FILE= "L_2001224EN.001301.TIF">"
