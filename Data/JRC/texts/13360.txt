Joint Declaration on political dialogue between the European Union and Montenegro [1]
(2006/C 242/02)
Based on the commitments undertaken at the EU-Western Balkans Summit held in Thessaloniki on 21 June 2003, the European Union and Montenegro (hereinafter referred to as "the Parties") express their resolution to reinforce and intensify their mutual relations in the political fields.
Accordingly, the Parties agree to establish a regular political dialogue which will accompany and consolidate their rapprochement, support the political and economic changes underway in Montenegro, and contribute to establish new forms of cooperation, in particular taking into account Montenegro's status as a potential candidate for European Union membership.
The political dialogue, based on shared values and aspirations, will aim at:
1. Reinforcing democratic principles and institutions as well as rule of law, human rights and respect for and protection of minorities;
2. Promoting regional cooperation, development of good neighbourly relations and fulfilment of obligations under international law, including full and unequivocal cooperation with the ICTY;
3. Facilitating the integration of Montenegro to the fullest possible extent into the political and economic mainstream of Europe based on its individual merits and achievements;
4. Increasing convergence of positions between the Parties on international issues, and on those matters likely to have substantial effects on the Parties, including cooperation in the fight against terrorism, organised crime and corruption, and in other areas in the field of justice and home affairs;
5. Enabling each Party to consider the position and interests of the other Party in their respective decision making process;
6. Enhancing security and stability in the whole of Europe and, in particular, in South-Eastern Europe, through cooperation in the areas covered by the Common Foreign and Security Policy of the European Union.
The political dialogue between the Parties will take place through regular consultations, contacts and exchange of information as appropriate, in particular in the following formats:
1. High-level meetings between representatives of Montenegro on the one hand, and representatives of the European Union, in the Troika format, on the other;
2. Providing mutual information on foreign policy decisions taking full advantage of diplomatic channels, including contacts at the bilateral level in third countries as well as within multilateral fora such as the United Nations, OSCE and other international organisations;
3. Contacts at parliamentary level;
4. Any other means which would contribute to consolidating, and developing dialogue between the Parties.
Political dialogue will also take place within the framework of the EU-Western Balkans Forum, the high level multilateral political forum established at the EU-Western Balkans Summit held in Thessaloniki.
[1] Text adopted by the Council on 15 September 2006.
--------------------------------------------------
