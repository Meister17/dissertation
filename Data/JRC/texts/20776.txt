REGULATION (EURATOM, ECSC, EEC) No 549/69 OF THE COUNCIL of 25 March 1969 determining the categories of officials and other servants of the European Communities to whom the provisions of Article 12, the second paragraph of Article 13 and Article 14 of the Protocol on the Privileges and Immunities of the Communities apply
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing a Single Council and a Single Commission of the European Communities, and in particular the first paragraph of Article 28 thereof;
Having regard to the Protocol on the Privileges and Immunities of the European Communities, and in particular Articles 16 and 22 thereof;
Having regard to the proposal from the Commission;
Having regard to the Opinion of the European Parliament (1);
Having regard to the Opinion of the Court of Justice of the European Communities;
Whereas the privileges, immunities and facilities conferred on officials and other servants of the Communities by the Protocol on the Privileges and Immunities are granted solely in the interest of the Communities;
Whereas it is therefore important to ensure that officials and other servants, in view of their duties and responsibilities and of their particular situation, benefit from such privileges, immunities and facilities as are necessary for the proper functioning of the Communities;
HAS ADOPTED THIS REGULATION:
Article 1
The provisions of Article 12 of the Protocol on the Privileges and Immunities of the Communities shall apply to the following categories: (a) officials coming under the Staff Regulations of Officials of the Communities, with the exception of officials placed on non-active status, to whom only Article 12 (a) and, in respect of allowances paid by the Communities, Article 12 (c) shall apply;
(b) staff coming under the Conditions of Employment of Other Servants of the Communities, with the exception of: 1. Local staff, to whom only Article 12 (a) shall apply;
2. part-time auxiliary staff, to whom only Article 12 (a) and (b) and, in respect of remuneration paid by the Communities, Article 12 (c) shall apply.
Article 2
The provisions of the second paragraph of Article 13 of the Protocol on the Privileges and Immunities of the Communities shall apply to the following categories: (a) persons coming under the Staff Regulations of Officials or the Conditions of Employment of Other Servants of the Communities, including those who receive the compensation provided for in the case of retirement in the interests of the service, with the exception of local staff;
(b) persons receiving disability, retirement or survivors' pensions paid by the Communities;
(c) persons receiving the compensation provided for in Article 5 of Regulation (EEC, Euratom, ECSC) No 259/68 (2) in the case of termination of service. (1) OJ No C 135, 14.12.1968, p. 31. (2) OJ No L 56, 4.3.1968, p. 1.
Article 3
The provisions of Article 14 of the Protocol on the Privileges and Immunities of the Communities shall apply to the following categories: (a) officials coming under the Staff Regulations of Officials of the Communities;
(b) staff coming under the Conditions of Employment of Other Servants of the Communities, with the exception of local staff.
Article 4
Without prejudice to the provisions of the first paragraph of Article 22 of the Protocol on the Privileges and Immunities of the European Community concerning members of the organs of the European Investment Bank, the privileges and immunities provided for in Article 12, in the second paragraph of Article 13 and in Article 14 of the Protocol shall apply under the same conditions and within the same limits as those laid down in Articles 1, 2 and 3 of this Regulation to: - staff of the European Investment Bank;
- persons receiving disability, retirement or survivors' pensions paid by the European Investment Bank.
Article 5
Regulation No 8/63 Euratom, 127/63/EEC (1) is hereby repealed.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 25 March 1969.
For the Council
The President
G. THORN (1) OJ No 181, 11.12.1963, p. 2880/63.
