COMMISSION DECISION of 21 June 1996 amending Chapter 7 of Annex I to Council Directive 92/118/EEC laying down animal health and public health requirements governing trade in and imports into the Community of products not subject to the said requirements laid down in specific Community rules referred to in Annex A, Chapter I to Directive 89/662/EEC and, as regards pathogens, to Directive 90/425/EEC (Text with EEA relevance) (96/405/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 92/118/EEC of 17 December 1992 laying down animal health and public health requirements governing trade in and imports into the Community of products not subject to the said requirements laid down in specific Community rules referred to in Annex A, Chapter I to Directive 89/662/EEC and, as regards pathogens, to Directive 90/425/EEC (1), as last amended by Decision 96/340/EC (2), and in particular the second paragraph of Article 15 thereof,
Whereas application of the rules laid down has led to certain difficulties with the import of blood and blood products of animal origin not intended for human consumption;
Whereas more detailed rules should be laid down regarding the various categories of blood products of animal origin;
Whereas, for reasons of clarity, Chapter 7 of Annex I to Directive 92/118/EEC should be redrafted;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
Chapter 7 of Annex I to Directive 92/118/EEC is hereby replaced by the Annex hereto.
Article 2
This Decision shall apply from 1 July 1996.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 21 June 1996.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 62, 15. 3. 1993, p. 49.
(2) OJ No L 129, 30. 5. 1996, p. 35.
ANNEX
'CHAPTER 7
Blood and blood products of ungulates and poultry
(with the exception of serum from equidae)
I. Fresh blood and blood products intended for human consumption
A. Trade
1. Trade in fresh blood of ungulates and poultry intended for human consumption is subject to the animal health conditions applicable to trade in fresh meat pursuant to Council Directives 72/461/EEC (1), 91/494/EEC (2) or 91/495/EEC (3).
2. Trade in blood products intended for human consumption is subject to the animal health conditions laid down in Chapter 11 of this Directive.
B. Imports
1. Imports of fresh blood of domestic ungulates intended for human consumption are prohibited pursuant to Council Directive 72/462/EEC (4).
Imports of fresh blood of domestic poultry intended for human consumption are subject to the animal health conditions laid down in Directive 91/494/EEC.
Imports of fresh blood of reared game intended for human consumption are subject to the animal health conditions laid down in Chapter 11 of this Annex.
2. Imports of blood products for human consumption, including those referred to in Council Directive 77/99/EEC (5), are subject to the animal health conditions applicable to meat products pursuant to Directive 72/462/EEC and this Directive, without prejudice to the rules on blood-based processed animal protein products referred to in Chapter 6 of this Annex.
II. Fresh blood and blood products not intended for human consumption
A. Definitions
For the purposes of this point, the following definitions shall apply:
blood:
- whole blood defined as "low-risk material" within the meaning of Directive 90/667/EEC;
blood products:
- fractions of blood which may have undergone treatment other than that provided for in Directive 90/667/EEC,
or
- blood which has undergone treatment other than that provided for in Directive 90/667/EEC;
products used for in vitro diagnosis:
- a packaged product, ready for use by the end user, containing a blood product, and used as a reagent, reagent product, calibrator, kit or any other system, whether used alone or in combination, intended to be used in vitro for the examination of samples of human or animal origin, with the exception of donated organs or blood, solely or principally with a view to the diagnosis of a physiological state, state of health, disease or genetic abnormality or to determine safety and compatibility with reagents;
laboratory reagent:
- a packaged product, ready for use by the end user, containing a blood product, and intended for laboratory use as a reagent or reagent product, whether used alone or in combination;
(1) OJ No L 302, 31. 12. 1972, p. 24.
(2) OJ No L 268, 24. 9. 1991, p. 35.
(3) OJ No L 268, 24. 9. 1991, p. 41.
(4) OJ No L 302, 31. 12. 1972, p. 28.
(5) OJ No L 26, 31. 1. 1977, p. 85.
full treatment:
- heat treatment at a temperature of 65 °C for at least three hours, followed by an effectiveness check,
or
- irradiation at 2,5 megarads or by gamma rays, followed by an effectiveness check,
or
- change in pH to pH 5 for two hours, followed by an effectiveness check,
or
- the treatment provided for in Chapter 4 of this Annex,
or
- any other treatment or process to be laid down in accordance with the procedure laid down in Article 18.
B. Trade
Trade in blood and blood products is subject to the animal health conditions laid down in Chapter II of this Directive and to the conditions laid down in Directive 90/667/EEC.
C. Imports
1. Imports of blood are subject to the animal health conditions laid down in Chapter 10 of this Annex.
2. (a) Imports of blood products are authorized provided that each consignment is accompanied by a certificate, the form of which is to be fixed pursuant to the procedure laid down in Article 18, certifying that:
- the products originate in a third country in which no case of foot-and-mouth disease has been recorded within at least 24 months and no case of vesicular stomatitis, swine vesicular disease, rinderpest, peste des petits ruminants, Rift Valley fever, blue tongue, African horse sickness, classical swine fever, African swine fever, Newcastle disease or avian influenza has been recorded for 12 months in the susceptible species and in which vaccination has not been carried out against those diseases for at least 12 months. The health certificate may be made out according to the species of animal from which the blood products are derived,
or
- in the case of blood products derived from bovine animals, they originate in an area of a third country fulfilling the conditions set out in the first indent from which imports of bovine animals, their fresh meat or their sperm are authorized pursuant to Community legislation. The blood from which such products are manufactured must be from bovine animals from that area of the third country and must have been collected:
- in slaughterhouses approved in accordance with Community legislation,
or
- in slaughterhouses approved and supervised by the competent authorities of the third country. The Commission and Member States must be notified of the address and approval number of such slaughterhouses,
or
- in the case of blood products derived from bovine animals, they have undergone full treatment guaranteeing the absence of pathogens of the bovine diseases referred to in the first indent,
or
- in the case of blood products derived from bovine animals, they fulfil the conditions laid down in Chapter 10 of this Annex. In such cases, the packaging may not be opened during storage and the processing undertaking must carry out full treatment of the products concerned.
(b) The specific conditions relating to imports of products for use in in vitro diagnosis and laboratory reagents shall be established, where necessary, in accordance with the procedure laid down in Article 18.
III. General provisions
The detailed rules for the application of this Chapter are to be adopted, where necessary, in accordance with the procedure laid down in Article 18.`
