Commission Regulation (EC) No 1668/2006
of 10 November 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 11 November 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 10 November 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 10 November 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 86,5 |
096 | 30,1 |
204 | 44,7 |
999 | 53,8 |
07070005 | 052 | 116,3 |
204 | 49,7 |
220 | 155,5 |
628 | 196,3 |
999 | 129,5 |
07099070 | 052 | 101,8 |
204 | 147,8 |
999 | 124,8 |
08052010 | 204 | 84,0 |
999 | 84,0 |
08052030, 08052050, 08052070, 08052090 | 052 | 67,8 |
400 | 84,2 |
528 | 40,7 |
624 | 86,7 |
999 | 69,9 |
08055010 | 052 | 63,9 |
388 | 46,8 |
524 | 56,1 |
528 | 39,6 |
999 | 51,6 |
08061010 | 052 | 111,3 |
400 | 211,5 |
508 | 268,1 |
999 | 197,0 |
08081080 | 388 | 79,1 |
400 | 106,4 |
720 | 73,5 |
800 | 160,8 |
999 | 105,0 |
08082050 | 052 | 83,1 |
400 | 216,1 |
720 | 83,9 |
999 | 127,7 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
