Council Regulation (EC) No 1210/2003
of 7 July 2003
concerning certain specific restrictions on economic and financial relations with Iraq and repealing Regulation (EC) No 2465/96
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Articles 60 and 301 thereof,
Having regard to Common Position 2003/495/CFSP on Iraq and repealing Common Positions 1996/741/CFSP and 2002/599/CFSP(1),
Having regard to the proposal from the Commission,
Whereas:
(1) Further to United Nations Security Council Resolution 661 (1990) and subsequent relevant Resolutions, in particular Resolution 986 (1995), the Council imposed a comprehensive embargo on trade with Iraq. This embargo is at present laid down in Council Regulation (EC) No 2465/96 of 17 December 1996 concerning the interruption of economic and financial relations between the European Community and Iraq(2).
(2) In its Resolution 1483 (2003) of 22 May 2003, the Security Council decided that, with certain exceptions, all prohibitions related to trade with Iraq and the provision of financial or economic resources to Iraq should no longer apply.
(3) With the exception of a ban on exports of arms and related materiel to Iraq, the Resolution provides that the comprehensive restrictions concerning trade should be repealed and replaced with specific restrictions applying to proceeds from all export sales of petroleum, petroleum products, and natural gas from Iraq, and to trade in goods belonging to Iraq's cultural heritage with the objective of facilitating the safe return of those goods.
(4) The Resolution also states that certain funds and economic resources, in particular those belonging to former Iraqi President Saddam Hussein and senior officials of his regime, should be frozen, subject to designation by the Committee of the Security Council established pursuant to paragraph 6 of Resolution 661 (1990), and that such funds should subsequently be transferred to the Development Fund for Iraq.
(5) In order to allow the Member States to effect the transfer of frozen funds, economic resources and proceeds of economic resources to the Development Fund for Iraq, provision should be made for such funds and economic resources to be unfrozen.
(6) The Resolution stipulates that all petroleum, petroleum products and natural gas exported by Iraq, as well as the payments for such goods, should be exempt from legal proceedings, attachment, garnishment and execution by those having claims against Iraq. This temporary measure is necessary in order to promote the economic reconstruction of Iraq and the restructuring of its debt, which will help remove the threat to international peace and security constituted by the current situation in Iraq in the common interest of the international community and in particular the Community and its Member States.
(7) Common Position 2003/495/CFSP makes provision for an amendment of the current Community regime in order to align it with UNSC Resolution 1483 (2003).
(8) These measures fall under the scope of the Treaty and, therefore, in particular with a view to avoiding distortion of competition, Community legislation is necessary to implement the relevant decisions of the Security Council as far as the territory of the Community is concerned. For the purposes of this Regulation, the territory of the Community is deemed to encompass the territories of the Member States to which the Treaty is applicable, under the conditions laid down in that Treaty.
(9) In order to create maximum legal certainty within the Community, the names and other relevant data relating to natural or legal persons, groups or entities identified by the UN authorities whose funds and economic resources are to be frozen, should be published and a procedure established within the Community to amend these lists.
(10) For reasons of expediency, the Commission should be empowered to amend the Annexes to this Regulation setting out the list of cultural goods, the lists of persons, bodies and entities, whose funds and economic resources are to be frozen, and the list of competent authorities.
(11) The competent authorities of the Member States should, where necessary, be empowered to ensure compliance with the provisions of this Regulation.
(12) The Commission and Member States should inform each other of the measures taken under this Regulation and of other relevant information at their disposal in connection with this Regulation, and should cooperate with the Committee established by UNSC Resolution 661 (1990), in particular by supplying it with information.
(13) Member States should lay down rules on sanctions applicable to infringements of the provisions of this Regulation and ensure that they are implemented. Those sanctions should be effective, proportionate and dissuasive.
(14) Since the comprehensive trade measures of Regulation (EC) No 2465/96 are being replaced by the specific trade restrictions of this Regulation, and this Regulation imposes freezing measures which require immediate application by economic operators, it is necessary to ensure that sanctions for breaches of this Regulation can be imposed as soon as it enters into force.
(15) For the sake of clarity, Regulation (EC) No 2465/96 should be repealed in its entirety.
(16) Council Regulation (EEC) No 3541/92 of 7 December 1992 prohibiting the satisfying of Iraqi claims with regard to contracts and transactions, the performance of which was affected by United Nations Security Council Resolution 661 (1990) and related resolutions(3) should remain in force,
HAS ADOPTED THIS REGULATION:
Article 1
For the purpose of this Regulation, the following definitions shall apply:
1. "Sanctions Committee" means: the Committee of the Security Council of the United Nations which was established pursuant to paragraph 6 of Resolution 661 (1990);
2. "funds" means financial assets and economic benefits of every kind, including but not limited to:
(a) cash, cheques, claims on money, drafts, money orders and other payment instruments;
(b) deposits with financial institutions or other entities, balances on accounts, debts and debt obligations;
(c) publicly- and privately-traded securities and debt instruments, including stocks and shares, certificates representing securities, bonds, notes, warrants, debentures and derivatives contracts;
(d) interest, dividends or other income on or value accruing from or generated by assets;
(e) credit, right of set-off, guarantees, performance bonds or other financial commitments;
(f) letters of credit, bills of lading, bills of sale;
(g) documents evidencing an interest in funds or financial resources,
(h) any other instrument of export-financing;
3. "economic resources" means assets of every kind, whether tangible or intangible, movable or immovable, which are not funds but can be used to obtain funds, goods or services;
4. "freezing of funds" means preventing any move, transfer, alteration, use of or dealing with funds in any way that would result in any change in their volume, amount, location, ownership, possession, character, destination or other change that would enable the use of the funds, including portfolio management;
5. "freezing of economic resources" means preventing their use to obtain funds, goods or services in any way, including, but not limited to, by selling, hiring or mortgaging them;
6. "Development Fund for Iraq" means the Development Fund for Iraq held by the Central Bank of Iraq.
Article 2
All proceeds from all export sales of petroleum, petroleum products, and natural gas from Iraq, as listed in Annex I, as of 22 May 2003 shall be deposited into the Development Fund for Iraq under the conditions set out in UNSC Resolution 1483 (2003) and in particular paragraphs 20 and 21 thereof, until such time as an internationally recognized, representative government of Iraq is properly constituted.
Article 3
1. The following shall be prohibited:
(a) the import of or the introduction into the territory of the Community of,
(b) the export of or removal from the territory of the Community of, and
(c) the dealing in, Iraqi cultural property and other items of archaeological, historical, cultural, rare scientific and religious importance including those items listed in Annex II, if they have been illegally removed from locations in Iraq, in particular, if:
(i) the items form an integral part of either the public collections listed in the inventories of Iraqi museums, archives or libraries' conservation collection, or the inventories of Iraqi religious institutions, or
(ii) there exists reasonable suspicion that the goods have been removed from Iraq without the consent of their legitimate owner or have been removed in breach of Iraq's laws and regulations.
2. These prohibitions shall not apply if it is shown that either:
(a) the cultural items were exported from Iraq prior to 6 August 1990; or
(b) the cultural items are being returned to Iraqi institutions in accordance with the objective of safe return as set out in paragraph 7 of UNSC Resolution 1483 (2003).
Article 4
1. All funds and economic resources located outside Iraq on or after 22 May 2003 of the previous Government of Iraq, or of any of the public bodies, corporations, including companies established under private law in which the public authorities have a majority stake, and agencies of that Republic, identified by the Sanctions Committee and listed in Annex III, shall be frozen.
2. All funds and economic resources belonging to, or owned or held by the following persons, identified by the Sanctions Committee and listed in Annex IV, shall be frozen:
(a) former President Saddam Hussein;
(b) senior officials of his regime;
(c) immediate members of their families; or
(d) legal persons, bodies or entities owned or controlled directly or indirectly by the persons referred to in subparagraphs (a), (b) and (c) or by any natural or legal person acting on their behalf or at their direction.
3. No funds shall be made available, directly or indirectly, to, or for the benefit of, a natural or legal person, body or entity listed in Annexes III and IV.
4. No economic resources shall be made available, directly or indirectly, to, or for the benefit of, a natural or legal person, body or entity listed in Annexes III and IV, so as to enable that person, group or entity to obtain funds, goods or services.
Article 5
1. The crediting of frozen accounts shall be allowed on the condition that any additions shall be frozen.
2. This Regulation shall not require the freezing of a transfer of funds to a beneficiary in the Community by or through an Iraqi bank meeting the conditions of Article 4(1), if such transfer constitutes a payment for goods and services ordered by customers of that bank. It shall not restrict the validity and use of the guarantees and letters of credit issued by Iraqi banks meeting the conditions of Article 4(1), at the request of their customers, with a view to payment for goods or services which such customers concerned have ordered in the Community.
Article 6
Funds, economic resources and proceeds of economic resources frozen pursuant to Article 4 shall only be unfrozen for the purpose of their transfer to the Development Fund for Iraq held by the Central Bank of Iraq, under the conditions set out in UNSC Resolution 1483 (2003).
Article 7
1. The participation, knowingly and intentionally, in activities, the object or effect of which is, directly or indirectly, to circumvent Article 4 or to promote the transactions referred to in Articles 2 and 3, shall be prohibited.
2. Any information that the provisions of this Regulation are being, or have been, circumvented shall be notified to the competent authorities of the Member States, as listed in Annex V, and, directly or through these competent authorities, to the Commission.
Article 8
1. Without prejudice to the applicable rules concerning reporting, confidentiality and professional secrecy and to the provisions of Article 284 of the Treaty, natural and legal persons, entities and bodies shall:
(a) provide immediately any information which would facilitate compliance with this Regulation, such as accounts and amounts frozen in accordance with Article 4, to the competent authorities of the Member States listed in Annex V where they are resident or located, and, directly or through these competent authorities, to the Commission;
(b) cooperate with the competent authorities listed in Annex V in any verification of this information.
2. Any information provided or received in accordance with this Article shall be used only for the purposes for which it was provided or received.
3. Any additional information directly received by the Commission shall be made available to the competent authorities of the Member States concerned.
Article 9
The freezing of funds and economic resources, carried out in good faith that such action is in accordance with this Regulation, shall not give rise to liability of any kind on the part of the natural or legal person or entity implementing it, or its directors or employees, unless it is proved that the funds and economic resources were frozen as result of negligence.
Article 10
1. The following shall be immune from legal proceedings and shall not be subjected to any form of attachment, garnishment or execution:
(a) petroleum, petroleum products and natural gas originating in Iraq, until title to such goods has been passed to a purchaser;
(b) proceeds and obligations arising from the sale of petroleum, petroleum products and natural gas originating in Iraq, including payments for such goods deposited into the Development Fund for Iraq held by the Central Bank of Iraq;
(c) funds and economic resources frozen pursuant to Article 4;
(d) the Development Fund for Iraq held by the Central Bank of Iraq.
2. By way of derogation from paragraph 1, the proceeds and obligations arising from the sale of petroleum, petroleum products and natural gas originating in Iraq, and the Development Fund for Iraq shall not be immune from claims made on the basis of Iraq's liability for damages in connection with any ecological accident occurring after 22 May 2003.
Article 11
The Commission shall be empowered to:
(a) amend Annex II as necessary;
(b) amend or supplement Annexes III and IV on the basis of determinations made by either the United Nations Security Council or the Sanctions Committee; and
(c) amend Annex V on the basis of information supplied by Member States.
Article 12
Without prejudice to the rights and obligations of the Member States under the Charter of the United Nations, the Commission shall maintain all necessary contacts with the Sanctions Committee for the purpose of the effective implementation of this Regulation.
Article 13
The Commission and the Member States shall immediately inform each other of any measures taken pursuant to this Regulation. They shall supply each other with relevant information at their disposal in connection with this Regulation, in particular information received in accordance with Article 8 and information relating to breaches of the provisions of this Regulation, enforcement problems and judgements handed down by national courts.
Article 14
This Regulation shall apply notwithstanding any rights conferred or obligations imposed by any international agreement signed or any contract entered into or any licence or permit granted before the entry into force of this Regulation.
Article 15
1. The Member States shall lay down the rules on sanctions applicable to infringements of the provisions of this Regulation and shall take all measures necessary to ensure that they are implemented. The sanctions provided for must be effective, proportionate and dissuasive.
2. Pending the adoption, where necessary, of any legislation to that end, the sanctions to be imposed where the provisions of this Regulation are infringed shall, where relevant, be those determined by the Member States in order to give effect to Article 7(3) of Regulation (EC) No 2465/96.
3. Each Member State shall be responsible for bringing proceedings against any natural or legal person, group or entity under its jurisdiction, in cases of breach of any of the restrictive measures laid down in this Regulation by any such person, group or entity.
Article 16
This Regulation shall apply:
(a) within the territory of the Community, including its airspace;
(b) on board any aircraft or any vessel under the jurisdiction of a Member State;
(c) to any person elsewhere who is a national of a Member State;
(d) to any legal person, group or entity which is incorporated or constituted under the law of a Member State;
(e) to any legal person, group or entity doing business within the Community.
Article 17
Regulation (EC) No 2465/96 is hereby repealed.
Article 18
1. This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union.
2. Save for Articles 4 and 6, it shall apply from 23 May 2003.
3. Article 10 shall apply until 31 December 2007.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 7 July 2003.
For the Council
The President
F. Frattini
(1) See page 72 of this Official Journal.
(2) OJ L 337, 27.12.1996, p. 1. Regulation as last amended by Commission Regulation (EC) No 208/2003 (OJ L 28, 4.2.2003, p. 26).
(3) OJ L 361, 10.12.1992, p. 1.
ANNEX I
List of goods referred to in Article 2
>TABLE>
ANNEX II
List of goods referred to in Article 3
>TABLE>
ANNEX III
List of public bodies, corporations and agencies and natural and legal persons, bodies and entities of the previous government of Iraq referred to in Article 4(1), (3) and (4)
p.m.
ANNEX IV
List of natural and legal persons, bodies or entities associated with the regime of former President Saddam Hussein referred to in Article 4(2), (3) and (4)
1. NAME: Saddam Hussein Al-Tikriti
ALIAS: Abu Ali
DATE OF BIRTH/PLACE OF BIRTH: 28 April 1937, al-Awja, near Tikrit
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Named in 1483
2. NAME: Qusay Saddam Hussein Al-Tikriti
DATE OF BIRTH/PLACE OF BIRTH: 1965 or 1966, Baghdad
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Saddam's second son;
Oversaw Special Republican Guard, Special Security Organisation, and Republican Guard
3. NAME: Uday Saddam Hussein Al-Tikriti
DATE OF BIRTH/PLACE OF BIRTH: 1964 or 1967, Baghdad
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Saddam's eldest son;
Leader of Paramilitary Organisation Fedayeen Saddam
4. NAME: Abid Hamid Mahmud Al-Tikriti
ALIAS: Abid Hamid Bid Hamid Mahmud
Col Abdel Hamid Mahmoud
Abed Mahmoud Hammud
DATE OF BIRTH/PLACE OF BIRTH: Circa 1957, al-Awja, near Tikrit
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Saddam's Presidential Secretary and Key Advisor
5. NAME: Ali Hassan Al-Majid Al-Tikriti
ALIAS: Al-Kimawi
DATE OF BIRTH/PLACE OF BIRTH: 1943, al-Awja, near Tikrit, Iraq
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Presidential Advisor and Senior Member of Revolutionary Command Council
6. NAME: Izzat Ibrahim al-Duri
ALIAS: Abu Brays
Abu Ahmad
DATE OF BIRTH/PLACE OF BIRTH: 1942, al-Dur
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Deputy Commander-in-Chief of Iraqi Military,
Deputy Secretary, Ba'th Party Regional Command,
Vice Chairman, Revolutionary Command Council
7. NAME: Hani Abd-Al-Latif Tilfah Al-Tikriti
DATE OF BIRTH/PLACE OF BIRTH: Circa 1962, al-Awja, near Tikrit
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
No 2 in Special Security Organisation
8. NAME: Aziz Salih al-Numan
DATE OF BIRTH/PLACE OF BIRTH: 1941 or 1945, An Nasiriyah
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Ba'th Party Regional Command Chairman;
Former Governor of Karbala and An Najaf;
Former Minister of Agriculture and Agrarian Reform (1986-1987)
9. NAME: Muhammad Hamza Zubaidi
DATE OF BIRTH/PLACE OF BIRTH: 1938, Babylon, Babil
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Former Prime Minister
10. NAME: Kamal Mustafa Abdallah
ALIAS: Kamal Mustafa Abdallah Sultan al-Tikriti
DATE OF BIRTH/PLACE OF BIRTH: 1952 or 4 May 1955, Tikrit
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Republican Guard Secretary;
Led Special Republican Guards and commanded both Republican Guard Corps
11. NAME: Barzan Abd al-Ghafur Sulaiman Majid Al-Tikriti
ALIAS: Barzan Razuki Abd al-Ghafur
DATE OF BIRTH/PLACE OF BIRTH: 1960, Salah al-Din
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Commander, Special Republican Guard
12. NAME: Muzahim Sa'b Hassan Al-Tikriti
DATE OF BIRTH/PLACE OF BIRTH: Circa 1946 or 1949 or 1960, Salah al-Din or al-Awja near Tikrit
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Led Iraq's Air Defence Forces;
Deputy Director of Organisation of Military Industrialisation
13. NAME: Ibrahim Ahmad Abd al-Sattar Muhammed Al-Tikriti
DATE OF BIRTH/PLACE OF BIRTH: 1950, Mosul
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Armed Forces Chief of Staff
14. NAME: Saif-al-Din Fulayyih Hassan Taha Al-Rawi
ALIAS: Ayad Futayyih Al-Rawi
DATE OF BIRTH/PLACE OF BIRTH: 1953, Ramadi
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Republican Guard Chief of Staff
15. NAME: Rafi Abd-al-Latif Tilfah Al-Tikriti
DATE OF BIRTH/PLACE OF BIRTH: Circa 1954, Tikrit
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Director of Directorate of General Security
16. NAME: Tahir Jalil Habbush Al-Tikriti
DATE OF BIRTH/PLACE OF BIRTH: 1950, Tikrit
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Director of Iraqi Intelligence Services;
Head of Directorate of General Security 1997 to 1999
17. NAME: Hamid Raja Shalah Al-Tikriti
ALIAS: Hassan Al-Tikriti; Hamid Raja-Shalah Hassum Al-Tikriti;
DATE OF BIRTH/PLACE OF BIRTH: 1950, Bayji, Salah al-Din Governorate
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Air Force Commander
18. NAME: Latif Nusayyif Jasim Al-Dulaymi
DATE OF BIRTH/PLACE OF BIRTH: Circa 1941, ar-Rashidiyah, suburb of Baghdad
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Ba'th Party Military Bureau Deputy Chairman;
Labour and Social Affairs Minister (1993 to 1996)
19. NAME: Abd-al-Tawwab Mullah Huwaysh
DATE OF BIRTH/PLACE OF BIRTH: Circa 1957 or 14 March 1942, either Mosul or Baghdad
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Deputy Prime Minister;
Director of Organisation of Military Industrialisation
20. NAME: Taha Yassin Ramadan Al-Jizrawi
DATE OF BIRTH/PLACE OF BIRTH: Circa 1938, Mosul
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Vice President since 1991
21. NAME: Rukan Razuki Abd-al-Ghafur Sulaiman Al-Tikriti
ALIAS: Rukan Abdal-Ghaffur Sulayman al-Majid;
Rukan Razuqi Abd al-Ghafur Al-Majid;
Rukan Abd al-Ghaffur al-Majid Al-Tikriti Abu Walid;
DATE OF BIRTH/PLACE OF BIRTH: 1956, Tikrit
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Head of Tribal Affairs in Presidential Office
22. NAME: Jamal Mustafa Abdallah Sultan Al-Tikriti
DATE OF BIRTH/PLACE OF BIRTH: 4 May 1955, al-Samnah, near Tikrit
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Deputy Head of Tribal Affairs in Presidential Office
23. NAME: Mizban Khadr Hadi
DATE OF BIRTH/PLACE OF BIRTH: 1938, Mandali District, Diyala
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Member, Ba'th Party Regional Command and Revolutionary Command Council since 1991
24. NAME: Taha Muhyi-al-Din Ma'ruf
DATE OF BIRTH/PLACE OF BIRTH: 1924, Sulaymaniyah
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Vice President, Revolutionary Command Council
25. NAME: Tariq Aziz
ALIAS: Tariq Mikhail Aziz
DATE OF BIRTH/PLACE OF BIRTH: 1 July 1936, Mosul or Baghdad
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Deputy Prime Minister;
PASSPORT: (July 1997): No 34409/129
26. NAME: Walid Hamid Tawfiq Al-Tikriti
ALIAS: Walid Hamid Tawfiq al-Nasiri
DATE OF BIRTH/PLACE OF BIRTH: 1954, Tikrit
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Governor of Basrah
27. NAME: Sultan Hashim Ahmad Al-Ta'i
DATE OF BIRTH/PLACE OF BIRTH: 1944, Mosul
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Minister of Defence
28. NAME: Hikmat Mizban Ibrahim al-Azzawi
DATE OF BIRTH/PLACE OF BIRTH: 1934, Diyala
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Deputy Prime Minister and Finance Minister
29. NAME: Mahmud Dhiyab Al-Ahmed
DATE OF BIRTH/PLACE OF BIRTH: 1953, Baghdad or Mosul
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Minister of Interior
30. NAME: Ayad Futayyih Khalifa Al-Rawi
DATE OF BIRTH/PLACE OF BIRTH: Circa 1942, Rawah
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Chief of Staff, Quds Force, 2001 to 2003;
Former Governor of Baghdad and Ta'mim
31. NAME: Zuhair Talib Abd-al-Sattar Al-Naqib
DATE OF BIRTH/PLACE OF BIRTH: Circa 1948
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Director, Military Intelligence
32. NAME: Amir Hamudi Hassan Al-Sa'di
DATE OF BIRTH/PLACE OF BIRTH: 5 April 1938, Baghdad
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Presidential Scientific Advisor;
Senior Deputy, Organisation of Military Industrialisation, 1988 to 1991;
Former President, Technical Corps for Special Projects;
PASSPORTS:?No 33301/862
Issued: 17 October 1997
Expires: 1 October 2005?
M0003264580
Issued: Unknown
Expires: Unknown?
H0100009
Issued: May 2001
Expires: Unknown
33. NAME: Amir Rashid Muhammad Al-Ubaidi
DATE OF BIRTH/PLACE OF BIRTH: 1939, Baghdad
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Minister of Oil, 1996 to 2003;
Head, Organisation of Military Industrialisation, early 1990s.
34. NAME: Husam Muhammad Amin Al-Yassin
DATE OF BIRTH/PLACE OF BIRTH: 1953 or 1958, Tikrit
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Head, National Monitoring Directorate
35. NAME: Muhammad Mahdi Al-Salih
DATE OF BIRTH/PLACE OF BIRTH: 1947 or 1949, al-Anbar Governate
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Minister of Trade, 1987 to 2003;
Chief, Presidential Office, mid-1980s
36. NAME: Sab'awi Ibrahim Hassan Al-Tikriti
DATE OF BIRTH/PLACE OF BIRTH: 1947, Tikrit
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Presidential Advisor;
Director of General Security, early 1990s;
Chief, Iraqi Intelligence Services, 1990 to 1991;
Half-brother of Saddam Hussein
37. NAME: Watban Ibrahim Hassan Al-Tikriti
ALIAS: Watab Ibrahim al-Hassan
DATE OF BIRTH/PLACE OF BIRTH: 1952, Tikrit
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Presidential Advisor;
Minister of Interior, Early 1990s;
Half-brother of Saddam Hussein
38. NAME: Barzan Ibrahim Hassan Al-Tikriti
DATE OF BIRTH/PLACE OF BIRTH: 1951, Tikrit
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Presidential Advisor;
Permanent Representative to UN (Geneva), 1989 to 1998;
Head, Iraqi Intelligence Services, early 1980s;
Half-brother of Saddam Hussein
39. NAME: Huda Salih Mahdi Ammash
DATE OF BIRTH/PLACE OF BIRTH: 1953, Baghdad
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Member, Ba'th Party Regional Command;
Head, Biological Laboratories, Military Industrial Organisation, mid-1990s;
Former Head, Student and Youth Bureau, Ba'th Party;
Former Head, Professional Bureau of Women's Affairs;
40. NAME: Abd-al-Baqi Abd-al-Karim Abdallah Al-Sa'dun
DATE OF BIRTH/PLACE OF BIRTH: 1947
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Ba'th Party Regional Command Chairman, Diyala
Deputy Commander, Southern Region, 1998 to 2000;
Former National Assembly Speaker
41. NAME: Muhammad Zimam Abd-al-Razzaq Al-Sa'dun
DATE OF BIRTH/PLACE OF BIRTH: 1942, Suq Ash-Shuyukh District, Dhi-Qar
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Ba'th Party Regional Command Chairman, At-Tamin;
Minister of Interior, 1995 to 2001
42. NAME: Samir Abd al-Aziz Al-Najim
DATE OF BIRTH/PLACE OF BIRTH: 1937 or 1938, Baghdad
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Ba'th Party Regional Command Chairman, East Baghdad;
43. NAME: Humam Abd-al-Khaliq Abd-al-Ghafur
ALIAS: Humam 'Abd al-Khaliq 'Abd al-Rahman;
Humam 'Abd-al-Khaliq Rashid
DATE OF BIRTH/PLACE OF BIRTH: 1945, Ar-Ramadi
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Minister of Higher Education and Research, 1992 to 1997, 2001 to 2003;
Minister of Culture, 1997 to 2001;
Director and Deputy Director, Iraqi Atomic Energy Organisation, 1980s;
PASSPORT: 0018061/104, issued 12 September 1993
44. NAME: Yahia Abdallah Al-Ubaidi
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Ba'th Party Regional Command Chairman, al-Basrah
45. NAME: Nayif Shindakh Thamir Ghalib
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Ba'th Party Regional Command Chairman, An-Najaf;
Member, Iraqi National Assembly;
NOTE: Died in 2003
46. NAME: Saif-al-Din Al-Mashhadani
DATE OF BIRTH/PLACE OF BIRTH: 1956, Baghdad
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Ba'th Party Regional Command Chairman, Al-Muthanna
47. NAME: Fadil Mahmud Gharib
ALIAS: Gharib Muhammad Fazel al-Mashaikhi
DATE OF BIRTH/PLACE OF BIRTH: 1944, Dujail
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Ba'th Party Regional Command Chairman, Babil;
Chairman, General Federation of Iraqi Trade Unions
48. NAME: Muhsin Khadr Al-Khafaji
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Ba'th Party Regional Command Chairman, al-Qadisyah
49. NAME: Rashid Taan Kathim
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Ba'th Party Regional Command Chairman, al-Anbar
50. NAME: Ugla Abid Sakr Al-Zubaisi
ALIAS: Saqr al-Kabisi Abd Aqala
DATE OF BIRTH/PLACE OF BIRTH: 1944, Kubaisi, al-Anbar
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Ba'th Party Regional Command Chairman, Maysan
51. NAME: Ghazi Hammud Al-Ubaidi
DATE OF BIRTH/PLACE OF BIRTH: 1944, Baghdad
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Ba'th Party Regional Command Chairman, Wasit
52. NAME: Adil Abdallah Mahdi
DATE OF BIRTH/PLACE OF BIRTH: 1945, al-Dur
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Ba'th Party Regional Command Chairman, Dhi-Qar;
Former Ba'th Party Chairman for Diyala and al-Anbar
53. NAME: Qaid Hussein Al-Awadi
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Ba'th Party Regional Command Chairman, Ninawa;
Former Governor of An-Najaf, circa 1998 to 2002
54. NAME: Khamis Sirhan Al-Muhammad
ALIAS: Dr. Fnu Mnu Khamis
NATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Ba'th Party Regional Command Chairman, Karbala
55. NAME: Sa'd Abd-al-Majid Al-Faisal Al-Tikriti
ATE OF BIRTH/PLACE OF BIRTH: 1944, Tikrit
ATIONALITY: Iraq
UNSC RESOLUTION 1483 BASIS:
Ba'th Party Regional Command Chairman, Salah Ad-Din;
Former Undersecretary for Security Affairs, Foreign Ministry
ANNEX V
List of competent authorities referred to in Articles 7 and 8
BELGIUM
Service Public Fédéral Economie, PME, Classes Moyennes et Energie
Administration des relations économiques
Politique d'accès aux marchés
Service: Licences
60, Rue Général Leman
B-1040 Bruxelles
Tél. (32-2) 206 58 11
Fax (32-2) 230 83 22
Federale Overheidsdienst Economie, KMO, Middenstand en Energie
Bestuur economische betrekkingen
Marktordening
Dienst: vergunningen
60, Generaal Lemanstraat
B-1040 Brussel
Tel. (32-2) 206 58 11
Fax (32-2) 230 83 22
Service Public Fédéral Finances
Administration de la Trésorerie
Avenue des Arts, 30
B-1040 Bruxelles
Fax (32-2) 233 75 18
E-mail: quesfinvragen.tf@minfin.fed.be
mailto: quesfinvragen.tf@minfin.fed.be
Federale Overheidsdienst Financiën
Administratie van de Thesaurie
Kunstlaan, 30
B-1040 Brussel
Fax (32-2) 233 75 18
E-mail: quesfinvragen.tf@minfin.fed.be
mailto: quesfinvragen.tf@minfin.fed.be
DENMARK
Erhvervs- og Boligstyrelsen
Dahlerups Pakhus
Langelinie Allé 17
DK-2100 København Ø
Tel. (45) 35 46 60 00
Fax (45) 35 46 60 01
GERMANY
For funds and financial assets:
Deutsche Bundesbank
Postfach 100 602
D-60006 Frankfurt am Main
Tel. (49-69) 95661
Fax (49-69) 5601071
For Iraqi cultural property:
Zollkriminalamt
Bergisch Gladbacher Str. 837
D-51069 Köln
Tel. (49-221) 6720
Fax (49-221) 6724500
E-mail: poststelle@zka.bgmv.de
Internet: www.zollkriminalamt.de
GREECE
Υπουργείο Εθνικής Οικονομίας και Οικονομικών
Γενική Γραμματεία Διεθνών Σχέσεων
Γενική Διεύθυνση Πολιτικού Προγραμματισμού και Εφαρμογής
Διεύθυνση Διεθνών Οικονομικών Θεμάτων
Τηλ.: 30210 32 86 021, 32 86 051
Φαξ: 30210 32 86 094, 32 86 059
E-mail: e3c@dos.gr
Ministry of Economy and Economics General Secretariat of International Relations
General Directorate for Policy Planning and Implementation
Directory for International Economy Issues
Tel. 30210 32 86 021, 32 86 051
Fax 30210 32 86 094, 32 86 059
E-mail: e3c@dos.gr
SPAIN
Ministerio de Economía
Secretaría General de Comercio Exterior
Po de la Castellana 162
E-28046 MADRID
Tel. (34-91) 349 38 60
Fax (34-91) 457 28 63
FRANCE
Ministère de l'économie, des finances et de l'industrie
Direction du Trésor
Service des affaires européennes et internationales
Sous direction E
139, rue de Bercy
F-75 572 Paris Cedex 12
Tél. (33-1) 44 87 72 85
Fax (33-1) 53 18 96 37
Ministère des Affaires étrangères
Direction des Nations unies et des Organisations internationales
Sous-direction des affaires politiques
37, quai d'Orsay
75700 Paris 07SP
Tél. (33-1) 43174678/5968/5032
Fax (33-1) 43174691
IRELAND
Licensing Unit Department of Enterprise, Trade and Employment
Block C
Earlsfort Centre
Hatch Street
Dublin 2 Ireland
Tel. (353-1) 6312534
Fax (353-1) 6312562
ITALY
Ministero delle Attività Produttive
D. G. per la Politica Commerciale e per la Gestione del Regime degli Scambi
Divisione IV - UOPAT
Viale Boston, 35
I-00144 Roma
Dirigente:
Tel. (39-06) 59647534
Fax (39-06) 59647506
Collaboratori:
Tel. (39-06) 59933295
Fax (39-06) 59932430
LUXEMBOURG
Ministère des affaires étrangères, du commerce extérieur, de la
coopération, de l'action humanitaire et de la défense
Direction des relations économiques internationales
BP 1602
L-1016 Luxembourg
Tél. (352) 478-1 ou 478-2350
Fax (352) 22 20 48
Office des licences
BP 113
L-2011 Luxembourg
Tél. (352) 478 23 70
Fax (352) 46 61 38
Ministère des finances
3, rue de la Congrégation
L-1352 Luxembourg
Tel. (352) 478-2712
Fax (352) 47 52 41
NETHERLANDS
General coordination of Iraq sanctions
Ministerie van Buitenlandse Zaken
Departement Politieke Zaken
Postbus 20061
2500 EB Den Haag
Nederland
Fax (31-70) 348 4638
Tel. (31-70) 348 6211
e-mail: DPZ@minbuza.nl
Specifically for financial sanctions
Ministerie van Financiën
Directie Financiële Martken/Afdeling Integriteit
Postbus 20201
2500 EE Den Haag
Fax (31-70) 342 7918
Tel. (31-70) 342 8148
For Iraqi cultural property
Inspectie Cultuurbezit
Prins Willem-Alexander Hof 28
2595 BE Den Haag
Tel. (31-70) 302 8120
Fax (31-70) 365 1914
AUSTRIA
Bundesministerium für Wirtschaft und Arbeit
Abteilung C2/2
Außenwirtschaftsadministration
Stubenring 1
A-1010 Wien
Tel. (43-1) 71100/8345
Fax (43-1) 71100/8386
Österreichische Nationalbank
Otto-Wagner-Platz 3
A-1090 Wien
Tel. (43-1) 404 20 0
Fax (43-1) 404 20 7399
PORTUGAL
Ministério dos Negócios Estrangeiros
Direcção Geral dos Assuntos Multilaterais
Direcção de Serviços das Organizações Políticas Multilaterais
Largo do Rilvas,
P-1399-030 Lisboa
Portugal
e-mail: spm@sg.mne.gov.pt
Tel: (351-21) 3946702
Fax: (351-21) 3946073
FINLAND
Ulkoasiainministeriö/Utrikesministeriet
PL/PB 176
FIN-00161 Helsinki/Helsingfors
Tel. (358-9) 16 05 59 00
Fax (358-9) 16 05 57 07
SWEDEN
Utrikesdepartementet
Rättssekretariatet för EU-frågor
S-103 39 Stockholm
Tel. (46-8) 405 1000
Fax (46-8) 723 1176
UNITED KINGDOM
H M Treasury
International Financial Services Team
1 Horseguards Road
London SW1A 2HQ
United Kingdom
Tel. (44-207) 270 5550,
Fax (44-207) 270 5430
Bank of England
Financial Sanctions Unit
Threadneedle Street
London EC2R 8AH
United Kingdom
Tel. (44-207) 601 4768
Fax (44-207) 601 4309
EUROPEAN COMMUNITY
Commission of the European Communities
Directorate-General for External Relations
Directorate CFSP
Unit A.2: Legal and institutional matters for external relations - Sanctions
CHAR 12/163
B-1049 Bruxelles/Brussel
Tel. (32-2) 295 81 48, 296 25 56
Fax (32-2) 296 75 63
E-mail: relex-sanctions@cec.eu.int
