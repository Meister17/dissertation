Commission Regulation (EC) No 2107/2005
of 21 December 2005
amending Regulations (EC) No 174/1999, (EC) No 2771/1999, (EC) No 2707/2000, (EC) No 214/2001 and (EC) No 1898/2005 in the milk sector
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1255/1999 of 17 May 1999 on the common organisation of the market in milk and milk products [1], and in particular Articles 10, 15, 31(14) and 40 thereof,
Whereas:
(1) As from 1 January 2006 Council Directive 92/46/EEC of 16 June 1992 laying down the health rules for the production and placing on the market of raw milk, heat-treated milk and milk-based products [2], is repealed by Directive 2004/41/EC of the European Parliament and of the Council [3] and replaced by Regulation (EC) No 852/2004 of the European Parliament and of the Council of 29 April 2004 on the hygiene of foodstuffs [4] and by Regulation (EC) No 853/2004 of the European Parliament and of the Council of 29 April 2004 laying down specific hygiene rules for food of animal origin [5].
(2) For reasons of clarity it is appropriate to adapt accordingly the references made to Directive 92/46/EEC in Commission Regulation (EC) No 174/1999 of 26 January 1999 laying down special detailed rules for the application of Council Regulation (EEC) No 804/68 as regards export licences and export refunds in the case of milk and milk products [6], in Commission Regulation (EC) No 2771/1999 of 16 December 1999 laying down detailed rules for the application of Council Regulation (EC) No 1255/1999 as regards intervention on the market in butter and cream [7], in Commission Regulation (EC) No 2707/2000 of 11 December 2000 laying down rules for applying Council Regulation (EC) No 1255/1999 as regards Community aid for supplying milk and certain milk products to pupils in educational establishments [8], in Commission Regulation (EC) No 214/2001 of 12 January 2001 laying down detailed rules for the application of Council Regulation (EC) No 1255/1999 as regards intervention on the market in skimmed-milk powder [9], and in Commission Regulation (EC) No 1898/2005 of 9 November 2005 laying down detailed rules for implementing Council Regulation (EC) No 1255/1999 as regards measures for the disposal of cream, butter and concentrated butter on the Community market [10].
(3) In accordance with Article 1 point (a) of Regulation (EC) No 1898/2005, intervention butter bought in under Article 6(2) of Regulation (EC) No 1255/1999 to be sold at reduced prices shall have been taken into storage before 1 January 2003. In view of the quantity still available and the market situation, that date should be amended to 1 January 2004. Article 1 point (a) of the abovementioned Regulation must therefore be amended.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk products,
HAS ADOPTED THIS REGULATION:
Article 1
Article 1(4) of Regulation (EC) No 174/1999 is replaced by the following:
"4. For a refund to be granted on the products listed in Article 1 of Regulation (EC) No 1255/1999 they must meet the relevant requirements of Regulation (EC) No 852/2004 of the European Parliament and of the Council [11] and Regulation (EC) No 853/2004 of the European Parliament and of the Council [12], notably preparation in an approved establishment and compliance with the identification marking requirements specified in Section I of Annex II to Regulation (EC) No 853/2004.
Article 2
Article 5(1)(a) of Regulation (EC) No 2771/1999 is replaced by the following:
"(a) are approved in accordance with Article 4 of Regulation (EC) No 853/2004 of the European Parliament and of the Council [13] and have the appropriate technical equipment;
Article 3
Article 3(6) of Regulation (EC) No 2707/2000 is replaced by the following:
"6. An aid shall only be granted on the products listed in the Annex I to this Regulation if the products comply with the requirements of Regulation (EC) No 852/2004 of the European Parliament and of the Council [14] and Regulation (EC) No 853/2004 of the European Parliament and of the Council [15], and in particular the requirements concerning preparation in an approved establishment and the identification marking requirements specified in Section I of Annex II to Regulation (EC) No 853/2004.
Article 4
Regulation (EC) No 214/2001 is amended as follows:
1. Article 3(1)(a) is replaced by the following:
"(a) are approved in accordance with Article 4 of Regulation (EC) No 853/2004 of the European Parliament and of the Council [16] and have the appropriate technical equipment;
2. in Annex I, footnote 5 is replaced by the following:
"(5) Raw milk used for the manufacture of skimmed-milk powder must meet the requirements specified in Section IX of Annex III to Regulation (EC) No 853/2004."
Article 5
Regulation (EC) No 1898/2005 is amended as follows:
1. in Article 1(a), " 1 January 2003" is replaced by " 1 January 2004".
2. in Article 5(1) the second subparagraph is replaced by the following:
"Butter, concentrated butter, cream and the intermediate products referred to in the first subparagraph shall meet the requirements of Regulation (EC) No 852/2004 of the European Parliament and of the Council [17] and Regulation (EC) No 853/2004 of the European Parliament and of the Council [18], in particular as regards preparation in an approved establishment and compliance with the identification marking requirements specified in Section I of Annex II to Regulation (EC) No 853/2004.
3. Article 13(1)(b) is replaced by the following:
"(b) where applicable, they have been approved in accordance with Article 4 of Regulation (EC) No 853/2004";
4. Article 47(1) the second subparagraph is replaced by the following:
"It shall meet the requirements of Regulations (EC) No 852/2004 and (EC) No 853/2004, in particular as regards preparation in an approved establishment and compliance with the identification marking requirements specified in Section I of Annex II to Regulation (EC) No 853/2004.";
5. Article 63(2)(a) is replaced by the following:
"(a) they have been approved in accordance with Article 4 of Regulation (EC) No 853/2004";
6. Article 72(b)(ii) is replaced by the following:
"(ii) the requirements of Regulations (EC) No 852/2004 and (EC) No 853/2004, in particular as regards preparation in an approved establishment and compliance with the identification marking requirements specified in Section I of Annex II to Regulation (EC) No 853/2004.";
7. Article 81(1) is replaced by the following:
"1. The butter shall be delivered to the beneficiary in packages bearing in clear and indelible lettering the national quality class and identification marking in accordance with Article 72(b) and one or more of the entries listed in Annex XVI(1)."
Article 6
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
It shall apply from 1 January 2006 except Article 5(1) which shall apply from 16 December 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 21 December 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 160, 26.6.1999, p. 48. Regulation last amended by Commission Regulation (EC) No 1913/2005 (OJ L 307, 25.11.2005, p. 2).
[2] OJ L 268, 14.9.1992, p. 1. Directive as last amended by Regulation (EC) No 806/2003 (OJ L 122, 16.5.2003, p. 1).
[3] OJ L 157, 30.4.2004, p. 33.
[4] OJ L 139, 30.4.2004, p. 1. Corrected version in OJ L 226, 25.6.2004, p. 3.
[5] OJ L 139, 30.4.2004, p. 55. Corrected version in OJ L 226, 25.6.2004, p. 22.
[6] OJ L 20, 27.1.1999, p. 8. Regulation last amended by Regulation (EC) No 1513/2005 (OJ L 241, 17.9.2005, p. 45).
[7] OJ L 333, 24.12.1999, p. 11. Regulation last amended by Regulation (EC) No 1802/2005 (OJ L 290, 4.11.2005, p. 3).
[8] OJ L 311, 12.12.2000, p. 37. Regulation last amended by Regulation (EC) No 865/2005 (OJ L 144, 8.6.2005, p. 41).
[9] OJ L 37, 7.2.2001, p. 100. Regulation last amended by Regulation (EC) No 1195/2005 (OJ L 194, 26.7.2005, p. 8).
[10] OJ L 308, 25.11.2005, p. 1.
[11] OJ L 139, 30.4.2004, p. 1. Corrected by OJ L 226, 25.6.2004, p. 3.
[12] OJ L 139, 30.4.2004, p. 55. Corrected by OJ L 226, 25.6.2004, p. 22."
[13] OJ L 139, 30.4.2004, p. 55. Corrected by OJ L 226, 25.6.2004, p. 22."
[14] OJ L 139, 30.4.2004, p. 1. Corrected by OJ L 226, 25.6.2004, p. 3.
[15] OJ L 139, 30.4.2004, p. 55. Corrected by OJ L 226, 25.6.2004, p. 22."
[16] OJ L 139, 30.4.2004, p. 55. Corrected by OJ L 226, 25.6.2004, p. 22.";
[17] OJ L 139, 30.4.2004, p. 1. Corrected by OJ L 226, 25.6.2004, p. 3.
[18] OJ L 139, 30.4.2004, p. 55. Corrected by OJ L 226, 25.6.2004, p. 22.";
--------------------------------------------------
