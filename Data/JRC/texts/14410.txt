Judgment of the Court (First Chamber) of 5 October 2006 — Commission of the European Communities v Kingdom of Belgium
(Case C-377/03) [1]
Parties
Applicant: Commission of the European Communities (represented by: C. Giolito and G. Wilms, Agents)
Defendant: Kingdom of Belgium (represented by: E. Dominkovits, A. Goldman and M. Wimmer, Agents, B. van de Walle de Ghelcke, Lawyer)
Re:
Failure of a Member State to fulfil obligations — Articles 6, 9, 10 and 11 of Council Regulation (EC, Euratom) No 1150/2000 of 22 May 2000 implementing Decision 94/728/EC, Euratom on the system of the Communities' own resources (OJ 2000 L 130, p. 1) — Default or delay in the payment of the own resources to the Commission — Failure to comply with accounting rules — Irregular release of certain transit documents (TIR carnets) by the Belgian customs authority
Operative part of the judgment
The Court:
1. Declares that by failing to enter in the accounts, or by making a late entry in the accounts of, the own resources arising from the TIR carnets which had not been discharged properly, by placing them in the B accounts instead of entering them in the A accounts, with the result that the relevant own resources were not made available to the Commission of the European Communities within the time-limits,
- by refusing to pay default interest on the amounts owing to the Commission of the European Communities,
- the Kingdom of Belgium has failed to fulfil its obligations under Articles 6, 9, 10 and 11 of Council Regulation (EC, Euratom) No 1150/2000 of 22 May 2000 implementing Decision 94/728/EC, Euratom on the system of the Communities' own resources, which, with effect from 31 May 2000, repealed and replaced Council Regulation (EEC, Euratom) No 1552/89 of 29 May 1989 implementing Decision 88/376/EEC, Euratom on the system of the Communities' own resources, which had identical subject-matter;
2. Dismisses the remainder of the action;
3. Orders the Kingdom of Belgium to pay the costs.
[1] OJ C 264, 01.11.2003.
--------------------------------------------------
