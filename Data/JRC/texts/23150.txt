AGREEMENT between the European Communities and the Government of the United States of America on the application of positive comity principles in the enforcement of their competition laws
THE EUROPEAN COMMUNITY AND THE EUROPEAN COAL AND STEEL COMMUNITY
of the one part (hereinafter 'the European Communities`), and
THE GOVERNMENT OF THE UNITED STATES OF AMERICA
of the other part,
Having regard to the 23 September 1991 Agreement between the European Communities and the Government of the United States of America regarding the application of their competition laws, and the exchange of interpretative letters dated 31 May and 31 July 1995 in relation to that Agreement (together hereinafter 'the 1991 Agreement`),
Recognising that the 1991 Agreement has contributed to coordination, cooperation, and avoidance of conflicts in competition law enforcement,
Noting in particular Article V of the 1991 Agreement, commonly referred to as the 'positive comity` Article, which calls for cooperation regarding anti-competitive activities occurring in the territory of one Party that adversely affect the interests of the other Party,
Believing that further elaboration of the principles of positive comity and of the implementation of those principles would enhance the 1991 Agreement's effectiveness in relation to such conduct,
and
Noting that nothing in this Agreement or its implementation shall be construed as prejudicing either Party's position on issues of competition law jurisdiction in the international context,
HAVE AGREED AS FOLLOWS:
Article I
Scope and purpose of this Agreement
1. This Agreement applies where a Party satisfies the other that there is reason to believe that the following circumstances are present:
(a) anti-competitive activities are occurring in whole or in substantial part in the territory of one of the Parties and are adversely affecting the interests of the other Party; and
(b) the activities in question are impermissible under the competition laws of the Party in the territory of which the activities are occurring.
2. The purposes of this Agreement are to:
(a) help ensure that trade and investment flows between the Parties and competition and consumer welfare within the territories of the parties are not impeded by anti-competitive activities for which the competition laws of one or both Parties can provide a remedy, and
(b) establish cooperative procedures to achieve the most effective and efficient enforcement of competition law, whereby the competition authorities of each Party will normally avoid allocating enforcement resources to deal with anti-competitive activities that occur principally in and are directed principally towards the other Party's territory, where the competition authorities of the other Party are able and prepared to examine and take effective sanctions under their law to deal with those activities.
Article II
Definitions
As used in this Agreement:
1. 'Adverse effects` and 'adversely affected` mean harm caused by anti-competitive activities to:
(a) the ability of firms in the territory of a Party to export to, invest in, or otherwise compete in the territory of the other Party; or
(b) competition in a Party's domestic or import markets.
2. 'Requesting Party` means a Party that is adversely affected by anti-competitive activities occurring in whole or in substantial part in the territory of the other Party.
3. 'Requested Party` means a Party in the territory of which such anti-competitive activities appear to be occurring.
4. 'Competition law(s)` means
(a) for the European Communities, Articles 85, 86, and 89 of the Treaty establishing the European Community (EC), Articles 65 and 66(7) of the Treaty establishing the European Coal and Steel Community (ECSC), and their implementing instruments, to the exclusion of Council Regulation (EEC) No 4064/89 on the control of concentrations between undertakings; and
(b) for the United States of America, the Sherman Act (15 U.S.C. §§ 1-7), the Clayton Act (15 U.S.C. §§ 12-27, except as it relates to investigations pursuant to Title II of the Hart-Scott-Rodino Antitrust Improvements Act of 1976, 15 U.S.C. § 18a), the Wilson Tariff Act (15 U.S.C. §§ 8-11), and the Federal Trade Commission Act (15 U.S.C. §§ 41-58, except as these sections relate to consumer protection functions);
as well as such other laws or regulations as the Parties shall jointly agree in writing to be a 'competition law` for the purposes of this Agreement.
5. 'Competition authorities` means:
(a) for the European Communities, the Commission of the European Communities, as to its responsibilities pursuant to the competition laws of the European Communities, and
(b) for the United States, the Antitrust Division of the United States Department of Justice and the Federal Trade Commission.
6. 'Enforcement activities` means any application of competition law by way of investigation or proceeding conducted by the competition authorities of a Party.
7. 'Anti-competitive activities` means any conduct or transaction that is impermissible under the competition laws of a Party.
Article III
Positive comity
The competition authorities of a Requesting Party may request the competition authorities of a Requested Party to investigate and, if warranted, to remedy anti-competitive activities in accordance with the Requested Party's competition laws. Such a request may be made regardless of whether the activities also violate the Requesting Party's competition laws, and regardless of whether the competition authorities of the Requesting Party have commenced or contemplate taking enforcement activities under their own competition laws.
Article IV
Deferral or suspension of investigations in reliance on enforcement activity by the Requested Party
1. The competition authorities of the Parties may agree that the competition authorities of the Requesting Party will defer or suspend pending or contemplated enforcement activities during the pendency of enforcement activities of the Requested Party.
2. The competition authorities of a Requesting Party will normally defer or suspend their own enforcement activities in favour of enforcement activities by the competition authorities of the Requested Party when the following conditions are satisfied:
(a) the anti-competitive activities at issue:
(i) do not have a direct, substantial and reasonably foreseeable impact on consumers in the Requesting Party's territory; or
(ii) where the anti-competitive activities do have such an impact on the Requesting Party's consumers, they occur principally in and are directed principally towards the other Party's territory;
(b) the adverse effects on the interests of the Requesting Party can be and are likely to be fully and adequately investigated and, as appropriate, eliminated or adequately remedied pursuant to the laws, procedures, and available remedies of the Requested Party. The Parties recognise that it may be appropriate to pursue separate enforcement activities where anti-competitive activities affecting both territories justify the imposition of penalties within both jurisdictions; and
(c) the competition authorities of the Requested Party agree that in conducting their own enforcement activities, they will:
(i) devote adequate resources to investigate the anti-competitive activities and, where appropriate, promptly pursue adequate enforcement activities;
(ii) use their best efforts to pursue all reasonably available sources of information, including such sources of information as may be suggested by the competition authorities of the Requesting Party;
(iii) inform the competition authorities of the Requesting Party, on request or at reasonable intervals, of the status of their enforcement activities and intentions, and where appropriate provide to the competition authorities of the Requesting Party relevant confidential information if consent has been obtained from the source concerned. The use and disclosure of such information shall be governed by Article V;
(iv) promptly notify the competition authorities of the Requesting Party of any change in their intentions with respect to investigation or enforcement;
(v) use their best efforts to complete their investigation and to obtain a remedy or initiate proceedings within six months, or such other time as agreed to by the competition authorities of the Parties, of the deferral or suspension of enforcement activities by the competition authorities of the Requesting Party;
(vi) fully inform the competition authorities of the Requesting Party of the results of their investigation, and take into account the views of the competition authorities of the Requesting Party, prior to any settlement, initiation of proceedings, adoption of remedies, or termination of the investigation; and
(vii) comply with any reasonable request that may be made by the competition authorities of the Requesting Party.
When the above conditions are satisfied, a Requesting Party which chooses not to defer or suspend its enforcement activities shall inform the competition authorities of the Requested Party of its reasons.
3. The competition authorities of the Requesting Party may defer or suspend their own enforcement activities if fewer than all of the conditions set out in paragraph 2 are satisfied.
4. Nothing in this Agreement precludes the competition authorities of a Requesting Party that choose to defer or suspend independent enforcement activities from later initiating or such activities. In such circumstances, the competition authorities of the Requesting Party will promptly inform the competition authorities of the Requested Party of their intentions and reasons. If the competition authorities of the Requested Party continue with their own investigation, the competition authorities of the two Parties shall, where appropriate, coordinate their respective investigations under the criteria and procedures of Article IV of the 1991 Agreement.
Article V
Confidentiality and use of information
Where pursuant to this Agreement the competition authorities of one Party provide information to the competition authorities of the other Party for the purpose of implementing this Agreement, that information shall be used by the latter competition authorities only for that purpose. However, the competition authorities that provided the information may consent to another use, on condition that where confidential information has been provided pursuant to Article IV(2)(c)(iii) on the basis of the consent of the source concerned, that source also agrees to the other use. Disclosure of such information shall be governed by the provisions of Article VIII of the 1991 Agreement and the exchange of interpretative letters dated 31 May and 31 July 1995.
Article VI
Relationship to the 1991 Agreement
This Agreement shall supplement and be interpreted consistently with the 1991 Agreement, which remains fully in force.
Article VII
Existing law
Nothing in this Agreement shall be interpreted in a manner inconsistent with the existing laws, or as requiring any change in the laws, of the European Communities or the United States of America or of their respective Member States or states.
Article VIII
Entry into force and termination
1. This Agreement shall enter into force upon signature.
2. This Agreement shall remain in force until 60 days after the date on which either Party notifies the other Party in writing that it wishes to terminate the Agreement.
IN WITNESS WHEREOF, the undersigned, being duly authorised, have signed this Agreement.
DONE at Brussels and Washington, in duplicate, in the English language.
For the European Community and for the European Coal and Steel Community
Date: >REFERENCE TO A FILM>
Date: >REFERENCE TO A FILM>
For the Government of the United States of America
Date: >REFERENCE TO A FILM>
Date: >REFERENCE TO A FILM>
