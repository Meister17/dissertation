Commission Regulation (EC) No 80/2001
of 16 January 2001
laying down detailed rules for the application of Council Regulation (EC) No 104/2000 as regards notifications concerning recognition of producer organisations, the fixing of prices and intervention within the scope of the common organisation of the market in fishery and aquaculture products
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 104/2000 of 17 December 1999 on the common organisation of the markets in fishery and aquaculture products(1), and in particular Article 34(2) thereof,
Whereas:
(1) In accordance with Article 13(6) of Regulation (EC) No 104/2000, the Commission publishes annually a list of recognised producer organisations and associations thereof. Member States must therefore provide it with adequate information.
(2) The Commission has to be able to monitor the price stabilisation activities of the producer organisations and the way in which they apply the systems of financial compensation and carry-over premiums.
(3) The Community intervention arrangements pursuant to Articles 21 to 26 of Regulation (EC) No 104/2000 require information to be available on the prices recorded in clearly defined regions at regular intervals.
(4) A system of electronic transmission of data between the Member States and the Commission has been introduced within the framework of management of the common fisheries policy (FIDES II system). It should be used for the purposes of collecting the information referred to in this Regulation.
(5) The data formerly collected under Commission Regulation (EEC) No 2210/93 of 26 July 1993 on the communication of information for the purposes of the common organisation of the market in fishery and aquaculture products(2), as amended by Regulation (EC) No 843/95(3), should therefore be simplified, harmonised and supplemented. A new Regulation should therefore be drawn up and Regulation (EEC) No 2210/93 should be repealed.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Fishery Products,
HAS ADOPTED THIS REGULATION:
CHAPTER I
Notifications regarding recognition of producer organisations and associations thereof
Article 1
Member States shall notify the Commission of the information referred to in Articles 6(1)(c) and 13(3)(d) of Regulation (EC) No 104/2000 within two months of the date of the adopted decision at the latest.
This information and the format in which it is to be sent shall be as set out in Annex I to this Regulation.
CHAPTER II
Prices and intervention
Article 2
Member States shall notify to the Commission, no later than two months after the beginning of each fishing year, the information referred to in Article 17(4) of Regulation (EC) No 104/2000.
Member States shall notify the Commission immediately of any change in the details referred to in the first paragraph.
This information and the format in which it is to be sent shall be as set out in Annex II to this Regulation.
Article 3
For the species listed in Annexes I and IV to Regulation (EC) No 104/2000, Member States shall notify the Commission of the quantities landed, sold, withdrawn and carried over throughout their territory, together with the value of the quantities sold, in each quarter in the various regions defined in Table 1 of Annex VIII to this Regulation, no later than seven weeks after the quarter in question.
Where there is a crisis for one or more species listed in Annex I to Regulation (EC) No 104/2000, Member States shall notify the Commission of the quantities landed, sold, withdrawn and carried over throughout their territory, together with the value of the quantities sold, in each fortnight in the various regions defined in Table 1 of the Annex VIII to this Regulation, no later than two weeks after the fortnight in question.
This information and the format in which it is to be sent shall be as set out in Annex III to this Regulation.
Article 4
Member States shall notify the Commission every quarter, for each product listed in Annex I to Regulation (EC) No 104/2000 that has been withdrawn, of the value and the quantities disposed of, broken down by the disposal options laid down in Article 1 of Commission Regulation (EEC) No 1501/83(4), no later than eight weeks after the quarter in question.
This information and the format in which it is to be sent shall be as set out in Annex IV to this Regulation.
Article 5
Member States shall notify the Commission, for each product listed in Annex II to Regulation (EC) No 104/2000, of the quantities landed, sold, and stored, together with the value of the quantities sold, in each quarter in the various regions defined in Table 1 of the Annex VIII to this Regulation, no later than six weeks after the quarter in question.
This information and the format in which it is to be sent shall be as set out in Annex V to this Regulation.
Article 6
Member States shall notify the Commission, for each product listed in Annex III to Regulation (EC) No 104/2000, of the quantities landed, sold, and delivered to industry by producer organisation, together with the value of the quantities delivered to industry, in each month in the various regions defined in Table 1 of Annex VIII to this Regulation, no later than six weeks after the month in question.
This information and the format in which it is to be sent shall be as set out in Annex VI to this Regulation.
Article 7
Member States shall provide the Commission each year, within three months of the end of the year in question, with information to enable the technical costs relating to the operations required for stabilisation and storage provided for in Articles 23 and 25 of Regulation (EC) No 104/2000 to be determined.
This information and the format in which it is to be sent shall be as set out in Annex VII to this Regulation.
CHAPTER III
General and final provisions
Article 8
Member States shall send the information to the Commission by electronic means, using the transmission systems currently used for data exchange within the framework of management of the common fisheries policy (FIDES II system).
Article 9
Regulation (EEC) No 2210/93 is hereby repealed.
Article 10
This Regulation shall enter into force on 1 January 2001.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 16 January 2001.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 17, 21.1.2000, p. 22.
(2) OJ L 197, 6.8.1993, p. 8.
(3) OJ L 85, 19.4.1995, p. 13.
(4) OJ L 152, 10.6.1983, p. 22.
ANNEX I
Information on producer organisations and associations thereof
>TABLE>
ANNEX II
Withdrawal prices applied by producer organisations
Send two months after the beginning of the fishing year
>TABLE>
ANNEX III
Products in Annex I and IV to Council Regulation (EC) No 104/2000
Quarterly notification
>TABLE>
ANNEX IV
Products in Annex I to Regulation (EC) No 104/2000
Use of products withdrawn from the market
Quarterly notification
>TABLE>
ANNEX V
Products in Annex II to Regulation (EC) No 104/2000 (Quarterly notification)
>TABLE>
ANNEX VI
Products in Annex III to Regulation (EC) No 104/2000
Frequency: monthly
>TABLE>
ANNEX VII
Products in Annexes I and II to Regulation (EC) No 104/2000
Frequency: annual
>TABLE>
ANNEX VIII
TABLE 1
>TABLE>
TABLE 2
Size codes
>TABLE>
TABLE 3
Presentation codes
>TABLE>
TABLE 4
Preservation codes
>TABLE>
TABLE 5
Freshness codes
>TABLE>
TABLE 6
Currency codes
>TABLE>
TABLE 7
>TABLE>
TABLE 8
Regions where the withdrawal price is adjusted by a regional coefficient
>TABLE>
TABLE 9
Use of withdrawals
>TABLE>
TABLE 10
Type of fishing
>TABLE>
TABLE 11
Type of technical cost
>TABLE>
