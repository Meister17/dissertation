COUNCIL DECISION
of 21 November 1988
concerning the conclusion of an Additional Protocol to the Cooperation Agreement between the European Economic Community and the Syrian Arab Republic
(88/598/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES
Having regard to the Treaty establishing the European Economic Community, and in particular Article 238 thereof,
Having regard to the recommendation from the Commission,
Having regard to the assent of the European Parliament (1),
Whereas the Additional Protocol to the Cooperation Agreement between the European Economic Community and the Syrian Arab Republic (2), signed at Brussels on 18 January 1977, should be approved.
HAS DECIDED AS FOLLOWS:
Article 1
The Additional Protocol to the Cooperation Agreement between the European Economic Community and the Syrian Arab Republic is hereby approved on behalf of the Community.
The text of the Protocol is attached to this Decision.
Article 2
The President of the Council shall give the notification provided for in Article 5 of the Protocol (3).
Article 3
This Decision shall take effect on the day following its publication in the Official Journal of the European Communities.
Done at Brussels, 21 November 1988.
For the Council
The President
Th. PANGALOS
EWG:L327UMBE23.96
FF: 7UEN; SETUP: 01; Hoehe: 450 mm; 39 Zeilen; 1428 Zeichen;
Bediener: JUTT Pr.: C;
Kunde:
(1) OJ N° C 290, 14. 11. 1988.
(2) OJ N° L 269, 27. 9. 1978, p. 2.
(3) See p. 64 of this Official Journal.
