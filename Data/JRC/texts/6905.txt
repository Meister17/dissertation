Commission Decision
of 17 January 2005
amending Decision 92/452/EEC as regards embryo collection teams in the United States of America
(notified under document number C(2005) 32)
(Text with EEA relevance)
(2005/29/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 89/556/EEC of 25 September 1989 on animal health conditions governing intra-Community trade in and importation from third countries of embryos of domestic animals of the bovine species [1], and in particular Article 8 thereof,
Whereas:
(1) Commission Decision 92/452/EEC of 30 July 1992 establishing lists of embryo collection teams and embryo production teams approved in third countries for export of bovine embryos to the Community [2] provides that Member States are only to import embryos from third countries where they have been collected, processed and stored by embryo collection teams listed in that Decision.
(2) The United States of America has requested that an amendment be made to the list as regards entries for that country.
(3) The United States of America has provided guarantees regarding compliance with the appropriate rules set out in Directive 89/556/EEC and the embryo collection team concerned has been officially approved for exports to the Community by the veterinary services of that country.
(4) Decision 92/452/EEC should therefore be amended accordingly.
(5) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
The Annex to Decision 92/452/EEC is amended in accordance with the Annex to this Decision.
Article 2
This Decision shall apply from 22 January 2005.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 17 January 2005.
For the Commission
Markos Kyprianou
Member of the Commission
--------------------------------------------------
[1] OJ L 302, 19.10.1989, p. 1. Directive as last amended by Regulation (EC) No 806/2003 (OJ L 122, 16.5.2003, p. 1).
[2] OJ L 250, 29.8.1992, p. 40. Decision as last amended by Decision 2004/568/EC (OJ L 252, 28.7.2004, p. 5).
--------------------------------------------------
+++++ ANNEX 1 +++++</br>
