Decision of the EEA Joint Committee
No 143/2005
of 2 December 2005
amending Annex II (Technical regulations, standards, testing and certification) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex II to the Agreement was amended by Decision of the EEA Joint Committee No 133/2005 [1].
(2) Commission Directive 2005/34/EC of 17 May 2005 amending Council Directive 91/414/EEC to include etoxazole and tepraloxydim as active substances [2] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following indent shall be added in point 12a (Council Directive 91/414/EEC) of Chapter XV of Annex II to the Agreement:
"— 32005 L 0034: Commission Directive 2005/34/EC (OJ L 125, 18.5.2005, p. 5)."
Article 2
The texts of Directive 2005/34/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 3 December 2005, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 2 December 2005.
For the EEA Joint Committee
The President
H.S.H. Prinz Nikolaus von Liechtenstein
[1] OJ L 14, 19.1.2006, p. 21.
[2] OJ L 125, 18.5.2005, p. 5.
[3] No constitutional requirements indicated.
--------------------------------------------------
