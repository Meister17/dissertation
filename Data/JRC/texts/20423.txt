COMMISSION DIRECTIVE 94/78/EC of 21 December 1994 adapting to technical progress Council Directive 78/549/EEC, as regards the wheel guards of motor vehicles
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 70/156/EEC of 6 February 1970 on the approximation of the laws of the Member States relating to the type-approval of motor vehicles and their trailers (1) as last amended by Commission Directive 93/81/EEC (2), and in particular Article 13 (2) thereof,
Having regard to Council Directive 78/549/EEC of 12 June 1978 on the approximation of the laws of the Member States relating to the wheel guards of motor vehicles (3), and in particular Article 4 thereof,
Whereas Directive 78/549/EEC is one of the separate directives of the EEC type-approval procedure which has been established by Directive 70/156/EEC; whereas, consequently, the provisions laid down in Directive 70/156/EEC relating to vehicle systems, components and separate technical units apply to this Directive;
Whereas, in particular, Articles 3 (4) and 4 (3) of Directive 70/156/EEC necessitate that each separate directive has attached to it an information document incorporating the relevant items of Annex I to that Directive and also a type-approval certificate based on Annex VI thereto in order that type-approval may be computerized;
Whereas the number of four-wheel-drive passenger cars is increasing, whether the four-wheel-drive is engaged permanently, automatically or under the control of the driver; whereas it is necessary, in the case of such passenger cars, and in the light of technical progress, to review certain design and operating parameters and to amend certain provisions of Directive 78/549/EEC in a manner which reflects the current and future market situation and which is consistent with proper design, construction and safe operating practice;
Whereas the provisions of this Directive are in accordance with the opinion of the Committee on the Adaptation to Technical Progress established by Directive 70/156/EEC,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Directive 78/549/EEC is amended as follows:
1. In Article 1, the words '(as defined in Annex I to Directive 70/156/EEC)' are replaced by the words '(as defined in Annex II A to Directive 70/156/EEC)'.
2. Annexes I and II are amended in accordance with points 1, 2 and 3 of the Annex hereto.
3. Annex III set out in point 4 of the Annex hereto is added.
Article 2
1. With effect from 1 July 1995 Member States may not, on grounds relating to wheel guards:
- refuse, in respect of a type of motor vehicle, to grant EEC type-approval or to grant national type-approval, or
- prohibit the registration, sale or entry into service of vehicles
if the wheel guards comply with the requirements of Directive 78/549/EEC, as amended by this Directive.
2. With effect from 1 January 1996 the Member States:
- shall no longer grant EEC type-approval, and
- may refuse to grant national type-approval
for a type of vehicle on grounds relating to the wheel guards if the requirements of Directive 78/549/EEC, as amended by this Directive, are not fulfilled.
Article 3
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive before 1 July 1995. They shall immediately inform the Commission thereof.
When Member States adopt these provisions, these shall contain a reference to this Directive or shall be accompanied by such reference at the time of their official publication. The procedure for such reference shall be adopted by Member States.
2. Member States shall communicate to the Commission the texts of the main provisions of national law which they adopt in the field covered by this Directive.
Article 4
This Directive shall enter into force on the 20th day following that of its publication in the Official Journal of the European Communities.
Done at Brussels, 21 December 1994.
For the Commission
Martin BANGEMANN
Member of the Commission
(1) OJ No L 42, 23. 2. 1970, p. 1.(2) OJ No L 264, 23. 10. 1993, p. 49.(3) OJ No L 168, 26. 6. 1978, p. 45.
ANNEX
1. The following is inserted immediately prior to the heading 'Annex I':
'List of Annexes
Annex I General requirements, special requirements, use of chains, application for EEC type-approval, granting of EEC type-approval, amendments to approvals, conformity of production
Annex II Information document
Annex III Type-approval certificate'
2. Annex I is amended as follows:
(a) The following heading to Annex I is inserted:
'GENERAL REQUIREMENTS, SPECIAL REQUIREMENTS, USE OF CHAINS, APPLICATION FOR EEC TYPE-APPROVAL, GRANTING OF EEC TYPE-APPROVAL, AMENDMENTS TO APPROVALS, CONFORMITY OF PRODUCTION'
(b) Item 2.1 is replaced by the following:
'2.1 The wheel guards shall meet the following requirements when the vehicle is in running order (see item 2.6 of Annex II) and carrying one passenger in the front seat and the wheels are in the straight ahead position:'
(c) Item 2.1.1 is replaced by the following:
'2.1.1. In the part formed by radial planes at an angle of 30 to the front and 50 to the rear of the centre of the wheels (see Figure 1), the overal width (q) of the wheel guards shall be at least sufficient to cover the total type width (b) taking into account the extremes of tyre/wheel combination as specified by the manufacturer and as indicated in item 1.3 of the Appendix to Annex III. In the case of twin wheels, the total width over the two tyres (t) shall be taken into account.'
(d) Item 3.1 is replaced by the following:
'3.1. In the case of vehicles where only two wheels are driven, the manufacturer shall certify that the vehicle is so designed that at least one type of snow chain can be used on at least one of the types of wheel and tyre approved for the drive wheels of that type of vehicle. One chain/tyre/wheel combination that is suitable for the vehicle shall be specified by the manufacturer and indicated in item 1.2 of the Appendix to Annex III.'
(e) The following new items 3.2 and 3.3 shall be added to point 3:
'3.2. In the case of vehicles where four wheels are driven, including vehicles where one drive axle may be disengaged manually or automatically, the manufacturer shall certify that the vehicle is so designed that at least one type of snow chain can be used on at least one of the types of wheel and tyre approved for at least one drive axle, which cannot be disengaged, of that type of vehicle. One chain/tyre/wheel combination that is situable for the vehicle and the drive wheels to which these chains may be fitted shall be specified by the manufacturer and indicated in item 1.2 of the Appendix to Annex III.
3.3. Instructions regarding the type or types of chains that may be used must accompany each vehicle falling within the series conforming to the EEC type-approval vehicle.'
(f) Item 4.1 is replaced by the following:
'4.1. The application for EEC type-approval pursuant to Article 3 (4) of Directive 70/156/EEC of a vehicle type with regard to wheel guards shall be submitted by the manufacturer.'
(g) Item 4.2 is replaced by the following:
'4.2. A model for the information document is given in Annex II.'
(h) Items 4.2.1 and 4.2.2 are deleted.
(i) The following items 5, 6 and 7 are added:
'5. GRANTING OF EEC TYPE-APPROVAL
5.1. If the relevant requirements are satisfied, EEC type-approval pursuant to Article 4 (3) of Directive 70/156/EEC shall be granted.
5.2. A model for the EEC type-approval certificate is given in Annex III.
5.3. An approval number in accordance with Annex VII to Directive 70/156/EEC shall be assigned to each type of vehicle approved. The same Member State shall not assign the same number to another type of vehicle.
6. MODIFICATIONS OF THE TYPE AND AMENDMENTS TO APPROVALS
6.1. In the case of modifications of the vehicle type approved pursuant to this Directive, the provisions of Article 5 of Directive 70/156/EEC shall apply.
7. CONFORMITY OF PRODUCTION
7.1. Measures to ensure the conformity of production shall be taken in accordance with the provisions laid down in Article 10 of Directive 70/156/EEC.'
3. Annex II is replaced by the following:
'ANNEX II
Information document No . . . . . .
pursuant to Annex I of Council Directive 70/156/EEC relating to EEC type-approval of a vehicle with respect to the wheel guards, Directive 78/549/EEC, as last amended by Directive . . . . . ./. . . . . ./EEC.
The following information, if applicable, must be supplied in triplicate and include a list of contents. Any drawings must be supplied in appropriate scale and in sufficient detail on size A4 or on a folder of A4 format. Photographs, if any, must show sufficient detail.
If the systems, components or separate technical units have electronic controls, information concerning their performance must be supplied.
0 GENERAL
0.1. Make (trade name of manufacturer):
0.2. Type and general commercial description(s):
0.3. Means of identification of type, if marked, on the vehicle (1)():
0.3.1 Location of that marking:
0.4. Category of vehicle (2)():
0.5. Name and address of manufacturer:
0.8. Address(es) of assembly plant(s):
1. GENERAL CONSTRUCTION CHARACTERISTICS OF THE VEHICLE
1.1. Photographs and/or drawings of a representative vehicle.
1.3. Number of axles and wheels:
1.3.1. Number and position of axles with double wheels.
2. MASSES AND DIMENSIONS (3)()
(in kg and mm) (refer to drawing where applicable)
2.4. Range of vehicle dimensions (overall)
2.4.1. For chassis without bodywork
The item numbers and footnotes used in this Information document correspond to those set out in Annex I to Directive 70/156/EEC.
Items not relevant for the purpose of this Directive are omitted.
2.4.1.3 Height (unladen) (4)() (For suspension adjustable for height, indicate normal running position):
2.4.2. For chassis with bodywork
2.4.2.3. Height (unladen) (5)() (For suspension adjustable for height, indicate normal running position):
2.6. Mass of the vehicle with bodywork in running order, or mass of the chassis with cab if the manufacturer does not fit the bodywork (including coolant, oils, fuel, tools, spare wheel and driver) (6)() (maximum and minimum for each version):
6 SUSPENSION
6.6. Tyres and wheels
6.6.1. Tyre/wheel combination(s) (For tyres indicate size designation, minimum load capacity index, minimum speed category symbol; for wheels indicate rim size(s) and off-set(s))
6.6.1.1. Axles
6.6.1.1.1. Axle 1:
6.6.1.1.2. Axle 2:
6.6.1.1.3. Axle 3:
6.6.1.1.4. Axle 4:
6.6.4. Chain/tyre/wheel combination on the front and/or rear axle that is suitable for the type of vehicle, as recommended by the manufacturer.
9. BODYWORK
9.16. Wheel guards
9.16.1. Brief description of the vehicle with regard to its wheel guards:
9.16.2. Detailed drawings of the wheel guards and their position on the vehicle showing the dimension specified in Figure 1 of Annex I to Directive 78/549/EEC and taking account of the extremes of tyre/wheel combinations.'
4. The following Annex III is added:
'ANNEX III
MODEL
[maximum format: A4 (210 × 297 mm)]
EEC TYPE-APPROVAL CERTIFICATE
Communication concerning the
- type-approval (7)
- extension of type-approval (8)
- refusal of type-approval (9)
- withdrawal of type-approval (10)
of a type of a vehicle/component/separate technical unit (11) with regard to Directive . . . . . ./. . . . . ./EEC, as last amended by Directive . . . . . ./. . . . . ./EEC.
Type-approval number:
Reason for extension:
Stamp of Administration Section I
0.2. Make (trade name of manufacturer):
0.2. Type and general commercial description(s):
0.3. Means of identification of type, if marked on the vehicle/component/separate technical unit (12) (13)
0.3.1. Location of that marking:
0.4. Category of vehicle. (14)
0.5. Name and address of manufacturer.
0.7. In the case of components and separate technical units, location and method of affixing of the EEC approval mark:
0.8. Name(s) and address(es) of assembly plant(s):
Section II
1 Additional information (where applicable): see Appendix
2 Technical service responsible for carrying out the tests:
3 Date of test report:
4 Number of test report:
5 Remarks (if any): see Appendix
6 Place:
7 Date:
8 Signature:
9 The index to the information package lodged with the approval authority, which may be obtained on request, is attached.
Appendix
to EEC approval certificate No. . . . . . .
concerning the type-approval of a vehicle with regard to Directive 78/549/EEC as last amended by Directive . . . . . ./. . . . . ./EEC
1. Additional information
1.1. Brief description of the vehicle with regard to the wheel guards:
1.2. Chain/tyre/wheel size combination on the front and/or rear axle specified by the manufacturer:
1.3. Tyre/wheel combination(s) including off-sets specified by the manufacturer:
5. Remarks.'
(1) Delete where not applicable.(2) Delete where not applicable.(3) If the means of identification of type contains characters not relevant to describe the vehicle, component or separate technical unit types covered by this type-approval certificate such characters shall be represented in the documentation by the symbol: '?' (e.g. ABC??123??)(4) As defined in Annex II A to Directive 70/156/EEC.
