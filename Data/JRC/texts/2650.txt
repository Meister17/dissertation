Commission Regulation (EC) No 1293/2005
of 5 August 2005
amending Regulation (EEC) No 2676/90 determining Community methods for the analysis of wines
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1493/1999 of 17 May 1999 on the common organisation of the market in wine [1], and in particular Article 46(3) thereof,
Whereas:
(1) The method for measuring the excess pressure in sparkling and semi-sparkling wines has been laid down in accordance with internationally recognised criteria. The International Vine and Wine Office (OIV) adopted the new description of this method at its General Assembly in 2003.
(2) Using this method will ensure simpler, more precise monitoring of excess pressure in these wines.
(3) The description of the usual method in Chapter 37 of the Annex to Commission Regulation (EEC) No 2676/90 [2] is no longer necessary and paragraph 3 of Chapter 37 should therefore be deleted. The updated description of this method should, moreover, be introduced in a new chapter of the Annex to that Regulation.
(4) Regulation (EEC) No 2676/90 should therefore be amended accordingly,
(5) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Wine,
HAS ADOPTED THIS REGULATION:
Article 1
The Annex to Regulation (EEC) No 2676/90 is hereby amended in accordance with the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 August 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 179, 14.7.1999, p. 1. Regulation as last amended by Commission Regulation (EC) No 1188/2005 (OJ L 193, 23.7.2005, p. 24).
[2] OJ L 272, 3.10.1990, p. 1. Regulation last amended by Regulation (EC) No 355/2005 (OJ L 56, 2.3.2005, p. 3).
--------------------------------------------------
ANNEX
The Annex to Regulation (EEC) No 2676/90 is hereby amended as follows:
1. Chapter "37. Carbon Dioxide" is amended as follows:
(a) paragraph 1 is amended as follows:
(i) the title is replaced by the following: "1. PRINCIPLE OF THE METHOD";
(ii) point 1.2 is deleted;
(b) in paragraph 2, the title of point 2.3 is replaced by the following title: "Calculation of the theoretical excess pressure";
(c) paragraphs 3 and 4 are deleted;
2. after Chapter 37, the following text is inserted as Chapter 37a:
"37a — MEASURING EXCESS PRESSURE IN SPARKLING AND SEMI-SPARKLING WINES
1. PRINCIPLE
Once the temperature has stabilised and the bottle has been shaken, the excess pressure is measured using an aphrometer (pressure gauge). It is expressed in pascals (Pa) (Type I method). The method also applies to aerated sparkling wines and aerated semi-sparkling wines.
2. EQUIPMENT
The apparatus measuring the excess pressure in bottles of sparkling and semi-sparkling wine is called an aphrometer. It takes different forms depending on how the bottle is stoppered (metal capsules, cap, cork or plastic stopper).
2.1. Bottles with capsules
The aphrometer is in three parts (Figure 1):
- the top part (a screw needle holder) is made up of a manometer, a manual tightening ring, an endless screw, which slips into the middle part, and a needle, which goes through the capsule. The needle has a lateral hole that transmits pressure to the manometer. A joint ensures that it is tightly sealed onto the capsule of the bottle,
- the middle part (or the nut) permits the centring of the top part. It is screwed into the lower part, holding the entire apparatus firmly onto the bottle,
- the lower part (clamp) is equipped with a spur that slips under the ring of the bottle, holding the entire apparatus together. There are rings adapted to fit every kind of bottle.
2.2. Bottles with corks
The aphrometer is in two parts (Figure 2):
- the top part is identical to the previous apparatus, but the needle is longer. It is made up of a long empty tube with a point on one end to help insert the needle through the cork. This point is detachable and falls into the wine once the cork has been pierced,
- the lower part is made up of a nut and a base sitting on the stopper. This is equipped with four tightening screws used to keep the whole apparatus on the stopper.
Figure 2 — Aphrometer for corks
Figure 1 — Aphrometer for capsules
Remarks concerning the manometers that equip these two types of apparatus:
- they can be either mechanical Bourdon tube-type devices or digital piezoelectric sensors. In the first case, the Bourdon tube must be made of stainless steel,
- they are graduated in pascals (Pa). For sparkling wine, it is more practical to use 105 pascals (105 Pa) or kilopascals (kPa) as the unit of measurement,
- they can be from different classes. The class of a manometer is the precision of a reading compared to a full-scale reading expressed as a percentage (e.g. manometer 1000 kPa class 1 signifies the maximum useable pressure 1000 kPa, reading at ± 10 kPa). Class 1 is recommended for precise measurements.
3. PROCEDURE
Measurements must be carried out on bottles where the temperature has stabilised for at least 24 hours. After piercing the crown, the cork or plastic stopper, the bottle must be shaken vigorously to reach a constant pressure, in order to take a reading.
3.1. Bottles with capsules
Slip the clamp over the spur binders under the ring of the bottle. Tighten the nut until the entire apparatus is tight on the bottle. The top part is screwed onto the nut. To avoid any gas escaping, the capsule should be pierced as quickly as possible to bring the joint into contact with the capsule. The bottle must be shaken vigorously until it reached a constant pressure, when a reading can be taken.
3.2. Bottles with corks
Place a point at the end of the needle. Position this on the cork. Tighten the four screws on the cork. Tighten top part (the needle goes through the cork). The point must fall into the bottle so that the pressure can be transmitted to the manometer. Take a reading after shaking the bottle until it reaches a constant pressure. Retrieve the point after taking the reading.
4. EXPRESSION OF RESULTS
The excess pressure at 20 °C (Paph20) is expressed in pascals (Pa) or kilopascals (kPa). This must be in accordance with the precision of the manometer (e.g. 6,3 × 105 Pa or 630 kPa and not 6,33 × 105 Pa or 633 kPa for a class 1 manometer with a full-scale reading of 1000 kPa).
When the measure of temperature is other than 20 °C, this must be corrected by multiplying the pressure measured by an appropriate coefficient (see Table 1).
Table 1
Ratio of Paph20 excess pressure in a sparkling or semi-sparkling wine at 20 °C with the Papht excess pressure at a temperature t
°C | |
0 | 1,85 |
1 | 1,80 |
2 | 1,74 |
3 | 1,68 |
4 | 1,64 |
5 | 1,59 |
6 | 1,54 |
7 | 1,50 |
8 | 1,45 |
9 | 1,40 |
10 | 1,36 |
11 | 1,32 |
12 | 1,28 |
13 | 1,24 |
14 | 1,20 |
15 | 1,16 |
16 | 1,13 |
17 | 1,09 |
18 | 1,06 |
19 | 1,03 |
20 | 1,00 |
21 | 0,97 |
22 | 0,95 |
23 | 0,93 |
24 | 0,91 |
25 | 0,88 |
5. CONTROLLING THE RESULTS
Method of direct determination of physical parameters (Type I criteria method)
Checking the aphrometers
Aphrometers must be checked regularly (at least once a year).
This is done using a calibration bench. This allows the manometer for testing to be compared with a high-quality reference manometer measured by national standards, mounted in parallel. The check compares the values indicated by the two apparatuses for rising, and then falling, levels of pressure. If there is a difference, the manometer can be regulated using an adjustment screw.
Laboratories and authorised organisations are all equipped with calibration benches; they are also available from manometer manufacturers."
--------------------------------------------------
