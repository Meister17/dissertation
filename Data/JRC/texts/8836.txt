COUNCIL REGULATION (EEC) No 2300/76 of 20 September 1976 concluding the Framework Agreement for commercial and economic cooperation between the European Communities and Canada
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 113 and 235 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Whereas the conclusion by the European Economic Community of the Framework Agreement for commercial and economic cooperation between the European Communities and Canada, signed in Ottawa on 6 July 1976, appears necessary for the attainment of the ends of the Community in the sphere of external economic relations ; whereas certain forms of economic cooperation provided for by the Agreement exceed the powers of action specified in the sphere of the common commercial policy,
HAS ADOPTED THIS REGULATION:
Article 1
The Framework Agreement for commercial and economic cooperation between the European Communities and Canada is hereby concluded on behalf of the European Economic Community.
The text of the Agreement is annexed to this Regulation.
Article 2
Pursuant to Article VIII of the Agreement, the President of the Council shall give notification that the procedures necessary for the entry into force of the Agreement have been completed on the part of the European Economic Community (2).
Article 3
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 September 1976.
For the Council
The President
M. van der STOEL (1)Opinion delivered on 14 September 1976 (not yet published in the Official Journal). (2)The date of entry into force of the Agreement will be published in the Official Journal of the European Communities.
