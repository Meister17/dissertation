Commission Regulation (EC) No 1555/2006
of 18 October 2006
amending Regulation (EC) No 1039/2006 on opening a standing invitation to tender for the resale on the Community market of sugar held by the intervention agencies of Belgium, the Czech Republic, Germany, Spain, Ireland, Italy, Hungary, Poland, Slovenia, Slovakia and Sweden
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 318/2006 of 20 February 2006 on the common organisation of the market in the sugar sector [1], and in particular Article 40(2)(d) thereof,
Whereas:
(1) The quantities for resale stated in Commission Regulation (EC) No 1039/2006 [2] reflect the state of the intervention stocks on 30 June 2006. Since this date, quantities have been sold by the intervention agencies and in the case of Germany, there are no intervention stocks remaining.
(2) Commission Regulation (EC) No 1539/2006 of 13 October 2006 adopting a plan allocating resources to the Member States to be charged against the 2007 budget year for the supply of food from intervention stocks for the benefit of the most deprived persons in the Community [3] states that 33224 tonnes of sugar in storage must be withdrawn from the Community's intervention stocks for distribution in the Member States in accordance with Annex I to this Regulation.
(3) These quantities should be taken into account in the sale by standing invitation to tender on the Community internal market.
(4) In order to ensure proper management of sugar in storage, provision should be made for a communication from the Member States on the quantities actually sold.
(5) Regulation (EC) No 1039/2006 should be amended accordingly.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Sugar,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1039/2006 is amended as follows:
1. The title is replaced by the following:
"Commission Regulation (EC) No 1039/2006 of 7 July 2006 opening a standing invitation to tender for the resale on the Community market of sugar held by the intervention agencies of Belgium, the Czech Republic, Spain, Ireland, Italy, Hungary, Poland, Slovenia, Slovakia and Sweden".
2. Article 1 is replaced by the following:
"Article 1
The intervention agencies of Belgium, Czech Republic, Spain, Ireland, Italy, Hungary, Poland, Slovenia, Slovakia and Sweden shall offer for sale by standing invitation to tender on the Community internal market a total quantity of 899896,41 tonnes of sugar accepted into intervention and available for sale on the internal market. The quantities involved per Member State are set out in Annex I.".
3. The following paragraph is added to Article 4:
"3. On the fifth working day at the latest after the Commission fixes the minimum sale price, the intervention agencies involved shall communicate to the Commission, in the form laid down in Annex III, the quantity actually sold by partial invitation to tender.".
4. Annex I is replaced by Annex I to this Regulation.
5. An Annex III of which the text is set out in Annex II to this Regulation is added.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 October 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 58, 28.2.2006, p. 1.
[2] OJ L 187, 8.7.2006, p. 3.
[3] OJ L 283, 14.10.2006, p. 14.
--------------------------------------------------
ANNEX I
"
"ANNEX I
Member States holding intervention sugar
Member State | Intervention agency | Quantities held by the intervention agency and available for the sale on the internal market |
Belgium | Bureau d’intervention et de restitution belge Rue de Trèves, 82 B-1040 Bruxelles Tél. (32-2) 287 24 11 Fax (32-2) 287 25 24 | 28648,00 |
Czech Republic | Státní zemědělský intervenční fond, oddělení pro cukr a škrob Ve Smečkách 33 CZ-11000 PRAHA 1 Tél. (420) 222 87 14 27 Fax (420) 222 87 18 75 | 34156,72 |
Spain | Fondo Español de Garantía Agraria Beneficencia, 8 E-28004 Madrid Tel (34) 913 47 64 66 Fax (34) 913 47 63 97 | 77334,00 |
Ireland | Intervention Section On Farm Investment Subsidies and Storage Division Department of Agriculture and Food Johnstown Castle Estate Wexford Ireland Tel. (353) 536 34 37 Fax (353) 914 28 43 | 12000,00 |
Italy | AGEA — Agenzia per le erogazioni in agricoltura Ufficio ammassi pubblici e privati e alcool Via Torino, 45 I-00185 Roma Tel.: (39) 06 49 499 558 Fax: (39) 06 49 499 761 | 494011,70 |
Hungary | Mezőgazdasági és Vidékfejlesztési Hivatal (MVH), Budapest (Agricultural and Rural Development Agency) Soroksári út 22–24 H-1095 Budapest Tél. (36-1) 219 62 13 Fax (36-1) 219 89 05 or (36-1) 219 62 59 | 141942,90 |
Poland | Agencja Rynku Rolnego Biuro Cukru Dział Dopłat i Interwencji Nowy Świat 6/12 00-400 Warszawa Tel.: (48-22) 661 71 30 Faks: (48-22) 661 72 77 | 13118,00 |
Slovenia | Agencija RS za kmetijske trge in razvoj podeželja Dunajska 160 SI-1000 Ljubljana Tel. (386-1) 580 77 92 Faks (386-1) 478 92 06 | 5647,00 |
Slovakia | Podohospodarska platobna agentura Oddelenie cukru a ostatných komodit Dobrovičova, 12 SK – 815 26 Bratislava Tél (4214) 58 24 32 55 Fax (4212) 53 41 26 65 | 34000,00 |
Sweden | Jordbruksverket Vallgatan 8 S-55182 Jönköping Tfn: (46-36) 15 50 00 Fax: (46-36) 19 05 46 | 59038,00" |
"
--------------------------------------------------
ANNEX II
"
"ANNEX III
Model for the notification to the Commission as referred to in Article 4(3)
Form [1]
Partial invitation to tender of … for the resale of sugar held by the intervention agencies
(Regulation (EC) No 1039/2006)
1 | 2 |
Member State selling intervention sugar | Quantity actually sold (in tonnes) |
"
[1] To be faxed to the following number: (32-2) 292 10 34."
--------------------------------------------------
