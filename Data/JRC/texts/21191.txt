Council Joint Action
of 21 May 2002
regarding a contribution of the European Union towards reinforcing the capacity of the Georgian authorities to support and protect the OSCE observer mission on the border of Georgia with the Ingush and Chechen Republics of the Russian Federation
(2002/373/CFSP)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union and in particular Article 14 thereof,
Whereas:
(1) On 20 July 2000, the Council adopted Joint Action 2000/456/CFSP regarding a contribution of the European Union towards reinforcing the capacity of the Georgian authorities to support and protect the OSCE observer mission on the border of the Republic of Georgia with the Chechen Republic of the Russian Federation(1), which expired on 31 December 2000.
(2) On 26 July 2001, the Council adopted Joint Action 2001/568/CFSP(2) with a view to ensuring the continued support of the European Union towards stability in the Caucasus region.
(3) From 28 May to 1 June 2001, General Sir Garry Johnson, as an expert of the European Union, carried out an assessment mission in which he concluded that there would be a need for additional EU support, should the OSCE mandate be extended to the Ingush sector.
(4) On 13 December 2001, the OSCE decided to expand the mandate of the OSCE mission to Georgia to observe and report on movement across the border between Georgia and the Ingush Republic of the Russian Federation,
HAS ADOPTED THIS JOINT ACTION:
Article 1
1. The European Union shall provide assistance to reinforce the capacity of the Georgian authorities to provide, through their border guards, support and protection for the OSCE observer mission on the border of Georgia with the Ingush Republic of the Russian Federation, as well as continued assistance in relation to the border of Georgia with the Chechen Republic of the Russian Federation.
2. For this purpose, the European Union shall provide financial support to the OSCE's mission to Georgia, and in particular its border monitoring operation, to cover expenditure related to certain equipment.
Article 2
1. The OSCE mission to Georgia shall be responsible for the procurement and handing over of the equipment.
2. The Commission shall conclude a financing agreement with the OSCE on the use of the European Union financial support, which shall take the form of a grant. The grant shall cover the procurement of items selected by the OSCE according to the needs of the Georgian authorities at both the Ingush and Chechen sections of the Georgian border.
3. The Commission, through its delegation in Tbilisi, shall liaise closely with the OSCE in order to monitor and evaluate the effective delivery of the equipment to the Georgian border guards and its further use.
4. In carrying out its activities, the Commission will cooperate, as appropriate, with local missions of Member States.
5. The Commission shall report on the implementation of the Action to the Council under the authority of the Presidency, assisted by the Secretary-General of the Council, High Representative for the CFSP.
Article 3
1. The financial reference amount for the purposes referred to in Article 1 shall be EUR 100000.
2. The expenditure financed by the amount stipulated in paragraph 1 shall be managed in accordance with the European Community procedures and rules applicable to the budget.
Article 4
This Joint Action shall enter into force on the day of its adoption. It shall expire 12 months after the financing agreement between the Commission and the OSCE has been concluded.
Article 5
This Joint Action shall be published in the Official Journal.
Done at Brussels, 21 May 2002.
For the Council
The President
R. De Miguel
(1) OJ L 183, 22.7.2000, p. 3.
(2) OJ L 202, 27.7.2001, p. 2.
