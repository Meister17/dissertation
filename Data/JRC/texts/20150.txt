COMMISSION REGULATION (EC) No 879/95 of 21 April 1995 amending Commission Regulation (EEC) No 3447/90 on special conditions for the granting of private storage aid for sheepmeat and goatmeat
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3013/89 of 25 September 1989 on the common organization of the market in sheepmeat and goatmeat (1), as last amended by the Act of Accession of Austria, Finland and Sweden, and in particular Article 7 (5) thereof,
Whereas Commission Regulation (EEC) No 3447/90 of 28 November 1990 on special conditions for the granting of private storage aid for sheepmeat and goatmeat (2), as last amended by Regulation (EEC) No 1258/91 (3), provides that applications in the context of private storage aid be submitted to the intervention agencies in each Member State; whereas as a result of the accession of Austria, Finland and Sweden the intervention agencies of these new Member States should be included in the Annex to this Regulation and the opportunity should be taken to correct the addresses of other intervention agencies;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Sheep and Goats,
HAS ADOPTED THIS REGULATION:
Article 1
The Annex to Regulation (EEC) No 3447/90 is hereby replaced by the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 21 April 1995.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 289, 7. 10. 1989, p. 1.
(2) OJ No L 333, 30. 11. 1990, p. 46.
(3) OJ No L 120, 15. 5. 1991, p. 15.
ANEXO - BILAG - ANHANG - ÐÁÑÁÑÔÇÌÁ - ANNEX - ANNEXE - ALLEGATO - BIJLAGE - ANEXO - LIITE - BILAGA
>TABLE>
