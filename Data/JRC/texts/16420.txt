COMMISSION REGULATION (EC) No 2141/96 of 7 November 1996 concerning the examination of an application for the transfer of a marketing authorization for a medicinal product falling within the scope of Council Regulation (EC) No 2309/93
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2309/93 of 22 July 1993 laying down Community procedures for the authorization and supervision of medicinal products for human and veterinary use and establishing a European Agency for the Evaluation of Medicinal Products (1), and in particular Articles 15 (4) and 37 (4) thereof,
Whereas Commission Regulation (EC) No 542/95 of 10 March 1995 concerning the examination of variations to the terms of a marketing authorization falling within the scope of Council Regulation (EEC) No 2309/93 (2) applies if the name and/or address of the holder of the marketing authorization changes but the holder of the said authorization remains the same;
Whereas appropriate provisions must therefore be adopted for the examination of an application for the transfer of a marketing authorization for a medicinal product granted in accordance with the provisions of Regulation (EEC) No 2309/93 where the new holder of the authorization is not the previous holder;
Whereas it is necessary in particular to institute an administrative procedure to enable the marketing authorization decision to be quickly amended in that event, provided that the transfer application submitted is valid and the conditions relating to it have been fulfilled;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Standing Committee on Medicinal Products for Human Use and the Standing Committee on Veterinary Medicinal Products,
HAS ADOPTED THIS REGULATION:
Article 1
This Regulation lays down the procedure for the examination of applications for the transfer of a marketing authorization granted in accordance with Regulation (EC) No 2309/93, except for the situations covered by point 3 of Annex I to Regulation (EC) No 542/95.
Definition
Article 2
For the purposes of this Regulation, 'transfer of a marketing authorization` means the procedure of changing the addressee (hereinafter referred to as 'the holder`) of the marketing authorization decision adopted pursuant to Article 10 (1) and (2) or Article 32 (1) and (2) of Council Regulation (EEC) No 2309/93, the new holder not being the previous holder.
Administrative procedure
Article 3
1. To obtain a transfer of a marketing authorization, the holder of this authorization shall submit an application to the European Agency for the Evaluation of Medicinal Products (hereinafter referred to as 'the Agency`), accompanied by the documents mentioned in the Annex to this Regulation.
2. Such an application shall only concern the transfer of one marketing authorization and shall be accompanied by the relevant fee provided for by Council Regulation (EC) No 297/95 on fees payable to the European Agency for the Evaluation of Medicinal Products (3).
Article 4
The Agency shall submit, within 30 days following receipt of an application within the meaning of Article 3 (2), an opinion concerning this application to the holder of the marketing authorization, to the person to whom the transfer shall be granted and to the Commission.
Article 5
The Agency's opinion referred to in Article 4 can only be unfavourable if the documents submitted in support of the application are incomplete or if it appears that the person to whom the transfer shall be granted is not established within the Community.
Article 6
In the case of a favourable opinion and without prejudice to the application of other provisions of Community law, the Commission shall immediately amend the decision taken in accordance with Articles 10 or 32 of Regulation (EEC) No 2309/93.
General and final provisions
Article 7
1. The transfer of the marketing authorization shall be authorized from the date of notification of the amendment of the Commission decision referred to in Article 6 (2).
2. The date on which the transfer actually takes place shall be set by the Agency by mutual agreement with the holder of the marketing authorization and the person to whom the transfer is to be granted. The Agency shall immediately inform the Commission of this date.
3. The transfer of a marketing authorization shall not affect any of the time limits provided for in Articles 13 and 35 of Regulation (EC) No 2309/93.
Article 8
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 7 November 1996.
For the Commission
Martin BANGEMANN
Member of the Commission
(1) OJ No L 214, 24. 8. 1993, p. 1.
(2) OJ No L 55, 11. 3. 1995, p. 15.
(3) OJ No L 35, 15. 2. 1995, p. 1.
ANNEX
Documents to be submitted to the Agency for the Evaluation of Medicinal Products pursuant to Article 3 (1)
The documents mentioned in points 1 to 4 must be authenticated by the signature of the holder of the marketing authorization and by that of the person to whom the transfer is to be granted.
1. The name of the medicinal product concerned by the authorization transfer, the authorization number(s) and the date(s) on which the authorization(s) was(were) granted.
2. The identification (name and address) of the holder of the marketing authorization to be transferred and the identification (name and address) of the person to whom the transfer is to be granted.
3. A document certifying that the complete and up-to-date file concerning the medicinal product or a copy of this file has been made available to or has been transferred to the person to whom the transfer is to be granted.
4. Without prejudice to the final decision, a document stating the date on which the person to whom the transfer is to be granted can actually take over all responsibilities, of the holder of the marketing authorization for the medicinal product concerned, from the previous holder.
5. The person to whom the transfer is to be granted shall provide documents showing his capacity to perform all the responsibilities required of a marketing authorisation holder under Community pharmaceutical legislation, in particular:
- a document identifying the qualified person within the meaning of Article 21 or Article 43 of Regulation (EEC) No 2309/93, together with a curriculum vitae and the address, telephone and fax number,
- as far as the medicinal products for human use are concerned, a document describing the scientific service in charge of information about the medicinal product within the meaning of Article 13 of Council Directive 92/28/EEC (1) including the address, the telephone and fax number.
6. The summary of the product's characteristics, the mock-ups of the outer and primary packaging as well as the leaflet bearing insert the name of the person to whom the transfer is to be granted.
(1) OJ No L 113, 30. 4. 1992, p. 13.
