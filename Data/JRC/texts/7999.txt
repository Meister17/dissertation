COMMISSION DIRECTIVE
of 30 October 1990
adapting to technical progress Council Directive 77/541/EEC on the approximation of the laws of the Member States relating to safety belts and restraint systems of motor vehicles
(90/628/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 77/541/EEC of 28 June 1977 on the approximation of the laws of the Member States relating to safety belts and restraint systems of motor vehicles (1), as last amended by the Act of Accession of Spain and Portugal, and in particular Article 10 thereof,
Whereas a comprehensive evaluation of Directive 77/541/EEC has shown that it is possible to improve road safety further by applying practical experience and technological development and taking into account the progress made in the Economic Community for Europe of the United Nations, notably in Regulation No 16, by
- inserting requirements for type-approval of 'harness belts` for special vehicle types,
- inserting requirements for type-approval of safety belts with preloaders,
- specifying installation of appropriate safety belts for all seating positions in all motor vehicle categories with minimal exemptions as necessary,
- introducing a model for a document certifying the actual installation of safety belts in a specific vehicle type to be approved,
- introducing test requirements for height adjustment devices for belts,
and
- introducing more stringent requirements on the conformity of production;
Whereas that experience shows that some existing definitions and requirements have to be adjusted slightly;
Whereas the protection of passengers, especially of passengers in buses and coaches, against ejection in case of an accident must be improved and further modifications to the Directive should be introduced for this purpose;
Whereas everything should be done in order that these modifications can be applied by 31 December 1991 at the latest;
Whereas the adoption of a Directive on the mandatory wearing of safety belts for all occupants of vehicles of less then 3,5 tonnes should be linked to a further modification of the Directive aiming to render mandatory the installation of three-point belts with retractors also for the rear outboard seats of such vehicles;
Whereas the provisions of this Directive are in accordance with the opinion of the Committee on the Adaptation to Technical Progress of the Directives on the removal of technical barriers to trade in motor vehicles,
HAS ADOPTED THIS DIRECTIVE:
Article 1
The Annexes to Directive 77/541/EEC are hereby amended in accordance with the Annex to this Directive.
Article 2
1. With effect from 1 May 1991 no Member State may
(a) on grounds relating to safety belts or restraint systems:
- refuse, in respect of a type of vehicle, to grant EEC type-approval, or to issue the copy of the certificate provided for in the last indent of Article 10 (1) of Council Directive 70/156/EEC (1), or to grant national type-approval, or
- prohibit the entry into service of vehicles
if the safety belts or restraint systems of this type of vehicle or of these vehicles have been approved in accordance with Directive 77/541/EEC, as amended by this Directive;
(b)
- refuse to grant EEC component type-approval in respect of a type of safety belt or restraint system, intended for installation in a vehicle, which complies with the requirements of Directive 77/541/EEC, as amended by this Directive,
- prohibit the placing on the market of such safety belts and restraint systems which bear the EEC component type-approval marks prescribed in this Directive.
2. With effect from 1 July 1992 Member States:
(a)
- shall no longer issue the copy of the certificate provided for in the last indent of Article 10 (1) of Directive 70/156/EEC in respect of a type of vehicle,
- may refuse to grant national type-approval in respect of a type of vehicle,
of which the safety belts or restraint systems have not been approved in accordance with Directive 77/541/EEC, as amended by this Directive;
(b)
- may refuse to grant EEC component type-approval in respect of a type of safety belt or restraint system, intended for installation in a vehicle, which does
not comply with the requirements of Directive 77/541/EEC, as amended by this Directive.
3. With effect from 1 July 1997 Member States:
- may prohibit the entry into service of vehicles of which the safety belts or restraint system have not been approved in accordance with Directive 77/541/EEC, as amended by this Directive,
- may prohibit the placing on the market of safety belts and restraint systems, intended for installation in a vehicle, which do not bear the EEC component type-approval marks prescribed in the Directive.
Article 3
Member States shall implement the provisions necessary to comply with this Directive before 1 May 1991. They shall forthwith inform the Commission thereof.
When Member States adopt these provisions, these shall contain a reference to this Directive or shall be accompanied by such reference at the time of their official publication. The procedure for such reference shall be adopted by Member States.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 30 October 1990.
For the Commission
Martin BANGEMANN
Vice-President
(1) OJ N° L 220, 29. 8. 1977, p. 95.
(1) OJ N° L 42, 23. 2. 1970, p. 1.
ANNEX
Annex I is amended as follows:
1.4.
Item 1.4: add at the end:
'. . . adjusting device, except in the case of a harness belt buckle`.
Add the following new item 1.8.6:
'1.8.6.
"Belt adjustment device for height'` means a device enabling the position in height of the upper loop of a belt to be adjusted according to the requirements of the individual wearer and the position of the seat. Such a device may be considered as a part of the belt or a part of the anchorage of the belt'.
After item 1.12, a new item 1.12.1 is added:
'1.12.1.
A "front passenger seat'` means any seat where the "foremost H-point'' of the seat in question is in or in front of the vertical transverse plane through the drivers R-point'.
Add the following new item 1.22:
'1.22.
"Preloading device'` means an additional or integrated device which tightens the strap in order to reduce the slack of the belt during a crash sequence'.
Item 2.1.2.1: modify the first sentence as follows:
'. . . drawings and,
- in the case of retractors, installation instructions for the sensing device shall be provided.
- for pre-loading devices or systems, a full technical description of the construction and function including the sensing device, if any, describing the method of activation and any necessary method to avoid inadvertent activation shall be provided.
The drawings must show . . .`
Add to item 2.1.2.1:
'If the belt is designed to be fixed to the vehicle structure through a belt adjustment device for height, the technical description shall specify whether or not this device is considered as a part of the belt;`
Add a new item 2.1.4 to read as follows:
2.1.4.
'The competent authority shall verify the existence of satisfactory arrangements for ensuring effective checks on conformity of production before type-approval is granted.`
2.4.2.1.
Item 2.4.2.1, modify the last sentence as follows:
'The parts of the buckle likely to contact the body of the wearer shall present a section of not less than 20 cm$ and at least 46 mm in width, measured in a plane situated at a maximum distance of 2,5 mm from the contact surface.
In the case of harness belt buckles, the latter requirement shall be regarded as satisfied if the contact area of the buckle with the wearer`s body is comprised between 20 and 40 cm$.'
Add at the end of item 2.4.2.3, as amended by Directive 82/319/EEC, the following new sentence (1):
'In the case of harness belt buckles, this test may be carried out without all the tongues being introduced.`
Modify item 2.4.4 as follows:
'2.4.4.
Attachments and belt adjustment devices for height
The attachments shall be tested for strength as prescribed in items 2.7.6.1 and 2.7.6.2. The actual belt adjustment devices for height shall be tested for strength as prescribed in item 2.7.6.2 of the present Directive where they have not been tested on the vehicle in application of amended Directive 76/115/EEC relating to anchorages for safety belts. These parts must not break or become detached under the tension set up by the prescribed load.`
Add the following new item 2.4.6:
'2.4.6.
Pre-loading device
2.4.6.1.
After being submitted to corrosion testing in accordance with item 2.7.2, the pre-loading device (including the impact sensor connected to the device by the original plugs but without any current passing through them) shall operate normally.
2.4.6.2.
It shall be verified that inadvertent operation of the device does not involve any risk of bodily injury for the wearer.
2.4.6.3.
In the case of pyrotechnic pre-loading devices:
2.4.6.3.1.
After being submitted to conditioning in accordance with item 2.7.10.2, operation of the pre-loading device must not have been activated by temperature and the device shall operate normally.
2.4.6.3.2.
Precautions shall be taken to prevent the hot gases expelled from igniting adjacent flammable materials.`
2.6.1.2.
Item 2.6.1.2, add at the end:
'In the case of safety belts with retractors, the retractor shall have been subjected to the dust resistance test laid down in item 2.7.7.3; in addition, in the case of safety belts or restraint system equipped with a pre-loading device comprising pyrotechnic means, the device shall have been subjected to the conditioning specified in item 2.7.10.2.`
Add the following new item 2.6.1.2.3:
'2.6.1.2.3.
In the case of a belt intended for use with a belt adjustment device for height, as defined in item 1.8.6 above, the test shall be carried out with the device adjusted in the most unfavourable position(s) chosen by the technical service responsible for testing. However if the adjustment device for height consists of the anchorage itself, as permitted by Directive 76/115/EEC, the technical service responsible for the tests can, if it so wishes, apply the provisions of item 2.7.8.1 hereafter.`
Add the following new item 2.6.1.2.4:
'2.6.1.2.4.
In the case of a safety belt with pre-loading device, one of the dynamic tests shall be carried out with the device in operation and the other with the device not in use.
In the first case:
2.6.1.2.4.1.
During the test the minimum displacements specified in item 2.6.1.3.2. above, may be reduced by half.
2.6.1.2.4.2.
After the test, the force measured as indicated in item 2.7.10.1 shall not exceed 100 daN.`
2.6.1.3.2.
Item 2.6.1.3.2, add after the first sentence:
'In case of a harness belt the minimum displacement specified for the pelvis may be reduced by half.` (remainder unchanged).
Item 2.7.6.1.
After the first sentence of item 2.7.6.1., as amended by Directive 82/319/EEC, add a new sentence to read:
'In the case of harness belts, the buckle shall be connected to the testing apparatus by the straps which are attached to the buckle and the tongue or two tongues located in an approximately symmetrical way to the geometric centre of the buckle.`
The rest of the paragraph is unchanged.
Item 2.7.6.2, amend to read:
'2.7.6.2.
The attachments and any belt adjustment devices for height shall be tested in the manner . . . with the strap completely unwound from the reel.`
Item 2.7.7.2.2, amend the second sentence as follows:
'The design of any such test apparatus shall ensure that the required acceleration is achieved before the webbing is drawn out of the retractor by more than 5 mm and that the withdrawal takes place at an average rate of increase of acceleration of at least 25 g/s and not more than 150 g/s.`
Item 2.7.8.1, add at the end the following new sentences:
'In that case, when the dynamic test has been carried out for a type of vehicle it need not be repeated for other types of vehicle where each anchorage point is less than 50 mm distant from the corresponding anchorage point of the tested belt. Alternatively, manufacturers may determine hypothetical anchorage position for testing in order to enclose the maximum number of real anchorage points. If the belt is equipped with a belt adjustment device for height such as defined in 1.8.6 above, the position of the device and the means of securing it shall be the same as those of the vehicle design.`
Correct the fifth line of the existing item 2.7.8.1 (English text) to read:
' . . .or with the data supplied by the manufacturer . . .`
Modify item 2.7.8.1.1 to add the following two sentences at the beginning:
'In the case of a safety belt or restraint system with pre-loading devices relying on component parts other than those incorporated in the belt assembly itself, the belt assembly shall be mounted in conjunction with the necessary additional vehicle parts on the test trolley in the manner prescribed in items 2.7.8.1.2 to 2.7.8.1.6.
Alternatively, in the case where those devices cannot be tested on the test trolley, the manufacturer may demonstrate by a conventional frontal impact test at 50 km/h in conformity with ISO procedure 3560 (1975/11/01 - Road vehicles - Frontal fixed barrier collision test method) that the device complies with the requirements of the Directive.`
Item 2.7.9.2. Amend the second sentence of item 2.7.9.2, as amended by Directive 82/319/EEC, to read:
'A load shall be applied to the buckle by direct traction via the straps tied to it so that all the straps are subjected to the force of 60/n daN; "n'` is the number of straps linked to the buckle when it is in a locked position, its minimum is deemed to be 2.'
Add the following new item 2.7.10:
'2.7.10.
Additional tests on safety belts with pre-loading devices
2.7.10.1.
The pre-loading force shall be measured in less than four seconds after the impact as close as possible to the contact point with the manikin on the free length of the strap between the manikin and the pre-loading device or sash guide, if any, the manikin having been replaced in its originally seated position if necessary.
2.7.10.2.
Conditioning
The pre-loading device may be separated from the safety belt to be tested and kept for 24 hours at a temperature of 60 p 5 gC. The temperature shall then raised to 100 p 5 gC for two hours. Subsequently it shall be kept for 24 hours at a temperature of 30 p 5 gC. After being removed from conditioning, the device shall warm up to ambient temperature. If it has been separated it shall be fitted again to the safety belt.`
Renumber item 2.8 to read:
'2.8.
Conformity of production
2.8.1.
Any safety belt or restraint system approved under this Directive shall be so manufactured as to conform to the type approved by meeting the requirements set forth in 2.3, 2.4, 2.5, 2.6 and 2.7 above.
2.8.2.
In order to verify that the requirements of item 2.8.1 are met, suitable controls of the production shall be carried out.
2.8.3.
The holder of the approval shall in particular:
2.8.3.1.
Ensure existence of procedures for the effective control of the quality of products;
2.8.3.2.
Have access to the control equipment necessary for checking the conformity of each approved type;
2.8.3.3.
Ensure that data of test results are recorded and that annexed documents remain available for a period to be determined in accordance with the administrative service;
2.8.3.4.
Analyse the results of each type of test, in order to verify and ensure the stability of the safety belt or restraint system characteristics, making allowance for variation of an industrial production;
2.8.3.5.
Ensure that, for each type of safety belt or restraint system, at least the tests prescribed in Annex XVI are carried out;
2.8.3.6.
Ensure that any samples or test pieces giving evidence of non-conformity with the type of test considered shall give rise to another sampling and another test. All the necessary steps shall be taken to re-establish the conformity of the corresponding production.
2.8.4.
The competent authority which has granted type-approval may at any time verify the conformity control methods applicable to each production unit.
2.8.4.1.
In every inspection, the test books and production survey records shall be presented to the visiting inspector.
2.8.4.2.
The inspector may take samples at random which will be tested in the manufacturer`s laboratory. The minimum number of samples may be determined according to the results of the manufacturer's own verification.
2.8.4.3.
When the quality level appears unsatisfactory, or when it seems necessary to verify the validity of the tests carried out in application of item 2.8.4.2, the inspector shall select samples to be sent to the technical service which has conducted the type-approval tests.
2.8.4.4.
The competent authority may carry out any test prescribed in this Directive.
2.8.4.5.
The normal frequency of inspections authorized by the competent authority shall be two per year. In the case where negative results are recorded during one of these inspections, the competent authority shall ensure that all necessary steps are taken to re-establish the conformity of production as rapidly as possible.'
Items 3.1 to 3.1.5 are replaced by the following:
'3.1.
Vehicle equipment (1)
3.1.1.
Any vehicle covered by Article 9, in categories M and N (except those vehicles which include places specially designed for standing passengers in categories M2 over 3,5 t and M3 must be equipped with safety belts or restraint systems which satisfy the requirements of this Directive.
3.1.2.
The types of safety belts or restraint systems for each seating position where installation is required shall be those specified in Annex XV (with which neither non-locking retractors (1.8.1) nor manually unlocking retractors (1.8.2) can be used). For all seating positions where lap belts type B are specified in Annex XV lap belts type Br3 are permitted except in the case that, in use, they retract to such an extent as to reduce comfort in a notable way after normal buckling up.
3.1.3.
However, for outboard seating positions, other than front, of vehicles of the category M1 shown in Annex XV and marked with the symbol o, the installation of a lap belt of the type B, Br3 or Br4m is allowed, where there exists a passage between a seat and the nearest side wall of the vehicle intended to permit access of passengers to other parts of the vehicle. A space between a seat and the side wall is considered as a passage, if the distance between that side wall, with all doors closed, and a vertical longitudinal plane passing throw the centreline of the seat concerned - measured at the R-point position and perpendicularly to the median longitudinal plane of the vehicle - is more than 500 mm.
3.1.4.
Where no safety belts are required any type of safety belt or restraint system conforming to this Directive may be provided at the choice of the manufacturer. A-type belts of the types permitted in Annex XV may be provided as an alternative to lap belts for those seating positions where lap belts are specified in Annex XV.
3.1.5.
On three point belts fitted with retractors, one retractor must operate at least on the diagonal strap.
3.1.6.
Except for vehicles of category M1 an emergency locking retractor of type 4N (1.8.5) may be permitted instead of a retractor of type 4 (1.8.4) where it has been shown to the satisfaction of the services responsible for the tests that the fitting of a type 4 retractor would not be practical.
3.1.7.
For the front outboard and front centre seating positions shown in Annex XV and marked with the symbol *, lap belts of the type specified on that Annex shall be considered adequate where the windscreen is located outside the reference zone defined in Annex II to Directive 74/60/EEC.
As regards safety belts, the windscreen is considered as part of the reference zone when it is capable of entering into static contact with the test apparatus according to the method described in Annex II to Directive 74/60/EEC.
3.1.8.
For all seating positions in Annex XV marked with the symbol ll=, lap belts of the types specified in Annex XV must be provided where an "exposed seating position'` as defined in 3.1.9 exists.
3.1.9.
An "exposed seating position'' is one where there is no "protective screen'' in front of the seat within the following defined space:
- between two horizontal planes, one through the H-point and the other 400 mm above it,
- between two vertical longitudinal planes which are symmetrical in relation to the H-point and are 400 mm apart,
- behind a transverse vertical plane 1,30 m from the H-point.
For the purpose of this requirement "protective screen'' means a surface of suitable strength and showing no discontinuities such that, if a sphere of 165 mm diameter is geometrically projected in a longitudinal horizontal direction through any point of the space defined above and through the centre of the sphere, nowhere in the protective screen is there any aperture through which the geometrical projection of the sphere could be passed.
A seat is considered to be an "exposed seating position'', if the protective screens within the space defined above have a combined surface area of less than 800 cm$.'
After item 3.2.2.3, a new item 3.2.2.4 is added:
'3.2.2.4.
The design and installation of every safety belt provided for each seating position shall be such as to be readily available for use. Furthermore, where the complete seat or the seat cushion and/or the seat back can be folded to permit access to the rear of the vehicle or to carry goods or luggage, after folding and restoring those seats to the seating position, the safety belts provided for those seats shall be accessible for use or can be easily recovered from under or behind the seat by one person, according to instructions in the vehicle users handbook, without the need for that person to have training or practice.`
Item 3.3.2, the fourth sentence is amended as follows:
'In the case of safety belts or restraint systems for front outboard seating positions, except if these are harness belts, the buckle shall also be capable of being locked in the same manner.`
The last sentence of item 3.3.2 is replaced by the following:
'A check shall be made to ensure that if the buckle is in contact with the wearer, the contact surface satisfies the requirements of 2.4.2.1 of this Annex.`
Annex II, item 1, add:
. . ./device for height adjustment of the upper loop
In Annex II after the type approval certificate the following model document shall be added:
Appendix 1
MODEL DOCUMENT
INSTALLATION OF SEAT BELTS AND RESTRAINT SYSTEMS
(Maximum format A 4)
Name of Administration issuing notification .
Notification concerning an evaluation of the installation of seat belts or restraint systems in a vehicle in accordance with item 3 to Annex I to Directive 77/541/EEC, as amended by Directives 81/576/EEC, 82/319/EEC and 90/628/EEC.
This notification is a summary of the main features of the installation and states whether the administration considered it to be satisfactory, unsatisfactory or no longer satisfactory.
Notification reference number .
1.
Trade name or mark of the motor vehicle .
2.
Vehicle type and category .
3.
Name and address of manufacturer .
.
4.
If applicable, name and address of his representative .
.
5.
Description on the seat belts or restraint systems. This shall include the following:
5.1.
Seat belts
Make .
Basic approval number .
Position on vehicle .
5.2.
Seat belt anchorages
Basic approval number .
5.3.
Seats and their anchorages
Basic approval number .
6.
Vehicle submitted for evaluation on .
7.
Technical service conducting the inspection .
.
8.
Date of report issued by that service .
9.
Number of report issued by that service .
10.
The installation is considered to be satisfactory/unsatisfactory/no longer satisfactory (1) in terms of 3.1 to 3.3.4 of Annex I
11.
Place .
12.
Date .
13.
The following documents, relating to this notification, can be obtained if specially requested: approval reports, photographs and/or sketches for the item listed at 5.1, 5.2 and 5.3.
14.
Signature: .
Annex III is amended as follows:
Item 1.1.1:
- add:
'9 for Spain
21
for Portugal`
- replace the letters 'GR` by 'EL`.
Add a new item 1.1.3.2.3 as follows:
'1.1.3.2.3.
the letter ''p'` in the case of a safety belt with a pre-loading device'.
Annex VII is amended as follows:
Item 3: add to the first paragraph the following new sentence:
'The tolerance on the position of the anchorage points is such that each anchorage point shall be situated at most at 50 mm from corresponding A, B and K indicated in figure 1, or A1, B1 and K, as the case
may be.`
Add the following item 3.1:
'3.1. In the case of a belt equipped with a belt adjustment device for height as defined in 1.8.6 of this Directive, this device shall be secured either to a rigid frame, or to a part of the vehicle on which it is normally mounted which shall be securely fixed on the test trolley.`
Figure 1 is replaced by the following figure:
>START OF GRAPHIC>
<?aa8K>Figure 1
<?aeFN6><?aa8N>TROLLEY, SEAT, ANCHORAGE
<?aePD8><?aeJA-2><?aeAD>C 1
C
C 2
725
C
C 1
C
C 2
125
125
<?aeAE><?aa8N>Stainless steel plate
(without painting)
Dimensions in mm
Tolerances ± 5 mm
>END OF GRAPHIC>
*
oe
ll=
Annex X is amended as follows:
Modify item 2 (g) to add at the end: '. . . or, when a seat belt is equipped with a pre-loading device, when the latter has been activated`.
Add the following new Annexes XV and XVI
ANNEX XV
SAFETY BELT INSTALLATION SHOWING THE BELT TYPES AND RETRACTOR TYPES
>TABLE>
ANNEX XVI
CONTROL OF CONFORMITY OF PRODUCTION
1.
TESTS
Safety belts shall be required to demonstrate compliance with the requirements on which the following tests are based:
1.1.
Verification of the locking threshold and durability of emergency locking retractors
According to the provisions of item 2.7.7.2, in the most unfavourable direction as appropriate after having undergone the durability testing detailed in items 2.7.2, 2.7.7.1 and 2.7.7.3, as a requirement of item 2.4.5.2.5.
1.2.
Verification of the durability of automatically locking retractors
According to the provisions of item 2.7.7.1, supplemented by the tests in items 2.7.2 and 2.7.7.3, as a requirement of 2.4.5.1.3.
1.3.
Test for strength of straps after conditioning
According to the procedure described in item 2.7.5, after conditioning according to the requirements of items 2.7.3.1 to 2.7.3.5.
1.3.1.
Test for strength of straps after abrasion
According to the procedure described in item 2.7.5, after conditioning according to the requirements described in item 2.7.3.6.
1.4.
Microslip test
According to the procedure described in item 2.7.4.
1.5.
Test of the rigid parts
According to the procedure described in item 2.7.6.
1.6.
Verification of the performance requirements of the safety belt or restraint system when subjected to the dynamic test
1.6.1.
Tests with conditioning
1.6.1.1.
Belts or restraint systems fitted with an emergency locking retractor: according to the provisions set out in items 2.7.8 and 2.7.9, using a belt which was previously been subjected to 45 000 cycles of the endurance test of the retractor prescribed in item 2.7.7.1 and to the tests defined in items 2.4.2.3, 2.7.2 and 2.7.7.3.
1.6.1.2.
Belts or restraint systems fitted with an automatically-locking retractor: according to the provisions set out in items 2.7.8 and 2.7.9, using a belt which has previously been subjected to 10 000 cycles of the endurance test of the retractor prescribed in item 2.7.7.1 and also to the tests prescribed in items 2.4.2.3, 2.7.2 and 2.7.7.3.
1.6.1.3.
Static belt: according to the provisions set out in items 2.7.8 and 2.7.9, on a safety belt which has been subjected to the test prescribed in items 2.4.2.3 and 2.7.2.
1.6.2.
Test without any conditioning
According to the provisions set out in items 2.7.8 and 2.7.9.
2.
TEST FREQUENCY AND RESULTS
2.1.
The frequency of testing to the requirements of items 1.1 to 1.5 shall be on a statistically controlled and random basis in accordance with one of the regular quality assurance procedures.
2.1.1.
Furthermore, in the case of emergency locking retractors, all assemblies shall be checked:
2.1.1.1.
either according to the provisions set out in items 2.7.7.2.1 and 2.7.7.2.2, in the most unfavourable direction, as specified in item 2.7.7.2.1.2. Test results shall meet the requirements of items 2.4.5.2.1.1 and 2.4.5.2.3.
2.1.1.2.
or according to the provisions set out in item 2.7.7.2.3, in the most unfavourable direction. Nevertheless, the speed of inclination can be more than the prescribed speed in so far as it does not affect the test results. Test results shall meet the requirements of item 2.4.5.2.1.4.
2.2.
In the case of verification of compliance with the dynamic test according to item 1.6 of this Annex, this shall be carried out with a minimum frequency of:
2.2.1.
Tests with conditioning
2.2.1.1.
In the case of belts fitted with an emergency locking retractor,
- where the daily production is greater than 1 000 belts: one in 100 000 belts produced, with a minimum frequency of one every two weeks,
- where the daily production is smaller than or equal to 1 000 belts: one in 10 000 belts produced, with a minimum frequency on one per year,
per sort of locking mechanism (1), shall be subjected to the test prescribed in item 1.6.1.1 of this Annex.
2.2.1.2.
In the case of belts fitted with an automatically-locking retractor and of static belts,
- where the daily production is greater than 1 000 belts: one in 100 000 belts produced, with a minimum frequency of one every two weeks,
- where the daily production is smaller than or equal to 1 000 belts: one in 10 000 belts produced, with a minimum frequency of one per year,
shall be subjected to the test prescribed in item 1.6.1.2 or 1.6.1.3 of this Annex respectively.
2.2.2.
Tests without conditioning
2.2.2.1.
In the case of belts fitted with an emergency locking retractor, the following number of samples shall be subjected to the test prescribed in item 1.6.2 of this Annex:
2.2.2.1.1.
for a production of not less than 5 000 belts per day, two belts per 25 000 produced with a minimum frequency of one per day, per sort of locking mechanism;
2.2.2.1.2.
for a production of less than 5 000 belts per day, one belt per 5 000 produced with a minimum frequency of one per year, per sort of locking mechanism.
2.2.2.2.
In the case of belts fitted with an automatically locking retractor and of static belts, the following number of samples shall be subjected to the test prescribed in item 1.6.2 of this Annex:
2.2.2.2.1.
for a production of not less than 5 000 belts per day, two belts per 25 000 produced with a minimum frequency of one per day, per approved type,
2.2.2.2.2.
for a production of less than 5 000 belts per day, one belt per 5 000 produced with a minimum frequency of one per year, per approved type.
2.2.3.
Results
Test results shall meet the requirements set out in item 2.6.1.3.1 of Annex I.
The forward displacement of the manikin may be controlled with regard to item 2.6.1.3.2 of Annex I (or item 2.6.1.4 where applicable) during a test performed with conditioning according to item 1.6.1 of this Annex by means of a simplified adapted method.
2.3.
Where a test sample fails a particular test to which it has been subjected, a further test to the same requirements shall be carried out on at least three other samples. In the case of dynamic tests if one of the latter fails the test, the holder of the approval or his duly accredited representative shall notify the competent authority which has granted type approval indicating what steps have been taken to re-establish the conformity of production.'
(1) OJ N° L 139, 19. 5. 1982, p. 17.
(1) In addition to the requirements of 3.1, Member States may, under national law, accept other types of safety belts or restraint systems covered by this Directive for certain vehicle types.
(1).
(1) Strike out what does not apply.
(1) For the purposes of this Annex "sort of locking mechanism'' means all emergency locking retractors whose mechanisms differ only in the lead angle(s) of the sensing device to the vehicle's reference axis system.
