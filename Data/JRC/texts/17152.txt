Council Decision
of 5 July 2004
appointing a Belgian member of the Economic and Social Committee
(2004/743/EC, Euratom)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 259 thereof,
Having regard to the Treaty establishing the European Atomic Energy Community, and in particular Article 167 thereof,
Having regard to Council Decision 2002/758/EC, Euratom of 17 September 2002 appointing the members of the Economic and Social Committee for the period from 21 September 2002 to 20 September 2006 [1],
Having regard to the nomination submitted by the Belgian Government,
Having obtained the opinion of the Commission of the European Union,
HAS DECIDED AS FOLLOWS:
Sole Article
Mr Tony VANDEPUTTE is hereby appointed a member of the Economic and Social Committee in place of Mr Wilfried BEIRNAERT for the remainder of the latter's term of office, which runs until 20 September 2006.
Done at Brussels, 5 July 2004.
For the Council
The President
G. Zalm
--------------------------------------------------
[1] OJ L 253, 21.9.2002, p. 9.
