Order of the Court of First Instance of 5 September 2006 — Alofs
v Commission
(Case T-239/99) [1]
(2006/C 261/58)
Language of the case: Dutch
The President of the Second Chamber Extended Composition has ordered that the case be removed from the register.
[1] OJ C 6, 8.1.2000.
--------------------------------------------------
