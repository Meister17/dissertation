Opinion of the European Central Bank
of 27 February 2006
concerning the proposal for a Council regulation laying down detailed rules for the implementation of Council Regulation (EC) No 2494/95 as regards the temporal coverage of price collection in the harmonised index of consumer prices
(CON/2006/13)
(2006/C 55/30)
On 24 February 2006 the European Central Bank (ECB) received a request from the Council of the European Union for an opinion on a proposal for a Council regulation laying down detailed rules for the implementation of Council Regulation (EC) No 2494/95 of 23 October 1995 concerning harmonized indices of consumer prices (the "HICP Regulation") [1] as regards the temporal coverage of price collection in the harmonised index of consumer prices (the "proposed regulation").
The ECB's competence to deliver an opinion is based on the first indent of Article 105(4) of the Treaty establishing the European Community and on Article 5(3) of the HICP Regulation. In accordance with the first sentence of Article 17.5 of the Rules of Procedure of the European Central Bank, the Governing Council has adopted this opinion.
1. The objective of the proposed regulation is to establish minimum standards for price collection periods within each month in order to improve the comparability of the harmonised indices of consumer prices (HICPs) across Member States and the reliability of the euro area HICP. Because price collection periods differ across Member States, for some items covered by the HICPs short-term variations of prices within a month may lead to significant differences in estimated price changes. The ECB welcomes the proposed regulation which requires that price collections take place across at least one working week, at or near the middle of the month, and across more than one working week for products that are known to show sharp and irregular price changes within a month. These minimum standards represent a compromise between the need for harmonising price collection across Member States, on the one hand, and the costs involved in changing existing price collection practices, on the other.
2. The requirement that products which show volatile prices shall be collected "over a period of more than one working week", allows Member States some leeway in the implementation of this proposed provision. Close monitoring of the effectiveness of the implementation of the proposed regulation, in this respect, is therefore warranted.
3. The ECB understands that the proposed regulation shall not preclude the release of provisional HICP or HICP flash estimates and shall not affect the current release timetable for the euro area HICP.
4. The ECB agrees with the proposed implementation from January 2007 onwards, given that no systematic effects on measured annual or monthly price changes are expected. Therefore, it is not expected that the revision of back-data will be necessary.
Done at Frankfurt am Main, 27 February 2006.
The President of the ECB
Jean-Claude Trichet
[1] OJ L 257, 27.10.1995, p. 1.
--------------------------------------------------
