Council Joint Action 2006/202/CFSP
of 27 February 2006
amending and extending Joint Action 2005/643/CFSP on the European Union Monitoring Mission in Aceh (Indonesia) (Aceh Monitoring Mission — AMM)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union, and, in particular Article 14 thereof,
Whereas:
(1) On 9 September 2005 the Council adopted Joint Action 2005/643/CFSP [1] establishing from 15 September 2005 a European Union Monitoring Mission in Aceh (Indonesia) (Aceh Monitoring Mission — AMM) in order to assist Indonesia in implementing the final agreement on Aceh, as set out in the Memorandum of Understanding (MoU) between the Government of Indonesia (GoI) and the Free Aceh Movement (GAM), signed on 15 August 2005.
(2) Article 15 of Joint Action 2005/643/CFSP provides that the Council shall, not later than 15 March 2006, evaluate whether the AMM should be extended.
(3) On 13 February 2006 the Government of Indonesia invited the EU to extend the mandate of the AMM for a further period of three months. GAM also indicated its support for such an extension of the mission.
(4) On 27 February 2006 the Council reiterated its support for the peace process in Aceh and confirmed its willingness to extend the mandate of the AMM for a further period of three months.
(5) Joint Action 2005/643/CFSP should be amended accordingly,
HAS ADOPTED THIS JOINT ACTION:
Article 1
Joint Action 2005/643/CFSP is hereby amended as follows:
1. Article 2(2) shall be replaced by the following:
"2. In particular, the AMM shall:
(a) monitor the reintegration of active GAM members;
(b) monitor the human rights situation and provide assistance in this field in the context of the task set out in (a) above;
(c) monitor the process of legislation change;
(d) rule on disputed amnesty cases;
(e) investigate and rule on complaints and alleged violations of the MoU;
(f) establish and maintain liaison and good cooperation with the parties.";
2. Article 4 shall be replaced by the following:
"Article 4
Structure of the AMM
The structure of the AMM, including the Headquarters and District offices, shall be specified in the OPLAN.";
3. In the second paragraph of Article 16 the date shall be replaced by the following date: " 15 June 2006".
Article 2
This Joint Action shall enter into force on the date of its adoption.
Article 3
This Joint Action shall be published in the Official Journal of the European Union.
Done at Brussels, 27 February 2006.
For the Council
The President
U. Plassnik
[1] OJ L 234, 10.9.2005, p. 13.
--------------------------------------------------
