Commission Decision
of 27 December 2004
authorising a method for grading pig carcases in Cyprus
(notified under document number C(2004) 5296)
(Only the Greek text is authentic)
(2005/7/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3220/84 of 13 November 1984 determining the Community scale for grading pig carcases [1], and in particular Article 5(2) thereof,
Whereas:
(1) Article 2(3) of Regulation (EEC) No 3220/84 provides that the grading of pig carcases must be determined by estimating the content of lean meat in accordance with statistically proven assessment methods based on the physical measurement of one or more anatomical parts of the pig carcase; the authorisation of grading methods is subject to compliance with a maximum tolerance for statistical error in assessment; this tolerance was defined in Article 3 of Commission Regulation (EEC) No 2967/85 of 24 October 1985 laying down detailed rules for the application of the Community scale for grading pig carcases [2].
(2) The Government of Cyprus has requested the Commission to authorise one method for grading pig carcases and has submitted the results of its dissection trial which was executed before the day of accession, by presenting part two of the protocol provided for in Article 3 of Regulation (EEC) No 2967/85.
(3) The evaluation of this request has revealed that the conditions for authorising this grading method are fulfilled.
(4) In Cyprus, commercial practice does not require that the tongue and the kidneys are removed from the pig carcase; this should be taken into account in adjusting the weight for standard presentation.
(5) No modification of the apparatus or the grading method may be authorised except by means of a new Commission Decision adopted in the light of experience gained; for this reason, the present authorisation may be revoked.
(6) The measures provided for in this Decision are in accordance with the opinion of the Management Committee for Pigmeat,
HAS ADOPTED THIS DECISION:
Article 1
The use of the following method is hereby authorised for grading pig carcases pursuant to Regulation (EEC) No 3220/84 in Cyprus:
- the apparatus termed "Hennessy Grading Probe (HGP 4)" and assessment methods related thereto, details of which are given in the Annex.
Article 2
Notwithstanding the standard presentation referred to in Article 2 of Regulation (EEC) No 3220/84, pig carcases may be presented in Cyprus with the tongue and kidneys attached before being weighted and graded. In order to establish quotations for pig carcases on a comparable basis, the recorded hot weight shall be reduced by 0,8 kg.
Article 3
Modifications of the apparatus or the assessment method shall not be authorised.
Article 4
This Decision is addressed to the Republic of Cyprus.
Done at Brussels, 27 December 2004.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 301, 20.11.1984, p. 1. Regulation as last amended by Regulation (EC) No 3513/93 (OJ L 320, 22.12.1993, p. 5).
[2] OJ L 285, 25.10.1985, p. 39. Regulation amended by Regulation (EC) No 3127/94 (OJ L 330, 21.12.1994, p. 43).
--------------------------------------------------
ANNEX
Methods for grading pig carcases in Cyprus
Hennessy Grading Probe (HGP 4)
1. Grading of pig carcases is carried out by means of the apparatus called "Hennessy Grading Probe (HGP 4)".
2. The apparatus shall be equipped with a probe of 5,95 millimetres diameter (and of 6,3 mm at the blade of the top of the probe) containing a photodiode (Siemens LED of the type LYU 260-EO) and photodetector of the type 58 MR and having an operating distance of between 0 and 120 millimetres. The results of the measurements shall be converted into estimated lean meat content by means of the HGP 4 itself or a computer linked to it.
3. The lean meat content of the carcase shall be calculated according to the following formula:
ý = 62,965 – 0,368X1 – 0,517X2 + 0,132 W
where:
ý = the estimated percentage of lean meat in the carcase,
X1 = the thickness of back-fat (including rind) in millimetres, measured at 8 cm off the midline of the carcase behind the last rib,
X2 = the thickness of back-fat (including rind) in millimetres, measured at 6 cm off the midline of the carcase between the third and fourth last rib,
W = the thickness of muscle in millimetres, measured at the same time and in the same place as X2.
The formula shall be valid for carcases weighing between 55 and 120 kilograms.
--------------------------------------------------
