Notice of initiation of an accelerated review of Council Regulation (EC) No 2603/2000 imposing a definitive countervailing duty on imports of certain polyethylene terephthalate originating, inter alia, in India
(2005/C 8/02)
The Commission has received an application for an accelerated review pursuant to Article 20 of Council Regulation (EC) No 2026/97 [1] ("the basic Regulation"), with regard to imports of certain polyethylene terephthalate originating, inter alia, in India, subject to a definitive countervailing duty imposed by Council Regulation (EC) No 2603/2000 [2].
1. Request for review
The application was lodged by South Asian Petrochem Limited ("the applicant"), an exporting producer in India.
2. Product
The product under review is polyethylene terephthalate with a coefficient of viscosity of 78 ml/g or higher, according to the DIN ("Deutsche Industrienorm") 53728, originating in India ("the product concerned"), normally declared within CN code 39076020. This CN code is given only for information.
3. Existing measures
The measure currently in force is a definitive countervailing duty imposed by Council Regulation (EC) No 2603/2000 under which imports into the Community of the product concerned originating, inter alia, in India, including those produced by the applicant, are subject to a definitive countervailing duty of EUR 41,3/tonne, with the exception of imports from several companies expressly mentioned which are subject to individual duty rates.
4. Grounds for the review
The applicant alleges that it was not investigated during the period of investigation on which the countervailing measure was based, i.e. the period from 1 October 1998 to 30 September 1999 ("the original investigation period") for reasons other than the refusal to cooperate. The applicant alleges that it did not export the product concerned to the Community during the original investigation period, and that it is not related to any of the exporting producers of the product which are subject to the existing measure.
The applicant further alleges that it has begun exporting the product concerned to the Community after the end of the original investigation period. On the basis of the above, it has requested that an individual duty rate be established for it.
5. Procedure
Community producers known to be concerned have been informed of the above application and have been given an opportunity to comment. No comments have been received.
Having determined, after consulting the Advisory Committee, that sufficient evidence exists to justify the initiation of an accelerated review, the Commission hereby initiates a review in accordance with Article 20 of the basic Regulation.
(a) Questionnaires
In order to obtain the information it deems necessary for its investigation, the Commission will send a questionnaire to the applicant.
(b) Collection of information and holding of hearings
Interested parties, provided they can show that they are likely to be affected by the results of the review, are hereby invited to present their views in writing, and submit replies to the questionnaire mentioned in point 5(a) of this notice or any other information to be taken into account during the review. This information and supporting evidence must reach the Commission within the time limit set in point 6(a) of this notice.
Furthermore, the Commission may hear interested parties, provided that they make a request showing that there are particular reasons why they should be heard. This request must be made within the time limit set in point 6(b) of this notice.
6. Time limits
(a) For parties to make themselves known, to submit questionnaire replies and any other information
All interested parties, if their representations are to be taken into account during the investigation, must make themselves known by contacting the Commission, present their views and submit a questionnaire reply to the questionnaire mentioned in point 5(a) of this notice or any other information within 40 days of the date of publication of this notice in the Official Journal of the European Union, unless otherwise specified. Attention is drawn to the fact that the exercise of most procedural rights set out in the basic Regulation depends on the party's making itself known within the aforementioned period.
(b) Hearings
All interested parties may also apply to be heard by the Commission within the same 40-day time limit.
7. Written submissions, questionnaire replies and correspondence
All submissions and requests made by interested parties must be made in writing (not in electronic format, unless otherwise specified) and must indicate the name, address, e-mail address, telephone and fax, and/or telex numbers of the interested party. All written submissions, including the information requested in this notice, questionnaire replies and correspondence provided by interested parties on a confidential basis shall be labelled as "Limited" [3] and, in accordance with Article 29(2) of the basic Regulation, shall be accompanied by a non-confidential version, which will be labelled "For inspection by interested parties".
Commission address for correspondence:
European Commission
Directorate General for Trade
Directorate B
Office: J-79 5/16
B-1049 Brussels
Fax (32-2) 295 65 05
Telex COMEU B 21877.
8. Non-cooperation
In cases in which any interested party refuses access to or does not provide the necessary information within the time limits, or significantly impedes the investigation, provisional or final findings, affirmative or negative, may be made in accordance with Article 28 of the basic Regulation, on the basis of the facts available.
Where it is found that any interested party has supplied false or misleading information, the information shall be disregarded and use may be made of the facts available. If an interested party does not cooperate or cooperates only partially, and findings are therefore based on facts available in accordance with Article 28 of the basic Regulation, the result may be less favourable to that party than if it had cooperated.
--------------------------------------------------
[1] OJ L 288, 21.10.1997, p.1. Regulation as last amended by Council Regulation (EC) No 461/2004, OJ L 77, 13.3.2004, p. 12.
[2] OJ L 301, 30.11.2000, p. 1.
[3] This means that the document is for internal use only. It is protected pursuant to Article 4 of Regulation (EC) No 1049/2001 of the European Parliament and of the Council (OJ L 145, 31.5.2001, p. 43). It is a confidential document pursuant to Article 29 of Council Regulation (EC) No 2026/97 (OJ L 288, 21.10.1997, p. 1) and Article 12 of the WTO Agreement on Subsidies and Countervaling Measures ("ASCM").
