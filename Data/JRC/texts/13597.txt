Order of the President of the Fourth Chamber of the Court of 19 May 2006 — Commission of the European Communities
v Kingdom of the Netherlands
(Case C-308/05) [1]
(2006/C 224/66)
Language of the case: Dutch
The President of the Fourth Chamber has ordered that the case be removed from the register.
[1] OJ C 243, 1.10.2004.
--------------------------------------------------
