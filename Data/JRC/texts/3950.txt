Commission Regulation (EC) No 1045/2005
of 4 July 2005
amending Regulation (EC) No 2760/98 concerning the implementation of a programme for cross-border cooperation in the framework of the Phare programme
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3906/89 of 18 December 1989 on economic aid to certain countries of central and eastern Europe [1], and in particular Article 8 thereof,
Whereas:
(1) The European Council decided at its meeting in Brussels on 17 and 18 June 2004 that Croatia shall be a candidate country for membership, and requested the Commission to prepare a pre-accession strategy for Croatia, including the necessary instruments,
(2) Council Regulation (EC) No 2257/2004 included Croatia among the beneficiary countries of the Phare pre-accession instrument as of 2 January 2005,
(3) Commission Regulation (EC) No 2760/98 [2] should, therefore, be amended to extend the Phare cross-border co-operation programme to include the Croatian borders with neighbouring countries,
(4) The measures provided for in this Regulation are in accordance with the opinion of the Committee for Economic Restructuring in certain countries of central and eastern Europe,
HAS ADOPTED THIS REGULATION:
Article 1
At the end of Article 2(1) of Regulation (EC) No 2760/98, the following text shall be added:
"(c) Croatia and Italy, Croatia and Slovenia, Croatia and Hungary, Croatia and Serbia and Montenegro, Croatia and Bosnia and Herzegovina."
Article 2
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 4 July 2005.
For the Commission
Olli Rehn
Member of the Commission
[1] OJ L 375, 23.12.1989, p. 11. Regulation as last amended by Regulation (EC) No 2257/2004 (OJ L 389, 30.12.2004, p. 1).
[2] OJ L 345, 19.12.1998, p. 49. Regulation as last amended by Regulation (EC) No 1822/2003 (OJ L 267, 17.10.2003, p. 9).
--------------------------------------------------
