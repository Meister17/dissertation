Information communicated by Member States regarding state aid granted under Commission Regulation (EC) No 70/2001 of 12 January 2001 on the application of Articles 87 and 88 of the EC Treaty to state aid to small and medium-sized enterprises
(2005/C 139/05)
(Text with EEA relevance)
Aid No: XS 12/02
Member State: Germany
Region: Baden-Württemberg
Title of aid scheme: Development Programme for Rural Areas (ELR), with amendments as at 1 January 2002
Legal basis: Landwirtschafts- und Landeskulturgesetz des Landes Baden-Württemberg i.d.F. vom 20.5.1994; Landeshaushaltsordnung des Landes Baden-Württemberg i.d.F. vom 15.12.1998
Annual expenditure planned or overall amount of individual aid granted to the company: About EUR 30 m.
1. Under the "Employment" priority, small (medium-sized) enterprises (defined as in Regulation (EC) No 69/2001) are granted assistance up to 15 % (7.5 %) of eligible expenditure, with a maximum of EUR 250000.
2. Under the "General service" priority, assistance up to 20 % of eligible expenditure, with a maximum of EUr 100000, can be granted. The de minimis rule (Regulation (EC) No 69/2001) is therefore complied with.
Date of implementation: 1 December 2002
Duration of scheme: Until 30 June 2007
Objective of aid:
The objective of the ELR is to maintain and further enhance living and working conditions, primarily for villages and municipalities in rural areas, through measures for structural improvements to counter depopulation and cushion structural changes in agriculture and thus address natural living conditions with care.
The Programme covers the priorities "Employment", "General services", "Housing" and "Common facilities". Commercial measures are assisted only under the first two priorities. Under the priority "Employment", only SMEs as defined in Regulation (EC) No 70/2001 are assisted. Under the priority "General services", only assistance that comes within the de minimis rule is granted. The figures on costs and aid intensity given above relate exclusively to these commercial measures.
Economic sectors concerned:
..XX. other areas of manufacturing
..XX other services
Name and address of the granting authority Ministerium für Ernährung und Ländlichen Raum
PO Box 103444
D-70029 Stuttgart
Other information Ständige Vertretung des Landes Baden-Württemberg bei der EU
Square Vergote 9
B-1200 Brussels
Aid number: XS 52/02
Member State: Austria
Title of aid scheme:
Scheme to promote technology transfer
sub-scheme B innovation management, soft aid, consultancy:
sub-scheme C cooperation and networks:
Legal basis: Allgemeine Rahmenrichtlinien für die Gewährung von Förderungen aus Bundesmitteln vom 7. Juni 1977
Annual expenditure planned under the scheme: Grants: approximately EUR 3.74 million on average
Maximum aid intensity: In the case of projects of small and medium-sized enterprises: maximum 50 % (gross)
Duration of scheme: 2002-2006 Until 31 December 2006 in accordance with European Commission requirements
- improving innovation management within enterprises
- improving transfer processes by establishing and testing innovative transfer models (cooperation models, networks and platforms) with a supra-regional catalyst action, which are aimed at increasing the level of innovation among enterprises, groups of enterprises and sectors (SMEs).
Economic sector(s) concerned: All economic sectors
Name and address of the granting authority Ministry for Economic Affairs and Labour
Stubenring 1
A-1011 Vienna
Frau Silvia Schmöller, Abt. VI/3
Tel. (431) 71100-5691
Fax (431) 71100-2036
E-Mail: silvia.schmoeller@bmwa.gv.at
Mag. Josef Mandl, Abteilungsleiter der Abt. VI/3
Tel. (431) 71100-5679
Fax (431) 71100-2036
E-Mail: josef.mandl@bmwa.gv.at
Other information: Sub-schemes B + C are being implemented by way of calls for tender.
Aid number: XS 91/03
Member State: United Kingdom
Region: North East of England
Title of aid scheme or name of Company receiving individual aid: Rivergreen Developments Plc
Legal Basis: Regional Development Agencies Act 1998
SOURCE OF FUNDS |
2003 | GBP 360800 |
2004 | GBP 139200 |
Total | GBP 500000 |
2003 | GBP 1441560 |
2004 | GBP 555957 |
Total | GBP 1997517 |
2003 | GBP 257015 |
2004 | GBP 99063 |
Total | GBP 356078 |
2003 | GBP 2059375 |
2004 | GBP 794220 |
Total | GBP 2853595 |
Grand Total | GBP 2853595 |
Maximum aid intensity: The maximum aid intensity will be 30 % (Assisted area 20 % for Sunderland under Art. 87(3)(c) + 10 % to reach the maximum aid intensity of 30 %)
Date of implementation: 1 August 2003
Duration of scheme or individual aid award: 31 August 2005
Objective of aid:
The project involves the creation of high quality new workspace accommodation units and associated office units on a brownfield site located in Pallion, Sunderland. The units will target SMEs over 36 months old.
The project is a new build and will comprise accommodation of high quality and well-designed infrastructure in an area of high unemployment and social deprivation.
The desired outcome of the project is to facilitate sustainable economic development that will generate employment in Pallion and the subregion.
Economic sector(s) concerned: All eligible industry sectors (Without prejudice to special rules concerning state aid in certain sectors — Art. 1(2) of the SME block exemption regulations)
Name and address of granting authority Ann Beavers
European Programme Secretariat
Government Office North East
Wellbar House
Gallowgate
NEWCASTLE UPON TYNE
NE1 4TD
Mr M Henning
Commercial Manager
Rivergreen Development Plc
Aykley Heads Business Centre
AYKLEY HEADS
Durham
DH1 5TS
Aid number: XS 111/03
Member State: Italy
Region: Friuli-Venezia Giulia
Title of aid scheme: Subsidised loans to craft firms for business investment.
Legal basis: Regolamento approvato con decreto del presidente della regione n. 131/pres. del 10.5.2003
(in euros) |
2003 | 2004 | 2005 |
3770000 | 3500000 | 3500000 |
(a) 15 % gge for small firms; 7.5 % gge for medium-sized firms;
(b) for firms in areas eligible for the derogation under Article 87(3)(c) of the EC Treaty: the intensity of aid in the form of subsidised loans must not exceed 8 % nge, plus 10 % gge for small firms and 6 % gge for medium-sized firms.
Date of implementation: 10 September 2003
Duration of scheme: Until 31 December 2006
Objective of aid: The aid is intended for investment by small and medium-sized firms to purchase, build, expand and modernise workshops and to purchase machinery, vehicles (except in the transport sector), equipment and furniture for the exclusive use of the firm.
Economic sector(s) concerned: All sectors except for activities referred to in Article 1(2) of Commission Regulation (EC) No 70/2001 of 12 January 2001.
Name and address of granting authority Regione Autonoma Friuli Venezia Giulia
Direzione regionale dell'Artigianato e della cooperazione
Servizio per lo sviluppo dell'artigianato
Via Giulia 75/1 — I-34100 Trieste
Tel. (040) 377 48 34
Fax (040) 377 48 10
e-mail: dir.art.coop@regione.fgv.it
Other information: From 2005 the new definition of small and medium-sized enterprises and micro enterprises will apply.
Aid number: XS113/03
Member State: Italy
Region: Lombardy
Title of aid scheme: "Support for integrated investment in industrial districts and "metadistricts" in Lombardy: support for investment and innovation"
Legal basis: DGR VII/13321 — 13.6.2003; DGR VII/11384 - 29.11.2002; DGR VII/3839 — 16.3.2001; VII/6356 5.10.2001; L.R. 24.3.2003 n. 3
Annual expenditure planned under the scheme or overall amount of individual aid granted: The annual expenditure planned for overall financing of policies to support investment in industrial "metadistricts" and districts (support for investment and innovation and support for research and development activities) is approximately EUR 60 million for the next three years. The budget will be established each year by the regional authorities.
- tangible and intangible fixed assets, as defined in Article 2 of Regulation (EC) No 70/2001: 7.5 % for medium-sized firms and 15 % for small firms.
Where the investment is carried out in areas eligible for the derogation under Article 87(3)(c) of the EC Treaty, the amount shall be 8 % nge + 6 % gge for medium-sized firms and 8 % nge + 10 % gge for small firms;
- services supplied by external consultants:These services must not be continuous or regular nor relate to the firm's normal operating expenditure, in accordance with Article 5 of Regulation (EC) No 70/2001: 50 % of the cost of services supplied externally;
- costs relating to participation in fairs and exhibitions in accordance with Article 5(b) of Regulation (EC) No 70/2001: 50 % of the additional costs incurred for renting, setting up and running the stand.
Date of implementation: Aid may be granted under the scheme from 1 January 2004.
Duration of scheme or individual aid: 31 December 2006
Objective of aid: Support for SMEs in industrial districts and "metadistricts" in Lombardy
Economic sector(s) concerned Food biotechnology
Non-food biotechnology
Fashion
Design
New materials
Name and address of granting authority Regione Lombardia — Direzione Generale Industria, PMI, Cooperazione e Turismo
Via Pola, 14 — I-20124 Milano
Aid number: XS 128/02
Member State: Germany
Region: Lower Saxony (rural district of Holzminden)
Title of aid scheme: Guidelines for the promotion of productive investment by individual SMEs in the rural district of Holzminden
Legal basis: § 108 der Niedersächsischen Landkreisordnung (NLO) in der Fassung vom 22.8.1996 (Niedersächsisches Gesetz- und Verordnungsblatt S. 365) i. V. mit § 65 der Niedersächsischen Gemeindeordnung (NGO) in der Fassung vom 22.8.1996 (Niedersächsisches Gesetz- und Verordnungsblatt S. 382)
Annual expenditure planned under the scheme: EUR 50000
- in the case of small enterprises, to no more than 15 %
- and in the case of medium-sized enterprises, to no more than 7.5 %
of eligible investment expenditure. The aid ceiling is set at EUR 7500. The grant may be increased by as much as 5 % in the case of the recruitment of women, those receiving social assistance, unemployed young people under 25 and the long-term unemployed.
The rules on the cumulation of aid are complied with.
Date of implementation: 1 January 2003
Duration of scheme: 1 January 2003 to 31 December 2006
- Setting-up of an establishment if the investment creates at least one full-time job;
- extension of an establishment if the number of long-term jobs is increased by 15 % as compared with the situation before the start of the investment;
- rationalisation of an establishment if this helps to maintain it as a going concern and to safeguard jobs;
- acquisition of an establishment that has been closed or is threatened with closure, provided that this is done under market conditions.
The long-term jobs created must be maintained for at least three years after payment of the grant. The aid takes the form of investment grants. All depreciable fixed assets relating to physical and intangible assets are eligible.
Economic sector(s) concerned: Applications may be submitted by SMEs with fewer than 250 employees in industry, crafts, the distributive trade and the building, transport, services and hotel sectors and by start-ups in these areas the registered office or place of business of which is in the rural district of Holzminden, excluding certain parts of the town of Holzminden itself. Aid may not be granted to firms in sensitive sectors.
Name and address of the granting authority Landkreis Holzminden
Bürgermeister-Schrader-Straße 24
D-37603 Holzminden
Other information Angela Schürzeberg, tel. 05531/707 — 276
Elke Gresens, tel. 05531/707 — 275
Sonja Schaper, tel. 05531/707 — 275
E-mail address: wirtschaftsfoerderung@landkreis-holzminden.de
--------------------------------------------------
