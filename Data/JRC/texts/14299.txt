[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 31.3.2006
COM(2006) 147 final
2006/0051 (ACC)
2006/0052 (ACC)
Proposal for a
COUNCIL DECISION
on the conclusion of an Agreement in the form of an Exchange of Letters between the European Community and the Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu
Proposal for a
COUNCIL REGULATION
concerning the implementation of the Agreement concluded by the EC following negotiations in the framework of Article XXIV.6 of GATT 1994, amending Annex I to Regulation (EEC) No 2658/87 on the tariff and statistical nomenclature and on the Common Customs Tariff
(presented by the Commission)
EXPLANATORY MEMORANDUM
1. In the context of enlarging the customs union the provisions of GATT Article XXIV.6 oblige the EC to enter into negotiations with third countries having negotiating rights in any of the acceding Members in order to agree on compensatory adjustment if the adoption of the EC’s external tariff regime results in an increase in tariff beyond the level for which the acceding country has bound itself at the WTO, whilst taking ‘due account to reductions of duties on the same tariff line made by other constituents of the customs union upon its formation’.
2. On 22 March 2004, the Council authorised the Commission to open negotiations under Article XXIV.6 of the GATT 1994 (COM proposal 6792/04 WTO 34).
3. Negotiations have been conducted by the Commission in consultation with the Committee established by Article 133 of the Treaty and within the framework of the negotiating directives issued by the Council.
4. The Commission has negotiated with the Members of the WTO holding negotiations rights with respect to the withdrawal of specific concessions in relation to the withdrawal of the schedules of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic, in the course of the accession to the European Community.
5. The negotiations have resulted in an Agreement in the form of an exchange of letters with the Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu.
6. This proposal asks the Council to approve this agreement.
2006/0051 (ACC)
Proposal for a
COUNCIL DECISION
on the conclusion of an Agreement in the form of an Exchange of Letters between the European Community and the Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 in conjunction with the first sentence of the first subparagraph of Article 300(2) thereof,
Having regard to the proposal from the Commission,
Whereas:
1. On 22 March 2004 the Council authorised the Commission to open negotiations with certain other Members of the WTO under Article XXIV.6 of the GATT 1994, in the course of the accessions to the European Community of Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic.
2. Negotiations have been conducted by the Commission in consultation with the Committee established by Article 133 of the Treaty and within the framework of the negotiating directives issued by the Council.
3. The Commission has finalised negotiations for an Agreement in the form of an Exchange of Letters between the European Community and the Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu. This agreement should be approved.
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement in the form of an Exchange of Letters between the European Community and the Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu with respect to the withdrawal of specific concessions in relation to the withdrawal of the schedules of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the course of accession to the European Community; are hereby approved on behalf of the Community.
The text of the Agreement is annexed to this Decision.
Article 2
The President of the Council is hereby authorised to designate the person(s) empowered to sign the Agreement in the form of an Exchange of Letters referred to in Article 1 in order to bind the Community.
Done at Brussels,
For the Council
The President
AGREEMENT
In the form of an exchange of letters between the European Community and the Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu
relating to the modification of concessions in the Schedules of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the course of accession to the European Community.
Letter from the European Communities
Sir,
Following the initiation of negotiations between the European Communities (EC) and the Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu under Article XXIV:6 and Article XXVIII of GATT 1994 for the modification of concessions in the Schedules of Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the course of their accession to the EC, the following is agreed between the EC and the Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu in order to conclude the negotiations initiated following the EC’s notification of 19 January 2004 to the WTO pursuant to Article XXIV:6. of GATT 1994
The EC agrees to incorporate in its Schedule for the customs territory of EC 25, the concessions that were included in its previous schedule of EC 15.
The EC agrees that it will incorporate in its schedule for the EC 25 the following concession:
87120030 (bicycles not motorized): A lowering of the current bound EC duty of 15% to 14.0%.
This Agreement shall enter into force upon the exchange of letters of agreement, following consideration by the parties in accordance with their own procedures. The EC shall use its best endeavours to put in place the appropriate implementing measures before 1 March 2006 and under no circumstances later than 1 July 2006.
On behalf of the European Community
AGREEMENT
In the form of an exchange of letters between the European Community and the Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu
relating to the modification of concessions in the Schedules of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the course of accession to the European Communities.
Letter from the Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu
Sir,
Reference is made to your letter stating:
“Following the initiation of negotiations between the European Communities (EC) and the Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu under Article XXIV:6 and Article XXVIII of GATT 1994 for the modification of concessions in the Schedules of Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the course of their accession to the EC, the following is agreed between the EC and the Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu in order to conclude the negotiations initiated following the EC’s notification of 19 January 2004 to the WTO pursuant to Article XXIV:6. of GATT 1994
The EC agrees to incorporate in its Schedule for the customs territory of EC 25, the concessions that were included in its previous schedule of EC 15.
The EC agrees that it will incorporate in its schedule for the EC 25 the following concession:
87120030 (bicycles not motorized): A lowering of the current bound EC duty of 15% to 14.0%.
This Agreement shall enter into force upon the exchange of letters of agreement, following consideration by the parties in accordance with their own procedures. The EC shall use its best endeavours to put in place the appropriate implementing measures before 1 March 2006 and under no circumstances later than 1 July 2006.”
I hereby have the honour to express my government’s agreement.
On behalf of the Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu
EXPLANATORY MEMORANDUM
4. Reference is made to Council Decision XXXX, regarding the conclusion of an Agreement with the Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu following negotiations in the framework of Article XXIV:6 of the GATT 1994.
5. The present proposal for a Council Regulation implements the Agreement entered into by the Community.
2006/0052 (ACC)
Proposal for a
COUNCIL REGULATION
concerning the implementation of the Agreement concluded by the EC following negotiations in the framework of Article XXIV.6 of GATT 1994, amending Annex I to Regulation (EEC) No 2658/87 on the tariff and statistical nomenclature and on the Common Customs Tariff
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 thereof,
Having regard to the proposal from the Commission[1],
Whereas:
(1) Council Regulation (EEC) No 2658/87(1) established a goods nomenclature, hereinafter referred to as the "Combined Nomenclature", and set out the conventional duty rates of the Common Customs Tariff.
(2) By its decision XX/XXX/EC concerning the conclusion of Agreements in the form of an Exchange of Letters between the European Community and the Separate Customs Territory of Taiwan, Penghu, Kinmen and Matsu, the Council approved, on behalf of the Community, the before mentioned Agreement with a view to closing negotiations initiated pursuant to Article XXIV:6 of GATT 1994.
HAS ADOPTED THIS REGULATION:
Article 1
Annex I to Regulation (EEC) No 2658/87 shall be amended with the tariffs shown in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union .
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels,
For the Council
The President
ANNEX
Notwithstanding the rules for the interpretation of the Combined Nomenclature, the wording for the description of the products is to be considered as having no more than an indicative value, the concessions being determined, within the context of this Annex, by the coverage of the CN codes as they exist at the time of adoption of the current regulation. Where ex CN codes are indicated, the concessions are to be determined by application of the CN code and corresponding description taken together.
Part Two Schedule of Customs duties. |
CN code | Description | Duty rate |
8712 0030 | Bicycles not motorized | Implemented through Council Regulation XXX |
[1] OJ C [..],[..], p. [..].
