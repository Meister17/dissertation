Council Regulation (EC) No 962/2006
of 27 June 2006
amending Regulation (EC) No 2505/96 opening and providing for the administration of autonomous Community tariff quotas for certain agricultural and industrial products
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 26 thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) On 20 December 1996, the Council adopted Regulation (EC) No 2505/96 opening and providing for the administration of autonomous Community tariff quotas for certain agricultural and industrial products [1]. Community demand for the products in question should be met under the most favourable conditions. For that purpose, new Community tariff quotas should be opened at reduced or zero rates of duty for appropriate volumes while avoiding any disturbance to the markets for these products.
(2) The quota amount for an autonomous Community tariff quota is insufficient to meet the needs of the Community industry for the current quota period and should therefore be increased.
(3) Regulation (EC) No 2505/96 should therefore be amended accordingly,
HAS ADOPTED THIS REGULATION:
Article 1
The tariff quotas listed in the Annex to this Regulation shall be added to Annex I to Regulation (EC) No 2505/96.
Article 2
For the quota period from 1 January to 31 December 2006, in Annex I to Regulation (EC) No 2505/96 the amount of the tariff quota for the order number 09.2986 shall be fixed at 14315 tonnes.
Article 3
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
It shall apply from 1 July 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 27 June 2006.
For the Council
The President
J. Pröll
[1] OJ L 345, 31.12.1996, p. 1. Regulation as last amended by Regulation (EC) No 151/2006 (OJ L 25, 28.1.2006, p. 1).
--------------------------------------------------
ANNEX
Order No | CN code | TARIC subdivision | Description | Quota volume | Quota duty % | Quota period |
"09.2967 | 70112000 | 30 | Glass face plate with a diagonal measurement of 63 cm (± 0,2 cm) from the outer edge to the outer edge and having a light transmission of 56,8 % (± 3 %) by a glass thickness of 10,16 mm | 150000 units | 0 | 1.7. to 31.12.2006 |
09.2976 | ex84079010 | 10 | Four-stroke petrol engines of a cylinder capacity not exceeding 250 cm3 for use in the manufacture of lawnmowers of subheading 843311 or mowers with motor of subheading 84332010 [1] | 2500000 units | 0 | 1.7.2006 to 30.6.2007 |
09.2977 | 29261000 | | Acrylonitrile | 40000 tonnes | 0 | 1.7. to 31.12.2006 |
09.2986 | ex38249099 | 76 | Mixture of tertiary amines containing: 60 % by weight of dodecyldimethylamine, or more,20 % by weight of dimethyl(tetradecyl)amine, or more,0,5 % by weight of hexadecyldimethylamine, or more,for use in the production of amine oxides [1] | 14315 tonnes | 0 | 1.1. to 31.12.2006 |
[1] Checks on this prescribed end use shall be carried out pursuant to the relevant Community provisions."
--------------------------------------------------
