Improving Lexical Mapping Model of English-Korean Bitext Using Structural Features
by Seonho Kim, Juntae Yoon, and Mansuk Song

References

Adam Berger, Stephen Della Pietra, and Vincent Della Pietra. 1996. A maximum entropy approach to natural language processing. Computational Linguistics.
Eric Brill. 1995. Transformation-based error-driven learning and natural language processing: A case study in part-of-speech tagging. Computational Linguistics.
Peter F. Brown, Stephen Della Pietra, Vincent Della Pietra, Robert Mercer. 1993. The mathematics of statistical machine translation: parameter estimation. Computational Linguistics.
A. P. Dempster, N. M. Laird, and D. B. Rubin. 1976. Maximum likelihood from incomplete data via the EM algorithm. The Royal Statistics Society.
Pascale Fung and Kenneth W. Church. 1994. Kvec: A new approach for aligning paralle texts. COLING.
William A. Gale and Kenneth W. Church. 1991. Identifying Word Correspondance in Parallel Text. Proc. of DARPA/NLP Workshop.
William A. Gale and Kenneth W. Church. 1993. A program for aligning sentences in bilingual corpora. Computational Linguistics.
Fred Jelinek. 1993. Statistical Methods for Speech Recognition. MIT Press.
Julian Kupiec. 1993. An algorithm for finding noun phrase correspondences in bilingual corpora. ACL.
Yuji Matsumoto. 1993. Structural matching of parallel texts. ACL.
I. Dan Melamed. 1997. A word-to-word model of translation equivalence. ACL/EACL.
Franz Josef Och and Hermann Ney. 2000. Improving statistical alignment models. ACL.
H. Papageorgiou, L. Cranias and S. Piperidis. 1994. Automatic alignment in parallel corpora. ACL.
Stephen Della Pietra, Vincent Della Pietra, John D. Lafferty. 1997. Inducing features of random fields. IEEE Transactions on Pattern Analysis and Machine Intelligence.
Kengo Sato. 1998. Maxmium Entropy model learning of the translation rules. ACL COLING.
Jung H. Shin, Young S. Han, and Key-Sun Choi. 1996. Bilingual knowledge acquisition from Korean-English parallel corpus using alignment method. COLING.
Frank Smadja, Kathleen R. McKeown, and Vasileios Hatzivassiloglou. 1996. Translating collocations for bilingual lexicons: a statistical approach. Computational Linguistics.
Christoph Tillmann and Hermann Ney. 2000. Word re-ordering and dp-based search in statistical machine translation. COLING.
Ye-Yi Wang and Alex Waibel. 1998. Modeling with structures in machine translation. ACL COLING.
Hideo Watanabe, Sadao Kurohashi, and Eiji Aramaki. 2000. Finding structural correspondances from bilingual parsed corpus for corpus-based translation. COLING.
Dekai Wu. 1997. Stochastic inversion transduction grammar and bilingual parsing of parallel corpora. Computational Linguistics.
Kaoru Yamamoto and Yuji Mtsumoto. 2000. Acquisition of phrase-level bilingual corrspondance using dependency structure. COLING.
Juntae Yoon, Chunghee Lee, Seonho Kim, and Mansuk Song. 1999. Morphological analyzer of Yonsei Univ., Morany: Morphological analysis based on large lexical database extracted from corpus. Proc. of Hangule and Korean Information Processing Workshop.
