References

1   Nancy Chinchor. 1997. MUC-7 Named Entity Task Definition, Version 3.5. http://www.itl.nist.gov/iaui/894.02/related_projects/muc/proceedings/ne_task.html. 

2   Lisa Ferro, Inderjeet Mani, Beth Sundheim, and George Wilson. 2001. Tides temporal annotation guidelines, version 1.0.2. Technical report, The MITRE Corporation. 

3   GENIA. 2003. Genia project home page. http://www-tsujii.is.s.u-tokyo.ac.jp/~genia. 

4   Bob Ingria and James Pustejovsky. 2002. TimeML Specification 1.0 (internal version 3.0.9), July. http://www.cs.brandeis.edu/%7Ejamesp/arda/time/documentation/TimeML-Draft3.0.9. html. 

5   James Pustejovsky, Roser Sauri, Andrea Setzer, Rob Giazauskas, and Bob Ingria. 2002. TimeML Annotation Guideline 1.00 (internal version 0.4.0), July. http://www.cs.brandeis.edu/%7Ejamesp/arda /time/documentation/TimeML-Draft3.0.9.html. 

6   Andrea Setzer. 2001. Temporal Information in Newswire Articles: An Annotation Scheme and Corpus Study. Ph.D. thesis, University of Sheffield. 

7   TEI Consortium. 2003. The text encoding initiative, http://www.tei-c.org/. 
