Workshops 
 
 
Grammar Engineering and Evaluation 
null Roberto Bartolini, Alessandro Lenci, Simonetta Montemagni, Vito Pirrelli: 
Grammar and Lexicon in the Robust Parsing of Italian towards a 
Non-Naïve Interplay 
 
null Emily M. Bender, Dan Flickinger, Stephan Oepen: The Grammar Matrix: An 
Open-Source Starter-Kit for the Rapid Development of 
Cross-linguistically Consistent Broad-Coverage Precision Grammars 
null Miriam Butt, Helge Dyvik, Tracy Holloway King, Hiroshi Masuichi, Christian 
Rohrer: The Parallel Grammar Project 
null Richard Campbell, Carmen Lozano, Jessie Pinkham, Martine Smets: Machine
Translation as a Testbed for Multilingual Analysis 
null Caroline Hagège, Gabriel G. Bès: Encoding and Reusing Linguistic 
Information Expressed by Linguistic Properties 
null Ronald M. Kaplan, Tracy Holloway King, John T. Maxwell III: Adapting 
Existing Grammars: The XLE Experience 
null Alexandra Kinyon, Carlos A. Prolo: A Classification of Grammar 
Development Strategies 
null Stephan Oepen, Emily M. Bender, Uli Callmeier, Dan Flickinger, Melanie 
Siegel: Parallel Distributed Grammar Engineering for Practical 
Applications 
null Carlos A. Prolo: Coping with Problems in Grammars Automatically 
Extracted from Treebanks 
null Hisami Suzuki: A Development Environment for Large-scale Multi-lingual 
Parsing Systems 
