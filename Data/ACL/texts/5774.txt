Generation of single-sentence paraphrases from
predicate#2Fargument structure using lexico-grammatical resources
Raymond Kozlowski, Kathleen F. McCoy, and K. Vijay-Shanker
Department of Computer and Information Sciences
University of Delaware
Newark, DE 19716, USA
kozlowsk,mccoy,vijay@cis.udel.edu
Abstract
Paraphrases, which stem from the va-
riety of lexical and grammatical means
of expressing meaning available in a
language, pose challenges for a sen-
tence generation system. In this
paper, we discuss the generation of
paraphrases from predicate#2Fargument
structure using a simple, uniform gen-
eration methodology. Central to our
approach are lexico-grammatical re-
sources which pair elementary seman-
tic structures with their syntactic re-
alization and a simple but powerful
mechanism for combining resources.
1 Introduction
In natural language generation, producing some
realization of the input semantics is not the only
goal. The same meaning can often be expressed
in various ways using di#0Berent lexical and syn-
tactic means. These di#0Berent realizations, called
paraphrases, vary considerably in appropriate-
ness based on pragmatic factors and commu-
nicative goals. If a generator is to come up with
the most appropriate realization, it must be ca-
pable of generating all paraphrases that realize
the input semantics. Even if it makes choices on
pragmatic grounds during generation and pro-
duces a single realization, the ability to generate
them all must still exist.
Variety of lexical and grammatical forms
of expression pose challenges to a generator
#28#28Stede, 1999#29; #28Elhadad et al., 1997#29; #28Nicolov
et al., 1995#29#29. In this paper, we discuss the gen-
eration of single-sentence paraphrases realizing
the same semantics in a uniform fashion using a
simple sentence generation architecture.
In order to handle the various ways of realiz-
ing meaning in a simple manner, we believe that
the generation architecture should not be aware
of the variety and not have any special mech-
anisms to handle the di#0Berent types of realiza-
tions
1
. Instead, we want all lexical and gram-
matical variety to follow automatically from the
variety of the elementary building blocks of gen-
eration, lexico-grammatical resources.
We have developed a fully-operational proto-
type of our generation system capable of gen-
erating the examples presented in this paper,
which illustrate a wide range of paraphrases.
As we shall see, the paraphrases that are pro-
duced by the system depend entirely on the
actual lexicon used in the particular applica-
tion. Determining the range of alternate forms
that constitute paraphrases is not the focus of
this work. Instead, we describe a framework in
which lexico-grammatical resources, if properly
de#0Cned, can be used to generate paraphrases.
2 Typical generation methodology
Sentence generation takes as input some seman-
tic representation of the meaning to be conveyed
in a sentence. We make the assumption that
1
Ability to handle variety in a uniform manner is also
importantinmultilingual generation as some forms avail-
able in one language may not be available in another.
ENJOY
EXPERIENCER THEME
AMY INTERACTION
Figure 1: The semantics underlying #282a-2b#29
the input is a hierarchical predicate#2Fargument
structure such as that shown in Fig. 1. The
output of this process should be a set of gram-
matical sentences whose meaning matches the
original semantic input.
One standard approach to sentence genera-
tion from predicate#2Fargument structure #28like the
semantic-head-driven generation in #28Shieber et
al., 1990#29#29 involves a simple algorithm.
1. decompose the input into the top predicate
#28to be realized by a #28single#29 lexical item that
serves as the syntactic head#29 and identify
the arguments and modi#0Cers
2. recursively realize arguments, then modi-
#0Cers
3. combine the realizations in step 2 with the
head in step 1
In realizing the input in Fig. 1, the input can be
decomposed into the top predicate which can be
realized by a syntactic head #28a transitive verb#29
and its two arguments, the experiencer and the
theme. Suppose that the verb enjoy is chosen
to realize the top predicate. The two arguments
can then be independently realized as Amy and
the interaction. Finally, the realization of the
experiencer, Amy, can be placed in the subject
position and that of the theme, the interaction,
in the complement position, yielding #282a#29.
Our architecture is very similar but we argue
for a more central role of lexico-grammatical re-
sources driving the realization process.
3 Challenges in generating
paraphrases
Paraphrases come from various sources. In this
section, we give examples of some types of para-
phrases we handle and discuss the challenges
they pose to other generators. We also identify
types of paraphrases we do not consider.
3.1 Paraphrases we handle
Simple synonymy The simplest source of
paraphrases is simple synonymy. We take sim-
ple synonyms to be di#0Berent words that have
the same meaning and are of the same syntactic
category and set up the same syntactic context.
#281a#29 Booth killed Lincoln.
#281b#29 Booth assassinated Lincoln.
A generation system must be able to allow
the same semantic input to be realized in dif-
ferent ways. Notice that the words kill and as-
sassinate are not always interchangeable, e.g.,
assassinate is only appropriate when the victim
is a famous person. Such constraints need to be
captured with selectional restrictions lest inap-
propriate realizations be produced.
Di#0Berent placement of argument realiza-
tions Sometimes di#0Berent synonyms, like the
verbs enjoy and please, place argument realiza-
tions di#0Berently with respect to the head, as il-
lustrated in #282a-2b#29.
#282a#29 Amy enjoyed the interaction.
#282b#29 The interaction pleased Amy.
To handle this variety, a uniform generation
methodology should not assume a #0Cxed map-
ping between thematic and syntactic roles but
let each lexical item determine the placementof
argument realizations. Generation systems that
use such a #0Cxed mapping must override it for
the divergent cases #28e.g., #28Dorr, 1993#29#29.
Words with overlapping meaning There
are often cases of di#0Berentwords that realize dif-
ferent but overlapping semantic pieces. The eas-
iest way to see this is in what has been termed
incorporation, where a word not only realizes a
predicate but also one or more of its arguments.
Di#0Berentwords may incorporate di#0Berent argu-
ments or none at all, which may lead to para-
phrases, as illustrated in #283a-3c#29.
#283a#29 Charles #0Dew across the ocean.
#283b#29 Charles crossed the ocean by plane.
#283c#29 Charles went across the ocean by plane.
Notice that the verb #0Dy realizes not only go-
ing but also the mode of transportation being a
plane, the verb cross with its complement real-
ize going whose path is across the object realized
by the complement, and the verb go only real-
izes going. For all of these verbs, the remaining
arguments are realized by modi#0Cers.
Incorporation shows that a uniform genera-
tor should use the word choices to determine 1#29
what portion of the semantics they realize, 2#29
what portions are to be realized as arguments
of the realized semantics, and 3#29 what portions
remain to be realized and attached as modi#0Cers.
Generation systems that assume a one-to-one
mapping between semantic and syntactic units
#28e.g., #28Dorr, 1993#29#29 must use special processing
for cases of overlapping semantics.
Di#0Berent syntactic categories Predicates
can often be realized by words of di#0Berent syn-
tactic categories, e.g., the verb found and the
noun founding, as in #284a-4b#29.
#284a#29 I know that Olds founded GM.
#284b#29 I know about the founding of GM by Olds.
Words of di#0Berent syntactic categories usu-
ally have di#0Berent syntactic consequences. One
such consequence is the presence of additional
syntactic material. Notice that #284b#29 contains
the prepositions of and by while #284a#29 does not.
These prepositions might be considered a syn-
tactic consequence of the use of the noun found-
ing in this con#0Cguration. Another syntactic con-
sequence is a di#0Berent placement of argument re-
alizations. The realization of the founder is the
subject of the verb found in #284a#29 while in #284b#29
the use of founding leads to its placement in the
object position of the preposition by.
Grammatical alternations Words can be
put in a variety of grammatical alternations such
as the active and passivevoice, as in #285a-5b#29, the
topicalized form, the it-cleft form, etc.
#285a#29 Oswald killed Kennedy.
#285b#29 Kennedy was killed by Oswald.
The choice of di#0Berent grammatical alterna-
tions has di#0Berent syntactic consequences which
must be enforced in generation, such as the pres-
ence or absence of the copula and the di#0Berent
placement of argument realizations. In some
systems such as ones based on Tree-Adjoining
Grammars #28TAG#29, including ours, these con-
sequences are encapsulated within elementary
structures of the grammar. Thus, such systems
do not have to speci#0Ccally reason about these
consequences, as do some other systems.
More complex alternations The same con-
tent of excelling at an activity can be realized by
the verb excel, the adverb well, and the adjective
good, as illustrated in #286a-6c#29.
#286a#29 Barbara excels at teaching.
#286b#29 Barbara teaches well.
#286c#29 Barbara is a good teacher.
This variety of expression, often called head
switching, poses a considerable di#0Eculty for
most existing sentence generators. The di#0E-
culty stems from the fact that the realization
of a phrase #28sentence#29 typically starts with the
syntactic head #28verb#29 which sets up a syntactic
context into which other constituents are #0Ct. If
the top predicate is the excelling, wehavetobe
able to start generation not only with the verb
excel but also with the adverb well and the ad-
jective good, typically not seen as setting up an
appropriate syntactic context into which the re-
maining arguments can be #0Ct. Existing genera-
tion systems that handle this variety do so using
special assumptions or exceptional processing,
all in order to start the generation of a phrase
with the syntactic head #28e.g., #28Stede, 1999#29, #28El-
hadad et al., 1997#29, #28Nicolov et al., 1995#29, #28Dorr,
1993#29#29. Our system does not require that the se-
mantic head map to the syntactic head.
Di#0Berent grammatical forms realizing se-
mantic content Finally, we consider a case,
which to our knowledge is not handled by other
generation systems, where grammatical forms
realize content independently of the lexical item
on which they act, as in #287a-7b#29.
#287a#29 Who rules Jordan?
#287b#29 Identify the ruler of Jordan!
The wh-question form, as used in #287a#29, real-
izes a request for identi#0Ccation by the listener
#28in this case, the ruler of Jordan#29. Likewise, the
imperative structure #28used in #287b#29#29 realizes a re-
quest or a command to the listener #28in this case,
to identify the ruler of Jordan#29.
3.2 Paraphrases we do not consider
Since our focus is on sentence generation and not
sentence planning, we only consider the genera-
tion of single-sentence paraphrases. Hence, we
do not have the ability to generate #288a-8b#29 from
the same input.
#288a#29 CS1 has a programming lab.
#288b#29 CS1 has a lab. It involves programming.
Since we do not reason about the semantic
input, including deriving entailment relations,
we cannot generate #289a-9b#29 from the same input.
#289a#29 Oslo is the capital of Norway.
#289b#29 Oslo is located in Norway.
4 Our generation methodology
Generation in our system is driven by the
semantic input, realized by selecting lexico-
grammatical resources matching pieces of it,
starting with the top predicate. The realization
of a piece containing the top predicate provides
the syntactic context into which the realizations
of the remaining pieces can be #0Ct #28their place-
ment being determined by the resource#29.
The key to our abilitytohandle paraphrases
in a uniform manner is that our processing is
driven by our lexicon and thus we do not make
any a priori assumptions about 1#29 the amount
of the input realized by a lexical unit, 2#29 the re-
lationship between semantic and syntactic types
#28and thus the syntactic rank or category of the
realization of the top piece#29, 3#29 the nature of
the mapping between thematic roles and syn-
tactic positions, and 4#29 the grammatical alter-
nation #28e.g., there are di#0Berent resources for the
same verb in di#0Berent alternations: the active,
passive, topicalized, etc.#29. Because this informa-
tion is contained in each lexico-grammatical re-
source, generation can proceed no matter what
choices are speci#0Ced about these in each indi-
vidual resource. Our approach is fundamen-
tally di#0Berent from systems that reason directly
about syntax and build realizations by syntactic
rank #28#28Bateman, 1997#29, #28Elhadad et al., 1997#29;
#28Nicolov et al., 1995#29; #28Stone and Doran, 1997#29#29.
4.1 Our algorithm
Our generation algorithm is a simple, recursive,
semantic-head-driven generation process, con-
sistent with the approach described in section 2,
but one driven by the semantic input and the
lexico-grammatical resources.
1. given an unrealized input, #0Cnd a lexico-
grammatical resource that matches a por-
tion including the top predicate and satis-
#0Ces any selectional restrictions
2. recursively realize arguments, then modi-
#0Cers
3. combine the realizations in step 2 with the
resource in step 1, as determined by the re-
source in step 1
Notice the prominence of lexico-grammatical re-
sources in steps 1 and 3 of this algorithm. The
standard approach in section 2 need not be
driven by resources.
4.2 Lexico-grammatical resources
The key to the simplicity of our algorithm lies in
the lexico-grammatical resources, which encap-
sulate information necessary to carry through
generation. These consist of three parts:
#0F the semantic side: the portion of seman-
tics realized by the resource #28including the
predicate and any arguments; this part is
matched against the input semantics#29
#0F the syntactic side: either word#28s#29 in a syn-
tactic con#0Cguration or a grammatical form
without words, and syntactic consequences
#0F a mapping between semantic and syntactic
constituents indicating which constituent
on the semantic side is realized by which
constituent on the syntactic side
Consider the resources for the verbs enjoy and
please in Fig. 2. The semantic sides indicate
that these resources realize the predicate ENJOY
and the thematic roles EXPERIENCER and THEME.
The arguments #0Clling those roles #28whichmust be
realized separately, as indicated by dashed out-
lines#29 appear as variables X and Y which will be
matched against actual arguments. The syntac-
tic sides contain the verbs enjoy and please in
the active voice con#0Cguration. The mappings
include links between ENJOY and its realization
as well as links between the unrealized agent#28X#29
or theme #28Y#29 and the subject or the complement.
Our mapping between semantic and syntactic
constituents bears resemblance to the pairingsin
Synchronous TAG #28Shieber and Schabes, 1990#29.
Just like in Synchronous TAG, the mapping is
VPNP
VP
NPV
enjoy
a78
S
00
1
ENJOY
EXPERIENCER THEME
XY
1
VPNP
VP
NPV
please
a78
S
00
1
ENJOY
EXPERIENCER THEME
XY
1
Figure 2: Two di#0Berent resources for ENJOY
critical for combining realizations #28in step 3 of
our algorithm in section 4.1#29. There are, how-
ever, advantages that our approach has. For
one, we are not constrained by the isomorphism
requirement in a Synchronous TAG derivation.
Also, the DSG formalism that we use a#0Bords
greater #0Dexibility, signi#0Ccant in our approach, as
discussed later in this paper #28and in more detail
in #28Kozlowski, 2002b#29#29.
4.3 The grammatical formalism
Both step 3 of our algorithm #28putting re-
alizations together#29 and the needs of lexico-
grammatical resources #28the encapsulation of
syntactic consequences such as the position
of argument realizations#29 place signi#0Ccant de-
mands on the grammatical formalism to be used
in the implementation of the architecture. One
grammatical formalism that is well-suited for
our purposes is the D-Tree Substitution Gram-
mars #28DSG, #28Rambow et al., 2001#29#29, a variant
of Tree-Adjoining Grammars #28TAG#29. This for-
malism features an extended domain of locality
and #0Dexibility in encapsulation of syntactic con-
sequences, crucial in our architecture.
Consider the elementary DSG structures on
the right-hand-side of the resources for enjoy
and please in Fig. 2. Note that nodes marked
with #23 are substitution nodes corresponding to
syntactic positions into which the realizations of
S
a78
00
1
1
VPNP
VP
please
NPVa78
S
00
1
1
NP
the interactionAmy
VPNP
VP
V
enjoy
Figure 3: Combining argument realizations with
the resources for enjoy and please
arguments will be substituted. The positions of
both the subject and the complement are en-
capsulated in these elementary structures. This
allows the mapping between semantic and syn-
tactic constituents to be de#0Cned locally within
the resources. Dotted lines indicate domination
of length zero or more where syntactic material
#28e.g., modi#0Cers#29 may end up.
4.4 Using resources in our algorithm
Step 1 of our algorithm requires matching the se-
mantic side of a resource against the top of the
input and testing selectional restrictions. A se-
mantic side matches if it can be overlaid against
the input. Details of this process are given
in #28Kozlowski, 2002a#29. Selectional restrictions
#28type restrictions on arguments#29 are associated
with nodes on the semantic side of resources.
In their evaluation, the appropriate knowledge
base instance is accessed and its type is tested.
More details about using selectional restrictions
in generation and in our architecture are given
in #28Kozlowski et al., 2002#29.
Resources for enjoy and please which match
the top of the input in Fig. 1 are shown in
Fig. 2. In doing the matching, the arguments
AMY and INTERACTION are uni#0Ced with X and
Y. The dashed outlines around X and Y indicate
that the resource does not realize them. Our al-
gorithm calls for the independent recursive real-
ization of these arguments and then putting to-
gether those realizations with the syntactic side
of the resource, as indicated by the mapping.
0
a78
fly
VPNP
VP
S
0
V
GO
1
PLANE
AGENT MODE
X
ACROSS
VPNP
VP
NPV
cross
a78
S
00
1
GO
AGENT PATH
X
1
THEME
Y
Figure 4: Two di#0Berent resources for GO
PATH MODE
ACROSS
OCEAN
THEME
AGENT
GO
PLANECHARLES
Figure 5: The semantics underlying#283a-3c#29 with
portion realized by cross in bold
This is shown in Fig. 3. The argument realiza-
tions, Amy and the interaction, are placed in the
subject and complement positions of enjoy and
please, according to the mapping in the corre-
sponding resources.
4.5 Driving decomposition by resources
The semantic side of a resource determines
which arguments, if any, are realized by the re-
source, while the matching done in step 1 of our
algorithm determines the portions that must be
realized by modi#0Cers. This is always done the
same way regardless of the resources selected
and how much of the input they realize, such
as the two resources realizing the predicate GO
shown in Fig. 4, one for #0Dy which incorporates
MODE PLANE and another for cross which incor-
porates PATH ACROSS.
YX
AGENT THEME
NP
FOUND
Y
1
X
THEMEAGENT
1
00
S
a78
found
V
VP
NP VP
a78
N’
the
a78
0
NP
DN’
1
FOUND
2
PP
1
by
PNP
founding
a78
of
PNP
a78 PP
2
N
N’
Figure 6: Two di#0Berent resources for FOUND
Suppose the semantic input underlying #283a-
3c#29 is as given in Fig. 5. The portion shown
in bold is realized by the resource for cross in
Fig. 4. The agentofGO and the theme of ACROSS
are to be realized as arguments. The remaining
thematic role MODE with the argument PLANE #0Cll-
ing it, is to be realized by a modi#0Cer.
4.6 Encapsulation of syntactic
consequences
All syntactic information should be encapsu-
lated within resources and transparent to the
algorithm. This includes the identi#0Ccation of ar-
guments, including their placement with respect
to the realization. Another example of a syn-
tactic consequence is the presence of additional
syntactic material required by the lexical item in
the particular syntactic con#0Cguration. The verb
found inthe active con#0Cguration, as in #284a#29, does
not require any additional syntactic material.
On the other hand, the noun founding in the
con#0Cguration with prepositional phrases headed
by of and by, as in #284b#29, may be said to require
the use of the prepositions. The resources for
found and founding are shown in Fig. 6. Encap-
sulation of such consequences allows us to avoid
special mechanisms to keep track of and enforce
EXPERIENCER
EXCEL
THEME
[0]:
1
a78V
VP
a78
at
P
PP
[0]:
excel
VP
[0]:
[0]:
0
a78
well
Adv
0
Adv’
1
Adv’
1
VP
AdvP
P
AGENT
NP
1
PRO
NP
S
P
THEMEEXPERIENCER
EXCEL
00
S
VP
Figure 7: Two di#0Berent resources for EXCEL
them for individual resources.
4.7 Syntactic rank and category
No assumptions are made about the realization
of a piece of input semantics, including its syn-
tactic rank and category. For instance, the pred-
icate EXCEL can be realized by the verb excel,
the adverb well, and the adjective good, as illus-
trated in #286a-6c#29. The processing is the same:
a resource is selected and any argument realiza-
tions are attached to the resource.
Fig. 7 shows a resource for the predicate
EXCEL realized by the verb excel. What is in-
teresting about this case is that the DSG for-
malism we chose allows us to encapsulate the
PRO in the subject position of the complement
as a syntactic consequence of the verb excel in
this con#0Cguration. The other resource for EXCEL
shown in Fig. 7 is unusual in that the predicate
is realized byanadverb, well. Note the link be-
tween the uninstantiated theme on the semantic
side and the position for its corresponding syn-
tactic realization, the substitution node VP
1
2
.
Suppose the semantic input underlying #286a-
2
Also notice that the experiencer of EXCEL is consid-
ered realized by the well resource and coindexed with the
agent of the theme of EXCEL, to be realized by a separate
resource.
[1]
[1]: BARBARA
TEACH
[1]
AGENT
EXCEL
THEMEEXPERIENCER
Figure 8: The semantics underlying #286a-6c#29
6c#29 is as given in Fig. 8 and the well resource in
Fig. 7 is selected to realize the top of the seman-
tics. The matching in step 1 of our algorithm
determines that the subtree of the input rooted
at TEACH must be recursively realized. The re-
alization of this subtree yields Barbara teaches.
Because of the link between the theme of EXCEL
and the VP
1
node of well, the realization Bar-
bara teaches is substituted to the VP
1
node of
well. This is a more complex substitution than
in regular TAG #28where the substitution node is
identi#0Ced with the root of the argument realiza-
tion#29, and is equivalent to the adjunction of well
to Barbarateaches. In DSG, we are able to treat
structures such as the well structure as initial
and not auxiliary, as TAG would. Thus, argu-
ment realizations are combined with all struc-
tures in a uniform fashion.
4.8 Grammatical forms
As discussed before, grammatical forms them-
selves can realize a piece of semantics. For in-
stance, the imperative syntactic form realizes a
request or a command to the listener, as shown
in Fig. 9. Likewise, the wh-question form real-
izes a request to identify, also shown in Fig. 9.
In our system, whether the realization has any
lexical items is not relevant.
4.9 The role of DSG
We believe that the choice of the DSG formal-
ism plays a crucial role in maintaining our sim-
ple methodology. Like TAG, DSG allows cap-
turing syntactic consequences in one elementary
structure. DSG, however, allows even greater
#0Dexibilityinwhat is included in an elementary
structure. Note that in DSG wemayhave non-
immediate domination links between nodes of
[empty:+]
[subj−empty:+]
[0]:
[0]:
REQUEST
ACTION
ACTION
REQUEST
P
YOU
S
NP
(you)
IDENTIFY
THEME
S
1
NP
[inv:+]
S
NP
εwho
a78N
YOU SET−OF
THEME SUCH−THAT
P
AGENT
Figure 9: Two di#0Berent resources for REQUEST
di#0Berent syntactic categories #28e.g., between the S
and NP in Fig. 9 and also in the excel at structure
in Fig. 7#29. DSG also allows uniform treatment
of complementation and modi#0Ccation using the
operations of substitution #28regardless of the re-
alization of the predicate, e.g., the structures in
Fig. 7#29 and adjunction, respectively.
5 Conclusions
Although we only consider paraphrases with the
same semantics, there is still a wide variety of
expression which poses challenges to any genera-
tion system. In overcoming those challenges and
generating in a simple manner in our architec-
ture, our lexico-grammatical resources play an
important role in each phase of generation. En-
capsulation of syntactic consequences withinele-
mentary syntactic structures keeps our method-
ology modular. Whatever those consequences,
often very di#0Berent for di#0Berent paraphrases,
generation always proceeds in the same manner.
Both the algorithm and the constraints on
our lexico-grammatical resources place signif-
icant demands on the grammatical formalism
used for the architecture. We #0Cnd that the DSG
formalism meets those demands well.

References
John Bateman. 1997. Enabling technology for mul-
tilingual natural language generation: the KPML
developmentenvironment. Natural Language En-
gineering, 3#281#29:15#7B55.
Bonnie J. Dorr. 1993. Interlingual machine transla-
tion: a parametrized approach. Arti#0Ccial Intelli-
gence, 63#281#29:429#7B492.
Michael Elhadad, Kathleen McKeown, and Jacques
Robin. 1997. Floating constraints in lexical
choice. Computational Intelligence, 23:195#7B239.
Raymond Kozlowski, Kathleen F. McCoy, and
K. Vijay-Shanker. 2002. Selectional restrictions
in natural language sentence generation. In Pro-
ceedings of the 6th World Multiconference on Sys-
temics, Cybernetics, and Informatics #28SCI'02#29.
Raymond Kozlowski. 2002a. Driving multilingual
sentence generation with lexico-grammatical re-
sources. In Proceedings of the Second Interna-
tional Natural Language Generation Conference
#28INLG'02#29 - Student Session.
Raymond Kozlowski. 2002b. DSG#2FTAG - An appro-
priate grammatical formalism for #0Dexible sentence
generation. In Proceedings of the Student Research
Workshop at the 40th Annual Meeting of the Asso-
ciation for Computational Linguistics #28ACL'02#29.
Nicolas Nicolov, Chris Mellish, and Graeme Ritchie.
1995. Sentence Generation from Conceptual
Graphs. In Proceedings of the 3rd International
Conference on Conceptual Structures #28ICCS'95#29.
Owen Rambow, K. Vijay-Shanker, and David Weir.
2001. D-Tree Substitution Grammars. Computa-
tional Linguistics, 27#281#29:87#7B122.
Stuart M. Shieber and Yves Schabes. 1990. Syn-
chronous Tree-Adjoining Grammars. In Proceed-
ings of the 13th International Conference on Com-
putational Linguistics.
Stuart M. Shieber, Gertjan van Noord, Fernando
C. N. Pereira, and Robert C. Moore. 1990.
Semantic-Head-Driven Generation. Computa-
tional Linguistics, 16#281#29:30#7B42.
Manfred Stede. 1999. Lexical semantics and knowl-
edge representation in multilingual text genera-
tion. Kluwer Academic Publishers, Boston.
Matthew Stone and Christine Doran. 1997. Sen-
tence Planning as Description Using Tree Adjoin-
ing Grammar. In Proceedings of the 35th Annual
Meeting of the Association for Computational Lin-
guistics #28ACL'97#29.
