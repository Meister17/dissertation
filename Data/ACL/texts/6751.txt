Improving Word Alignment Models Using Structured Monolingual Corpora
by Wei Wang and Ming Zhou

References
P. F. Brown, S. A. Della Pietra, V. J. Della Pietra, R. L. Mercer. 1993. The mathematics of statistical machine translation: parameter estimation. Computational Linguistics.
P. Fung and Y. Lee. 1998. Translating unknown words using nonparallel, comparable texts. COLING ACL.
R. Hwa, P. Resnik, A. Weinberg, O. Kolak. 2002. Evaluating translational correspondence using annotation projection. ACL.
S. Ker and J. Zhang. 1997. A class-based approach to word alignment. Computational Linguistics.
P. Koehn and K. Knight. 2000. Estimating word translation probabilities from unrelated monolingual corpora using the EM algorithm. AAAI.
D. Lin. 1993. Principle based parsing without overgeneration. ACL.
D. Lin. 1998. Automatic retrieval and clustering of similar words. COLING ACL.
Y. Matsumoto. 1993. Structural matching of parallel texts. ACL.
J. Mei. 2002. Xiandai Hanyu Tong Yi CiDian. The Commerical Press LTD. of China.
G. Miller. 1990. WordNet: An online lexical database. International Journal of Lexicography.
J. Nie, M. Simard, P. Isabelle, R. Durand. 1999. Cross-language information retrieval based on parallel texts and automatic mining of parallel texts in the web. SIGIR.
F. Och and H. Ney. 2003. A systematic comparison of various statistical alignment models. Computational Linguistics.
F. Och. 2003. Minimum error rate training in statistical machine translation. ACL.
P. Resnik and N. Smith. 2003. The web as a parallel corpus. Computational Linguistics. 
D. Wu. 1997. Stochastic inversion transduction grammars and bilingual parsing of parallel corpora. Computational Linguistics.
M. Zhou. 2000. A block-based robust dependency parser for unrestricted chinese text. Second workshop on Chinese language processing.
M. Zhou, Y. Ding, C. Huang. 2001. Improving translation selection with a new translation model trained by independent monolingual corpora. Computational Linguistics and Chinese Language Processing.
