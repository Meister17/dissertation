CHART PARSING ACCORDING TO THE SLOT AND FILLER PRINCIPLE 
Peter HELLWIG 
University of Heidelberg 
P.O. Box 105 760 
D-6900 Heidelberg, FRG 
Abstract 
A parser is an algorithm that assigns a 
structural description to a string according 
to a grammar. It follows from this defini- 
tion that there are three general issues in 
parser design: the structure to be assigned, 
the type of grammar, the recognition algo~ 
rithm. Common parsers employ phrase struc- 
ture descriptions, rule-based grammars, and 
derivation or transition oriented recogni- 
tion. The following choices result in a new 
parser: The structure to be assigned to the 
input is a dependency tree with lexical, 
morpho-syntactic and functional-syntactic 
information associated with each node and 
coded by complex categories which are sub- 
ject to unification. The grammar is lexica- 
lized, i.e. the syntactical relationships 
are stated as part of the lexical descrip- 
tions of the elements of the language. The 
algorithm relies on the slot and filler 
principle in order to draw up complex struc- 
tures. It utilizes a well-formed substring 
table (chart) which allows for discontinuous 
segments. 
1. Dependency Structure 
The structuring principle of constituency 
trees is concatenation and the part-whole 
-relationship. The structuring principle of 
dependency trees is the relationship between 
lexemes and their complements. Note: It is 
not correct (or at least misleading) to de- 
fine dependency as a relationship between 
words, as it is often done. The possibility 
and necessity of complements depend on the 
lexical meaning of words, i.e. a word which 
denotes a relationship asks for entities 
which it relates, a word which denotes a mo- 
dification asks for an entity which it modi- 
fies etc. While it is awkward to associate 
functions (deep cases, roles, grammatical 
relationships) with phrase structures, it is 
not difficult to paraphrase the functions of 
complements on a lexical basis. For example, 
the argument of the predicate "sleep" deno- 
tes the sleeper; the meaning of "persuade" 
includes the persuader, the persuaded person 
and the contents of the persuasion. In a 
next step, one can abstract from the con- 
crete function of dependents and arrive at 
abstract functions like subject, object, ad- 
junct etc. 
Of course, the complements covering these 
roles can be single words as well as large 
phrases; for example "John", "my father", 
"the president of the United States" can all 
fill the role of the sleeper with respect to 
the predicate "sleep". However, phrases need 
not be represented by separate nodes in de- 
pendency trees (as they do in phrase mar- 
kers) because their internal structure is 
again a question of dependency between lexe- 
mes and their complements. In a dependency 
tree, phrases are represented directly by 
their internal structure, which results in 
an arc between the superodinated head and 
the head within the complementary phrase. 
Nevertheless, the real principle of depen- 
242 
dency is a relationship between words and 
structures, or, formally, between single 
nodes and trees. Taking this into account, 
dependency trees are much more appealing 
than has been recognized so far. 
In order to restrict linguistic structures 
according to syntactic and semantic require- 
ments, the use of complex categories is 
state of the art. Complex categories are 
sets of parameters (attributes) and values 
(features). Agreement between entities can 
be formulated in a general way in terms of 
parameters; the assignment of actual feature 
values is achieved by the mechanism of uni- 
fication. If dependency J.s the relationship 
along which the catagories are unified, 
functional=syntactic and mo~ho-syntactic 
features can be handeled completely in par- 
allel, as opposed to the two-phase mechanism 
which, for example, characterizes Lexical 
Functional Grammar. Each element in the de- 
pendency tree carries three labels: a role 
(which applies to the (sub)tree of which the 
element is the head), a lexeme, and a set of 
grammatical features. 
Constituency and dependency both have to be 
represented somehow or other in the syntac- 
tic description. As a consequence, recent 
developments have led to a convergence of 
formalisms of both origins with respect to 
their contents. (A good example is the simi- 
larity between Head-Driven Phrase Structure 
Grammar /Pollard, Sag 1987/ and Dependency 
Unification Grammar /Hellwig 1986/.) If 
phrase structure trees are used, the differ- 
ence between governor and dependent must be 
denoted by the categories that label the 
nodes, e.g. by a x-bar notation. If depen- 
dency trees are used, the concatenation 
relationship must be denoted by positional 
features which are part of the complex 
morpho-svntactic category. 
2. Chart parsing based on a lexicalized 
grammar 
The structure to be associated with a well- 
formed string can be defined in two ways: 
either by a set of abstract rules which 
describe the possible constructions of the 
~language or by a description of the combi-. 
nation capabilities of the basic elements. 
The latter fits with the dependency ap- 
proach. Given a lexical item and its morpho- 
syntactic properties, it is relatively easy 
to give a precise description of its possi- 
ble complements. The main advantage of this 
lexicalistic approach is the fact that aug- 
menting or changing the description of an 
item normally does not interfere with the 
rest while any change in a rule-based gram- 
mar might produce unforeseen side effects 
with regard to the whole. 
The prerequisite for a lexicalized dependen- 
cy grammar are trees that comprise slots. A 
slot is a description of the head of a tree 
that fits into another tree. Formally, a 
slot is a direct dependent of a head with a 
role associated to it, with a variable in 
the lexeme position, and with a categoriza- 
tion that covers all of the morpho-syntactic 
properties of the appertaining complement. 
If cross categorization does not allow all 
of the p~ssible properties of a complement 
within one category to be stated, a disjunc- 
tion of slots is used to express the alter- 
natives. The only mechanism needed for draw-- 
ing up complex structures is the unification 
of slots and potential fillers. 
The control of the parsing process is 
achieved by means of a well-formed substring 
table ((\]hart). It is widely accepted that 
chart parsing is superior to backtracking or 
to parallel processing of every path. A com- 
mon version of a chart can be vizualized as 
a network of vertices representing points in 
the input, linked by edges representing seg- 
ments. The edges are labelled with the cate- 
gories that the parser has assigned to the 
constituents concerned. Alternatively, each 
edge is associated with a complete structur- 
al descrLption, including the information 
which is carried by the covered edges. In 
this case, a chart is simply a collect\]on of 
trees (implemented as lists) projected on 
the various segments in the input. The inno- 
vation with regard to chart parsing th~vt is 
proposed in this paper is a label.ling of 
edges by trees that comprise slots. 
At the beginning, an edge for each word is 
entered into the chart. Each edge is label\] o~ 
ed with a tree. The head of this tree con- 
tains the lexeme that is associated with the 
word according to the \].exicon; it carries a 
morpho-syntactic category according to the 
morphological properties of the word in 
question: it normally contains a variab\].e as 
a role l~arker, since the syntagmatic func- 
tion of the corresponding segment is still 
unknown. A slot is subordinated to the head 
for each element that is to be dependent in 
the resulting structure, if any. Slots are 
added to a lexical item according to c~>mple- 
tion patterns refered to in the lexicon. (We 
can not qo into details here.) 
Subsequently, each tree in the chart looks 
for a slot in a "tree that is associated with 
annother edge. If the head of the searching 
tree fitn the description in the slot then a 
new edge is drawn and labelled with the com- 
pound tree that results from inserting the 
first tree into the second. The categories 
of the ~ew tree are the result of unifying 
the categories of the slot tree and the fil- 
ler tree. Special features state the posi- 
tional re~/irements, e.g. whether the seg- 
ment corresponding to the filler has to pre- 
ceed or to follow of the segment correspond- 
ing to the element dominating the slot. This 
process continues until no new tree is pro- 
duced. Parsing was successful if at \].east 
one edge covers the whole input. The depen- 
dency tr~e associated with this edge is the 
desired structural description. 
The fo\].lowing example illustrates the me- 
chanism. 
(I) Flyir,g planes can be dangerous. 
The lexicon lookup leads to the initializa- 
tion of %he chart in figure i. 
I%°,- 
noun re) 
un--- Ivu~b - f~n IveTb inf ladje) lassert$on\[ 
(ATr{._ I (~v_ I (eA_ I I--~I 
adje la)) I verb I ndje ra))l I verb I 
l~f ~~) I i I fin ~a))\[ 
Flying planes can be dangerous 
Fig. 1 The chart at the end of the lexicon phase 
(The notation is simplified and we have 
omitted a detailed categorization. The de- 
pendency structure is represented by the 
bracketing. Lexemes are underlined; roles 
are in capita\] letters; slots are marked by 
the underscore in the lexeme position. "re" 
means right adjacent, "la" means left adja- 
cent with respect to the head. ) 
At the end of the slot fi\].\].ing process the 
chart looks like figure 2. 
(ILLOC assertion (ILLOC assertion 
(PRED c gl~verb fin (PRED .can verb fin 
(MV !)f{ verb inf (MV be. verb inf 
( \[~A (lal)gero~l_s adj e ) ) (PA daDqerous ad\] e ) ) 
(SUBJ flying, noun (SUBJ planes noun 
(OBJ p\] anes noun) ) (ATR f~ffl~\[\[ adj e) ) ) ) 
................................ m 
c~n verb fin (ca! ~ verb fin ! 
(MV be verb inf (MV be_verb inf I 
(PA dan~erotjs adje)) (PA _d.a!)~erqu~ adje}) 1 
SUSJ flyilj~_ ~loUn ( SUBJ \]31anes noun \] 
OBJ \]/la~es \[loun)) (ATR f\]~in!\[ ddje)))) I 
(p_lanes noun ~(E~n verb -in 
(ATR fl~i\q adje))\[ (MV \]pc verb ~l~f | 
(OBJ ~lanes noun re)) I I 
f g~nq \[ | l(be verb inf I 
no\]?n ~ ~(PA dan¢ierous aci e ' ' 
< ..... I ....... I ......... ,,\[ i t 
(fl~\[lq l(/~lanes \[(c n ~(be l(danqerous ILL C I aTi~e)-" \[,~-6un'- veTb fin I,'Trrb lnf la<\]}e\] ~asse~t~on I 
I (A'r~ - (MY _ (PA. I ~aEo--~i I 
| ad\]e la) verb a.~ije re)) I verb I 
| inf re) I fin !a))i 
/ I (suBJ _. / I i 
Flying planes can be dangerous 
Fig. 2 The ch~Lrt at the end of tbe pnrsl:~g process 
3. A chart for discontinuous segments 
In figure 2, the vertices represent left and 
right margins of segments. Adjacency is the 
inherent positional relationship in this mo- 
del. As a consequence, this chart does not 
allow for discontinuous constituents. This 
is a serious deficiency considering the phe- 
nomena of unbound dependencies which occur 
in many languages. The representation of po- 
sition by categoriesj however, opens up the 
possibility to state various kinds of posi- 
tional relationships between heads and de- 
pendents. In order to form discontinuous 
segments, the control of positions in the 
chart must be altered. The solution which we 
adopt is to represent the extensions of seg-- 
ments by bit strings consisting of one bit 
21,3 
for each word that is part of the segment 
and a zero bit for any word that is not 
within the segement (cf. fig. 3, trailing 
zeros are omitted). Discontinuous segements 
are depicted by the alternation of ones and 
• zeros in the bit string. The intersection of 
the appertaining bit strings is formed be- 
fore a filler is compared with a slot. This 
intersection must be empty, because other- 
wise both segments contain overlapping por- 
tions and one can not be a dependent of the 
other. After a filler is inserted into a 
slot, the union of both bit strings is form- 
ed and associated with the new tree. This 
amounts to island extension of the parsed 
substrings. Other operations on the bit 
strings allow for calculating the specific 
positional relationships stated within the 
categories of slots. The parser runs through 
alternate cycles: It tries to build continu- 
ous segments first. Then it uses these seg- 
ments to form discontinuous segments. From 
the results it tries to build continuous 
segments again and so on. The input is ac- 
cepted if a bitstring can be obtained that 
contains as many bits as there are words in 
the input. 
The example in figure 3 roughly illustrates 
the device. (Integers among the categories 
represent the sequence of dependents belong- 
ing to the same head. Lines separate the 
cycles.) 
(2) What does Gudrun feed to her cat ? 
The parser is fully implemented within the 
system PLAIN (Programs for Language Analysis 
and Inference), developed at the University 
of Heidelberg. So far, it is applied to Ger- 
man (Heidelberg, Kiel), French (Paris), and 
English (Surrey/UK, Pisa, Hawaii). 
Position: Tree: 
(i) 1 (whatpron) 
(2) Ol (do verbfin 
(SUBJ - noun ra i) 
(MV _ verb inf 2)) 
(3) 001 (Gudrun noun) 
(4) 0001 (feed verb inf 
(DOBJ - 15 
(IDOBJ _ to ra 2)) 
(5) OOO01 (to 
(._ noun ra)) 
(6) 000001 (her poss) 
(7) GO00001 (cat noun 
(DET _ poss la)5 
(85 00000001 (ILLOC~uestion 
(PRED _ do verb fin la)) 
(95 Oll (d o verb fin 
(SUBJ Gudrun noun ra I) 
(MV _ verb inf 25) 
(I0) 0000011 (c~at 
(DET her poss la)5 
(ii) 0000111 (to 
(cat noun ra 
(DET her poss laS) ) 
(12) OOOllll (feed verb inf 
(DOBJ - l) 
(IDOBJ t.~ora 2 
(cat noun 
(DET he rposs I~55)) ................................................................. 
(13) I001111 (feed verb inf 
(DOBJ what i) 
(IDOBJ t__oora 2 
(cat noun 
(DET he_rrposs La))5) ................................................................. 
(I~ 5 lllllll (d~overb fin 
(SUBJ Gudrun noun ra I) 
(MV feed verb inf 
(DOBJ wha___t 15 
(IDOBJ t_~ora 2 
(cat noun 
(DET her poss Ia5)5) 5 
I15) IIiiiiii (ILLOC~n 
(PREDdoverb fin 
(SUBJ Gudrun noun ra I) 
(MV feed verb inf 
(DOBJ what i) 
(IDOBJ to ra 2 
( c a-~--noun 
(DET her poss la)))5) 
Fig. 3 States of a chart including discontinuous segments 
244 

References 

Peter Hellwig: "PLAIN - A Program System for 
Dependency Analysis and for Simulating Na- 
tural Language Inference". In: Leonard Bolc 
(ed.): Representation and Processing of Na- 
tural Language. Wien: Hanser; London, Bas- 
ingstoke: Macmillan 1980, pp. 271-376. 

Peter Hellwig: "Dependency Unification Gram- 
mar". In: llth International Conference on 
Computational Linguistics. Proceedings. 
Bonn, August, 25th to 29th, 1986, University 
of Bonn, pp. 195-198. 

Carl Pollard, Ivan A. Sag: Information-Based 
Syntax and Semantics. Vol. I. Fundamentals. 
CSLI Lecture Notes, No. 12., Stanford Uni- 
versity 1987. 
