Workshops 
 
 
Multilingual Summarization and Question Answering 
null Margit Becher, Brigitte Endres-Niggemeyer, Gerrit Fichtner: Scenario Forms 
for Web Information Seeking and Summarizing in Bone Marrow 
Transplantation 
 
null Hanmin Jung, Gary Geunbae Lee: Multilingual Question Answering with 
High Portability on Relational Databases 
null Harksoo Kim, Jungyun Seo: A Reliable Indexing Method for a Practical QA 
System 
null Yu-Sheng Lai, Kuao-Ann Fung, Chung-Hsien Wu: FAQ Mining via List 
Detection 
null Wei Li, Rohini K. Srihari, Xiaoge Li, M. Srikanth, Xiuhong Zhang, Cheng Niu: 
Extracting Exact Answers to Questions Based on Structural Links 
null Fernando LLopis, José Luis Vicedo, Antonio Ferrández: Passage Selection 
to Improve Question Answering 
null Hiroyuki Sakai, Shigeru Masuyama: Unsupervised Knowledge Acquisition 
about the Deletion Possibility of Adnominal Verb Phrases 
