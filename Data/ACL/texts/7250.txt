 
Preface 
 
Continuing with a series of successful SIGdial workshops in Hong Kong and Denmark, 
this workshop will span the ACL and ISCA SIGdial interest area of discourse and 
dialogue. This series provides a regular forum for the presentation of research in this area 
to both the larger SIGdial community as well as researchers outside this community. The 
workshop is organized by SIGdial (http://www.sigdial.org) which is sponsored jointly by 
ACL (http://www.aclweb.org) and ISCA (http://www.isca-speech.org). 
 
The workshop includes a few plenary presentations of general interest along with three 
thematic sessions. The thematic sessions will combine long presentations, short 
presentations, and demos (as appropriate) with extended discussions. The three themes 
for this workshop are: 
 
•Annotation Methods for Corpora  
•Architectures for Dialogue Systems: Agent-based vs. Pipelined  
•Learning and Adaptivity in Dialogue Systems  
 
We would like to thank our colleagues who acted as the programme committee members 
and reviewed the submissions, thus helping us in deciding on the final programme. 
  
Jan Alexandersson, DFKI, Germany 
Peter Boda, Nokia Research Center, Finland 
Charles Callaway, Istituto Trentino di Cultura - IRST, Italy 
Lauri Carlson, University of Helsinki, Finland 
Alison Cawsey, Heriot-Watt University, UK 
Joyce Chai, IBM T.J.Watson Research Center, USA 
Yasuharu Den, Chiba University, Japan 
Barbara Di Eugenio, University of Illinois at Chicago, USA 
Reva Freedman, Northern Illinois University, USA 
Nancy Green, University of North Carolina at Greensboro, USA 
Graeme Hirst, University of Toronto, Canada 
Arne Jonsson, University of Linköping, Sweden 
Yasuhiro Katagiri, ATR, Japan 
Jan van Kuppevelt, University of Stuttgart, Germany 
Staffan Larsson, University of Göteborg, Sweden 
Anton Nijholt, University of Twente, The Netherlands 
Ronnie Smith, East Carolina University, USA 
Marc Swerts, IPO, The Netherlands and Antwerp University, Belgium 
 
 
Kristiina Jokinen,  University of Art and Design Helsinki 
Susan McRoy, University of Wisconsin-Milwaukee 
Co-chairs, Third SIGdial Workshop on Discourse and Dialogue 
 
 
