Oki Electric Industry: Description of the Oki System as Used for MUC-7
by J. Fukumoto, F. Masui, M. Shimohata, M. Sasaki

References

[1] TIPSTER TEXT PROGRAM Phrase II, DARPA, (1996).
[2] Proceedings of 6th Message Understanding Conference (MUC-6), DARPA, (1995).
[3] Masui, F., Tsunashima, T., Sugio, T., Tazoe, T. and Shiino, T.: \Analysis of Lengthy Sentences Using an English Comparative Structure Model", System and Computers in Japan, pp.40{48, SCRIPTA TECHNICA Inc., (1996).
[4] PENSEE, http://www.oki.co.jp/OKI/RDG/English/kikaku/vol.1/sugio/main.html http://www.oki.co.jp/OKI/Home/English/Topic/PENSEE/

