References

Salim Abu-Rabia. 1999. The effect of Arabic vowels on the reading comprehension of second- and sixth-grade native Arab children. Journal of Psycholinguist Research.

Yaser Al-Onaizan and Kevin Knight. 2002. Machine transliteration of names in Arabic texts. Proc. of Workshop on computational approahces to semitic languages.

Cyril Allauzen and Mehryar Mohri and Brian Roark. 2003. Generalized algorithms for constructing statistical language models. ACL.

Tim Buckwalter. 2002. Arabic transliteration table. online.

Tim Buckwalter. 2002. Buckwalter Arabic morphological analyzer version 1.0. LDC.

Tim Buckwalter. 2004. Issues in Arabic orthography and morphology analysis. COLING workshop on computational approaches to arabic script-based languages.

Mona Diab and Kadri Hacioglu and Daniel Jurafsky. 2004. Automatic tagging of Arabic text: from raw text to base phrase chunks. HLT NAACL.

Ya'akov Gal. 2002. An HMM approach to vowel restoration in Arabic and Hebrew. Workshop on Computational Approaches to Semitic Languages.

Slava M. Katz. 1987. Estimation of probabilities from sparse data for the language model component of a speech recognizer. IEEE Transactions on Acoustics, Speech amd Signal Processing.

Katrin Kirchoff and Jeff Bilmes and Sourin Das and Nicolae Duta and Melissa Egan and Gang Ji and Feng He and John Henderson and Daben Liu and Mohamed Noamany and Pat Schone and Richard Schwarta and Dimitra Vergyri. 2002. Novel approaches to Arabic speech recognition: report from the 2002 Johns-Hopkins summer workshop.

Mehryar Mohri and Fernando Pereira and Michael Riley. 2000. The design principles of a weighted finite-state transducer library. Theoretical Computer Science.

Fernando C. N. Pereira and Michael Riley. 1997. Speech recognition by composition of weighted finite automata. In Roche and Schabes, eds., Finite-state devices for natural language processing.  MIT press.

Dimitra Vergyri and Katrin Kirchhoff. 2004. Automatic diacritization of Arabic for acoustic modeling in speech recognition. COLING workshop on Computational Approaches to Arabic script-based languages.

Shuly Wintner and Shlomo Yona. 2003. Resources for processing hebrew. Proc. of MT Summit IX workshop on machine translation for semitic languages.
