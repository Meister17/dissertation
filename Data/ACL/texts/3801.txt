Minimal Recursion Semantics as Dominance Constraints:
Translation, Evaluation, and Analysis
Ruth Fuchss,
1
Alexander Koller,
1
Joachim Niehren,
2
and Stefan Thater
1
1
Dept. of Computational Linguistics, Saarland University, Saarbrücken, Germany
∗
2
INRIA Futurs, Lille, France
{fuchss,koller,stth}@coli.uni-sb.de
Abstract
We show that a practical translation of MRS de-
scriptions into normal dominance constraints is fea-
sible. We start from a recent theoretical translation
and verify its assumptions on the outputs of the En-
glish Resource Grammar (ERG) on the Redwoods
corpus. The main assumption of the translation—
that all relevant underspecified descriptions are
nets—is validated for a large majority of cases; all
non-nets computed by the ERG seem to be system-
atically incomplete.
1 Introduction
Underspecification is the standard approach to deal-
ing with scope ambiguity (Alshawi and Crouch,
1992; Pinkal, 1996). The readings of underspecified
expressions are represented by compact and concise
descriptions, instead of being enumerated explic-
itly. Underspecified descriptions are easier to de-
rive in syntax-semantics interfaces (Egg et al., 2001;
Copestake et al., 2001), useful in applications such
as machine translation (Copestake et al., 1995), and
can be resolved by need.
Two important underspecification formalisms in
the recent literature are Minimal Recursion Seman-
tics (MRS) (Copestake et al., 2004) and dominance
constraints (Egg et al., 2001). MRS is the under-
specification language which is used in large-scale
HPSG grammars, such as the English Resource
Grammar (ERG) (Copestake and Flickinger, 2000).
The main advantage of dominance constraints is
that they can be solved very efficiently (Althaus et
al., 2003; Bodirsky et al., 2004).
Niehren and Thater (2003) defined, in a theo-
retical paper, a translation from MRS into normal
dominance constraints. This translation clarified the
precise relationship between these two related for-
malisms, and made the powerful meta-theory of
dominance constraints accessible to MRS. Their
goal was to also make the large grammars for MRS
∗
Supported by the CHORUS project of the SFB 378 of the
DFG.
and the efficient constraint solvers for dominance
constraints available to the other formalism.
However, Niehren and Thater made three techni-
cal assumptions:
1. that EP-conjunction can be resolved in a pre-
processing step;
2. that the qeq relation in MRS is simply domi-
nance;
3. and (most importantly) that all linguistically
correct and relevant MRS expressions belong
to a certain class of constraints called nets.
This means that it is not obvious whether their
result can be immediately applied to the output of
practical grammars like the ERG.
In this paper, we evaluate the truth of these as-
sumptions on the MRS expressions which the ERG
computes for the sentences in the Redwoods Tree-
bank (Oepen et al., 2002). The main result of our
evaluation is that 83% of the Redwoods sentences
are indeed nets, and 17% aren’t. A closer analysis
of the non-nets reveals that they seem to be sys-
tematically incomplete, i. e. they predict more read-
ings than the sentence actually has. This supports
the claim that all linguistically correct MRS expres-
sions are indeed nets. We also verify the other two
assumptions, one empirically and one by proof.
Our results are practically relevant because dom-
inance constraint solvers are much faster and have
more predictable runtimes when solving nets than
the LKB solver for MRS (Copestake, 2002), as we
also show here. In addition, nets might be useful as
a debugging tool to identify potentially problematic
semantic outputs when designing a grammar.
Plan of the Paper. We first recall the definitions
of MRS (§2) and dominance constraints (§3). We
present the translation from MRS-nets to domi-
nance constraints (§4) and prove that it can be ex-
tended to MRS-nets with EP-conjunction (§5). Fi-
nally we evaluate the net hypothesis and the qeq
assumption on the Redwoods corpus, and compare
runtimes (§6).
2 Minimal Recursion Semantics
This section presents a definition of Minimal Re-
cursion Semantics (MRS) (Copestake et al., 2004)
including EP-conjunctions with a merging seman-
tics. Full MRS with qeq-semantics, top handles, and
event variables will be discussed in the last para-
graph.
MRS Syntax. MRS constraints are conjunctive
formulas over the following vocabulary:
1. An infinite set of variables ranged over by h.
Variables are also called handles.
2. An infinite set of constants x,y,z denoting in-
divual variables of the object language.
3. A set of function symbols ranged over by P,
and a set of quantifier symbols ranged over by
Q. Pairs Q
x
are further function symbols.
4. The binary predicate symbol ‘=
q
’.
MRS constraints have three kinds of literals, two
kinds of elementary predications (EPs) in the first
two lines and handle constraints in the third line:
1. h : P(x
1
,...,x
n
,h
1
,...,h
m
), where n,m ≥ 0
2. h : Q
x
(h
1
,h
2
)
3. h
1
=
q
h
2
In EPs, label positions are on the left of ‘:’ and argu-
ment positions on the right. Let M be a set of literals.
The label set lab(M) contains all handles of M that
occur in label but not in argument position, and the
argument handle set arg(M) contains all handles of
M that occur in argument but not in label position.
Definition 1 (MRS constraints). An MRS con-
straint (MRS for short) is a finite set M of MRS-
literals such that:
M1 every handle occurs at most once in argument
position in M,
M2 handle constraints h =
q
h
prime
always relate argu-
ment handles h to labels h
prime
, and
M3 for every constant (individual variable) x in ar-
gument position in M there is a unique literal
of the form h : Q
x
(h
1
,h
2
) in M.
We say that an MRS M is compact if every han-
dle h in M is either a label or an argument handle.
Compactness simplifies the following proofs, but it
is no serious restriction in practice.
We usually represent MRSs as directed graphs:
the nodes of the graph are the handles of the MRS,
EPs are represented as solid lines, and handle con-
straints are represented as dotted lines. For instance,
the following MRS is represented by the graph on
the left of Fig. 1.
{h
5
: some
y
(h
6
,h
8
),h
7
: book(y),h
1
: every
x
(h
2
,h
4
),
h
3
: student(x),h
9
: read(x,y),h
2
=
q
h
3
,h
6
=
q
h
7
}
every
x
some
y
student
x
book
y
read
x,y
every
x
some
y
student
x
book
y
read
x,y
every
x
some
y
student
x
book
y
read
x,y
Figure 1: An MRS and its two configurations.
Note that the relation between bound variables
and their binders is made explicit by binding edges
drawn as dotted lines (cf. C2 below); transitively re-
dundand binding edges (e. g., from some
y
to book
y
)
however are omited.
MRS Semantics. Readings of underspecified rep-
resentations correspond to configurations of MRS
constraints. Intuitively, a configuration is an MRS
where all handle constraints have been resolved by
plugging the “tree fragments” into each other.
Let M be an MRS and h,h
prime
be handles in M.We
say that h immediately outscopes h
prime
in M if there
is an EP in M with label h and argument handle h
prime
,
and we say that h outscopes h
prime
in M if the pair (h,h
prime
)
belongs to the reflexive transitive closure of the im-
mediate outscope relation of M.
Definition 2 (MRS configurations). An MRS M is
a configuration if it satisfies conditions C1 and C2:
C1 The graph of M is a tree of solid edges: (i) all
handles are labels i. e., arg(M)=/0 and M con-
tains no handle constraints, (ii) handles don’t
properly outscope themselve, and (iii) all han-
dles are pairwise connected by EPs in M.
C2 If h : Q
x
(h
1
,h
2
) and h
prime
: P(...,x,...) belong to
M, then h outscopes h
prime
in Mi.e., binding edges
in the graph of M are transitively redundant.
We say that a configuration M is configuration of
an MRS M
prime
if there exists a partial substitution σ :
lab(M
prime
)squigglerightarg(M
prime
) that states how to identify labels
with argument handles of M
prime
so that:
C3 M ={σ(E)| E is an EP in M
prime
}, and
C4 for all h =
q
h
prime
in M
prime
, h outscopes σ(h
prime
) in M.
The value σ(E) is obtained by substituting all la-
bels in dom(σ) in E while leaving all other handels
unchanged.
The MRS on the left of Fig. 1, for instance, has
two configurations given to the right.
EP-conjunctions. Definitions 1 and 2 generalize
the idealized definition of MRS of Niehren and
Thater (2003) by EP-conjunctions with a merging
semantics. An MRS M contains an EP-conjunction
if it contains different EPs with the same label h.The
intuition is that EP-conjunctions are interpreted by
object language conjunctions.
P
1
, P
2
P
3
{h
1
: P
1
(h
2
),h
1
: P
2
(h
3
),h
4
: P
3
h
2
=
q
h
4
,h
3
=
q
h
4
}
Figure 2: An unsolvable MRS with EP-conjunction
P
1
P
3
P
2
P
1
P
2
, P
3
configures
Figure 3: A solvable MRS without merging-free
configaration
Fig. 2 shows an MRS with an EP-conjunction and
its graph. The function symbols of both EPs are con-
joined and their arguments are merged into a set.
The MRS does not have configurations since the ar-
gument handles of the merged EPs cannot jointly
outscope the node P
4
.
We call a configuration merging if it contains EP-
conjunctions, and merging-free otherwise. Merging
configurations are needed to solve EP-conjuctions
such as {h : P
1
, h : P
2
}. Unfortunately, they can also
solve MRSs without EP-conjunctions, such as the
MRS in Fig. 3. The unique configuration of this
MRS is a merging configuration: the labels of P
1
and P
2
must be identified with the only available ar-
gument handle. The admission of merging configu-
rations may thus have important consequences for
the solution space of arbitrary MRSs.
Standard MRS. Standard MRS requires three
further extensions: (i) qeq-semantics, (ii) top-
handles, and (iii) event variables. These extensions
are less relevant for our comparision.
The qeq-semantics restricts the interpretation of
handle constraints beyond dominance. Let M be an
MRS with handles h,h
prime
. We say that h is qeq h
prime
in M
if either h = h
prime
, or there is an EP h : Q
x
(h
0
,h
1
) in M
and h
1
is qeq h
prime
in M. Every qeq-configuration is a
configuration as defined above, but not necessarily
vice versa. The qeq-restriction is relevant in theory
but will turn out unproblematic in practice (see §6).
Standard MRS requires the existence of top
handles in all MRS constraints. This condition
doesn’t matter for MRSs with connected graphs (see
(Bodirsky et al., 2004) for the proof idea). MRSs
with unconnected graphs clearly do not play any
role in practical underspecified semantics.
Finally, MRSs permit events variables e,e
prime
as a
second form of constants. They are treated equally
to individual variables except that they cannot be
bound by quantifiers.
3 Dominance Constraints
Dominance constraints are a general framework for
describing trees. For scope underspecification, they
are used to describe the syntax trees of object lan-
guage formulas. Dominance constraints are the core
language underlying CLLS (Egg et al., 2001) which
adds parallelism and binding constraints.
Syntax and semantics. We assume a possibly in-
finite signature Σ = {f,g,...} of function symbols
with fixed arities (written ar(f)) and an infinite set
of variables ranged over by X,Y,Z.
A dominance constraint ϕ is a conjunction of
dominance, inequality, and labeling literals of the
following form, where ar(f)=n:
ϕ ::= X triangleleft
∗
Y | X negationslash= Y | X : f(X
1
,...,X
n
)|ϕ∧ϕ
prime
Dominance constraints are interpreted over fi-
nite constructor trees i. e., ground terms constructed
from the function symbols in Σ. We identify ground
terms with trees that are rooted, ranked, edge-
ordered and labeled. A solution for a dominance
constraint ϕ consists of a tree τ and an assign-
ment α that maps the variables in ϕ to nodes of τ
such that all constraints are satisfied: labeling lit-
erals X : f(X
1
,...,X
n
) are satisfied iff α(X) is la-
beled with f and its daughters are α(X
1
),...,α(X
n
)
in this order; dominance literals X triangleleft
∗
Y are satisfied
iff α(X) dominates α(Y) in τ; and inequality literals
X negationslash= Y are satisfied iff α(X) and α(Y) are distinct
nodes.
Solved forms. Satisfiable dominance constraints
have infinitely many solutions. Constraint solvers
for dominance constraints therefore do not enumer-
ate solutions but solved forms i. e., “tree shaped”
constraints. To this end, we consider (weakly) nor-
mal dominance constraints (Bodirsky et al., 2004).
We call a variable a hole of ϕ if it occurs in argu-
ment position in ϕ and a root of ϕ otherwise.
Definition 3. A dominance constraint ϕ is normal
if it satisfies the following conditions.
N1 (a) each variable of ϕ occurs at most once in
the labeling literals of ϕ.
(b) each variable of ϕ occurs at least once in
the labeling literals of ϕ.
N2 for distinct roots X and Y of ϕ, X negationslash=Y is in ϕ.
N3 (a) if X triangleleft
∗
Y occurs in ϕ, Y is a root in ϕ.
(b) if X triangleleft
∗
Y occurs in ϕ, X is a hole in ϕ.
We call ϕ weakly normal if it satisfies the above
properties except for N1 (b) and N3 (b).
Note that Definition 3 imposes compactness: the
height of tree fragments is always one. This is not
every
x
some
y
student
x
book
y
read
x,y
every
x
some
y
student
x
book
y
read
x,y
every
x
some
y
student
x
book
y
read
x,y
Figure 4: A normal dominance constraint (left) and
its two solved forms (right).
a serious restriction, as weakly normal dominance
constraints can be compactified, provided that dom-
inance links relate either roots or holes with roots.
Weakly normal dominance constraints ϕ can be
represented by dominance graphs. The dominance
graph of ϕ is a directed graph G =(V,E
T
unionmultiE
D
) de-
fined as follows. The nodes of G are the variables of
ϕ. Labeling literals X : f(X
1
,...,X
k
) are represented
by tree edges (X,X
i
)∈ E
T
, for 1 ≤ i ≤ k, and domi-
nance literals X triangleleft
∗
X
prime
are represented by dominance
edges (X,X
prime
)∈ E
D
. Inequality literals are not repre-
sented in the graph. In pictures, labeling literals are
drawn with solid lines and dominance edges with
dotted lines.
We say that a constraint ϕ is in solved form if its
graph is in solved form. A graph G is in solved form
iff it is a forest. The solved forms of G are solved
forms G
prime
which are more specific than Gi.e., they
differ only in their dominance edges and the reacha-
bility relation of G extends the reachability of G
prime
.A
minimal solved form is a solved form which is min-
imal with respect to specificity. Simple solved forms
are solved forms where every hole has exactly one
outgoing dominance edge. Fig. 4 shows as a con-
crete example the translation of the MRS descrip-
tion in Fig. 1 together with its two minimal solved
forms. Both solved forms are simple.
4 Translating Merging-Free MRS-Nets
This section defines MRS-nets without EP-
conjunctions, and sketches their translation to
normal dominance constraints. We define nets
equally for MRSs and dominance constraints. The
key semantic property of nets is that different
notions of solutions coincide. In this section, we
show that merging-free configurations coincides
to minimal solved forms. §5 generalizes the trans-
lation by adding EP-conjunctions and permitting
merging semantics.
Pre-translation. An MRS constraint M can be
represented as a corresponding dominance con-
straint ϕ
M
as follows: The variables of ϕ
M
are the
handles of M, and the literals of ϕ
M
correspond
... ... ...
... ...
(a) strong (b) weak (c) island
Figure 5: Fragment Schemata of Nets
those of M in the following sence:
h : P(x
1
,...,x
n
,h
1
,...,h
k
)mapsto→ h : P
x
1
,...,x
n
(h
1
,...,h
k
)
h : Q
x
(h
1
,h
2
)mapsto→ h : Q
x
(h
1
,h
2
)
h =
q
h
prime
mapsto→ h triangleleft
∗
h
prime
Additionally, dominance literals h triangleleft
∗
h
prime
are added to
ϕ
M
for all h,h
prime
s. t. h : Q
x
(h
1
,h
2
) and h
prime
: P(...,x,...)
belong to M (cf. C2), and literals h negationslash= h
prime
are added
to ϕ
M
for all h,h
prime
in distinct label position in M.
Lemma 1. If a compact MRS M does not contain
EP-conjunctions then ϕ
M
is weakly normal, and the
graph of M is the transitive reduction of the graph
of ϕ
M
.
Nets. A hypernormal path (Althaus et al., 2003)
in a constraint graph is a path in the undirected
graph that contains for every leaf X at most one in-
cident dominance edge.
Let ϕ be a weakly normal dominance constraint
and let G be the constraint graph of ϕ. We say that
ϕ is a dominance net if the transitive reduction G
prime
of G is a net. G
prime
is a net if every tree fragment F
of G
prime
satisfies one of the following three conditions,
illustrated in Fig. 5:
Strong. Every hole of F has exactly one outgoing
dominance edge, and there is no weak root-to-root
dominance edge.
Weak. Every hole except for the last one has ex-
actly one outgoing dominance edge; the last hole
has no outgoing dominance edge, and there is ex-
actly one weak root-to-root dominance edge.
Island. The fragment has one hole X, and all vari-
ables which are connected to X by dominance edges
are connected by a hypernormal path in the graph
where F has been removed.
We say that an MRS M is an MRS-net if the pre-
translation of its literals results in a dominance net
ϕ
M
. We say that an MRS-net M is connected if ϕ
M
is connected; ϕ
M
is connected if the graph of ϕ
M
is
connected.
Note that this notion of MRS-nets implies that
MRS-nets cannot contain EP-conjunctions as other-
wise the resulting dominance constraint would not
be weakly normal. §5 shows that EP-conjunctions
can be resolved i. e., MRSs with EP-conjunctions
can be mapped to corresponding MRSs without EP-
conjunctions.
If M is an MRS-net (without EP-conjunctions),
then M can be translated into a corresponding dom-
inance constraint ϕ by first pre-translating M into
a ϕ
M
and then normalizing ϕ
M
by replacing weak
root-to-root dominance edges in weak fragments by
dominance edges which start from the open last
hole.
Theorem 1 (Niehren and Thater, 2003). Let M be
an MRS and ϕ
M
be the translation of M.IfM is a
connected MRS-net, then the merging-free configu-
rations of M bijectively correspond to the minimal
solved forms of the ϕ
M
.
The following section generalizes this result to
MRS-nets with a merging semantics.
5 Merging and EP-Conjunctions
We now show that if an MRS is a net, then all its
configurations are merging-free, which in particular
means that the translation can be applied to the more
general version of MRS with a merging semantics.
Lemma 2 (Niehren and Thater, 2003). All mini-
mal solved forms of a connected dominance net are
simple.
Lemma 3. If all solved forms of a normal domi-
nance constraint are simple, then all of its solved
forms are minimal.
Theorem 2. The configurations of an MRS-net M
are merging-free.
Proof. Let M
prime
be a configuration of M and let σ be
the underlying substitution. We construct a solved
form ϕ
M
prime as follows: the labeling literals of ϕ
M
prime are
the pre-translations of the EPs in M, and ϕ
M
prime has a
dominance literal h
prime
triangleleft
∗
h iff (h,h
prime
)∈σ, and inequal-
ity literals X negationslash= Y for all distinct roots in ϕ
M
prime.
By condition C1 in Def. 2, the graph of M
prime
is a
tree, hence the graph of ϕ
M
prime must also be a tree i. e.,
ϕ
M
prime is a solved form. ϕ
M
prime must also be more spe-
cific than the graph of ϕ
M
because the graph of M
prime
satisfies all dominance requirements of the handle
constraints in M, hence ϕ
M
prime is a solved form of ϕ
M
.
M clearly solved ϕ
M
prime. By Lemmata 2 and 3, ϕ
M
prime
must be simple and minimal because ϕ
M
is a net.
But then M
prime
cannot contain EP-conjunctions i. e., M
prime
is merging-free.
The merging semantics of MRS is needed to
solve EP-conjunctions. As we have seen, the merg-
ing semantics is not relevant for MRS constraints
which are nets. This also verifies Niehren and
Thater’s (2003) assumption that EP-conjunctions
are “syntactic sugar” which can be resolved in a pre-
processing step: EP-conjunctions can be resolved
by exhaustively applying the following rule which
adds new literals to make the implicit conjunction
explicit:
h : E
1
(h
1
,...,h
n
),h : E
2
(h
prime
1
,...,h
prime
m
)⇒
h :‘E
1
&E
2
’(h
1
,...,h
n
,h
prime
1
,...,h
prime
m
),
where E(h
1
,...,h
n
) stands for an EP with argument
handles h
1
,...,h
n
, and where ‘E
1
&E
2
’ is a complex
function symbol. If this rule is applied exhaustively
to an MRS M, we obtain an MRS M
prime
without EP-
conjunctions. It should be intuitively clear that the
configurations of M and M
prime
correspond; Therefore,
the configurations of M also correspond to the min-
imal solved forms of the translation of M
prime
.
6 Evaluation
The two remaining assumptions underlying the
translation are the “net-hypothesis” that all lin-
guistically relevant MRS expressions are nets, and
the “qeq-hypothesis” that handle constraints can be
given a dominance semantics practice. In this sec-
tion, we empirically show that both assumptions are
met in practice.
As an interesting side effect, we also compare the
run-times of the constraint-solvers we used, and we
find that the dominance constraint solver typically
outperforms the MRS solver, often by significant
margins.
Grammar and Resources. We use the English
Resource Grammar (ERG), a large-scale HPSG
grammar, in connection with the LKB system, a
grammar development environment for typed fea-
ture grammars (Copestake and Flickinger, 2000).
We use the system to parse sentences and output
MRS constraints which we then translate into domi-
nance constraints. As a test corpus, we use the Red-
woods Treebank (Oepen et al., 2002) which con-
tains 6612 sentences. We exclude the sentences that
cannot be parsed due to memory capacities or words
and grammatical structures that are not included in
the ERG, or which produce ill-formed MRS expres-
sions (typically violating M1) and thus base our
evaluation on a corpus containing 6242 sentences.
In case of syntactic ambiguity, we only use the first
reading output by the LKB system.
To enumerate the solutions of MRS constraints
and their translations, we use the MRS solver built
into the LKB system and a solver for weakly nor-
mal dominance constraints (Bodirsky et al., 2004),
...
(a) open hole (b) ill-formed island
Figure 6: Two classes of non-nets
which is implemented in C
++
and uses LEDA, a
class library for efficient data types and algorithms
(Mehlhorn and Näher, 1999).
6.1 Relevant Constraints are Nets
We check for 6242 constraints whether they consti-
tute nets. It turns out that 5200 (83.31%) constitute
nets while 1042 (16.69%) violate one or more net-
conditions.
Non-nets. The evaluation shows that the hypoth-
esis that all relevant constraints are nets seems to
be falsified: there are constraints that are not nets.
However, a closer analysis suggests that these con-
straints are incomplete and predict more readings
than the sentence actually has. This can also be il-
lustrated with the average number of solutions: For
the Redwoods corpus in combination with the ERG,
nets have 1836 solutions on average, while non-nets
have 14039 solutions, which is a factor of 7.7. The
large number of solutions for non-nets is due to the
“structural weakness” of non-nets; often, non-nets
have only merging configurations.
Non-nets can be classified into two categories
(see Fig. 6): The first class are violated “strong”
fragments which have holes without outgoing dom-
inance edge and without a corresponding root-to-
root dominance edge. The second class are violated
“island” fragments where several outgoing domi-
nance edges from one hole lead to nodes which
are not hypernormally connected. There are two
more possibilities for violated “weak” fragments—
having more than one weak dominance edge or hav-
ing a weak dominance edge without empty hole—,
but they occur infrequently (4.4%). If those weak
fragments were normalized, they would constitute
violated island fragments, so we count them as such.
124 (11.9%) of the non-nets contain empty holes,
762 (73.13%) contain violated island fragments,
and 156 (14.97%) contain both. Those constraints
that contain only empty holes and no violated is-
land fragments cannot be configured, as in configu-
rations, all holes must be filled.
Fragments with open holes occur frequently, but
not in all contexts, for constraints representing for
example time specifications (e. g., “from nine to
twelve” or “a three o’clock flight”) or intensional
expressions (e. g., “Is it?” or “I suppose”). Ill-
available
e
, a
x
a
y
cafeteria
x
sauna
y
and
e,x,y
prop
a
x
a
y
cafeteria
x
sauna
y
, 
and
e,x,y
available
e
prop
a
x
a
y
cafeteria
x
sauna
y
and
e,x,y
available
e
prop
ϕ
1
ϕ
2
Figure 7: An MRS for “A sauna and a cafeteria are
available” (top) and two of sixteen merging config-
urations (below).
a
x
a
y
cafeteria
x
sauna
y
and
e,x,y
available
e
prop
Figure 8: The “repaired” MRS from Fig. 7
formed island fragments are often triggered by some
kind of coordination, like “a restaurant and/or a
sauna” or “a hundred and thirty Marks”, also im-
plicit ones like “one hour thirty minutes” or “one
thirty”. Constraints with both kinds of violated frag-
ments emerge when there is some input that yields
an open hole and another part of the input yields a
violated island fragment (for example in construc-
tions like “from nine to eleven thirty” or “the ten
o’clock flight Friday or Thursday”, but not neces-
sarily as obviously as in those examples).
The constraint on the left in Fig. 7 gives a con-
crete example for violated island fragments. The
topmost fragment has outgoing dominance edges
to otherwise unconnected subconstraints ϕ
1
and ϕ
2
.
Under the merging-free semantics of the MRS di-
alect used in (Niehren and Thater, 2003) where ev-
ery hole has to be filled exactly once, this constraint
cannot be configured: there is no hole into which
“available” could be plugged. However, standard
MRS has merging configuration where holes can be
filled more than once. For the constraint in Fig. 7
this means that “available” can be merged in almost
everywhere, only restricted by the “qeq-semantics”
which forbids for instance “available” to be merged
with “sauna.” In fact, the MRS constraint solver de-
rives sixteen configurations for the constraint, two
of which are given in Fig. 7, although the sentence
has only two scope readings.
We conjecture that non-nets are semantically “in-
complete” in the sense that certain constraints are
missing. For instance, an alternative analysis for the
above constraint is given in Fig. 8. The constraint
adds an additional argument handle to “and” and
places a dominance edge from this handle to “avail-
able.” In fact, the constraint is a net; it has exactly
two readings.
6.2 Qeq is dominance
For all nets, the dominance constraint solver cal-
culates the same number of solutions as the MRS
solver does, with 3 exceptions that hint at problems
in the syntax-semantics interface. As every config-
uration that satisfies proper qeq-constraints is also
a configuration if handle constraints are interpreted
under the weaker notion of dominance, the solutions
computed by the dominance constraint solver and
the MRS solver must be identical for every con-
straint. This means that the additional expressivity
of proper qeq-constraints is not used in practice,
which in turn means that in practice, the translation
is sound and correct even for the standard MRS no-
tion of solution, given the constraint is a net.
6.3 Comparison of Runtimes
The availability of a large body of underspecified
descriptions both in MRS and in dominance con-
straint format makes it possible to compare the
solvers for the two underspecification formalisms.
We measured the runtimes on all nets using a Pen-
tium III CPU at 1.3 GHz. The tests were run in a
multi-user environment, but as the MRS and domi-
nance measurements were conducted pairwise, con-
ditions were equal for every MRS constraint and
corresponding dominance constraint.
The measurements for all MRS-nets with less
than thirty dominance edges are plotted in Fig. 9.
Inputs are grouped according to the constraint size.
The filled circles indicate average runtimes within
each size group for enumerating all solutions us-
ing the dominance solver, and the empty circles in-
dicate the same for the LKB solver. The brackets
around each point indicate maximum and minimum
runtimes in that group. Note that the vertical axis is
logarithmic.
We excluded cases in which one or both of the
solvers did not return any results: There were 173
sentences (3.33% of all nets) on which the LKB
solver ran out of memory, and 1 sentence (0.02%)
that took the dominance solver more than two min-
utes to solve.
The graph shows that the dominance constraint
solver is generally much faster than the LKB solver:
The average runtime is less by a factor of 50 for
constraints of size 10, and this grows to a factor
of 500 for constraints of size 25. Our experiments
show that the dominance solver outperforms the
LKB solver on 98% the cases. In addition, its run-
times are much more predictable, as the brackets in
the graph are also shorter by two or three orders
of magnitude, and the standard deviation is much
smaller (not shown).
7 Conclusion
We developed Niehren and Thater’s (2003) theoret-
ical translation into a practical system for translat-
ing MRS into dominance constraints, applied it sys-
tematically to MRSs produced by English Resource
Grammar for the Redwoods treebank, and evaluated
the results. We showed that:
1. most “real life” MRS expressions are MRS-
nets, which means that the translation is correct
in these cases;
2. for nets, merging is not necessary (or even pos-
sible);
3. the practical translation works perfectly for all
MRS-nets from the corpus; in particular, the
=
q
relation can be taken as synonymous with
dominance in practice.
Because the translation works so well in practice,
we were able to compare the runtimes of MRS and
dominance constraint solvers on the same inputs.
This evaluation shows that the dominance constraint
solver outperforms the MRS solver and displays
more predictable runtimes. A researcher working
with MRS can now solve MRS nets using the ef-
ficient dominance constraint solvers.
A small but significant number of the MRS con-
straints derived by the ERG are not nets. We have
argued that these constraints seem to be systemati-
cally incomplete, and their correct completions are
indeed nets. A more detailed evaluation is an impor-
tant task for future research, but if our “net hypoth-
esis” is true, a system that tests whether all outputs
of a grammar are nets (or a formal “safety criterion”
that would prove this theoretically) could be a use-
ful tool for developing and debugging grammars.
From a more abstract point of view, our evalua-
tion contributes to the fundamental question of what
expressive power an underspecification formalism
needs. It turned out that the distinction between qeq
 1
 10
 100
 1000
 10000
 100000
 1e+06
 0  5  10  15  20  25  30
Time (ms)
Size (number of dominance edges)
DC solver (LEDA)
MRS solver
Figure 9: Comparison of runtimes for the MRS and dominance constraint solvers.
and dominance hardly plays a role in practice. If the
net hypothesis is true, it also follows that merging is
not necessary because EP-conjunctions can be con-
verted into ordinary conjunctions. More research
along these lines could help unify different under-
specification formalisms and the resources that are
available for them.
Acknowledgments We are grateful to Ann
Copestake for many fruitful discussions, and to our
reviewers for helpful comments.
References
H. Alshawi and R. Crouch. 1992. Monotonic se-
mantic interpretation. In Proc. 30th ACL, pages
32–39.
Ernst Althaus, Denys Duchier, Alexander Koller,
Kurt Mehlhorn, Joachim Niehren, and Sven
Thiel. 2003. An efficient graph algorithm for
dominance constraints. Journal of Algorithms,
48:194–219.
Manuel Bodirsky, Denys Duchier, Joachim Niehren,
and Sebastian Miele. 2004. An efficient algo-
rithm for weakly normal dominance constraints.
In ACM-SIAM Symposium on Discrete Algo-
rithms. The ACM Press.
Ann Copestake and Dan Flickinger. 2000. An
open-source grammar development environment
and broad-coverage english grammar using
HPSG. In Conference on Language Resources
and Evaluation.
Ann Copestake, Dan Flickinger, Rob Malouf, Su-
sanne Riehemann, and Ivan Sag. 1995. Transla-
tion using Minimal Recursion Semantics. Leu-
ven.
Ann Copestake, Alex Lascarides, and Dan
Flickinger. 2001. An algebra for semantic
construction in constraint-based grammars. In
Proceedings of the 39th Annual Meeting of the
Association for Computational Linguistics, pages
132–139, Toulouse, France.
Ann Copestake, Dan Flickinger, Carl Pollard, and
Ivan Sag. 2004. Minimal recursion semantics:
An introduction. Journal of Language and Com-
putation. To appear.
Ann Copestake. 2002. Implementing Typed Feature
Structure Grammars. CSLI Publications, Stan-
ford, CA.
Markus Egg, Alexander Koller, and Joachim
Niehren. 2001. The Constraint Language for
Lambda Structures. Logic, Language, and Infor-
mation, 10:457–485.
K. Mehlhorn and S. Näher. 1999. The LEDA Plat-
form of Combinatorial and Geometric Comput-
ing. Cambridge University Press, Cambridge.
See also http://www.mpi-sb.mpg.de/LEDA/.
Joachim Niehren and Stefan Thater. 2003. Bridg-
ing the gap between underspecification for-
malisms: Minimal recursion semantics as dom-
inance constraints. In Proceedings of the 41st
Annual Meeting of the Association for Computa-
tional Linguistics.
Stephan Oepen, Kristina Toutanova, Stuart Shieber,
Christopher Manning, Dan Flickinger, and
Thorsten Brants. 2002. The LinGO Redwoods
treebank: Motivation and preliminary applica-
tions. In Proceedings of the 19th International
Conference on Computational Linguistics
(COLING’02), pages 1253–1257.
Manfred Pinkal. 1996. Radical underspecification.
In 10th Amsterdam Colloquium, pages 587–606.
