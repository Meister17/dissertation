References
Steven Abney. 1990. Rapid Incremental Parsing with Repair. In Proceedings of the 8th New OED Conference: Electronic Text Research. University of Waterloo, Ontario. 

Ann Bies, Mark Fergusson, Karen Katz, and Robert MacIntyre. 1995. Bracketing Guidelines for Treebank II Style Penn Treebank Project. Technical report, University of Pennsylvania. 

Eric Brill. 1994. Some advances in rule-based part of speech tagging. In Proceedings of the Twelfth National Conference on Artificial Intelligence (AAAI-94). Seattle, Washington. 

Xavier Carreras and Lluis Marquez. 2001. Boosting Trees for Clause Splitting. In Proceedings of CoNLL-2001. Toulouse, France. 

Herve Dejean. 2001. Using ALLis for clausing. Proceedings of CoNLL-2001. Toulouse, France. 

Eva Ejerhed. 1988. Finding clauses in unrestricted text by finitary and stochastic methods. In Pro ceedings of the second Conference on Applied Natural Language Processing, pages 219-227. 

Eva Ejerhed. 1996. Finite state segmentation of dis course into clauses. In Proceedings of the ECAI '96 Workshop on Extended finite state models of language. ECAI '96, Budapest, Hungary. 

James Hammerton. 2001. Clause identification with Long Short-Term Memory. In Proceedings of CoNLL-2001. Toulouse, France. 

Vilson J. Leffa. 1998. Clause Processing in Complex Sentences. In Proceedings of LREC'98. Granada, Spain. 

Mitchell P. Marcus, Beatrice Santorini, and Mary Ann Marcinkiewicz. 1993. Building a large annotated corpus of English: the Penn Treebank. Computational Linguistics, 19(2). 

Antonio Molina and Ferran Pla. 2001. Clause De tection using HMM. In Proceedings of CoNLL-2001. Toulouse, France. 

Constantin Orasan. 2000. A hybrid method for 
clause splitting in unrestricted English texts. In Proceedings of ACIDCA'2000. Monastir, Tunisia. 

H. V. Papageorgiou. 1997. Clause recognition in the framework of alignment. In R. Mitkov and N. Nicolov, editors, Recent Advances in Natural Language Processing.

Jon D. Patrick and Ishaan Goyal. 2001. Boosted decision graphs for NLP learning tasks. CoNLL 2001.

Lance A. Ramshaw and Mitchell P. Marcus. 1995. Text Chunking using transformation-based learning. ACL Workshop on Very large corpora.

Erik F. Tjong Kim Sang. 2001. Memory-based clause identification. CoNLL 2001.

Erik F. Tjong Kim Sang and Sabine Buchholz. 2000. Introduction to the CoNLL-2000 shared task: chunking. CoNLL LLL 2000.

Erik F. Tjong Kim Sang. 2000. Text chunking by system combination. CoNLL LLL 2000.

C. J. van Rijsbergen. 1975. Information Retrieval. Buttersworth.
