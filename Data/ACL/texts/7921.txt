References

1   Baggia P., Kelner A., P�rennou E., Popovici C., Strum J., Wessel F. 1999. Language Modeling and Spoken Dialogue Systems the ARISE experience. Eurospeech99, pp. 1767--1770. 

2   Bonafonte A., Castell N., LLeida E., Mari�o J. B., Sanchis E., Torres M. I., Aibar P. 2000. Desarrollo de un sistema de di�logo oral en dominios restringidos. I Jornadas en Tecnolog�a del Habla, Sevilla. 

3   L. Lamel , S. Rosset , J. L. Gauvain , S. Bennacef , M. Garnier-Rizet , B. Prouts, The LIMSI ARISE system, Speech Communication, v.31 n.4, p.339-353, Aug. 2000 

4   Martinez C., and Casacuberta F. 2000. A pattern recognition approach to dialog labelling using finite-state transducers. V Iberoamerican Symposium on Pattern Recognition, pp. 669--677. 

5   Pieraccini R, Levin E., and Eckert W. 1997. AMICA: the AT&T Mixed Initiative Conversational Architecture. Eurospeech97, pp. 1875--1878. 

6   Segarra E., Sanchis E., Galiano M., Garc�a F., Hurtado L. F. 2001. Extracting semantic information through automatic learning techniques. IX Spanish Symposium on Pattern Recognition and Image Analysis (AERFAI), Castell�n.
