References

1   M. Arbabi , S. M. Fischthal , V. C. Cheng , E. Bart, Algorithms for Arabic name transliteration, IBM Journal of Research and Development, v.38 n.2, p.183-194, March 1994 

2   Asanee Kawtrakul, Amarin Deemagarn, Chalathip Thumkanon, Navapat Khantonthong, and Paul McFetridge. 1998. Backward Transliteration for Thai Document Retrieval. In Proceedings of the 1998 IEEE Asia-Pacific Conference on Circuits and Systems (APCCAS), pages 563--566. 

3   Kevin Knight , Jonathan Graehl, Machine transliteration, Proceedings of the eighth conference on European chapter of the Association for Computational Linguistics, p.128-135, July 07-12, 1997, Madrid, Spain 

4   Klaus Lagally. 1999. ArabTEX: A System for Typesetting Arabic, User Manual Version 3.09. Technical Report 1998/09, Universitat Stuttgart, Fakultät Informatik, Breitwiesenstraße 20--22, 70565 Stuttgart, Germany. 

5   Bonnie G. Stalls and Kevin Knight. 1998. Translating Names and Technical Terms in Arabic Text. In Proceedings of the COLING/ACL Workshop on Computational Approaches to Semitic Languages. 
