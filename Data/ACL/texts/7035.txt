References

1   Hiyan Alshawi , Shona Douglas , Srinivas Bangalore, Learning dependency translation models as collections of finite-state head transducers, Computational Linguistics, v.26 n.1, p.45-60, March 2000 

2   D. Carter, M. Rayner, et al. 2000. Evaluation. In Rayner et al. (Rayner et al., 2000). 

3   FlexiPC, 2002. http://www.flexipc.com/product/, then "translator". As of 15 Mar 2002. 

4   R. Frederking, A. Rudnicky, and C. Hogan. 1997. Interactive speech translation in the diplomat project. In Proceedings of the Spoken Language Translation workshop at the 35th Meeting of the Association for Computational Linguistics, Madrid, Spain. 

5   IntegratedWaveTechnologies, 2002. http://www.i-w-t.com/investor.html. As of 15 Mar 2002. 

6   Nuance, 2002. http://www.nuance.com. As of 1 Feb 2002. 

7   M. Rayner, D. Carter, P. Bouillon, V. Digalakis, and M. Wir�n, editors. 2000. The Spoken Language Translator. Cambridge University Press. 

8   M. Rayner, J. Dowding, and B. A. Hockey. 2001. A baseline method for compiling typed unification grammars into context free language models. In Proceedings of Eurospeech 2001, pages 729--732, Aalborg, Denmark. 

9   M. Th�riault, 2002. Questionnaire de d�pistage pour adultes (in French). As of 15 Mar 2002. 

10   W. Wahlster, editor. 2000. Verbmobil: Foundations of Speech-to-Speech Translation. Springer.
