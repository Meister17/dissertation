The Semantics of Metaphor in the Game Theoretic Semantics
 with at Least Two Cordination Equilibria
Chiaki Ohkura
Division of Information Science
Graduate Schol of Science and Technology
Chiba University
1-3 Yayoicho, Inageku, Chiba 263-852 Japan
cohkura@cogsci.L.chiba-u.ac.jp
Abstract
The objective of this paper is to present a
new dimension of Game Theoretic Se-
mantics (GTS) using the idea of the cor-
dination problem game to explain the
semantics of metaphor. A metaphorical
expresion
1
 such as ‘man is a wolf’ is a
contradictory statement that insists al
objects in the set of ‘man’ also fals under
‘wolf’. The study of metaphorical expres-
sion should be on intentions of such con-
tradictory language use, their intended
efect, and the conditions to lead to the ef-
fects. This is the tradition of Rhetoric
since Aristotle. It may be natural to char-
acterize this aproach as pragmatic in the
tradition of the late 20th century paradigm
of linguistics, which is the trichotomy of
syntax, semantics and pragmatics. How-
ever the pragmatic aproach canot ex-
plain what Richards (1936) caled
‘tension’ betwen two thoughts in a meta-
phorical expresion. GTS has ben devel-
                                                            
1
 In this paper, I say ‘metaphorical expresion’ rather
than ‘metaphor’. ‘Metaphor’ is comonly used as a
topic, but it is used at word level in traditional rhetoric
and at sentence level after Black. Also, ‘metaphor’ is
used for both metaphorical expresion and a kind of
trope. So I use ‘metaphorical expresion’ to refer lin-
guistic expresion which is comonly caled ‘metaphor’.
oped as a posible substitute to the Tar-
skian truth conditional semantics. This
paper explores a new posibility of GTS
in the cordination problem game intro-
duced by Lewis (1961) for a semantics,
which admits the plurality of meanings.
1 Introduction
The objective of this paper is to present a new di-
mension of Game Theoretic Semantics (GTS) us-
ing the idea of the cordination problem game in a
search for the semantics of metaphor:
(ex.1) ‘Man is a human being.’
(ex.2) ‘Man is a wolf.’
If being ‘man’ presuposes being ‘human be-
ing’, then what (ex.1) means is that al objects
faling under the category (or the set) of ‘man’ also
fal under ‘human being’, so (ex.1) must be true a
priori. In the same way, (ex.2) means that al ob-
jects faling under ‘man’ also fal under ‘wolf’, and
this must be false, a contradictory expresion. But
we can easily imagine some situations where (ex.2)
would be meaningful.
EXAMPLE: Sue went to a party. Her friend,
Pam introduced Ian to her because she knew
Sue had broken up recently, and she had a be-
lief that Sue should have a new partner. Sue
enjoyed the conversation with him and found
him very atractive. Ian apeared to fel the
same way. When Sue tok her leave, Ian of-
fered a lift for her. Pam, who was watching out
for them, winked and said ‘Sue, ‘man is a
wolf’, you know’.
In this example, Pam’s utering ‘man is a wolf’
does not sem contradictory. It is very natural and
loks meaningful even though it may admit several
interpretations. Therefore, this kind of sentence can
be meaningful in our everyday usage.
But utering a contradictory expresion is a de-
viation from our general understanding about lan-
guage use. The uterer’s intentions as to why such
a deviation is made, its intended efects and the
conditions to materialize the intended efects: these
have ben the objective of the study of metaphor in
the tradition of rhetoric.
Metaphor has ben regarded as a part of rheto-
ric. But if we focus on the fact that it uses ‘uterer’
and ‘use’ as its items, it may be natural to regard
rhetoric as a part in pragmatics in the trichotomy of
syntax, semantics and pragmatics in the late 20th
century linguistic paradigm. In the first section, I
wil examine the pragmatic aproach, Aristotle and
Davidson, and make clear that these are not
enough to present what metaphor means, and we
ned to chose a semantic aproach.
Richards, who opened the new path to seman-
tics, introduced the idea of ‘tension’. In the tradi-
tion of rhetoric, metaphor had ben thought to have
‘two thoughts in a sentence’
2
 (Richards). Richards
explained the special property that metaphorical
expresion has and normal expresion does not, is
in the tension, which is born betwen these two
thoughts in one expresion. What Richards cals
‘tension’ is the interaction of meanings betwen
literal meaning and metaphorical meaning in an
expresion. This ‘tension’, which canot be ex-
plained by a pragmatic aproach, is the foundation
of the semantic aproach lead by Richards and
Black.
                                                            
2
 What Richards means by ‘thoughts’ is not clear, but I
asume it is literal meaning and metaphorical meaning.
Partly he sems to admit two meanings and partly he
sems to admit only one meaning in metaphorical ex-
presion. Sometimes he sems to mean comparison ob-
jects as in comparison theory. However what is
important is not what he meant, but what he sugests.
However, even semantic aproach had failed to
explain ‘tension’ completely. It is because major
semantics based on the Tarskian truth definition
state the meaning of a sentence is only one. ‘Ten-
sion’ neds to have more than two meanings in a
sentence by definition.
Existing GTS uses mainly zero-sum games. The
equilibrium is coresponded to the meaning of a
sentence, so each sentence has one meaning. This
does not met the condition of meaning that we
demand for the semantics of metaphor in the line
of Richards’ idea.
The cordination problem game is a type of
game which Lewis analyses as a part of this theory
of convention (Lewis, 1961). There must be at
least more than two equilibria in the game by defi-
nition. If we aply this game as a model of mean-
ing in GTS, those equilibria can corespond to the
plural meanings in a metaphorical expresion. This
conclusion promises us a simple model of the
complex state of meanings in figurative language.
2 From Pragmatic Aproach to Semantic
Aproach
In this section, I criticize Aristotle and Davidson as
representatives of the pragmatic aproach to the
study of metaphor and show that the semantic ap-
proach is necesary for the study of metaphorical
expresions. Afterwards, I analyze Richard and
Black’s semantic aproaches and point out that
pragmatic elements are stil left in their aproaches.
A more abstract semantic aproach is neded.
2.1 Aristotle and Traditional Rhetoric
Aristotle’s ‘Poetics’ and ‘Rhetoric’ have ben
the foundation of rhetorical studies for more than
230 years. Aristotle regards metaphor as transfer-
ence and this thought has lasted as the core idea in
the tradition of rhetoric.
Metaphor is the aplication of an alien name by
transference either from genus to species, or
from species to genus, or from species to spe-
cies, or by analogy, that is, proportion. (1457b)
Acording to Aristotle, expresion ‘the twilight
of gods’ is a transference betwen ‘twilight’ and
‘last’ which have similarity (end of something). By
similarity, words are conected and can have the
other’s meaning. In other words, similarity lets
words have other meanings (metaphorical mean-
ings) that the words originaly do not have. Black
cals it the substitution view of metaphor (1962).
Substitution theory insists that the reason for
metaphorical expresion is in its psychological ef-
fects to hearers. Here, metaphor is studied in terms
of how it is used and what the efect is of language
use. This let us categorize substitution theory as a
part in pragmatics. Pragmatics is the study about
how and why language is used, using user and use
as the terms. So does substitution theory.
2.2 Davidson’s Pragmatics
Davidson is regarded as one of the leading phi-
losophers in the pragmatic aproaches. He denies
metaphorical meaning in metaphorical expresion,
which has never ben doubted without much ar-
gument. Metaphorical expresions have literal
meaning only. We realize an expresion is meta-
phorical though a purely pragmatic proces. An
uterance is made and the conditions bring us to
realize that the meaning of the uterance is not lit-
eral (Davidson, 1978).
 What Davidson explains is only this realization
of non-literal meaning. Metaphorical meaning it-
self is denied and he makes it clear that there is no
such thing as metaphorical meaning, a special kind
of mental, cognitive property. This is remarkable
progres from Aristotelian substitution theory in
terms of avoiding problems of paraphrasing which
I wil criticize in the next sub-section in detail. Da-
vidson says; ‘If this is right, what we atempt in
“paraphrasing” a metaphor canot be to give its
meaning, for that lies on the surface; rather we at-
tempt to evoke what the metaphor brings to our
atention.’ (Davidson, 1978)
What paraphrasing atempts is not similarity
betwen literal meaning and metaphorical meaning.
Since Davidson denies metaphorical meaning itself,
he does not ned to compare two meanings.
There are at least two points to criticize David-
son. Firstly, to pursue what is the meaning of
metaphor itself is a very important question and
Davidson’s reply is just an escape from the prob-
lem, though it is quite a remarkable escape. One
says ‘he is a pumpkin’ and if another does not un-
derstand the uterance and asks what it means,
paraphrasing (‘it means he is very stupid’) is a pos-
sible answer and the paraphrasing does have sense.
Paraphrasing may not be able to have exactly the
same mental content that the original metaphorical
expresion has, but it does have a part of it. Other-
wise the hearer canot be persuaded.
Secondly, insisting there is no such thing as
metaphorical meaning is a revolution and very in-
spiring, but it might be to unatural since we can
talk and wonder about it. In this field of study, re-
ality has a special sense because what we argue
about is mainly abstract objects that do not ned to
exist. However, we should stick to our reality, our
sense of naturalnes, not to recreate our language
understanding to fit our theories. What maters to
us with metaphorical expresions are not only the
uterer’s intention, but also the meaning of the ex-
presions.
2.3 Problems in Substitution Theory
The first two of the folowing points are major
criticisms of substitution theory. Firstly, the meta-
phorical meanings of a metaphorical expresion do
not substitute al of the literal meaning. The second
point reflects my idea of understanding meanings.
Substitution theory says that similarity makes a
metaphor, similarity in the meanings of two com-
pared ideas (literal meaning and metaphorical
meaning) makes a metaphorical expresion. But
this is complete oposite.
The first objection, that metaphorical meanings
in a metaphorical expresion do not substitute al
the literal meanings of the expresion, is, in other
words: paraphrasing of a metaphorical expresion
canot present al the meanings that the original
metaphorical expresion has.
(ex.3) She is a lily of valey.
(ex.4) She is a dewy red rose.
These expresions can be paraphrased;
(ex.5) She is beautiful.
It is aparent that (ex.5) hold a part of the mean-
ings that (ex.3) and (ex.4) have, but not al of them,
because the same person may not use (ex.3) and
(ex.4) to the same woman with the same sense of
values. Repetition of paraphrasing may increase
the richnes of the explanation and lead to beter
understanding, but it wil never reach the ful
meaning of the original expresion.
The second objection claims that similarity
does not make metaphor, but metaphor makes us
realize the similarity. As Eisenstein created the
‘flow’ of story by montage sequence in his film
‘Batleship Potemkin’, people try to find seman-
tic conection when some information is given in a
line. Especialy, if it is presented as a sentence, we
almost automaticaly try to find ‘meaning’ in it.
‘Colorles gren ideas slep furiously’ is a famous
meaningles sentence writen by Chomsky. He
made up this sentence as nonsensical sentence, but
he himself admits that it can be interpreted as a
metaphor. It can be a headline of newspaper that
tels Grenpeace members who lost their livelines
to stop their political activities.
Those objections above to pragmatic aproach
3
to metaphor sugest that there are elements in the
study of metaphor that are to be left for the seman-
tic aproach. In the next sub-section, I wil revise
the semantic aproach in Richards and Black.
2.4 Richards’ Interaction Theory
Richards introduces interaction theory that insists
the meaning of metaphor is the tension betwen
two thoughts (literal meaning and metaphorical
meaning) in a sentence. Before him, metaphor had
ben studied at word level. Richards brought the
viewpoint of semantics which focus on meaning at
sentence level.
Another character of his theory is to demand
unlikelines betwen literal meaning and meta-
phorical meaning. This unlikelines is the source of
the tension, literal, artistic value that the metaphor
has. Richards says, ‘as the two things put together
are more remote, the tension created is, of course,
greater’.
2.5 Black’s Interaction Theory
Black develops interaction theory to more rigid
theory that covers not only figurative language but
the whole of language. He introduces the idea of
‘focus’ and ‘frame’. For Black, metaphorical ex-
presion ‘Man is a wolf’ is an expresion that has a
                                                            
3
 Here, I would like to state what I mean by ‘pragmatics’.
I interpreted Aristotle as a pragmatist. In Aristotle, lan-
guage use is judged as rhetoric when the use is under-
stod. It is a context-conscious theory. Semantics
caries a diferent task. It should give the foundation of
meaning. It should give other property diferent from
the condition of language use.
focus ‘a wolf’ in a linguistic frame of ‘man is’.
This frame is literal, but it wil be contradictory
when ‘a wolf’ is also literal (principal subject). We
realize this categorical mistake and find ‘focus’
metaphorical meaning (subsidiary subject) led by
asociation of comonplaces. Asociated com-
monplaces enable us to share the same metaphori-
cal meaning for the same expresion. Literal
meaning is infered first, then after realizing its
categorical mistake, metaphorical meaning is in-
fered. This inference model is often used in the
semantic aproach after Black.
2.6 Problems in Interaction Theory
In the two previous sub-sections, I have revised the
main characters of Richards and Black’s interac-
tion theory. These are more sophisticated than
Aristotelian substitution theory for admiting the
dynamics of meaning. However some problems
also remain in interaction theory.
Firstly, Black suceds to explain why we un-
derstand the so-caled ‘dead metaphor’ in the same
way, why we se similarities in metaphor by intro-
ducing the idea of ‘asociated comonplaces’. But
this also weakens the creativity of new metaphori-
cal expresions. If ‘asociated comonplaces’
gives us stable understanding of meanings, this
also prevent us not to have a new metaphorical
meaning out of the system.
Secondly, although Black strongly insists the
necesity of semantics in his study of metaphor,
Black’s theory uses the inference model which is
very pragmatic. What he tries to expres in his se-
mantics was the meaning which is understod in
context. Therefore he neds inference model and
asociated comonplaces. I do not deny the infer-
ence model. We use our inference in our language
use to determine one meaning for an expresion.
But semantics, which gives the foundation of
meanings to language use, should avoid such a
pragmatic proces. We should separate semantics
and pragmatics as clearly as posible.
3 Tasks for the Semantics of Metaphor
In the previous section, we have overloked some
major theories for metaphor. I sorted them into two
types, the pragmatic aproach and the semantic
aproach. What is characteristic in the pragmatic
aproach is that what maters in a metaphor is its
use and efect. In Davidson, he even denies meta-
phorical meaning and replaces it with the psy-
chological efect of literal meaning. The semantic
aproach like Black criticizes Aristotelian para-
phrasing and admits metaphorical meaning deliv-
ered though inference. This also means that
metaphorical meaning is subsidiary meaning.
These two are very complex and we canot se
a clear confrontation betwen them. The pragmatic
aproach canot avoid refering to metaphorical
meaning when we consider the reason of meta-
phorical language use. The semantic aproach also
canot evade using inference model, which may
belong to pragmatics.
Let’s be naive, and remember how we read a
new metaphorical expresion.
(ex.6)
‘She was a swan floating on the sea;
alone swan on the sea of sorow
blue sea, nor blue sky could not hold her.’
Do we think about their literal meanings? No
4
. We
understand the metaphorical meaning of ‘sea of
sorow’ before thinking of its literal meaning. And
when we enjoy its literal value, we read (or hear) it
again and again, may think about its metaphorical
meaning and literal meaning. We do enjoy these
two thoughts in an expresion, and the tension that
those thoughts create. In this acount, I folow in-
teraction theory that Richards insists.
What I have argued in this section are;
1. A metaphorical expresion has some mean-
ings including literal meaning and metaphori-
cal meaning.
2. Metaphorical meaning canot be paraphrased.
Repeating paraphrasing canot give the
original metaphorical meaning.
3. Semantics is a description of language ex-
presion, not language interpretation, so it
                                                            
4
 I have received some inspiring comentary on this
point. Some argue that they gain mental image of a lit-
eral sea imediately after reading it. My answer to this
claim is what they gained is actualy a metaphorical
meaning of the expresion. Literal ‘sea’ has a contra-
diction to ‘of sorow’. The image may resemble to its
literal meaning though.
should exclude the inference proces as a part
of it.
The four theorists we have reviewed canot
satisfy these thre conditions. To met the demand,
we ned to have a semantics, which alows the plu-
rality of meanings in an expresion.
4 GTS with the Idea of Cordination
Problem Game
GTS (Game Theoretic Semantics) has ben devel-
oped as an alternative semantics where major se-
mantics have ben based on Tarski’s definition of
truth. As the name shows, GTS is a semantics that
models the idea of game theory. Most GTS theo-
rists use zero-sum games betwen two players,
Myself (Verifier) and Nature (Falsifier) for in Hin-
tika, for example. Verifier tries to make a sen-
tence true and Falsifier does the oposite. Whether
the sentence is true or false is known by knowing
who wil win the game. It is to state one meaning
for a sentence, and the meaning is compared with
an equilibrium in a game. However, if we kep
using zero-sum games, we canot represent more
than two meanings in a sentence.
4.1 Cordination Problem Game
Cordination problem game, which is analyzed
and defined by Lewis (1961), is a game with at
least two equilibria. Supose we are talking on the
telephone and in the midle of the conversation,
the line is sudenly cut of. I wil wonder whether I
should cal back because if you cal me while I cal
you, the line wil be busy and we canot reach
each other. At the same time, you must be won-
dering if you should cal me. This is a problem.
In this telephone line case, it can be a problem
because there are at least two equilibria in this
situation; one is the caler cals back and the re-
ceiver waits, and another is the caler waits and the
receiver cals back. Which to chose is not a prob-
lem. The important thing is that there are two
equilibria, which are indiferent for the caler to
chose, so they wonder. Lewis cals this kind of
problem ‘cordination problems’, and equilibrium
‘cordination equilibrium’. Players of these games
wish to cordinate acording to their expectations
about what the other is going to do to gain a beter
outcome comparing to not cordinating.
The cordination problem games have at least
two cordination equilibria by definition. There-
fore using GTS with a cordination problem game,
we can represent a sentence having more than two
meanings.
A metaphorical sentence coresponds to a cor-
dination problem game which has two equilibria
that corespond to literal meaning and metaphori-
cal meaning. Since both cordination equilibria are
same in its role as equilibrium, none of the mean-
ings is superior or inferior (Figure 1).
Cordination
Equilibrium
Literal
Meaning
Cordination
Equilibrium
Metaphorical
eaning
Figure 1
Interestingly, this cordination problem game
also has ‘tension’. A player of this game has to
make a choice which wil be meaningles if his
choice difers from another’s. We do not wonder
when we have only one choice. e have to take it.
But the players in cordination problem game have
at least two choices. This presence of two choices
itself is the ‘tension’ of the metaphorical expres-
sion. Being of one afects the other. They interact.
The reason of enjoyable uneasines of metaphori-
cal expresion is in this interaction in this
polysemy.
With GTS extended by cordination problem
game, we gain the folowing thre points.
Firstly, semantics that alows more than two
meanings for a sentence is posible. The semantics
also promises us to find both literal and metaphori-
cal meanings in a sentence.
As a result of the first point, metaphorical
meaning does not remain in its subsidiary position
infered from primary meaning. It is a meaning as
important as literal meaning.
The being of two equilibria represents ‘tension’
that Richards insists as the character of the mean-
ing of metaphorical expresions.
4.2 Diference from Hintika’s GTS
This cordination problem game is very diferent
from what Hintika and his folowers use. In Hin-
tika’s GTS, game directs the logical operation of
logic of a sentence in order to state truth and false-
hod of the sentence. It is because the meaning of
the expresion is to be one.
On the contrary, cordination problem game it-
self canot be operated as Hintika’s does because
the game has two equilibria and this is the point of
the game.
However Lewis starts his analysis from the
game and ends in convention. Acording to Lewis,
people in the il telephone line area start making
convention that one of them (caler or receiver)
should cal back. After the convention spreads over
the area, the game has a unique equilibrium and the
residents find the problem solved
5
.
In the proces of building up convention, the
elements to determine which equilibrium to fal in
are out of the game. In the case of metaphor, I let
them belong to the area of pragmatics. Though
context, we use our inference system to determine,
to grasp what is meant by the expresion when it is
polysemous. We may know what is the subject,
what is the intention of the uterer, which meaning
should be apropriate in the time and place when it
is expresed. These are the problems to be left in
the consideration of the inference proces.
4.3 Problem of ‘Dead Metaphor’
What makes the diference betwen literal meaning
and metaphorical meaning? I think this is just a
mater of frequency of use.
‘Dead metaphor’ is a type of metaphor of which
metaphorical meaning is rigid and most of us un-
derstand it in same way so that often we find its
metaphorical meaning in dictionary. On the other
hand, we have very new, poetic metaphors that
may be understod in various ways. Also there are
metaphors of which metaphorical meaning became
its literal meaning and do not have original literal
meaning as you se in ‘leg of chair’.
                                                            
5
 It may remain as a cordination problem game because
any new resident moves to the area, they may have
same problem until he finds out the convention or is told
by someone. The potential posibility of the problem is
always there.
The diferences betwen those types of meta-
phors are mater of frequency of use. If a very new
metaphorical expresion is used, its metaphorical
meaning may be ambiguous, can be paraphrased
but posibly misunderstod. But as the expresion
is used again and again, its metaphorical meaning
grows to gain comon interpretation in the used
language. As the metaphorical meaning becomes
comon understanding of the expresion, the
metaphorical expresion starts losing its tension
betwen literal meaning and metaphorical meaning.
This is how a metaphorical expresion is born and
dies.
This is stil just a sugestion of the life of a
metaphorical expresion. One of the bigest prob-
lems to this acount is that it does not explain how
we understand very new metaphorical expresion
which we had never heard of. We ned the study of
other level of meaning to explain it. But stil, if we
understand those diferent types of metaphorical
expresions are on a same line, continuous being, it
may be fruitful of the semantics of metaphorical
expresion.
5 Why More Than Two Meanings?
In this section, I wil spread my idea of the mean-
ings in metaphorical expresions through some
observations of the usage.
5.1 Meanings in a Polysemous Expresion
One of the main points of this paper is that it is
aiming to expres the state of meaning of an ex-
presion, not the meaning that is understod. In
everyday conversation, we often asure what is the
meaning of the expresion that we use with phases
like ‘you know what I mean.’ ‘the meaning of this
term is..’. This is necesary when we come acros
some polysemous expresion.
If the meaning of the sentence is clear in its
context, then uterer does not ned to asure its
meaning, but when the uterer asure its meaning
with other words. Here, we se two characters of
polysemous expresion. Firstly, a polysemous ex-
presion has more than two meanings. Secondly, a
polysemous expresion ned to be defined its
meaning in the usage by context.
5.2 Metaphorical Meanings in an Expresion
Let’s go back to our familiar example.
(ex.2) ‘Man is a wolf’
This is often understod as a metaphor to expres
man’s hunger and brutality. However ‘wolf’ has
diferent aspects in its image. In Mongolia, wolf is
the divine animal and the Mongolian hero, Ching-
gis Khan is caled ‘blue wolf’. Other East Asian
myths regard Sirius as wolf and atributes loneli-
nes and rationality to the star. In this context,
‘man is a wolf’ may means ‘man is a creature
which has clear eyes to se and judge with his own
evaluation, holds his own view even others are
against him’. This image may also be found in
‘lone wolf’ in English.
What I intend to do here is not insisting multi-
culturalism. What I do is to insist that metaphorical
expresions are polysemous expresions. And as I
wrote above, the meaning of polysemous expres-
sion is determined in its context (if it is sucesful).
As the name ‘polysemy’ tels, we atribute more
than two meanings to an expresion. Why can’t we
do same treatment to metaphors?
The ‘man is a wolf’ expresion has at least one
literal meaning which is a false expresion in our
world, and two metaphorical meanings. When we
say we understand the meaning of an expresion,
this means that we chose a meaning of the ex-
presion sucesfuly. The meaning of a meta-
phorical expresion is infered and one of them is
chosen.
Metaphorical expresions are often found in
poetical expresions.
(ex.7) ‘my love wears forbiden colors’
What is ‘forbiden colors’? How ‘love’ wears
color? As the expresion is newer, we consider
what is realy meant and try to grasp its metaphori-
cal meaning through its literal meaning, we come
and go betwen those two meanings
6
. In this proc-
es, we se both meanings in a metaphorical ex-
presion. When we ‘taste’ a poetic expresion, we
often go through this proces and this proces itself
is a part of the ‘tasting’ of the poem. When we en-
joy poems, we enjoy how the literal meaning and
                                                            
6
‘grasp its metaphorical meaning through its literal
meaning’ sounds as if I admit to use the inference model
for semantics. But here, I am talking about how e un-
derstand, not about semantics.
metaphorical meaning are related. This is what
Richards caled ‘tension’ and this is the meaning of
metaphorical expresion. In order to expres this
‘tension’, we should admit both literal and meta-
phorical meanings in a sentence.
5.3 The Meanings of ‘meaning’
The semantics with cordination problem game
have diferent meaning of ‘meaning’ from the one
in traditional understanding of metaphor.
When they ask ‘what metaphor mean?’, they
have a presuposition that there is a semantic
property that coresponds to a metaphorical ex-
presion. With cordination problem game, what
coresponds to a metaphorical expresion is a game
that has two equilibria. The meaning of a meta-
phorical expresion is a game that has two ‘mean-
ings’, which are two choices with the tension
caused by the coexistence of the two meanings.
6 Conclusion
Metaphor is an expresion in which there is both
literal meaning and metaphorical meaning. This is
my answer to the question, ‘What is metaphor?’.
Metaphorical meaning is one of the meanings that
a metaphorical expresion has. It may be clear by
its context. But it is not infered from logical con-
tradiction when we understand the expresion with
its literal meaning.
Some, important problems are stil left unsolved.
I mention one of them as my next task to do im-
mediately: GTS extended with cordination prob-
lem game alows us to have more than two
meanings in an expresion or a sentence. However,
we canot distinguish these two meanings in this
GTS. Since we can distinguish which is literal and
which is metaphorical, we should be able to do the
same thing in our semantics.

References
Aristotle The Poetics, Translated by S. H. Butcher, The
Internet Clasics Archive by Daniel C. Stevenson,
Web Atomics.
URL=htp:/clasics.mit.edu/Aristotle/poetics.mb.txt
Aristotle 1954. Rhetoric, translated by W. Rhys Roberts,
hypertextual resource compiled by Le Honeycut
URL=htp:/ww.public.iastate.edu/~honeyl/Rhetori
c/index.html
Black, Max 1978. in Sacks 1978.
Black, Max 1962. Models and Metaphors: Studies in
Language and Philosophy, Cornel University Pres
Davidson, Donald 1978 in Sacks 1978.
Hintika, Jako and Kulas, Jack 1983. The Game of
Language: Studies in Game-Theoretical Semantics
and Its Aplications (Synthese Language Library;
v.2), D. Reidel Publishing Company
Hintika, Jako and Kulas, Jack 1985. Anaphora and
Definite Descriptions: Two Aplications of Game-
Theoretical Semantics (Synthese Language Library;
v.26), D. Reidel Publishing Company
Hintika, Jako and Sandu, Gabriel 197. in Handbok
of Logic and Language, ed. by J. van Benthem and A.
ter Meulen, Elsevier Science B. V.
Hodges, Wilfrid 201. Logic and Games, The Stanford
Encyclopedia of Philosophy, Fal 201 Edition, ed.
Edward Zalta,
URL=htp:/plato.stanford.edu/archives/fal201/entri
es/logic-games/
Lewis, David K. 1969. Convention | A Philosophical
Study, Harverd University Pres.
Ortony, Andrew, ed. 1979. Metaphor and Thought,
Cambridge University Pres
Richards, I.A. 1936. The Philosophy of Rhetoric, Ox-
ford University Pres
Sacks, Sheldon. ed. 1978. On Metaphor, The University
of Chicago Pres
