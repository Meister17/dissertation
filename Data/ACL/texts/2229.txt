Multi-Component TAG and Notions of Formal Power
William Schuler, David Chiang
Computer and Information Science
University of Pennsylvania
Philadelphia, PA 19104
fschuler,dchiangg@linc.cis.upenn.edu
Mark Dras
Inst. for Research in Cognitive Science
University of Pennsylvania
Suite 400A, 3401 Walnut Street
Philadelphia, PA 19104-6228
madras@linc.cis.upenn.edu
Abstract
This paper presents a restricted version
of Set-Local Multi-Component TAGs
#28Weir, 1988#29 which retains the strong
generativecapacityofTree-LocalMulti-
Component TAG #28i.e. produces the
same derived structures#29 but has a
greater derivational generative capacity
#28i.e. can derive those structures in more
ways#29. This formalism is then applied as
a framework for integrating dependency
and constituency based linguistic repre-
sentations.
1 Introduction
An aim of one strand of research in gener-
ative grammar is to #0Cnd a formalism that
has a restricted descriptive capacity su#0Ecient
to describe natural language, but no more
powerful than necessary, so that the reasons
some constructions are not legal in any nat-
ural language is explained by the formalism
rather than stipulations in the linguistic the-
ory. Several mildly context-sensitive grammar
formalisms, all characterizing the same string
languages, are currently possible candidates
for adequately describing natural language;
however, they di#0Ber in their capacities to as-
sign appropriate linguistic structural descrip-
tions to these string languages. The work in
this paper is in the vein of other work #28Joshi,
2000#29 in extracting as much structural de-
scriptive power given a #0Cxed ability to de-
scribe strings, and uses this to model depen-
dency as well as constituency correctly.
One way to characterize a formalism's de-
scriptivepower is by the the set of string lan-
guages it can generate, called its weak gener-
ative capacity. For example, Tree Adjoining
Grammars #28TAGs#29 #28Joshi et al., 1975#29 can
generate the language a
n
b
n
c
n
d
n
and Context-
Free Grammars #28CFGs#29 cannot #28Joshi, 1985#29.
S
a #0F b
S
a S
a #0F b
b
S
a S
a S
a #0F b
b
b
:::
Figure 1: CFG-generable tree set for a
n
b
n
.
S
a S
b #0F
S
a S
a S
b S
b #0F
S
a S
a S
a S
b S
b S
b #0F
:::
Figure 2: TAG-generable tree set for a
n
b
n
.
However, weak generative capacity ignores
the capacity ofa grammar formalismto gener-
ate derived trees. This is known as its strong
generative capacity. For example, CFGs and
TAGs can both generate the language a
n
b
n
,
but CFGs can only associate the a's and b's
by making them siblings in the derived tree,
as shown in Figure 1, whereas a TAG can gen-
erate the in#0Cnite set of trees for the language
a
n
b
n
that have a's and b's as siblings, as well
as the in#0Cnite set of trees where the a's dom-
inate the b's in each tree, shown in Figure 2
#28Joshi, 1985#29; thus TAGs have more strong
generative capacity than CFGs.
In addition to the tree sets and string lan-
guages a formalism can generate, there may
also be linguistic reasons to care about how
these structures are derived. For this reason,
multi-component TAGs #28MCTAGs#29 #28Weir,
1988#29 have been adopted to model some
linguistic phenomena. In multi-component
TAG, elementary trees are grouped into tree
sets, and at each step of the derivation all the
trees of a set adjoin simultaneously. In tree-
local MCTAG #28TL-MCTAG#29 all the trees of
a set are required to adjoin into the same
elementary tree; in set-local MCTAG #28SL-
MCTAG#29 all the trees of a set are required
to adjoin into the same elementary tree set.
TL-MCTAGs can generate the same string
languages and derived tree sets as ordinary
TAGs, so they have the same weak and strong
generative capacities, but TL-MCTAGs can
derive these same strings and trees in more
than TAGs can. One motivation for TL-
MCTAG as a linguistic formalism #28Frank,
1992#29 is that it can generate a functional head
#28such as does#29 in the same derivational step
as the lexical head with which it is associated
#28see Figure 3#29 without violating any assump-
tions about the derived phrase structure tree
#7B something TAGs cannot do in every case.
#0C
seem
:
S
does
S
.
.
.
VP
seem VP#03
#0B
sleep
:
S
John VP
to sleep
#0B
sleep
#0C
seem
S
does S
John VP
seem VP
to sleep
Figure 3: TL-MCTAG generable derivation
This notion of the derivations of a gram-
mar formalism as they relate to the struc-
tures they derive has been called the deriva-
tional generative capacity #281992#29. Somewhat
more formally #28for a precise de#0Cnition, see
Becker et al. #281992#29#29: we annotate each ele-
ment of a derived structure with a code indi-
cating which step of the derivation produced
that element. This code is simply the address
of the corresponding node in the derivation
tree.
1
Then a formalism's derivational gener-
ative capacity is the sets of derived structures,
thus annotated, that it can generate.
1
In Becker et al. #281992#29 the derived structures were
always strings, and the codes were not addresses but
unordered identi#0Cers. We trust that our de#0Cnition is
in the spirit of theirs.
The derivational generative capacity of a
formalism also describes what parts of a de-
rivedstructure combinewitheach other. Thus
if we consider each derivation step to corre-
spond to a semantic dependency, then deriva-
tional generative capacity describes what
other elements a semantic element may de-
pendon. That is,ifweinterpret the derivation
trees of TAG as dependency structures and
the derived trees as phrase structures, then
the derivational generative capacity of TAG
limitsthe possibledependencystructures that
can be assigned to a given phrase structure.
1.1 Dependency and Constituency
We have seen that TL-MCTAGs can gener-
ate some derivations for #5CDoes John seem
to sleep" that TAG cannot, but even TL-
MCTAG cannot generate the string, #5CDoes
John seem likely to sleep" with a derived tree
that matches some linguistic notion of correct
constituency and a derivation that matches
some notion of correct dependency. This is
because the components for `does' and `seem'
would have to adjoin into di#0Berent compo-
nents of the elementary tree set for `likely'
#28see Figure 4#29, whichwould require a set-local
multi-componentTAG instead of tree-local.
#0C
seem
:
S
does
S
.
.
.
VP
seem VP#03
#0C
likely
:
S
.
.
.
VP
likely VP#03
#0B
sleep
:
S
John VP
to sleep
#0B
sleep
#0C
likely
#0C
seem
Figure 4: SL-MCTAG generable derivation
Unfortunately, unrestricted set-local multi-
componentTAGs not only have more deriva-
tional generative capacity than TAGs, but
they also have more weak generative capac-
ity: SL-MCTAGs can generate the quadru-
ple copy language wwww, for example, which
does not correspond to any known linguis-
tic phenomenon. Other formalisms aiming to
model dependency correctly similarly expand
weak generative capacity, notably D-tree Sub-
stitution Grammar #28Rambow et al., 1995#29,
and consequently end up with much greater
parsing complexity.
The work in this paper follows another
Figure 5: Set-local adjunction.
line of research which has focused on squeez-
ing as much strong generative capacity as
possible out of weakly TAG-equivalent for-
malisms. Tree-local multicomponent TAG
#28Weir, 1988#29, nondirectional composition
#28Joshi and Vijay-Shanker, 1999#29, and seg-
mented adjunction #28Kulick, 2000#29 are exam-
ples of this approach, wherein the constraint
on weak generative capacity naturally limits
the expressivity of these systems. We discuss
the relation of the formalism of this paper,
Restricted MCTAG #28R-MCTAG#29 with some
of these in Section 5.
2 Formalism
2.1 Restricting set-local MCTAG
The way we propose to deal with multi-
component adjunction is #0Crst to limit the
number of components to two, and then,
roughly speaking, to treat two-component
adjunction as one-component adjunction by
temporarily removing the material between
the two adjunction sites. The reasons behind
this scheme will be explained in subsequent
sections, but we mention it now because it
motivates the somewhat complicated restric-
tions on possible adjunction sites:
#0F One adjunction site must dominate the
other. If the two sites are #11
h
and #11
l
, call
the set of nodes dominated by one node
but not strictly dominated by the other
the site-segment h#11
h
;#11
l
i.
#0F Removing a site-segment must not de-
priveatree of its foot node. That is, no
site-segment h#11
h
;#11
l
i may contain a foot
node unless #11
l
is itself the foot node.
#0F If two tree sets adjoin into the same tree,
the two site-segments must be simulta-
neously removable. That is, the two site-
segments must be disjoint, or one must
contain the other.
Because of the #0Crst restriction, we depict
tree sets with the components connected by
a dominance link #28dotted line#29, in the man-
ner of #28Becker et al., 1991#29. As written, the
above rules only allow tree-local adjunction;
we can generalize them to allow set-local ad-
junction by treating this dominance link like
an ordinary arc. But this would increase the
weak generative capacity of the system. For
present purposes it is su#0Ecient just to allow
one type of set-local adjunction: adjoin the
upper tree to the upper foot, and the lower
tree to the lower root #28see Figure 5#29.
This does not increase the weak generative
capacity, as will be shown in Section 2.3. Ob-
serve that the set-local TAG given in Figure 5
obeys the above restrictions.
2.2 2LTAG
For the following section, it is useful to think
of TAG in a manner other than the usual.
Instead of it being a tree-rewriting system
whose derivation history is recorded in a
derivation tree, it can be thought of as a set
of trees #28the `derivation' trees#29 with a yield
function #28here, reading o#0B the node labels of
derivation trees, and composing correspond-
ing elementary trees by adjunction or sub-
stitution as appropriate#29 applied to get the
TAG trees. Weir #281988#29 observed that several
TAGs could be daisy-chained into a multi-
level TAG whose yield function is the com-
position of the individual yield functions.
More precisely: a 2LTAG is a pair of
TAGs hG;G
0
i = hh#06;NT;I;A;Si;hI #5B A;I #5B
A;I
0
;A
0
;S
0
ii.
We call G the object-level grammar, and
G
0
the meta-level grammar. The object-level
grammar is a standard TAG: #06 and NT are
its terminal and nonterminal alphabets, I and
A are its initial and auxiliary trees, and S 2 I
contains the trees which derivations may start
with.
The meta-level grammar G
0
is de#0Cned so
that it derives trees that look like derivation
trees of G:
#0F Nodes are labeled with #28the names of#29
elementary trees of G.
#0F Foot nodes have no labels.
#0F Arcs are labeled with Gorn addresses.
2
2
The Gorn address of a root node is #0F;ifanodehas
Gorn address #11, then its ith child has Gorn address
#0B
#0C
#0B
Figure 6: Adjoining into #0B by removing #0C
#0B
.
#0F An auxiliary tree may adjoin anywhere.
#0F Whena tree #0C isadjoinedat a node #11, #11 is
rewritten as #0C, and the foot of #0C inherits
the label of #11.
The tree set of hG;G
0
i, T #28hG;G
0
i#29, is
f
G
#5BT #28G
0
#29#5D, where f
G
is the yield function of
G and T #28G
0
#29 is the tree set of G
0
. Thus, the
elementary trees of G
0
are combined to form
a derived tree, which is then interpreted as a
derivation tree for G, which gives instructions
for combining elementary trees of G into the
#0Cnal derived tree.
It was shown in Dras #281999#29 that when the
meta-level grammar is in the regular form of
Rogers #281994#29 the formalism is weakly equiv-
alenttoTAG.
2.3 Reducing restricted R-MCTAG
to RF-2LTAG
Consider the case of a multicomponent tree
set f#0C
1
;#0C
2
g adjoining into an initial tree #0B
#28Figure 6#29. Recall that we de#0Cned a site-
segment of a pair of adjunction sites to be all
the nodes which are dominated by the upper
site but not the lower site. Imagine that the
site-segment #0C
#0B
isexcised from #0B, and that #0C
1
and #0C
2
are fused into a single elementary tree.
Now we can simulate the multi-component
adjunction by ordinary adjunction: adjoin the
fused #0C
1
and #0C
2
into what is left of #0B; then
replace #0C
#0B
by adjoiningit between #0C
1
and #0C
2
.
The replacement of #0C
#0B
can be postponed
inde#0Cnitely: some other #28fused#29 tree set
f#0C
1
0
;#0C
2
0
g can adjoin between #0C
1
and #0C
2
, and
so on, and then #0C
#0B
adjoins between the last
pair of trees. This will produce the same re-
sult as a series of set-local adjunctions.
More formally:
1. Fuse all the elementary tree sets of the
grammar by identifying the upper foot
#11 #01 i.
with the lower root. Designate this fused
node the meta-foot.
2. For each tree, and for every possiblecom-
bination of site-segments, excise all the
site-segments and add all the trees thus
produced #28the excised auxiliary trees and
the remainders#29 to the grammar.
Now that our grammar has been smashed
to pieces, we must make sure that the right
pieces go back in the right places. We could do
this using features, but the resultinggrammar
would only be strongly equivalent, not deriva-
tionally equivalent, to the original. Therefore
we use a meta-level grammar instead:
1. For each initial tree, and for every pos-
sible combination of site-segments, con-
struct the derivation tree that will re-
assemble the pieces created in step #282#29
above and add it to the meta-level gram-
mar.
2. For each auxiliarytree, andfor every pos-
sible combination of site-segments, con-
struct a derivation tree as above, and for
the node which corresponds to the piece
containing the meta-foot, add a child, la-
bel its arc with the meta-foot's address
#28within the piece#29, and mark it a foot
node. Addthe resulting#28meta-level#29 aux-
iliary tree to the meta-level grammar.
Observe that set-local adjunction corre-
sponds to meta-level adjunction along the
#28meta-level#29 spine. Recall that we restricted
set-local adjunction so that a tree set can
only adjoin at the foot of the upper tree and
the root of the lower tree. Since this pair of
nodes corresponds to the meta-foot, we can
restate our restriction in terms of the con-
verted grammar: no meta-level adjunction is
allowed along the spine of a #28meta-level#29 aux-
iliary tree except at the #28meta-level#29 foot.
Then all meta-level adjunction is regular
adjunction in the sense of #28Rogers, 1994#29.
Therefore this converted 2LTAG produces
derivation tree sets which are recognizable,
and therefore our formalism is strongly equiv-
alenttoTAG.
Note that this restriction is much stronger
than Rogers' regular form restriction. This
was done for two reasons. First, the de#0Cni-
tion of our restriction would have been more
complicated otherwise; second, this restric-
tion overcomes some computational di#0Ecul-
ties with RF-TAG whichwe discuss below.
3 Linguistic Applications
In cases where TAG models dependencies cor-
rectly, the use of R-MCTAG is straightfor-
ward: when an auxiliary tree adjoins at a
site pair which is just a single node, it looks
just like conventional adjunction. However, in
problematiccases we can use the extra expres-
sivepower of R-MCTAG to model dependen-
cies correctly. Two such cases are discussed
below.
3.1 Bridge and Raising Verbs
S
NP
John
VP
V
thinks
S
.
.
.
S#03
S
C
that
S
.
.
.
VP
V
seems
VP#03
S
NP
Mary
VP
V
to sleep
Figure 7: Trees for #281#29
Consider the case of sentences which con-
tain both bridge and raising verbs, noted
by Rambow et al. #281995#29. In most TAG-based
analyses, bridge verbs adjoin at S #28or C
0
#29, and
raising verbs adjoin at VP #28or I
0
#29. Thus the
derivation for a sentence like
#281#29 John thinks that Mary seems to
sleep.
will have the trees for thinks and seems si-
multaneously adjoining into the tree for like,
which, when interpreted, gives an incorrect
dependency structure.
But under the present view we can ana-
lyze sentences like #281#29 with derivations mir-
roring dependencies. The desired trees for #281#29
are shown in Figure 7. Since the tree for that
seems can meta-adjoin around the subject,
the tree for thinks correctly adjoins into the
tree for seems rather than eat.
Also, although the above analysis produces
the correct dependency links, the directions
are inverted in some cases. This is a disad-
vantage compared to, for example, DSG; but
since the directions are consistently inverted,
for applications like translation or statistical
modeling, the particular choice of direction is
usually immaterial.
3.2 More on Raising Verbs
Tree-local MCTAG is able to derive #282a#29, but
unable to derive #282b#29 except by adjoining the
auxiliary tree for to be likely at the foot of the
auxiliary tree for seem #28Frank et al., 1999#29.
#282#29 a. Does John seem to sleep?
b. Does John seem to be likely to
sleep?
The derivation structure of this analysis does
not match the dependencies, however|seem
adjoins into to sleep.
DSG can derive this sentence with a deriva-
tion matching the dependencies, but it loses
some of the advantage of TAG in that, for
example, cases of super-raising #28where the
verb is raised out of two clauses#29 must be ex-
plicitly ruled out by subsertion-insertion con-
straints. Frank et al. #281999#29 and Kulick #282000#29
give analyses of raising which assign the de-
sired derivation structures without running
into this problem. It turns out that the anal-
ysis of raising from the previous section, de-
signed for a translation problem, has both
of these properties as well. The grammar is
shown back in Figure 4.
4 A Parser
Figure 8 shows a CKY-style parser for our
restrictionof MCTAG as a system of inference
rules. It is limited to grammars whose trees
are at most binary-branching.
The parser consists of rules over items of
one of the following forms, where w
1
#01#01#01w
n
is
the input; #11, #11
h
, and #11
l
specify nodes of the
grammar; i, j, k, and l are integers between 0
and n inclusive; and code is either + or ,:
#0F #5B#11;code;i;,;,;l;,;,#5D and
#5B#11;code;i;j;k;l;,;,#5D function as in
a CKY-style parser for standard TAG
#28Vijay-Shanker, 1987#29: the subtree
rooted by #11 2 T derives a tree whose
fringe is w
i
#01#01#01w
l
if T is initial, or
w
i
#01#01#01w
j
Fw
k
#01#01#01w
l
if T is the lower
auxiliary tree of a set and F is the label
of its foot node. In all four item forms,
code = + i#0B adjunction has taken place
at #11.
#0F #5B#11;code;i;j;k;l;,;#11
l
#5D speci#0Ces that the
segment h#11;#11
l
i derives a tree whose
fringe is w
i
#01#01#01w
j
Lw
k
#01#01#01w
l
, where L is
the label of #11
l
.Intuitively, it means that
a potential site-segment has been recog-
nized.
#0F #5B#11;code;i;j;k;l;#11
h
;#11
l
#5D speci#0Ces, if #11 be-
longs to the upper tree of a set, that
the subtree rooted by #11, the segment
h#11
h
;#11
l
i, and the lower tree concatenated
together derive a tree whose fringe is
w
i
#01#01#01w
j
Fw
k
#01#01#01w
l
, where F is the la-
bel of the lower foot node. Intuitively,it
means that a tree set has been partially
recognized, with a site-segment inserted
between the two components.
The rules which require di#0Ber from a TAG
parser and hence explanation are Pseudopod,
Push, Pop, and Pop-push. Pseudopod applies
to any potential lower adjunction site and is
so called because the parser essentially views
every potential site-segment as an auxiliary
tree #28see Section 2.3#29, and the Pseudopod ax-
iom recognizes the feet of these false auxiliary
trees.
The Push rule performs the adjunction of
one of these false auxiliary trees|that is, it
places a site-segmentbetween the two trees of
an elementary tree set. It is so called because
the site-segmentissaved in a #5Cstack" so that
the rest of its elementary tree can be recog-
nized later. Of course, in our case the #5Cstack"
has at most one element.
The Pop rule does the reverse: every com-
pleted elementary tree set must contain a
site-segment, and the Pop rule places it back
where the site-segment came from, emptying
the #5Cstack." The Pop-push rule performs set-
local adjunction: a completed elementary tree
set is placed between the two trees of yet an-
other elementary tree set, and the #5Cstack" is
unchanged.
Pop-push is computationally the most ex-
pensive rule; since it involves six indices and
three di#0Berent elementary trees, its running
time is O#28n
6
G
3
#29.
It was noted in #28Chiang et al., 2000#29 that
for synchronous RF-2LTAG, parse forests
could not be transferred in time O#28n
6
#29. This
fact turnsout to be connected to several prop-
erties of RF-TAG #28Rogers, 1994#29.
3
3
Thanks to Anoop Sarkar for pointing out the #0Crst
The CKY-style parser for regular form
TAG described in #28Rogers, 1994#29 essentially
keeps track of adjunctions using stacks, and
the regular form constraint ensures that the
stack depth is bounded. The only kinds of ad-
junction that can occur to arbitrary depth are
root and foot adjunction, which are treated
similarly to substitution and do not a#0Bect the
stacks. The reader will note that our parser
works in exactly the same way.
A problem arises if we allow both root
and foot adjunction,however. It iswell-known
that allowing both types of adjunction creates
derivationalambiguity#28Vijay-Shanker, 1987#29:
adjoining #0C
1
at the foot of #0C
2
produces the
same derived tree that adjoining #0C
1
at the
root of #0C
2
would. The problem is not the am-
biguity per se, but that the regular form TAG
parser, unlike a standard TAG parser, does
not always distinguish these multiple deriva-
tions, because root and foot adjunction are
both performed by the same rule #28analogous
to ourPop-push#29.Thusfora given application
of this rule, it is not possible to say which tree
is adjoining into which without examining the
rest of the derivation.
But this knowledge is necessary to per-
form certain tasks online: for example, enforc-
ing adjoining constraints, computing proba-
bilities #28and pruning based on them#29, or per-
forming synchronous mappings. Therefore we
arbitrarily forbid one of the two possibilities.
4
The parser given in Section 4 already takes
this into account.
5 Discussion
Our version of MCTAG follows other
work in incorporating dependency into a
constituency-based approach to modeling
natural language. One such early integra-
tion involved work by Gaifman #281965#29, which
showed that projective dependency grammars
could be represented by CFGs. However, it
is known that there are common phenom-
ena which require non-projective dependency
grammars, so looking only at projective de-
such connection.
4
Against tradition, we forbid root adjunction, be-
cause adjunction at the foot ensures that a bottom-up
traversal of the derived tree will encounter elementary
trees in the same order as they appear in a bottom-up
traversal of the derivation tree, simplifying the calcu-
lation of derivations.
Goal: #5B#11
r
;,;0;,;,;n;,;,#5D #11
r
an initial root
#28Leaf#29 #5B#11;+;i;,;,;j;,;,#5D #11 a leaf
#28Foot#29 #5B#11;+;i;i;j;j;,;,#5D #11 alower foot
#28Pseudopod#29 #5B#11;+;i;i;j;j;,;#11#5D
#28Unary#29
#5B#11
1
;+;i;p;q;j;#11
h
;#11
l
#5D
#5B#11;,;i;p;q;j;#11
h
;#11
l
#5D
#11
#11
1
#28Binary 1#29
#5B#11
1
;+;i;p;q;j;#11
h
;#11
l
#5D #5B#11
2
;+;j;,;,;k;,;,#5D
#5B#11;,;i;p;q;k;#11
h
;#11
l
#5D
#11
#11
1
#11
2
#28Binary 2#29
#5B#11
1
;+;i;,;,;j;,;,#5D #5B#11
2
;+;j;p;q;k;#11
h
;#11
l
#5D
#5B#11;,;i;p;q;k;#11
h
;#11
l
#5D
#11
#11
1
#11
2
#28No adjunction#29
#5B#11;,;i;p;q;j;#11
h
;#11
l
#5D
#5B#11;+;i;p;q;j;#11
h
;#11
l
#5D
#28Push#29
#5B#11
1
;+;j;p;q;k;,;,#5D #5B#11
h
;,;i;j;k;l;,;#11
l
#5D
#5B#11;+;i;p;q;l;#11
h
;#11
l
#5D
#11
.
.
.
#11
1
#28i.e. #11 is an upper foot
and #11
1
isalower root#29
#28Pop#29
#5B#11
l
;,;j;p;q;k;#11
h
0
;#11
l
0
#5D #5B#11
r
;+;i;j;k;l;#11
h
;#11
l
#5D
#5B#11
h
;+;i;p;q;l;#11
h
0
;#11
l
0
#5D
#11
r
a root of an upper tree
adjoinable at h#11
h
;#11
l
i
#28Pop-push#29
#5B#11
1
;+;j;p;q;k;,;,#5D #5B#11
r
;+;i;j;k;l;#11
h
;#11
l
#5D
#5B#11;+;i;p;q;l;#11
h
;#11
l
#5D
#11
.
.
.
#11
1
, #11
r
arootofanupper
tree adjoinable at
h#11;#11
1
i
Figure 8: Parser
pendency grammars is inadequate. Follow-
ing the observation of TAG derivations' sim-
ilarity to dependency relations, other for-
malisms have also looked at relating depen-
dency and constituency approaches to gram-
mar formalisms.
A more recent instance is D-Tree Substi-
tution Grammars #28DSG#29 #28Rambow et al.,
1995#29, where the derivations are also inter-
preted as dependency relations. Thought of
in the terms of this paper, there is a clear
parallel with R-MCTAG, with a local set
ultimately representing dependencies having
some yield function applied to it; the idea
of non-immediate dominance also appears in
both formalisms. The di#0Berence between the
two is in the kinds of languages that they are
able to describe: DSG is both less and more
restrictive than R-MCTAG. DSG can gener-
ate the language count-k for some arbitrary
k #28that is, fa
1
n
a
2
n
:::a
k
n
g#29, which makes
it extremely powerful, whereas R-MCTAG
can only generate count-4. However, DSG
cannot generate the copy language #28that is,
fww j w 2 #06
#03
g with #06 some terminal al-
phabet#29, whereas R-MCTAG can; this may
be problematic for a formalism modeling nat-
ural language, given the key role of the copy
language in demonstrating that natural lan-
guage is not context-free #28Shieber, 1985#29. R-
MCTAGisthusa moreconstrainedrelaxation
of the notion of immediate dominance in fa-
vor of non-immediate dominance than is the
case for DSG.
Another formalism of particular interest
here is the Segmented Adjoining Grammar of
#28Kulick, 2000#29. This generalization of TAGis
characterized by an extension of the adjoining
operation, motivated by evidence in scram-
bling, clitic climbing and subject-to-subject
raising. Most interestingly, this extension to
TAG, proposed on empirical grounds, is de-
#0Cned by a composition operation with con-
strained non-immediate dominance links that
looks quite similar to the formalism described
in this paper, which began from formal con-
siderationsand was then appliedto data. This
con#0Duence suggests that the ideas described
here concerning combining dependency and
constituency might be reaching towards some
deeper connection.
6 Conclusion
From a theoretical perspective, extracting
more derivational generative capacity and
thereby integrating dependency and con-
stituency into a common framework is an in-
teresting exercise. It also, however, proves to
be useful in modeling otherwise problematic
constructions, such as subject-auxiliary inver-
sion and bridge and raising verb interleaving.
Moreover, the formalism developed from the-
oretical considerations, presented in this pa-
per, has similar properties to work developed
on empirical grounds, suggesting that this is
worth further exploration.
References
Tilman Becker, Aravind Joshi, and Owen Ram-
bow. 1991. Long distance scrambling and tree
adjoining grammars. In Fifth Conferenceofthe
European Chapter of the Association for Com-
putational Linguistics #28EACL'91#29, pages 21#7B26.
Tilman Becker, Owen Rambow, and Michael Niv.
1992. The derivational generativepower of for-
mal systems, or, Scrambling is beyond LCFRS.
Technical Report IRCS-92-38, Institute for Re-
search in Cognitive Science, UniversityofPenn-
sylvania.
David Chiang, William Schuler, and Mark Dras.
2000. Some Remarks on an Extension of Syn-
chronous TAG. In Proceedings of TAG+5,
Paris, France.
Mark Dras. 1999. A meta-level grammar: re-
de#0Cning synchronous TAG for translation and
paraphrase. In Proceedings of the 37th Annual
Meeting of the Association for Computational
Linguistics #28ACL '99#29.
Robert Frank, Seth Kulick, and K. Vijay-Shanker.
1999. C-command and extraction in tree-
adjoining grammar. Proceedings of the Sixth
Meeting on the Mathematics of Language
#28MOL6#29.
Robert Frank. 1992. Syntactic locality and
tree adjoining grammar: grammatical acquisi-
tion and processing perspectives. Ph.D. the-
sis, Computer Science Department, University
of Pennsylvania.
Haim Gaifman. 1965. Dependency Systems and
Phrase-Structure Systems. Information and
Control, 8:304#7B337.
Gerald Gazdar. 1988. Applicability of indexed
grammars to natural languages. In Uwe Reyle
and Christian Rohrer, editors, Natural Lan-
guage Parsin and Linguistic Theories. D. Reidel
Publishing Company, Dordrecht, Holland.
Aravind Joshi and K. Vijay-Shanker. 1999. Com-
positional Semantics with Lexicalized Tree-
Adjoining Grammar #28LTAG#29: How Much Un-
derspeci#0Ccation is Necessary? In Proceedings of
the 2nd International Workshop on Computa-
tional Semantics.
Aravind K. Joshi, Leon S. Levy, and M. Taka-
hashi. 1975. Tree adjunct grammars. Journal
of computer and system sciences, 10:136#7B163.
Aravind K. Joshi. 1985. Howmuch context sen-
sitivity is necessary for characterizing struc-
tural descriptions: Tree adjoining grammars. In
L. Karttunen D. Dowty and A. Zwicky, editors,
Natural language parsing: Psychological, com-
putational and theoretical perspectives, pages
206#7B250. Cambridge University Press, Cam-
bridge, U.K.
Aravind Joshi. 2000. Relationship between strong
and weak generativepower of formal systems.
In Proceedings of TAG+5, pages107#7B114,Paris,
France.
Seth Kulick. 2000. A uniform account of locality
constraints for clitic climbing and long scram-
bling. In Proceedings of the Penn Linguistics
Colloquium.
Owen Rambow, David Weir, and K. Vijay-
Shanker. 1995. D-tree grammars. In Proceed-
ings of the 33rdAnnual Meeting of the Associa-
tion for Computational Linguistics #28ACL '95#29.
James Rogers. 1994. Capturing CFLs with tree
adjoining grammars. In Proceedings of the 32nd
Annual Meeting of the Association for Compu-
tational Linguistics #28ACL '94#29.
Stuart Shieber. 1985. Evidence against the
context-freeness of natural language. Linguis-
tics and Philosophy, 8:333#7B343.
K. Vijay-Shanker. 1987. A study of tree adjoining
grammars. Ph.D. thesis, Department of Com-
puter and Information Science, University of
Pennsylvania.
David Weir. 1988. Characterizing Mildly
Context-Sensitive Grammar Formalisms.
Ph.D. thesis, Department of Computer and In-
formation Science, UniversityofPennsylvania.
