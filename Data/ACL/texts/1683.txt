Learning Semantic Constraints for
the Automatic Discovery of Part-Whole Relations
Roxana Girju
Human Language Technology
Research Institute,
University of Texas at Dallas
and
Computer Science Department,
Baylor University
girju@ecs.baylor.edu
Adriana Badulescu and Dan Moldovan
Human Language Technology
Research Institute,
University of Texas at Dallas
adriana@hlt.utdallas.edu,
moldovan@utdallas.edu
Abstract
The discovery of semantic relations from text
becomes increasingly important for applica-
tions such as Question Answering, Informa-
tion Extraction, Text Summarization, Text Un-
derstanding, and others. The semantic rela-
tions are detected by checking selectional con-
straints. This paper presents a method and its
results for learning semantic constraints to de-
tect part-whole relations. Twenty constraints
were found. Their validity was tested on a
10,000 sentence corpus, and the targeted part-
whole relations were detected with an accuracy
of 83%.
1 Introduction
1.1 Problem description
An important semantic relation for several NLP applica-
tions is the part-whole relation, or meronymy. Consider
the text:
The car’s mail messenger is busy
at work in the mail car as the
train moves along. Through the
open side door of the car, moving
scenery can be seen. The worker
is alarmed when he hears an unusual
sound. He peeks through the door’s
keyhole leading to the tender and
locomotive cab and sees the two ban-
dits trying to break through the
express car door.
There are six part-whole relations in this text: 1) the
mail car is part of the train, 2) the side door is part of
the car, 3) the keyhole is part of the door, 4) the cab is
part of the locomotive, 5) the door is part of the car, and
6) the car is part of the express train (the last two in the
compound noun express car door).
Understanding part-whole relations allows Question
Answering systems to address questions such as “What
are the components of X?, What is X made of? and others.
Question Answering, Information Extraction and Text
Summarization systems often need to identify relations
between entities as well as synthesize information gath-
ered from multiple documents. More and more knowl-
edge intensive techniques are used to augment statistical
methods when building advanced NLP applications.
This paper provides a method for deriving semantic
constraints necessary to discover part-whole relations.
1.2 Semantics of part-whole relation
There are different ways in which we refer to something
as being a part of something else, and this led many re-
searchers to claim that meronymy is a complex relation
that “should be treated as a collection of relations, not as
a single relation” (Iris et al. , 1988).
Based on linguistic and cognitive considerations about
the way parts contribute to the structure of the wholes,
Winston, Chaffin and Hermann (Winston et al. ,
1987) determined in 1987 six types of part-whole rela-
tions: Component-Integral object (wheel - car), Member-
Collection (soldier - army), Portion-Mass (meter - kilo-
meter), Stuff-Object (alcohol - wine), Feature-Activity
(paying - shopping), and Place-Area (oasis - desert).
The part-whole relations in WordNet are classified into
three basic types: Member-of (e.g., UK IS-MEMBER-OF
NATO), Stuff-of (e.g., carbon IS-STUFF-OF coal), and
all other part-whole relations grouped under the general
name of Part-of (e.g., leg IS-PART-OF table). In this paper
we lump together all the part-whole relation types, but if
necessary, one can train the system separately on each of
the six meronymy types to increase the learning accuracy.
1.3 Previous work
Although part-whole relations were studied by philoso-
phers, logicians, psychologists and linguists, not much
work has been done to automatically identify the
                                                               Edmonton, May-June 2003
                                                                 Main Papers , pp. 1-8
                                                         Proceedings of HLT-NAACL 2003
meronymy relation in text. Hearst (Hearst, 1998) de-
veloped a method for the automatic acquisition of hyper-
nymy relations by identifying a set of frequently used
and unambiguous lexico-syntactic patterns. Then, she
tried applying the same method to meronymy, but with-
out much success, as the patterns detected also expressed
other semantic relations.
In 1999, Berland and Charniak (Charniak, 1999) ap-
plied statistical methods on a very large corpus to find
part-whole relations. Using Hearst’s method, they fo-
cused on a small set of lexico-syntactic patterns that fre-
quently refer to meronymy and a list of 6 seeds represent-
ing whole objects. Their system’s output was an ordered
list of possible parts according to some statistical metrics.
The accuracy obtained for the first 50 parts was 55%.
2 Lexico-syntactic patterns expressing
meronymy
2.1 Variety of meronymy expressions
Since there are many ways in which something can
be part of something else, there is a variety of lexico-
syntactic structures that can express the meronymy se-
mantic relation. Expressions that reflect semantic rela-
tions are either explicit or implicit. The explicit ones are
further broken down into unambiguous and ambiguous.
A. Explicit part-whole constructions
There are unambiguous lexical expressions that always
convey a part-whole relation. For example:
The substance consists of two ingre-
dients.
The cloud was made of dust.
Iceland is a member of NATO.
In these cases the simple detection of the patterns leads
to the discovery of part-whole relations.
On the other hand, there are many ambiguous expres-
sions that are explicit but convey part-whole relations
only in some contexts. These expressions can be detected
only with complex semantic constraints.
Examples are:
The horn is part of the car.
(whereas ‘‘He is part of the game’’ is not
meronymic).
B. Implicit part-whole constructions
In addition to the explicit patterns, there are other
patterns that express part-whole relations implicitly.
Examples are: girl’s mouth, eyes of the
baby, door knob, oxygen-rich water,
high heel shoes.
2.2 An algorithm for finding lexico-syntactic
patterns
In order to identify lexical forms that express part-whole
relations, the following algorithm was used:
Step 1. Pick pairs of WordNet concepts a0a2a1 , a0a4a3 among
which there is a part-whole relation.
We selected 100 pairs of part-whole concepts that were
evenly distributed over all nine WordNet noun hierar-
chies.
Step 2. Extract lexico-syntactic patterns that link the two
selected concepts of each pair by searching a collection
of texts.
For each pair of part-whole concepts determined
above, search a collection of documents and retain only
the sentences containing that pair. We chose two dis-
tinct text collections: SemCor 1.7 and LA Times from
TREC-9. From each collection 10,000 sentences were
selected randomly. We manually inspected these sen-
tences and picked only those in which the pairs referred
to meronymy.
The result of this step is a list of lexico-syntactic ex-
pressions that reflect meronymy. From syntactic point of
view, these patterns can be classified in two major cate-
gories:
a5 Phrase-level patterns, where the part and whole
concepts are included in the same phrase. For exam-
ple, in the pattern “a6a8a7a10a9a11a7a12a7a10a13 ” the noun phrase that
contains the part (X) and the prepositional phrase
that contains the whole (Y) form a noun phrase (NP).
Throughout this paper, X represents the part, and Y
represents the whole.
a5 Sentence-level patterns, where the part-whole rela-
tion is intrasentential. A frequent example is the pat-
tern “a6a14a7 a13 verb a6a14a7 a9 ”.
From the 20,000 SemCor and LA Times sentences,
535 part-whole occurrences were detected. Of these
493 (92.15%) were phrase-level patterns and only
42 sentence-level patterns. There were 54 distinct
meronymic lexico-syntactic patterns, of which 36 phrase-
level patterns and 18 sentence-level patterns. The most
frequent phrase-level patterns were:
“a6a14a7a10a15 of a6a8a7a17a16 ” occurring 173 of 493 times or 35%;
“a6a14a7a10a15 ’s a6a8a7a17a16 ” occurring 71 of 493 times or 14%;
The most frequent sentence-level pattern was
“a6a14a7a10a15 Verb a6a8a7a17a16 ” occurring 18 of 42 times (43%).
These observations are consistent with the results in
(Evens et al. , 1980). Based on these statistics, we
decided to focus in this paper only on the three patterns
above. The problem, however, is that these are some of
the most ambiguous part-whole relation patterns. For ex-
ample, in addition to meronymic relations, the genitives
can express POSSESSION (Mary’s toy), KINSHIP (Mary’s
brother), and many other relations. The same is true
for “a6a8a7 a15 Verb a6a8a7 a16 ” patterns (“Kate has green eyes”
is meronymic, while “Kate has a cat” is POSSESSION).
As it can be seen, the genitives and the have-verb pat-
terns are ambiguous. Thus we need some semantic con-
straints to differentiate the part-whole relations from the
other possible meanings these patterns may have.
3 Learning Semantic Constraints
3.1 Approach
The learning procedure proposed here is supervised, for
the learning algorithm is provided with a set of in-
puts along with the corresponding set of correct outputs.
Based on a set of positive and negative meronymic train-
ing examples provided and annotated by the user, the al-
gorithm creates a decision tree and a set of rules that clas-
sify new data. The rules produce constraints on the noun
constituents of the lexical patterns.
For the discovery of the semantic constraints we used
C4.5 decision tree learning (Quinlan, 1993). The learned
function is represented by a decision tree, or a set of if-
then rules. The decision tree learning searches a complete
hypothesis space from simple to complex hypotheses un-
til it finds a hypothesis consistent with the data. Its bias
is a preference for the shorter tree that places high in-
formation gain attributes closer to the root. The error in
the training examples can be overcome by using different
training and a test corpora, or by cross-validation tech-
niques.
C4.5 receives in general two input files, the NAMES
file defining the names of the attributes, attribute values
and classes, and the DATA file containing the examples.
The output of C4.5 consists of two types of files, the
TREE file containing the decision tree and some statis-
tics, and the RULES file containing the rules extracted
from the decision tree and some statistics for training and
test data. This last file also contains a default rule that is
usually used to classify unseen instances when no other
rule applies.
3.2 Preprocessing Part-Whole Lexico-Syntactic
Patterns
Since our constraint learning procedure is based on the
semantic information provided by WordNet, we need to
preprocess the noun phrases (NPs) extracted and identify
the part and the whole. For each NP we keep only the
largest word sequence (from left to right) that is defined
in WordNet as a concept.
For example, from the noun phrase “brown carving
knife” the procedure retains only “carving knife”, as it
is the WordNet concept with the largest number of words
in the noun phrase. For each such concept, we manually
annotate it with its corresponding sense in WordNet, for
example carving knifea0 1 means sense number 1.
3.3 Building the Training Corpus and the Test
Corpus
In order to learn the constraints, we used the SemCor 1.7
and TREC 9 text collections. From the first two sets of
the SemCor collection, 19,000 sentences were selected.
Another 100,000 sentences were extracted from the LA
Times articles of TREC 9. A corpus “A” was thus created
from the selected sentences of each text collection. Each
sentence in this corpus was then parsed using the syntac-
tic parser developed by Charniak (Charniak, 2000).
Focusing only on the sentences containing relations in-
dicated by the three patterns considered, we manually
annotated all the noun phrases in the 53,944 relation-
ships matched by these patterns with their correspond-
ing senses in WordNet (with the exception of those from
SemCor). 6,973 of these relationships were part-whole
relations, while 46,971 were not meronymic relations.
We used for training a corpus of 34,609 positive exam-
ples (6,973 pairs of NPs in a part-whole relation extracted
from the corpus “A” and 27,636 extracted from WordNet
as selected pairs) and 46,971 negative examples (the non-
part-whole relations extracted from corpus “A”).
3.4 Learning Algorithm
Input: positive and negative meronymic examples of
pairs of concepts.
Output: semantic constraints on concepts.
Step 1. Generalize the training examples
Initially, the training corpus consists of examples that
have the following format:
a1 part#sense; whole#sense; target
a2 ,
where target can be either “Yes” or “No”, as the rela-
tion between the part and whole is meronymy or not.
For example, a1 oasisa0 1; deserta0 1; Yesa2 in-
dicates that between oasis and desert there is a
meronymic relation.
From this initial set of examples an intermediate cor-
pus was created by expanding each example using the
following format:
a1 part#sense, class part#sense;
whole#sense, class whole#sense;
targeta2 ,
where class part and class whole correspond to
the WordNet semantic classes of the part, respec-
tively whole concepts. For instance, the initial
example a1 ariaa0 1; operaa0 1; Yesa2 be-
comes a1 ariaa0 1, entitya0 1; operaa0 1,
abstractiona0 6; Yesa2 , as the part concept
ariaa0 1 belongs to the entitya0 1 hierarchy in
WordNet and the whole concept operaa0 1 is part of the
abstractiona0 6 hierarchy.
From this intermediate corpus a generalized set of
training examples was built, retaining only the semantic
classes and the target value. At this point, the generalized
training corpus contains three types of examples:
1. Positive examples
a0 X hierarchy#sense; Y hierarchy#sense;
Yesa1
2. Negative examples
a0 X hierarchy#sense; Y hierarchy#sense; No
a1
3. Ambiguous examples
a0 X hierarchy#sense; Y hierarchy#sense;
Yes/Noa1
The third situation occurs when the training cor-
pus contains both positive and negative examples for
the same hierarchy types. For example, both rela-
tions a1 apartmenta0 1; womana0 1; Noa2 and
a1 hand
a0 1; womana0 1; Yesa2 are mapped into
the more general type a1 entitya0 1; entitya0 1;
Yes/Noa2 . However, the first example is negative (a
POSSESSION relation), while the second one is a positive
example.
Step 2. Learning constraints for unambiguous examples
For the unambiguous examples in the generalized train-
ing corpus (those that are either positive or negative), con-
straints are determined using C4.5. In this context, the
features are the components of the relation (the part and,
respectively the whole) and the values of the features are
their corresponding WordNet semantic classes (the fur-
thest ancestor in WordNet of the corresponding concept).
With the first two types of examples, the unambiguous
ones, a new training corpus was created on which we ap-
plied C4.5 using a 10-fold cross validation. The output
is represented by 10 sets of rules generated from these
unambiguous examples.
The rules in each set were ranked according to their
frequency of occurrence and average accuracy obtained
for that particular set. In order to use the best rules, we
decided to keep only the ones that had a frequency above
a threshold (occur in at least 7 of the 10 sets of rules) and
an average accuracy greater than 50a2 .
Step 3. Specialize the ambiguous examples
A part of the generalized training corpus contains am-
biguous examples. These examples refer to the same se-
mantic classes in WordNet, but their target value is in
some cases “Yes” and in others “No”. Since C4.5 can-
not be applied in this situation, we recursively specialize
these examples to eliminate the ambiguity.
The specialization procedure is based on the IS-A in-
formation provided by WordNet. Initially, each semantic
class represented the root of one of the noun hierarchies
in WordNet. By specialization, a semantic class is re-
placed with its first hyponym, i.e. the concept immedi-
ately below in the hierarchy. For this task, we considered
again the intermediate training corpus of examples.
For instance, the examples a1 apartmenta0 1,
entitya0 1; womana0 1, entitya0 1; Noa2
and a1 handa0 1, entitya0 1; womana0 1,
entitya0 1; Yesa2 that caused the ambiguity
a1 entity
a0a4a3 ; entitya0 1; Yes/Noa2 , were
replaced with a1 apartmenta0 1, wholea0 2;
womana0 1, causal agenta0 1; Noa2 , re-
spectively a1 handa0 1, parta0 7; womana0 1,
causal agenta0 1; Yesa2 . These two intermediate
examples are thus generalized in the less ambiguous
examples a1 wholea0 2; causal agenta0 1; Noa2
and a1 parta0 7; causal agenta0 1; Yesa2 . This
way, we specialize the ambiguous examples with more
specific values for the attributes. The specialization
process for this particular example is shown in Figure 1.
causal_agent#1
apartment#1 hand#1 women#1
entity#1
whole#2 part#7
Figure 1: The specialization of the ambiguous training exam-
ples with the corresponding WordNet hyponyms as new seman-
tic classes
Although this specialization procedure eliminates a
part of the ambiguous examples, there is no guarantee
it will work for all the ambiguous examples of this type.
This is because the specialization splits the initial hier-
archy into smaller distinct subhierarchies, and thus, the
examples are distributed over this new set of subhier-
archies. For the examples described above, the proce-
dure eliminates the ambiguity through specialization of
the semantic classes into two new ones: whole - causal
agent, and respectively part - causal agent. However, if
the training corpus contained the examples a1 lega0 2;
insecta0 1; Yesa2 and a1 worlda0 7; insecta0 1;
Noa2 , the procedure specializes them in the ambigu-
ous example a1 parta0 7; organisma0 1; Yes/Noa2
and the ambiguity still remains.
Steps 2 and 3 are repeated until there are no more
ambiguous examples. The general architecture of this
procedure is shown in Figure 2. Here is an example
file
DATA
Learn
C4.5
using
Format
Specialize
Examples for
Next Iteration
Examples
Training
FormatGeneralize
Examples
Examples
Ambiguous
Examples
Unambiguous
Constraints
Examples
Classes
NAMES file
Detect
Ambiguous
Examples
Examples
Semantic classes
Semantic
Features
New Semantic Classes
New Examples
Figure 2: Architecture of the constraint learning procedure.
of iterations produced by the program to specialize
ambiguous data:
entitya0 1 - entitya0 1
body of watera0 1 - locationa0 1
locationa0 1 - body of watera0 1
locationa0 1 - objecta0 1
regiona0 1 - artifacta0 1
regiona0 3 - artifacta0 1
Each indentation represents a new iteration.
3.5 The Constraints
Table 1 summarizes the constraints learned by the pro-
gram. The meaning of a constraint with the part Class-X,
the whole Class-Y and the value 1 is “if Part is a Class-X
and Whole is a Class-Y then it is a part-whole relation”
and for the value 0 is “if Part is a Class-X and Whole is a
Class-Y then it is not a part-whole relation”. For exam-
ple, “if Part is an entitya0a4a3 and the Whole is a wholea0 2
then it is not a part-whole relation”. (wholea0 2 is the
WordNet concept meaning “an assemblage of parts that
is regarded as a single entity”).
When forming larger, more complex rules, if the part
and the whole contain more then one value, one of these
values is negated (preceded by !). For example for the
part objecta0 1 and the whole organisma0 1 the constraint
is “if the Part is objecta0 1 and not substancea0 1 and not
natural objecta0 1 and the Whole is organisma0 1 and not
planta0 2 and not animala0 1 then NO part-whole rela-
tion”.
4 Results for discovering part-whole
relations
To validate the constraints for extracting part-whole re-
lations, a new test corpus “B” was created from other
10,000 sentences of TREC-9 LA Times news articles.
This corpus was parsed and disambiguated using a Word
Sense Disambiguation system that has an accuracy of
81a2 when disambiguating nouns in open-domain (Mi-
halcea and Moldovan, 2001). The results provided by the
part-whole relation discovery procedure were validated
by a human annotator.
Let us define the precision and recall performance met-
rics in this context:
a0a2a1a4a3a6a5a8a7a10a9a11a7a13a12a4a14a16a15
a17a16a18a2a19a16a20
a3a6a1a21a12a23a22a24a5a25a12a4a1a26a1a27a3a6a5a29a28a31a30a33a32a34a1a4a3a11a28a35a1a26a7a36a3a38a37a39a3a6a40a41a1a27a3a38a30a33a42a43a28a44a7a36a12a26a14a45a9
a17a16a18a46a19a16a20
a3a6a1a21a12a27a22a24a1a27a3a38a30a33a42a43a28a44a7a36a12a26a14a45a9a47a1a4a3a6a28a44a1a4a7a36a3a6a37a39a3a6a40
a1a4a3a6a5a8a42a48a30a49a30a50a15
a17a16a18a2a19a51a20
a3a6a1a52a12a23a22a53a5a25a12a4a1a26a1a4a3a38a5a29a28a31a30a33a32a54a1a27a3a11a28a35a1a26a7a36a3a6a37a39a3a38a40a41a1a4a3a26a30a55a42a39a28a44a7a13a12a4a14a45a9
a17a16a18a2a19a51a20
a3a6a1a52a12a23a22a56a5a8a12a26a1a4a1a4a3a6a5a25a28a57a1a27a3a38a30a33a42a43a28a44a7a36a12a26a14a45a9
On the test corpus there were 119 meronymy relations
expressed by the three patterns considered. The system
retrieved 140 relations, of which 117 were meronymy re-
lations and 23 were non-meronymy relations, yielding a
precision of 83% and a recall of 98%. Table 2 shows the
results obtained for each of the three patterns and for all
of them.
However, there were other 43 manner relations found
in the corpus, expressed by other than the three lexico-
syntactic patterns considered in this paper, yielding a
global meronymy relation coverage (recall) of 72a2 .
[117/119+43]
The errors are explained mostly by the fact that
the genitives and the verb have are very ambiguous.
These lexico-syntactic patterns encode numerous rela-
tions which are very difficult to disambiguate based only
on the nouns they connect. The errors were also caused
by the incorrect parsing of a few s-genitives, the use of the
rules with smaller accuracy (e.g. 50%), the wrong word
sense disambiguation of some concepts, and the lack of
named entity recognition in WordNet (e.g., proper names
of persons, places, etc.).
Nr Class-X Class-Y Value Accuracy Frequency Example
1 objecta0 1 social eventa0 1 1 69.84 9 scenea0 4 - moviea0 1
2 wholea0 2 social eventa0 1 1 63.00 7 sequencea0 3 - moviea0 1
3 entitya0 1 groupa0 1 1 academiciana0 1 - academya0 2
kinga0 1 - royaltya0 1
4 locationa0 1 peoplea0 1 0 50.00 8 sectiona0 3 - nationa0 1
5 organisma0 1 systema0 1 0 50.00 8 archbishopa0 1 - Yorka0 1
6 groupa0 1 groupa0 1 1 military reservea0 1 - military unita0 1
amoebidaa0 1 - genus amoebaa0 1
7 collectiona0 1 arrangementa0 2 0 92.60 10 dataa0 1 - tablea0 1
8 arrangementa0 2 social groupa0 1 0 85.70 10 hierarchya0 1 - churcha0 1
9 systema0 1 collectiona0 1 0 85.70 10 economya0 1 - selectiona0 2
10 entitya0 1 entitya0 1 1 doora0 4 - cara0 4
pointa0 15 - knifea0 1
11 pointa0 2 objecta0 1 0 89.55 10 placea0a2a1 - walla0 2
12 locationa0 1 objecta0 1 1 basea0 16 - boxa0 1
13 geographic areaa0 1 instrumentalitya0 3 0 80.75 8 graveyarda0 1 - shipa0 1
14 persona0 1 persona0 1 0 89.55 10 childa0 1 - womana0 1
15 objecta0 1 organisma0 1 0 deska0 1 - mana0 1 - No
!substancea0 1 !planta0 2 feathera0 1 - birda0 1 - Yes
!natural objecta0 1 !animala0 1 bladea0 1 - grassa0 1 - Yes
bodya0 1 - mana0 1 - Yes
16 organisma0 1 objecta0 1 0 authora0 1 - booka0 1 - No
!planta0 2 !landa0 3 Romaniana0 1 - Romaniaa0 1 - Yes
17 entitya0 1 wholea0 2 0 98.82 10 authora0 1 - booka0 1
attorneya0 1 - hotela0 1
18 entitya0 1 locationa0 1 0 98.16 10 ambassadora0 1 - Romaniaa0 1
conquerora0 1 - Denmarka0 1
19 entitya0 1 thinga0 12 0 97.46 10 cupa0 3 - somethinga0 1
20 entitya0 1 body of watera0 1 0 91.76 10 commissionera0 1 - pacifica0 1
Table 1: The list of all the constrains accompanied by examples (! means “is not”, 1 means “Is a part-whole relation,”
and 0 means “Is not a part-whole relation” )
5 Application to Question Answering
The part-whole semantic relation occurs with high fre-
quency in open text. Its discovery is paramount for many
applications. In this section we mention only Question
Answering. For many questions such as “What parts does
General Electric manufacture?”, “What are the compo-
nents of X?”, “What is Y made of?”, etc., the discovery
of part-whole relations is necessary to assemble the right
answer.
The concepts and part-whole relations acquired from
a collection of documents can be useful in answering
difficult questions that normally cannot be handled based
solely on keywords matching and proximity. As the level
of difficulty increases, Question Answering systems
need richer semantic resources, including ontologies and
larger knowledge bases. Consider the question:
What does the AH-64A Apache helicopter consist of?
For questions like this, the system must extract all the
components the war helicopter has. Unless an ontology
of such army attack helicopter parts exists in the knowl-
edge base, which in an open domain situation is highly
unlikely, the system must first acquire from the docu-
ment collection all the direct and indirect pieces the he-
licopter is made of. These parts can be scattered all over
the text collection, so the Question Answering system has
to gather together these partial answers into a single and
concise hierarchy of parts. This technique is called An-
swer Fusion.
Using a state-of-the-art Question Answering system
(Harabagiu et al. , 2001) adapted for Answer Fusion
(Girju, 2001) and including a meronymy module, the
question presented above was answered by searching the
Internet at the website for Defence Industries - Army
(www.army-technology.com). The system started with the
question focus AH-64A Apache helicopter and extracted
and disambiguated all the meronymy relations using the
part-whole module. The following taxonomic ontology
was created for this question:
AH-64A Apache Helicopter
Helfire air-to-surface missile
millimetre wave seeker
70mm Folding Fin Aerial rocket
30mm Cannon camera
armaments
Number of Relations Y verb X Y’s X X of Y All patterns
Number of patterns 280 225 962 1467
Number of correct 18 23 78 119
relations
Number of relations 25 24 91 140
retrieved
Number of correctly 18 22 77 117
retrieved relations
Precision 72a2 91.16a2 84.61a2 83.57a2
Recall 100a2 95.65a2 98.71a2 98.31a2
Table 2: The number relations obtained and the accuracy for each pattern and for all the patterns used for this research
General Electric 1700-GE engine
4-rail launchers
four-bladed main rotor
anti-tank laser guided missile
Longbow millimetre wave fire control radar
integrated radar frequency interferometer
rotating turret
tandem cockpit
Kevlar seats
For example, the relation “AH-64A Apache helicopter
has part Hellfire air-to-surface missile” was determined
from the sentence “ AH-64A Apache helicopter has a
Longbow-millimeter wave fire control radar and a Hell-
fire air-to-surface missile”. For validation only the heads
of the noun phrases were considered as they occur in
WordNet (i.e., helicopter and air-to-surface missile, re-
spectively).
6 Conclusions
The method presented in this paper for the detection
and validation of part-whole relation is semi-automatic
and has a better accuracy than the previous attempts
(Charniak, 1999). It discovers semi-automatically the
part-whole lexico-syntactic patterns and learns (automat-
ically) the semantic constraints needed for the disam-
biguation of these generally applicable patterns.
We combined the results of the decision tree learning
with an IS-A hierarchy (the WordNet IS-A relation) spe-
cialization for a more accurate learning.
The method presented in this paper can be generalized
to discover other semantic relations. The only part-whole
elements used in this algorithm were the patterns and the
training examples. Thus the learning procedure and the
validation procedure are generally applicable and we in-
tend to use the method for the detection of other semantic
relations such as manner, influence, and others. The in-
convenience of the method is that for a very precise learn-
ing the number of examples (both positive and negative)
should be very large.
We also intend to automate the detection of lexico-
syntactic patterns and to discover constraints for all the
part-whole patterns.
References
Matthew Berland and Eugene Charniak. 1999. Finding
Parts in Very Large Corpora. In Proceedings of the
37th Annual Meeting of the Association for the Com-
putational Linguistics (ACL-99), pages 57-64, College
Park, MD.
Eugene Charniak. 2000. A maximum-entropy-inspired
parser. In Proceedings of the North American Chap-
ter of the Association for Computational Linguistics
(NAACL 2000), Seattle, WA.
M. Evens, B. Litowitz, J. Markowitz, R. Smith, and O.
Werner. 1980. Lexical-semantic relations: A Com-
parative Survey. In Proceedings of the Linguistic Re-
search, pages 187-190, Edmonton, Canada.
Peter Gerstl and Simone Pribbenow. 1996. A concep-
tual theory of Part-Whole Relations and Its Applica-
tions. In Data and Knowledge Engineering, 20(3),
pages 305-322.
Roxana Girju. 2001. Answer Fusion with On-Line
Ontology Development. In Proceedings of the North
American Chapter of the Association for Computa-
tional Linguistics (NAACL) - Student Research Work-
shop, (NAACL 2001), Pittsburgh, PA.
Marti Hearst. 1998. Automated Discovery of Word-
Net Relations. In An Electronic Lexical Database and
Some of its Applications, MIT Press, Cambridge, MA.
Madelyn Iris, Bonnie Litowitz and Martha Evens. 1988.
Problems of the Part-Whole Relation. In Relational
Models of the Lexicon: Representing Knowledge in Se-
mantic Networks, Cambridge University Press, Evens
(ed.), pages 261-288.
Rada Mihalcea and Dan Moldovan. 2001. A Highly Ac-
curate Bootstrapping Algorithm for Word Sense Dis-
ambiguation. In the International Journal on Artificial
Intelligence Tools, vol.10, no.1-2, pages 5-21.
S. Harabagiu, D. Moldovan, M. Pasca, M. Surdeanu, R.
Mihalcea, R. Girju, V. Rus, F. Lacatusu, P. Moraescu,
and R. Bunescu. 2001. Answering Complex, List and
Context Questions with LCC’s Question-Answering
Server. In Proceedings of the TExt Retrieval Confer-
ence for Question Answering (TREC 10).
J. Ross Quinlan. 1993. C4.5: Programs for Machine
Learning. Morgan Kaufmann.
Morton Winston, Roger Chaffin, and Douglas Herrmann.
1987. A Taxonomy of Part-Whole Relations. In Cog-
nitive Science, vol. 11, pages 417-444.
