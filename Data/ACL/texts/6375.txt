References

Laura Alonso and Jennafer Shih and Irene Castellon and Llluis Padro. 2003. An Analytic account of discourse markers for shallow NLP. ESSLLI workshop The MEaning and Implementation of Discourse Particles.

Laura Alonso and Daniel Alonso and Ezequiel Andujar and Robert Sola. 2004. Anotacion discursiva de corpus en espanol. Tech Report GRIAL series.

Nicholas Asher and Laure Vieu. 2001. Subordinating and coordinating discourse relations. International workshop on Semantics, pragmatics, and rhetorics.

Jean Carletta. 1996. Assessing agreement on classification tasks: the kappa statistic. Computational Linguistics. 

Lynn Carlson and Daniel Marcu. 2001. Discourse tagging manual. Tech Report ISI-TR-545.

Lynn Carlson and Daniel Marcu and Mary Ellen Okurowski. 2003. Building a discourse-tagged corpus in the framework of rhetorical structure theory. In Kuppevelt and Smith, eds., Current Directions in Discourse and Dialogue. Kluwer.

Robin Cooper and Staffan Larsson and Colin Matheson and Massimo Poesio and David Traum. 1999. Coding instructional dialogue for information states. Tech Report D1.1.

Discourse Resource Initiative. 1997. Standards for dialogue coding in natural language processing. Tech Report 167.

Barbara J. Grosz and Candace L. Sidner. 1986. Attention, intention, and the structure of discourse. Computational Linguistics. 

Michael A. K. Halliday and Ruqaiya Hasan. 1976. Cohesion in English. Longman.

Andrew Kehler. 2002. Coherence, reference, and the theory of grammar. CSLI.

Alistair Knott. 1996. A data-driven mthodology for motivating a set of coherence relations. PhD thesis, Univ. of Edinburgh.

Luuk Lagerwerf. 1998. Causal connectives have presuppositions: effects on coherence and discourse structure. Den Haag.

William C. Mann and Sandra A. Thompson. 1988. Rhetorical structure theory: toward a functional theory of text organisation. Text.

Johanna D. Moore and MArtha E. Pollack. 1992. A problem for RST: the need for multi-level discourse analysis. Computational Linguistics.

Livia Polanyi and Chris Culy and Martin van den Berg and Gian Lorenzo Thione and David Ahn. 2004. A rule based approach to discourse parsing. SIGDIAL 2004.

Livia Polanyi. 1988. A formal model of the structure of discourse. Journal of Pragmatics.

Ted J. M. Sanders and Wilbert P. M. Spooren and Leo G. M. Noordman. 1992. Toward a taxonomy of coherence relations. Discourse Processes.

E. Sweetser. 1990. From etymology to pragmatics. Metaphorical and cultural aspects of semantic structure. Cambridge Univ. Press.

Carla Umbach. 2004. Contrast and information structure: a focus-based analysis of 'but'. Journal of Semantics.

Arie Verhagen. 2001. Subordination and discourse segmentation revisited, or why matrix clauses may be more dependent than complements. In Sanders, Schilperoord, and Spooren, eds., Text representation. Linguistic and psychological aspects.

Bonnie Lynn Webber and Matthew Stone and Aravind K. Joshi and Alistair Knott. 2003. Anaphora and discourse structure. Computational Linguistics.

Bonnie Lynn Webber. 1978. A formal approach to discourse anaphora. PhD theses, Harvard.
