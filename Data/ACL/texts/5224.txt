Proceedings of the 8th International Workshop on Tree Adjoining Grammar and Related Formalisms, pages 141–146,
Sydney, July 2006. c©2006 Association for Computational Linguistics
Parsing TAG with Abstract Categorial Grammar
Sylvain Salvati
National Institute of Informatics
2-1-2 Hitotsubashi, Chiyoda-ku,
Tokyo 101-8430, JAPAN
salvati@nii.ac.jp
Abstract
This paper presents informally an Earley
algorithm for TAG which behaves as the
algorithm given by (Schabes and Joshi,
1988). This algorithm is a specialization
to TAG of a more general algorithm ded-
icated to second order ACGs. As second
order ACGs allows to encode Linear Con-
text Free Rewriting Systems (LCFRS) (de
Groote and Pogodalla, 2004), the presen-
tation of this algorithm gives a rough pre-
sentation of the formal tools which can
be used to design efficient algorithms for
LCFRS. Furthermore, as these tools allow
to parse linear λ-terms, they can be used
as a basis for developping algorithms for
generation.
1 Introduction
The algorithm we present is a specialization to
TAGs of a more general one dedicated to second
order Abstract Categorial Grammars (ACGs) (de
Groote, 2001). Our aim is to give here an informal
presentation of tools that can be used to design ef-
ficient parsing algorithms for formalisms more ex-
pressive than TAG. Therefore, we only give a rep-
resentation of TAGs with linear λ-terms together
with simple derivation rules; we do not give in
complete details the technical relation with ACGs.
For some more information about ACGs and their
relation to TAGs, one may read (de Groote, 2001)
and (de Groote, 2002).
The advantage of using ACG is that they are
defined with very few primitives, but can encode
many formalisms. Thus they are well suited to
study from a general perspective a full class of for-
malisms. In particular, a special class of ACGs
(second order ACGs) embeds LCFRS (de Groote
and Pogodalla, 2004), i.e. mildly context sensi-
tive languages. Therefore, the study of second
order ACGs leads to insights on mildly context
sensitive languages. Having a general framework
to describe parsing algorithms for mildly context
sensitive languages may give some help to trans-
fer some interesting parsing technique from one
formalism to another. It can be, for example, a
good mean to obtain prefix-valid algorithms, LC
algorithms, LR algorithms. . . for the full class of
mildly context sensitive languages.
The class of languages described by second or-
der ACGs is wider than mildly context sensitive
languages. They can encode tree languages, and
more generally languages of linear λ-terms. As
Montague style semantics (Montague, 1974) is
based on λ-calculus, being able to parse linear λ-
term is a first step towards generation algorithms
seen as parsing algorithm. Furthermore, since this
parsing algorithm is a generalization of algorithms
`a la Earley for CFGs and TAGs, the more general
algorithm that can be used for generation (when
semantic formulae are linear) can be considered
as efficient.
The paper is organized as follows: section two
gives basic defintions and tools concerning the lin-
ear λ-calculus. Section three explains how the in-
dices usually used by parsers are represented for
the linear λ-calculus. Section four gives a rough
explaination of the encoding of TAGs within a
compiled representation of second order ACGs.
Section five explains the parsing algorithm and we
conclude with section six.
2 The linear λ-calculus
We begin by giving a brief definition of linear
types and linear λ-terms together with some stan-
141
dard notations. We assume that the reader is famil-
iar with the usual notions related to λ-calculus (β-
conversion, free variables, capture-avoiding sub-
stitutions. . . ); for more details about λ-calculus,
one may consult (Barendregt, 1984).
Definition 1 The set of linear types, T , is the
smallest set containing {∗} and such that if α,β ∈
T then (αmultimapβ) ∈ T .
Given a type (α1 multimap (···(αn multimap ∗)···)), we
write it (α1,...,αn)multimap∗.
Definition 2 Given a infinite enumerable set of
variables, X, and an alphabet Σ, we define the
set of linear λ-terms of type α ∈ T , Λα, as the
smallest set satisfying the following properties:
1. x ∈ X ⇒ xα ∈ Λα
2. t ∈ Λα ∧xβ ∈ FV (t) ⇒ λxβ.t ∈ Λβmultimapα
3. a ∈ Σ ⇒ a ∈ Λ∗multimap∗
4. t1 ∈ Λβmultimapα∧t2 ∈ Λβ∧FV (t1)∩FV (t2) ⇒
(t1t2) ∈ Λα
In general, we write λx1 ...xn.t for
λx1....λxn.t and we write t0t1 ...tn for
(...(t0t1)...tn). Strings are represented by
closed linear λ-terms of type str = ∗ multimap ∗.
Given a string abcde, it is represented by the
following linear λ-term: λy∗.a(b(c(d(ey∗))));
/w/ represents the set of terms which are
β-convertible to the λ-term representing the
string w. Concatenation is represented by
+ = λxstr1 xstr2 y∗.xstr1 (xstr2 y∗), and (+w1)w2
will be written w1 + w2. The concatenation
is moreover associative, we may thus write
w1 +··· + wn.
For the description of our algorithm, we rely on
contexts:
Definition 3 A context is a λ-term with a hole.
Contexts are defined by the following grammar:
C = [] | ΛC|CΛ | λV.C
The insertion of a term within a context is done
the obvious way. One has nevertheless to remark
that when a term t is inserted in a context C[], the
context C[] can bind variables free in t. For exam-
ple, if C[] = λx.[] and t = x then C[t] = λx.x
and x which was free in t is not free anymore in
C[t].
3 Indices as syntactic descriptions
Usually the items of Earley algorithms use indices
to represent positions in the input string. The algo-
rithm we describe is a particular instance of a more
general one which parses linear λ-terms rather
than strings. In that case, one cannot describe in a
simple way positions by means of indices. Instead
of indices, positions in a term t will be represented
with zippers ((Huet, 1997)), i.e. a pair (C[],v) of
a context and a term such that C[v] = t. Figure 1
explicits the correspondence between indices and
zippers via an example.
The items of Earley algorithms for TAGs use
pairs of indices to describe portions of the input
string. In our algorithm, this role is played by lin-
ear types built upon zippers; the parsing process
can be seen as a type-checking process in a par-
ticular type system. We will not present this sys-
tem here, but we will give a flavor of the mean-
ing of those types called syntactic descriptions
(Salvati, 2006). In order to represent the portion
of a string between the indices i and j, we use
the zippers (Ci[],vi) and (Cj[],vj) which respec-
tively represent the position i and j in the string.
The portion of string is represented by the syntac-
tic description (Cj[],vj) multimap (Ci[],vi); this syn-
tactic description can be used to type functions
which take vj as argument and return vi as a re-
sult. For example, given the syntactic description:
(λx.a(b(c[])),d(ex)) multimap (λx.a[],b(c(d(ex)))),
it represents the set of functions that result in
terms that are β-convertible to b(c(d(ex))) when
they take d(ex) as an argument; this set is
exactly /bc/. Our algorithm uses representa-
tions of string contexts with syntactic descrip-
tions such as d = ((C1[],v1) multimap (C2[],v2)) multimap
(C3[],v3)multimap(C4[],v4) (in the following we write
((C1[],v1)multimap(C2[],v2),(C3[],v3))multimap(C4[],v4)
for such syntactic descriptions). Assume that
(C1[],v1) multimap (C2[],v2) represents /bc/ and that
(C3[],v3) multimap (C4[],v4) represents /abcde/, then
d describes the terms which give a result in
/abcde/ when they are applied to an element
of /bc/. Thus, d describes the set of terms β-
convertible to λfy.a(f(d(ey))), the set of terms
representing the string context a[ ]de.
Some of the syntactic descriptions we use may
contain variables denoting non-specified syntactic
descriptions that may be instanciated during pars-
ing. In particular, the syntactic description vari-
able F will always be used as a non-specified syn-
142
0 (λx.[],a(b(c(d(ex))))) 1 (λx.a[],b(c(d(ex))))
2 (λx.a(b[]),c(d(ex))) 3 (λx.a(b(c[])),d(ex))
4 (λx.a(b(c(d[]),ex) 5 (λx.a(b(c(d(e[])))),x)
abcde
Figure 1: Correspondence indices/zippers for the string abcde
tactic description representing strings (i.e. F may
only be substituted by a syntactic description of
the form (C1[],v1) multimap (C2[],v2)), such syntac-
tic descriptions will represent the foot of an auxil-
iary tree. We will also use Y to represent a non-
specifed point in the input sentence (i.e. Y may
only be substituted by syntactic descriptions of
the form (C[],v)), such syntactic descriptions will
represent the end of an elementary tree.
As syntactic desccriptions are types for the lin-
ear λ-calculus, we introduce the notion of typing
context for syntactic descriptions.
Definition 4 A typing context Γ (context for
short), is a set of pairs of the form x : d where x
is a variable and d is a syntactic description such
that x : d ∈ Γ and x : e ∈ Γ iff d = e.
If x : d ∈ Γ, then we say that x is declared with
type d in Γ.
Typing contexts Γ must not be confused with
contexts C[]. If a typing context Γ is the set
{x1 : d1;...;xn : dn} then we will write if by
x1 : d1,...,xn : dn. In the present paper, typing
contexts may declare at most two variables.
4 Representing TAG with second order
ACGs
We cannot give here a detailed definition of second
order ACGs here. We therefore directly explain
how to transform TAGs into lexical entries repre-
senting a second order ACG that can be directly
used by the algorithm.
We represent a TAG G by a set of lexical en-
tries LG. Lexical entries are triples (Γ,t,α) where
Γ is a typing context, t is a linear λ-term and α
is either Na, Ns or Na.1 if N is a non-terminal
of the considered TAG. Without loss of general-
ity, we consider that the adjunction at an interior
node of an elementary tree is either mandatory
or forbidden1. We adopt the convention of rep-
1We do not treat here the case of optional adjunction, but
our method can be straightforwardly extended to cope with
it, following ideas from (de Groote, 2002). It only modifies
the way we encode a TAG with a set of lexical entries, the
algorithm remains unchanged.
resenting adjunction nodes labeled with N by the
variable xstrmultimapstrNa , the substitution nodes labeled
with N ↓ by the variable xstrNs, the foot node of
an auxiliary tree labeled with N∗ by the variable
fstrNa.1 and the variable y∗ will represent the end
of strings. When necessary, in order to respect
the linearity constraints of the λ-terms, indices are
used to distinguish those variables. This conven-
tion being settled, the type annotation on variables
is not necessary anymore, thus we will write xNa,
xNs, fNa.1 and y. To translate the TAG, we use
the function φ defined by figure 2. Given an initial
tree T whose root is labeled by N and t the normal
form of φ(T), ( ,t,Ns)2 is the lexical entry asso-
ciated to T; if T is an auxiliary tree whose root
is labeled by N and t is the normal form of φ(T)
then ( ,λfNa.1.t,Na)2 is the lexical entry associ-
ated to T. A TAG G is represented by LG the
smallest set verifying:
1. if T is an elementary tree of G then the lexi-
cal entry associated to T is in LG.
2. if ( ,t,α) ∈ LG, with α equals to Na or Ns,
and t = C[xNat1t2] then (Γ,t1,Na.1) ∈ LG
where Γ = fMa.1 : F if fMa.1 ∈ FV (t1)
otherwise Γ is the empty typing context.
Given a term t such that xα ∈ FV (t), and
(Γ,tprime,α) ∈ LG, then we say that t is rewritten
as t[xα := tprime], t ⇒ t[xα := tprime]. Furthermore if xα
is the leftmost variable we write t ⇒l t[xα := tprime].
It is easy to check that if t ∗⇒ tprime with FV (tprime) = ∅,
then t ∗⇒l tprime. A string w is generated by a LG
whenever xSs ∗⇒ t and t ∈ /w/ (S being the start
symbol of G). Straightforwardly, the set of strings
generated by LG is exactly the language of G.
5 The algorithm
As we want to emphasize the fact that the algo-
rithm we propose borrows much to type checking,
we use sequents in the items the algorithm manip-
ulates. Sequents are objects of the form Γ turnstileleft t : d
2In that case the typing context is empty.
143
φ



N
T1 Tn. . .


−→ λy.xNa(φ(T1) +··· + φ(Tn))y xNa and y are fresh
φ



NNA
T1 Tn. . .


−→ φ(T1) +··· + φ(Tn)
φ(N∗) −→ λy.xNa(λy.fN.1y)y
φ(N∗NA) −→ λy.fN.1y
φ(N ↓) −→ λy.xNsy
φ(a) −→ λy.ay
φ(epsilon1) −→ λy.y
Figure 2: Translating TAG into ACG: definition of φ
where Γ is a typing context, t is a linear λ-term,
and d is a syntactic description.
The algorithm uses two kinds of items; either
items of the form (α;Γ turnstileleft t : d;L) (where L is
a list of sequents, the subgoals, here L contains
either zero or one element) or items of the form
[Na.1;Γ;t;(C1[],v1) multimap (C2[],v2)]. All the pos-
sible instances of the items are given by figure 3.
The algorithm is a recognizer but can easily be ex-
tended into a parser3. It fills iteratively a chart until
a fixed-point is reached. Elements are added to the
chart by means of inference rules given by figure
4, in a deductive parsing fashion (Shieber et al.,
1995). Inference rules contain two parts: the first
part is a set of premises which state conditions on
elements that are already in the chart. The second
part gives the new element to add to the chart if
it is not already present. For the more general al-
gorithm, the rules are not much more numerous as
they can be abstracted into more general schemes.
An item of the form (α;Γ1 turnstileleft t1 : d;Γ2 turnstileleft t2 :
(C1[],v1)) verifies:
1. (Γprime1,t1,α) ∈ LG where Γprime1 = fNa.1 : F if
Γ1 = fNa.1 : e or Γprime1 = Γ1 otherwise.
2. there is a context C[] such that t1 = C[t2] and
if d is of the form (d1,...,dn)multimap(C2[],v2)
(n must be 1, or 2) then C[y] ∗⇒l tprime so that tprime
is described by (C1[],v1)multimap(C2[],v2).
3. if Γ1 = fNa.1 : (C3[],v3) multimap (C4[],v4)
or if d = ((C3[],v3) multimap (C4[],v4),Y ) multimap
3Actually, if it is extended into a parser, it will ouput the
shared forest of the derivation trees; (de Groote, 2002) ex-
plains how to obtain the derived trees from the derivation
trees in the framework of ACGs
(C2[],v2) and t1 = λfNa.1y.v then fNa.1 ⇒l
tprimeprime and tprimeprime is described by (C3[],v3) multimap
(C4[],v4)
An item of the form (α;Γ turnstileleft t : d;) verifies:
1. (Γprime,t,α) ∈ LG where Γprime = fNa.1 : F if
Γ = fNa.1 : e or Γprime = Γ otherwise
2. d does not contain non-specified syntactic
descriptions4.
3. t ∗⇒l tprime and tprime is described by d (d may either
represent a string context or a string).
4. if Γ = fNa.1 : (C3[],v3) multimap (C4[],v4) or if
d = ((C3[],v3) multimap(C4[],v4),(C1[],v1)) multimap
(C2[],v2) and t1 = λfNa.1y.tprime then fMa.1 ∗⇒l
tprimeprime and tprimeprime is described by (C3[],v3) multimap
(C4[],v4)
Finally an item of the form
[Na.1;Γ;t;(C1[],v1) multimap (C2[],v2)] im-
plies the existence of tprime, (C3[],v3) and
(C4[],v4) such that (Na;turnstileleft tprime : ((C3[],v3) multimap
(C4[],v4),(C1[],v1)) multimap (C2[],v2);) and
(Na.1;Γ turnstileleft t : (C3[],v3) multimap (C4[],v4));) are in
the chart.
An input λy.C[y] is recognized iff when the
fixed-point is reached, the chart contains an item
of the form (Ss; turnstileleft t : (λy.C[],y) multimap
(λy.[],C[y]); ) (where S is the start symbol of the
TAG G.
4There is no occurence of F or Y in d.
144
General items
(Na ; turnstileleft λfNa.1y.t1 : (F,Y )multimap (C1[],v1) ; fNa.1 : F,y : Y turnstileleft t2 : (C2[],v2))
(Na ; turnstileleft λfNa.1y.t : ((C1[],v1)multimap(C2[],v2),Y )multimap (C3[],v3) ; y : Y turnstileleft t2 : (C4[],v4))
(Na ; turnstileleft λfNa.1y.t : ((C1[],v1)multimap(C2[],v2),(C3[],v3))multimap (C4[],v4) ; )
(α ; turnstileleft λy.t1 : Y multimap (C1[],v1) ; y : Y turnstileleft t2 : (C2[],v2))
(α ; turnstileleft λy.t : (C1[],v1)multimap(C2[],v2) ; )
(Na.1 ; fMa.1 : F turnstileleft λy.t : Y multimap(C[],v) ; fMa.1 : F,y : Y turnstileleft t2 : (C2[],v2)
(Na.1 ; fMa.1 : (C1[],v1)multimap (C2[],v2) turnstileleft λy.t : Y multimap(C3[],v3) ; y : Y turnstileleft t2 : (C4[],v4))
(Na.1 ; fMa.1 : (C1[],v1)multimap (C2[],v2) turnstileleft λy.t : (C3[],v3)multimap (C4[],v4) ; )
Wrapped subtrees
[Na.1 ; ; t ; (C1[],v1)multimap (C2[],v2)]
[Na.1 ; fMa.1 : (C1[],v1)multimap(C2[],v2) ; t ; (C3[],v3)multimap (C4[],v4)]
Figure 3: Possible items
6 Conclusion and perspective
In this paper, we have illustrated the use for TAGs
of general and abstract tools, syntactic descrip-
tions, which can be used to parse linear λ-terms.
Even though ACGs are very general in their def-
inition, the algorithm we describe shows that this
generality is not a source of unefficiency. Indeed,
this algorithm, a special instance of a general one
which can parse any second order ACG and it be-
haves exactly the same way as the algorithm given
by (Schabes and Joshi, 1988) so that it parses a
second order ACG encoding a TAG in O(n6).
The technique used enables to see generation as
parsing. In the framework of second order ACG,
the logical formulae on which generation is per-
formed are bound to be obtained from semantic re-
cipies coded with linear λ-terms and are therefore
not really adapted to Montague semantics. Nev-
ertheless, syntactic descriptions can be extended
with intersection types (Dezani-Ciancaglini et al.,
2005) in order to cope with simply typed λ-
calculus. With this extension, it seems possible
to extend the algorithm for second order ACGs
so that it can deal with simply typed λ-terms and
without loosing its efficiency in the linear case.

References
Henk P. Barendregt. 1984. The Lambda Calculus: Its
Syntax and Semantics, volume 103. Studies in Logic
and the Foundations of Mathematics, North-Holland
Amsterdam. revised edition.
Philippe de Groote and Sylvain Pogodalla. 2004. On
the expressive power of abstract categorial gram-
mars: Representing context-free formalisms. Jour-
nal of Logic, Language and Information, 13(4):421–
438.
Philippe de Groote. 2001. Towards abstract categorial
grammars. In Association for Computational Lin-
guistic, editor, Proceedings 39th Annual Meeting
and 10th Conference of the European Chapter,
pages 148–155. Morgan Kaufmann Publishers.
Philippe de Groote. 2002. Tree-adjoining grammars
as abstract categorial grammars. TAG+6, Proceed-
ings of the sixth International Workshop on Tree Ad-
joining Grammars and Related Frameworks, pages
145–150.
Mariangiola Dezani-Ciancaglini, Furio Honsell, and
Yoko Motohama. 2005. Compositional Characteri-
zation of λ-terms using Intersection Types. Theoret.
Comput. Sci., 340(3):459–495.
G´erard Huet. 1997. The zipper. Journal of Functional
Programming, 7(5):549–554.
Richard Montague. 1974. Formal Philosophy: Se-
lected Papers of Richard Montague. Yale University
Press, New Haven, CT.
Sylvain Salvati. 2006. Syntactic descriptions: a type
system for solving matching equations in the linear
λ-calculus. In to be published in the proceedings
of the 17th International Conference on Rewriting
Techniques and Applications.
Yves Schabes and Aravind K. Joshi. 1988. An earley-
type parsing algorithm for tree adjoining grammars.
In Proceedings of the 26th annual meeting on Asso-
ciation for Computational Linguistics, pages 258–
269, Morristown, NJ, USA. Association for Compu-
tational Linguistics.
Stuart M. Shieber, Yves Schabes, and Fernando C. N.
Pereira. 1995. Principles and implementation of
deductive parsing. Journal of Logic Programming,
24(1–2):3–36, July–August. Also available as cmp-
lg/9404008.
