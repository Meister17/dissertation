ABSTRACT 
'~4achine Transcoding" 
There are grounds for questioning whether translation done by computer 
should resemble closely, either in process or in product, translation done manual- 
ly. Several workers in machine translation have proposed radical departures from 
the naively adopted goal of simulating manual translation mechanically. Because 
they saw a similarity between their mechanically feasible output and the pidgin 
languages that have arisen naturally in some parts of the world, they dubbed their 
proposals 'pidgin translation'. 
This paper explores a more rigorous approach, which may be described as 
follows: texts in one language are encoded into words and other morphs taken from 
a second language, but this is done without disturbing the syntax of the source 
language. The term 'transcoding' is coined to denote that process. Transcoding 
results in a product in many ways similar to earlier types of pidgin translation 
(and incidentally more like natural pidgin languages than the latter were), but is 
founded on the principle that no information-bearing elements at all may be lost in 
the process. This principle is embodied in two strict guidelines: 
I. Each and every grammatical morph in the original must be reproduced in 
the transcoded text by a code morph; the linear order of elements is 
not to be changed. 
2. The transcoding dictionary (the codebook) must be so constructed as 
to preserve all the lexical contrasts inherent in the source vocabu- 
lary. 
These guidelines are investigated and supplemented, and their effects 
are predicted with regard to (1) the output, (2) the user's ability to interpret 
transcoded material and to learn to interpret it better, (5) the possibility-of 
employing transcoding to make rough drafts for the human professionals in trans- 
lation agencies to revise. 
Sample passages of transcoded Arabic and transcoded Chinese are 
presented, following which some comparisons are made between transcoding and pidgin 
translation, machine translation as practised or envisioned, and manual translation. 
T.R. Hofmann, 
Collage Militaire Royal, 
St-Jean, Que. 
15 
Brian Harris, 
Machine Translation Project, 
Universit~ de Montreal. 
RESUME 
Le transcodage automatique 
On peut douter qu'une traduction automatique doive ressembler beaucoup 
une traduction faite ~ la main, dans sa forme ou dans le processus qui le produit. 
Plusieurs d'entre ceux qui se sont attaqu~s aux probl~mes de la traduction automa- 
tique ont propos6 de s'~carter de fa~on radicale de ce but na~f qu'est la simula- 
tion automatique du processus de traduction humaine. Ayant vu quelques similarit~s 
entre leur sortie machine, qui de toute ~vidence repr~sentait quelque chose de 
faisable sur ordinateur, et les langues naturelles dites creoles qui ont surgi en 
diverses r~gions du monde, ils surnommaient leur genre de traduction "traduction 
cr~olis~e". 
Cette comaunication fair le point d'une approche plus rigoureuse. I1 
s'agit de textes dans une langue source que l'on fait coder de nouveau en des * 
mots et des morphemes tir~s d'une langue seconde, mais tout en conservant la 
syntaxe et m6me la s~mantique de la langue source. D'apr~s la pr~sente commu- 
nication, ce proc6d6 s'appelle 'transcodage'. Le transcodage donc nous fournit 
un produit semblable ~ bien des ~gards ~ la traduction cr6olis~e ci-dessus; 
d'ailleurs il se rapproche encore plus des cr6oles naturels que celle-ci. Cepen- 
dant, il est bas~ sur un principe qui interdit formellement la perte, en cours 
du transcodage, de tout ~l~ment porteur d'information grammaticale ou s~mantique. 
Ce principe se r~alise par deux consignes: 
1. Tousles ~l~ments grammaticaux du texte source seront repr~sent~s 
dans la version transcod~e; l'ordre lin~aire des ~l~ments ne doit 
pas 6ire modifi~. 
2. Lots de la fabrication d'un dictionnaire destin~ au transcodage, 
qui serait en effet une esp~ce de dictionnaire chiffr~, on conser- 
vera soigneusement tousles ¢ontrastes ressortant de la structure 
du lexique de la langue source. 
¢ Nous explorons la port~e de ces ¢onsignes, nous les compl~tons, et 
pr~voyons leurs effets sur (i) la sortie (2) la possibilit~ pour l'usager d'in- 
terpr6£er le materiel transcod~ et d'apprendre ~ l'interpr~ter mieux (3) la 
possibilit~ d'employer le transcodage pour preparer des premieres versions gros- . 
si~res que les sp~cialistes humains des agences de traduction pourraient am~lio- 
rer. 
Nous pr~sentons des exemples d'arabe transcod6 et de chinois trans- 
cod~, suivis de quelques comparaisons entre le transcodage, la traduction cr6o- 
lisle, la traduction automatique telle qu'on la pratique et telle qu'on l'envisage 
pour l'avenir, et la traduction manuelle. 
I 
14 
T.R. Hofmann 
Collage Militaire Royal 
St-Jean, Qua. 
Brian Harris 
Projet de Traduction Automatique 
Universit~ de Montreal 
