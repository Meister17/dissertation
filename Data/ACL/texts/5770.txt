References

Jian Sun, et al. 2002. Chinese Named Entity
Identification Using Class-based Language Model.
Proceedings of the 19th International Conference on
Computational Linguistics

Hsin-His Chen, et al. 1997. Description of the NTU
System Used for MET2. Proceedings of the Seventh
Message Understanding Conference

Tat-Seng Chua, et al. 2002. Learning Pattern Rules for
Chinese Named Entity Extraction. Proceedings of
AAAI�02

W.J.Teahan, et al. 1999. A Compression-based
Algorithm for Chinese Word Segmentation.
Computational Linguistic 26(2000) 375-393

Maosong Sun, et al. 1994. Identifying Chinese Names in
Unrestricted Texts. Journal of Chinese Information
Processing. 1994,8(2)

Collins, Singer. 1999. Unsupervised Models for Named
Entity Classification. Proceedings of 1999 Joint
SIGDAT Conference on Empirical Methods in NLP
and Very Large Corpora

Daniel M. Bikel, et al. 1997. Nymble: a High-
Performance Learning Name-finder. Proceedings of
ANLP-97, page 194-201, 1997

Yu et al. 1998. Description of the Kent Ridge Digital
Labs System Used for MUC-7. Proceedings of the
Seventh Message Understanding Conference

Silviu Cucerzan, David Yarowsky. 1999. Language
Independent Named Entity Recognition Combining
Morphological and Contextual Evidence.
Proceedings 1999 Joint SIGDAT Conference on
EMNLP and VLC

Peter F.Brown, et al. 1992. Class-Based n-gram Model
of Natural Language. 1992 Association for
Computational Linguistics

A. Mikheev, M. Moens, and C. Grover. 1999. Named
entity recognition without gazetteers. Proceedings of
the Ninth Conference of the European Chapter of the
Association for Computational Linguistics. Bergen,
Norway

Borthwich. A. 1999. A Maximum Entropy Approach to
Named Entity Recognition. PhD Dissertation

Dong & Dong. 2000. Hownet. At: http://www.keenage.com

Yu, S.W. 1999. The Specification and Manual of
Chinese Word Segmentation and Part of Speech
Tagging. At: http://www.icl.pku.edu.cn/Introduction/
corpustagging. htm

Mei, J.J, et al. 1983. ????/TONG YI CI CI
LIN. Shanghai CISHU Press
