CondiLioned UnificaLiou for Natural l,an~uage Processinp, 
A\]\]STV~ACT 
This paper prescnLs what wc c.all a condiLiol'md unification, a 
r'm'w meLhod of unificatiol'~ for processing natural languages. 
The key idea is to annotate Lhe patterns wiLh a cerLcdn sort 
of conditions, so that they carry abundant inforrnation. '\]'his 
met.bed t.ransmits inforrnaLion frorn one pattern to another 
more efl'icienLly Lhan proecdurc aLLachmenLs, in which 
information cortLaincd in the procedure is embcddcd in the 
progranl rather Lhan dirccl./y aLLachcd Lo paLL(ms Coupled 
wilt techniques in forrnal linguistics> i\]\]orcovcr, conditiorled 
unification serves most. types o1" opcrations for natlu'ai 
\]ar/guage processil'q~,. 
KSiti f/asida 
\]'\]\]ecLroLechllica\[ 1,abe ' A.ory 
Ul\]lczorlo I } 4, 7~aktlra MtlFa, Niibari-Gurl, 
Ibaraki, \[tOb Japan 
(\[3) ptlt_tllS psrl nrnb(prcsonL, P, N) : 
notAlrd ~'-tng(P, N) 
l~ut_Lns, psn nl:nl)(T, l", N) : not_pres(T) 
noL_3rd sng(lst., ,N). not pres(past) 
not_~rd~'-;ng(;~nd, N) not pres(past4~a'rtlclp\]e ) 
rlotA/rd, sr~g(,'Wd, plural) not pres(basc). 
1. Introduction 
A currcr'd, major t.rcrY.t of naturul la~/guage processing is 
ehara.cterized by Lhc overall use o\[ unification (Sttiebc~r 
(198'l), Kay (1985), Proudir:~ and Pollard (1,985), Percira 
(198b), Shicbcr (1985), etc) reflecLing lhe recent develop 
merits in nonLra.nsformaLJonal linguistic \[ormalisrns, such as 
Lexical FuncUonal Grammar (}lrcsnan (198E)), Generalizcd 
Phrase St.r'tJcl.ur( (\]rarnrnar (GPS(\]) (Gazdar, Klein, Pulhlm 
and Sag (I 985)), i Icad Grammar (Pol}ard (19f1,1)), and tIcad- 
l)riven Phrase Structure Grammar (lIPS(;) (Pc\]lard 
(1985a.,b)). These formalisnls dispense wiLt ,qlobal opcra- 
Lioits sueh as t.ransfornlaLion, alld instead cxp!oit h~cal 
operations each C'Ollf'lrled wttJ/i\[l a local tree Such local 
operations ar'c forunulatcd in Lcrms of uni~caLion 
\]Iowevcr, Lhe ordinary unification as in Prolog is 
insufficient, seeu rrorn both scientific (here, alias liriguJsl,ic) 
and cngin(.'ering poilfl'.¢ of vicw '\['he F, robh-'trl is that p,t 
tc\]~\[\]s Lo bc tl\[li(ie({ wiL\]l each other lack the cape.city rot car- 
rying irfforrnaLion 
In Lhis papcr we \[)rcscnl a new mcLhod of unificaLion 
which we (call conditioned unification. The essence of the 
method is t.o deal wit.h paLLcrns aimoLated by seine sort of 
condit, ioils. These eondiLioi<ls are so cortsl, raincd /-Is Lo 'oe 
cfficicntly operated on, and yet to be able to carry rich 
enough informaLion t.o caDLure linguistic gcneralizations. 
2. The Problem 
Ordinary patterns as it/ Pr(;h)g Is.el< cxprcssivc power, 
t)esatlSC var\[ablcs theFcirl arc Sil)i\[)ly il\](iClCl"tlliltdt(7 alld 
Ihtis car'ry almost no irffqrrnalion 'l'hercforc, stie}l palL(ms 
aud unification among thcm arc msuffiei0nt for' capturlng 
t\]le {~,i<'al/l rYlat ic al <!,>erm r'al ixat ior~ and tim process:n~> 
effici(ncy. \],It us look a.t some c:<amph.~s below A ~,,l'anl 
matical catc£>ory is assumed Lo be it llst of features A 
feature consisLs of a feature nalnc and a w~hic, and 
rcprcscnLcd as a t.cmn like tt~rn.e (vat,z(). 
'\['hc \]cxical entry of English vcrb p'u,t, for instance, can 
not. be described as a I'roloc patLcrn, bill needs some arlllO- 
Lation (i.c.,p~zt Im.s~)s?t.~zmS(T, P, N)) as in (1) 
(1) k:xicorl(puL, I tensc('I'), p(msorl(\]')> number(N)I) : 
put_Lns_ psn nrnb(T, P, N) 
}let(?, fcaLLIICS olincr Lhan ten, se., perso?t, and ?lattnher arc 
omitLed, arm predicate p~ztmtm.spsTt~z?n.b is dcfinc.d c,s in 
(2) 
For a biL morc COIT/I)I i:aLcd instance, conmdcr the rela- 
tionship between a synLacLic gap and iLs filler. In GPSG, 
IIPSC, cte., tiffs relationship is; captured in terms of the 
SI,ASI\[ fea/.urc, which reprcscnts gaps in the category of \] 
U~.i~tk is craz U, for cxamplc, thc SI,ASII feature is spcciflcd 
as \[NI j\] ller'e SI,ASI{ is assumed to take as its wdue alist of 
catcgorncs. SLaLcd below is a simplil3cd principle about the 
disti'ibution of this featqrc in I.yptca\] cascs 
(3) lu a local tr'ec, Lhc rllotl;cr catcgor/s S\],ASI\] 
tea.Lure is obl.aJncd by coneatcr~atir, g from h.fi\[. Lo 
rip, ht the S I,ASII fcat,wcs of hey de.,.ightcrs 
In order to describe: this principle, s('nnetlting :uorc than a 
nlerc pat t.crn ts lcquir(x\] again: 
(i) IocalJr'cc.(lslc, sh(X)\], Ismsh(¥)l, Islash(Z)\])- 
append(Y, Z, X) 
l'eaturcs othcr thanSI,AS}l arc omitted herc. 
The so called procedure altachmcnts is the most coln- 
rnon way or conllflclncntJnp, the \])oor clcscriplivc capacM.y or 
ordinary patterns \["or instance, you may regard Lhc bodies 
of \]h)rn elaus(s (1) and (4) as at la,_hed procedures 
The dr'awbacl< of procedure atLachr~lcnl is ut the fact 
t.hnt the ouly way of using Lhc proccclurcs Is to execute 
thorn I"or t.his reason, proecdur,}s arc Irmrcly embedded lu 
programs, rcAhcr than at.lath(x\] to those paLterrls which 
th(sc itrotu'ams operate on The irtforrnaLion which 
\[)ro((durcs cantain car/rx~t {U.'nera\]\]y be I~.',rricd aFOlllld 
a( ross scvc;a/ part ial s\[rtlC\[ tlI( s ci\]ch Of which it pFoocc\]arc 
dircclly operates on, bccausc> oncc a procc(lurc is cxc 
cnt.td, the informs.lion whkh it c.ontainc:d is palqially lost 
For instance, when Icxical entry (1) is cxploiLecl, 
p~ztJ.n.s pstt.n;/m,6(?', I), /\i) is cxecut.cd and 7' and /~ arc 
il~stanliatcd Lo be preset~.t and Ist, icsp(cLivc\[y 'l'h'ds Ic\[L 
bchh~d is the informaLion abotlL the other ways Lo instan- 
LiaLc those wwiablcs. 
Actual procedure attaclu'ncrd.s musL be arr-ar<e, ed so 
that infornlat on shouhl not be it)st whelt procedures arc 
cxccutcd Freeze of Prolog (Colmcraucr (1982)), for 
instanc/, is a mcans of tins arr~ingerncnt. \]\]y exc(tll.i\[\]g 
freeze(V, "~), atomic formula ~ is frozen; i.c , the exccuLlon 
of'~ is >-uspcnded until w~riable X is instanttat.cd \]f'¢ con- 
tams .'(, thcl'cforc, }lop(fully uot. so rnuch lrtforrnat.iol~ is test. 
whcc ¢ is cxecuLed 
Ncvc.rthcless, freeze is problematic in two rcspt(ts 
Virsk, irJorn\]ation cart still be lost when the frozen pro-- 
ccdtll'CS LIFe cxccnted. Seeond, too nltlCh illforrllatiol3 cat\] 
be accumulatcd whilc several procedures arc frozen Sup 
pose, for itlst.ance, thaL freeze IX, t~tet~ber(.Y, \[a, 6 })) and 
fr<,t~.~<~. (r, ,~.',~,.~.,'(}'. I~'.~ I)) have bccn execut, cd '\['herr, X 
and Y can be ulfif\]cd with each other witt~ouL awakening 
ciLbcr procedure. In that (asc, Lho iifforn/at, ion that X may 
bc t) is redundant bctwccn Lhc I.wo proccdures, and Litc 
other part or irlfornmLion those Droecdtlres contain is Ill(Of\]" 
sistcitL What one might hope here is \[o Jrlstitntiatc )( (and 
Y) to be b If we had cxectitcd freeze(Y, member(Y, It, 
cL ) iristcad of freeze (Y, rn.ernber(Y, Ib, c I), computational 
85 
resources would be wasted as the price for a wrong process- 
int. 
After all, it is up to a programmer to Lake a deliberate 
care so that information should t)e efficiently transmitted 
across patterns This causes sewral problems interwoven 
with one another. First, since those programs reflect the 
intended order of execution, they fad to straightforwardly 
capture the nniforrnitJes captured by rules or principles 
such as (3). Accordingly, programnnng takes rnuch labor'. 
Moreover, the resulting programs work efficiently only along 
t.he initially inLer~ded order. 
3o Conditioned Unification 
3.1. Conditioned Patterns 
These problems will be. settled if we earl attach informa- 
tion to patterns, instead of attaching procedures to pro- 
grams l\[ere wc consider that such information is carried 
by some conditions on variables Variables are then 
regarded as carrying some information rather than remain:- 
ing simply indcterminatc 
I-}y a conditioned pal.tern let. us refer to a pair o\[a pat 
tern and a condition on the w~riables contained in that pat.- 
tern. l~'or simplicity, assume LhaL the condition of a condi- 
tioned pattern consists of atomic formulas of Pro/og whose 
argument positions are filled with variables appearing m tile 
pattx.'rn, and that the pre(hcates heading those atomic for 
mulas are defh~ed in l.erms of Horn clauses. For instance, 
we would hkc to regard the whole tbing in (\[) or (4) as a con- 
dJtioncd pattern. 
• 3.2. Modular Conditions 
The conditions in conditioned patterns must not be exe- 
cuted, or the contained information would be partially lost 
Tile conditions have to be somehow joined when conditioned 
patterns are unified, so t.hat the information they contain 
should be transmitted properly in tile sense that the result- 
ing condit.ion is equivalent \[o the Logical conjunction of tam 
input renditions and contains nciCrmr rcdnndant nor ineon 
sistent information. We call suet a unification a conditioned 
unification 
A simple way to reduce redundancy and inconsistency 
in a ('.ondiLion is to let each part of each possible value of 
each variable be sLlbjcct to at, most one constraint. \],eL us 
formulate this below. We say that a condition is 
superficially modular, when no variable appears twice in 
that rendition For instance, (Sa) is a superficially modular 
condition, whereas (Sb,c) are not. (Conditions are some. 
times wr'itterr as lists of atomic forrnulas ) 
(',9 a \[a(X, Y), b(Z), a(U, v)\] 
b. l a(X, Y), b(Y)\] 
e \[a(X,Y,X)\] 
l,'urther we say that a condition ~I' is modular, when all the 
relevant renditions are superficially modular, lIere, the 
relevant, conditions are {I} and the bodies of Horn clauses 
reached by descending along the definitions of the predi- 
cat.es appearing in ¢. A predicate is said to be modular 
when its definition contains only those Iiorn clauses whose 
bodies are modular conditions. A predicate is potentially 
modular when it is equivalent to some modular predicate 
A modular condition does not. impose two constraints 
on any one part. of any variable, and thcrcfore contains ne> 
kher redundancy nor ineonsistency, ltereaRer we consider 
that the condition m (.'very conditioned pattern should be 
modular. 
a.a. l'Jxpressive Power 
Conditioned patterns can carry rich enough informa- 
tion for capturing the linguistic generality. Obviously, at 
86 
 'st., they can describe any finite set of finite patterns. \];'or 
instance, (I) is regarded as a conditioned pattern with 
modular condition \[pztt_g'ms_pstt~q,r~zb (T, P, N)\]. Moreover, 
also some recursivc predicates are modular, as is demon- 
strated below. 
(6) a appcnd(\[\], Y, Y): 
append(\[U I X\], Y, \[U I Z\]) :- append(X, Y, Z). 
b sublist(\[\], Y). 
sub\]ist(\[U I<I, \[U I Y\]) :- sublist(X, Y). 
sublJst(X, \[U IY\]):-sublist(X, Y). 
Thus, (4) is also a conditioned pattern. 
\]lowever, some recursive predicates are not potentially 
modular. They include reverse (the binary predieate which 
is satisfied i~r its two arguments are the reversals of each 
oilier, as in reverse(\[tot, b\], c, all, \[d, c,\[ct b\]\])), .perm 
(Lbe binary predicate satisfied iff its arguments arc permu- 
tat, ions of each other, as inperm(\[i, 2, 3\], \[2, 1, 3\])), subset 
(the binary predicate which obtains iff the first argument is 
a subset of the second, as in s~zbset(\[d! b\], to, b, c, all)), 
etc. 
New.'rtheless, t.his causes no problem regarding natural 
language proeessing, because potentially infinite patterns 
corne up only out of features such as SLASt\[, which do not 
require those non ruodular predicates. 
3.4. The Unifier 
Shown below is a'trace of the conditioned unification 
between conditioned patter'us (7) and (8) (here we use the 
same notation for eondit.ioned patterns as for IIorn clauses), 
where the predicates therein have been defied as in (9). 
(The definitions of c0 and e3 are not exploited.) First, we 
unify iX, )2 Z, g/\] and \[A, 7\], C, D\] with one another and get. 
X- A, Y : /3, Z = C, and W = D \]n the environment under 
lifts unification, the two conditions are concatenated, result- 
ing in \[c0(X), e I(Y, Z), e2(Z, W)\]. The major task of this 
conditioned unification is to obtain a modular condition 
equivalent to this rmn-rnodular conditiorl This is tire job of 
funcl.ion ~tod~zlayi, ze. Mod~zla.~tze rails function ~;~ttegrctte, 
which r'eLtlrns an atomic formula equiwrlent Lo the gives 
condition. The Lcrminatior~ of a ?rtodulct,'ize or anir~fegrate 
is indicated by ~ preceding the reLurn-waluc, with the 
same amount of indentation as the outset of this function- 
rail was indicated witb When an {~ztegro, te calls a ~zodula~'- 
ize, the alphabetic identifier of the exploited Ilorn clause is 
indicated to the h.'ft hand side, and the temporal unification 
to the right-hand side. Atomic formulas made in integrate 
is written following 4. Each lIorn clause entered into the 
definition is shown following % and given an alphabetic 
identifier indicatedto the right-hand side. 
(7) IX, Y, Z, W\] :-- e0(X), el (Y, Z). 
(8) \[A, \]~, C, D\] :- e2(C, D). 
(9) e*(0, \]). (a) 
e ~ (q, e) (b) 
ca(l, P):-e:Xl'). (c) 
c~(e, 0). (d) 
modularize(\[e0(X), cl(Y, Z), c2(Z, W)\]) 
integrate(\[e0(X) \]) 
cO(X) 
integPate(\[cl(Y, Z), e2(Z, W)\]) 
c4(Y, Z, W) 
(a) modularize(\[e2(1, W)\]) Y = 0, Z = 1 
integrate(to2(1' W)\]) 
* eS(W) 
(c) rnodularize(\[e3(P)\]) W = P 
integrate(\[e3(P)\]) 
=~ e3(P) 
tea(p)\] 
c~(p) :- ca(P). (0 
=:> eS(W) 
-~ \[c,~)(w)\] 
1' o4(o, :, w) :. ~:',~(w). (j) 
(b) n:odular:'zc(\[c2(2, W)i) ¥ :- q, '/, :~ 
i:,t.o~ra~,'.(Im<3(a W)\]) 
* cO(W) 
(d) nladularizc(l I) w =.: o =-~ \[\] 
cS(0) (k) 
=+ c6(W) 
> I cs(w)J 
" o,3(q, ~, w) : o6(w). (I) 
=> c4(Y, Z, W) 
- > \[co(x), <:4(< z, W) l 
We CaN refine Lhe progra\]'n o\[ "btt~.grcs, ta so that it should 
avoid ally predicaLe w}iose definiLion coiuLains only one llorn 
clause. For instance, Lhe definiLion of cb consisLs only o\[(i) 
InsLead of (j), LhereR)re, we may }rove cd(0, 1, P) : c3(P) 
Also (1) can bc replaced by c 4(0, 2> 0), based on (k) 
We are able Lo work out r'ccursivc condiLions from F, lvor; 
recursivc coI:dit.iolls, lVor example, considor how X and Z 
arc unifiod under" t, ha conclit,iol: (10), whore ~rte'n~be.r is 
defined as in (1 1) 
(10) \[n,e:nher(X, Y), o0(z)\] 
(11) n/cinber(A, IA I IJl). (a) 
member(A, IC I i~i) :-i ....... her(A, it) (b) 
The Lracc of this ulfif\]cat.k~n is showl~ b('\]:'w, whc's'c prcdica.l~' 
c 1 is rccursivcly (\]o/~ll/Cd based on Lhc i'(,ctlrsiv(! dcfillJLioH of 
77"~ (¢ 77}, t) I~, ?' 
modularizc(lmcmbcr(X, Y), cO(X)I) 
int.cgraLo(I member(X, Y), cO(X)\]) 
, el(X, Y) 
(a) modularize(\[cO(A)\]) X = A, V -. \[A Ill\] 
int.e<~';ratc (I c O(A)I) 
= > oO(A) 
=~ 1 <:)(A)\] 
1' c I(A, \[A I ~<J) :-- c0(A) 
(1)) n:odularizc(lmcn,bcr(A, 11), c0(A)l) X :: A, Y :: \[c!i;I 
Jnl.c<qral.e(\[ nlernbcr(A, I~), cO(A)I) 
:~ el(A, 1~) 
::> \[cl(A, 33)1 
1'ci(A, Iclt<l): c~(A, 13). 
.... > c:(X, Y) 
:.~ \[o:(x, Y)I 
IL Js a job of in, tegra, te, Lo handle re,cursive de,hiLton. The 
lasL g?l, te.g?,ts, te. above recognizes Lhat. the first 4m.tegrate, 
which is Lrying Lo (\]cNr/c c 1, was called wit.h the same argu- 
ITlCrlI.S except, the variable narnes, llencc t.he last "inttegrctte 
simply reLul'ns c. I(A, H), because t,hc conLent, or cl is now 
bring worked ouL tlrldoY Lhc J'\]rsl. ~?ttegro.te arid thus it is 
rednndanL fol' t, he lasL {~tfegrate Lo \[urLhcr examine c 1. 
It is not. a\]ways possible fro' the above unifier t.~ unify 
paLL(2i"\[/s tlrl(\](~r roc. tlr'sivo Col\]d\[Liol/S \["or J//sLalloL', J\[ Cf/illIOL 
unify X with )" under \[appe~td.(X, Y, Z)\], becal_tsc Lhe result 
ing condiLion is noL potsnLia\]\]y rnodular, llowcver, such a 
situaLiol'l CtOCS FioL seoln t.o occur Jn actual \[al:g:lagc proccss- 
ir:g. 
4. Conclusiori 
We have prc.'-~er, Led a new nle/hod a\[ umfiealion, wh,ch 
wc call o. coildiLior~c(\] tltti(loaLioli, Whcl'e paLLorils to be 
uniPlc(\] "'re annoLaked by a certain sort. of corldit.ions on lhe 
variables wifich octroi" ill those patt.crn.<;. Theso condiLions 
are so r'est.ricted t.haL Lhey conlain as lit.Lie redundancy a<'; 
possible, and d'ms arc always assured to be satisfiable. 
This method has Lhe fo/h)wtng welcome characteristics 
l"h'st, I.he \])attorns to bc unified can carry at)llllda\]'lL infos' 
mat.ion rcprcscnLcd by t.he conditions han:,~in!,; on t.hClll The 
expressive capacity of Lhe,<¢c condiLion,s is sl:fl'Jc{en\[ for cap- 
t.uring \]JH~U, IIihLJ(: ~sCHCl'a\]i',,iOS ~ccorld, such irfformat.ion is 
cfreclivcly Ir'ansrnitLed, by h~t.egrat.\[n? the col~dil,ion.~ v;her, 
pat.'..crl:,<s o.ro unif'ied Unlike procedure aLLacl:ment.s, in thil~ 
COllne(:lion, Ll/c infornGaLioi~-conveying <.'fficicl:cy of our Colt 
dilioiued unif'icat.\[on is no{ afl'c'(gcd by the direct.ion of t.i~c 
daLa.flow Therefore, O/ll" col'l(\]{lioned unifies.Lion is oo;rn- 
plel/ly r(ver'sJbk< and ',hns is \[n'on:ising its a Los\] for 
dcscril)h'T> <~{l'all/lllilrs fOF bolh SCllL(Hb':C comprel/ensiol: slid 
prochl(d toll 
Owing t.o Lhese cllar'act(!risLics, Otll conditioned 
unif'caLian l)r'avh|es a now prog, ra.unniiug 1.1aradigtn foi 
I/illt/l'/tl lar/y,/lag(." \[)l'OCCSSil/lJ>, rcph~.cing proccd/1Fc aLt, o.ctl- 
I3:ont.s which haw3 tradlLionally el2joyed i.\]lc Llbiq/lity Lhat. 
t.hcy do noL descrvc 

References

Bresnan, J. (ed.) (1982) The Mental Representation of Grammatical Relations, MIT Press, Cambridge, Massachusetts. 

Cohnerauer, A. (1982) Prolog II Reference Manual and Theoretical Model ERA CNRS 363, Groupe d'Intelligence Artificielle, Universit� de Marseilic, Marselle. 

Gazdar, G. E. Klein, G. K. Pullum, and J. A. Sag (1985) Generalized Phrase Structure Grammar; Basil Blackwell. Oxford. 

Kay, M. (1985) "Parsing in Functional Unification Grammar." Natural Language Parsing, pp. 251--278, Cambridge University Press, Cambridge, England. 

Fernando C. N. Pereira, A structure-sharing representation for unification-based grammar formalisms, Proceedings of the 23rd annual meeting on Association for Computational Linguistics, p.137-144, July 08-12, 1985, Chicago, Illinois 

Pollard, C. J. (1984) Generalized Phrase Structure Grammar. Head Grammars, and Natural Languages. Doctoral dissertation, Stanford University, Stanford, California. 

Pollard, C. J. (1985a) Lecture Notes on Head-Driven Phrase Structure Grammar. Center for the Study of Language and Information. 

Pollard, C. J. (1985b) "Phrase Structure Grammar without Metarules," Proceedings of the Fourth West Coast Conference on Formal Linguistics, University of Southern California, Los Angeles, California. 

Derek Proudian , Carl Pollard, Parsing Head-driven Phrase Structure Grammar, Proceedings of the 23rd annual meeting on Association for Computational Linguistics, p.167-171, July 08-12, 1985, Chicago, Illinois 

Stuart M. Shieber, The design of a computer language for linguistic information, Proceedings of the 22nd annual meeting on Association for Computational Linguistics, p.362-366, July 02-06, 1984, Stanford, California 

Stuart M. Shieber, Using restriction to extend parsing algorithms for complex-feature-based formalisms, Proceedings of the 23rd annual meeting on Association for Computational Linguistics, p.145-152, July 08-12, 1985, Chicago, Illinois 
