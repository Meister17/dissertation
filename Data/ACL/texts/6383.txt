References

C. Baker and C. Fillmore and J. Lowe. The berkeley framenet project. COLING ACL.

Daniel Gildea and Daniel Jurafsky. 2002. Automatic lebeling of semantic roles. Computational Linguistics.

P. Kingsbury and M. Palmer and M. Marcus. Adding semantic annotation to the penn treebank. HLT.

S. Pradhan and K. Hacioglu and V. Krugler and W. Ward and J. Martin and D. Jurafsky. 2003. Semantic role parsing: adding semantic structure to unstructured text. ICDM.

M. Surdeanu et al. 2003. PRoceedings of ACL 2003.
