﻿References

Steven Abney. 1996.  Partial Parsing via Finite-State Cascades. In ESSLLI-96 Workshop on Robust Parsing, Prague, Czech Republic.

Salah Ait-Mokhtar, Jean-Pierre Chanod, and Claude Roux. 2002. Robustness beyond shallowness: incremental deep parsing. NLE.

Judith Eckle-Kohler. 1999. Linguistisches Wissen zur Aotumatischen Lexikon-Akquisition aus deutschen Textcorpora.

Anette Frank, Markus Becker, Berthold Crysmann, Bernd Kiefer, and Ulrich Schafer.  2003. Integrated Shallow and Deep Parsing: TopP meets HPSG.  In ACL 2003, Sapporo, Japan.

Dale Gerdemann and Gertjan can Noord. 2000. Approximation and Exactness in Finite State Optimality Theory.  In Proceedings of the Fifth Workshop of the ACL Special Interest Group in Computational Phonology, Luxembourg.

Erhard W. Hinrichs, Sandra Kubler, Frank Henrik Muller, and Tylman Ule.  2002.  A Hybrid Architecture for Robust Parsing of German. LREC 2002.

Frank Henrik Muller and Tylman Ule.  2002.  Annotating topological fields and chunks - and revising POS tags at the same time.  COLING 2002.

Frank Henrik Muller. 2004.  A Finite State Approach to Shallow Parsing and Grammatical Functions Anootation of German.  PhD Thesis.

Gunter Neumann, Christian Braun, and Jakub Piskorski. 2000.  A Divide-and-Conquer Strategy for Shallow Parsing of German Free Texts.  ANLP 2000.

Kemal Oflazer. 2003.  Dependency Parsing with an extended finite-state approach.  Computational Linguistics.

Michael Schiehlen. 2003.  Combining Deep and Shallow Approaches in Parsing German.  ACL 2003.

Anne Schiller, Simone Teufel, and Christine Thielen, 1995.  Guidelines fur das Taggen deutscher Textcorpora mit STTS.

Anne Schiller. 1995. DMOR: Benutzer-Handbuch.

Heike Telljohann, Erhard W. Hinrichs, and Sandra Kubler. 2003. Stylebook for the Tubingen Treebank of Written German.  Technical Report.

Tylman Ule and Frank Henrik Muller.  2004. KaRoPars: Ein Systemzur linguistischen Annotation grosser Text-Korpora des Deutschen.  In A. Mehler and H. Lobin, editors, Automatische Textanalyse System und Methoden zur Annotation und Analyse naturlichsprachlicher.

Hans Uszkoreit. 1987.  Word order and constituent structure in German, volume 8 of CSLI lecture notes.