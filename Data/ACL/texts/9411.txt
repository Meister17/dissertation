CONVERSATIONS WITH A COMPUTER - AN EXAMPLE OF NETWORK 
PROGRAMMING IN RELATIONAL DATA BASE ENVIRONMENT 
M. Nalbach, K. Studzinski and S. Wsligorski 
Institute of Informatics 
Warsaw University 
00901 Warsaw, PKiN 850 
Poland 
Any efficient human-computer conversation in an ethnic 
language needs rather large ammount of information, which 
generally may be classified as follows: 
I/ Script: rules governing entire exchange of messages be- 
tween computer and human which generally determine how 
computer reacts and responds in various possible situat- 
ions. 
2/ Grammar and vocabulary of the language: rules of recogni- 
tion of words, phrases and entire messages, including 
algorithms of lexical and syntactical analysis. 
3/ Semantics: rule~ of understanding of words and messages, 
including methods of determining how the words and mess- 
ages relate to data stored in memory and what should be 
specific reactions of the computer according to the re- 
cognized meaning messages. 
This data may be presented in various forms, e.g. as 
dictionaries, transition networks for lexical analysis, 
augmented transition networks (ATN) for syntactic analysis, 
semantic networks, relations, end so on. They may be also 
included into programs which control and run conversetions. 
- 203 - 
It is evident that this information must be easily mod- 
ifiable in case of debugging or improvement. It is very use- 
ful if the form of ell data created by a user complies with 
certain standards and the data ere stored in 8 uniform way, 
so as to make understanding and modification as easy as poss- 
ible. For this reason we use as a standard storage a relat- 
ional data base. 
We may consider any real conversation with 8 computer 
as a sequence of atomic units, each comprising one exchauge 
of messages between human and computer, with all accompany- 
ing actions. Every dialogue determines one sequence of such 
units and transitions. All such sequences of units and trans- 
itions form a network. It turns out that it is very useful 
to introduce recursion into such networks; since it is poss- 
ible to store or to fetch data during a dialogue, this con- 
cept resembles augmented transition networks (ATN). In fact, 
these conversation control networks may be transformed into 
ATN. However, their original form provides better protection 
against undesired indeterminism and backtracking. ATN in their 
original form are applied for syntactical analysis. 
Obviously, such networks are nothing else as programs 
in a special programming lar~uage (or lengu~es), quite nat- 
urelly~ networks may be comfortably described by relations. 
This paper presents results of an ~plementation of these 
ideas. Data base management programs are in Fortran, but 
user access to the data base is entirely conversational. All 
networks stored in the base have form of relations. For ex- 
ample, elements of a relation for an ATN may have the form 
shown in Fig. I. This relation is translated into • simpler 
one in which all conditions and actions are replaced by links 
to Fortran subroutines obtained as results of translation of 
corresponding expressions, and names of states in "tO" field 
are replaced by identifiers of appropriate tuples in the new 
relation. Networks in this form may be interpreted or compil- 
- 204. - 
ed to Fortran. Compilation is usually msde for large ATN's 
for which simple interpretation would be too slow. We obtain- 
ed in this way good speeds even for very complicated syntact- 
ic grammars. Conversation control netwOrks are always inter- 
preted, for in this case no speedup was necessary. The con- 
versation control allows bootstrapping, i.e. an appropriately 
created network may control process of conversational creat- 
ion or modification of any CCN. 
Our implementation was made in Fortran, in spite of 811 
its disadvantages, mainly for Portability reasons. It is still 
much more easy to transfer to other computer or mini soft- 
ware written in Fortran than in any other language, including 
LISP. 
STATE!ARCNAM 
s/ TST 
S5/ PUSH 
SIOI POP 
ARG',.~ENT 
ONTEST 
tU'S/ 
AP~.,t~COETR (N) 
CONDITION 
EQ(LEX 
qUOt~ ~ON~ 
~Q (G~I~ (CAT) 
~vot~ ~A~ PRON}} 
T 
ACTIONS ~0 
SETR (WHERE ON) ~ 1 ! 
SETR(ADJ $4/ 
Fig. I 
- 205 - 
