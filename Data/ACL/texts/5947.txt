Message from the Program Committee Co-Chair
This volume is the Proceedings of the Sixth International Workshop on Information Re-
trieval with Asian Languages (IRAL 2003) which is held on July 7, 2003, in Sappro, Japan. This
workshop is the sixth event of the series after about two years absence. The first workshop was
held in Korea, 1996, and then we had 4 more workshops in Japan, Singapore, Taiwan and Hong
Kong, respectively.
Unfortunately, because of organizing difficulties in the steering committee members of par-
ticipating Asian countries, we could not hold workshops in 2001 and 2002. In IRAL2000 in Hong
Kong, we collocated the workshop just before ACL2000. In the Year 2003, we have ACL2003 in
Sapporo, and we proposed to have an IRAL workshop as one of ACL supported workshops.
For this workshop, we have 25 paper submissions from 7 countries/area; Canada (1), Hong
Kong (1), Japan (10), Korea (8), Taiwan (2), Thailand (1), and US (2). From all these submis-
sions, the Program Committee has chosen 13 papers for oral presentation and 9 papers for poster
presentation.
On behalf of the organizing staff, I would like to express our heartfelt thanks to all of the
people who attended and worked for the IRAL 2003 workshop. We hope having our workshop
within ACL2003 will be a good chance to start again our periodical gathering of IR research
community.
Professor Jun Adachi
Program Committee Co-Chair, IRAL 2003
Director, Research Center for Information Resources
National Institute of Informatics
i
