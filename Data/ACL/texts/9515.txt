STRATIFIKATIVE SPRACHBESCHREIBUNG ~IT "INKORPORIERENDER" 
BEDEUTUNGSKO~PONENTE - ELEHENTE EINES ENTWUR~S 
Bernd Koenitz 
Karl-Marx-Universit~t, Leipzig, DDR 
In generativen Beschreibungen naturlicher Sprachen 
pflegen paradigmatische nicht-eindeutige Beziehungen zwischen 
Einheiten der lautlichen (bzw. graphischen) Ebene und Einhei- 
ten der Bedeutungsebene - Mehrdeutigkeit und Synomymie - auch 
als paradigmatische Beziehungen (der Metasprache) dargestellt 
zu werden, wobei meist yon mehreren Reprasentationsebenen 
ausgegamgenwlrdund nicht-eindeutige Beziehungen zwischen 
den Einheitel beliebiger Ebenenpaare in Betracht ko--.en. Ob- 
wohl eine rein paradigmatische Beschreibung offensichtlich 
ihrem Wesen nach paradigmatischer Beziehungen nur zu nat~r- 
lich erscheint, wird in diesem Beitrag da~ttr pl~diert, Mehr- 
deutigkeiten in eine syntagmatische Notation zu projizieren; 
im Falle eines n-deutlgen Ausdrucks E einer naturlichen Spra- 
che L enthalt deseen Notation auf der Bedeutungsebene B(E ) 
die Teilnotationen B(E)I , ..° B(E)i , ... B(E)n 
(I ~ i ~ n), so dass jede Teilnotation B(E)i eine der poten- 
tiellen Bedeutungen yon E beschreibt. Eine solche Bedeutungs- 
notierung ist vergleichbar mlt der Ausgabe eines Textes bei 
auf Nachredaktion berechnetem maechinellen Uabersetzen, in 
der mehrere "synonyme" Ausdr~cke zur Auswahl angebotenumd 
neben~inandergeschrieben werdenv Synonymlebeziehungen (echte 
- soweit ein solcher Begriff Berechtigunghat) wurden aller- 
dings in einem abstrakten (generativen) Modell auf diese 
- 162 - 
Weise nicht adaquat beschrieben; die Paradigmatizit~t yon Sy- 
non~miebeziehungen ist offensichtlich, und die spezifischen 
funktlonellen Nichtaquivalenzen, die zwlschen Synonymen be- 
stehen (k~nnen), wurden damlt ja nicht erfasst. 
Im Palle yon eehrdeutlgkeiten ist deren rein paradigma- 
tiseher Charakter zumlndest nicht offensichtlich, da die hler- 
bel in Betracht zu ziehenden Entlt~ten, die Bewusstseinsabbil- 
der, der direkten Beobachtung nicht zug~ugllch slnd und erst 
nachgewiesen werden m~sste, dass im Palle der Verwendung eines 
mehrdeutlgen Ausdrucks in einem Text in jedem Falle nut ein 
Element der Menge der potentlellen Bedeutungen realieiert wird. 
Die Annahme der grunds~tzlichen M~gllchkeit slmultaner Reali- 
eierung yon mehr ale einer der potentlellen Bedeutungen eines 
nat~rllchsprachigen Ausdrucks erschelnt als berechtlgt, wenn 
man solche Erscheinungen in Erw~gung zieht wle Texte, die fur 
die Nonosemierung eines mehrdeutiEen Elements keine ausrelchen- 
de Information liefern, ferner Konnotatlonen (ira Sinne yon an 
der Peripheri~des Bewusstseins realislerten Nebenbedeutungen) 
sowle auch (oft unter "Vaghalt" subsumlerte) Undifferenziert- 
heit der Bedeutung. 
Besondere Beachtung verdient in diesem Zusammemhang auch 
die Tatsache, dass Mehrdeutigkelten generell dadurch gegeben 
sind, dass einmal der Zeichenkorper selbst eine (die minimale) 
Bedeutung einer Benennung begrundet und andererseits Jede Be- 
nennung auch in metasprachlicher Bedeutung (ale "metasprach- 
licher Eigenname") verwendet werden kann: Eine Benennung N be- 
deutet ob~ektsprachlich auch (u~d mindestens) "ein (bzw. das- 
~enlge) x, das "N" gennant wlrd" und hat daruber hlnaus die 
potentlelle metasprachliche Bedeutung °das~enige x, das mit 
"N" identisch let'. 
Wenn vorausgesetzt wird, dass in der Bedeu~anotlerung 
eines Jeden mehrdeutigen Ausdrucks die Notlerungen aller eel- 
net potentiellen Bedeutungen enthalten sind, so ist unter die- 
sem Aspekt die Eineindeutlgkeit der Beziehungen zwischen den 
- 163 - 
Einheiten der phonischen (bzw. ~aphischen) Ebene und denen 
der Bedeutungsebene gewahrleiste%. Da Jedoch zugleich die na- 
t{~rlioh n/cht zu bestreitende und fur das Funktionieren der 
Sprache unabdingbare Erscheinung der Selektierung einer echten 
Teilmenge der Menge der potentiellen Bedeutungen eines Aus- 
druckes (ira No~nalfalle Monosemierung) erkle~t werden muss, 
w~rd an~enommen, dass in der Bedeutungsnotierung der Jeweili- 
gen komplexeren (kombinatorischen) Einheit, innerhalb derer 
die Bed~u~en f~r die Selektion einer Teilmen6e der Bedeu- 
tungen des betreffenden (weniger komplexen) Ausdrucks gegeben 
s~d, diese Bedeu%ungsselektion ausgerlesen wird. ~ 
Im Falle yon Synonymiebeziehungen ist die Eineindeutig- 
keit der Relationen zlrlschen den entsprechenden Ellaheiten der 
verschiedenen Ebenen in der genera%lven Beschreibung einer- 
seits dutch entsprechende stilistische Indizierung der seman- 
¢ischen Notierung, andererseits dutch die Integ~lerung der Be- 
schreibu~ des Zeichenkorpers in die Bedeutungsnotieru~ ge- 
wabrleis%et. Die Beschreibung yon S~-nonymie bleibt im Weiter~n 
ausser Betracht, doch der in diesem Zusannnenhang zuletzt ge- 
nannte Gesichtspunkt ist lnsofern wichtig, als hieraus beson- 
ders deutlich erkennbar ist, dass in der bier ins Auge gefass- 
ten Eenerativen Beschreibung die den Repr~sentationsebenen 
entsprechenden (generativen) Komponenten auf spezifische Wei- 
se ~Liteinander verknupft sind, wobei die Bedeutungskomponente 
eine zentrale Stellung und integrative l~mktion hat, indem 
die mit den "niederen", der Oberflache n~n Ebenen korres- 
pondierenden Komponenten der Beschreibung separierbar, zu- 
gleSQh abet in die Bedeutu~skomponente "inkorporiert" sind. 
Es wird angenommen - und kurz be~z%mdet -, dass ein ab- 
straktes Modell n~L~ Elgenschaften, wie sie bier angedeutet 
wurden, in verschiedener Hinsicht auch unter dem Gesichts- 
punkt de~ maschinellen Uebersetzung yon Interesse sein d~Lrfte. 
- 164- 
