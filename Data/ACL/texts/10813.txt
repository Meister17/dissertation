I. REL English in Terms of Modern Linguistics 
REL, a Rapidly Extensible Language System, is an 
integrated information system operating in conversational inter- 
action with the computer. It is intended for work with large or 
small data bases by means of highly individualized languages. The 
architecture of REL is based on theoretical assumptions about 
human information dynamics \[I\], among them the expanding 
process of conceptualization in working with data, and the idio- 
syncratic language use of the individual workers. The result of 
these assumptions is a system which allows the construction of 
highly individualized languages which are closely knit with the 
structure of the data and which can be rapidly extended and 
augmented with new concepts and structures through a facile 
definitional capability. The REL language processor is designed 
to accommodate a variety of languages whose structural charaCter- 
istics may be considerably divergent. 
The REL English is one of the languages within the REL 
system. It is intended to facilitate sophisticated work with com- 
puters without the need for mastering programming languages. 
I 
The structural power of REL English matches the extremely 
flexible organization of data in ring forms. Extensions of the 
basic REL English language can be achieved either through 
",. __ 
defining new concepts and structures in terms of the existing ones 
or through addition of new rules. 
The REL dialect and idiolects 
English is our primary mode of verbal communication, 
therefore everyone has the right to know what someone else means 
by it. We use the term "English" in its most ordinary sense, i.e. 
we bear in mind the fact that there really is no one English lan- 
guage. Rather, the tern~ English refers to as many idiolects as 
there are speakers, these idiolects being grouped into dialects. 
The REL English is one such dialect. It shares with natural lan- 
guage also the characteristic of being, in its design and function- 
ing, a conglomerate of idiolects, which we call versions. 
Thompson's design philosophy of REL \[Z\] defines the theoretical 
basis for the assumption of individual, idiolectal approach to the 
use of information. 
REL English as a formal language 
The second basic characteristic is that REL English is a 
formal language. The characteristics of English as a formal 
language are discussed in an earlier paper \[3\]. The 
central thesis of that paper is that English becomes a formal 
language when the subject matter which it talks about is limited to 
material whose interrelationships are specifiable ina limited 
number of precisely structured categories. It is the type of 
2 
structuration of the subject matter and not the nature of the subject 
matter itself that produces the necessary limitations. Natural 
language encompasses a multitude of formal languages and it is the 
complexities of the memory structures on which natural language 
can and does operate that account for the complexities, flexibility 
and richness of natural language. These latter give rise to the 
notorious problem of ambiguities in natural language analysis. 
Arnbiguities 
What about ambiguities in REL English? The purpose of I~EL 
English grammar is to provide a language facilitating work with 
computers. It is thus assumed that the language is used for a 
specific purpose in a specific context. Allowance for ambiguities at 
the phrase level, with subsequent disambiguition through context, is 
a powerful mechanism in a language. It is this aspect of ambiguity 
we wish to include. 
Ambiguities, in the general case and in our case, are due to 
different semantic interpretations (data structuration) arising from 
different deep structures. Ambiguous constructions are of two 
main types- (1) those which are structurally ambiguous, e.g. , 
'Boston ships" is ambiguous over all relations existing between 
"Boston" and "ships" (built in Boston, with home port in Boston, 
etc. ); and (Z) those which are semantically ambiguous, e.g. , 
~location of King" if "King" can refer both to Captain King and the 
3 
destroyer King in the data elements. Ambiguities of the first type 
can be resolved by the specification of the relation, those of the 
second type by inclusion of larger context. Chomsky's well-known 
example of an ambiguous sentence "Flying planes can be dangerous" 
is of the first type; Katz and Fodor's "bachelor" is of the second 
type. The purpose of REL sentence analysis is not to find all 
possible interpretations of ambiguous sentences irrespective of 
context. Rather, the purpose is maximal disambiguation where 
such disambiguation is possible in terms of semantic interpreta- 
tion, providing for the preservation of ambiguities present in 
memory structures if the syntactic form of the query is ambiguous. 
Nature of restrictions 
How does our English compare with English as discussed by 
modern linguists? On the level of surface structure, they are 
essentially the same. Some more complex transformationally 
derived strings, such as certain forms of elipsis are not handled as 
yet. However, most of the common forms are treated in a straight- 
forward manner. Although some constructions which can be formed 
in natural conversational English are not provided in the basic 
English package, such deficiencies can to a large extent be over- 
come by the capability for definitional extension provided by the 
system. 
The level of deep structure presents more problems. As 
4 
distinct from surface structure, deep structure is that level of 
syntactic analysis which constitutes the input to semantic analysis, 
both in Chomsky's \[4\] terms and ours. What is the nature of this 
semantic interpretation? 
In the general case, little is known. In our case, as in most 
types of computer analysis, interpretation is in terms of the internal 
forms of organization of the data in memory. To the extent that the 
constituents of deep structure can be directly correlated with 
corresponding structures in the data, semantic analysis, and there- 
fore sentence analysis, can be carried to completion. 
It is important to distinguish, in this regard, between two 
quite distinct though related ways in which language use can be 
restricted. The first is by the ways in which the data is organized, 
that is the structural forms used and the interlinkages which are 
formed for the manipulation of these structures. This type we will 
call "structural" restrictions. The second is by restrictions of the 
subject matter, or the universe of discourse; this we will call "dis- 
course" restrictions. When one restricts the universe of discourse 
to a body of material which is naturally formal or has been formal- 
ized, one often tacitly accepts the structural restrictions thus im- 
I 
posed. To the uninitiated, it may appear that it is the discouse 
limitations and not the implied structural limitations that make the 
material amenable to machine analysis. However, it is the estab- 
5 
lishment of relatabilit¥ between deep structural constituents and 
data structural forms, rather than discourse restrictions that make 
computer processing of the semantic component possible. Any con- 
tent area whose data is organized into these given structural forms 
can be equally efficiently processed by a system establishing such 
inter r elationship s. 
The restrictions on REL English are a function of the first 
type of restrictions, i.e., structural restrictions. Not all deep 
structures found in natural English are brought out by our 
analysis, because constituents of these deep structures do not 
correspond to structural relations in the organization of our data. 
For instance, "collections of boys" and "boys' collections" are con- 
sidered synonyn~ous, although they are not in English. Consider: 
"At the fair, I saw collections of boys. " and "At the fair, I saw 
boys' collections. " 
Finally, a limitation in reverse, as it were, is the fact that 
we have emphasized the inclusion of grammatical strings rather 
than attending to strict insistence on grammaticallty. 
Organization of the I%EL English grammars 
The REL English grammar consists of a syntactic component 
and a semantic component. The syntactic component consists of a 
We are indebted to Norton Greenfeld for many valuable insights 
and comments as well as for the excellent programming of a large 
portion of the current English rules. 
6 
set of rewrite rules, context-free and general, which build the deep 
structure Phrase-markers in the form of kernel sentences, and a 
number of transformationai rules. The semantic component con- 
sists of semantic transformation rules acting on the memory struc- 
tures of the data bases. The language processor, described in 
detail in the accompanying paper \[5\], builds a sentence analysis 
Phrase-marker. The sentence is interpreted by successive appli- 
cation of the corresponding semantic transformations in accordance 
with the parsing. The application of semantic rules to the consti- 
tuents of a complex phrase produces the interpretation of the phrase 
or, if the phrase is semantically meaningless, an indication of that 
to the language processor. 
The parts of speech of REL English are given in Figure I, 
together with examples. These parts of speech are inclusive terms 
for syntactic classes (labels on the parsing tree), and semantic 
categories (memory structures). 
Function words, e. g. , al__!l, o__ff, wha_.___tt, are distinct from 
referent words; the former are empty in the sense of not being 
associated with memory structures. Other aspects of the grarnrnar, 
namely features, name and relation modification, verbs, clauses, 
I 
quantifiers and conjunctions, are discussed in detail in the remairder 
of the paper. 
II. REL Data Structures 
Structural Power of Language and Structural Power of Data 
Organization 
The case of English as a programming language against other 
programming languages needs a defense. There are obvious justi- 
fications. For a user for whom the computer is only a tool facili- 
tating interaction with his data, English, being his natural mode of 
language communication, is a medium allowing maximum concen- 
tration on the problem itself by minimizing the need for concern 
with the medium. The flexibility of natural-like language augmented 
by the extensibility of such a language provides a means for facile 
manipulation of existing data structures and for new concept forma- 
tion. We believe that a deeper justification lies in the relationship 
between language structure and data structuration. There must 
exist a correspondence between the power of language and the power 
of data organization. Given the following extreme alternatives: 
1) highly 
formatted 
data 
2) powerfully 
structured 
data 
1) formatted desirable 
language 
2) powerfully 
structured undesirable 
language 
the 1) - 1) and 2) - 2) combinations are optimal. 
undesirable 
desirable 
The use of a flexible, 
rich language for work with highly formatted data introduces too 
many alternatives and ambiguities which may actually lower effi- 
ciency. Conversely, the use of a formatted language for work with 
powerfully structured data precludes full and efficient exploitation 
of the organization of the data by imposing a restricted medium of 
access. 
Rin G Structure of REL English Data Bases 
Since effective communication requires the maximization of 
information, the structuration of the process must be a compromise 
between the communicating entities -- in our case, the human user 
and the computer. Ring structures are a very flexible medium, 
suitable for the organization of data on which natural language 
operates and formalized enough to be amenable to manipulation by 
computer programs. \[6\] 
The semantic component of REL English, mentioned above, 
consists of the interpretation part of the rules of grammar. 
interpretation rules constitute checks on the ring structures. 
take as our data base the following environment: 
l) 
Z) 
3) 
4) 
These 
Let us 
There are three ships: l~aru, Pinta and Nina. 
The respective locations of these ships are: Boston, 
Boston and New York. 
Boston, New York and Chicago are cities. 
The respective populations of these cities are 3205, 11366 
and 6689, in thousands. 
Figures Z and 3 illustrate how ring structures contain this environ- 
ment. Let us follow these rings. Links from the SHIP ring lead to 
Maru, Pinta and Nina. The arrow at intersection points on the SHIP 
ring pointing down and on the Maru, Pinta and Nina pointing up 
indicate the class-member relationship: Pinta is a member of the 
class SHIP. This exemplifies ring structures which relate classes 
of objects and individual objects. The other type of ring structure 
are binary relations between rings. If we follow now the link from 
the Pinta ring through the location ring, we reach the Boston ring. 
The relation of location holds between Pinta and Boston, thus, in 
predicate notation, we can say: location (Pinta, Boston). The 
symbols 0, 1 and 2 refer to the order in this relationship. 
Number relations, such as population, relate a ring to a 
number: population (Boston, 3205). They exist as links within the 
respective rings to which they refer. However, the numerical data 
itself is in the ring to which it applies. Thus one finds a referent to 
population on the Boston ring; and associated with the referent to 
population is the data entry. 
The structural methods used to handle the time aspects of time- 
related data are not discussed in this paper for reasons of space. 
Ring structures are also built during analysis of sentences and 
undone on their completion. Thus in the processing of the sentence: 
l0 
What are the locations of ships? 
the semantic transformation associated with the 'N -~ R of N' rule 
will build a scratch ring of all locations of ships, in the particular 
case at hand, Boston and New York. This scratch ring will be inter- 
related to the data base in the same manner as permanent rings and 
thus will participate in further analysis in a straightforward way. 
Upon completion of the analysis and response to the given sentence, 
the links to this scratch ring and the scratch ring itself will be 
deleted from other rings of the data base. 
A second type of scratch ring is a number ring, that is a ring 
all of whose entries are numerical data. \]For example, the phrase: 
'populations of cities' will result in the creation of such a number 
ring containing the numbers 3205, 11366, 6689. The syntactic 
result of 'population of cities' has part of speech U, and the number 
ring feature (discussed below) designates the character of the under- 
lying structure. 
Rules such as 'U -~ average U', or 'U-~ sum of U' apply to 
number rings. New data can be entered simply from the console, 
for example that the Santa Maria is a ship located in Boston: 
def: Santa Maria: = name I 
The Santa Maria is a ship'. 
The location of the Santa Maria is Boston~ 
This will result in the creation of a ring for Santa Maria and a 
11 
linking ring between that ring, location and Boston. 
Features and Their Functions 
Features constitute an important and powerful mechanism in 
REL. We use the term in the general Cbomsky sense \[4\], but with 
some significant modifications. Some aspects of the operation of 
features from the point of view of the language processor are dis- 
cussed in \[5\]. Summarily, the role of features is subcategorization, 
determination of syntactic constructions and order of syntactic 
g r ouping s. 
(a) There are only two semantic features in the present REL 
English. The tense character of the verb is a feature assigned to the 
individual lexical item. The feature distinguishing a number ring 
from a number is assigned to a data structure which in a sense is 
also treated as a lexical item in sentence analysis. 
Co) Morphemic features subcategorize syntactically parts of 
speech. These include the Nominative/Possessive and Singular/ 
Plural subcategorizations of Nouns and Relations; and the Singular 
and Participle subeategorizations of Verbs. Morphemic features 
often function asprecedence markers as well. Thus checks on 
Nora./Poss. and Sing./Plural features force the following parsing: 
(((small (computer s))') (word size)) 
excluding such parsings as: 
(small (((computer s)') (word size))) 
12 
(c) The passive voice feature of verbs is set by the verb "to be" 
used as an auxilary. Once set, it signals other routines to appropri- 
ately handle subject and object, essentially keying the passive trans- 
formation. The negation feature on the verb is set by "not" and its 
contracted forms. 
(d) Precedence features. The remaining features determine 
the applicability of rules and the order of their application to form 
syntactic groupings. On N and R they are: 
determiner -- which prevents further modification 
e.g. the the ball, big the ball, are excluded. 
phrasal -- which prevents the application of certain 
morphemic rules to a syntactically modified phrase. 
pre-modification and post-modification -- which cause 
the following constituent analysis: 
e.g. (the (dentist (son of (Sally Smith)))) 
not: (((the dentist) son) of (Sally Smith)) 
They also are used to maintain parallel construction 
in conjunctive phrases. 
e.g. ((JohnWs son) and (Mary's daughter)) 
not: John's (son and (Mary's daughter)) 
while: (Mary's (uncles and aunts)) 
not: ((Mary's uncles) and aunts) 
On the V phrases, right modification and subject features play a 
13 
similar role. Auxilaries must go on before right modification; right 
modification must be completed before the subject can go on. 
e.g. (John (((will marry) Joan) before 1970)) 
not: ((John (will (marry Joan))) before 1970) 
The importance of precedence features is that they sharply 
curtail redundant parsings. Thus the rules: 
N -~ Ns N -~ NN 
N -~ N's N -~ the N 
give seven parsings of the phrase 'the boy's dogs'. The feature 
checks eliminate all but one, namely: (the ((boy's) (dogs))). Since 
these feature checks are made by the parser at the earliest possible 
moment, they provide a very efficient and effective control on 
redundanc y. 
14 
f . 
III. Building of Linguistic Structures 
Name Modification 
A "name" refers to a class or the individual members of a 
class in the ring structured data base, a "relation" to a binary 
predicate. 
Name modification constructions consist of a name as head 
and one or more modifiers. Modifiers are: determiners, relations, 
and other names. 
The indefinite and definite articles comprise the first class. 
Their functions are complex, and an adequate discussion would go 
beyond the scope of this paper. 
Relations can function as pre-modifiers and post-modifiers. 
Accordingly, there are two different syntactic constructions, which 
we treat in REL as synonymous: "location of John" and "John's 
location". 
Figures 4 and 5 illustrate the application of such rules. 
Relation Modification 
We distinguish between two types of relations, primitive and 
complex. A primitive R directly designates an associated ring 
structure. A complex R designates a list structure consisting of 
constructions which build a complex relationship out of primitive 
ones. The process of building this list structure is referred to as 
15 
relation modification. Figure 6 shows the possible constructions, 
together with grammar rules for relation modification. Semantic 
transformations for name modification rules recursively resolve 
this list structure. For example, in the analysis of the phrase: 
John's male child's location 
"Male child's location" gives rise to the cornplex R: 
composition \[modification \[male, child\], location\]. 
Then the name modification rule is applied to: "John's R", whose 
semantic transformation expands the complex R, developing the 
location of each "male (child of John)". 
Relation modification plays an important role in definitions 
of complex concepts. It is employed widely in elevating lanEuages 
beyond the base level of primitive relationships. This can be 
exemplified in family relationship situations. For instance, given 
the "parent" and "identity 9' relations as primitive, the following 
series of definitions introduces the relation "uncle": 
def:child:converse of parent 
def:son:male child 
clef:sibling:child of parent and not identity 
def:brother :male sibling 
def:uncle:brother of parent 
Relation modification also plays an important role in building 
complex definitions involving number relations such as area, GNP, 
salary, etc. Consider the definition: 
16 
def:per capita "GNP":"GNP"/population 
Complex relations of this sort function in such expressions as 
"Average per capita area of nations". 
The Verb 
We are accustomed to the traditional definition saying that a 
verb denotes an action or a state: an action is performed by an actor 
(subject) on an object, a state is a momentarily or permanently 
frozen action between subjects and objects. Thus, an action, in this 
inclusive sense, is characterized by (i) the aspect of beginning, 
ending, duration or momentariness; (Z) by its situation in time, and 
(3) by referring to subjects and objects. Two groups of verbs may 
be distinguished: those referring to a relation between subjects and 
objects, and those which establish a connection between them. The 
relation expressed by a verb constitutes (and is here referred to as) 
its 'predicate'; such verbs are called relation verbs. Relation verbs 
also express temporal aspects of a relation. Typical relation verbs 
are "arrive" and "leave"; both refer to the relation of 'location'; 
"arrive" refers to the beginning of the existence of this relation, 
and "leave" to its ending. For instance, "John left Boston" means 
that the relation of 'location' existing between John and Boston came 
to an end. Verbs which express a connection between subjects and 
objects are referred to as copulas, e.g. "is" in "John is a boy". 
The copula itself constitutes the predicate. 
17 
1. Relation Verbs 
Each of the four following sentences contains, in surface 
structure, a different verb. 
(1) John arrived in Boston. 
(ii) John left Boston. 
(iii) John lived in Boston. 
(iv) John is residing in Boston. 
The underlying structure of these sentences is identical: 'location 
(John, Boston)', except for the temporal aspect of this relation: 
beginning in (i), ending in (ii), momentariness in (iii) and duration 
in (iv). In REL English, the temporal aspect is denoted by the 
'tense character' feature. 
Verbs are introduced through language extension. They are 
defined in terms of a relation and a tense character. The relation 
must denote an already existing ring structure. Thus, given the 
relation of "location", the verb "arrive" is defined by: 
def:arrive:verb (location, 1). 
A verb is internally represented as a "verb table", with part 
of speech V. In the verb table for "arrive", 'location' is entered 
as predicate and '1' as tense character, indicating beginning. 
Initially, the verb table is assigned the present tense, or 'now- 
hess'. This can he subsequently modified by verb morphology, 
18 
auxiliary verbs, and tense modifiers (temporal phrases or clauses). 
Z. Copula verbs 
Copula verbs have a verb table which has a copula predicate 
and (at present) no tense character. The copulas are: "is", "are", 
"was", "were", and their contracted negatives. For example, the 
rule 'C -* wasn't' will result in the creation of a verb table with a 
copula predicate, time equal to past, and the feature of negation. 
3. Verb table modification 
The elements of a verb table correspond to the elements of a 
kernel clause (i. e. one with a single deep structure Phrase-marker). 
They are: subject, predicate, object, tense character, and times 
tl, t 2. The subjects and objects of the kernel may be N (names) and 
U (numbers}. The predicate is a relation. The types of the tense 
character are discussed above. The times on a verb table indicate 
a point or interval within the span bounded by past (0) and future (~). 
The t 1 and t 2 either both specify a single point or bind an interval of 
time in which the event indicated by the clause takes place. 
REL verbs are marked by three regular inflectional mor- 
phemes: past tense, past participle, and 3rd person singular. 
The function of inflectional morphemes and auxiliary verbs is two- 
fold: modification of the original time in the verb table and setting 
of syntactic features. For example, the past tense morpheme and 
the auxiliary "did" modify t 1 to be past, thus establishing the time 
19 
interval '0 to now' (i. e. t 1 = 0, t 2 = now); the auxiliary "will" modi- 
fies t 2 to be future, thus establishing t 1 = now, t 2 = = ; 3rd person 
singular and auxiliaries "has" and "does" set the singular feature. 
Time modifiers (M) further modify the verb table. They origi- 
nate from prepositional time phrases, e.g. "before 1960 ~v, and sub- 
ordinate clauses, e.g. "before John arrived in Boston N. The times 
of the original verb table (or as modified through the past morpheme 
and/or auxiliaries) are further modified so as to be in accord with 
time modifiers. Thus, in the sentence "Will John arrive before 
19707 ,t the verb table for "arrive" has tl, t 2 - now; modification 
by "will" changes t 2 to '~'; modification by "before 1970 '~ changes 
t 2 to '1970', thus resulting in the intersected time interval 'between 
now and 1970'. 
Subjects and objects are inserted in the verb table upon appli- 
cation of the rules: 
(i) V -* N V (rule putting subject on) 
(ii) V -* V N (rule putting object on) 
(iii) V -* VbyN. 
These rules also check whether the passive feature is on (set by 
copulas forming passives), and if it is, rule (i) converts the N into 
an object; rule (ii) converts the N into a subject; rule (iii) applies 
only if the passive feature is on and converts the N into a subject. 
Number subjects and objects do not participate in the passive trans- 
20 
formation. 
Negation is handled by a feature on the verb table. It is set by 
rules processing "not" with copulas, both in isolation and contracted, 
and rules processing "not" with verbs. They differ syntactically in 
the position of "not". If the negative feature is already set by a pre- 
vious rule, the result is positive (i. e. double negatives turn into a 
positive). 
Let us consider an example of verb table accumulation (figure 
7). The development of the verb table shows the successive modifi- 
cation due to the verb rules which have applied. The parsings reflect 
the use of features which eliminate some possible parsings which are 
incorrect or irrelevant to the analysis, and determine the order of 
parsing. 
Clause Processing 
The accumulated verb table constitutes the input to the clause 
processing routine. The verb table may be either complete, i.e. 
have all its positions filled, or else have subjects or objects missing. 
The clause processor completes the verb table if necessary, and 
returns it together with a list of times for which the clause holds, 
and another list of times for which it does no~ hold. The results of 
the clause processor are subsequently used by the sentence rules to 
produce output, or by the subordinate clause rules. 
21 
Sentences are clauses plus sentence delimiters -- in REL 
English, these are initial capital letters, and terminal question mark 
or exclamation mark. Accordingly, we distinguish two types of 
sentences, questions which interrogate the data, and commands 
which modify it. 
The rules for handling questions are: (S stands for sentence, 
K for capital letter) 
(i) S -* K V? Did Stan marry Jill before 1950? 
(ii) S -* K what/who V? Who are the Smiths' children? 
(iii) S -- K M? When did John arrive? 
Rule (i) results in a yes/no output depending on whether the relation 
holds at the specified time. Suppose the verb table is given us 
predicate spouse 
subj e ct Stan 
obj e ct Jill 
tense character begin 
t 1 0 
t 2 1950 
Suppose also the data show that Stan married Jill in 1948. Since 1948 
is within the time interval '0 to 1950', the answer will be affirmative 
If the input is a negative question, we use the time for which the 
relation does not hold. 
The output of rule (ii) is one or more Ns. These are supplied 
by the clause processing routine as either subjects or objects. In 
example (ii), subjects are supplied. 
ZZ 
The output of rule (iii) is a time (or time list) at which 
the relation indicated in the verb table holds. 
The output may be ambiguous. For example, "Did Smith live 
in New York? " would result in both "yes" and "no" as ambiguous 
output if "Smith" referred to one Smith who did and another who did 
not live in New York. "Who is Stan's sister? " would have ambiguous 
answers if Stan had more than one sister. 
The output is a Vacuous Description if there is no valid seman- 
tic analysis of the query. For example in the sentence, "What is the 
income of the present king of France? " the phrase "present king of 
France" is a vacuous description in terms of our data if the data 
contains factual information on present-day France. This aspect of 
the analysis is not explicated here for lack of space. 
The second type of sentences, commands, are used for adding 
or deleting data. The rules and corresponding examples are: 
(i) S -~ K V~ John is a boy~ 
Sally lived in Boston after 1950! 
(ii) S -~ K R of V~ The location of Sally was Boston 
after 1950'. 
$ 
The income of John is 10000~ 
The data is deleted if the verb table has the negative feature set. 
There are two restrictions on the above rules: the subject of the 
23 
verb table in (i) must not be modified; and the predicate of the verb 
table in (ii) must be a copula. 
The structure of the input sentence determines the structural 
relations to be established between items in the data. 
Subordinate clauses modify some item in another clause, or 
another clause as a whole. In REL English, clauses of the first type 
are always relative clauses; they are introduced by the pronouns 
"who", "which", "that", "whom" and "whose". Clauses modifying 
other clauses are temporal clauses introduced by "before", "after" 
and "when", and result in time modification. 
Examples of relative clauses: 
(i) N -~ N who V Did the boy who left Boston marry 
Jill? 
(ii) N -* N that V Was Boston the city that John left? 
(iii) N -* N whose R V Did the boy whose father left Boston 
marry Jill? 
Verb tables representing relative clauses are completed using 
the noun which the clause modifies. Thus in (i) the N is used as the 
subject of the verb table, in (ii) either as subject or object. In (iii) 
the N for which the relation R holds is used as subject or object. 
Parallel to the above rules are rules with a comma preceding 
the relative pronoun. However, the rules with commas apply only 
24 
to post-modified NXs and the comma plays a disambiguating function. 
For instance, "parents of boys who left Boston" can be ambiguous; 
in our English, the relative clause refers to "boys", while in 
"parents of boys, who left Boston" the relative clause refers to 
"parents ". 
Examples of temporal clauses: 
(i) M -~ before V Did John marry Jill before Jill left 
Boston? 
(ii) M "~ when V What was John's income when Stan lived 
in Boston? 
The output of rule (i) is a time modifier with the time interval t 1 = 0, 
t 2 = date specified by the subordinate clause. In the subsequent 
analysis these times are compared with the times on the main clause 
in the same way as time modifiers such as "before 1960." For 
instance, let us assume that John married Jill in 1950 and that Jill 
left Boston in 1955. The output of rule (i) will now be the interval 
'0 to 1955 ~ and the times to be intersected are '0 to 1955' and 1950: 
o .... .......... ..... • 
The result is, obviously, a "yes" answer. The output of rule (ii) is 
a tense modifier with the time interval t 1 - t 2 - date specified by the 
subordinate clause. 
Quantifie r s 
By "quantifiers" we refer to "all", "some", "how many", 
"what", "each" and "no". Phrases containing such words are of 
i 
25 
particular importance since they often are used in referring to 
aggregates and more abstract concepts. 
Some examples are: 
Did some Smith who lived in New York move to Boston? 
How many Smiths have an income greater than 12000? 
What was the average between 1950 and 1960 of the incomes of 
each Smith? 
In such sentences, a class of objects is considered. The 
sentence constitutes a condition on each element of this class, the 
condition depending on the particular quantifier. Consider in detail 
the example: "Is Boston the location of all men? " To answer this 
question, each man must be considered in turn. We apply the term 
generation to this kind of a process. As each man is generated in 
turn, say man., the sentence "Is Boston the location of man.? " 
must be processed. If the answer to all cases is "yes", then the 
original question is answered affirmatively; otherwise, it is 
answered negatively. In the case of the "some '~ generator, the 
answer would be affirmative if at least one of the men lives in 
Boston. For the question "What men live in Boston? ,i the answer 
is the list of men for whom the clause "man. lives in Boston" holds. 1 
Several generators can be nested in a sentence. An example 
is: ~'Each Smith lived in what cities? " In general, the nesting of 
generators poses difficult and challenging problems. Although we 
26 
have been able to satisfactorily deal with these problems, an 
adequate treatment must be deferred to a separate publication because 
of their complexity. 
Conjunctions 
Conjunction of R's was discussed above under relation modifi- 
cation. Conjunction of N's and V's set up generators according to the 
following rules : 
NGE -~ NJ N 
NGEJ -~ N, NJ 
VGE -~ V JN 
VGEJ -~ V, VJ 
where the GE subscript indicates a generated phrase. If the J-phrase 
is "and", the generator is an "all" generator; if the J-phrase is "or", 
the generator is a "some" generator. An example is given in figure 8. 
27 
......................................................................................... ~ ...................... 
IV. A Few Inclusive Examples 
>~hat was the average betwe-en 196h and now of the incomes of 
eac~ woman who has not married? 
........... Sd~-Smi~l~ .............................. ~ ......................................................... i ..... 
Sally Smith 10,892.3 c 
............ >~ach--Jone~-liVe~-i-h-~Qh~C-CitY-whe~-~iII-~6n~-~-FiVe~ ......................... 
in Los Angeles? 
~ary $~a-#tin Jones Boston 
Jack Jones Boston 
......... ~ iiI 'dories Smi th ............ L6S AG~eies .................................................... 
dohn Jones Boston 
........... >~i d-S taft- Sm i tll 'mar ry -d\[\] i-J86 ~ -ana-i-f ~-T6- L~-~6ggle% ? ..................... 
Yes 
>l~hat were the incomes of Steve Smith's parents after Steve 
Smith arrived? .. : ............................................................... 
...... dill doriesSmith - " h,b-O0 August up to December, 1962 
......... 0 from 195.3._.t!.p_ t_o_.d_une__ I_9.69 ........................... 
............ Si:anS6~ith 8,000 AuE.ust up to December, 1962 
9,000 in 1963 
10,000 from 196h up to 1967 
. 11,000 from.1967..L~p...t;o _dune..\]:._9_fi9 ................................ 
->When Was tile number 0f Siliiths equal to 5? 
AMBIGUOUS OUTPUT: 
................ (i-) .... AU~dg t-1938- - to-Sei~t e,hbe r- ~i95G ........................................................... 
(2) September 1958 to l.'~ay 1960 
................. >t~l~ ~ n--a i d- e E61~--Jd~gs-\]- i-~6--i h--E~l-~- c i t y ? 
John Jones Boston 1930 to June 1959 
....... di \] \] Jones Smith .......... New York ................................. SePte.n~-Ser i-9-5-5--t~o-A-l~r -~ 
Los Angeles dune 1961 to dune \]95 (. 
..................... - ............... Bostoti ................................ ~J-Liiy i937--t~o--K6~,tist i (- 
Jack Jones Boston 1935 to June 1969 
..... ~.~ie~-- I~6~ tl ii- -d6~i~-E .......... i~i5 s t o n -i-9 3-0--t-6- Ji~-5-6 1969 
.......... >4ef :.i n the.._"19hO"s ~.be!we_en..'_?_1_9_h()!'._ a.gd _lQ..y_ea is _a_i~t_e.r__-!_9_ 90_ '_'_ .............. 
DEFINED. 
>What was Stan Smith's income in the 1960s? 
.......... 6,000 iil 1960 ........................................................... ~ .................................. 
8,000 from 1961 up to 1963 
"--" ~-,-'~'0-0"- TS- ~ 
I0,000 from I96h up to 1967 
.............. 11, 000 f toni "196"7- d p "to" ;J-6h-6 -19-69 ....................................................... 
>_ 
................................................................................. ............................................. 
Z8 
..................................................... 7 ................................................. ' " 
\[1\] 
\[z\] 
\[3\] 
.i 
Symbol 
N 
R 
V 
C 
J 
T 
M 
U 
S 
Name 
name 
relation 
verb table 
copula 
conjunction 
time 
time modifier 
numb e r 
sentence 
Example s 
boy, John, the location of John 
location, father, age 
arrive, has lived 
is, ~vas 
and, or 
January 1960 
before January 1960 
between now and 1970 
5Z, 61.34 
John is a boy! 
Is John a boy? 
Figure 1 Parts of Speech 
t- 
O 
./" i"-.__) / " ~\ 
/ \x '/ 
/ /¢J ,,, I 
c L.-....Z.,- ~/" ~ .,..E v ~ \ ~ w 
m ! -- ~ >z r I.~ t o~ I o \ "~ ~k " ~ ~( "" -",t' 
" / \\ \ ",7/ 
/ ~, \ \ // 
2 I 0"% ~ ~ ~) ~ ~, , -/o,'~~,,\ 
\ / " ~ I -\ \ / \1 \_\ 
\ \ // \ \ ~ ~ ///,// 
% \ / I ,,~, /~ \ X I / / / 
\\ \ I /I /, _/~ 
.: 
t',,1 
f,., 
\ 
 l:,i I ~ ~1 ~ ,,~ 
I I°° I ~.~ ~-~ 
o 
0 
C~ 
u 
0 
u 
II) 
r~ 
i 
0 
.o ~ t~ o 
~o9 g. 
• 2 ~ m m ~ 
,.~ O 
O ~ 
o a 
U U 
2 2 
N N 
N 
~g 
0 ~0 
2 
O 
0 • u ~ 
s~ 
~ g 
O 
U 
N 
0 ~o 
O 
U 
O ,--i 
N 
! 
i .t 
0 
,.0 
o 
0 
0 ..0 
o 
U o 
,.0 
"~ o-. 
o 
0 
0 u 
k ~ h 
o 
N 
m ~ 
~ 0 
,'~ O 
~ U 
Z 
l 
Q~ 
k 
k o 
o 
i 
~ Z 
o .~ 
0 ~ 
.~~ 
0 
.a 
u 
o 
0 ~ 
~ ~ .~.~ 
o ~ ~ 
~~ ~ ~ ~.~ 
0 
o 
o~ 
0 0 
~ 0 0 40 
0 0 
k 
U 
k 
u 
U o U 
Z Z 
Z 
o 
composition 
conjunction 
disjunction 
modification 
relative negation 
converse 
R~ RR 
R~ R JR 
R~ NR 
R~ Rbut not R 
R~converse of R 
son's location 
uncles and aunts 
uncles or aunts 
boy friend 
uncles but not aunts 
converse of parent 
P 
v 
(for number relations) 
sum 
difference 
product 
quotient 
R~R+ R 
R.~R - R 
R~R ~; R 
R --R/R 
direct salaries plus overhead 
pay rate * hours worked 
GNP/population 
Figure 6 Complex Relation Constructions 
V 
V 
11 ! 
I 9 %/ 
%/ 
! 71 V 
F 5.I ~J ¥ N 
w- r ~-~ 
,o 4 Sue will have divorced John before 1970 
Verb Table 
predicate 
subject 
object 
tense character 
t 1 
t z 
participle 
feature / 
spouse 
end 
nOW 
now 
off 
5 
spouse 
end 
0 
now 
on 
6 
spouse 
end 
0 
now 
off 
7 
spouse 
end 
0 
off 
8 
spouse 
John 
end 
0 
off 
9 
spouse 
J obn 
end 
0 
1970 
off 
II 
spouse 
Sue 
John 
end 
0 
1970 
off 
Figure 7 Verb Table Accumulation 
(generator resolution) 
~'C-E" 
%~E (nested all and some generators) 
! 
NGE (some generator) 
IV N 
v N Y N N r "i 
~_.~_,, ~_~_, ~ ,--~ ~ , ., .-----, 
Stan lived in Boston and married the daughter of Jill or the aunt of Mary? 
I 
Figure 8 Example of Conjunctions 

REFERENCES 

Thompson, F.B. , "The Dynamics of Information, " 
.Quarterly Review (in press). 

Thompson, F. B, , et al. , "REL: A Rapidly Extensible Language 
System, " Proc. National ACNI Conf. , August 1969 (in press). 

Thompson, F. B. , "English for the Computer, " Proc. AFIPS 
Fall Joint Comp. Conf., Z9 (1966), 349-356. 

Chomsky, N., Aspects of the Theor~of Syntax, B/lIT Press, 1965. 

Lockemann, P.C., Thompson, F. B., REL: A Rapidly Exten- 
sible System, I: The REL Language Processor. 

Craig, J. A., Berezner, S. C., Carney, H. C., Longyear, C. 
R. , "DEACON: Direct English Access and Control, " Proc. 
AFIPS Fall Joint Comp. Conf., 29 (1966), 365-380. 
