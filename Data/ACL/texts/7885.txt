References

1   Nello Cristianini , John Shawe-Taylor, An introduction to support Vector Machines: and other kernel-based learning methods, Cambridge University Press, New York, NY, 1999 

2   Taku Kudoh , Yuji Matsumoto, Use of support vector learning for chunk identification, Proceedings of the 2nd workshop on Learning language in logic and the 4th conference on Computational natural language learning, September 13-14, 2000, Lisbon, Portugal 

3   Taku Kudoh. 2000. Tinysvm: Support vector machines. http://cl.aist-nara.ac.jp/ takuku//software/Tiny SVM/index.html. 

4   Sadao Kurohashi and Makoto Nagao, 1998. Japanese Morphological Analysis System JUMAN version 3.5. Department of Informatics, Kyoto University. (in Japanese). 

5   Masaki Murata, Qing Ma, Kiyotaka Uchimoto, and Hitoshi Isahara. 1999. An example-based approach to Japanese-to-English translation of tense, aspect, and modality. In TMI '99, pages 66--76. 

6   Masaki Murata , Kiyotaka Uchimoto , Qing Ma , Hitoshi Isahara, Bunsetsu identification using category-exclusive rules, Proceedings of the 18th conference on Computational linguistics, p.565-571, July 31-August 04, 2000, Saarbrücken, Germany 

7   Masaki Murata, Masao Utiyama, Kiyotaka Uchimoto, Qing Ma, and Hitoshi Isahara. 2001. Correction of the modality corpus for machine translation based on machine-learning method. 7th Annual Meeting of the Association for Natural Language Processing. (in Japanese; the English translation of this paper is available at http://arXiv.org/abs/cs/0105001). 

8   Eric Sven Ristad. 1997. Maximum Entropy Modeling for Natural Language. ACL/EACL Tutorial Program, Madrid. 

9   Eric Sven Ristad. 1998. Maximum Entropy Modeling Toolkit, Release 1.6 beta. http://www.mnemonic.com/software/memt. 

10   Satoshi Sekine, The domain dependence of parsing, Proceedings of the fifth conference on Applied natural language processing, p.96-102, March 31-April 03, 1997, Washington, DC 

11   Hirotoshi Taira and Masahiko Haruno. 2000. Feature selection in svm text categorization. Transactions of Information Processing Society of Japan, 41(4):1113--1123. (in Japanese). 
