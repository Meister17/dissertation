References

1   Steve Abney. 1996. Partial Parsing via Finite-State Cascades. In: Proceedings of the ESSLLI'96 Robust Parsing Workshop. Prague, Czech Republic. 

2   DOM. 1998. Document Object Model (DOM) Level 1. Specification Version 1.0. W3C Recommendation. http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001 

3   Claire Grover, Colin Matheson, Andrei Mikheev and Marc Moens. 2000. LT TTT - A Flexible To-kenisation Tool. In: Proceedings of Second International Conference on Language Resources and Evaluation (LREC 2000). 

4   K. Koskenniemi. 1983. Two-level Model for Morphological Analysis. In: Proceedings of IJCAI-83 , pages: 683--685, Karlsruhe, Germany. 

5   Kiril Simov, Zdravko Peev, Milen Kouylekov, Alexander Simov, Marin Dimitrov, Atanas Kiryakov. 2001. CLaRK - an XML-based System for Corpora Development. In: Proc. of the Corpus Linguistics 2001 Conference, pages: 558--560. Lancaster, UK. 

6   Text Encoding Initiative. 1997. Guidelines for Electronic Text Encoding and Interchange. Sperberg-McQueen C. M., Burnard L (eds). 

7   Corpus Encoding Standard. 2001. XCES: Corpus Encoding Standard for XML. Vassar College, New York, USA. http://www.cs.vassar.edu/XCES/ 

8   XML. 2000. Extensible Markup Language (XML) 1.0 (Second Edition). W3C Recommendation. http://www.w3.org/TR/REC-xml 

9   XML Schema. 2001. XML Schema Part 1: Structures. W3C Recommendation. http://www.w3.org/TR/xmlschema-1/ 

10   XPath. 1999. XML Path Lamguage (XPath) version 1.0. W3C Recommendation. http://www.w3.org/TR/xpath 

11   XSLT. 1999. XSL Transformations (XSLT) version 1.0. W3C Recommendation. http://www.w3.org/TR/xslt 
