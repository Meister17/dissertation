TEAM: A TRANSPORTABLE NATURAL-LANGUAGE INTERFACE SYSTEM
 
B a r b a r a J. G r o s z Artificial Intelligence Center SRI I n t e r n a t i 
o n a l Menlo P a r k , CA 94025
 
A.
 
Overview
 
A major benefit of u s i n g n a t u r a l language to the i n f o r m a t i o n 
in a database is that it shifts o n t o t h e system t h e b u r d e n of m e d 
i a t i n g b e t w e e n two v i e w s o f t h e d a t a : t h e way i n which 
t h e d a t a i s s t o r e d ( t h e " d a t a b a s e v i e w " ) , and t h e 
way i n which an e n d - u s e r thinks about it (the "user*s view"). Database 
information is recorded in terms of files, r e c o r d s , and fields, while 
natural-language expressions refer t o the same information i n terms of 
entities and relationships in the world. A major problem in constructing a 
natural-language interface is determining how to encode and use the information 
needed to bridge these two views. Current natural-language interface systems 
require extensive efforts by specialists in natural-language processing to p r o 
v i d e them w i t h t h e i n f o r m a t i o n t h e y need t o do the 
bridging. The systems are, in effect, handtallored to provide access to 
particular databases.
 access
 
how Co o b t a i n t h e information requested. Moving s u c h s y s t e m s to 
a new d a t a b a s e r e q u i r e s c a r e f u l handcrafting that involves d 
e t a i l e d knowledge o f such things ae p a r s i n g p r o c e d u r e s , t 
h e p a r t i c u l a r way i n which domain i n f o r m a t i o n i s stored, 
and data-access procedures. To provide for transportability, TEAM s e p a r a t 
e s i n f o r m a t i o n a b o u t language, about the domain, and a b o u t 
the database. The d e c i s i o n t o p r o v i d e t r a n s p o r t a b i l i 
t y to existing conventional databases (which d i s t i n g u i s h e s TEAM 
from CHAT [ W a r r e n , 1981]) means that t h e d a t a b a s e c a n n o t be 
r e s t r u c t u r e d t o make t h e way i n w h i c h i t s t o r e s d a t a 
more c o m p a t i b l e w i t h t h e way i n which a u s e r may a s k a b o u 
t t h e data. A l t h o u g h many p r o b l e m s can be a v o i d e d i f one 
i s a l l o w e d t o d e s i g n t h e d a t a b a s e a s w e l l a s the 
natural-language system, given the prevalence of existing conventional 
databases, approaches w h i c h make t h i s assumption are likely t o have 
limited applicability in the near-term.
 
This paper f o c u s e s on the p r o b l e m of constructing transportable 
natural-language interfaces, i . e . , s y s t e m s t h a t can be a d a p t e 
d t o p r o v i d e a c c e s s t o d a t a b a s e s f o r which t h e y were 
not specifically handtailored. It describes an initial version of a 
transportable system, called TEAM (for ~ransportable E_ngllsh A_ccess Data 
manager). The hypothesis underlying the research described in this paper is that 
the i n f o r m a t i o n required for the adaptation can be obtained through an 
Lnteractlve dialogue with database management personnel who are not familiar 
with natural-language processing techniques.
 
The TEAM s y s t e m h a s three major components: ( 1 ) an a c q u Z s t t i o 
n component, ( 2 ) t h e DIALOGIC language system [Grosz, et al., 1982], and (3) 
a data-access ccaponent. Section C descrlbes how the language and data-access 
components were designed to accommodate the needs of transportability. S e c t i 
o o D d e s c r i b e s the d e s i g n of the acquisition component to allow 
flexible interaction ~rlth a database expert and discusses acquisition problems 
caused by the differences between the database view and user view. Section E 
shows how end-user queries are interpreted after an acquisition has been 
completed. Section F describes the current state of development of TEAM and 
lists several problems currently under investigation.
 
B.
 
I s s u e s of T r a n s p o r t a b i l i t y
 C. System Design
 
The insistence on transportability distinguishes TEAM from previous systems such 
as LADDER [Hendrlx ec al., [978] LUNAR [Woods, Kaplan, and Webber, 1972], PLANES 
[Waltz, 1975], REL [Thompson, [975], and has affected ~he design of the 
natural-language processln~ system in several ways. Most previously built 
naturallanguage interface systems have used techniques that make them inherently 
difficult to transfer to new domains and databases. The internal representations 
[n these systems typically intermix (in their data structures and procedures) 
information about language with information about the domain and the database. 
In addition, in Interpretln~ a query, the systems conflate what a user is 
requesting (what hls query "means") with 39
 
I n TEAM, t h e t r a n s l a t i o n o f an E n g l i s h q u e r y into a 
database query takes place in two s t e p s . First, the DIALOGIC system 
constructs a representation of the literal meaning or "logical form" of the 
query [Moore, 1981]. Second, the data-access component translates the logical 
form into a formal database query. Each of these steps requires a combination of 
some information that is dependent on the domain or the database wlth some 
information that is not. To provide for transportability, the TEAM system 
carefully separates these two kinds of information.
 
fiI.
 
Domain- and Database-Dependent Information
 
To adapt TEAM to a new database three kinds of information must be acquired: 
information about words, about concepts, and about the structure of the 
database. The data structures that encode this information--and the language 
processing and data-access procedures that use them--are designed to allow for 
acquiring new information automatically. Information about words, lexlcal 
information, includes the syntactic properties of the words that will be used in 
querying the database and semantic information about the kind of concept t o 
which a particular word refers. TEAM records the lexlcal information specific to 
a given domain in a lexicon. Conceptual information includes information about 
taxonomic relationships, about the kinds of objects that can serve as arguments 
to a predicate, and a b o u t t h e k i n d s o f p r o p e r t i e s an object 
can have. I n TEAM, t h e internal representation of information about the 
entities in the domain of discourse and the relationships that can hold among 
them is provided by a conceptual schema. This schema includes a sort hierarchy 
encoding the taxonomic relationships among objects in the domain, information 
about constraints on arguments to predicates, and information about 
relationships among certain types of predicates. database schema encodes 
information about how concepts in the conceptual schena map onto the structures 
of a particular database. In particular, it links conceptual-schema 
representations of entities and relationships in the domain to their realization 
in a particular database. TEAM currently assumes a relational database with a 
number of f i l e s . (No languageprocesslng-related problems are entailed in 
moving TEAM to other database models.) Each file is about some kind of object 
(e.g., employees, students, ships, processor chips); the fields of the file 
record properties of the object (e.g., department, age, length). A
 
To provide access to the informa=,on in a particular database, each of the 
components of DIALOG~C must access domain-speciflc information about the words 
and concepts relevant to that database. The information required by the 
syntactic rules is found in the lexicon. Information required by the semantic 
and pragmatic rules is found in the lexicon or the conceptual schema. The rules 
themselves however do not include such domain-dependent information and 
therefore do not need to be changed for different databases. In a similar 
manner, the data-access component separates general rules for translating 
logical forms into database queries from information about a particular 
database. The rules access information i n the conceptual and database schemata 
to interpret queries for a particular database.
 
D.
 
Acquisition
 
TEAM i s d e s i g n e d t o i n t e r a c t w i t h two k i n d s o f u s e r s 
: a d a t a b a s e e x p e r t (DBE) and an e n d - u s e r . The DBE provides 
information about the files and fields in the database through a system-dlrected 
acquisition dialogue. As a result of this dlaloEue, the language-processlng and 
data-access components are extended so that the end-user may query the new 
database in natural-language.
 
i.
 
Acquisition Questions
 
Because the DBE is assumed to be familiar with database structures, but not with 
language-processlng techniques, the acquisition dialogue is oriented around 
database structures. That is, the questions are about the kinds of things in the 
files and fields of the database, rather than about lexlcal entries, sort 
hierarchies, and predicates. The disparity between the database view of the data 
and the end-user's view make the acquisition process nontrlvlal. For instance, 
consider a database of information about students in a university. From the 
perspective of an enduser "sophomore" refers to a subset of all of the students, 
those who are in their second year at the university. The fact that a particular 
student is a sophomore might be recorded in the database in a number of ways, 
including: (l) in a separate file containing information about the sophomore 
students; (2) by a special value in a symbolic field (e.g., a CLASS field [n 
which the value SOPH indicates "sophomore"); (3) by a "true" value in a Boolean 
field (e.g., a * in an [S-$O?H field). For natural-language querying to be 
useful, the end-user must be protected from having to know which type of 
representation was chosen. The questions posed to the DBE for each kind of 
database construct must be sufficient to allow DIALOGIC to handle approximately 
the same range of
 
Domain-lndependent
 
Information
 
The language executive [Grosz, e t a l . , 1982; Walker, 1978|, DIALOGIC, 
coordinates syntactic, semantic, and basic pragmatic rules in translating an 
English query into logical form. DIALOGIC's syntactic rules provide a general 
grammar of English [Robinson, 1982]. A semantic "translation" rule associated 
with each syntactic phrase rule specifies how the constituents of the phrase are 
to be interpreted. Basic pragmatic functions take local context into account in 
providing the interpretation of such things as noun-noun combinations. DIALOGIC 
also includes a quantlfler-scoping algorithm.
 
filinguistic expressions (e.g., for referring to "students i n t h e sophomore c 
l a s s ' ) r e g a r d l e s s o f the particular database implementation 
chosen. In all c a s e s , TEAM w i l l c r e a t e a l e x i c a l e n t r y f 
o r " s o p h o m o r e " and an e n t r y i n t h e c o n c e p t u a l schema 
to represent the concept of sophomores. The database attachment for thls concept 
will depend on t h e p a r t i c u l a r d a t a b a s e s t r u c t u r e , as 
w i l l the kinds of predicates f o r which i t can be an argument.
 
Example of Acquisition Queeclons
 
I n d e s i g n i n g TEAM we f o u n d i t i m p o r t a n t to distinguish 
three differanc kinds of fields N arlthmeCic, feature (Boolean), and s y m b o l 
l c - - o n the b a s i s of t h e r a n g e of l i n g u i s t i c expressions 
to which each gives r i s e . AriChmetic fields contain numeric values on which 
comparisons and computations llke averaging are likely to be done. (Fields 
containing dates a r e n o t y e t h a n d l e d by TEAM.) Feature fields 
contain true/false values w h i c h r e c o r d w h e t h e r o r n o t some a t 
t r i b u t e i s a property of the object d e s c r i b e d by t h e file. 
Symbolic f i e l d s typically contain values that c o r r e s p o n d to n o u 
n s o r a d j e c t i v e s t h a t d e n o t e t h e s u b t y p e s o f t h e 
domain d e n o t e d by t h e f i e l d . D i f f e r e n t a c q u i s i t i o 
n q u e s t i o n s a r e asked f o r each type of field. These are illustrated 
in the example i n S e c t i o n D.3.
 
To illustrate the acquisition of information, consider a database, called CHIP, 
containing information about processor chips. In particular, the fields in this 
database contain the following information: the identification number o f a c h 
i p ( I D ) , its m a n u f a c t u r e r (MAKER) its width i n b i t s (WIDTH), 
ice speed in m e g a h e r t z (SPEED), its cost i n d o l l a r s (PRICE), the 
kind of technology (FAMILY), and a flag indicating wheCher o r noc t h e r e is 
an e x p o r t l i c e n s e f o r t h e c h i p (EXP). In the figures discussed 
below, the DBE's r e s p o n s e is indicated in uppercase. For many quesClone 
the DBE is presented wlch a llst of options from which ha can choose. For these 
questions, the complete llst is shown and the answer indicated in boldface. F i 
g u r e i shows t h e short-form of the questions asked about the file itself. 
In r e s p o n s e to q u e s t i o n ( 1 ) , t h e DBE t e l l s TEAM w h a t 
fields are in the file. Responses to the r e m a i n i n g quesCloms allow TEAM 
t o identify t h e kind of object the file contains information about (2), types 
of linguistic expressions used to refer to It [ (6) and (7)], how to identify 
individual objects in the database (4), and how to s p e c i f y i n d i v i d u 
a l o b j e c t s to the u s e r ( 5 ) . These responses result in the words 
"chip" and " p r o c e s s o r " b e i n g added t o t h e l e x i c o n , a new 
s o r t added to the taxonomy (providing the interpretation f o r t h e s e w o 
r d s ) , and a l i n k made i n t h e d a t a b a s e schema b e t w e e n t h 
i s sort and records i n the file CHIP.
 Figure 2 gives the short-form of the most central questions asked about symbolic 
fields, using the field MAKER (chip manufacturers) as exemplar. These questions 
are used to determine the kinds of properties represented, how t h e s e r e l a 
t e t o p r o p e r t i e s i n o t h e r f i e l d s , and the k i n d s of 
linguistic expressions the field values can give rise to. Question (4) allows 
TEAM to determine that individual field values refer to manufacturers rather 
than chips. The long-form of Q u e s t i o n (7) i s : Will you want to ask, for 
example, "How many MOTOROLA processors are there?" to get a count of the number 
of PROCESSORS with CHIP-MAKER-MOTOROLA? Question (8) expands to: Will you want 
to ask, for example, "How many HOTOROLAS are there?" to get a count of the 
number of PROCESSORS with CHIP-MAKER-MOTOROLA?
 
Acquisition
 
Strategy
 
The ~ a J o r features of the s tra te gy developed for acquiring information 
about a database from a DBE include: (1) providiu E multiple levels of detail 
for each question posed to the DBE; (2) allowing a DBE to review previous 
answers and change them; and (3) checking for legal answers.
 At present, TEAM initially presents the DBE wlth the short-form of a quesclou. A 
more detailed version ("long-form') of the question, including examples 
illustratlng different kinds of responses, can be requested by the DBE. An 
obvious excenslon to this strategy would be to present different Inltial levels 
t o different users ( d e p e n d i n g , f o r e x a m p l e , on t h e i r p r 
e v i o u s experience wlth the system).
 
A c q u i s i t i o n I s e a s i e r i f e a c h new p i e c e of information 
is immediately i n t e g r a t e d into the u n d e r l y i n g knowledge s t r 
u c t u r e s o f t h e p r o g r a m . 8 o w e v e r , we a l s o wanted Co a l 
l o w t h e DSE t o change a n s w e r s to p r e v i o u s q u e s t i o n s ( 
t h i s has t u r n e d o u t to be an essential feature of TEAM). Some 
questions (e.g., those about irregular plural forms and synonyms) affect only a 
single part of TEAM (the lexicon). Other questions (e.g., those about feature 
fields) affect all components of the system. Because of the complex interaction 
between acquisition questions and components of the system to be updated, 
immediate integration of new information is not possible. As a result, updating 
of the lexicon, conceptual schema, and database schema Is not done until an 
acqulsition dialogue is completed.
 
In t h i s ease, t h e a n s w e r to q u e s t i o n ( 7 ) I s " y e s " and to 
q u e s t i o n ( 8 ) " n o " ; the field has v a l u e s that can be used as 
explicit, but not implicit, classifiers. Contrast this wlth a symbolic field in 
a file about students that contains the class of a student; in this case the 
answer to both
 
fiauesclons would be affirmative because, for example, the phrases "sophomore 
woman" and "sophomores" can be used to refer to refer to STUDENTS with 
CLASS=SOPHOMORE. In other cases, the values may serve neither as explicit nor as 
implicit classifiers. For example, one cannot say *"the shoe employees" or *"the 
shoes" to mean "employees in the SHOE department". For both questions (7) and 
(8) a positive answer i s the default. It i s i m p o r t a n t to allow the 
user to override thls default, because TEAM must be able to avoid spurious 
ambiguities (e.g., where two fields have identical field values, but where the 
values can be classifiers for only one field.). Following acquisition of this 
field, lexical entries are made for "maker" and any synonyms supplied by the 
user. Again a new s o n is created. It i s marked a s h a v i n g v a l u e s t 
h a t can be explicit, b u t not implicit, classifiers. Later, when the actual 
connection to the database is made, individual field values (e.g., "Motorola") 
will be made individual instances of this new sort. Figure (3) presents the 
questions asked about arithmetic fields, using the PRICE field as exemplar. 
Because dates, measures, and count quantities are all handled differently, TEAM 
must first determine which kind of arithmetic object is in the field (2). In 
this case we have a unit of "worth" (6) measured in "dollars" (4). Questions (8) 
and (9) supply information needed for interpreting expressions Involvlng 
comparatives (e.g., "What chips are more expensive than the Z8080?") and 
superlatives (e--~7, "What is the cheapest chip?"). Figure 4 gives the expanded 
version of these questions. As a result of thls acquisition, a new subsort of 
the (measure) sort WORTH i s added to the taxonomy for PRICE, and is noted as 
measured in dollars. In addition, lexlcal entries are created for adjectives 
indicating positive ("expensive") and negative ("cheap") degrees of price and 
are linked to a binary predicate that relates a chip to its price. Feature 
fields are the most difficult fields to handle. They represent a single 
(arbitrary) property of an entity, with values that indicate whether or not the 
entity has the property, and they give rise to a wide range of linguistic 
expresslons--adJectlvals, nouns, phrases. The short-form of the questions asked 
about feature fields are given in Figure 5, using the field EXP; the value YES 
indicates there is an export license for a given processor, and NO indicates 
there is not. Figures 6, 7, and 8 give the expanded form of questions (4), (6), 
and (B) respectively. The expanded form illustrates the kinds of end-user 
queries that TEAM can handle after the DBE has answered these questions (see 
also Figure 9). Providing thls kind of illustration has turned out to be 
essential for getting these questions answered correctly.
 
Each of these types of expression leads to new lexlcal, conceptual schema, and 
database schema entries. I n general in the conceptual schema, feature field 
adJectlvals and abstract nouns result in the creation of new predicates (see 
Section E for an example); count nouns result in the creation of new subsorts of 
the file subject sort. The database schema contains informatlon about which 
field to access and what field value is required. TEAM also includes a limlted 
capability for acqulrln8 verbs. At present, only transitive verbs can be 
acquired. One of the arguments to the predicate cozTespondlng to a verb must be 
of the same sort as the file subject. The other argument must correspond to the 
sort of one of the fields. For the CHIP database, the DBE could specify that the 
verb "make" (and/or "manufacture") takes a CHIP as one argument and a MAKER as 
the second argument.
 
E.
 
Sample Q u e r i e s and T h e i r
 
[nterpretatlons
 
After the DBE has completed an acquisition session for a file, TEAM can 
interpret and respond Co end-user queries. Figure 9 lists some sample end-user 
queries for the file illustrated in the previous section. The role of the 
different kinds of informatlon acquired above can be seen by considering the 
logical forms produced for several queries and the database attachments for the 
sorts and predicates that appear in them. The following examples illustrate the 
information acquired for the three different fields described in the preceding 
section. Given the query, What are the Motorola chips? DIALOGIC produces the 
following logical form: (Query (WHAT tl (THING tl) (THE p2 (AND (PROCESSOR p2) 
(MAKER-OF p2 MOTOROLA)) (EQ p2 tl)))) where WHAT and THE are quantifiers; 1 tl 
and p2 are variables; AND and EQ have their usual interpretation. The predicates 
PROCESSOR and MAKER-OF and the constant MOTOROLA were created as a result of 
acquisition. The schema: PROCESSOR: MAKER-OF: following information in the 
database
 
1 Because the current version of DIALOGIC takes no account of the 
slngular/plural distinction, the uniqueness presupposition normally associated 
with "the" is not enforced. 42
 
fii s u s e d , a l o n g with s o r ~ h i e r a r c h y i n f o r m a t i o n i 
n the conceptual schema, t o g e n e r a t e the actual database query. 
Similarly, t h e e n d - u s e r query chips?
 
new acqulslClon component allows t h e user more flexibility i n answering 
questions and provides a wider range of default answers. TEAM c u r r e n t l y 
h a n d l e s m u l t i p l e files and provides transportability to a l i m i t 
e d r a n g e o f databases. As menCloned previously, a relational database 
model is assumed. Currently, TEAM also assumes all files are In third normal 
form. The acquisition of verbs is limited Co allowing t h e DBE Co s p e c i f y 
t r a n s I C l v e v e r b s , as described in S e c t i o n D.3. We a r e c u 
r r e n t l y excending TEAM t o
 
What a r e t h e e x p o r t a b l e would l e a d to t h e l o g i c a l form:
 
( Q u e r y (WHAT t l (THING cl) (THE p2 (AND (PROCESSOR p2)
 
where EXP-POS is a predlcace created by acquisIClon; it is true if its argumanC 
is exportable. In thls case the relevant database scheme information I s : 
PROCESSOR: EXP-POS: file-CHIP keyfleld-[D file-CHIP fleld-EXP fieldvalue-T
 
(I)
 
Provide for interpretation of expressions involving such things as mass terms, 
aggregates, quantified c o a m a n d s , and commands t h a c r e q u i r e t h 
e s y s t e m Co p e r f o r m f u n c t i o n s o t h e r t h a n q u e r y i n 
g che d a t a b a s e . Provide for efficient p r o c e s s i n g of the m o s t 
common f o r m s o f c o n j u n c t i o n . Generalize the verb acquisition p r 
o c e d u r e s and e x t e n d TEAM t o h a n d l e more complex verbs, 
including such Chings as verbs wlth mulClple delineations, verbs chat require 
special prepositions, and verbs that allow senCenclel complements. Handle d a t 
a b a s e s encoding time-related information and e x t e n d DIALOGIC to handle 
expressions involving clme and tense.
 
Finally, co illustrate how TEAM h a n d l e s arithmetic f i e l d s , and I n p 
a r t i c u l a r the use of comparatives, consider the query: What c h i p i s 
c h e a p e r chart 5 d o l l a r s ?
 
The l o g i c a l
 
form f o r Chin q u e r y I s
 
( Q u e r y (WHAT pl (PROCESSOR pl) ((MORE C ~ A P ) pl (DOLLAH 5)))) The 
conceptual schema encodes the relationship between the predicates CHEAP and 
PRICE-OF (again, both concepts created as a result of acquisition), wlCh t h e 
following information CHEAP: measure-predlcate-PRICE-OF scale-negative
 
G.
 
Acknowledgments
 
The d e v e l o p m e n t of TEAM has involved the efforts of many people. Doug 
Appelc, Armar Archbold, Bob Moore, Jerry Hobbs, Paul Marcln, Pernando Pereira, 
Jane Robinson, Daniel Sagalowicz, and David Warren have made ~ a J o r 
contributions. This research was supported by the Defense Advanced Research 
Projects Agency with the Naval Electronic Systems Command under Contract 
N0003980-'<:-0645. The views and conclusions contained in Chin document are 
chose of the author and should not be interpreted as representative of the 
official policies, either expressed or implied, of the Defense Advanced Research 
Projects Agency or the United States Government.
 
And the relevant database schema Informaclon is: PROCESSOR: PRICE-OF: file-CHIP 
keyfield-[D flit-CHIP field(argl)=[D fleld(arg2)-PRICE
 
F.
 
Status and Future Research
 
An initial version of TEAM was implemented in a combination of Incerlisp 
(acquisition and DIALOGIC components) and Prolog (data access component) on the 
DEC2060, but address space llmicatlons made continued development difficult. 
Current research on TEAM is being done on the Symbolics LISP machine. The 
acquisition component has been redesigned co cake advantage of capabilities 
provided by che blcmap display. The 43
 
File name- C H ~ (1} Fields - (ID MAKER WIDTH SPEED PRICE FAMILY EXP) (2) 
Subject - P R O C E S S O R (31 Synonyms for P R O C E S S O R - C H I P (4} 
Primazy key - ID {5} Identifyingfields- M A K E R ID (8) Can one say W h o are 
the P R O C E S S O R S ? - Y E S N O (7) Pronouns for filesubject - H E S H E 
IT T H E Y (8) Field containing the name of each file subject - ID Figure 1: 
Questions About File
 
Field - P R I C E ( 1) Type of field SYMBOLIC A R I T l t M E T I C FEATURE (2) 
Value t y p e . DATES M E A S U R E S COUNTS [3) Are the units implicit? Y E S N 
O (4) Enter implicit unit - DOLLAR (5) Abbreviation for this unit.~ (6) Measure 
type of this trait - TIME WEIGHT SPEED VOLUME LINEAR AREA W O R T H OTHER {7) 
Minimum and maximum numeric valucs- (1,100) (8} Positive adjectives - (EXPENSIVE 
COSTLY) (9) Negative adjective - (CHEAP) Figure 3: Questions for Arithmetic 
Field P R I C E
 
Please specify any adjectives that can be used in their comparative or 
superlative form to indicate how much each P R O C E S S O R is in a 
positivedirectionon the scale measured by the values of CHIP-PRICE. In a file 
about machine-tools with a numeric field called PRICE, one could ask: How E X P 
E N S I V E is each tool? to mean What is the price of each tool.~ EXPENSIVE, 
COSTLY, AND (HIGH PRICED) ~re positive adjectives designating the upper range of 
the PRICE scale.
 C H E A P and (LOW PRICED), which designate the lower range of the PRICE scale, 
are negative adjectives.
 
Field - M A K E R ( I ) Type of field - S Y M B O L I C ARITHMETIC FEATURE (2) 
.Axe field values units of measure? YES N O (3} Noun subvategory - P R O P E R 
COUNT MASS (4} Domain of field value's reference - SUBJECT F I E L D (5) Can you 
say W h o is the C H I P - M A K E R t Y E S N O (6) Typical value - M O R T O R 
O L A (7) Will values of this field be used as cia~sifers.~ E S N O Y {8) Will 
the values in this field be used alone as implicit classifiers? YES N O Figure 
2: Questions for Symbolic Field M A K E R
 
Please enter any such adjectives you will want to ~ querying the database. 
Figure 4: Expanded Version of Adjective Questions (Arithmetic Field}
 
in
 
Field - E X P (I) Type of field - SYMBOLIC ARITHMETIC F E A T U R E (2) Positive 
value - YES (3) Negative value - NO (4) Positive adjectives - EXPORTABLE (5) 
Negative adjectives - UNEXPORTABLE (6) Positive abstraA't nouns - EXPORT 
AUTHORIZATION (7) Negative abstract no~.1 (8) Pmitive common nouns (9) Negative 
common nouns Figure 5: Questions for Feature Field ]gXP
 
List any count nous~ ammciated with positive field value YES. In general, this 
is any word wwww such that you might want to u k : What PROCESSORS are wwww-s! 
to mean What PROCESSORS have a CHIP-EXP of YES? For example, in a file about 
EMPLOYEEs with � feature field CITIZEN having a positive field value Y and n e ~ 
t i v e field value N, you might want to aek: Which employees are citizens? 
instead of Which employees have a CITIZEN of Y? Figure 8: Feature Field Count 
Nouns
 
What adjectivab are aasoeiated with the field values YES in this field? In 
general these are word.5 wwww such that you might want to Mk: Which PROCESSORS 
are www~'
 
Which PROCESSORS have � CHIP-EXP of YES! For example, in s medical file about 
PATIENTs with a feature field IMM having a positive field value Y and a negative 
filed value N, you might want to ask: Which patients are IMMUNE (or RESISTANT, 
PROTECTED)! Figure 6: Feature Field Adjectivals
 
~,Vhat 8 bit chips are cheaper than the fastest exportable chip made by Zilogt 
Who makes the fastest exportable N M O $ chip costing less than 10 dollars! By 
whom is the most expensive chip reader Who b the cheapest exportable chip made 
by! Who is the most expensive chip made? What is the fastest exportable chip 
that Motorola makes? What 16 bit chips does Zilog make? Who makes the fastest 
exportable N M O S chip? Who makes the faatest exportable chip.~ Does Zilog make 
a chip that is faster than every chip that Intel makes? Are there any 8 bit 
Ziiog chipe? is some exportable chip faster than 12 mhz? Is every Ziiog chip 
that is f ~ t e r than 5 mhz exportable? How faat is the faate~t exportable 
chip? How expensive is the f~stest ~'~MOS chipt Figure 9: Sample questions for 
CHIP databaae
 
List any abstrart nouns ~k~tociated with the positive feature value YES. In 
general this is any word wwww such that you might want to ask a question of the 
form: Which PROCESSORS hove wwww?
 tO m e a n
 
Which PROCESSORS have CHIP-EXP of YES! For example, in a medical databaae about 
PATIENTs with a feature field IMM having a positive field value Y and a negative 
field value N, you might want to a~k: ~,Vhich patients have IMMUNITY? instead of 
Which patients have aa IMM of Y? Figure 7: Feature Field Abstract Nouns
 

REFERENCES
 
Grosz, B. et al . [1982] "DIALOGIC: A Core Natural Language Processing System," Proceedings of the Ninth International Conference on Computational Linguistics, Prague, Czechoslovakia (July 1982).
 
Moore, R. C. [1981] "Problems in Logical Form," in Proceedings of the 19th Annual Meeting of the Association for Computaional Linguistics, pp. 117-L24. The Association for Computaional Linguistics, SRI International, Menlo Park, Californla (June 1981)..
 
Waltz, D. [1975] "Natural Language Access to a Large Data Base: An Engineering Approach," Proc. 4th International Joint Conference on Artificial Intelligence, Tbillsl, USSR, pp. 868-872 (September 1975). 

Warren, D . R . [1981] "Efficient Processing of Interactive Relational Database Queries Expressed in Logic," Proc. Seventh International Conference on Very Large DataBases, Cannes, France, pp. 2"'2~-2--~',
 
Robinson, J. [1982] "DIAGRAM: A Grammar for Dialogues," Communications of the ACM, Vol. 25, No. 1, pp. 27-47 (January 1982).
 
Thompson, g . B . and Thompson, B . H . [1975] "Practical Natural Language Processing: The REL System as Prototype," H. Rubinoff and M. C. Yovlts, eds., pp. 109-168, Advances in Computers 13, Academic Press, New York, (New York 1975). 

Woods, W. A., R. M. Kaplan, and B. N-Nebber [I972] "The Lunar Sciences Natural Language Information System," BBN Report 2378, Bolt Beranek and Newman, Cambridge, Massachusetts (1972).
 
Walker, D. E. (ed.) [1978] Understanding Spoken Language, Elsevier North-Hollam~, New York, New York, (1978).
