Calibrating Features For Semantic Role Labeling
by Nianwen Xue and Martha Palmer

References
C. Baker, C. Fillmore, and J. Lowe. 1998. The berkeley framenet project. In Proceedings of COLING-ACL, Singapore.
Xavier Carreras and Lluis Marquez. 2004. Introduction to the CoNLL-2004 Shared Task: Semantic Role Labeling. In Proceedings of CoNLL 2004.
E. Charniak. 2001. Immediate-head Parsing for Language Models. In ACL-01.
John Chen and Owen Rambow. 2003. Use of Deep Linguistic Features for the Recognition and Labeling of Semantic Arguments. In Proceedings of the 2003 Conference on Empirical Methods in Natural Language Processing, Sapporo, Japan.
Michael Collins. 1999. Head-driven Statistical Models for Natural Language Parsing. Ph.D. thesis, University of Pennsylvania.
Dan Gildea and Julia Hockenmaier. 2003. Identifying Semantic Roles Using Combinatory Categorial Grammar. In EMNLP-03, Sapporo, Japan.
D. Gildea and D. Jurafsky. 2002. Automatic labeling for semantic roles. Computational Linguistics, 28(3):245{288.
Dan Gildea and Martha Palmer. 2002. The Necessity of Parsing for Predicate Argument Recognition. In Proceedings of the 40th Meeting of the Association for Computational Linguistics, Philadelphia, PA.
Paul Kingsbury and Martha Palmer. 2002. From Treebank to PropBank. In Proceedings of the 3rd International Conference on Language Resources and Evaluation (LREC2002), Las Palmas, Spain.
M. Marcus, B. Santorini, and M. A. Marcinkiewicz. 1993. Building a Large Annotated Corpus of English: the Penn Treebank. Computational Linguistics.
Mitchell Marcus, Grace Kim, Mary Ann Marcinkiewicz, et al. 1994. The Penn Treebank: Annotating Predicate Argument Structure. In Proc of ARPA speech and Natural language workshop.
Martha Palmer, Dan Gildea, and Paul Kingsbury. submitted. The proposition bank: An annotated corpus of semantic roles. Computational Linguistics.

Sameer Pradhan, Kadri Hacioglu, WayneWard, James H. Martin, and Daniel Jurafsky. 2003. Semantic Role Parsing: Adding Semantic Structure to Unstructured Text. In Proceedings of the International Conference on Data Mining (ICDM-2003).
Sameer Pradhan,WayneWard, Kadri Hacioglu, James H. Martin, and Daniel Jurafsky. 2004. Shallow Semantic Parsing Using Support Vector Machines. In Proceedings of NAACL-HLT 2004, Boston, Mass.
Honglin Sun and Daniel Jurafsky. 2004. Shallow semantic parsing of chinese. In Proceedings of NAACL 2004, Boston, USA.
Nianwen Xue and Martha Palmer. 2003. Annotating the Propositions in the Penn Chinese Treebank. In The Proceedings of the 2nd SIGHAN Workshop on Chinese Language Processing, Sapporo, Japan.
