References

E. Brill and R. C. Moore. 2000. An improved error model for noisy channel spelling correction. ACL.

P. R. Clarkson and R. Rosenfeld. 1997. Statistical language modeling using the CMU-Cambridge toolkit. Proc. of ESCA Eurospeech.

Fred J. Damerau. 1964. A technique for computer detection and correction of spelling errors. Communications of the ACM.

EAGLES-I. 1996. Final Report. In Evaluation of NLP Systems.

David Graff. 2003. The New York Times Newswire Service. English Gigaword LDC-2003T05.

Donald E. Knuth. 1981. Sorting and Searching. volume 2 of The Art of Computer Programming. Addison-Wesley.

Karen Kukich. 1992. Techniques for automatically correcting words in text. ACM Computing surveys.

V. I. Levenshtein. 1965. Binary codes capable of correcting deletions, insertions, and reversals. In Cybernetics and Control Theory.

D. Lewis and Y. Yang and T. G. Rose and F. Li. 2003. RCV1: a new benchmark collection for text categorization research. Journal of Machine Learning Research.

Martin Reynaert. 2004. Text induced spelling correction. COLING.

George Kingsley Zipf. 1935. The psycho-biology of language: an introduction to dynamic phililogy. MIT Press.
