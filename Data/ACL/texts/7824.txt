SPONSORS:
CLASS (http://www.class-tech.org)
ACL 2001 (http://www.irit.fr/ACTIVITES/EQ ILPL/aclWeb/acl2001.html)
ELRA (http://www.icp.grenet.fr/ELRA/home.html)
ELSNET (http://www.elsnet.org)
WORKSHOP ORGANIZATION COMMITTEE:
David G. Novick
Department of Computer Science
University of Texas at El Paso
El Paso, TX 79968, USA
Phone: +1 915-747-6952
novick@cs.utep.edu
http://www.cs.utep.edu/novick
Joseph Mariani
Limsi - CNRS
Bˆatiment 508 Universit´e Paris XI
BP 133 - 91403 ORSAY Cedex - France
Fax: +33 (0)1 69 85 80 88
mariani@limsi.fr
http://www.limsi.fr/Individu/mariani
Candy Kamm
AT&T Labs
180 Park, Bldg 103
Florham Park, NJ 07932, USA
+1 973-360-8540
cak@research.att.com
http://www.research.att.com/info/cak
Patrick Paroubek
Spoken Language Processing Group / Human-Machine Communication Department
Limsi - CNRS
Bˆatiment 508 Universit´e Paris XI
BP 133 - 91403 ORSAY Cedex - France
Fax: +33 (0)1 69 85 80 88
Phone: +33 (0)1 69 85 81 91
pap@limsi.fr
http://www.limsi.fr/Individu/pap
Nils Dahlb¨ack
Computer & Information Science Department
Link¨oping University
S-581 83 Link¨oping Sweden
Phone: +46 13 28 16 64
nilda@ida.liu.se
http://www.ida.liu.se/˜nilda/
Frankie James
RIACS Mail Stop 19-39
NASA Ames Research Center
Moffett Field, CA 94035, USA
Phone: +1 650-604-0197
fjames@riacs.edu
http://www-pcd.stanford.edu/frankie/
Karen Ward
Department of Computer Science
University of Texas at El Paso
El Paso, TX 79968 USA
Phone: +1 915-747-6957
kward@cs.utep.edu
http://www.cs.utep.edu/kward
SCIENTIFIC COMMITTEE:
David G. Novick
Joseph Mariani
Candy Kamm
Patrick Paroubek
Nils Dahlb¨ack
Frankie James
Karen Ward
Christian Jacquemin
Niels Ole Bernsen
Stephane Chaudiron
Khalid Choukri
Martin Rajman
Robert Gaizauskas
Donna Harman
Jose Pardo
Oliviero Stock
Said Tazi
