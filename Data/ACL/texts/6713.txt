References

K. J. Lee and Y. S. Hwang and H. C. Rim. 2003. Two-phase biomedical NE recognition based on SVMs. ACL workshop on natural language processing in biomedicine.

T. Brants. 2000. TnT - a statistical part-of-speech tagger. ANLP.

L. Ramshaw and M. Marcus. 1995. Text chunk using transformation-based learning. ACL workshop on VLC.
