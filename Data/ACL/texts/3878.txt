Wrapping of Trees
James Rogers
Department of Computer Science
Earlham College
Richmond, IN 47374, USA
jrogers@cs.earlham.edu
Abstract
We explore the descriptive power, in terms of syn-
tactic phenomena, of a formalism that extends Tree-
Adjoining Grammar (TAG) by adding a fourth level
of hierarchical decomposition to the three levels
TAG already employs. While extending the descrip-
tive power minimally, the additional level of decom-
position allows us to obtain a uniform account of a
range of phenomena that has heretofore been dif-
ficult to encompass, an account that employs uni-
tary elementary structures and eschews synchro-
nized derivation operations, and which is, in many
respects, closer to the spirit of the intuitions under-
lying TAG-based linguistic theory than previously
considered extensions to TAG.
1 Introduction
Tree-Adjoining Grammar (TAG) (Joshi and Sch-
abes, 1997; Joshi et al., 1975) is a grammar formal-
ism which comes with a well-developed theory of
natural language syntax (Frank, 2002; Frank, 1992;
Kroch and Joshi, 1985). There are, however, a num-
ber of constructions, many in the core of language,
which present difficulties for the linguistic under-
pinnings of TAG systems, although not necessarily
for the implemented systems themselves. Most of
these involve the combining of trees in ways that are
more complicated than the simple embedding pro-
vided by the tree-adjunction operation.
The most widely studied way of addressing these
constructions within TAG-based linguistic theory
(Kroch and Joshi, 1987; Kroch, 1989; Frank, 2002)
has been to assume some sort of multi-component
adjoining (MCTAG (Weir, 1988)), in which elemen-
tary structures are factored into sets of trees that
are adjoined simultaneously at multiple points. De-
pending on the restrictions placed on where this ad-
joining can occur the effect of such extensions range
from no increase in complexity of either the licensed
tree sets or the computational complexity of pars-
ing, to substantial increases in both. In this paper
we explore these issues within the framework of an
extension of TAG that is conservative in the sense
that it preserves the unitary nature of the elemen-
tary structures and of the adjunction operation and
extends the descriptive power minimally.
While the paper is organized around particular
syntactic phenomena, it is not a study of syntax
itself. We make no attempt to provide a compre-
hensive theory of syntax. In fact, we attempt to
simply instantiate the foundations of existing the-
ory (Frank, 2002) in as faithful a way as possible.
Our primary focus is the interplay between the lin-
guistic theory and the formal language theory. All
of the phenomena we consider can be (and in prac-
tice are (Group, 1998)) handled ad hoc with feature-
structure based TAG (FTAG, (Vijay-Shanker and
Joshi, 1991)). From a practical perspective, the
role of the underlying linguistic theory is, at least
in part, to insure consistent and comprehensive im-
plementation of ad hoc mechanisms. From a theo-
retical perspective, the role of the formal language
framework is, at least in part, to insure coherent and
computationally well-grounded theories. Our over-
all goal is to find formal systems that are as close as
possible to being a direct embodiment of the prin-
ciples guiding the linguistic theory and which are
maximally constrained in their formal and compu-
tational complexity.
2 Hierarchical Decomposition of Strings
and Trees
Like many approaches to formalization of natural
language syntax, TAG is based on a hierarchical de-
composition of strings which is represented by or-
dered trees. (Figure 1.) These trees are, in essence,
graphs representing two relationships—the left-to-
right ordering of the structural components of the
string and the relationship between a component
and its immediate constituents.
The distinguishing characteristic of TAG is that it
identifies an additional hierarchical decomposition
of these trees. This shows up, for instance when a
clause which has the form of a wh-question is em-
bedded as an argument within another clause. In the
VP
I’
I
does
VP
V
like
DP
Bob
Alice
IP
DP
CP
DPa0
who
C’
Ia1
does
Alice
DP
IP
Ia1
I’
t
tlike
V DPa0
Figure 1: Wh-movement and subj-aux inversion.
VP
V
think
that
a2
C
Carol
IP
DP
Alice
IP
DP a3
I
does
VP
V
like
DPa4
t
who
CP
DPa4 a2
Ia5
does a3
Ia5
t
DP
Alice
IP
a6
I
does
DPa7
t
V
like
VP
C
a8DPa7
who
CP
Ia9
t
V
think
a8
VP
a8
Ia9
does
a8
DP
Carol
IP
a6
Figure 2: Bridge verbs and subj-aux inversion.
wh-form (as in the right-hand tree of Figure 1), one
of the arguments of the verb is fronted as a wh-word
and the inflectional element (does, in this case) pre-
cedes the subject. This is generally known in the lit-
erature as wh-movement and subj-aux inversion, but
TAG does not necessarily assume there is any ac-
tual transformational movement involved, only that
there is a systematic relationship between the wh-
form and the canonical configuration. The ‘a10 ’s in the
trees mark the position of the corresponding compo-
nents in the canonical trees.1
When such a clause occurs as the argument of
a bridge verb (such as think or believe) it is split,
with the wh-word appearing to the left of the matrix
clause and the rest of the subordinate clause occur-
ring to the right (Figure 2). Standardly, TAG ac-
counts analyze this as insertion of the tree for the
matrix clause between the upper an lower portions
1This systematic relationship between the wh-form and the
canonical configuration has been a fundamental component of
syntactic theories dating back, at least, to the work of Harris in
the ’50’s.
of the tree for the embedded clause, an operation
known as tree-adjunction. In effect, the tree for
the embedded clause is wrapped around that of the
matrix clause. This process may iterate, with ad-
junction of arbitrarily many instances of bridge verb
trees:
Who does Bob believe . . . Carol thinks that Al-
ice likes.
One of the key advantages of this approach is that
the wh-word is introduced into the derivation within
the same elementary structure as the verb it is an ar-
gument of. Hence these structures are semantically
coherent—they express all and only the structural
relationships between the elements of a single func-
tional domain (Frank, 2002). The adjoined struc-
tures are similarly coherent and the derivation pre-
serves that coherence at all stages.
Following Rogers (2003) we will represent this
by connecting the adjoined tree to the point at which
it adjoins via a third, “tree constituency” relation as
in the right hand part of Figure 2. This gives us
like
a0
I
does
VP
V
seem
a0
I
to
VP
V DP
Bob
Alice
IP
DP
VP
V
seem
a0
I
to
Alice
IP
DP
Ia1
does
a2
who
CP
DPa3
Ia1
t
a0
VP
V
like
DPa3
t
Figure 3: Raising verbs.
structures that we usually conceptualize as three-
dimensional trees, but which can simply be regarded
as graphs with three sorts of edges, one for each
of the hierarchical relations expressed by the struc-
tures. Within this context, tree-adjunction is a pro-
cess of concatenating these structures, identifying
the root of the adjoined structure with the point at
which it is adjoined.2
The resulting complex structures are formally
equivalent to the derivation trees in standard for-
malizations of TAG. The derived tree is obtained by
concatenating the tree yield of the structure analo-
gously to the way that the string yield of a deriva-
tion tree is concatenated to form the derived string
of a context-free grammar. Note that in this case it is
essential to identify the point in the frontier of each
tree component at which the components it domi-
nates will be attached. This point is referred to as
the foot of the tree and the path to it from the root is
referred to as the (principal) spine of the tree. Here
we have marked the spines by doubling the corre-
sponding edges of the graphs.
Following Rogers (2002), we will treat the sub-
ject of the clause as if it were “adjoined” into the
rest of the clause at the root of the a4 . At this point,
this is for purely theory-internal reasons—it will al-
low us to exploit the additional formal power we
will shortly bring to bear. It should be noted that
it does not represent ordinary adjunction. The sub-
ject originates in the same elementary structure as
the rest of the clause, it is just a somewhat richer
structure than the more standard tree.
3 Raising Verbs and Subj-Aux Inversion
A problem arises, for this account, when the matrix
verb is a raising verb, such as seems or appears as
in
2Context-free derivation can be viewed as a similar process
of concatenating trees.
Alice seems to like Bob
Who does Alice seem to like
Here the matrix clause and the embedded clause
share, in some sense, the same subject argument.
(Figure 3.) Raising verbs are distinguished, further,
from the control verbs (such as want or promise) in
the fact that they may realize their subject as an ex-
pletive it:
It seems Alice likes Bob.
Note, in particular, that in each of these cases the
inflection is carried by the matrix clause. In order to
maintain semantic coherence, we will assume that
the subject originates in the elementary structure of
the embedded clause. This, then, interprets the rais-
ing verb as taking an a4 to an a4 , adjoining at the a4
between the subject and the inflectional element of
the embedded clause (as in the left-hand side of Fig-
ure 3).
For the declarative form this provides a nesting of
the trees similar to that of the bridge verbs; the em-
bedded clause tree is wrapped around that of the ma-
trix clause. For the wh-form, however, the wrapping
pattern is more complex. Since who and Alice must
originate in the same elementary structure as like,
while does must originate in the same elementary
structure as seem, the trees evidently must factor
and be interleaved as shown in the right-hand side of
the figure. Such a wrapping pattern is not possible
in ordinary TAG. The sequences of labels occurring
along the spines of TAG tree sets must form context-
free languages (Weir, 1988). Hence the “center-
embedded” wrapping patterns of the bridge verbs
and the declarative form of the raising verbs are pos-
sible but the “cross-serial” pattern of the wh-form of
the raising verbs is not.
a0
a0
DPa1
who
CP DP
Alice
IP
a0
V
seem
a0
VP
Ia2
t
VP
DPa1
t
V
seem
a0
a3
DPa1
VP
I
to
VP
I
to
IP
DPa1
t
V
like
Ia2
t
a3
Ia2
does
Ia2
does
a0
a0V
like
VP
Ia2
does
a3
DP
Alice
DPa1
who
CP
DPa1
DP
Alice
who
IP
CP
a0
seem
V
VP
a0
Ia2
t
I
to
V
like
t
Figure 4: An higher-order account.
4 Higher-order Decomposition
One approach to obtaining the more complicated
wrapping pattern that occurs in the wh-form of the
raising verb trees is to move to a formalism in which
the spine languages of the derived trees are TALs
(the string languages derived by TAGs), which can
describe such patterns. One such formalism is the
third level of Weir’s Control Language Hierarchy
(Weir, 1992) which admits sets of derivation trees
generated by CFGs which are filtered by a require-
ment that the sequences of labels on the spines oc-
cur in some particular TAL.3 The problem with this
approach is that it abandons the notion of semantic
coherence of the elementary structures.
It turns out, however, that one can generate ex-
actly the same tree sets if one moves to a for-
malism in which another level of hierarchical de-
composition is introduced (Rogers, 2003). This
now gives structures which employ four hierarchical
relations—the fourth representing the constituency
relation encoding a hierarchical decomposition of
the third-level structures. In this framework, the
seem structure can be taken to be inserted between
the subject and the rest of the like structure as shown
in Figure 4. Again, spines are marked by doubling
3TAG is equivalent to the second level of this hierarchy, in
which the spine languages are Context-Free.
the edges.
The third-order yield of the corresponding de-
rived structure now wraps the third-order like struc-
ture around that of the seem structure, with the frag-
ment of like that contains the subject attaching at
the third-order “foot” node in the tree-yield of the
seem structure (the a4 ) as shown at the bottom of the
figure. The center-embedding wrapping pattern of
these third-order spines guarantees that the wrap-
ping pattern of spines of the tree yield will be a
TAL, in particular, the “cross-serial” pattern needed
by raising of wh-form structures.
The fourth-order structure has the added benefit
of clearly justifying the status of the like structure as
a single elementary structure despite of the apparent
extraction of the subject along the third relation.
5 Locality Effects
Note that it is the a4 to a4 recursion along the third-
order spine of the seem structure that actually does
the raising of the subject. One of the consequences
of this is that that-trace violations, such as
a4 Who
a5 does Alicea6 seem that a7a8a6 does like a7a9a5 .
cannot occur. If the complementizer originates in
the seem structure, it will occur under the a10 . If it
originates in the like tree it will occur in a similar
position between the CP and the a4 . In either case,
VP
a0
seem
V
I
does
a0
a1
IP
it
DP
Bob
V
like
DP
CP
a0
a0
a0DP
Alice
IP
a1
I
does
C
that
Figure 5: Expletive it.
the complementizer must precede the raised subject
in the derived string.
If we fill the subject position of the seem struc-
ture with expletive it, as in Figure 5, the a4 position
in the yield of the structure is occupied and we no
longer have a4 to a4 recursion. This motivates analyz-
ing these structures as a10 to a10 recursion, similar to
bridge verbs, rather than a4 to a4 . (Figure 5.) More
importantly the presence of the expletive subject in
the seem tree rules out super-raising violations such
as
a4 Alice does it seems
a7 does like Bob.
a4 Alice does appear it seems
a7 does like Bob.
No matter how the seem structure is interpreted, if it
is to raise Alice then the Alice structure will have to
settle somewhere in its yield. Without extending the
seem structure to include the a10 position, none of the
possible positions will yield the correct string (and
all can be ruled out on simple structural grounds). If
the seem structure is extended to include the a10 , the
raising will be ruled out on the assumption that the
structure must attach at a10 .
6 Subject-Object Asymmetry
Another phenomenon that has proved problematic
for standard TAG accounts is extraction from nomi-
nals, such as
Who did Alice publish a picture of a7 .
Here the wh-word is an argument of the preposi-
tional phrase in the object nominal picture of. Ap-
parently, the tree structure involves wrapping of the
picture tree around the publish tree. (See Figure 6.)
The problem, as normally analyzed (Frank, 2002;
Kroch, 1989), is that the the publish tree does have
the recursive structure normally assumed for auxil-
iary trees. We will take a somewhat less strict view
and rule out the adjunction of the publish tree sim-
ply on the grounds that it would involve attaching a
structure rooted in a10 (or possibly CP) to a DP node.
The usual way around this difficulty has been to
assume that the who is introduced in the publish
tree, corresponding, presumably, to the as yet miss-
ing DP. The picture tree is then factored into two
components, an isolated DP node which adjoins at
the wh-DP, establishing its connection to the argu-
ment trace, and the picture DP which combines at
the object position of publish.
This seems to at least test the spirit of the seman-
tic coherence requirement. If the who is not extra-
neous in the publish tree then it must be related in
some way to the object position. But the identity of
who is ultimately not the object of publish (a pic-
ture) but rather the object of the embedded preposi-
tion (the person the picture is of).
If we analyze this in terms of a fourth hierarchi-
cal relation, we can allow the who to originate in
the picture structure, which would now be rooted
in CP. This could be allowed to attach at the root
of the publish structure on the assumption that it is
a C-node of some sort, providing the wrapping of
its tree-yield around that of the publish. (See Fig-
ure 6.) Thus we get an account with intact elemen-
tary structures which are unquestionably semanti-
cally coherent.
One of the striking characteristics of extraction of
this sort is the asymmetry between extraction from
the object, which is acceptable, and extraction from
the subject, which is not:
a4 Who did a picture of
a7 illustrate the point.
In the account under consideration, we might con-
template a similar combination of structures, but in
this case the picture DP has to somehow migrate up
to combine at the subject position. Under our as-
sumption that the subject structure is attached to the
illustrate tree via the third relation, this would re-
quire the subject structure to, in effect, have two
PP
of
P
DPa0
t
P
of
PP
a picture
DP
t
DPa0
DP
Ia1
t
a picture
V
publish
DP
VP
who
IP
CP IPa1
did
a2
CP
who
DPa0
DP
Alice
IP
a3
VP
DP
publish
V
IPa1
did
a2
DPa0
who
CP
t
a picture
DP
DP
Ia1
DPa0
t
P
of
PP
Alice
DP
a3
DPa0
CP
IP
a3
DP
Alice
a2
Ia1
t
did
IPa1
V
publish
DP
VP
Figure 6: Extraction from object nominal.
CP
V
IP
DP
DPa4
t
a picture
P
of
PP
VP
the point
illustrate
DP
DPa4
who
V
DP
illustrate
the point
VP
a5
DP
CP
who
DPa4
Ia6
t
PP
of
P
V
DP
illustrate
the point
VP
t
DPa4
IPa6
did
a7
a picture
DP
DP
a7
did
DPa4
who
CP
IPa6
IPa6
did
a7
t
Ia6
DP
IP
a5
DPa4
t
P
of
PP
DP
a picture
DP
a5
IP Ia6
t
Figure 7: Extraction from subject nominal.
feet, an extension that strictly increases the gen-
erative power of the formalism. Alternatively, we
might assume that the picture structure attaches in
the yield of the illustrate structure or between the
main part of the structure and the subject tree, but
either of these would fail to promote the who to the
root of the yield structure.
7 Processing
As with any computationally oriented formalism,
the ability to define the correct set of structures is
only one aspect of the problem. Just as important
is the question of the complexity of processing lan-
guage relative to that definition. Fortunately, the
languages of the Control Language Hierarchy are
well understood and recognition algorithms, based
on a CKY-style dynamic programming approach,
are know for each level. The time complexity of
the algorithm for the a0a2a1a4a3 level, as a function of the
length of the input (a5 ), is a6a8a7a9a5a11a10a13a12a14a16a15a18a17a20a19a16a21 (Palis and
Shende, 1992). In the case of the fourth-order gram-
mars, which correspond to the third level of the
CLH, this gives an upper bound of a6a8a7a9a5a23a22a24a14a25a21 .
While, strictly speaking, this is a feasible time
complexity, in practice we expect that approaches
with better average-case complexity, such as Early-
style algorithms, will be necessary if these gram-
mars are to be parsed directly. But, as we noted
in the introduction, grammars of this complexity
are not necessarily intended to be used as working
grammars. Rather they are mechanisms for express-
ing the linguistic theory serving as the foundation of
working grammars of more practical complexity.
Since all of our proposed use of the higher-order
relations involve either combining at a root (with-
out properly embedding) or embedding with finitely
bounded depth of nesting, the effect of the higher-
dimensional combining operations are expressible
using a finite set of features. Hence, the sets of
derived trees can be generated by adding finitely
many features to ordinary TAGs and the theory en-
tailed by our accounts of these phenomena (as ex-
pressed in the sets of derived trees) is expressible in
FTAG. Thus, a complete theory of syntax incorpo-
rating them would be (not necessarily not) compati-
ble with implementation within existing TAG-based
systems. A more long term goal is to implement
a compilation mechanism which will translate the
linguistic theory, stated in terms of the hierarchical
relations, directly into grammars stated in terms of
the existing TAG-based systems.
8 Conclusion
In many ways the formalism we have working with
is a minimal extension of ordinary TAGs. Formally,
the step from TAG to add the fourth hierarchical re-
lation is directly analogous to the step from CFG
to TAG. Moreover, while the graphs describing the
derived structures are often rather complicated, con-
ceptually they involve reasoning in terms of only a
single additional relation. The benefit of the added
complexity is a uniform account of a range of phe-
nomena that has heretofore been difficult to encom-
pass, an account that employs unitary elementary
structures and eschews synchronized derivation op-
erations, and which is, in many respects, closer to
the spirit of the intuitions underlying TAG-based
linguistic theory than previously considered exten-
sions to TAG.
While it is impossible to determine how compre-
hensive the coverage of a more fully developed the-
ory of syntax based on this formalism will be with-
out actually completing such a theory, we believe
that the results presented here suggest that the uni-
formity provided by adding this fourth level of de-
composition to our vocabulary is likely to more than
compensate for the added complexity of the fourth
level elementary structures.
References
Robert Evan Frank. 1992. Syntactic Locality and
Tree Adjoining Grammar: Grammatical, Acqui-
sition and Processing Perspectives. Ph.D. disser-
tation, Univ. of Penn.
Robert Frank. 2002. Phrase Structure Composition
and Syntactic Dependencies. MIT Press.
The XTAG Research Group. 1998. A lexical-
ized tree adjoining grammar for english. Tech-
nical Report IRCS-98-18, Institute for Research
in Cognitive Science.
Aravind K. Joshi and Yves Schabes. 1997. Tree-
adjoining grammars. In Handbook of Formal
Languages and Automata, volume 3, pages 69–
123. Springer-Verlag.
Aravind K. Joshi, Leon Levy, and Masako Taka-
hashi. 1975. Tree adjunct grammars. Journal of
the Computer and Systems Sciences, 10:136–163.
Anthony Kroch and Aravind K. Joshi. 1985. The
linquistic relevance of tree adjoining grammar.
Technical Report MS-CS-85-16, Dept. of Com-
puter and Information Sciences.
Anthony S. Kroch and Aravind K. Joshi. 1987. An-
alyzing extraposition in a tree adjoining grammar.
In Syntax and Semantics, pages 107–149. Aca-
demic Press. Vol. 20.
Anthony Kroch. 1989. Asymmetries in long dis-
tance extraction in a tree adjoining grammar. In
Mark Baltin and Anthony Kroch, editors, Alter-
native Conceptions of Phrase Structure, pages
66–98. University of Chicago Press.
Michael A. Palis and Sunil M. Shende. 1992. Up-
per bounds on recognition of a hierarchy of non-
context-free languages. Theoretical Computer
Science, 98:289–319.
James Rogers. 2002. One more perspective on se-
mantic relations in TAG. In Proceedings of the
Sixth International Workshop on Tree Adjoining
Grammars and Related Frameworks, Venice, IT,
May.
James Rogers. 2003. Syntactic structures as multi-
dimensional trees. Research on Language and
Computation, 1(3–4):265–305.
K. Vijay-Shanker and Aravind K. Joshi. 1991.
Unification based tree adjoining grammars. In
J. Wedekind, editor, Unification-based Gram-
mars. MIT Press, Cambridge, MA.
David J. Weir. 1988. Characterizing Mildly
Context-Sensitive Grammar Formalisms. Ph.D.
thesis, University of Pennsylvania.
David J. Weir. 1992. A geometric hierarchy be-
yond context-free languages. Theoretical Com-
puter Science, 104:235–261.
