References

1   Steven P. Shwartz, Applied Natural Language Processing, Petrocelli Books, Inc., Princeton, NJ, 1986 

2   Elaine Rich , Kevin Knight, Artificial Intelligence, McGraw-Hill Higher Education, 1990 

3   James Allen, Natural language understanding (2nd ed.), Benjamin-Cummings Publishing Co., Inc., Redwood City, CA, 1995 

4   Bora, Satyanath, 1968. bahal byaakaran. Jnananath Bora, Guwahati 

5   Goswami, Golokchandra, 1990. asamiyaa byaakaranar moulik bisaar. Bina Library, Guwahati 

6   Choudhury, Bhupendranath, 18e, 1973. asamiyaa bhaashaar byaakaran, pratham bhaag. Lawyer's Book Stall, Guwahati 

7   Sarma, Durgashankar Dev, 1977. sahaj byaakaran. Assam State Textbook Production and Publication Corporation Ltd., Guwahati-1 

8   Baruah, Hemchandra, 1985 Hem Kosha, 6e. Hemkosh Prakashan, Guwahati 

9   Verma, Shyamji Gokul, 1981. Maanak Hindi Byaakaran Tatha Rachnaa. Arya Book Depot, New Delhi-5 

10   Whitney, William Dwight, 1977. Sanskrit Grammar. Motilal Banarasidass, Delhi. 

11   Whitney, William Dwight, 1979. Roots, Verb Forms and Primary Derivatives of the Sanskrit Language. Motilal Banarasidass, Delhi. 

12   G�bor Pr�sz�ky , Bal�zs Kis, A unification-based approach to morpho-syntactic parsing of agglutinative and other (highly) inflectional languages, Proceedings of the 37th annual meeting of the Association for Computational Linguistics on Computational Linguistics, p.261-268, June 20-26, 1999, College Park, Maryland 

13   Bharati, Akshar, Chaitanya, Vineet and Sangal, Rajeev, 1995 Natural Language Processing - A Paninian Perspective. Prentice-Hall of India Pvt Ltd., New Delhi 

14   John Goldsmith, Unsupervised learning of the morphology of a natural language, Computational Linguistics, v.27 n.2, p.153-198, June 2001 

15   Kazakov, Dimitar, "Unsupervised Learning of Naive Morphology with Genetic Algorithms" Workshop Notes of the ECML/MLnet Workshop on Empirical Learning of Natural Language Processing Tasks, pp 105--112, April 26, 1997, Prague, Czech Republic 
