References

1   Eric Brill, A simple rule-based part of speech tagger, Proceedings of the third conference on Applied natural language processing, March 31-April 03, 1992, Trento, Italy 

2   Julio Gonzalo, Felisa Verdejo, Irina Chugur, and Juan Cigarran. 1998. Indexing with WordNet synsets can improve text retrieval. In Proceedings of the COLING-ACL'98 Workshop on Usage of WordNet in Natural Language Processing Systems, pages 38--44, Montreal, Canada. 

3   Sanda Harabagiu , Dan Moldovan , Marius Pasca , Rada Mihalcea , Mihai Surdeanu , Razvan Bunescu , Roxana G�rju , Vasile Rus , Paul Morarescu, The role of lexico-semantic feedback in open-domain textual question-answering, Proceedings of the 39th Annual Meeting on Association for Computational Linguistics, p.282-289, July 06-11, 2001, Toulouse, France 

4   Irene Langkilde , Kevin Knight, Generation that exploits corpus-based statistical knowledge, Proceedings of the 36th annual meeting on Association for Computational Linguistics, August 10-14, 1998, Montreal, Quebec, Canada 

5   Dekang Lin, Automatic retrieval and clustering of similar words, Proceedings of the 17th international conference on Computational linguistics, p.768-774, August 10-14, 1998, Montreal, Quebec, Canada 

6   Steven Lytinen, Noriko Tomuro, and Tom Repede. 2000. The use of WordNet sense tagging in FAQfinder. In Proceedings of the AAAI00 Workshop on AI and Web Search, Austin, Texas. 

7   Rada Mihalcea , Dan I. Moldovan, A method for word sense disambiguation of unrestricted text, Proceedings of the 37th annual meeting of the Association for Computational Linguistics on Computational Linguistics, p.152-158, June 20-26, 1999, College Park, Maryland 

8   George Miller, Richard Beckwith, Christiane Fellbaum, Derek Gross, and Katherine Miller. 1990. Introduction to WordNet: An on-line lexical database. Journal of Lexicography, 3(4):235--244. 

9   Dan Moldovan , Marius Pasca , Sanda Harabagiu , Mihai Surdeanu, Performance issues and error analysis in an open-domain Question Answering system, Proceedings of the 40th Annual Meeting on Association for Computational Linguistics, July 07-12, 2002, Philadelphia, Pennsylvania 

10   Judea Pearl, Probabilistic reasoning in intelligent systems: networks of plausible inference, Morgan Kaufmann Publishers Inc., San Francisco, CA, 1988 

11   Gerard Salton , Michael J. McGill, Introduction to Modern Information Retrieval, McGraw-Hill, Inc., New York, NY, 1986 

12   Hinrich Sch�tze and Jan O. Pedersen. 1995. Information retrieval based on word senses. In Proceedings of the Fourth Annual Symposium on Document Analysis and Information Retrieval, pages 161--175, Las Vegas, Nevada. 

13   Ingrid Zukerman , Sarah George, Towards a noise-tolerant, representation-independent mechanism for argument interpretation, Proceedings of the 19th international conference on Computational linguistics, p.1-7, August 24-September 01, 2002, Taipei, Taiwan 

14   Ingrid Zukerman , Bhavani Raskutti, Lexical query paraphrasing for document retrieval, Proceedings of the 19th international conference on Computational linguistics, p.1-7, August 24-September 01, 2002, Taipei, Taiwan
