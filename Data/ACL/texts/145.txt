Book Reviews Linguistic Structures Processing 
Linguistic Structures Processing - 
Studies in Linguistics, Computational 
Linguistics, and Artificial Intelligence 
Antonio Zampolli, Editor 
North-Holland Publishing Co., New York, 1977, 
586 pp., $48.00, ISBN 0-444o85017-1. 
This book is a collection of good articles. It is 
not, however, a good collection of articles. The 
only connection between them is that their authors 
all lectured at the International Summer School on 
Computational and Mathematical Linguistics at Pisa 
in 1974. Each lecturer was asked to contribute a 
chapter to the book; some of the contributions were 
specifically written for it, while others are papers 
that the authors had published elsewhere. Although 
each article is good by itself, the book as a whole 
lacks a common theme, a logical progression from 
one article to another, and a common level of back- 
ground knowledge expected of the reader. 
Three of the articles taken together make a good 
survey of computational linguistics: On natural lan- 
guage based computer systems by Stanley Petrick, 
Natural language understanding systems within the A1 
paradigm by Yorick Wilks, and Five lectures on arti- 
ficial intelligence by Terry Winograd. Although the 
articles are three to five years old, the issues they 
discuss are still among the most active research top- 
ics today. One strength is the variety of viewpoints 
on many of the same systems and issues. One 
weakness is the skimpy treatment of semantic net- 
works and related graphs: Winograd, for example, 
devotes two pages to them out of 123, while using 
eleven pages to reproduce the same SHRDLU dialog 
that he has been quoting for the past eight years. 
One absurdity is the placement of these introductory 
articles near the end of the book because the chap- 
ters are listed alphabetically by their authors' last 
names. 
Three tutorials on techniques are Synthesis of 
speech from unrestricted text by Jonathan Allen, 
Morphological and syntactic analysis by Martin Kay, 
and Lunar rocks in natural English by William 
Woods. Allen's article is a short survey of the state 
of the art and current issues in speech synthesis. 
Woods describes the various phases of the LUNAR 
system; he doesn't give enough detail to enable a 
beginner to build his own system, but he gives 
enough motivation and references to show someone 
where to go for further information. Kay, however, 
buries the reader in detail, including 21 pages of 
traces from his parser. Such detail is acceptable in a 
technical report, but an article of this sort should 
put more emphasis on the reasons for these techni- 
ques. Some comparisons with the parsing methods 
of Petrick, Wilks, Winograd, and Woods would be 
especially useful since they are discussed elsewhere 
in the same book. 
Two articles that relate computational questions 
to more general issues in linguistics and psychology 
are Scenes-and-frames semantics by Charles Fillmore 
and Cognition: The linguistic approach by David 
Hays. Fillmore's article meanders for seventeen 
untitled sections: he presents a wealth of observa- 
tions that a semantic theory must account for, but 
he never attempts to systematize his observations or 
present a tentative theory of his own. Hays, on the 
other hand, has a short, tightly organized discussion 
of the psychological implications of cognitive net- 
works. But his article is so vague and devoid of 
examples that it is hardly more than an extended 
abstract. 
Four other papers, 'The position of embedding 
transformations in a grammar" revisited by Emmon 
Bach, Focus and negation by Eva Haji~owi, Some 
observations concerning the differences between sen- 
tence and text by Ferenc Kiefer, and John is easy to 
please by Barbara Partee, treat theoretical points in 
linguistics that are also important computationally. 
Yet none of the authors cite any computational or 
AI work in their bibliographies or make any attempt 
to relate their issues to computational methods. 
These four articles illustrate a frequent failing of 
interdisciplinary conferences: the speakers talk past 
one another without ever reconciling their vocabu- 
laries or coming to grips with common issues. (In 
their more recent work, Bach and Partee and their 
graduate students have been combining Montague 
American Journal of Computational Linguistics, Volume 6, Number 1, January-March 1980 47 
Book Reviews Automated Theorem Proving 
grammar and purely linguistic theory with parsing 
techniques; it is a pity that their articles in this book 
make no suggestion of such a combination.) 
In summary, this book is not systematic enough 
for an introductory text, and it surveys too much 
familiar work for a research collection. Although 
the book is not suitable as a primary textbook, parts 
of it would make good supplementary reading for a 
course in AI or computational linguistics. 
John F. Sowa, IBM Systems Research Institute 
