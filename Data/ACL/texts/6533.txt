References

Steven Abney. 1995. Chunks and dependencies:
Bringing processing evidence to bear
on syntax. In Jennifer Cole, Georgia Green,
and Jerry Morgan, editors, Computational
Linguistics and the Foundations of Linguis-
tic Theory, pages 145{164. CSLI.

M. Burke, A. Cahill, R. O'Donovan, J. van
Genabith, and A. Way. 2004. Treebankbased
acquisistion of wide-coverage, probabilistic
LFG resources: Project overview, results
and evaluation. In The First Interna-
tional Joint Conference on Natural Language
Processing (IJCNLP-04), Workshop "Beyond
shallow analyses - Formalisms and statisti-
cal modeling for deep analyses", Sanya City,
China.

Aoife Cahill, Michael Burke, Ruth O'Donovan,
Josef van Genabith, and Andy Way. 2004.
Long-distance dependency resolution in automatically
acquired wide-coverage PCFGbased
LFG approximations. In Proceedings of
ACL-2004, Barcelona, Spain.

John Carroll, Guido Minnen, and Ted Briscoe.
1999. Corpus annotation for parser evaluation.
In Proceedings of the EACL-99 Post-
Conference Workshop on Linguistically Inter-
preted Corpora, Bergen, Norway.

Eugene Charniak. 2000. A maximum-entropyinspired
parser. In Proceedings of the North
American Chapter of the ACL, pages 132{139.

Noam Chomsky. 1995. The Minimalist Pro-
gram. The MIT Press, Cambridge, Massachusetts.

Hoojung Chung and Hae-Chang Rim. 2003. A
new probabilistic dependency parsing model
for head-nal, free word order languages. IE-
ICE Transaction on Information & System,
E86-D, No. 11:2490{2493.

Michael Collins and James Brooks. 1995.
Prepositional attachment through a backedo
 model. In Proceedings of the Third Work-
shop on Very Large Corpora, Cambridge,
MA.

Michael Collins. 1999. Head-Driven Statistical
Models for Natural Language Parsing. Ph.D.
thesis, University of Pennsylvania, Philadelphia,
PA.

Michael A. Covington. 1994. An empirically
motivated reinterpretation of Dependency
Grammar. Technical Report AI1994-01, University
of Georgia, Athens, Georgia.

Amit Dubey and Frank Keller. 2003. Probabilistic
parsing for German using sister-head
dependencies. In Proceedings of the 41st An-
nual Meeting of the Association for Compu-
tational Linguistics, Sapporo.

Jason Eisner. 2000. Bilexical grammars and
their cubic-time parsing algorithms. In Harry
Bunt and Anton Nijholt, editors, Advances in
Probabilistic and Other Parsing Technologies.
Kluwer.

Christiane Fellbaum, editor. 1998. WordNet:
An Electronic Lexical Database. MIT Press,
Cambridge, MA.

James Henderson. 2003. Inducing history
representations for broad coverage statistical
parsing. In Proceedings of HLT-NAACL
2003, Edmonton, Canada.

Julia Hockenmaier and Mark Steedman. 2002.
Generative models for statistical parsing with
combinatory categorial grammar. In Proceed-
ings of 40th Annual Meeting of the Associa-
tion for Computational Linguistics, Philadelphia.

Richard Hudson. 1984. Word Grammar. Basil
Blackwell, Oxford.

Mark Johnson. 2002. A simple patternmatching
algorithm for recovering empty
nodes and their antecedents. In Proceedings
of the 40th Meeting of the ACL, University of
Pennsylvania, Philadelphia.

J.D. Kim, T. Ohta, Y. Tateisi, and J. Tsujii.
2003. Genia corpus - a semantically annotated
corpus for bio-textmining. Bioinfor-
matics, 19(1):i180{i182.

Beth C. Levin. 1993. English Verb Classes
and Alternations: a Preliminary Investiga-
tion. University of Chicago Press, Chicago,
IL.

Dekang Lin. 1995. A dependency-based
method for evaluating broad-coverage
parsers. In Proceedings of IJCAI-95, Montreal.

Dekang Lin. 1998. Dependency-based evaluation
of MINIPAR. In Workshop on the Eval-
uation of Parsing Systems, Granada, Spain.

Mitch Marcus, Beatrice Santorini, and M.A.
Marcinkiewicz. 1993. Building a large annotated
corpus of English: the Penn Treebank.
Computational Linguistics, 19:313{330.

Igor Mel'cuk. 1988. Dependency Syntax: theory
and practice. State University of New York
Press, New York.

Diego Molla, Gerold Schneider, Rolf Schwitter,
and Michael Hess. 2000. Answer
Extraction using a Dependency Grammar
in ExtrAns. Traitement Automatique de
Langues (T.A.L.), Special Issue on Depen-
dency Grammar, 41(1):127{156.

Peter Neuhaus and Norbert Broker. 1997. The
complexity of recognition of linguistically adequate
dependency grammars. In Proceedings
of the 35th ACL and 8th EACL, pages 337{
343, Madrid, Spain.

Joakim Nivre. 2004. Inductive dependency
parsing. In Proceedings of Promote IT, Karlstad
University.

Judita Preiss. 2003. Using grammatical relations
to compare parsers. In Proc. of EACL
03, Budapest, Hungary.

Stefan Riezler, Tracy H. King, Ronald M. Kaplan,
Richard Crouch, John T. Maxwell,
and Mark Johnson. 2002. Parsing the Wall
Street Journal using a Lexical-Functional
Grammar and discriminative estimation techniques.
In Proc. of the 40th Annual Meet-
ing of the Association for Computational Lin-
guistics (ACL'02), Philadephia, PA.

Fabio Rinaldi, James Dowdall, Gerold Schneider,
and Andreas Persidis. 2004a. Answering
Questions in the Genomics Domain. In
ACL 2004 Workshop on Question Answering
in restricted domains, Barcelona, Spain, 21{
26 July.

Fabio Rinaldi, Michael Hess, James Dowdall,
Diego Molla, and Rolf Schwitter. 2004b.
Question answering in terminology-rich technical
domains. In Mark Maybury, editor,
New Directions in Question Answering.
MIT/AAAI Press.

Anoop Sarkar, Fei Xia, and Aravind Joshi.
2000. Some experiments on indicators of
parsing complexity for lexicalized grammars.
In Proc. of COLING.

Gerold Schneider. 2003. Extracting and using
trace-free Functional Dependencies from the
Penn Treebank to reduce parsing complexity.
In Proceedings of Treebanks and Linguis-
tic Theories (TLT) 2003, Vaxjo, Sweden.

Wojciech Skut, Brigitte Krenn, Thorsten
Brants, and Hans Uszkoreit. 1997. An annotation
scheme for free word order languages.
In Proceedings of the Fifth Conference on Ap-
plied Natural Language Processing (ANLP-
97), Washington, DC.

Pasi Tapanainen and Timo Jarvinen. 1997. A
non-projective dependency parser. In Pro-
ceedings of the 5th Conference on Applied
Natural Language Processing, pages 64{71.
Association for Computational Linguistics.

Lucien Tesniere. 1959. Elements de Syntaxe
Structurale. Librairie Klincksieck, Paris.
