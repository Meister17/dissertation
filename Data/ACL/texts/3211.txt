Chunk-based Statistical Translation
Taro Watanabe†, Eiichiro Sumita† and Hiroshi G. Okuno‡
{taro.watanabe, eiichiro.sumita}@atr.co.jp
†ATR Spoken Language Translation ‡Department of Intelligence Science
Research Laboratories and Technology
2-2-2 Hikaridai, Keihanna Science City Graduate School of Informatics, Kyoto Uniersity
Kyoto 619-0288 JAPAN Kyoto 606-8501 JAPAN
Abstract
This paper describes an alternative trans-
lation model based on a text chunk un-
der the framework of statistical machine
translation. The translation model sug-
gested here first performs chunking. Then,
each word in a chunk is translated. Fi-
nally, translated chunks are reordered.
Under this scenario of translation model-
ing, we have experimented on a broad-
coverage Japanese-English traveling cor-
pus and achieved improved performance.
1 Introduction
The framework of statistical machine translation for-
mulates the problem of translating a source sentence
in a language J into a target language E as the
maximization problem of the conditional probability
ˆ
E = argmax
E
P(E|J). The application of the Bayes
Rule resulted in
ˆ
E =argmax
E
P(E)P(J|E). The for-
mer term P(E) is called a language model, repre-
senting the likelihood of E. The latter term P(J|E)
is called a translation model, representing the gener-
ation probability from E into J.
As an implementation of P(J|E), the word align-
ment based statistical translation (Brown et al.,
1993) has been successfully applied to similar lan-
guage pairs, such as French–English and German–
English, but not to drastically diﬀerent ones, such
as Japanese–English. This failure has been due to
the limited representation by word alignment and
the weak model structure for handling complicated
word correspondence.
This paper provides a chunk-based statistical
translation as an alternative to the word alignment
based statistical translation. The translation process
inside the translation model is structured as follows.
A source sentence is first chunked, and then each
chunk is translated into target language with local
word alignments. Next, translated chunks are re-
ordered to match the target language constraints.
Based on this scenario, the chunk-based statis-
tical translation model is structured with several
components and trained by a variation of the EM-
algorithm. A translation experiment was carried
out with a decoder based on the left-to-right beam
search. It was observed that the translation quality
improved from 46.5% to 52.1% in BLEU score and
from 59.2% to 65.1% in subjective evaluation.
The next section briefly reviews the word align-
ment based statistical machine translation (Brown et
al., 1993). Section 3 discusses an alternative ap-
proach, a chunk-based translation model, ranging
from its structure to training procedure and decod-
ing algorithm. Then, Section 4 provides experimen-
tal results on Japanese-to-English translation in the
traveling domain, followed by discussion.
2 Word Alignment Based Statistical
Translation
Word alignment based statistical translation rep-
resents bilingual correspondence by the notion of
word alignment A, allowing one-to-many generation
from each source word. Figure 1 illustrates an exam-
ple of English and Japanese sentences, E and J, with
sample word alignments. In this example, “show
1
”
has generated two words, “mise
5
” and “tekudasai
6
”.
E = NULL
0
show
1
me
2
the
3
one
4
in
5
the
6
window
7
J= uindo
1
no
2
shinamono
3
o
4
mise
5
tekudasai
6
A=(7 0 4 0 1 1 )
Figure 1: Example of word alignment
Under this word alignment assumption, the transla-
tion model P(J|E) can be further decomposed with-
out approximation.
P(J|E)=
summationdisplay
A
P(J,A|E)
2.1 IBM Model
During the generation process from E to J, P(J,A|E)
is assumed to be structured with a couple of pro-
cesses, such as insertion, deletion and reorder. A
scenario for the word alignment based translation
model defined by Brown et al. (1993), for instance
IBM Model 4, goes as follows (refer to Figure 2).
1. Choose the number of words to generate for
each source word according to the Fertility
Model. For example, “show” was increased to
2 words, while “me” was deleted.
2. Insert NULLs at appropriate positions by the
NULL Generation Model. Two NULLs were
inserted after each “show” in Figure 2.
3. Translate word-by-word for each generated
word by looking up the Lexicon Model. One of
the two “show” words was translated to “mise.”
4. Reorder the translated words by referring to the
Distortion Model. The word “mise” was re-
ordered to the 5th position, and “uindo” was
reordered to the 1st position. Positioning is de-
termined by the previous word’s alignment to
capture phrasal constraints.
For the meanings of each symbol in each model, re-
fer to Brown et al. (1993).
2.2 Problems of Word Alignment Based
Translation Model
The strategy for the word alignment based transla-
tion model is to translate each word by generating
multiple single words (a bag of words) and to deter-
mine the position of each translated word. Although
show
1
show show mise uindo
1
me
2
show NULL no no
2
the
3
one show tekudasai shinamono
3
one
4
window NULL o o
4
in
5
one shinamono mise
5
the
6
window uindo tekudasai
6
window
7
n(2|E
1
)
n(0|E
2
)
n(0|E
3
)
...
Fertility
parenleftBig
4
2
parenrightBig
p
4−2
0
p
2
1
NULL
t(J
5
|E
1
)
t(J
6
|E
1
)
t(J
3
|E
4
)
...
Lexicon
d
1
(1−ceilingleft
3
1
ceilingright|E
4
,J
1
)
d
1
(3−ceilingleft
5+6
2
ceilingright|E
1
,J
3
)
d
1
(5−ceilingleft
2+4
2
ceilingright|NULL,J
5
)
d
>1
(6−5|J
6
)
Distortion
Figure 2: Word alignment based translation model
P(J,A|E) (IBM Model 4)
this procedure is suﬃcient to capture the bilingual
correspondence for similar language pairs, some is-
sues remain for drastically diﬀerent pairs:
Insertion/Deletion Modeling Although deletion
was modeled in the Fertility Model, it merely as-
signs zero to each deleted word without considering
context. Similarly, inserted words are selected by
the Lexical Model parameter and inserted at the po-
sitions determined by a binomial distribution.
This insertion/deletion scheme contributed to the
simplicity of this representation of the translation
processes, allowing a sophisticated application to
run on an enormous bilingual sentence collection.
However, it is apparent that the weak modeling of
those phenomena will lead to inferior performance
for language pairs such as Japanese and English.
Local Alignment Modeling The IBM Model 4
(and 5) simulates phrasal constraints, although there
were implicitly implemented as its Distortion Model
parameters. In addition, the entire reordering is
determined by a collection of local reorderings in-
suﬃcient to capture the long-distance phrasal con-
straints.
The next section introduces an alternative model-
ing, chunk-based statistical translation, which was
intended to resolve the above two issues.
3 Chunk-based Statistical Translation
Chunk-based statistical translation models the pro-
cess of chunking for both the source and target sen-
tences, E and J,
P(J|E)=
summationdisplay
J
summationdisplay
E
P(J,J,E|E)
where J and E are the chunked sentences for J
and E, respectively, defined as two-dimentional ar-
E= show
1
me
2
1
the
3
one
4
2
in
5
the
6
window
7
3
mise
5
tekudasai
6
shinamono
3
o
4
uindo
1
no
2
J= uindo
1
no
2
1
shinamono
3
o
4
2
mise
5
tekudasai
6
3
A=(3 2 1)
A=([7, 0] [4, 0] [1, 1] )
Figure 3: Example of chunk-based alignment
rays. For instance, J
i,j
represents the jth word of
the ith chunk. The number of chunks for source
and target is assumed to be equal, |J| = |E|,
so that each chunk can convey a unit of meaning
without added/subtracted information. The term
P(J,J,E|E) is further decomposed by chunk align-
ment A and word alignment for each chunk transla-
tionA.
P(J,J,E|E)=
summationdisplay
A
summationdisplay
A
P(J,J,A,A,E|E)
The notion of alignment A is the same as those found
in the word alignment based translation model,
which assigns a source chunk index for each target
chunk. Ais a two-dimensional array which assigns
a source word index for each target word per chunk.
For example, Figure 3 shows two-level alignments
taken from the example in Figure 1. The target
chunk at position 3,J
3
, “mise tekudasai” is aligned
to the first position (A
3
= 1), and both the words
“mise” and “tekudasai” are aligned to the first posi-
tion of the source sentence (A
3,1
=1,A
3,2
=1).
3.1 Translation Model Structure
The term P(J,J,A,A,E|E) is further decomposed
with approximation according to the scenario de-
scribed below (refer to Figure 4).
1. Perform chunking for source sentence E by
P(E|E). For instance, chunks of “show me” and
“the one” were derived. The process is mod-
eled by two steps:
(a) Selection of chunk size (Head Model).
For each word E
i
, assign the chunk size
ϕ
i
using the head modelepsilon1(ϕ
i
|E
i
). A word
with chunk size more than 0 (ϕ
i
> 0) is
treated as a head word, otherwise a non-
head (refer to the words in bold in Figure
4).
(b) Associate each non-head word to a head
word (Chunk Model). Each non-head
word E
i
is associated to a head word E
h
by
the probabilityη(c(E
h
)|h−i,c(E
i
)), where
h is the position of a head word and c(E)
is a function to map a word E to its word
class (i.e. POS). For instance, “the
3
”is
associated with the head word “one
4
” lo-
cated at 4−3=+1.
2. Select words to be translated with Deletion and
Fertility Model.
(a) Select the number of head words. For each
head word E
h
(ϕ
h
>0), choose fertilityφ
h
according to the Fertility Modelν(φ
h
|E
h
).
We assume that the head word must be
translated, therefore φ
h
> 0. In addition,
one of them is selected as a head word at
target position using a uniform distribu-
tion 1/φ
h
.
(b) Delete some non-head words. For
each non-head word E
i
(ϕ
i
= 0),
delete it according to the Deletion Model
δ(d
i
|c(E
i
),c(E
h
)), where E
h
is the head
word in the same chunk and d
i
is1ifE
i
is deleted, otherwise 0.
3. Insert some words. In Figure 4, NULLs were
inserted for two chunks. For each chunk E
i
,
select the number of spurious words φ
prime
i
by In-
sertion Modelι(φ
prime
i
|c(E
h
)), where E
h
is the head
word ofE
i
.
4. Translate word-by-word. Each source word E
i
,
including spurious words, is translated to J
j
ac-
cording to the Lexicon Model,τ(J
j
|E
i
).
5. Reorder words. Each word in a chunk is
reordered according to the Reorder Model
P(A
j
|E
A
j
,J
j
). The chunk reordering is taken
after the Distortion Model of IBM Model 4,
where the position is determined by the relative
position from the head word,
P(A
j
|E
A
j
,J
j
)=
|A
j
|
productdisplay
k=1
ρ(k−h|c(E
A
A
j
,k
),c(J
j,k
))
where h is the position of a head word for the
chunkJ
j
. For example, “no” is positioned−1
of “uindo”.
show
1
show show show mise mise uindo
1
me
2
me show show tekudasai tekudasai no
2
the
3
the NULL o shinamono shinamono
3
one
4
one one one shinamono oo
4
in
5
in mise
5
the
6
the NULL no uindo tekudasai
6
window
7
window window window uindo no
Chunking
Deletion
& Fertility
Insertion Lexicon Reorder
Chunk
Reorder
Figure 4: Chunk-based translation model. The words in bold are head words.
6. Reorder chunks. All of the chunks are
reordered according to the Chunk Reorder
Model, P(A|E,J). The chunk reordering is
also similar to the Distortion Model, where the
positioning is determined by the relative posi-
tion from the previous alignment
P(A|E,J)=
|J|
productdisplay
j=1
rho1( j− j
prime
|c(E
A
j
−1,h
prime),c(J
j,h
))
where j
prime
is the chunk alignment of the the pre-
vious chunk aE
A
j
−1
. h and h
prime
are the head word
indices for J
j
and E
A
j
−1
, respectively. Note
that the reordering is dependent on head words.
To summarize, the chunk-based translation model
can be formulated as
P(J|E) =
summationdisplay
E,J,A,A
productdisplay
i
epsilon1(ϕ
i
|E
i
)
×
productdisplay
i:ϕ
i
=0
η(c(E
h
i
)|h
i
−i,c(E
i
))
×
productdisplay
i:ϕ
i
>0
ν(φ
i
|E
i
)/φ
i
×
productdisplay
i:ϕ
i
=0
δ(d
i
|c(E
i
),c(E
h
i
))
×
productdisplay
i:ϕ
i
>0
ι(φ
prime
i
|c(E
i
))×
productdisplay
j
productdisplay
k
τ(J
j,k
|E
A
j,k
)
×
productdisplay
j
P(A
j
|E
A
j
,J
j
)×P(A|E,J)
.
3.2 Characteristics of chunk-based Translation
Model
The main diﬀerence to the word alignment based
translation model is the treatment of the bag of
word translations. The word alignment based trans-
lation model generates a bag of words for each
source word, while the chunk-based model con-
structs a set of target words from a set of source
words. The behavior is modeled as a chunking pro-
cedure by first associating words to the head word
of its chunk and then performing chunk-wise trans-
lation/insertion/deletion.
The complicated word alignment is handled by
the determination of word positions in two stages:
translation of chunk and chunk reordering. The for-
mer structures local orderings while the latter con-
stitutes global orderings. In addition, the concept of
head associated with each chunk plays the central
role in constraining diﬀerent levels of the reordering
by the relative positions from heads.
3.3 Parameter Estimation
The parameter estimation for the chunk-based trans-
lation model relies on the EM-algorithm (Dempster
et al., 1977). Given a large bilingual corpus the
conditional probability of P(J,A,A,E|J,E) =
P(J,J,A,A,E|E)/
summationtext
J,A,A,E
P(J,J,A,A,E|E)is
first estimated for each pair of J and E (E-step),
then each model parameters is computed based
on the estimated conditional probability (M-step).
The above procedure is iterated until the set of
parameters converge.
However, this naive algorithm will suﬀer from se-
vere computational problems. The enumeration of
all possible chunkingsJ andEtogether with word
alignmentAand chunk alignment A requires a sig-
nificant amount of computation. Therefore, we have
introduced a variation of the Inside-Outside algo-
rithm as seen in (Yamada and Knight, 2001) for E-
step computation. The details of the procedure are
described in Appendix A.
In addition to the computational problem, there
exists a local-maximum problem, where the EM-
Algorithm converges to a maximum solution but
does not guarantee finding the global maximum. In
order to solve this problem and to make the pa-
rameters converge quickly, IBM Model 4 parame-
ters were used as the initial parameters for training.
We directly applied the Lexicon Model and Fertility
Model to the chunk-based translation model but set
other parameters as uniform.
3.4 Decoding
The decoding algorithm employed for this chunk-
based statistical translation is based on the beam
search algorithm for word alignment statistical
translation presented in (Tillmann and Ney, 2000),
which generates outputs in left-to-right order by
consuming input in an arbitrary order.
The decoder consists of two stages:
1. Generate possible output chunks for all possi-
ble input chunks.
2. Generate hypothesized output by consuming
input chunks in arbitrary order and combining
possible output chunks in left-to-right order.
The generation of possible output chunks is es-
timated through an inverted lexicon model and
sequences of inserted strings (Tillmann and Ney,
2000). In addition, an example-based method is
also introduced, which generates candidate chunks
by looking up the viterbi chunking and alignment
from a training corpus.
Since the combination of all possible chunks is
computationally very expensive, we have introduced
the following pruning and scoring strategies.
beam pruning: Since the search space is enor-
mous, we have set up a size threshold to main-
tain partial hypotheses for both of the above
two stages. We also incorporated a threshold
for scoring, which allows partial hypotheses
with a certain score to be processed.
example-based scoring: Input/output chunk pairs
that appeared in a training corpus are “re-
warded” so that they are more likely kept in
the beam. During the decoding process, when
a pair of chunks appeared in the first stage, the
score is boosted by using this formula in the log
domain,
log P
tm
(J|E) + log P
lm
(E)
Table 1: Basic Travel Expression Corpus
Japanese English
# of sentences 171,894
# of words 1,181,188 1,009,065
vocabulary size 20472 16232
# of singletons 82,06 5,854
3-gram perplexity 23.7 35.8
+ weight×
summationdisplay
j
freq(E
A
j
,J
j
)
in which P
tm
(J|E) and P
lm
(E) are translation
model and language model probability, respec-
tively
1
, freq(E
A
j
,J
j
) is the frequency for the
pair E
A
j
and J
j
appearing in the training cor-
pus, and weight is a tuning parameter.
4 Experiments
The corpus for this experiment was extracted from
the Basic Travel Expression Corpus (BTEC), a col-
lection of conversational travel phrases for Japanese
and English (Takezawa et al., 2002) as seen in Ta-
ble 1. The entire corpus was split into three parts:
152,169 sentences for training, 4,846 sentences for
testing, and the remaining 10,148 sentences for pa-
rameter tuning, such as the termination criteria for
the training iteration and the parameter tuning for
decoders.
Three translation systems were tested for compar-
ison:
model4: Word alignment based translation model,
IBM Model 4 with a beam search decoder.
chunk3: Chunk-based translation model, limiting
the maximum allowed chunk size to 3.
model3+: chunk3 with example-based chunk can-
didate generation.
Figure 5 shows some examples of viterbi chunking
and chunk alignment for chunk3.
Translations were carried out on 510 sentences se-
lected randomly from the test set and evaluated ac-
cording to the following criteria with 16 reference
sets.
WER: Word-error-rate, which penalizes the edit distance
against reference translations.
1
For simplicity of notation, dependence on other variables
are omitted, such asJ.
[ i * have ][the * number ][of my * passport ]
[ *�����we][*j�w�Q][x*K��b]
[ i*have][a * stomach ache ][ please * give me ][some * medicine ]
[S�U*�M][*wp][* �][*<^M]
[ *i][*’d][* like ][a * table ][*for][*two][by the * window ][* if possible ]
[ *pV�y][
�][t*K�][ *�
;][w*�����][�mS*&M][*`hM][*wpbU]
[ i∗have ][a∗reservation ][∗for ][two∗nights ][my∗name is ][∗risa kobayashi ]
[�∗'][∗w]['��∗`][oM�∗wpb][U∗�
�x][	��∗��pb]
Figure 5: Examples of viterbi chunking and chunk alignment for English-to-Japanese translation model.
Chunks are bracketed and the words with∗to the left are head words.
Table 2: Experimental results for Japanese–English
translation
Model WER PER BLEU SE [%]
[%] [%] [%] AA+BA+B+C
model4 43.3 37.2 46.5 59.2 74.1 80.2
chunk3 40.9 36.1 48.4 59.8 73.5 78.8
chunk3+ 38.5 33.7 52.1 65.1 76.3 80.6
PER: Position independent WER, which penalizes without
considering positional disfluencies.
BLEU: BLEU score, which computes the ratio of n-gram for
the translation results found in reference translations (Pa-
pineni et al., 2002).
SE: Subjective evaluation ranks ranging from A to D
(A:Perfect, B:Fair, C:Acceptable and D:Nonsense),
judged by native speakers.
Table 2 summarizes the evaluation of Japanese-to-
English translations, and Figure 6 presents some of
the results by model4 and chunk3+.
As Table 2 indicates, chunk3 performs better than
model4 in terms of the non-subjective evaluations,
although it scores almost equally in subjective eval-
uations. With the help of example-based decoding,
chunk3+was evaluated as the best among the three
systems.
5 Discussion
The chunk-based translation model was originally
inspired by transfer-based machine translation but
modeled by chunks in order to capture syntax-based
correspondence. However, the structures evolved
into complicated modeling: The translation model
involves many stages, notably chunking and two
kinds of reordering, word-based and chunk-based
alignments. This is directly reflected in parameter
input:���(wY�x\�p
��pbT
reference: is this all the baggage from flight one five two
model4: is this all you baggage for flight one five two
chunk3: is this all the baggage from flight one five two
input:�	���������pS&M`�b
reference: may i have room service for breakfast please
model4: please give me some room service please
chunk3: i ’d like room service for breakfast
input:�`�`~D	G��w'��!�`hMwpbU
reference: hello i ’d like to change my reservation for march nineteenth
model4: i ’d like to change my reservation for ninety days be march hello
chunk3: hello i ’d like to change my reservation on march nineteenth
input:�~�4lo<^M?��s�pb
reference: wait a couple of minutes i ’m telephoning now
model4: is this the line is busy now a few minutes
chunk3: i ’m on another phone now please wait a couple of minutes
Figure 6: Translation examples by word alignment
based model and chunk-based model
estimation, where chunk3 took 20 days for 40 iter-
ations, which is roughly the same amount of time
required for training IBM Model 5 with pegging.
The unit of chunk in the statistical machine
translation framework has been extensively dis-
cussed in the literature. Och et al. (1999) pro-
posed a translation template approach that com-
putes phrasal mappings from the viterbi align-
ments of a training corpus. Watanabe et al. (2002)
used syntax-based phrase alignment to obtain
chunks. Marcu and Wong (2002) argued for a dif-
ferent phrase-based translation modeling that di-
rectly induces a phrase-by-phrase lexicon model
from word-wise data. All of these methods bias
the training and/or decoding with phrase-level ex-
amples obtained by preprocessing a corpus (Och et
al., 1999; Watanabe et al., 2002) or by allowing a
lexicon model to hold phrases (Marcu and Wong,
2002). On the other hand, the chunk-based transla-
tion model holds the knowledge of how to construct
a sequence of chunks from a sequence of words. The
former approach is suitable for inputs with less de-
viation from a training corpus, while the latter ap-
proach will be able to perform well on unseen word
sequences, although chunk-based examples are also
useful for decoding to overcome the limited context
of a n-gram based language model.
Wang (1998) presented a diﬀerent chunk-based
method by treating the translation model as a phrase-
to-string process. Yamada and Knight (2001) fur-
ther extended the model to a syntax-to-string trans-
lation modeling. Both assume that the source part
of a translation model is structured either with a se-
quence of chunks or with a parse tree, while our
method directly models a string-to-string procedure.
It is clear that the string-to-string modeling with hi-
den chunk-layers is computationally more expensive
than those structure-to-string models. However, the
structure-to-string approaches are already biased by
a monolingual chunking or parsing, which, in turn,
might not be able to uncover the bilingual phrasal or
syntactical constraints often observed in a corpus.
Alshawi et al. (2000) also presented a two-level
arranged word ordering and chunk ordering by a hi-
erarchically organized collection of finite state trans-
ducers. The main diﬀerence from our work is that
their approach is basically deterministic, while the
chunk-based translation model is non-deterministic.
The former method, of course, performs more ef-
ficient decoding but requires stronger heuristics to
generate a set of transducers. Although the latter
approach demands a large amount of decoding time
and hypothesis space, it can operate on a very broad-
coverage corpus with appropriate translation model-
ing.
Acknowledgments
The research reported here was supported in part by
a contract with the Telecommunications Advance-
ment Organization of Japan entitled “A study of
speech dialogue translation technology based on a
large corpus”.
References
Hiyan Alshawi, Srinivas Bangalore, and Shona Douglas.
2000. Learning dependency translation models as col-
lections of finite state head transducers. Computa-
tional Linguistics, 26(1):45–60.
Peter F. Brown, Stephen A. Della Pietra, Vincent J. Della
Pietra, and Robert L. Mercer. 1993. The mathematics
of statistical machine translation: Parameter estima-
tion. Computational Linguistics, 19(2):263–311.
A. P. Dempster, N.M. Laird, and D.B.Rubin. 1977.
Maximum likelihood from incomplete data via the em
algorithm. Journal of the Royal Statistical Society,
B(39):1–38.
Daniel Marcu and William Wong. 2002. A phrase-based,
joint probability model for statistical machine transla-
tion. In Proc. of EMNLP-2002, Philadelphia, PA, July.
Franz Josef Och, Christoph Tillmann, and Hermann Ney.
1999. Improved alignment models for statistical ma-
chine translation. In Proc. of EMNLP/WVLC, Univer-
sity of Maryland, College Park, MD, June.
Kishore Papineni, Salim Roukos, Todd Ward, and Wei-
Jing Zhu. 2002. Bleu: a method for automatic eval-
uation of machine translation. In Proc. of ACL 2002,
pages 311–318.
Toshiyuki Takezawa, Eiichiro Sumita, Fumiaki Sugaya,
Hirofumi Yamamoto, and Seiichi Yamamoto. 2002.
Toward a broad-coverage bilingual corpus for speech
translation of travel conversations in the real world. In
Proc. of LREC 2002, pages 147–152, Las Palmas, Ca-
nary Islands, Spain, May.
Christoph Tillmann and Hermann Ney. 2000. Word
re-ordering and dp-based search in statistical machine
translation. In Proc. of the COLING 2000, July-
August.
Ye-Yi Wang. 1998. Grammar Inference and Statis-
tical Machine Translation. Ph.D. thesis, School of
Computer Science, Language Technologies Institute,
Carnegie Mellon University.
Taro Watanabe, Kenji Imamura, and Eiichiro Sumita.
2002. Statistical machine translation based on hierar-
chical phrase alignment. In Proc. of TMI 2002, pages
188–198, Keihanna, Japan, March.
Kenji Yamada and Kevin Knight. 2001. A syntax-based
statistical translation model. In Proc. of ACL 2001,
Toulouse, France.
Appendix A Inside-Outside Algorithm for
Chunk-based Translation
Model
The basic idea of inside-outside computation is to
separate the whole process into two parts, chunk
translation and chunk reordering. Chunk transla-
tion handles translation of each chunk, while chunk
reordering performs chunking and chunk reoprder-
ing. The inside (backward or beta) probabilities
can be derived, which represent the probability of
source/target paring of chunks and sentences. The
outside (forward or alpha) probabilities can be de-
fined as the probability of a particular source and
target pair appearing at a particular chunking and re-
ordering.
Inside Probability First, given E and J, compute
chunk translation inside probabilities for all the pos-
sible source and target chunks pairing E
i
prime
i
and J
j
prime
j
in
which E
i
prime
i
is the chunk ranging from index i to i
prime
,
β(E
i
prime
i
,J
j
prime
j
) =
summationdisplay
A
prime
P(A,J
j
prime
j
|E
i
prime
i
)
=
summationdisplay
A
prime
productdisplay
θ
P
θ
(A
prime
,J
j
prime
j
,E
i
prime
i
)
where P
θ
is the probability of a model with asso-
ciated values for corresponding random variables,
such asepsilon1(ϕ
i
|E
i
)orτ(J
j
|E
i
), except for the chunk re-
order modelrho1. A
prime
is a word alignment for the chunks
E
i
prime
i
and J
j
prime
j
.
Second, compute the inside probability for sen-
tence pairs E and J by considering all possible
chunkings and chunk alignments.
β(E,J) =
summationdisplay
E,J:|E|=|J|
summationdisplay
A
P(A,E,J,J|E)
=
summationdisplay
E,J:|E|=|J|
summationdisplay
A
P(A|E,J)
productdisplay
j
β(E
A
j
,J
j
)
Outside Probability The outside probability for
sentence pairing is always 1.
α(E,J)=1.0
The outside probabilities for each chunk pair is
α(E
i
prime
i
,J
j
prime
j
) = α(E,J)
summationdisplay
E,J:|E|=|J|
summationdisplay
A
P(A|E,J)
×
productdisplay
E
A
k
nequalE
i
prime
i
,J
k
nequalJ
j
prime
j
β(E
A
k
,J
k
)
.
Inside-Outside Computation The combination
of the above inside-outside probabilities yields the
following formulas for the accumulated counts of
pair occurrences.
First, the counts for each model parameterθwith
associated random variables count
θ
(Θ)is
count
θ
(Θ) =
summationdisplay
<E,J>
summationdisplay
Θ(A
prime
,E
i
prime
i
,J
j
prime
j
)
α(E
i
prime
i
,J
j
prime
j
)/β(E,J)
×
productdisplay
θ
prime
P
θ
prime(A
prime
,J
j
prime
j
,E
i
prime
i
)
.
Second, the count for chunk reordering with asso-
ciated random variables count
rho1
(Θ)is
count
rho1
(Θ) =
summationdisplay
<E,J>
α(E,J)/β(E,J)
summationdisplay
Θ(A,E,J)
P
rho1
(A|E,J)
productdisplay
k
β(E
A
k
,J
k
)
.
Approximation Even with the introduction of the
inside-outside parameter estimation paradigm, the
enumeration of all possible chunk pairing and word
alignment requires O(lmk
4
(k + 1)
k
) computations,
where l and m are sentence length for E and J, re-
spectively, and k is the maximum allowed number
of words per chunk. In addition, the enumeration
of all possible alignments for all possible chunked
sentences is O(2
l
2
m
n!), where n=|J|=|E|.
In order to handle the massive amount of compu-
tational demand, we have applied an approximation
to the inside-outside estimation procedure. First,
the enumeration of word alignment computation for
chunk translations was approximated by a set of
alignments, the viterbi alignment and neighboring
alignment through move/swap operations of partic-
ular word alignments.
Second, the chunk alignment enumeration was
also approximated by a set of chunking and chunk
alignments as follows.
1. Determines the number of chunks per sentence
2. Determine initial chunking and alignment
3. Compute viterbi chunking-alignment via hill-climbing
using the following operators
• Move boundary of chunk
• Swap chunk alignment
• Move head position
4. Compute neighboring chunking-alignment using the
above operators
