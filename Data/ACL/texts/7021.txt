References

1   S. A�t-Mokhtar. L'analyse pr�syntaxique en une seul �tape. PhD thesis, Universit� Blaise Pascal, 1998. 

2   S. A�t-Mokhtar, J-P Chanod, and C. Roux. A multi-input dual-entry dependency parser. In Proceedings of IWPT 2001, Beijing, 2001. 

3   G. B�s. La phrase verbale noyau en fran�ais. Recherches sur le fran�ais parl�, 15, 1999. 

4   G B�s and P. Blache. Propri�t�s et analyse d'un langage. In Actes de TALN 99, July Carg�se, 1999. 

5   G B�s and C. Hag�ge. Properties in 5p. Technical report, Groupe de Recherche dans les Industries de la Langue (GRIL), URL:lgril.univ-bpclermont.fr, 2001. 

6   G B�s, C. Hag�ge, and L. Coheur. Des propri�t�s linguistiques � l'analyse d'une langue. In Proceedings of the VEXTAL Conference, November Venice, 1999. 

7   Philippe Blache, Constraints, Linguistic Theories and Natural Language Processing, Proceedings of the Second International Conference on Natural Language Processing, p.221-232, June 02-04, 2000 

8   Pollard C. and I. Sag. Head-Driven Phrase Structure Grammar. CSLI Lecture Notes. Center for the Study of Language and Information, 1994. 

9   C. Hag�ge. Analyse syntaxique automatique du portugais. PhD thesis, Universit� Blaise Pascal, 2000. 

10   C. Hag�ge and G. B�s. Da observa��o de propriedades lingu�sticas � sua formaliza��o numa gram�tica do processamento da l�ngua. In Actas do III Encontro para o Processamento Computacional da L�ngua Portuguesa (PROPOR'98), Porto Alegre, 1998. 

11   Carl Pollard , Ivan A. Sag, Information-based syntax and semantics: Vol. 1: fundamentals, Center for the Study of Language and Information, Stanford, CA, 1988 

12   I. Sag and T. Wasow. Syntactic Theory: A formal Introduction. Center for the Study of Language and Information, Stanford University, 1999. 

13   L. Tesni�re. El�ments de syntaxe structurale. Klincksiek, 1969. 

14   Pasi Tapanainen , Timo J�rvinen, A non-projective dependency parser, Proceedings of the fifth conference on Applied natural language processing, p.64-71, March 31-April 03, 1997, Washington, DC 

15   T. Torris and P. Miller. Formalismes syntaxiques pour le traitement automatique du langage naturel. Herm�s, 1990. 
