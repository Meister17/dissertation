<!--
ID:35725804
TITLE:Biometrics make inroads 
SOURCE_ID:3240
SOURCE_NAME:Banker
AUTHOR:
NUMBER:957
ISSUE_DATE:2005-11-30
RECORD_DATE:2006-01-10


RBR_ID:1093
RBR_NAME:���������� ����
RBR_ID:2039
RBR_NAME:���������� �������
RBR_ID:2046
RBR_NAME:�������������� � ���������� �����, ������������ � ������ ����������
RBR_ID:10007
RBR_NAME:�������� �������������� � ����������
-->
<html>
<head>
<title>
35725804-Biometrics make inroads 
</title>
</head>
<body >


<P class=fancy align=justify><STRONG>Although not yet in general use, biometrics are slowly being tested and adopted in the banking sphere as a way of increasing security, <I>Wendy Atkins</I> reports.</STRONG></P>
<P align=justify>Five years ago, the average consumer&#8217;s reaction to biometrics was generally either quizzical, because they did not understand how it worked or believed that it belonged in the secret service fiction of James Bond, or suspicious because they did not want to disclose such personal information. All that has now changed. Consumers are increasingly aware of biometrics, with biometric passports and numerous proposals for biometric ID cards now becoming a reality. What does this mean for the banking sector?</P>
<P align=justify>While there is still a general feeling that it is too soon to adopt biometrics &#8211; especially as payment organisations are still trying to get to grips with Europay Mastercard Visa (EMV) requirements &#8211; a steady flow of customer-facing implementations and pilots has been taking place, with surprisingly positive results.</P>
<P><B>Tight security</B></P>
<P align=justify>The security advantage of biometric technology is that it cannot be borrowed, lost or stolen, and it can authenticate an individual by measuring their distinct physical characteristics or behavioural traits.</P>
<P align=justify>Physical characteristics measured by biometrics include: face, fingerprint, iris, hand geometry and retina.</P>
<P align=justify>Behavioural characteristics measured by biometrics include: dynamic signature, gait recognition, keystroke recognition and speaker verification.</P>
<P align=justify>In Israel, several banking organisations, including Bank Hapoalim, have adopted biometric signature technology to verify customers for secure payments transactions. Unlike other biometrics, this approach is transparent to the user. Consequently, acceptance has been high because there is little difference in the way that the consumer interacts with the bank compared with pre-biometric days.</P>
<P><B>At the fingertips</B></P>
<P align=justify>Signature technology is not the only biometric that is being used more. Fingerprint technology (which will be applied to many new e-passports in the long-term) is also being used in the payments market. Automated teller machine (ATM) manufacturer Diebold has supplied fingerprint-enabled ATMs to a bank in Chile as part of a pilot project, and NCR has installed 400 fingerprint-enabled terminals at BanCafe in Columbia. The ATMs, purchased at the end of 2002, have been established to improve security &#8211; customers do not need to carry ATM cards &#8211; and to encourage people to open accounts.</P>
<P align=justify>Fingerprint technology is being applied to the general payments arena through services such as Pay By Touch. This allows users to pay for purchases or to cash cheques with the touch of a finger. Retail outlets that have piloted this technology include the UK&#8217;s Oxford, Swindon and Gloucester branches of Cooperative Retail, as well as Piggly Wiggly, a south-eastern US grocery chain.</P>
<P align=justify>The banking sector is now using hand geometry technology, which has been used successfully for border and access control for several years, for staff and customer access control. For example, more than 300 banks in North America have deployed Diebold&#8217;s hand geometry system to clear customers into vaults so that they can open their safe-deposit boxes.</P>
<P align=justify>The use of biometrics has also been pioneered in south-east Asia. For example, Japan&#8217;s Suruga Bank has adopted an ATM system that verifies customers by the pattern of the blood vessels on the palms of their hands.</P>
<P align=justify>Voice technology is being used in some phone banking applications. Brazil&#8217;s Banco Bradesco uses speaker verification technology from Nuance to enable customers to check bank balances, make transactions, pay bills and transfer funds via the phone.</P>
<P align=justify>Iris technology, which has attracted a lot of media attention over the years, has so far failed to make any real impact on financial services. However, products are now being aimed at the sector. For example, one of Iridian&#8217;s latest cameras can photograph a user&#8217;s iris from a distance of about 18 inches (46cm).</P>
<P><B>The confidence key</B></P>
<P align=justify>With interest in the technology starting to grow and an increasing number of technologies on the market, any payments organisation considering deploying it needs to consider how it will be used. Is it for staff access or for consumer use? If it is for consumer use, the bank must have confidence that the system will not reject a genuine customer.</P>
<P align=justify>&#8220;Fingerprint is now the most frequently used biometric for customer-facing applications. But iris is probably one of the best from an inability-to-fool point of view,&#8221; says Michael Redding, a partner at Accenture. &#8220;However, usability is still a little tricky because people need to be trained to use the system.&#8221;</P>


</body>
</html>

