<!--
ID:35725806
TITLE:Get the rules right 
SOURCE_ID:3240
SOURCE_NAME:Banker
AUTHOR:
NUMBER:957
ISSUE_DATE:2005-11-30
RECORD_DATE:2006-01-10


RBR_ID:1093
RBR_NAME:���������� ����
RBR_ID:4939
RBR_NAME:������������� ���������� ������������
-->
<html>
<head>
<title>
35725806-Get the rules right 
</title>
</head>
<body >


<P class=fancy><STRONG>As US concerns over minimum regulatory capital requirements come late in the day, the Basel Committee must seriously consider delaying implementation, says <I>John D Hawke Jr</I>.</STRONG></P>
<P class=fancy align=justify>On September 30 the US bank regulatory agencies announced a revised schedule for the implementation of Basel II, the new capital standards for internationally active banks.</P>
<P class=caption align=justify>Under the revised schedule, US banks will start their one-year &#8220;parallel run&#8221; &#8211; tracking capital under both the old and new standards &#8211; in January 2008. Over the following three years, Basel II will be in place, subject to declining &#8220;floors&#8221; on the amount by which the capital of US banks will be permitted to decline under the new rules.</P>
<P align=justify>European banks will still be required to press forward with the earlier schedule, which requires full implementation of the simpler approaches under Basel II at the beginning of 2007, and the advanced internal ratings-based approach at the beginning of 2008. Thus, criticism of the US decision may be somewhat muted in Europe by the expectation that European banks will have a one-year competitive advantage in being able to move to lower regulatory capital requirements.</P>
<P align=justify>What is really going on here? Why are European regulators so adamant in pressing forward, while the US regulators are sounding strong cautionary warnings? There is no easy answer.</P>
<P><B>US stimulus</B></P>
<P align=justify>The US decision was motivated primarily by the results of QIS4, the most recent study of the quantitative impact of Basel II, which was completed in the US earlier this year. This study, like QIS3 before it, showed that Basel II would result in a wide dispersion of changes in the minimum regulatory capital (MRC) requirements of US banks.</P>
<P align=justify>Overall, QIS4 indicated that the impact for individual banks might range from an increase of 56% to a decrease of 47% in MRC, with an average reduction of 17% and a median reduction of 26%.</P>
<P align=justify>Similar volatility was seen in the impact on individual portfolios. The average change in wholesale credit was a reduction of 25%, and in retail credit of 26%. The average MRC for home equity and residential mortgage portfolios was expected to reduce by 74% and 62%, respectively, while MRC for credit card portfolios was expected to increase by 66%. This was undoubtedly due to the fact that Basel II would, for the first time,, require capital to be held against undrawn credit card lines or &#8220;open to buy&#8221; positions.</P>
<P align=justify>There was uncertainty about the causes of this volatility. The regulators said they would be doing more work to determine whether these results reflected actual differences in risk, limitations in the QIS4 methodology or variations in the reporting banks&#8217; Basel II readiness. But the prospect of such wide swings in the impact on MRC must be a matter of concern not only for regulators, but also for legislators. That was made clear in the US Congress at a hearing held shortly after the results were announced.</P>
<P align=justify>Why are European regulators and parliamentarians not similarly concerned? Could the European results be so different? Would it be acceptable in Europe for the MRC of an individual bank to decrease by more than 40%? And what would be the economic and political repercussions if individual banks found that their MRC might increase more than 50%?</P>
<P align=justify>From the inception of the Basel II exercise, the Basel Committee on Banking Supervision has steadfastly insisted that the capital accord should not result in a lowering of capital in the banking system as a whole. How can the committee keep faith with that objective in the face of QIS4?</P>
<P><B>Committee unease</B></P>
<P align=justify>It now appears that the committee does have some concerns: it announced earlier this year that it would undertake a QIS5 before the end of this year. The significance of this decision can only be appreciated against the background of opposition in the committee last year even to conduct QIS4.</P>
<P align=justify>In its release announcing QIS5, the committee said that if this new review should &#8220;reveal that the committee&#8217;s objectives on overall capital would not be achieved, the committee is prepared to take actions necessary to address the situation&#8221;. There is a strong sense that such &#8220;actions&#8221; are more likely to be some tinkering with the calibrations than any fundamental restructuring of the basic rules, however.</P>
<P align=justify>While it may be tempting to attribute the US regulators&#8217; decision to political pressures in the US, I do not believe that is the case. Congress has expressed more interest in and concern about Basel II in the past two years than in the preceding five years of the committee&#8217;s work. However, to the extent that congressional concern reflects apprehension about the lowering of capital levels, it is a concern the regulators themselves have.</P>
<P align=justify>One issue that has been given a sympathetic ear in Congress is competitive equity between Basel II banks and non-Basel II banks. Advanced principally by smaller community banks that fear the impact of lower capital for the large banks &#8211; even though they themselves have typically maintained higher capital ratios than the larger banks &#8211; the issue has become a trade association rallying cry.</P>
<P align=justify>The regulators have responded by airing what has come to be called &#8220;Basel IA&#8221;, a proposal that might import some aspects of Basel II&#8217;s &#8220;standardised&#8221; approach for non-Basel II banks, such as an increase in the number of risk weight categories, greater use of external ratings, expanded recognition of financial collateral and guarantees, and graduated risk weights for residential mortgages based on loan-to-value ratios.</P>
<P align=justify>The debate in Congress has taken an unattractive turn. Some members have been using the opportunity to blast the US Federal Reserve for what is seen as a high-handed approach to the selling of Basel II domestically. One member recently accused the Fed of treating Congress like &#8220;ignorant peasants&#8221;. While the Fed is sometimes seen as a bit Olympian in its relationships, the fact is that the Congress did not engage on Basel II until quite late in the game.</P>
<P align=justify>Despite repeated offers by the banking agencies to brief members and staff on Basel early in the process, there was little or no real interest shown until bank lobbyists began to flog such issues as operational risk. This contrasts markedly with other countries, such as Germany, where parliamentarians were sending marching orders to their Basel representatives at an early stage.</P>
<P align=justify>Although there have been threats from some more exercised members to stop Basel II &#8220;in its tracks&#8221;, the prospect of serious legislative intervention at this stage seems unlikely, particularly in light of the regulators&#8217; Basel IA initiative. Although it has been relatively silent on Basel issues, the Treasury Department would not be likely to support legislation that could cause a serious disruption in international financial relationships, as would almost certainly be the case if Congress were to interdict US participation in Basel II.</P>
<P align=justify>The primary objectives of Basel II &#8211; to provide a common set of capital rules for banks competing internationally, and to make those rules significantly reflective of the risks in bank portfolios and operations &#8211; are praiseworthy and exceedingly important. It would be unthinkable in today&#8217;s globalised banking markets to maintain rules for banks operating in the US that are different from those operating abroad. Thus, Basel II must be implemented.</P>
<P align=justify>Although there have been many complaints about the complexity of Basel II, as well as the prospect that widely differing systems of supervision may result in unequal application of the new rules, there are not likely to be major changes in the structure and approach that the committee has chosen.</P>
<P align=justify>That is not to say that the heavy drumbeat from Europe to adhere to the current schedule has merit. It is understandable that there may be weighty political considerations in the EU that lead many to want to bring finality to the process as soon as possible, but we are talking here about a set of rules that is likely to govern global banking for decades to come. It should be far more important to get the rules right than it is to implement them to some arbitrary schedule.</P>
<P><B>Leap in the dark</B></P>
<P align=justify>As Basel II stands today, there is no banking supervisor or parliamentarian in the world who can know with real comfort what the capital impact of Basel II will be, either on individual banks or the banking systems of the world. As important as it is to get Basel II into effect, it is far more important to assure that we are not sowing the seeds of international financial disaster in doing so.</P>
<P align=justify>The Basel Committee has a history of setting deadlines only to extend them as they approach. When the results of QIS5 are in hand, we must hope that the committee will take a hard-nosed look at the need for delaying implementation, rather than trying to patch up the current structure by tinkering with the calibrations.</P>
<P align=justify><I>John D Hawke is a partner at Arnold &amp; Porter LLP, Washington, DC, served as US Comptroller of the Currency from 1998 to 2004, as under secretary of the Treasury for domestic finance from 1995 to 1998, and as general counsel to the Federal Reserve Board from 1975 to 1978.</I></P>


</body>
</html>

