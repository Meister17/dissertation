<!--
ID:35410964
TITLE:������ ��������� ���������� ������
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:������ ������, ����������� ������������ ��������� ��� &#171;�����&#187;, ����� ���������, ���������� �� ������������� ������������ 

NUMBER:7
ISSUE_DATE:2005-07-31
RECORD_DATE:2005-10-14


-->
<html>
<head>
<title>
35410964-������ ��������� ���������� ������
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgwhite>
<TD></TD></TR>
<TR class=bodycolor>
<TD>
<P class=Main align=justify><EM>��� �������� ������� &#8212; ��������� ��������. �� ����� �������� ��������� ��������� ��������, ������� ���������������� ��� ������������ ����. ��� ������� &#171;��� ������, ��� �����&#187; �������� � ���� �������� ��������. ������, ����� ������� ������� &#8212; �� ��, � 97% ������� ����� ����������, ������ ������� ����� ��� ������������� ������������� ���������� �����? � ���, ��������, �������� �������� ���������� �� ������� ���������?</EM></P></TD></TR>
<TR class=bgwhite>
<TD>
<P class=Main align=justify>�� �������� ����� ����������: ��� � �� ����� ���������������� ������� ����� ������������ ����� ���� �������� ��������� ��������? ���, �����, � ������� ��� �������������? �, �������, ����� ��������� ����������� � �� �����������? 
<P class=Main align=justify>����������� � ���������� ������� ��������� ������������. ����� ����� ��������� �� �������������� ����������� �����, ������� ���������������� ����� ������������. ��� ���� ��� ������������ ������ � ����� ����� ��������, ��������� ����� privatebanking, ����������� ����������, ���������, ����������� � ������������� ������������� ������� �������� � ������� ������� �������, �������� ����� ��������, ��� ���������, ����������, ������ � ��� �����. 
<P class=Main align=justify>� ������ �� ����������� ���� ��������� ������������� �������. ���� �� ��� &#8212; ��� � ���� ���������� ����� ��� ����� ������? ����� ����� �������� �� ���� ������ � ����� ������������ ��������� ���������, ����� ����� ������� ������� � ��������� ��������� �������. 
<P class=Main align=justify>�� ���������� ������ �������� ������� ������ ������ ����������� ��������� ���������� � ��������� &#171;������ ������&#187;, �������, � ���� �������, ������ � ����� ������� ��������� &#171;������ ����������&#187; (������������� TNS/Gallup). 
<P class=Main align=justify>������������� ������ ��������� ���������� &#8212; c ������ 2003�� �� ������ 2004 ����. 
<P class=Main align=justify>������������� ������������� &#8212; �����������, �����, ������, �������� �������<SUP>1</SUP>. 
<P class=Main align=justify>������ ����������� ��������� ����������: ������������ ��������; ���������� ��������. 
<P class=Main align=justify>����������� ������: ���������� �������������� � �������� ��� ��� ����� ������ � ������� (�������, ��� ������ �� �� ���������� 4060%, �� ������ � ����� &#8212; 2030%, � �� �������� ������� ��������� ����, ��� �� ������ ����� ����������). 
<P class=Main align=justify>�������������� ������� � �������� ������: ���� TNS/Gallup AdFact. 
<P class=Main align=justify>� ��������� &#171;������ ������&#187; ������ ��������� ������������: 
<UL>
<LI>������; 
<LI>�������� � ������������ ���������; 
<LI>������ ������ (������). </LI></UL>
<P class=Subheader align=center>����� ����� ������ ��� ��������� ���������</P>
<P class=Subheader align=center><A href="reklamab.gif" target=_blank tppabs="http://www.bdm.ru/arhiv/2005/07/reklamab.gif"></A></P>
<P class=Rubrika align=justify><STRONG>����� � �����&#8230; </STRONG>
<P class=Main align=justify>������ � &#171;���������&#187; �������. � ����� ����� ���������� ������� ���������� ����� � ��� � ������ �� ������ ��������� ���� �������� �� 37% �� ��������� � ����������� �������� 2003��. ��������� ���������� ��������� ����� $80 ���. ��� ���������� ������, ������� ��������� ���������� ����������� ������ � ���: ������� &#8212; � �����&shy;������, � ������� &#8212; � �������� � �� ����� ����. ���������� ��������� ���������� &#8212; � ������ ���� � � ������ ������<SUP>2</SUP>. �� ���� ���������, ���������� ��������� ���������� ����������� ����������������� ���������� ����� � ��������������� �������. 
<P class=Subheader align=center>���������� ��������� ���������� � �� ���� � ��������� &#171;������ ������&#187; 
<P class=Main align=center><A href="reklama1b.gif" target=_blank tppabs="http://www.bdm.ru/arhiv/2005/07/reklama1b.gif"></A></P><STRONG>
<P class=RUBRIKA align=justify><STRONG>� ������ ��� �� ��? </STRONG></P>
<P class=Rubrika align=justify></STRONG>� ������ 2003�� �� ������ 2004 ���� � ��������� �������� ���������� ����� 152,9 ����� ��������� ��������� �� ������� ������. 
<P class=Main align=justify>�������� �������� ������� &#8212; �� ��� ����� ��������� ����� 53 ������ ��������� �����������. 
<P class=Main align=justify>������ ����� �� ������ ��������� ��������� ����������� �����. � ����� �� ����� ������� 42,9 ����� ��������� �������, ������������ ���������� ������. 
<P class=Main align=justify>�� ������� ����� &#8212; �����������. �� ���� ������ � ���� ����� 31,6 ����� ��������� ���������. 
<P class=Main align=justify>�� ��������� ����� &#8212; ������ � 16,8 �������� ��������� ���������. 
<P class=Main align=justify>� ���������, ����� ����� �������� ������� � ����������� &#8212; 8,6 ����� ���������. 
<P class=Main align=justify>������� � ������ �������� ���������� ����� ���� ����. ����� ������� �������� ��� � ��������� ��������� ���������� ���������� ���������� � 2004 ���� �� ����� �������������� �������� �� ������. �� ������ ����� &#8212; �����������, �� ��� &#171;�������&#187; � �����. ������� � ����������� � ��� ���� ������ �����������, � 2004 ���� ���� �������� ���� ����������. 
<P class=Main align=justify>���� �������� � ���������� �������������� ���������� ���������, �� �� ��������� ���������� ������������� ��������������� �� ��. ���������� �� ������ ��� ���������� �������� ���������� � �������� ��������� � &#171;��������&#187; �������������. 
<P class=Subheader align=center>������������� ��������� ���������� ������ �� ����� �������������� 
<P class=Main align=center><A href="reklama3b.gif" target=_blank tppabs="http://www.bdm.ru/arhiv/2005/07/reklama3b.gif"></A></P>
<P class=Rubrika align=justify><STRONG>��� ����� ������� ������������. </STRONG>
<P class=Subheader align=justify><STRONG>����������� </STRONG>
<P class=Main align=justify>�� �� ��������������� �� ������� ����� ������ &#8212; 51 (6% �� ������ ����� ��������������������). ��������� ��������������� � 2003 ���� ���� ������������, &#171;�������&#187;, &#171;��������&#187; � ���������� ��������� ����. � �������� ���� ������������ �������� ����������, �� � ���������� �������� ��������� ���������, ������� � ���������. 
<P class=Main align=justify>������ &#8212; �������� ����� ��� ������. ���������� ������������� ���� ��������� ���� ����� ���������� ��������� ���������� �� ������������ ������� (���, ������ �����, ������, ��� � �.�.). ������������� &#171;����������&#187; � ����� �������� �������� &#171;�����&#187; ������. ��� �� ��������� �� ���������� ������� �����, ���� �������� �������� ������ ��� ������ ��������� � ������������ ������ ������� ���������. 
<P class=Main align=justify>���� �� ������� �������� ��������� ���������� � ���� �������� ����, �������� ��������� ���������� �� ����������� ���������� �� ������� ������ � �� �������. ��������� ��������� ��������� ���������� ����������� � ������. 
<P class=Main align=justify>�������� ��� ���������� ������� �� �� &#8212; ������ ������� (������), ����������� � product placement ������������ �������������. ���� ����� ������������ �������� 5��������� (31%) � 15��������� (30%) ������. �� ������� � ��������� ������ &#8212; ����� �������, 30 � 20��������� (15% � 12% ��������������). ���� &#171;�������������&#187; 10��������� ������� &#8212; 6,9%, � 25��������� &#8212; ������ 1%. �� ������� ��������� � ����������� &#8212; ����� 4%. ���� ������ ������ �������� ������ (5 ���.) ����������� ���� &#171;��������&#187;, 10��������� &#8212; ��������������, 15 ������ ������� ��������� ���������� ��������� ����, 20 &#8212; ���������. ����� �������, ������������ ������ ����������� �������������. � ����������� ����� &#171;��������&#187; ����� ������� �������� &#8212; �� ��������������� ������ �� ����������� ������ ����������� �����������. 
<P class=Subheader align=center>������������� ��������� ���������� � ���������� ������� ������� ����� �������������� 
<P class=Main align=center><A href="reklama2b.gif" target=_blank tppabs="http://www.bdm.ru/arhiv/2005/07/reklama2b.gif"></A></P>
<P class=Subheader align=center>������������� ��������� ���������� � ���������� ������� ������� ����� ��������� 
<P class=Subheader align=center><A href="reklama4b.gif" target=_blank tppabs="http://www.bdm.ru/arhiv/2005/07/reklama4b.gif"></A></P>
<P class=Subheader align=justify><STRONG>������ </STRONG>
<P class=Main align=justify>�� ����� ������� ����� ���������� ��������� ��������� � ���������� ����� &#8212; ���� ��������� ��������� ����� 37% �� ������ ����� ��������������������. ������ ���� ����� �����, ������� ����� ������ �� �������������. 
<P class=Main align=justify>������ ��������� � 2003 ���� ��������� ����� ���� ������, &#171;�������&#187;, &#171;��������&#187;, � �� 11 ������� 2004�� ����� �� ������ ����� ����� Citibank, ������� ���� ������ � &#171;�������&#187; ��������� ���� ����� � ���������� ������. 
<P class=Main align=justify>���������� ��������� ���������� ������ ���������� �������������, ������ ����� � ������� � �������� ����������� ������� ����, ��� �� ��. 
<P class=Main align=justify>�� ������������� ������ �������� � �������&shy;���������� ������� � ����� �� ��������� ��������� $56 ���. ��������. ������� �������� ���������������� ��������� ���������, ������ ������������ ���������� �������� �������. �������� �� ��������� ������������ ����� ������� �������: &#171;���������&#187;, &#171;�����������&#187;, &#171;�����������������&#187; � &#171;�������&#187;. ���������, ��� � ������� ����, ����� ������, �� ��������� � 2003�, ���������� ������� � ������� &#171;7 ����&#187;. 
<P class=Main align=justify>���� �������� � ������������� �������, �������������� ��������������, �� � 56% ������� ������� ��������� � ���������� �������. 21% ���������� �� ������������ �������, 15% &#8212; �� ������������ ������. ���������� � ��������� �������� � ����������� �������� ����������� ���� &#8212; 5% � 3% ��������������. 
<P class=Main align=justify>������, ���� ������������� �������� � ����� ������ ���������� ��������������, �� ���������� �������� ��� (42%) ���������� �� ������������ ������� (��� ����������� ���, ��� ������� ����� ������, ��� � ���������� �������), 36% &#8212; �� ���������� � 13% &#8212; �� ������������ ������, 7% &#8212; �� ����������� ������� � 2% &#8212; �� ��������� �������. 
<P class=Main align=justify>�������� ���������� � ������ ������� �������������������� ����������� ����� ��, ��� � � ����� �� ��������� (������������ ������� &#8212; 44,6% ��������� ��������, ���������� ������ &#8212; 31,9%, ������������ ������ &#8212; 12,4%, ����������� ������� &#8212; 10,6%, ��������� ������� &#8212; ����� 1%). 
<P class=Main align=justify>� ��� ���� ������ &#8212; � 57% ������� ������� ������� ���� ������ � ���� �����&shy;����� �������. 
<P class=Subheader align=justify><STRONG>����� </STRONG>
<P class=Main align=justify>������� �� ����� ������������ ����� ���������� � ������� ����. ��������� ��������������� ����� �������� Citibank, ���������� � ���� &#171;��������&#187;. 
<P class=Main align=justify>������ ����������� � �������������� ���� ���������, ������� �����, ������ ����, ����� 7, ���������� ����� � ��� ������. 
<P class=Main align=justify>� 2004 ���� ���������� ������ ��������, ������ � 2003�, ������������ �� ������������� ���������, ���������� �����, ����, �� ����� 7 � ��� ������. 
<P class=Main align=justify>����� ���������������� ��� ������� &#8212; ������ ������ (86%), ������ ����� � ������� ����������� &#8212; 14% �� ������ ���������� ��������� �������. ���������� �������������������� �� ����� �������� &#8212; ����� 6% �� ������ ����� &#171;����&#187; �� �����. ��� �� ����� � ������� ���� ����� �������������� ������ �� ����������� ����� ����� �� �����. � ����� ���������� � ������� ���������� ����� �� ����� �� 11 ������� 2004 ���� � ������������ ������� 2003�� ������� �� 70%, �������� $10 ���. 
<P class=Main align=justify>������������ �������� ������ ��������� ���������� �� ����� � 2003� ���������� � ����� ����, �� � 2004� �������� ���������� &#8212; ���������� �������������� ����� � ������� ����� ������� (����� ������), �� ���������� � ����� � � ����� ����. 
<P class=Subheader align=justify><STRONG>�������� ������� </STRONG>
<P class=Main align=justify>��� � ������������, &#171;�������&#187; � �������� ���� �������������� �������� ������� �� ���������� ����� �������. ������ ���������� ����� �������� ������, ��� � ������ ��������������. 
<P class=Main align=justify>���� ����� � ���������� ��������� ��� ���������� ������� ������������ billboard (��� 3�6 �) &#8212; � 59% �������. Lamppost sign (���������� �� ������ ���������� ���������) ���������� 14% ��������������, trivision (���, ��������� �� �������������� ����������� �����, ����������� ������� ����������� 3 ����) &#8212; 9%, light box (����������, 1,2�1,8 �) &#8212; 5%. 
<P class=Main align=justify>���������� �����&shy;������������� � �������� ������� ���� ��������� ������� �� ������ ���������� ��������� (45%), trivision ���������� 24%, ���� 3x6 � &#8212; 19% ��������������. 
<P class=Subheader align=center>������������� ��������� ���������� � ���������� ������� ������� �� ����� ������� � ���������� ��������� 
<P class=Subheader align=center><A href="reklama5b.gif" target=_blank tppabs="http://www.bdm.ru/arhiv/2005/07/reklama5b.gif"></A></P>
<P class=Rubrika align=justify><STRONG>������� ����, ������� ����&#8230; </STRONG>
<P class=Main align=justify>����� �������, ������� ���������� ����� ����� �������� &#171;������������&#187; ��������. �� �����, ������� ��������� ������, ������������� �� �����������. ��� ���� ��� ��������� � ����� ����������� ��� ������� ���������, ��� � � ������ ������������� ������������<SUP>3</SUP> &#8212; ��� ����� ��������� �������������, ��� � ����� �� ����������. 
<P class=Main align=justify>����������� �������������� ���������� ������ � �������� �������, �������, ��� � ��, ����� ����� �������� ����� ���������� ���, ��� ��������� ������� ������������� ������ ����������� ����� (�������� ��� ��������). 
<P class=Main align=justify>�������������, ����������� ����������� � ������ ������� ���������� �� ����������� ���������� ������ �����, ��� ������������. ������������ ����� �������� �����, ����� � �������� ����������� ��� ���������� &#171;�����&#187; �����, � ��� �������� ������ ��� ���������. �� ��� ������, ��������� ����� ��� ������������ �� ����������� ����������� ��������, ������������ �������&shy;��������������� (��� ������, ��� � ��������) �� �� �����. 
<TABLE cellSpacing=0 cellPadding=0 width="99%" align=center border=1>
<TBODY>
<TR>
<TD width="6%">
<P align=justify><STRONG>&#8470;</STRONG></P></TD>
<TD width="25%">
<P align=justify><STRONG>�/�� �/�����</STRONG></P></TD>
<TD width="14%">
<P align=justify><STRONG>��</STRONG></P></TD>
<TD width="12%">
<P align=justify><STRONG>�����</STRONG></P></TD>
<TD width="15%">
<P align=justify><STRONG>������</STRONG></P></TD>
<TD width="14%">
<P align=justify><STRONG>������� �������</STRONG></P></TD>
<TD width="14%">
<P align=justify><STRONG>�����</STRONG></P></TD></TR>
<TR>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify><STRONG>����� � ���������� ���������</STRONG></P></TD>
<TD>
<P align=justify><STRONG>24 260 742</STRONG></P></TD>
<TD>
<P align=justify><STRONG>10 533 246</STRONG></P></TD>
<TD>
<P align=justify><STRONG>31 235101</STRONG></P></TD>
<TD>
<P align=justify><STRONG>16 921 130</STRONG></P></TD>
<TD>
<P align=justify><STRONG>82 951 119</STRONG></P></TD></TR>
<TR>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>����� ���-50</P></TD>
<TD>
<P align=justify>26 369 699</P></TD>
<TD>
<P align=justify>10 279 275</P></TD>
<TD>
<P align=justify>25 267181</P></TD>
<TD>
<P align=justify>8 951 570</P></TD>
<TD>
<P align=justify>70 368 625</P></TD></TR>
<TR>
<TD>
<P align=justify>1</P></TD>
<TD>
<P align=justify>������������</P></TD>
<TD>
<P align=justify>6 742147</P></TD>
<TD>
<P align=justify>434103</P></TD>
<TD>
<P align=justify>1 448 843</P></TD>
<TD>
<P align=justify>741 680</P></TD>
<TD>
<P align=justify>9 366 773</P></TD></TR>
<TR>
<TD>
<P align=justify>2</P></TD>
<TD>
<P align=justify>�������������� ����</P></TD>
<TD>
<P align=justify>2 141 587</P></TD>
<TD>
<P align=justify>643 600</P></TD>

<TD>
<P align=justify>1 602 205</P></TD>
<TD>
<P align=justify>1 323 910</P></TD>
<TD>
<P align=justify>5 712 202</P></TD></TR>
<TR>
<TD>
<P align=justify>3</P></TD>
<TD>
<P align=justify>CITIBANK</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>1 638547</P></TD>
<TD>
<P align=justify>2 843 986</P></TD>
<TD>
<P align=justify>810100</P></TD>
<TD>
<P align=justify>5 292 633</P></TD></TR>
<TR>
<TD>
<P align=justify>4</P></TD>
<TD>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>3 866 366</P></TD>
<TD>
<P align=justify>21 960</P></TD>
<TD>
<P align=justify>111 207</P></TD>
<TD>
<P align=justify>409 430</P></TD>
<TD>
<P align=justify>4 408 963</P></TD></TR>
<TR>
<TD>
<P align=justify>5</P></TD>
<TD>
<P align=justify>�������</P></TD>
<TD>
<P align=justify>3 202153</P></TD>
<TD>
<P align=justify>88182</P></TD>
<TD>
<P align=justify>521 013</P></TD>
<TD>
<P align=justify>178 350</P></TD>
<TD>
<P align=justify>3 989 698</P></TD></TR>
<TR>
<TD>
<P align=justify>6</P></TD>
<TD>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>92 669</P></TD>
<TD>
<P align=justify>563169</P></TD>
<TD>
<P align=justify>2 314 377</P></TD>
<TD>
<P align=justify>358 690</P></TD>
<TD>
<P align=justify>3 328 905</P></TD></TR>
<TR>
<TD>
<P align=justify>7</P></TD>
<TD>
<P align=justify>���������</P></TD>
<TD>
<P align=justify>2 951 426</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>66 602</P></TD>
<TD>
<P align=justify>69 950</P></TD>
<TD>
<P align=justify>3 087 978</P></TD></TR>
<TR>
<TD>
<P align=justify>8</P></TD>
<TD>
<P align=justify>����1 �.�. �.</P></TD>
<TD>
<P align=justify>2 432 248</P></TD>
<TD>
<P align=justify>179 692</P></TD>
<TD>
<P align=justify>59 894</P></TD>
<TD>
<P align=justify>5 300</P></TD>
<TD>
<P align=justify>2 677134</P></TD></TR>
<TR>
<TD>
<P align=justify>9</P></TD>
<TD>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>610 091</P></TD>
<TD>
<P align=justify>1 284735</P></TD>
<TD>
<P align=justify>220 645</P></TD>
<TD>
<P align=justify>232 850</P></TD>
<TD>
<P align=justify>2 348 321</P></TD></TR>
<TR>
<TD>
<P align=justify>10</P></TD>
<TD>
<P align=justify>���� ���� ����</P></TD>
<TD>
<P align=justify>115 790</P></TD>
<TD>
<P align=justify>1 014 512</P></TD>
<TD>
<P align=justify>1 160 692</P></TD>
<TD>
<P align=justify>4 500</P></TD>
<TD>
<P align=justify>2 2�5 494</P></TD></TR>
<TR>
<TD>
<P align=justify>11</P></TD>
<TD>
<P align=justify>����������������</P></TD>
<TD>
<P align=justify>1 414 945</P></TD>
<TD>
<P align=justify>232 526</P></TD>
<TD>
<P align=justify>203 003</P></TD>
<TD>
<P align=justify>227 900</P></TD>
<TD>
<P align=justify>2 078 374</P></TD></TR>
<TR>
<TD>
<P align=justify>12</P></TD>
<TD>
<P align=justify>������� ����</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>73 331</P></TD>
<TD>
<P align=justify>1 850 976</P></TD>
<TD>
<P align=justify>35 000</P></TD>
<TD>
<P align=justify>1 959 307</P></TD></TR>
<TR>
<TD>
<P align=justify>13</P></TD>
<TD>
<P align=justify>BSGV</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>401 449</P></TD>
<TD>
<P align=justify>1 120 680</P></TD>
<TD>
<P align=justify>175 600</P></TD>
<TD>
<P align=justify>1 697 729</P></TD></TR>
<TR>
<TD>
<P align=justify>14</P></TD>
<TD>
<P align=justify>���������� ������� ���</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>293164</P></TD>
<TD>
<P align=justify>1 242 049</P></TD>
<TD>
<P align=justify>99 200</P></TD>
<TD>
<P align=justify>1 634 413</P></TD></TR>
<TR>
<TD>
<P align=justify>15</P></TD>
<TD>
<P align=justify>���-����</P></TD>
<TD>
<P align=justify>350 388</P></TD>
<TD>
<P align=justify>314 607</P></TD>
<TD>
<P align=justify>893 593</P></TD>
<TD>
<P align=justify>29 050</P></TD>
<TD>
<P align=justify>1 593 643</P></TD></TR>
<TR>
<TD>
<P align=justify>16</P></TD>
<TD>
<P align=justify>��������� ����</P></TD>
<TD>
<P align=justify>859 072</P></TD>
<TD>
<P align=justify>289 734</P></TD>
<TD>
<P align=justify>253159</P></TD>
<TD>
<P align=justify>113 440</P></TD>
<TD>
<P align=justify>1 520 405</P></TD></TR>
<TR>
<TD>
<P align=justify>17</P></TD>
<TD>
<P align=justify>�������� ������� ����</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>1 328 060</P></TD>
<TD>
<P align=justify>50 600</P></TD>
<TD>
<P align=justify>1 378 660</P></TD></TR>
<TR>
<TD>
<P align=justify>18</P></TD>
<TD>
<P align=justify>�������������</P></TD>
<TD>
<P align=justify>487 352</P></TD>
<TD>
<P align=justify>236 236</P></TD>
<TD>
<P align=justify>188 268</P></TD>
<TD>
<P align=justify>142 840</P></TD>
<TD>
<P align=justify>1 054 696</P></TD></TR>
<TR>
<TD>
<P align=justify>19</P></TD>
<TD>
<P align=justify>�������</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>164 612</P></TD>
<TD>
<P align=justify>157 253</P></TD>
<TD>
<P align=justify>589 950</P></TD>
<TD>
<P align=justify>911 815</P></TD></TR>
<TR>
<TD>
<P align=justify>20</P></TD>
<TD>
<P align=justify>���������� ����</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>387 622</P></TD>
<TD>
<P align=justify>407 336</P></TD>
<TD>
<P align=justify>96 650</P></TD>
<TD>
<P align=justify>891 608</P></TD></TR>
<TR>
<TD>
<P align=justify>21</P></TD>
<TD>
<P align=justify>AMERICAN EXPRESS</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>332 880</P></TD>
<TD>
<P align=justify>484 564</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>817 444</P></TD></TR>
<TR>
<TD>
<P align=justify>22</P></TD>
<TD>
<P align=justify>������� ����</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>221 366</P></TD>
<TD>
<P align=justify>425 970</P></TD>
<TD>
<P align=justify>136 200</P></TD>
<TD>
<P align=justify>783 536</P></TD></TR>
<TR>
<TD>
<P align=justify>23</P></TD>
<TD>
<P align=justify>������������ ��������� ����</P></TD>
<TD>
<P align=justify>645 691</P></TD>
<TD>
<P align=justify>33 894</P></TD>
<TD>
<P align=justify>14 994</P></TD>
<TD>
<P align=justify>5100</P></TD>
<TD>
<P align=justify>699 679</P></TD></TR>
<TR>
<TD>
<P align=justify>24</P></TD>
<TD>
<P align=justify>���� ����-������</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>369 370</P></TD>
<TD>
<P align=justify>279 763</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>649133</P></TD></TR>
<TR>
<TD>
<P align=justify>25</P></TD>
<TD>
<P align=justify>�����-����</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>238 972</P></TD>
<TD>
<P align=justify>371 800</P></TD>
<TD>
<P align=justify>610772</P></TD></TR>
<TR>
<TD>
<P align=justify>26</P></TD>
<TD>
<P align=justify>���� ����</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>95 962</P></TD>
<TD>
<P align=justify>262 057</P></TD>
<TD>
<P align=justify>232 380</P></TD>
<TD>
<P align=justify>590 399</P></TD></TR>
<TR>
<TD>
<P align=justify>27</P></TD>
<TD>
<P align=justify>RAJFFEISENBANK ALISTRI�</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>35 689</P></TD>
<TD>
<P align=justify>177 032</P></TD>
<TD>
<P align=justify>361 900</P></TD>
<TD>
<P align=justify>574 621</P></TD></TR>
<TR>
<TD>
<P align=justify>28</P></TD>
<TD>
<P align=justify>�����������</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>140 207</P></TD>
<TD>
<P align=justify>268 301</P></TD>
<TD>
<P align=justify>149150</P></TD>
<TD>
<P align=justify>557 658</P></TD></TR>
<TR>
<TD>
<P align=justify>29</P></TD>
<TD>
<P align=justify>���� ����</P></TD>
<TD>
<P align=justify>395 631</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>100 443</P></TD>
<TD>
<P align=justify>9 000</P></TD>
<TD>
<P align=justify>505 074</P></TD></TR>
<TR>
<TD>
<P align=justify>30</P></TD>
<TD>
<P align=justify>�������������</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>268 913</P></TD>
<TD>
<P align=justify>204 750</P></TD>
<TD>
<P align=justify>473 663</P></TD></TR>
<TR>
<TD>
<P align=justify>31</P></TD>
<TD>
<P align=justify>�����������- ����</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>117115</P></TD>
<TD>
<P align=justify>343 280</P></TD>
<TD>
<P align=justify>460 395</P></TD></TR>
<TR>
<TD>
<P align=justify>32</P></TD>
<TD>
<P align=justify>���� (����)</P></TD>
<TD>
<P align=justify>20 733</P></TD>
<TD>
<P align=justify>48 037</P></TD>
<TD>
<P align=justify>368 385</P></TD>
<TD>
<P align=justify>1 900</P></TD>
<TD>
<P align=justify>439 055</P></TD></TR>
<TR>
<TD>
<P align=justify>33</P></TD>
<TD>
<P align=justify>���������� ��������� ����</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>162 795</P></TD>
<TD>
<P align=justify>245 892</P></TD>
<TD>
<P align=justify>20 300</P></TD>
<TD>
<P align=justify>428 987</P></TD></TR>
<TR>
<TD>
<P align=justify>34</P></TD>
<TD>
<P align=justify>������-���1/�</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>367 394</P></TD>
<TD>
<P align=justify>55 000</P></TD>
<TD>
<P align=justify>422 394</P></TD></TR>
<TR>
<TD>
<P align=justify>35</P></TD>
<TD>
<P align=justify>PARE X BANK</P></TD>
<TD>
<P align=justify>41 410</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>352 562</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>393 972</P></TD></TR>
<TR>
<TD>
<P align=justify>36</P></TD>
<TD>
<P align=justify>������ ��������� ��������</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>36 027</P></TD>
<TD>
<P align=justify>348150</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>384177</P></TD></TR>
<TR>
<TD>
<P align=justify>37</P></TD>
<TD>
<P align=justify>��������� ��������� ����</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>15 320</P></TD>
<TD>
<P align=justify>284 026</P></TD>
<TD>
<P align=justify>72 900</P></TD>
<TD>
<P align=justify>372 246</P></TD></TR>
<TR>
<TD>
<P align=justify>38</P></TD>
<TD>
<P align=justify>������ ��������������� ����</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>21 048</P></TD>
<TD>
<P align=justify>134169</P></TD>
<TD>
<P align=justify>210 870</P></TD>
<TD>
<P align=justify>366 087</P></TD></TR>
<TR>
<TD>
<P align=justify>39</P></TD>
<TD>
<P align=justify>��������������</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>104102</P></TD>
<TD>
<P align=justify>256 750</P></TD>
<TD>
<P align=justify>360 852</P></TD></TR>
<TR>
<TD>
<P align=justify>40</P></TD>
<TD>
<P align=justify>����� ����</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>17 370</P></TD>
<TD>
<P align=justify>236 673</P></TD>
<TD>
<P align=justify>72 850</P></TD>
<TD>
<P align=justify>326 893</P></TD></TR>
<TR>
<TD>
<P align=justify>41</P></TD>
<TD>
<P align=justify>������ ����</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>143 881</P></TD>
<TD>
<P align=justify>145 040</P></TD>
<TD>
<P align=justify>288 921</P></TD></TR>
<TR>
<TD>
<P align=justify>42</P></TD>
<TD>
<P align=justify>���� ��� ����������� ����</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>85 080</P></TD>
<TD>
<P align=justify>45 370</P></TD>
<TD>
<P align=justify>149 650</P></TD>
<TD>
<P align=justify>280 100</P></TD></TR>
<TR>
<TD>
<P align=justify>43</P></TD>
<TD>
<P align=justify>�������������</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>224 931</P></TD>
<TD>
<P align=justify>38 751</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>263 682</P></TD></TR>
<TR>
<TD>
<P align=justify>44</P></TD>
<TD>
<P align=justify>��������������</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>253109</P></TD>
<TD>
<P align=justify>6 400</P></TD>
<TD>
<P align=justify>259 509</P></TD></TR>
<TR>
<TD>
<P align=justify>45</P></TD>
<TD>
<P align=justify>DELTACREDIT</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>241 157</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>241 157</P></TD></TR>
<TR>
<TD>
<P align=justify>46</P></TD>
<TD>
<P align=justify>�������������</P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify></P></TD>
<TD>
<P align=justify>23 897</P></TD>
<TD>
<P align=justify>209 330</P></TD>
<TD>
<P align=justify>238 227</P></TD></TR></TBODY></TABLE>
<P class=Main align=justify><SUP>1</SUP><EM> ��������������� ����� ������ ����������� ��������� ���������� ������ � �����������, �� � ����� � �� �������������� ������� ������ � ���� ������ �� ����������.</EM> 
<P class=Main align=justify><SUP>2</SUP><EM>&nbsp;���� ��������� ���������� � ���� 2004 ���� ����������� ������������ ����� &#171;���������� ��������&#187;. �������� ������������� �������� ���� &#8212; ������������ � �����-����.</EM> 
<P class=Main align=justify><SUP>3</SUP><EM> � ��� ������ �����, ��� ������ �������� ������������� �������� ����� �������� ����.</EM></P></TD></TR></TBODY></TABLE>


</body>
</html>

