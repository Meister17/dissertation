<!--
ID:37462450
TITLE:��������� ��� �����������,
SOURCE_ID:10908
SOURCE_NAME:������������ ���������� ������
AUTHOR:
NUMBER:3
ISSUE_DATE:2007-03-31
RECORD_DATE:2007-03-28


-->
<html>
<head>
<title>
37462450-��������� ��� �����������,
</title>
</head>
<body >


<DIV class=article>
<DIV class=header>
<H2><SPAN><FONT size=3>������� ���� ������, �������������� ����-��������� ���</FONT></SPAN></H2></DIV>
<DIV class=body>
<P align=justify>���, ��� ������������ ����� �����������, ������������ ���������. ��������, ����-�� ��� ����������� ��������� �������. �� ���� ������� ������ ���.</P>
<P align=justify>�� ���� ��������������� ������������� ����. ����� ���, ���������� ������ ���� ����� ���������� �������� �� �������. ����������� ������������� ��������, �������� �������� (������������) ����������. �������������� � �������� ��������� �� ������ � &#171;������� �����&#187;. ��� �� ����� �������� �������� ������ ��������. ������ ����������� �� ������������ ���������� ������. ��������� ����������� ���� ����� ����������� ��� ��������� �������. ����� ����������� ���� �� ������� �����. ����� ����� �������������� �� ������� ����� � ������ ���������. ��� ����� ���������� ���� ����� ��� ��������� ���-�� ���������, �������� � ���� ��������, �����������, ����������, ��������. � ��� ��� ���������������� ������� �������.</P>
<P align=justify></P>
<P align=justify>���� ����� �� ����� ����������� ������ �� ������ ������� �����. ����� ��������� �����������, ���������� ������������������ �� ��������� �� ������� �����, ���������� �� ��������� � �������� ���������� ����, ������� �������� � �� ������� ����� ��� ����. ������� � ��������� �� ��������� �������� � ���, ��� � ��������� ���� ���� ������� ����������� ����� ����� ����������� ������������ �������.</P>
<P align=justify>���������� ���������� ������ �������� ��������� �������� �������� ����� � ������ ��� � 2004 ����. ��������� �� ����� ����������� ���������� ����� ����� ��������� ������, ���� ������ ��������� �� ��������, � ����� ������, ���� � ������ �����������, ��� ��� �� ������, ������� ��� ������� ��� ������� ����������� ���������� ��� ���������� �����. ������ � ���� �������� �������� ������ � ������������ �������� ����������. ������ �������� �������� � �������������� ����� ����������� �������� ������������� ��� ���������� �������� ��������� - ����������� ����������. ��� ��� �� ��������� �� ���� ������� ������ �� ������ ��������. �������� ������� ��������������� ������� ������� �� ����������� �������� ������ �������� ���.</P>
<P align=justify>������� ������� ����� ������� ������������� ���������, ������� ��������� ������ ������� ��������������� ��������. ��� ���������� ��������, ������� �������� ���� ����� ����������� ���������, �� �������� ����� ������������ ������� �� � ����������� ����������������. ��� ����������� � ���, ��� �� ���������� ��� �������������� ����������� ������, ����������� ����� �������������, � ������ ������� ����������.</P></DIV></DIV>


</body>
</html>

