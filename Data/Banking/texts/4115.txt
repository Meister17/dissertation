<!--
ID:38139821
TITLE:������� � ������ ������� ��� ����������� ��� ��������. ������������ ������ ��� �������������?
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:��������� ��������, PR - �������� RUBURO, E-mail:pr@ruburo.ru
NUMBER:17
ISSUE_DATE:2007-09-15
RECORD_DATE:2007-09-12


-->
<html>
<head>
<title>
38139821-������� � ������ ������� ��� ����������� ��� ��������. ������������ ������ ��� �������������?
</title>
</head>
<body >


<B>
<P align=justify>�������� �� �� ��� �� ��� ����� � ����� ������, �������� ������� ����������� ��������� � ������ �������������� ������ ���������������. ������� ������ ����, ��������������� �������, ��� ������, ���������� �� ������������ �������� ���������� ����������� (��������) ��������. �������� ���� ����� � �����������.</P></B>
<P align=justify>���� ������, ��������� �������� � ����� ��������� ������� ������������, ��������� ����� ��������� ���������� ����� ������� ��������������. ��� �� ����� ��������� � � ��������� ����������� �� ���������� ��������, ��� ��������� �������� ��� ���, ������� ���������� � ������� ���������� �������.</P>
<P align=justify>��� ���� ���������� ����������, ��� ������������ �������� &#8211; ���� �� ����� ������ ������������ ����������� �������, ��� ����������� �� ���������� ������, � ������� ��� ��������� (�� ����������� ������������ ������������, � ������� ������ � ���������� ����� 500 ���. �������). ������� � �������� ������ ����� ���� ������ ���������������. ��������, ������� �� ����� ���� ����� �������� � �������� ���� � ����� �����, �� ���-��, ���������� �� ���������� ��� ��������� �������, �� �������� ��� �������� ����� ������������ �� ������.</P>
<P align=justify>�������� ��� ������� ����� ������������� �� ���������� �� ������ ������� ���� ��������, ������������ �����������, ������� �������������, &#8211; ����������� ���������. ������ �� �������� ��� ����������� � ������ ����� ������������ �� ���������� ������������ ���������� ���������� ���������, ��� ���� ������ ���� �� ������ � ���� ������.</P>
<P align=justify>������� ����, ������� �����, ���������� �������� ������ � �.�. &#8211; ��� ��� ������ ������ �������� ����������� ��������� � ������. �� ���� ���� ������� � ���-�� ����� �������, ��� ���������� ����������� ��������� ����� &#8211; �, � ������� � "������������", "�������" ��� "����������" &#8211; �� ��� ��� ������ �� ������ ���������� ���������� ������ ���������� ������ ����� ������� ��� ����������� (���������� ���������� ���� ������, �������, �����������, ��������������, �������, ������� ���� �������� � ���� "������� �������� ������"). ��������� �� ������� ������������ ��� ����� �� �������� � �������� &#8211; ����� ������ ����������� ������ � �������.</P>
<P align=justify>�������, �����������, ������� � ����� ������� ������, ������� ������������ ����, ��� � ������ � ����������, &#8211; ����� ������������ ���������� ������� ������� � ������ �������� ��� ��������� ��������.</P>
<P align=justify>��� �� �������� �����������, �� ��� ����� �� ���� ������� ������ ��������������� ����� ����������� ��������, ����� ���������. � ��� � �������� � ������� ������ ���������� � ������ ������� �� ���������. ����� �� ������������ ������������ ������ ����� ��� ���������� ������, ������������, ���, ��� �������, ��������� &#8211; ����� ���������� ������� � ������� ������ � ������.</P>
<P align=justify>����������, ��� �������, ���������� � ������, �������������� ����������, �����, � �������, ����� ��������� ������, �� �� ������� �������� � ������/��, ������ ����� �� ������ ����� �������. ����� ��� �������������� ������� ����� �������� ������. ���� ��������, ����� ��������� �������� ���������� ����������� ���� &#8211; ����� ���-�� �������������� ��������� � ������������ � ��������� ���������� ����� ��� ������� ����� ������� ����� �������? ������� ��������, ��� ��������� ����������� � �������� ������������ ������������ ������, �� ������ �� � ���, ��������� ����� ������ ��������� �����������. �������� ����������� �� ������� ���������� ���������� ��������� �������� ����������� �������������� ����, ��� ��� ���������� ���������, � �������������� �������� ������� ������� � ����� ����� ����� ������. �� ���� �� ���������� ��������� ������, � �����������, ���������� ������������� ����������� �����, ������������ � ������ ������������ ��� �������������� ������� � ����������� ����������� �� ���������� ������ ������.</P>
<P align=justify>��������� ������� �� ������� ���������� ������ �� ������������� �������. � ���� ������� ���� ���� �� ������������, �������� ������ �� ��������. � ����������� ������� ������� ������ �� �������������� ���������, ��� ���� �� ���� ��� �������� ���������� ���� �����������.</P>
<P align=justify>����� ����������� ������������ �������� RUBURO &#8211; ����������� ����� ������� �� ��������� ��������� ��������� ���������� �� ��������. � ����� ����� ��������� ���� �����, ������� � �������������� � ���� ������ ������ ���������� �� ����������� � ������ ��� ���������� �������.</P>
<P align=justify>���� �� ����� ��������, ���� ��������� "�������" ������������ ������� ����� ������� ������ � ������ ������������� ��-�� ������� ������� ������������� � ����������� ������. ����� �������, ����������, ��� �������������� ������� "������������" ������� �� ������ &#8211; ������������ ���������� ����������� ���������, ������ ������������� ��������� �������, ������ ���� ����� ����������� ���� ����, ���� ���������� ��� ��� ������. � �������, �������� ��������� "������� �����" � ������, ���� ���� ���� �� �������� �� �������, �������� ������� �� ��������� � ������������ ������ ���������� ����������. ����������, �������, � ����� ��������� ���������, ������� �� ���������� �� ������� ������ ������ ��� �������.</P>
<P align=justify>������������� ���������� ������� � ������������� ������� &#8211; �� ����� ������� ����, ������� ���� �����������, ��� ��� ���������� ����� ��������������� ������� ������������. ���� ����� ���������� ��������� ������������ �������� � �� ����� ����������, � �� ����� ������������ ��������.</P>
<P align=justify>� RUBURO �������, ��� ���� ���� �������, ���� � �������, ����� ��������� �����������, �������� � ������������ ���������� � ���������� ��������, �� ����� ������ ����� ������� ������������� ������� � �������������� �����. ���������� �������� � ��������������, ��� ���������� ����������� � ������ �������� ������������ �������� ������������ ������.</P>


</body>
</html>

