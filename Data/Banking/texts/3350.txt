<!--
ID:35997377
TITLE:���������� � ����������� � �������������� ��������� ����������� �� 01.01.2006 �.*
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:3
ISSUE_DATE:2006-02-15
RECORD_DATE:2006-03-21


-->
<html>
<head>
<title>
35997377-���������� � ����������� � �������������� ��������� ����������� �� 01.01.2006 �.*
</title>
</head>
<body >


<P align=justify>* ���������� ������������ � ��� ����� � �� ��������� ��������, ����������� �� ��������������� ��������������� ������ �� �������� ����.</P>
<TABLE cellSpacing=2 cellPadding=9 width="100%" align=center border=1>
<TBODY>
<TR>
<TD vAlign=top colSpan=5><B>
<P align=center>����������� ��������� �����������</B></P></TD></TR>
<TR>
<TD vAlign=top width="70%" colSpan=2>
<P>&nbsp;</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center>�� 01.01.2005</P></TD>
<TD vAlign=top width="15%">
<P align=left>�� 01.01.2006</P></TD></TR>
<TR>
<TD vAlign=top width="70%" colSpan=2><B>
<P align=justify>1. ���������������� ��*<SUP>*</SUP> ������ ������ ���� �� ��������� ��� ������� �������������� �������������� �������, �����<SUP>1</SUP></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>1 516</B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=right>1 409</B></P></TD></TR>
<TR>
<TD vAlign=top width="70%" colSpan=2>
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="70%" colSpan=2>
<UL>
<LI>
<DIV align=justify>������</DIV></LI></UL></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 464</P></TD>
<TD vAlign=top width="15%">
<P align=right>1 356</P></TD></TR>
<TR>
<TD vAlign=top width="70%" colSpan=2>
<UL>
<LI>
<DIV align=justify>������������ ��</DIV></LI></UL></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>52</P></TD>
<TD vAlign=top width="15%">
<P align=right>53</P></TD></TR>
<TR>
<TD vAlign=top width="70%" colSpan=2>
<P align=justify>1.1. ���������������� �� �� 100%-��� ����������� �������� � ��������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>33</P></TD>
<TD vAlign=top width="15%">
<P align=right>42</P></TD></TR>
<TR>
<TD vAlign=top width="70%" colSpan=2>
<P align=justify>1.2. ��, ������������������ ������ ������, �� ��� �� ���������� �������� ������� � �� ���������� �������� (� ������ �������������� �������������� �����)</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1</P></TD>
<TD vAlign=top width="15%">
<P align=right>2</P></TD></TR>
<TR>
<TD vAlign=top width="70%" colSpan=2>
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="70%" colSpan=2>
<UL>
<LI>
<DIV align=justify>�����</DIV></LI></UL></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>0</P></TD>
<TD vAlign=top width="15%">
<P align=right>2</P></TD></TR>
<TR>
<TD vAlign=top width="70%" colSpan=2>
<UL>
<LI>
<DIV align=justify>������������ ��</DIV></LI></UL></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1</P></TD>
<TD vAlign=top width="15%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="70%" colSpan=2><B>
<P align=left>2. ������������ ��, ������������������ �� 01.07.2002 ������� ��������</P></B></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>2</B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=right>0</B></P></TD></TR>
<TR>
<TD vAlign=top colSpan=5><B>
<P align=center>����������� ��������� �����������</B></P></TD></TR>
<TR>
<TD vAlign=top width="70%" colSpan=2><B>
<P align=justify>3. ��, ������� ����� �� ������������� ���������� ��������, �����<SUP>2</SUP></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>1 299</B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=right>1 253</B></P></TD></TR>
<TR>
<TD vAlign=top width="70%" colSpan=2>
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<UL>
<LI>
<DIV align=justify>�����</DIV></LI></UL></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 249</P></TD>
<TD vAlign=top width="15%">
<P align=right>1 205</P></TD></TR>
<TR>
<TD vAlign=top width="70%" colSpan=2>
<UL>
<LI>
<DIV align=justify>������������ ��</DIV></LI></UL></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>50</P></TD>
<TD vAlign=top width="15%">
<P align=right>48</P></TD></TR>
<TR>
<TD vAlign=top colSpan=5><I>
<P align=right>�����������</I></P></TD></TR>
<TR>
<TD vAlign=top width="70%">&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center>�� 01.01.2005</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center>�� 01.01.2006</P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<P align=justify>3.1. ��, ������� �������� (����������), ��������������� ����� ��:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="70%">
<UL>
<LI>
<DIV align=justify>����������� ������� ���������</DIV></LI></UL></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 165</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 045</P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<UL>
<LI>
<DIV align=justify>������������� �������� � ����������� ������</DIV></LI></UL></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>839</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>827</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<UL>
<LI>
<DIV align=justify>����������� ��������</DIV></LI></UL></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>311</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>301</P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<UL>
<LI>
<DIV align=justify>���������� �������� � �������������</DIV></LI></UL></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="70%">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<P align=justify>����������</P></BLOCKQUOTE></BLOCKQUOTE></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>4</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>4</P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<P align=justify>��������<SUP>3</SUP></P></BLOCKQUOTE></BLOCKQUOTE></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>178</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>180</P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<P align=justify>3.2. �� � ����������� �������� � �������� ��������, �����</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>131</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>136</P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<P dir=ltr style="MARGIN-RIGHT: 0px" align=justify>�� ���:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="70%">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<P align=justify>�� 100%-���</P></BLOCKQUOTE></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>33</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>41</P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<P align=left>����� 50%</P></BLOCKQUOTE></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>9</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>11</P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<P align=justify>3.3. ��, ���������� � ������ ����� &#8211; ���������� ������� ������������ �����������</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>930</P></TD></TR>
<TR>
<TD vAlign=top width="70%"><B>
<P align=justify>4. ������������������ �������� ������� ����������� �� (��� ���.)</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>380 468</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>444 377</B></P></TD></TR>
<TR>
<TD vAlign=top width="70%"><B>
<P align=justify>5. ������� ����������� �� �� ���������� ��, �����</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>3 238</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>3 295</B></P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<P dir=ltr style="MARGIN-RIGHT: 0px" align=justify>�� ���:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="70%">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<P align=justify>��������� ������<SUP>4</SUP></P></BLOCKQUOTE></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 011</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 009</P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<P align=justify>������ �� 100%-��� ����������� �������� � �������� ��������</P></BLOCKQUOTE></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>16</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>29</P></TD></TR>
<TR>
<TD vAlign=top width="70%"><B>
<P align=justify>6. ������� ����������� �� �� �������, �����<SUP>5</SUP></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>3</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>3</B></P></TD></TR>
<TR>
<TD vAlign=top width="70%"><B>
<P align=justify>7. ������� ������-������������ �� ���������� ����������&nbsp;���������</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>0</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>0</B></P></TD></TR>
<TR>
<TD vAlign=top width="70%"><B>
<P align=justify>8. ����������������� ����������� ���������� ��, �����<SUP>6</SUP></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>350</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>467</B></P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<P dir=ltr style="MARGIN-RIGHT: 0px" align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="70%">
<UL>
<LI>
<DIV align=justify>�� ���������� ���������� ���������</DIV></LI></UL></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>306</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>422</P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<UL>
<LI>
<DIV align=justify>� ������� ���������</DIV></LI></UL></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>31</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>31</P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<UL>
<LI>
<DIV align=justify>� ������� ���������</DIV></LI></UL></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>13</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>14</P></TD></TR>
<TR>
<TD vAlign=top colSpan=5><B>
<P align=center>����� �������� � ���������� ����������� ���</B></P></TD></TR>
<TR>
<TD vAlign=top width="70%"><B>
<P align=left>9. ��, � ������� �������� (������������) �������� �� ������������� ���������� �������� � ������� �� ��������� �� ����� ��������������� ����������� ��������� �����������<SUP>7</SUP></P></B></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>218</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>154</B></P></TD></TR>
<TR>
<TD vAlign=top colSpan=5><I>
<P align=right>�����������</I></P></TD></TR>
<TR>
<TD vAlign=top width="70%">&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center>�� 01.01.2005</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center>�� 01.01.2006</P></TD></TR>
<TR>
<TD vAlign=top width="70%"><B>
<P align=left>10. ������� ������ � ����� ��������������� ����������� ��������� ����������� � ���������� �� ��� ������������ ����, �����<SUP>8</SUP></P></B></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>1 569</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>1 687</B></P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="70%">
<UL>
<LI>
<DIV align=left>� ����� � ������� (��������������) ��������</DIV></LI></UL></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 201</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1305</P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<UL>
<LI>
<DIV align=justify>� ����� � ��������������</DIV></LI></UL></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>367</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>381</P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<P align=justify>�� ���: � ����� �������</P></BLOCKQUOTE></BLOCKQUOTE></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>0</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<P align=justify>� ����� �������������</P></BLOCKQUOTE></BLOCKQUOTE></BLOCKQUOTE></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>367</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>381</P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<P align=justify>� ��� �����:</P></BLOCKQUOTE></BLOCKQUOTE></BLOCKQUOTE></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="70%">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<P align=justify>����� �������������� � ������� ������ ������</P></BLOCKQUOTE></BLOCKQUOTE></BLOCKQUOTE></BLOCKQUOTE></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>329</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>337</P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<P align=justify>������������ � ������ ������ (��� ����������� �������)</P></BLOCKQUOTE></BLOCKQUOTE></BLOCKQUOTE></BLOCKQUOTE></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>38</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>44</P></TD></TR>
<TR>
<TD vAlign=top width="70%">
<UL>
<LI>
<DIV align=justify>� ����� � ������������ �������� ���������� � ����������</DIV></LI></UL></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>4</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="70%">
<UL>
<LI>
<DIV align=justify>� ����� � ���������� ���������������� � ����� ������ ��������� ��������</DIV></LI></UL></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1</P></TD></TR></TBODY></TABLE><B>
<P align=justify>��������� � �������.</P></B><SUP>
<P align=justify>*</SUP>* �� &#8211; ��������� �����������. ������� "��������� �����������" � ��������� ���������� �������� ���� �� ��������� �������:</P>
<UL>
<LI>
<DIV align=justify>����������� ����, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� ������� � ������� ����� �� ������������� ���������� ��������;</DIV>
<LI>
<DIV align=justify>����������� ����, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� �������, �������, �� ���������� ����� �� ������������� ���������� ��������;</DIV>
<LI>
<DIV align=justify>����������� ����, ������������������ ������� �������� (�� ���������� � ���� ������������ ������ "� ������ � ���������� ������������") � ������� �������� ����� ������ �� ������������� ���������� ��������.</DIV></LI></UL><SUP
>
<P align=justify>1 </SUP>����������� ��, ������� ������ ������������ ���� �� �������� ����, � ��� ����� ��, ���������� ����� �� ������������� ���������� ��������, �� ��� �� ��������������� ��� ����������� ����.</P><SUP>
<P align=justify>2 </SUP>����������� ��, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� ������� � ������� ����� �� ������������� ���������� ��������, � ����� ������������ ��, ������������������ ������� �������� � ���������� �������� ����� ������ �� ������������� ���������� ��������.</P><SUP>
<P align=justify>3 </SUP>�������� � ������� 1996 �. � ������������ � ������� ����� ������ �� 03.12.1996 &#8470; 367.</P><SUP>
<P align=justify>4 </SUP>����������� ������� ��������� ������, ��������� � ����� ��������������� ����������� ��������� ����������� � ���������� ���������� ������. �� 01.01.1998 �. � ����������� ���������� � ��������� ������������ �� ������ ������ ����������� ����� ���������� ���������� ��������� ������ &#8211; 34 426.</P><SUP>
<P align=justify>5 </SUP>����������� �������, �������� ����������� �� �� �������.</P><SUP>
<P align=justify>6 </SUP>� ����� ���������������� ���������� �� �� ������� �������� �����������������, �� ������� ��������� � ���� ������ ����������� �� �������� �� �� �������.</P><SUP>
<P align=justify>7 </SUP>����� ���������� ��, � ������� ������ ������ ���� �������� (������������) �������� �� ������������� ���������� �������� (������� ��, �� ������� � ����� ��������������� ����������� ��������� ����������� ������� ������ �� �� ����������), &#8211; 1469.</P><SUP>
<P align=justify>8 </SUP>����� 01.07.2002 � ����� ��������������� ����������� ��������� ����������� ������ � ���������� ��������� ����������� ��� ������������ ���� �������� ������ ����� ��������������� ����������� ��������� ����������� � ����� � �� ����������� �������������� �������������� �������.</P>


</body>
</html>

