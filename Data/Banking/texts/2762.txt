<!--
ID:38517799
TITLE:��� � ������ � ������
SOURCE_ID:11528
SOURCE_NAME:���. ����� � ������� ���
AUTHOR:
NUMBER:9
ISSUE_DATE:2007-09-30
RECORD_DATE:2007-12-07


-->
<html>
<head>
<title>
38517799-��� � ������ � ������
</title>
</head>
<body >


<B>
<P align=justify>����� � ��� ������� �������� 341 �������. </B>55% �� ��� &#8212; � �������� �� 40 ���. 87% ����������� ��� ����� ������ �����������: 41% &#8212; �������������, 22% &#8212; �����������, 15% &#8212; �����������, 5% &#8212; ������������, 4% &#8212; ������. ������ ���������� ������� �������� ������ ������ �����������, ��� ��� �� 341 ���������� ��������� ���������� 348 ��������. ����. ������ ��� ��� 5% ���������� ����� � ����, � ��������� ���� ������� ����������� �����������. ������� ��������� ��� &#8212; ��� ������ ������� � 18 ���������� ����.</P><B>
<P align=justify>� ����� ������� ����������� ������� �������������� � ������� ������ ���� ���. </B>���� � ����� 2004-�� � �� ������ 381 ����, �� � ������� 2005-�� � ��� ����������� ��� 562 ��������� �����������. � ������� ���� ���������� ��������� ����� 10, � �� �������� ������ ������� 2007-�� &#8212; 4 �����.</P><B>
<P align=justify>�� 1 ������ 2005 ���� ���� ����������� ������� c�������� </B>4 ����. 967 ���. ���. �� 1 ���� 2007 ���� &#8212; 53 ����. 354 ���. ���. � 2004-� ���� ������������ � �������� �� ���� ������������� ������� ��. �������������� ������������� ����� ��� ���������� � ������������� ����� �� &#8220;����&#8221; � ������������ �� ������� 50 ������ � ����������� ������� � �������� 2 ����. ���. �� ��� 1894 ���. ���. ��������� � ����� �������� ������� � 106 ��������� &#8212; � ����� ��������������� ������ �����. ����� ����, � ������� 2004 ���� ������������� ������ �� � ���� ��������� � ������������ � �������, ��������������� ����. ����� �� ����� ��������� 2 619 ���. ���., � ����� ������ ��������� ��������� ������ �� ������.</P><B>
<P align=justify>�� ��� ����� &#8212; �� ��������� ������������� ����� ���������� �� �������.</P></B>
<P align=justify>&#8226; �� 8 ������� 2006 ���� &#8212; 100% ����� ������� � �����, �� �� ����� 100 ���. ���.</P>
<P align=justify>&#8226; � 9 ������� 2006-�� �� 25 ����� 2007-�� &#8212; 100% ����� ������� � �����, �� �� ����� 100 ���. ���., ���� 90% ����� ������� � �����, ����������� 100 ���. ���., � ������������ &#8212; �� ����� 190 000 ���.</P>
<P align=justify>&#8226; � 26 ����� 2007 ���� &#8212; 100% ����� ������� � �����, �� �� ����� 100 ���. ���., ���� 90% ����� ������� � �����, ����������� 100 ���. ���., � ������������ &#8212; �� ����� 400 ���. ���.</P><B>
<P align=justify>� ����������� ��������� ������ ������ �� �� �� ����� �����������.</B> ������� ��� ���������� 0,15% �� ����� ������������ ���������, � � 1 ���� 2007 ���� &#8212; 0,13%.</P><B>
<P align=justify>�� ����������� ���� ��������� ��� ����������������� ��������� �� ����������� �������:</P></B>
<P align=justify>&#8226; ����� ����������� ����� &#8212; ������-��-����;</P>
<P align=justify>&#8226; ����������� ����������� ����� &#8212; ������;</P>
<P align=justify>&#8226; ��������� ����������� ����� &#8212; ������������.</P>
<P align=justify>����������� �������� ���������������� � � ������ �������, �� ����������� ������������ ������, ��� ����������� ���� ���������.</P><B>
<P align=justify>���� ��������� ������������� </B>��� ������, ��������� � ���������� ����� � ����������� �� ������� ����� ����������� ������� ����� ���������� � ������.</P>
<P align=justify>����� ���� ������������� ������� ������� (����� ����, �������� ������������� �������� �������������, �� �������� �� ���������� � ���������� ��������), ������ �������� ����������� � �����������.</P><B>
<P align=justify>�� ����� ������������� ��� ��������� 17 ��������� �������</P></B>
<P align=center>
<CENTER>
<TABLE cellSpacing=1 width=592 border=1>
<TBODY>
<TR>
<TD vAlign=center colSpan=4>&nbsp;</TD></TR>
<TR>
<TD vAlign=center width="30%">
<P align=justify>������</P></TD>
<TD vAlign=center width="27%">
<P align=justify>���������� ��������� �������</P></TD>
<TD vAlign=center width="31%">
<P align=justify>����� ������������ ������ (���)</P></TD>
<TD vAlign=center width="12%">
<P align=justify>����� ������</P></TD></TR>
<TR>
<TD vAlign=center width="30%">
<P align=justify>������</P></TD>
<TD vAlign=center width="27%">
<P align=justify>11</P></TD>
<TD vAlign=center width="31%">
<P align=justify>30 162 337,23</P></TD>
<TD vAlign=center width="12%">
<P align=justify>497</P></TD></TR>
<TR>
<TD vAlign=center width="30%">
<P align=justify>������ ��������</P></TD>
<TD vAlign=center width="27%">
<P align=justify>1</P></TD>
<TD vAlign=center width="31%">
<P align=justify>�������� ��������� �������������</P></TD>
<TD vAlign=center width="12%">&nbsp;</TD></TR>
<TR>
<TD vAlign=center width="30%">
<P align=justify>�����-���������</P></TD>
<TD vAlign=center width="27%">
<P align=justify>1</P></TD>
<TD vAlign=center width="31%">
<P align=justify>156 886 916,73</P></TD>
<TD vAlign=center width="12%">
<P align=justify>1 550</P></TD></TR>
<TR>
<TD vAlign=center width="30%">
<P align=justify>�������</P></TD>
<TD vAlign=center width="27%">
<P align=justify>1</P></TD>
<TD vAlign=center width="31%">
<P align=justify>3 944 291,87</P></TD>
<TD vAlign=center width="12%">
<P align=justify>56</P></TD></TR>
<TR>
<TD vAlign=center width="30%">
<P align=justify>���������� �������� (���������)</P></TD>
<TD vAlign=center width="27%">
<P align=justify>2</P></TD>
<TD vAlign=center width="31%">
<P align=justify>3 823 014,60</P></TD>
<TD vAlign=center width="12%">
<P align=justify>54</P></TD></TR>
<TR>
<TD vAlign=center width="30%">
<P align=justify>����������� ������� (���������)</P></TD>
<TD vAlign=center width="27%">
<P align=justify>1</P></TD>
<TD vAlign=center width="31%">
<P align=justify>45 860 884,60</P></TD>
<TD vAlign=center width="12%">
<P align=justify>590</P></TD></TR>
<TR>
<TD vAlign=center width="30%">
<P align=justify>�����</P></TD>
<TD vAlign=center width="27%">
<P align=justify>17</P></TD>
<TD vAlign=center width="31%">
<P align=justify>240 677 445,03</P></TD>
<TD vAlign=center width="12%">
<P align=justify>2 747</P></TD></TR></TBODY></TABLE></CENTER><B>
<P align=justify>��� ����������� &#8212; ������ ������</B></P>
<P align=center>
<CENTER>
<TABLE cellSpacing=1 width=592 border=1>
<TBODY>
<TR>
<TD vAlign=center width="51%">&nbsp;</TD>
<TD vAlign=center width="6%">
<P align=justify>2004</P></TD>
<TD vAlign=center width="6%">
<P align=justify>2005</P></TD>
<TD vAlign=center width="6%">
<P align=justify>2006</P></TD>
<TD vAlign=center width="30%" colSpan=5>
<P align=justify>2007</P></TD></TR>
<TR>
<TD vAlign=center width="51%">
<P align=justify>&amp;NBSP;</P></TD>
<TD vAlign=center width="6%">
<P align=justify>01.01</P></TD>
<TD vAlign=center width="6%">
<P align=justify>01.07</P></TD>
<TD vAlign=center width="6%">
<P align=justify>01.01</P></TD>
<TD vAlign=center width="6%">
<P align=justify>01.07</P></TD>
<TD vAlign=center width="6%">
<P align=justify>01.01</P></TD>
<TD vAlign=center width="6%">
<P align=justify>01.07</P></TD>
<TD vAlign=center width="6%">
<P align=justify>01.01</P></TD>
<TD vAlign=center width="6%">
<P align=justify>01.07</P></TD></TR>
<TR>
<TD vAlign=center width="51%">
<P align=justify>���� ����������� ������� (���. ���)</P></TD>
<TD vAlign=center width="6%">&nbsp;</TD>
<TD vAlign=center width="6%">&nbsp;</TD>
<TD vAlign=center width="6%">
<P align=justify>4967</P></TD>
<TD vAlign=center width="6%">&nbsp;</TD>
<TD vAlign=center width="6%">
<P align=justify>20468</P></TD>
<TD vAlign=center width="6%">&nbsp;</TD>
<TD vAlign=center width="6%">&nbsp;</TD>
<TD vAlign=center width="6%">
<P align=justify>53354</P></TD></TR>
<TR>
<TD vAlign=center width="51%">
<P align=justify>����� ���������� ������� ��������� (� ����. ���)</P></TD>
<TD vAlign=center width="6%">
<P align=justify>1,56</P></TD>
<TD vAlign=center width="6%">
<P align=justify>1,82</P></TD>
<TD vAlign=center width="6%">
<P align=justify>2,03</P></TD>
<TD vAlign=center width="6%">
<P align=justify>2,36</P></TD>
<TD vAlign=center width="6%">
<P align=justify>2,74</P></TD>
<TD vAlign=center width="6%">
<P align=justify>3,12</P></TD>
<TD vAlign=center width="6%">
<P align=justify>3,78</P></TD>
<TD vAlign=center width="6%">
<P align=justify>4,34</P></TD></TR>
<TR>
<TD vAlign=center width="51%">&nbsp;</TD>
<TD vAlign=center width="6%">&nbsp;</TD>
<TD vAlign=center width="6%">&nbsp;</TD>
<TD vAlign=center width="6%">&nbsp;</TD>
<TD vAlign=center width="6%">&nbsp;</TD>
<TD vAlign=center width="6%">&nbsp;</TD>
<TD vAlign=center width="6%">&nbsp;</TD>
<TD vAlign=center width="6%">&nbsp;</TD>
<TD vAlign=center width="6%">&nbsp;</TD></TR></TBODY></TABLE></CENTER>
<P></P><FONT size=2>
<P align=justify></P></FONT>


</body>
</html>

