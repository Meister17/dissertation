<!--
ID:36956714
TITLE:������������� ������������� ����
SOURCE_ID:3126
SOURCE_NAME:������� ����������� ����
AUTHOR:
NUMBER:10
ISSUE_DATE:2006-10-31
RECORD_DATE:2006-11-20


-->
<html>
<head>
<title>
36956714-������������� ������������� ����
</title>
</head>
<body >


<P align=justify><B>� ������� ������ ������� �������� 3 ����. ���., ����� ������� ��������� ��������� ����������� �������.</B> �� ��������� � ������� ���� ������ ������� ����������� � 1,5 ���� &#8212; ����������� �� 1 ����. ������. ������� �������� ���������� �� ���� �������� ���������� ��������� �������� ����� ��� �������������� ���������� ����������, ������������� ������� ������ ������������, ������������������� ����� ��� ��������� ��������� ��������. �� 1 �������� ������� ������� ���������� ��� �������� 1,3 ����. ���., ��� � 1,4 ���� ������, ��� �� ������ ����. ����� 75% ���� ����� ���������� ������ ��������� � ����� 25%, ��� 324 ���. ���., &#8212; �������� �� ������ ���������� ����. ���� ��������� ���� �������� ����������� ��������� ������ ���������� �������� ��������. ��������� �������� ������ �� 0,6 ����. ���. �� ��������� � ������� ���� � � ������ �������� �������� 1,9 ����. ������. ������������ ��������� ����� ���������� ���� &#8212; ��������� ��� ����������� ������� ���������, �������� �������� ������������, ������ ������������ ���������. ����� ����� �������� ����� �� 8 ������� �������� �� �������� � ��������� � ������ �������� 125 �����.</P>
<P align=justify><B>� 1 �������� ���� ������������� �������� ����������� ����� ����������� ����� &#8220;������� ������&#8221;, ������� ���������� � ������ ������� &#8220;12 ��� &#8212; 12 �����&#8221;.</B> ������ ��������� �� ���� 2006 ��� � �������� 12-����� �����. �� �������� ����� ��� �������, ��������� � ����� � 1 �� 30 �������� ����� &#8220;����������&#8221;, �������� ���������� ����� &#8220;������� ��������&#8221;. ������� �������� &#8212; ���������� ����� &#8220;������� ������&#8221;, ������� �� 1 ������� ������������ ������� �� ������, ���� � ������ ���� ������� �� �����: ����������� ��������� �������� &#8212; �������������� ����� &#8220;������� ������ &#8212; MasterCard Gold&#8221; � ����������� ������� 5% � ���������� ������ �������� ������. ����� ����������� �� ������ � ������ �������� �������� ������ �� 1 �������. &nbsp;������������ ����� ����� ������� �� ��������������� 12-��. �� ����� ���� ��������� ��� 4 �����, � ���� ������� �������� ����� ����� �������� ���������� �������� � �������.</P>
<P align=justify><B>�����-������ &#8220;����������� �����������&#8221;, �������� ������ � 2006 ���� � ����� 12-�����, ���������� ��������� ���� ������ &#8212; ���������� � ���������� �������� � ��������� ������������� ����������� ������������� ���������. </B>��������� ������� �������� � ������� ����� 50 ������������� ������� ���������, ��������� ������� �����, ���������� ���������� �������. �����, ��������� ������������� ����� &#8220;�������&#8221;, � ������� ������ ������ ������������� ������ �� ������ ������ ������������� ����. ����������������� ��������������� �������� ������������� ������, ���� &#8220;�������&#8221; �������� � ����������� �����������: ����������� � ������� ������ ����� � �������������� ������ &#8220;����������� �����������&#8221; � ����� �� ������� ������� ������ &#8212; &#8220;�����&#8221;. � ������ ����� ���������� ���������������� ����������� �����, ������� ��������, ���������. ������ ���������� � ��� ����������� ���� �� ������� ����������� ������� �����. ��� ��������� �� ������ �������� �������, �� � ����� ������������� ��������� ����� &#8220;�������&#8221;, �. �. ������� �� ��� ���� ������ ��������� ����������� ����� � ������������ ������ ����� �� �����. � ������ ����� �������� ������������� ����� ������� ��������, ���������, ��� ������������ ������ �� ������ ��� ���� ����� ������� ��������, �� � ��� ���������� ������� �������� �������� � �. �.</P><I>
<P align=right>�����-������</P></I>
<P></A></P>


</body>
</html>

