<!--
ID:38463237
TITLE:����������� �� ����������������� ���������� ������� ���������� ��������� 
SOURCE_ID:3190
SOURCE_NAME:���������� ����� � ����������
AUTHOR:
NUMBER:1
ISSUE_DATE:2007-02-28
RECORD_DATE:2007-11-26


-->
<html>
<head>
<title>
38463237-����������� �� ����������������� ���������� ������� ���������� ��������� 
</title>
</head>
<body >

<!-- length="4178" -->
<P align=justify>� ������ 2007 ���� �� ��������� ������������� ���������� ��������� �������������� ����������� ������ � ��������� ��� �� ����������������� ���������� �������, ���������� �������� �������� ������������������� ������ ��������� � �������� ������ � ����������������� ������.</P>
<P align=justify>� ��������� ����� ����������� ����������� ���������. ����� ��� � ������ ������� ����� ��������������� ������ � ��������� ������ ����������� ����������� �������� �����������, � ��������� � ������ ������� ���������������� ���� ���������� ��������� � ������������� ���������� ������� ���������� �� ������ �� ���� ������������ �������� � �������.</P>
<P align=justify>����������� �� ����������������� ������������� ������������ ���������� �������, ����������� � �������� �����������, ����� ������ � �������� ���. �������� ������� � ���, ��� ��� �������� ������������� ���������� ������� �� ������ � 2002 ���� �������������� ����� �� ���� ������������� ����������� ���������� �� ������ ������� ������������� ������. ������ ����������� ������� ����������������� ���������� ����, ������� ����������� ��������, ���� �� ��������� - �������� �������� ���������� ���������� ��������������� ����������� ��������.</P>
<P align=justify>� ����� 95% �������������� �� �������� ������� � ����� ����������� ������������ ������������ ������� ���������� ���������� � ��������� �������� "���������". ����������� � �� ������ ����&shy;���� ������������ ����� ��������������� ����������� �������� (��������������) � ��������������� ������ ������, ���������� �� ������� �� ������������ ���� ������� ���������� ���������� ���� ����&shy;�� ��������, ��� ����� � �� ������������ �������������. ����� ����, ��� � ������������� ����������� ����� ���� ������� ����� ��������� ���������� ����� ������� ��������������� ������ �����.</P>
<P align=justify>����������� ������ ��� ���� ��������� �������� "��������" ����� �������� ����������, �� ��������� ������ �������������� ���, ������������������� ������ �� ������������. � ���� ����� ������������, ������� � 2008 ����, ��������� ���� � �������� ������� � ������������� ���������� �������, ����������� � �������� �����������. � ������: �������� "��������" ���������� ��������� �� ������� ������ � ���������� ����� ���������� ��������� � ����������� ��������� �� ������ ���������� ��������������� ����������� �������� �� ����� �������������� ���������� "��������", �� �� ���� ��������. ����� ����������� ������������� ������� "��������" ��������� �� ��� ����: �� ����������� �������� ���������� � ����������������� ���������� ����� � ����������� �������� �� ������ �������������� ���, � ����� �� ������� ������� ������. ��������� ����� ������������ ���&shy;��� �������������� �������� ����������� ����� ���������� ��������� �� ���� ������� ����������&shy;�� �������, ������� ����������� � ��� ����� �� ���� ������� ��� ��� �� ��������������� ������ �����.</P>
<P align=justify>��� ���� ������� ���������� �������������� ����� ��������������� ������ ������� �������� ���������� ���������� ���������� ����������, � ����� � ����������� (�� 2013 ����) ������������ ����� ���������� �� �� ������������. �������������� ��� �� ������� ����� � ������ ������������� ��������� ����� ������ � ��� � ������� ������ �� ���� ��������� �������, ��� ������������� ���������� �� ����� �� ����������.</P>
<P align=justify>������������ ������������������� ������ ���������� ������ ����������� ������������� ������� � ������������� ���������� ������� ���� ��� ��������� �������������� ���, ������� ���� ����������&shy;�� ��������� �� ��� � 2005 ����, ���� �� ����������� �����. ������������ ������� ������� ��� ���� ������ ��������������� ����������� ����������� � ��������� 2:1 � ��������������� ���������� ����&shy;�� �� ����� ���������� �������, ��� �������� ������� �������� ������ ����� ������� �������� ������.</P>
<P align=justify>��������������, ��� ����� ����������� �������� ������������� � ��������� �������� ����������&shy;��� ���������� ������� � ��������� ������, ��� ��������� � �������, ����� �������������� ��� �� ��&shy;����������� ������.</P>
<P align=justify>�������� ������������ ����� ������������������� ������</P>
<P align=justify></P>

</body>
</html>

