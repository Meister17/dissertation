<!--
ID:36798881
TITLE:�����-���������� ���� ������������ ������������� �������
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:������ �������, ����-��������� �����-����������� �����, ������� �����, �������� �� ������ � �������������� ����������� �������� ���� 
NUMBER:8
ISSUE_DATE:2006-08-31
RECORD_DATE:2006-10-13


RBR_ID:2039
RBR_NAME:���������� �������
-->
<html>
<head>
<title>
36798881-�����-���������� ���� ������������ ������������� �������
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgwhite>
<TD></TD></TR>
<TR class=bodycolor>
<TD>
<P align=justify><I>�����-���������� ����, �������� ���������� �������� �������� ������������� �����-����������� ����������� ������, ��������� � ������� ���������� ���������� �������� � ��������� ��������� ��������������� ����� �������� ������. ������ ������������� ������ ����� ����������� ������������� ��������� ������, ��������� � ������ ���������� ������� � ������. ����������� � ����������� ����������� ����� ��� ��������� ��� ���������� ���� ����������� ������������� �������. � ������� 2005 ��� �������� ������ �� �������� ���������� ������ ��������� ������ � ���������� ��� � ������������ �������������� ��������������. � ������������ � �������� ������� ��������� ������ �� ������ �������������������� ���������� ��� ��������� ���������� ���������� &#8212; ��� � �������������� �������. </I></P></TD></TR>
<TR class=bgwhite>
<TD>
<P class=Main align=justify>� �������� ������� ������� ���� ��������� ������������. ������ ���� �������� ����������� �������� ����. �� ������� ������ ���� ��� �������� ������ �� �������������� � ��������� � �������� �������. ��������� ������ �� ���������� ���������� ��� ����������� ������ 2006 ����. 
<P class=Main align=justify>��� �������� ����, ������� ������� ���� �������� ��� ��� ���������� ������, ������ � �����-��������� ����� ������� ���������-�������������. &#171;��� ��� ������, ����� ����������� ������ � ���������� ������ ������� ���� ��������� ������������ ������ �� ���������. � ����� ������ ���������� ������, ��� ����� �������� �������������� ������ � ������� �������� �������������������� ���. ������� ���� �������� ���� ����������� ��������� � ��� ���� ���� � ������. ������� �������� ����� ������� ��������������� � ��������������� �������� ����������� � ������������ ��������� �����, � ������ �������� � �������� �� ��������&#187;, &#8212; ����������� ������� �����, �������� �� ������ � �������������� ����������� �������� ����. 
<P class=Main align=justify>������ � ��-�������������� ����� ����������� � ��������� ��������� ������������ �������. ��� ���� ����� � ����� ���� ��������� ����������� ��������������� ������������� ����, ���������� ��� 16 �������� � 60 �������������� ������. ����� ����� ��� ���������� � ������� �������� ������� �����. � �������� ������������ ������������ ����������� �����. �������������� ������ ���������� ����� ����� ��� ����� ��������� ����������� ������������� ������� � ��� ����� ������� ��� ���� � ���� �������. 
<P class=Main align=justify>����������� ���� ���������� ����������� ������� ��� �������������� �������, ������� �������� ������ � ���� �������� ������, ������� ���������� �����������, ��������-�������������� ���� ���������� ��� � ���� �������� ������ ����� �������� � ��������� ���, ������� �������������� ����������������, ������� ����������������� ��������������� ���������, ������� ����������� � �������������� ������� �������� �������������. 
<P class=Main align=justify>����� ����, ����������� ���� ����������� ���� ����������� ����������� ������ � �������������� ����������������� � ��������� ���������. &#171;���������� ��������������� ��� �������� ������ ������������ � �������� ������ ��������. ��� �� ������ ������������� ����������������� ����������������� �������&#187;, &#8212; ������� ������ �������, ����-��������� �����-����������� �����. 
<P class=Subheader align=justify>������� �������� 
<P class=Main align=justify>������� �������� ������ �����, �������, ����������� ��������������� ��������� � ������� ���������� ���. &#171;���������� ���� ����������� ���������� �������� ���������� ������ �� �������������� ��������� �������� � ��������� ������, &#8212; ������� ������ �������. &#8212; �� ��� ���� ��������� ��� ����� ���� ��������� �� � �������� ������, � �� ���������� ���������� ���������� �� ��������� ������. � ���� �� ���������� ���� ������������� ������������ ����� ������ �������� � �������� �������, � ����� �������������� ����� �������� �� �������� �������. ���� ������ &#8212; �������� ����������� ����� ���������� ��������� ����������� ���������������� �������, ������������ � �������� �������. ���� ��� �� ������&#187;. 
<P class=Main align=justify>� �������� ������ ������� �������� ������ ������� �������� ������� ������ hi-end &#8212; HP StorageWorks XP10000. ��� ����� ����������� � �������� � ��������� ���, ����� ���� ������������ ���������� ���������� ������. ������� ���������� ����������� ��������� ���������� �������� ���������� ����� ������, ����� ������ ����������������� ��� ����������� ������� �������. 
<P class=Main align=justify>��� ��������������� �����&shy;��� ��������� ����� ����� ������������ ��������� ��������� ���������� HP StorageWorks EML103e. 
<P class=Main align=justify>� ����������� �������������������� �������, ����������� �������� � ��������� ��������, ���� �������� ������ ����������. ������ ����������� ����������� ���������� ����������� ���� � ������������������, ����� ���� ���������� �������� ������ �� ������� �����������. ��� ����� ���� ������ ������������ ������������ � ���������� ���������� ���������-���������� ����� &#8212; &#171;�����&#187; �����������. 
<P class=Subheader align=justify>�������������� ������� 
<P class=Main align=justify>� ������� ��� ����� ������������ ������� HP Proliant ��������� ����������� � �����-������� ��� ������� ��������. &#171;��������� ��������� ��������� � ���� ������ �������� ������������ ������������������� � ������� �� ������� ���������� �������, &#8212; ��������� ������� �����. &#8212; ��� �������� ����������� ��������� ������� �� �������� ���������� ������, � ����� ������������� ������� ����������� �� �������� ��������� ���������� ���&#187;. 
<P class=Main align=justify>� �����, ������� ���������� ��� �� ������ �������� � ����� ������ ����, � ����� ������ ����������� ����� �������� ��������� ����. ��� �������� �������� ������������������ �������������� ������ �����. 
<P class=Subheader align=justify>���������� ������� 
<P class=Main align=justify>� �������� ���������� ������ ��� ��� �������� ����� ���������, ��� � �������������� �������� ���������. ��-������, �������� ���������� ������� ������������ ������������� ������ ��������������� ������������ � ������������ �������� ������, �.�. �������� ������� � �������������� �������. ��-������, ��������� ���������������� ���������� ������� ������� ������� �� ������������ ������������ ���. ������� ��������� ��� ���������� ����� ��������� �������������� �������, �����������������, �������� �������������. 
<P class=Main align=justify>��������� ���������� �������������� ��� ���������� ������� 15-20% ��������� ����� �������. �������� �����, ��, �� ������ ������� �����, ��� ������� �� ������ ���������, �� � ����������. � �������� ����, ������������� ������� ���������� ��������� ���������, ���������� ���������� ���������� �� �� ������������. 
<P class=Main align=justify>� �������, ���������� ������� ����������� � ���������� ����������� ��������� ��� �������� ��� ������������ �������������� ������� �� 40% � ���. � ������������� &#171;������&#187; ���������� �������������� ������� (�� 10 ��� � ����) �� 30-40% ������� ��������������� ����� �� ����� �����������, ����������� � ������ ��������� �������������� �������, �������������� � ��������� ����������. ��� �������� ���������������� �������� ����������������� ��������� ����������� ������������� ��������� ��� ���������, ��� ����� �������� � ���������� ��� ������� ������ �� ����� ����������� �������������� ������. 
<P class=Main align=justify>&#171;��� �������������� ���������� ������ ����������� ���� ���� � ������� ������ ������ �������, ������� ����������� ������ ���������� � ��������������� ������������&#187;, &#8212; ��������� ������ �������. 
<P class=Subheader align=justify>���������� � ����� 
<P class=Main align=justify>������ ������, ��� ��������� ���������� ���������� ������ � ��������� ��������� ��������� �������� �� ������ ������������ �������� �����. �������� ���������� ������� �������������� ����� ��������, �������� �� ��������� ������������ ���������� ��� ��������� �������� ���������. ������, ������� ����� ������ � ����� ������ ������������ ���� ���������� �������� � ���������� � ������������ �������. �� �������� ��������� ���������, ������������ �����. 
<P class=Main align=justify>��������� ��� ������������� � ������ ��������������� �����-���������� ������ ����� ������ ������ �� ��������� ���-��� ����. �� � � ���������� ������� ����� ����� ����������������. � ���� ��, � ��� ����� ���� ���������� � ������ ���������� �������, ������ ��� � �����������. 
<P class=Main align=justify>� ������ ����� &#8211; ��������� ������� ����������� � ���������� ���. ��� ������ ����������� � �������� ������������������, ������� ����� � ������ ��������� ������ ���. ���������� ������� ��������� ������ ���, ������������ ������������� ����������� ��� ������������������, ������� � ������������������ ��� ����������� ������� �������� ��-�������� ��� ����� ����������� � ����������� ������� �������������� ������. 
<P align=justify>
<HR>

<P class=Subheader align=justify>� �����-���������� ����� 
<P align=justify>�����-���������� ���� &#8212; ������������� ��������� ����������� ������������ ��������, ������� ������������������� ��������� ���� � �������������� ���� ������ �������� �� ��&shy;�������� �����. ��������� 16 ��������, � ����� ����� 60 �������������� ������ � ���&shy;��������� ����. ������������� �����&shy;������������ � ������� ����� (������� ����������) � ������������. 
<P align=justify>�������� �������� ��������-���������� �����������: ���&shy;���������� �����-����������� ����������� ������-���� � ���� ������������ ��������������� �������������. �� ����������� ���� ��� ���� � ����� ������ ����� ����� ���������� 86,5%. 
<HR>

<P class=Subheader align=justify>���� ���� � �������� ��� ��� ���������� ������ 
<P align=justify>����� �� �������� �������� ��� � 2002-2003 ����� ���� �������� ������. ��� ��������������� ��������� ����� ���� ���������� ��� �������� �� �������� ������� ��������� ������, �������&shy;�������� ��� ������ ������������������ ���������� ������. � ���� �������� ���� ��������� ���������� �������, �������������������� ��������������, ��������� ���������, � ����� ������� �������� ������. 
<P align=justify>� 2005 ���� ����������� �������� ���� � HP �������� ������&shy;�������������� ������� �������� � ������� ��� � ���-�����. � 2006 ���� &#8212; ������� ������������ ��-�������������� ��������������� ������ ����� ������. 
<HR>

<P class=Subheader align=justify>���������� ������� � ���-����� � ������ 
<P align=justify><EM>��������� ������������������ ��-������</EM> 
<UL>
<LI>�� 30% ����������� ����� ������������������ ����; 
<LI>�� ��� ������� ����������� ����� ������������ ��������� ������� � �����������, ���� ������ ������� ����������� �� �������� ��������; 
<LI>�� 24% ����������� ����� ���������� �����������. </LI></UL>
<P align=justify><EM>��������� ���������� �������</EM> 
<UL>
<LI>����� �������������� ��������� ���������� ���������� &#8212; 15 ����� </LI></UL></TD></TR></TBODY></TABLE>


</body>
</html>

