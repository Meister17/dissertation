<!--
ID:36495927
TITLE:��������� �� ����������
SOURCE_ID:10908
SOURCE_NAME:������������ ���������� ������
AUTHOR:�������� ������
NUMBER:6
ISSUE_DATE:2006-06-30
RECORD_DATE:2006-08-01


RBR_ID:3412
RBR_NAME:�������-��������� �����
-->
<html>
<head>
<title>
36495927-��������� �� ����������
</title>
</head>
<body >


<H1 class=art_title><FONT size=3>��� ��������� ������� �� ��� ����</FONT></H1>
<DIV class=lead><STRONG><FONT>� ����-������� ����� ���������� ������� ������������ �� &#171;���&#187;. � ������ ���� �� ������ ���� ������� ���� � ����������, �� � ���� ������� ��������.</FONT></STRONG></DIV>
<DIV class=artic_body>
<P class=bukvitza><FONT>� ���������� ������� ���������� ������, ������������ ������������� ������ � �����������, ��������, ��������� � ��������������� �������� ������, � ��������� ��� ���������� �� ���������, ���������, ����-����������� � ����������� �������. ���������� � ��������� ����� �������� �������� ����, � �� ������� � ��������-������� ������������ ������. ������ ������ � ����� ����������� � �� ������, ������� ����� ������ �� ������ ��-�� ��������� �������� ������ �� �� ������, �� � ��-�� ����, ��� ���������� � �������� ������� ������� �� ����� ������ ������� �����, �������� ������������� � ������� ���� ������ (��������� 1). </FONT></P>
<P><FONT> </FONT></P>
<P><FONT>���, � ������� �� ���� ������� ���������� ������ �� �������� � ������� 2% �� ���� ����� �������� �������. ������, ���� ������� ������� �������������� ������������ ���������� � ����������� �������� �������: � ���� ������ �� ���� �������� �� 1,1% ���� ��������������. �� � ����� �������-������������ ���������� �� ��������� ����������� ������ �� � ������ ������. �� ����� �������, ���� ���������� �������� �������� (�� ������ ������� ������ �� ��������� 5 ���) ����������, �� � ������ � ���� �� �������� 2006 �. ��-�� ���������� ����� � ����� ����� �� ��������� �������� ������� �� ����� ����� ����� 113 ����. ���., � �� ����� ��� �� ������� �� �������� ��������� �������� �������� 14 ����. ���. � ����� ����� �������� ������� �������� ��� � ����������� ���� � ����������� �������� � �������� ���������, ����� ��������������� �� �������� �������� � ������� ������ ����� ������ � ����. � ���������� ������ ����������� ������ � ������� ��������� 47 ����. ���. �� �������� � 5 ����. ���. �� �������� �������. </FONT></P>
<P><FONT>�������� ��� � ���� ����, ��� ����������� � ��� ������, ������������� ����� �������� ����������� ��������, ����� ������ ���������� � ���� ����� �� ������ �������� �����, � ��� ����� ���������� ����������. ��� ����������� ����� �������� �������� ����� ����, ��� ������ ����������� ������� ����������� �������, ����� ������� ����� ������� ������� �������� �� �����, �� ����������� 100 ���. ���. </FONT></P>
<H2><FONT size=3>���� ������ ������ �������� </FONT></H2>
<P><FONT>����� ���������� �������� �������, ����� ���������� ����� ��������� ������������� ���������. �������, �������� ��������� ��������� �� �����, ������� �������� �������� ���� ��������� � ������ ������ �������� ��� ������. ��� ���� �������� ����������� ��������� �������� ��������� ���������� ������������� ������ �� ������� ������. </FONT></P>
<P class=vrez align=right><FONT><STRONG>��� ������ ����� ������������ �������, ��� ������ ������ ���� ��������� ������� �� ���������� ����� </STRONG></FONT></P>
<P><FONT>� ��������� ����� ���������� ������ ��� ������ ����������� �������� ������������ �������� ��������� (��������� 2). �������� �� ������� ������� ��� � �������� ������������� ���������� � ������, ������, �������� � �������. � ��� ����� ������ �� ��������� ����������� ���� �������������� ������ (����� � ����� �� ��������� �������� ������ ��������� �������, �������� �� ������� �������� �������� ������) �� 0,15- 0,18 ���������� ������. ���� ������ �� �������� ��������� � ������� � ������ ������� ���������� ���, ��� � ����� � ������ ���� � ������ ��������� ��������� ������� �������� ����������, ����������� �� ��� ��������� ��������. </FONT></P>
<P><FONT> </FONT></P>
<P><FONT>�� ������� �������� ���������� �������� �������� �������������� �������������� ���: � ������� �������� ����� ������, � � ������ ������ ���� � ����� �������� �������. � �� � ������ �������� ��������� ���������� ��������� � ����������, ��� � ���������� ����� �������� ������ �� ��������� ��� ������. </FONT></P>
<P><FONT>� ��� ���������� ��������� ������ ����� ������� � ���, ��� ������ ����� ������ � ���� ������ �������� ��������� �������� �������� �� ����������� ����� ��������. ������ ���������� �� ����� ��������� �� 1-1,5% ����, ��� �� ������� �������. ����, �� ������� ����� �������� ������, ��� �������, ������������ ���������: �� ���� ������� �� ����. �������� ������ - ������� ������������� ���, ����������� �������� ������� � ���������� ������������ ����. � ����� �������� � ��� � ����� �� ���� ������������� ������������, � ��� ����� ��������� ��� ������������ � ����� ������. </FONT></P>
<P><FONT>��������, � ��������������� ����� ��� ��� ����� ���������. � 2006 �. � ���� ����� � ������ � 1 ������ �� 31 ������� ���������� �����, � ������ ������� �������� ������������ ������ � ����� &#171;���������&#187;. ���� ������ ����� �������� ���������� ����� � ���������� ������ ����� ������������, � ����� ���������� �������� ������������� ������� �� ����� � �����. </FONT></P>
<P><FONT>����������� ������� ��������� � ���-����, ������������ � ���� ���� ���������� ����� ������� &#171;�������� ����������&#187; � ������, �������� � ����, ������� ���������� ������ �� 95 ����. ��� ����������� ���� ������������� � ������ � 24 ����� �� 24 ���, ������ ����������� �� ������ ������ �� �����������������, ��� ���� ��������� �� ���������� ���� �����. � �������� ������� ��������, ����������� �������� �� ����� &#171;�������� ����������&#187;, ���-���� ����� ����� ����������� �����. </FONT></P>
<P><FONT>����� ����� ���������� ����� ������ �� ������ ���������� ��������� ����. ���� ��� ����� �������� ������ &#171;����������� � ����&#187; �� ���� �������������� ����������. ��� ����� ����������� ������ ��������� ��������� �������� ���������� ���������� ����� �� ������� �����, ������� ��������� �� �����, � ����������, ����� � ����������� ��������� ����������� � ��������. </FONT></P>
<P><FONT>������ ������� ����� ������� ���������� ������ � ������ ���������� ������ ���� ������ �� ������ ����. � ��������� ����� ����������� �� ����� ����� �����, ��, ����� �������� �������� �������, ����� ��������� ���-�� ����������. ���, ����������, ��������, � ����� �������� ���������� �������� ��� ���������� ������� ������������ ������������ ����, � ����� ������ ��� ����������� ��������� � ���������� ������. ��������� ������������� �������� ������ �� �������� ����������� ���� �� ��������, � ����� ������ �� ���������� ������ �������� �������� ������� � ����� � ������� ����� �� ��������� ������. ���� ����� ���� &#171;�������&#187; ��������� ��������� ������������� �������������� ������, ��������� � ������������� ������������� ����, � &#171;����&#187; ����� ���������� ����������� ������� ����� ����� �������� � �������� ����� � ����� ���. </FONT></P>
<P><FONT>������, �� ������ ���������, ������� ������� ������� ������� ��� ������ ��������� ����� ����� �������� � ������������ �������������� ���������. ��� ������ �����, ��� �������, ������������ �������������� � ������ ���������� ������, ����� ����� ����� ������������ �� ��������� ����������� ������ �������. �� �� ������ ��� �������� �������������, �. �. ������ �� ��� �������� �������� ���������� ���� ������� � ������� �����. ������� ������ �������� �������� �������� ��� ������ ���������� ���� ����� ��������� ��������� � ������ ������� ������������, ������� ���, ���� ����������� ����� ����� ������������� �����������. ������ ����������, ����� ������� �� ������ ����� ������ � ������ ���� �� �������� �������� � ��� ������, ��� ������� ��� ����� ���������������� � �������� �� �������������. </FONT></P>
<P><FONT>�� ��� ���������� ������������ �����. ��������� ����� ��������� ��������������� ���� ���������, ���� ���������� ������ ������� � ������� ����� ��������������� ������ �� ���������. � ���� ������� ������ ��������� ����������� ����� ��������� �������� ��������, ����� �� ��������� ����� ����� ���������. � ���������� � �������� ���������� �������� ��������� ������. </FONT></P>
<P class=vrez align=right><FONT><STRONG>� ���� �� �������� ����� ���������� ������� ���������� �� ����� ����� ����� 113 ����. ������ </STRONG></FONT></P>
<H2><FONT size=3>������� ������ ���� ������ </FONT></H2>
<P><FONT>������� ��������, ��� �� ���� ����� ���� ������������ ������� ���� ��������� ������� �� ���������� ����� ����� ���������. ��������� ����� ������ ��������� �������� ������� ���� ������� �������, ��������� �� ��� ���� ����������� ������ �� ���������. ��� ������ �����, �������� �������� ���� ��������� �� �������� ������� �����������, ����� ��� �������, ��������� ��������������� ���������� ������ �� ������������ ���������. ��� ����� ������� ����� � ����� ���������� ��������, ��� ����� ������� �� ������� �� ������� �������� �����������. </FONT></P>
<P><FONT>���������, ��� �������� ������ ������ �� �������� ���������� ������ �� ������� ��������. �� ��������� 3 �������, ��� �������� ������ ��������� ������ ������ � ������, �. �. ������ ������������ � ���� ������ ��� ���������� ������� �� ����� ��������������� ���� ���������. � ������� � ����� �������� �� �������� ���������� ���������� ���� �������������, �� � ������ ����� �� ������� ������� ����������, �������� ������ ���� � ���. ��������, ��� ��������� ���������� ����� �� ������ ����������� ����������� ����������� ������� � ��������������, ��� ������� � ������������ ������ �������������� ������. ����� ������ �� �������� ���� ���� �������������� ������, � ��� � �������� �� ����� ��������� ����. ���� �� �����, ��������������� �� ����� ������ �������� �������� � ������ ����� ����� ����� ������� ��������. � � ��������� ������� ���� ���������� ������ �� �������� � ������������ ������������ ������. </FONT></P>
<P><FONT> </FONT></P>
<P><FONT></FONT></P>
<P><FONT>� ������ ���������, ��� �������� ��������� ��������� � ���������� ������ ������������� ���� �� �����. </FONT></P>
<P><FONT> </FONT></P>
<P><FONT>��� ������� �� ��������� 4, �������� �������� ��� ������ (� ����� ������ ����� ���� ��������� �������) �������� ������. ��� ��� ���� ����� � ���������� ��������-���������� �������� ���������� �� ���� � ��������. � ��� ���������� ������ ���������� � ���. ������ � ���, ���������� ��������, ��� �������� ��������� ���������� ������, ���������� � ������ � ��������, ����������� ����������� �� ���������� ��������-���������� ��������. ��� �� ����� ���� ������ ������ ���� �����������, �. �. � ��� � ��������� �������� ���� ���������� ���� �������� ������. �������������, �������� ��������� �� ���� ���������� ��������� ������, ����� ������������ ������� ������������ �� ���������� ������� ��������-���������� ��������.</FONT></P></DIV>


</body>
</html>

