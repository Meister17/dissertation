<!--
ID:38080521
TITLE:������ ������������ ������������ ����� ���������� ��������� �.�.��������� "� �������� ��������� � ��������� �� �� �� 26.03.2004 �. � 254-�"
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:16
ISSUE_DATE:2007-08-31
RECORD_DATE:2007-08-30


-->
<html>
<head>
<title>
38080521-������ ������������ ������������ ����� ���������� ��������� �.�.��������� "� �������� ��������� � ��������� �� �� �� 26.03.2004 �. � 254-�"
</title>
</head>
<body >


<P align=center>������<BR>������������ ������������ ����� ���������� ��������� �.�.���������<BR>��������� ����������� ��� �.�.���������<BR>(�� 06.08.2007 &#8470; �-02/5-418)</P><B>
<P align=justify>� �������� ���������<BR>� ��������� �� ��<BR>�� 26.03.2004 �. &#8470; 254-�</P></B>
<P align=justify>� �������� ��������� ����������� &#8211; ������ ��� ������������ ��������� �������� ���������� ��������� �� �� �� 26 ����� 2004 �. &#8470; 254-� "� ������� ������������ ���������� ������������� �������� �� ��������� ������ �� ������, �� ������� � ������������ � ��� �������������" (����� &#8211; ��������� &#8470; 254-�).</P>
<P align=justify>���, � ������������ � �. 3.1.5 ��������� &#8470; 254-� ��� ��������� ����� ��������� ����� �� ����� ������ ������� �� ����� ������������ �� ������ ��������� ����� ��������� �����. �������������� ��������� ��������, ����� ������� ����������� ������������� ������� ������� �� ������������ �������� ���������� ���, ������� ����������� �����, ����� �� ������ ��������� ����� ��������� �����.</P>
<P align=justify>�� ������ ������, ����� �. 3.1.5 ��������� &#8470; 254-� �� ��������� ��������� ��������, �������� � ������ "���������". ��� ��������������� ��������� ������ �� 30 ���� � �������� �������������� ������, �� ������������ 70% ����������� ������� ����������� ����. ��������� ������� �� ����� ����� ����������� ���������. � ���������� ���������� ������������� ������� �� ��������� ������ �� ������ ���������� ����� ���������� � ������� ��� ����� ���������, �� ������ �������������� ������� � ����� �������� ���������� �����.</P>
<P align=justify>�� ��������� ����������� ��������, ��� ���������� ���������� �������� �� ������������ �������� �� ����������� ���������� ���, � ��� ����� ����������� �����, �������������� ����������������.</P>
<P align=justify>�������� �������� ������������ ��������, ������ ��� ����������� ������ � ����������� �������� ��������������� ��������� � ��������� &#8470; 254-�. �� ������ ������, ������������� ������� �� ��������� ������ �� ������ ������������� � ������������ �������� ���������� ��� ������������� ��������� 1 ��� � ����� � ��������� ������� ���� ��������� ������.</P>
<P align=justify>���. �.�.��������<BR>���. 291-50-53</P>


</body>
</html>

