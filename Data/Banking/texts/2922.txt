<!--
ID:35382704
TITLE:������������� ���� � �����
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:
NUMBER:6
ISSUE_DATE:2005-06-30
RECORD_DATE:2005-10-07


RBR_ID:4932
RBR_NAME:������������� ���� � ������
-->
<html>
<head>
<title>
35382704-������������� ���� � �����
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgwhite>
<TD height=0>
<P class=Rubrika align=justify>�� ���������� ���������� ��������� 
<P class=question align=justify>������ �� ����������� ���� ���������� ��������� �� ����� &#8470;0401075, ����� ���� ���� � �����<SPAN class=question>�</SPAN>���� � ��������� &#8470;90902 ��������� ���������� �� ��������� ���������? ���� ��, �� ��������� �� ����������� ���� ��������� ��������������� � ��������� ���������, ����� ������������� ��������� ���� ����� ������? 
<P class=Main align=justify>����� 1.7 ����� 1 ��������� ����� ������ &#8470;2� �� 03.10.2002 �� ���������� ������� ������������ � ���� ��������� � ���� ���������� ��������, ������������ ����������� �������, �������� ��������������� ���������� � ������� ��������, �� ����������� ������������ ������� � ��������������� ��������� �������� ������� � ������������ � �����������������, ������� ����������� ������� ��������� ��������� �������, ������� ������ ����� �������� � �������� ������� ��������� ����������, ��������� �� ������� �� ������������� ��� ������������� ��������. 
<P class=Main align=justify>��� ������ ���������� ���������, ������������ ��������, ������������ ����������� ������� � ��������������� ��������� �������� ������� � ������ ������� ���, ����, ������������� �����������, ���������� �� �������� � ������������ � ������������ ������ 8.8 ����� 1 ��������� &#8470;2�, �� ����������� �������� ������ �����&shy;�������� � ���� ����������� ��������� &#171;������� ����� ����������&#187; � ������� �������������� �����������. 
<P class=Main align=justify>����� �������, ����, ������������� �����������&shy;��������, ���������� ��������� � ���������� � ��������� ��������������� ���������� (� ��������� ������). 
<P class=Main align=justify>&nbsp;� ����������������� ����� 
<P class=question align=justify>���������� �� �������� �������� ��������, ���������� ������ � ����������������� �����, ��������� �������: 
<UL>
<LI class=question>�������� ��������, ���������� �� ��������� ��� ������ � ����������������� �����, ���������� �� ��������� ������� ����� �/� 20202 &#171;����������������� �����&#187;; 
<LI class=question>�������� ��������, ����������� � ����������������� �����, ���������� � ������������� ����� �� ��������� ������� ���� �� ���������� �������������� ������ ������� �������� ������������� ��� ����� ������������ ���������� ��������; 
<LI class=question>������� �������� ������� �� �������� ����� 20202 &#171;����������������� �����&#187; �� ���������� ����� �������� � �����, ������� ����������� �������� ���������� � ������� ��� ������. �� ��������� ������� ���� �������� �������� ����� ���������������� ������� ����� � ��������� �� ���������� ��������� ������; 
<LI class=question>������ ������������ ������� �������� ���������� � ��������� ��������� ����������� � ������� �������������� ����� �������������� ��� ����� ���� �� ��������� ������� ����� 20202 &#171;����������������� �����&#187;. </LI></UL>
<P class=Main align=justify>��������� �������� ��������, ���������� ������ � ����������������� �����, ���������� � ������ � ������� ������� �������, ������������� ����������� ������ 2.6.9 ��������� ����� ������ &#8470;199� �� 09.10.2002 � ������ 2.2 ����� II ���������� � ��������� ����� ������ &#8470;205� �� 05.12.2002. 
<P class=Main align=justify>� ������������ � ������������ ������ 1.27 ����� I ��������� &#8470;205� ���� ��� ��������� �������� �������� � ����� ���� �� ����������. ��� ���� ������ ������������ �� �������� ���� �� 12 ����� ���������� �������� ��� (����� 2.2 ����� III ��������� &#8470;205�), � �� �� ���������� �������������� ������ ������� �������� ������������� ���. 
<P class=Main align=justify>��� �������� ������ ������������ ������� �������� ���������� � ��������� ����� � ������� �������������� �����, ����� ����� ����������������� �������������, ����������� � ���������� ������ ������������ �������������� ����� � ���������� � ������������ ����������&shy;�������� �������� ����� ������. �������� ���� ������������, �������� ���������� ������������ �����, �������������� �������� ������������ ��������� � ����������������� �����, � �������� � ����������� ���, ����� ����� �������� ���������� �� �������. ��������� ����� ������� ���������� ������, �������������� ���������� ������������ �����. ��� ���������� ������������ ����� ������� ���������, ���������� � ����� ����� ���������, ��������� � ������� �������� ��������������� ������ �������������� �����. ������ ������������ ������� �������� ���������� � ����� ��������� ����������� � ������� �������������� ����� �������������� � ������ ������ ��������� ������� ������, ��������� �� ���������� ����� 20202. 
<P class=Main align=justify>&nbsp;��������, ������������ �� ���� 
<P class=question align=justify>�� ����� ������ �������������� ����� �������������� ���� �������� �������� �������, ������������ �� ���� ��� ������� ��� ������� ����������� ������: �� ������� ������ �/� 304 &#171;������� �� �������������� ����� ������ �����&#187; ��� �� ������� ������ �/� 47403 � �/� 47404 &#171;������� � ��������� � ��������� �������&#187;? 
<P class=Main align=justify>� ������������ � ������������� ������������ �������������� ����� � ���������� ����� ������, ��������, ������������� �� ���� ��� ������� ��� ������� (��� � ������, ��� � � ����������� ������), �������� ��������� �� ����� 47404 &#171;������� � ��������� � ��������� �������&#187;. 
<P class=Main align=justify>&nbsp;������� ���� ����� ����������� ������ �� �/� 47416? 
<P class=question align=justify>����� 6 ���������� 28 � ��������� ����� ������ &#8470;2� �� 03.10.2002 ��������������� � ������ ���������� �������������� ����������, ��������� ��� ������������� �������� � ��� ���������� ����������� �� ��������� �������� ����� ��������� ���������� �� �/� 47416 ������ �� ����� ���� ����. ������ ��������� &#8470;2� ���������������� ������ �� ����������� �������� ������� ����� ������������ ������ �� ���������� ������. 
<P class=question align=justify>����� �� ���� ��������� �������� � ����������� ������ �� �/� 47416 �� ����� ������ ����? ������ ������������ ����������� ������������ ������� � ����� �������� ������� � ����������� ������, ���������� �� ����������������� ����� � ������������������ � � ���������������� � ����������� �� ����� ������������ �����������? 
<P class=Main align=justify>� ������������ � ������������� ������������ �������������� ����� � ���������� ����� ������, �������� ��������, �������� �� ���������� ����� 47416, �������� ��������� �� ��������������� ����� �� ����������, ���� �������� � ���� ����������� � ������� ���� ���� &#8212; � ������������ � ������� 1.5.4 ����� III ��������� ����� ������ &#8470;205�. ����������� ���� ���������� �� ��������� ������ 80 �� &#171;� ����������� ����� �� (����� ������)&#187;, �������� ����������� �������, ����� ���� ������������� �������� �� ����������� �������� �� ������ ��������� ���� ������������ ����, ���� ������� ��������������<STRONG> � �������� ���������� ���������� ���������.</STRONG> 
<P class=Main align=justify>����� �������� ������� (� ��� ����� � ����������� ������), ����������� �� ����������������� ���� ��������� ����������� <STRONG>�� ������-������������ </STRONG>� �������� � ���� ������������ ������ �� ���������� ����� 47416, ������ ���� ��������� �� ����� ����������� ������� ��� ���������� �� ������������ �� ���� ���������� ����������� � ����� ������� ��������� � ���������������� �������&shy;����������������. ����� ����� ��������� ������������ ����������� ����� ������ �� ������������ � � ����� � ���� ����� ������� � ������������ � ������������� ���������� ��������� �������� �������. 
<P class=Main align=justify>� ����� � ��������������, ���� ������ ��������, ��� ������� ���������������� �� ��������� � <STRONG>�������&shy;�������������</STRONG>, ������� ��������������� �� �������������� �������� � ������ ������������� ���������� � ������������ ������ �������� ����� ����, ��������� ����������� ������ ������������� �������������� � ��������������� ���������������� ���������� � �������� �� ��������� ����������� ���������. 
<P class=Main align=justify>&nbsp;���� ����� �� ����������� 
<P class=question align=justify>��������������� ������ ������ � ����� &#171;���������&#187; �� ��� ������� � ������������ ��������� �����, � ������������� ���� �������� �� ���� ������������ ����. �� ����� 91309 &#171;���������������� ������ �� �������������� �������� � ���� &#8220;���������&#8221; � &#8220;��� ����� �������������&#8221;&#187; ������� ������� ����������������� ������ �� &#171;����������&#187;. 
<P class=question align=justify>������� �� ����� � ������ ������ &#8212; � ����� � ���������� ����� �������� &#8212; ������� ���� 91309 � ���������������� ������� �� &#171;����������&#187;, ��� �� ����� ������ ������ ����� ��������� ������� ������������� �� ����� ������������ ����? 
<P class=Main align=justify>� ������������ � ������� 2.3.2.2 ���������� � ��������� &#8470;54� �� 31.08.1998, ��� ����������� �������� ����������/�������� �� �������� ��������� �����, ������������ ���������� ��������, ������������� ���� 91309 &#171;���������������� ������ �� �������������� �������� � ���� &#8220;���������&#8221;, � ����� &#8221;��� ����� �������������&#8221;&#187; �����������. �������� ����� �������������� ���������� �� ����� ��������� ���������������� ������� ����������������� � ����� � ����������� ���������� ��������. 
<P class=Main align=justify>&nbsp;�� ����������� ����������� ������� 
<P class=question align=justify>���� ������������� ������ ���������� ����� �� ����������� ��������������� ����������� ������� �������� ����������� ���������. ����� �� ����������� ������� ��� ��������� ��������� �� ������ �����, ���� ��������� � �������� �������, ��� ����������� �� �/� 61304 &#171;<SPAN class=question>������ ������� �������� �� ������ ���������&#187;, ���� ��������� � �������� �������.</SPAN> 
<P class=question align=justify>� ��������������� � ����� ���������� ������ ������� ��������� ��������� ��������� � ����� ����� �������� �������� �� ��������� ��������� �� ����������� �������, ���� ����� �������� � ������ ������ �� ����������� ���������? 
<P class=question align=justify>�� ����� ���������� ����� ������� ��������� ��������� �������� ��������� ����� ��������� ��������� ������ �� ������������ ������, ���� ���� �� ����� �������� �� ���������� �������� �� ����������� �������� ������� �� ���������� ���? 
<P class=Main align=justify>��������� ��������� � ����� ����� �������� �������� � �������� ����� �� ����������� ����������� ������� (����������� � �������� �������) ������� ���������� ����������� �� ����� �� ����� ������� � ����������� ���. 
<P class=Main align=justify>� ������������ �� ������� 5 ������ &#171;� ������ � ���������� ������������&#187;, ��������� �����������, ������ ���������� ��������, ������ ������������ ������ �� �������������� � ������ ���������� � ����������� ����� ����������� ��������� ��� ����������� � ��� ������ ��� �������� ���������� � ���������, � ����� ���� ������ � ������������ � ����������������� ��. 
<P class=Main align=justify>���������� � ��������� ����������� �������� �� �������� � ������� ���������� ������ ������������������ ���, �� ������ ������, �� ������������ �� ��������� � ���� ������ �� �������������� � ����������� �������������� ���������� ������. 
<P class=Main align=justify>� ������������ � ������� 1.16 ����� I ��������� ����� ������ &#8470;205�, ��� ������������� (������������) ���������� �������� ��������, ������������ (�����������) ���������� ������������� �� ���������� ������ �� ���������, �������� �� �������� ����������� ������ (��������) � ���������� ��������. 
<P class=Main align=justify>����� ��������� ��������� ������ �� ������������ ������, ���������� ������ �� �������� � ����������� ��������������� ����������� ������� � ������������ ������� �� ��������� ����� �������� ��������, ��������� ����� � ������������ ��������� � ������ ����������� �� �/� 42309 &#171;������ ������������ �������� ���������� ��� �� �������������&#187;, ���� �/� 42609 &#171;������ ������������ �������� ���������� ���&shy;������������ �� �������������&#187;. 
<P class=Main align=justify>&nbsp;���� ��������� ����� ������ 
<P class=question align=justify>�� ����� ���������� ������ �� ����� �������� � ������ ������ ������� �������� ������������� ���������� ������������� ��������� ����� ������? 
<P class=Main align=justify>�������� ���� �� ��������� �������������� ����� &#8212; ��������� ���������� ��� ������ &#8212; �������� ���������� � ������������ � �� ������������� ���������, � �� ����������� ������. ����� ����, ��� ��������� ��������������� �������� ���������� �������� �� �������������� ���������������� ����������� �����. 
<P class=Main align=justify>����� �������, �������� ������������ ����� ������ � � ������������ � ������� 5.2 ����� II &#171;�������������� ������&#187; ��������� &#8470;205�, ������������� ���������� ������������� ��������� ����� ������ ������� ��������, ������ �� ����� � ������ ���������, �� ���������� ������ �� ����� ������ �������� ������������. 
<P class=Main align=justify>��������������� ��������� �������������� ������������� ��� �������� ��������� � ���������� � ���������� ����� ������ &#8470;110� �� 16.01.2004.</P></TD></TR></TBODY></TABLE>


</body>
</html>

