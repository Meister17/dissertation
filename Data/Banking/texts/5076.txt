<!--
ID:35976119
TITLE:���� &#147;�������� �����&#148;
SOURCE_ID:3126
SOURCE_NAME:������� ����������� ����
AUTHOR:
NUMBER:12
ISSUE_DATE:2005-12-31
RECORD_DATE:2006-03-15


-->
<html>
<head>
<title>
35976119-���� &#147;�������� �����&#148;
</title>
</head>
<body >


<P align=justify><FONT face=Wingdings>&#178;</FONT> <B>���� � ��������� &#8220;LUCKY MOTORS&#8221; ����������� ����������� ��������� ������������ &#8220;������ &#8220;������������� +&#8221;.</B> � ������ ����� ������� ���������� ������ ���������� � ������ �� ������������ �������� ���������� ����� RENAULT, NISSAN, SUZUKI, KIA, ���, �������������� � ������ &#8220;LUCKY MOTORS&#8221;. ���������� ������ �� ������� ����� ������������: ��� ����� ������������ 1 ��� ���������� ������ � ������ �������� 8% �������, 2 ���� &#8212; 10% �������, 3 ���� &#8212; 12% �������. ������������� �� �������� �������� ����� ����������� ��������� 1% �� ����� �������. ������ ��������������� �� ����� �� 70 000 ���., �������������� ����� &#8212; �� ����� 50% �� ��������� ����������. ��� ���� ��� ����������� �� �����, ��� ��������� ������� ���������� �������������� ������� (�������). ������ ����� �������� ��������, ��� ���� ������� �������� ������� �� �����������.</P>
<P align=justify><FONT face=Wingdings>&#178;</FONT> <B>���� ���������� &#8220;�������&#8221; ����� �����������: ������ ����������� � ����� � ������������ 15 ����������, 5 �� ������� �������� �������� ������ �������� �������� �������.</B> ����� ��������� ��������� � ������������� � ����������. ��������� ��������� ������������ ����������� ����������� ���������� ������� ��������, � ����� �������: ������ ����� �������, �������������, ������������� � ������� �����, ������������, ���������; ������ ������������ ��������; ��������� �������; ������������ �������� ������� �� ����� � ����� (������� �������������� � ������, ��������, ����); ������� ������� �� ��������-����; ������ ����� ���������-�����������; ������� ������� �� ����� (�� �������������; �� ����� ������� ����� � ������� ����������, �������� �������� ��������); ������� �������� ������� �� ����� VISA � MasterCard. ��� ���� ��� �������� �� ����������� ������ ����� �����, ������������ � ��������� ��������, �������������� � ����� ��������� ���� &#8220;�������&#8221;, � ����� ������������ �� ����������� ����� � ����� ������������ ��������� ��� ����, ��� ������������ ������. ��� ������� �� �������� ����� �����������-���������� �������, �������� ����������� ���������. � ���� ������������� ���������� &#8220;�������&#8221; ������������� ����� �������� ���������� � ������������� ��������� ������: ��cord, Visa, MasterCard, STB, Union Card.</P>
<P align=justify><FONT face=Wingdings>&#178;</FONT> <B>������� ��������� ����������� ���� �����, ������������������ ��������, ������� ������������������� ������� ������� ��������� �� �����������: ���������� ����������� ����������� ����, ������������� ������, ��������� 200 ���. ����. </B>������������� � ������� &#8220;��������-����&#8221;, ��������� ������ ����� ��������� � ��������� ����������, ��������� ����������� ����� � ������� ������� ������� �� ��������. ��������� ������ ������ ����� � � ���������� ��������, ������� ����������� ����������� SMS-������� ����������� ������� ������, �������� ����� ������������� �� �������, ����������� �����, ���������� ����� �� ���� ������� ������� &#8212; ������ ������� SMS-������� �����. ���������� ����� �������� ����� &#8220;����������&#8221; � ���������, �������� ������ �� ��������� ���������� ������; ������� � ��� ������������� ���������� ����� ����� ���������� �������������� ����, � ��� ����� &#8220;�����������&#8221; ����� Visa Virtuon, ��������������� ��� �������� � &#8220;��������-���������&#8221;. ������ �������������� ������������, ���� ������ � ������������� �������: ������� ������� �� ��������� ������ ���������� ��� �� 1 ������� ��������� 903 ���. 784 ���. ������. ������� ������� �� ��������� ������ ������� �������� &#8212; ��� �������� ���������� ������������� ���������������� ��������� �������� � ������. ������ ������ �� ����������� ������� �� ��������� ����� ���� �������� ����� �� ���������� ����. � ������� ���� �������� ���������� ������ ������������ ������� �� ����� ����������.</P>
<P align=justify><FONT face=Wingdings>&#178;</FONT> <B>1 ������� ����� ������ �������������� ���� �����, ������������� � �������-��������������� ������ &#8220;���&#8221;. </B>� ����� ����� ����������� ���� ������ ���������� ���������: ������� &#8212; ����������� ���� ����� ��������������� ����������� ����� �������. ������� ������� ������ ������� �����, �������� ������, ��������� �������� ������� � ����� ����� ����, ����� ����������� ����������� ����� (� ��� ����� � ���������). ��� �������� ����������� ��� &#8220;���&#8221; ����� � ����� ������ ���������� �������� � �������� ������ ��������: �������� ������� ������ ������ ���������� �� ������� ������� &#8212; ��� ����������� �� ������ �������� ������ � ����������� ����, �� � ��������� �������� �������� �������� � ���������� ����������� �������� ����� �� ��������� ���� �������.</P><I>
<P align=right>������� �����������, �����-���������</P></I>


</body>
</html>

