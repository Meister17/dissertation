<!--
ID:38139826
TITLE:�� ������ �������� �� ������������� ���������� ��������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:17
ISSUE_DATE:2007-09-15
RECORD_DATE:2007-09-12


-->
<html>
<head>
<title>
38139826-�� ������ �������� �� ������������� ���������� ��������
</title>
</head>
<body >


<P align=justify>� ����� � ������������� ���������� ����������� �������, ������������ ���������� ������������, � ����������� ����� ����� ������, �������� ������������� ���������� ��� � ������� �������, ���� ������ ������� ��� ����������� �������� �� ������������� ���������� �������� � ��������� ������ � ���:</P>
<TABLE cellSpacing=2 cellPadding=7 width=610 border=1>
<TBODY>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>&#8470;</P>
<P align=center>�/�</FONT></P></TD>
<TD vAlign=center width="34%"><FONT size=3>
<P align=center>����, ���</FONT></P></TD>
<TD vAlign=center width="25%"><FONT size=3>
<P align=center>�����</FONT></P></TD>
<TD vAlign=top width="18%"><FONT size=3>
<P align=center>����</P>
<P align=center>�����������</FONT></P></TD>
<TD vAlign=top width="18%"><FONT size=3>
<P align=center>������������-��� �����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="34%"><FONT size=3>
<P>������������� ���� ��������������</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>������</FONT></P></TD>
<TD vAlign=top width="18%"><FONT size=3>
<P align=center>21.12.1990</FONT></P></TD>
<TD vAlign=top width="18%"><FONT size=3>
<P align=center>1200</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="34%"><FONT size=3>
<P>����������� ���</P>
<P>(�������� ������������ �����������)</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>������ ��������</FONT></P></TD>
<TD vAlign=top width="18%"><FONT size=3>
<P align=center>10.12.1990</FONT></P></TD>
<TD vAlign=top width="18%"><FONT size=3>
<P align=center>1141</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="34%"><FONT size=3>
<P>�������</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>������</FONT></P></TD>
<TD vAlign=top width="18%"><FONT size=3>
<P align=center>04.07.1994</FONT></P></TD>
<TD vAlign=top width="18%"><FONT size=3>
<P align=center>2955</FONT></P></TD></TR></TBODY></TABLE><FONT size=3></FONT><I>
<P align=right>�����-������ ������������<BR>������� � ������������ ������<BR>����� ������ �� 16.08.2007</P></I>


</body>
</html>

