<!--
ID:37637909
TITLE:��������� ��������� �������������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:9
ISSUE_DATE:2007-05-15
RECORD_DATE:2007-05-11


-->
<html>
<head>
<title>
37637909-��������� ��������� �������������
</title>
</head>
<body >


<P align=justify>������������ ��������� ������������� �� ���������� ������������� ������� � ���, �������� �������� �� ���������� ���������� ��������, ���������� ���������� � ����������� ������������ ����� ���������� � ��������� ��������� ������������:</P>
<TABLE cellSpacing=2 cellPadding=7 width=610 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>&#8470;</P>
<P align=center>�/�</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P align=center>������������</P>
<P align=center>���������</P>
<P align=center>�����������</FONT></P></TD>
<TD vAlign=top width="13%"><FONT size=3>
<P align=center>��������-������� �����</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>����</P>
<P align=center>������<BR>��������</FONT></P></TD>
<TD vAlign=top width="43%"><FONT size=3>
<P align=center>�.�.�. ������������</P>
<P align=center>��������� �������������,</P>
<P align=center>�������� �����, �������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P>�������</FONT></P></TD>
<TD vAlign=top width="13%"><FONT size=3>
<P align=center>734</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P>06.04.2007</FONT></P></TD>
<TD vAlign=top width="43%"><FONT size=3>
<P>367000, ���������� ��������, ���������, ��. �����������, 6�</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P>���</FONT></P></TD>
<TD vAlign=top width="13%"><FONT size=3>
<P align=center>2770</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P>28.03.2007</FONT></P></TD>
<TD vAlign=top width="43%"><FONT size=3>
<P>115054, ������, ����������� ���.,<BR>48-50, ���. 1, ������� 376</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>&#8470;</P>
<P align=center>�/�</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P align=center>������������</P>
<P align=center>���������</P>
<P align=center>�����������</FONT></P></TD>
<TD vAlign=top width="13%"><FONT size=3>
<P align=center>��������-������� �����</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>����</P>
<P align=center>������<BR>��������</FONT></P></TD>
<TD vAlign=top width="43%"><FONT size=3>
<P align=center>�.�.�. ������������</P>
<P align=center>��������� �������������,</P>
<P align=center>�������� �����, �������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P>���-�������</FONT></P></TD>
<TD vAlign=top width="13%"><FONT size=3>
<P align=center>3305-�</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P>22.03.2007</FONT></P></TD>
<TD vAlign=top width="43%"><FONT size=3>
<P>105062, ������, ��. �������-�������������, 16-18, ���. 1</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P>��������� ���� ��������</FONT></P></TD>
<TD vAlign=top width="13%"><FONT size=3>
<P align=center>2501</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P>06.04.2007</FONT></P></TD>
<TD vAlign=top width="43%"><FONT size=3>
<P>109147, ������, ��. ������������, 34, ����. 7</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>5</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P>�������-�������</FONT></P></TD>
<TD vAlign=top width="13%"><FONT size=3>
<P align=center>1687</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P>28.03.2007</FONT></P></TD>
<TD vAlign=top width="43%"><FONT size=3>
<P>368000, ���������� ��������, ��������, ��. ��������, 28</FONT></P></TD></TR></TBODY></TABLE><I>
<P align=right>"������� ����� ������", 2007, &#8470; 22, 23</P></I>


</body>
</html>

