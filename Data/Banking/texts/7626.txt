<!--
ID:37127974
TITLE:� ����������
SOURCE_ID:10908
SOURCE_NAME:������������ ���������� ������
AUTHOR:������ ������
NUMBER:12
ISSUE_DATE:2006-12-31
RECORD_DATE:2007-01-09


-->
<html>
<head>
<title>
37127974-� ����������
</title>
</head>
<body >


<DIV class=article>
<DIV class=header>
<H2><FONT size=3><SPAN>� ��������� �������</SPAN></FONT></H2></DIV>
<DIV class=lead align=right><STRONG><EM>���� ����� ����� ��������� ���� ������� ���������, �� ������� �� �������� ��������: ��� ���� � ��������� ����� �������� ������ � ����� ����������?</EM></STRONG></DIV>
<DIV class=vrez2>
<H3><FONT size=3>����� </FONT></H3>
<P align=justify><B>������ ������</B>, �������������� ����-��������� �� �������� �������� ���������� ���������� ������ (���). ������� ���������� ��������������� ����������� ��������. ������� � ����������� �����. � 2002 �. ������ � ��� �� ��������� ��������������� ����-����������.</P></DIV>
<P class=bukvitza align=justify>�������� ����� ����, �� ������� �������� �������� �������, �������������� �������� ������ �������, ������� ����� ����������� � ��������� ������������ �� ��������� ������������ ������ &#8470; 115-�� &#171;� ��������������� �����������:&#187;. � ���������, �������� ������ &#171;� ������ � ���������� ������������&#187;, ��������� �����������, ������ � ������� ���� ���������� ���������� ����������������, � ��� ����� ����� &#8470; 115-��, ����� �������� ��������. �� �� ������, ��� �������� ����������� ��������� ����������� ��� ���������� ����� ������ ������� � ����������� �������� ���������� � �������������� ������. ����� �������, � ���������� ���� ����������� ������, ������� �������� ����������� � �������� �������� ���� �������� ��� ��������������� �������� ���������� � ��������� �� ��������� �����, ��� ������ �������� �������� ��� ������� �������� � ����������� ����� �������. ������ ����� ��������� ��������� ��������� ��� ������ �����, ���������� �� ��� �������, ���������� ��������. �������, ��� ��� ���� ���� ��������, ��� ������ ��������� ����������.</P>
<H2 class=body><FONT size=3>���� ������� </FONT></H2>
<P class=body align=justify>�� ��������� ������, � ������� ���� �� 1250 ������ �� ��������� ������ ������� ��������� ��������������� ������ ���� 263 ��������� �����������. ��������, ��� ����� �� ����� �������� � ����� �������� �� ����������� ������. ��, ��� �� �����, ����������, ��� ������� ���� ������ ������ ������ ������������ �������� ����������� ���������� ���������� ������.</P>
<P class=body align=justify>����������� ���� ����� ������������ ��������� � ����� 115-�� ��, ��� ���������� ����������� � ��������������� ��������� �����������, ������� �� ������������� ��� �� �����-�� ������ ����������. � ��� �����, � ���������, ������� �������� �� �������������. ��� ��������, ��� �������� �� �������� ����������������, ��� �� ��������� �������� ���������������� ��� ������������ ������. ��� ���������, ������� ������������� � ������, ����� ��������-��������� �������� � ��� ������, ��� ��� �� ����� ������ � ����������� ������ &#171;�������&#187; ����.</P>
<P class=body align=justify>����� � ������ �������� ����������� ��������. �� � ���� �������� ��, ��� ��� �������� ������������ �������� �� ������������� �������� �������. ���� ��� ��������, ��� �� ������������ ���������������� �� ��� �������� �������� ������, � ��� ���-���� ����������.</P>
<P class=body align=justify>� ������ �������, �������� � ����� ��������� ����� ��-�������. ���� ��������� ���� ������� ����� ������������� ����� ��������� �����������, �� ������� �� �������� ��������, ������ ��� ����������. ��� ���� � ��������� ����� �������� ������ � ����� ����������? ���� �������� �������, � �� ������ ���������.</P>
<H2 class=body><FONT size=3>�������� ������� </FONT></H2>
<P class=body align=justify>�� ������ ����������� ����������, ������������� ���������� ������������ �������� ��� ���������, ������� ����� ����������� ���������� ������������� ��� ���������� ������ &#8470; 115-��, � ���������� ����������� ���������� � ��� ������� ��������� ��������. ��, ��������� - ��������, �������� ����� - �����������, � �� ��� ������ ����������� �����-�� ���������, �� ��� ������ ���� ��������� ����, ��� �����������. �����, ��� �� �������������� ���������� ���������, ����� ���� ������ ��� �������� ���������� � �� �������, �� ��� ���� ���� ������ � ���-���� �������� ���������, ����� ����� ���� ���������� � �������� 20-50 ����.</P>
<P class=body align=justify>������ �������� ��������� ����������� ����� ������ �� ����������, ���������� � ������ ��������� ���� ������. ��������, ���������� ������� ��������������� ����������� � �����, ��� ����� ���� ���������� ������������ � ��� ���������������� ������������ �� ������ ������. � ���� ������ � �������� ����������������� ��� � ��������� ����������� ����� ���� ��������� ������ �������� �������.</P>
<H2 class=body><FONT size=3>�� ���������� �������� � ����������� </FONT></H2>
<P class=body align=justify>���� ����������� ������� ��������������� ������������ ��������� �������� ��� ����� ����������, ������� ���������� � ������ &#8470; 115��. ����������� ������������ ������ ����������, ������� ����� �������� � ����������������. �������� ���������� ���� ��������� �����������. ���, ���� � ������� ���� � ���������������� ���� �������� 8 ���. ���������, �� � ���� ������� ������� ��� ������� 15-16 ���. � �� �� ����� �������������� ������� �� ��������� ������� ��� ���� �� ��������� ������� ��������� � ������������ ��������������. ������ ����: ��� 8 ���. ���������� ��������� ���� ���������� ����� ����� ��������� ���! ����� �������, ��������, ���� ������� � ����������. � ������������ ���������� ����� ����� ���� �����, ����� ������ ���� ������� �� ���� � �������� �������.</P>
<P class=body align=justify>������������� �� ����� �������? ����� ����� ���������� 8 ���. ���������, ���� �� �� ��������� ���������� ������ ����� ��������� ���? �������, ��� ���������� �� ��������� 7 ���. 999 ���. ��������� �� ������� � ������������ ���������� �������. � ���� ��� � ��� �� �������, �� ����, � ��� ����� ����� �����? ������ ����� ������ �������� � ����� ����������, ������� ������ �� ���������� ����? ��� ���� �� ��������������� ��������� ��������� ����������� ��� � ����������. ����� ������, ����������, ��� �����, ���� ������ �������� �����, �������� ��������� � ����� �����. � ����� ����, ��� ���������� �� ����� ������, ������ ��� ������ ����������. ���� ���������������� � ���� �������� ���������� ���������� �������� ������, ��������� � ����������������, �� �������� ���� � ������������������ ������. ��������� ����� ��������� ������������ ����������� ������. ����������, �� ��� ���� ���� ����������, ��� ����������, ����� ��� - ���� ��� ��������. � � ������ - �������� �����, �����, ������ ��������, ����������� ����� �����������...</P>
<P class=vrez2 align=right><STRONG>� 2005 ���� �� ��������� 8 ���. ���������� � ���������������� ��������� ���� ���������� ����� ����� ��������� ��� </STRONG></P>
<P class=body align=justify>�� ��������, ��� ��� ������� ������������� � ����� �������� ����� ����������, �� ������� ����, ��� ����� ��������������� ����������� ���������� �������. � ����������� ����� ������� � ������ ������������ ������� ����������� �������, ����� ���� ��� �������� �������� �������������� �������� � ������ � ��� �������� �������������� �������. � �������, ���� ����� ���� � 2004 �. �������� � ��������� ������ 12 ���������. �� ����� ���. � � ��� �������� �������� ������� 20 ���. ��������� � ����.</P>
<P class=body align=justify>�������, ��� ��������� ������ ����������� �������, ����� ��� ��������������� �� ��������� ������������ �������� ������� �� ����, ������ ����������� ��������������� ��������� ����������� �� �� ��������. ��, �� ������� ����, ���������� ������ ������ ����������� ���������������� ��� ���� ������ ������ �� �������������� ���������.</P>
<H2 class=body><FONT size=3>� ����������� </FONT></H2>
<P class=body align=justify>���� �� � ��������� ���� ����������� ����� ������ ������� ������ ��������. ������ ���, ��-������, ��� ��������� ���������� �������� ������ ��� ��������� � ������� ����������� �������, � ��-������, ��������� ��������� ������ �� ���������� ������ ������, ������� ���������� ������������� �������������� ����������. ������, �� ��� ������, ������� ��������������� ��������� ���������� ������� ������� �������������� ��������� �������. ��������� ����������� � ������ ��������� �������, ������������ �� �������. � ���� ��� ���, �� ������������� ������� � ���������� ������� ���������������.</P>
<DIV class=vrez2>
<H3><FONT size=3>������... </FONT></H3>
<P align=justify><B>� �������������� �� ��������������� ������ ���������� ������������� ��������� - &#171;��������� �� ���������� �� ��������� �����&#187;.</B> ��� �������, ������� ���������� ��������� �� ���������������� ������������, ��������� ���� ����������� ���������� �����.</P>
<P align=justify>&#171;���������&#187; ����� ���������� ���������� $20 ���. ������� ����������� �� ���� ������� �����, ����������� &#171;�����&#187;, ���� ���������� ����� ����� � ������� ��������� ���������� ������� ���������� �������� ������������ �������� �� ������� ���������� �����. ������ ���� ���������� ��� ��������, ��� ������� �������� � ������ ������� �������� ��������� � ���� �� ���������������� ������������.</P>
<P align=justify><B>� ����� ����������� �������� ����� ��������� ��� ��������� �������� ��������������. </B>��� �������� �� ���������� ������ �����. ������ ���������� ��������, ������������ � ����������������, ��� ������� ������� ���� ������ ��������� � ����������� �������������� �����. ������ ��������� �������� �������� ��� ��������, ����������� � ������������ (13 �������������� ���������) � ����������� ������ (11 �������������� ���������).</P></DIV></DIV>


</body>
</html>

