<!--
ID:35725802
TITLE:A chance to win business benefits 
SOURCE_ID:3240
SOURCE_NAME:Banker
AUTHOR:
NUMBER:957
ISSUE_DATE:2005-11-30
RECORD_DATE:2006-01-10


RBR_ID:1093
RBR_NAME:���������� ����
RBR_ID:4933
RBR_NAME:������ ����������� ��������� ������
RBR_ID:4939
RBR_NAME:������������� ���������� ������������
-->
<html>
<head>
<title>
35725802-A chance to win business benefits 
</title>
</head>
<body >


<P class=fancy align=justify><STRONG><I>Ravi Varadachari</I> explains how banks that plan to comply with Basel II&#8217;s advanced approaches can use them for strategic decision making, such as capital allocation for individual lines of business.</STRONG></P>
<P align=justify>One of the chief drawbacks of Basel I was its inability to demand that capital be held by banks commensurate with their underlying risk. The Basel Committee, therefore, initiated work on Basel II with this underlying purpose and attempted to construct rules to this effect.</P>
<P align=justify>The endeavour has been partially fulfilled, particularly for banks following the advanced approaches. Basel II, further, has a menu of approaches to credit risk to allow banks to evolve gradually: from a relatively easy-to-implement method like the standardised approach to an advanced internal ratings-based (IRB) approach.</P>
<P><B>The Basel I world</B></P>
<P align=justify>Banks endeavour to manage risk at the individual level as well as the portfolio level. In the Basel I world, banks would appraise entities based on risk factors and arrive at a go/no-go decision. Subsequently, the approved companies would be classified into homogenous risk categories. This would be the individual level analysis.</P>
<P align=justify>At a portfolio level, banks would check for concentrations (geography, industry or group companies) and ensure that they were at an acceptable level. From a regulatory perspective, the capital to be held would be based on a risk weight that was 100% for corporate exposures and 0% for sovereign exposures. A capital of 8% had to be held on the risk-weighted assets.</P>
<P align=justify>In a sense, Basel I was an attempt to measure capital based on the risk profile of the assets. However, it was a broad brush approach with little differentiation among credit exposures and had other shortcomings, too, like capital not being allocated for operational risk and possibilities of regulatory arbitrage.</P>
<P align=justify>As Basel I had these shortcomings, banks started using their internal methods to estimate capital commensurate with underlying risks for their internal decisions and the amount of capital to be maintained. Also, because banks understood their risks better, they were in a better position to quantify them using internal models. Hence, the origin of the terms &#8216;economic capital&#8217; (implying capital to be held based on the risks) and &#8216;regulatory capital&#8217; (implying capital to be held for regulatory compliance).</P>
<P><B>Estimating economic capital</B></P>
<P align=justify>The first step in estimating economic capital is to identify all the key risks to which a bank is exposed. Capital is needed to protect banks against risks like credit, market and operational risk.</P>
<P align=justify>Subsequently, adjustments need to be made for correlations that may exist among the different risk categories. Event risk also needs to be factored in (like the Asian crisis or the Russian crisis), which may not be appropriately handled by standard statistical techniques.</P>
<P align=justify>Thus, the idea is to find the <I>ex-ante</I> (forecast) capital that has to be held by a bank for all potential risks. Many banks have developed methodologies for estimating economic capital.</P>
<P align=justify>The typical steps for estimating economic capital (using a bottom-up approach &#8211; a top-down approach can also be used but decomposition of the capital across business lines may be difficult) for credit risk are:</P>
<P>
<UL>
<LI>Estimate the probability of default (PD) of a company and categorise companies with homogenous PDs in categories like AAA and AA. </LI></UL>
<UL>
<LI>Based on past rating history and PDs, build a transition matrix giving the probabilities of rating migration and default. </LI></UL>
<UL>
<LI>Estimate the <I>ex-ante</I> loss given default (LGD) based on a variety of factors including collateral. </LI></UL>
<UL>
<LI>Estimate the <I>ex-ante</I> exposure at default (EAD). </LI></UL>
<UL>
<LI>Aggregate the individual factors to estimate the loss distribution (this will be a multiplication of PD, LGD and EAD). Since parameters like LGD would not be a deterministic number, given the uncertainties associated with the above parameters, it would be best represented by a distribution. Hence the loss curve is also a distribution. </LI></UL>The mean of the distribution (also referred to as expected loss) would be used for pricing while the deviation around the mean (also referred to as unexpected loss &#8211; unexpected loss is also defined as the loss over and above the average value or mean at a certain confidence interval) would be used for capital allocation. The loss distributions can be generated through a simulation process or through an analytical model. 
<UL>
<LI>Incorporate factors that measure concentrations like correlations as additional inputs while generating the loss distributions. Correlations to be incorporated would include factors like industry, rating categories and geography. Subsequent to PD estimation, credit risk can be assessed in two ways: check for probability of default only (the default mode); or, besides defaults, look for rating migrations (the mark-to-market mode). </LI></UL>
<P align=justify>After the loss distribution is drawn up, a bank might want to maintain capital on a certain confidence interval (CI). Thus, a bank may state that it would maintain capital based on a 99.97% CI. This also means that the PD of the bank is 0.03%, which is equivalent to one year PD of an AA company based on data from rating agencies. This is normally referred to as the risk appetite of a bank. Rating agencies would not normally agree with this interpretation because they consider many other risks like strategy risk and management risk (apart from credit, market and operational risk) prior to arriving at the rating decision.</P>
<P align=center></P>
<P><B>The Basel II world</B></P>
<P align=justify>The Basel II regulatory formula, like most other methods for determining economic capital, follows the six steps explained above. However, substantial simplifying assumptions are made in steps 4 and 5. The table above highlights the similarities and differences among some of the popular methods.</P>
<P align=justify>Based on any one of those methods, the economic capital required for each business unit can be estimated. This is also referred to as the standalone economic capital. The economic capital would be less than the sum of all the standalone capitals due to diversification benefits.</P>
<P align=justify>Hence, most methods would measure correlation among the different business units.</P>
<P align=justify>If the correlation across all the business units is 1, then the economic capital would be a simple addition; in all other cases (ie, for correlation being &lt;1), the economic capital would be less than the simple addition. The amount of capital that each business adds to the total capital is also referred to as the marginal economic capital. Thus, for all scenarios except correlation equal to 1, the standalone economic capital would be more than the marginal economic capital.</P>
<P align=justify>The steps subsequent to estimating the standalone and marginal economic capital are simple. One method is to use the standalone economic capital for all evaluations like pricing, capital allocation and bonus. Another school of thought is to use marginal economic capital for pricing, internal capital allocation and evaluating acquisitions or divestitures, and standalone capital for measuring the performance of business managers (the size of the bonus to be paid to the manager).</P>
<P align=justify>For banks following the advanced approaches, the Basel II formula can be used for estimating the capital.</P>
<P><B>Accord winners</B></P>
<P align=justify>The winners in the next era will be financial institutions that are in a better position to assess and manage risk. Building appropriate risk management capabilities requires substantial investment in processes and technology. Basel II has now given an opportunity to have a single system that can be used for compliance as well as risk management. Banks that can exploit the accord for business benefits stand to gain.</P>
<P><I>Ravi Varadachari is a Senior Consultant, Credit Risk, at i-flex.</I></P>


</body>
</html>

