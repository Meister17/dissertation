<!--
ID:35284924
TITLE:�������������� �������� �� ������������ �������� ����� � I �������� 2005 ����.. ������ �������� ����������� ������
SOURCE_ID:3099
SOURCE_NAME:������ �����-����������
AUTHOR:
NUMBER:2
ISSUE_DATE:2005-06-30
RECORD_DATE:2005-09-14


RBR_ID:4928
RBR_NAME:��������� � ������� ��������
-->
<html>
<head>
<title>
35284924-�������������� �������� �� ������������ �������� ����� � I �������� 2005 ����.. ������ �������� ����������� ������
</title>
</head>
<body >


<P><FONT>������ ������<SUP>1</SUP>, ���������� �� ������ ���������� �������������� ������ � �������� �������� ����������� ������ �� ����� &#8470; 0409601 �� I ������� 2005 ���� �������, ��� �� ��������� � ���������� ��������� �� ��������� ������������ ��������� �������� ����������, ��������������� ��������� ����� �������� ����������� ������.</FONT></P>
<P align=justify><FONT>�� ��������� � ������������ IV �������� 2004 ���� ����� �������� ����������� ������, ��������� ������� � ���������� ���, �������� �� 27,1 %; ����� ��������� ������ ����� �������� &#8212; �� 11,4 %. ����� ������� ����� �������� ����������� ������ � ���������� ��������� ���������� �� 33,2 ���. �������� ��� �� ��������� � ������� �� IV ������� 2004 ���� (������� 1). �������� ����������� �� ����� �������� ����������� ������ � ���������� ��������� ���� ������ ����� &#8220;�����-����&#8221;, � ����� �����������-������������ ����, ������������� ���� �����-����������.</FONT></P>
<DIR>
<DIR>
<DIR>
<DIR><B>
<P align=right><FONT>������� 1<BR></FONT><FONT>���. �������� ���</FONT></P></DIR></DIR></DIR></DIR></B>
<CENTER>
<TABLE cellSpacing=1 cellPadding=4 width=527 border=1>
<TBODY>
<TR>
<TD vAlign=top width="63%" colSpan=2><B>
<P align=center><FONT>����������</FONT></B></P></TD>
<TD vAlign=top width="16%" colSpan=2><B>
<P align=center><FONT>I �������</FONT></P>
<P align=center><FONT>2005 ����</FONT></B></P></TD>
<TD vAlign=top width="21%"><B>
<P align=center><FONT>IV �������</FONT></P>
<P align=center><FONT>2004 ����</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="63%">
<P><FONT>������� ������ � ���������� ���</FONT></P></TD>
<TD vAlign=top width="16%" colSpan=2>
<P align=center><FONT>551,7</FONT></P></TD>
<TD vAlign=top width="21%" colSpan=2>
<P align=center><FONT>756,6</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="63%">
<P><FONT>������� ������ ���������� �����</FONT></P></TD>
<TD vAlign=top width="16%" colSpan=2>
<P align=center><FONT>657,1</FONT></P></TD>
<TD vAlign=top width="21%" colSpan=2>
<P align=center><FONT>741,5</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="63%">
<P><FONT>������ ������� (������ ������-�������)</FONT></P></TD>
<TD vAlign=top width="16%" colSpan=2>
<P align=center><FONT>105,4</FONT></P></TD>
<TD vAlign=top width="21%" colSpan=2>
<P align=center><FONT>-15,1</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="63%">
<P><FONT>������ ���� (������ �����-������)</FONT></P></TD>
<TD vAlign=top width="16%" colSpan=2>
<P align=center><FONT>8,0</FONT></P></TD>
<TD vAlign=top width="21%" colSpan=2>
<P align=center><FONT>-25,2</FONT></P></TD></TR></TBODY></TABLE></CENTER>
<P align=justify><FONT>� I �������� 2005 ���� ����� �������� ����������� ������, �������� ���������� ����� � �� ������� �������� ������, �������� �� 13,2 %; ����� ������, �������� �� ���������� ��� ��� ���������� �� ������� �������� �����, �������� �� 24,9&nbsp;%. � ���������� ���������� ������ �������� �� ������� �������� ������ ���������� ��� ���������� �� 37,0 ���. �������� ��� �� ��������� � ������� �� IV ������� 2004 ���� (������� 2).</FONT></P><B>
<P align=right><FONT>������� 2</FONT></P>
<P align=right><FONT>���. �������� ���</FONT></P></B>
<CENTER>
<TABLE cellSpacing=1 cellPadding=4 width=654 border=1>
<TBODY>
<TR>
<TD vAlign=top width="71%"><B>
<P align=center><FONT>����������</FONT></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=center><FONT>I �������</FONT></P>
<P align=center><FONT>2005</FONT></B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=center><FONT>IV �������</FONT></P>
<P align=center><FONT>2004</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="71%">
<P><FONT>������� �� ���������� ��� ��� ���������� �� ������� �������� �����</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>301,5</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center><FONT>401,4</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%">
<P><FONT>������ ���������� ����� � ������� �������� ������</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>411,5</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center><FONT>474,4</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%">
<P><FONT>������ �������� �� ������� �������� ������ ���������� ���</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>110,0</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center><FONT>73,0</FONT></P></TD></TR></TBODY></TABLE></CENTER>
<P align=justify><FONT>����� �� �������� ����������� ������ �� ������� ���������� ���, ���������� � �������������� ���������� &#8212; ������ �������� ���������� ��� � �������� ����������� ������� (����) &#8212; ���������� �� 157,5 ���. �������� ��� �� ��������� � ������� ����������� �������� (������� 3).</FONT></P><B>
<P align=right><FONT>������� 3<BR></FONT><FONT>���. �������� ���</FONT></P></B>
<CENTER>
<TABLE cellSpacing=1 cellPadding=4 width=659 border=1>
<TBODY>
<TR>
<TD vAlign=top width="76%"><B>
<P align=center><FONT>����������</FONT></B></P></TD>
<TD vAlign=top width="11%"><B>
<P align=center><FONT>I �������</FONT></P>
<P align=center><FONT>2005 ����</FONT></B></P></TD>
<TD vAlign=top width="13%"><B>
<P align=center><FONT>IV �������</FONT></P>
<P align=center><FONT>2004 ����</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="76%">
<P><FONT>������ �������-������� �������� ����������� ������ ����� �������� ������ �������������� ������ (������ �������)</FONT></P></TD>
<TD vAlign=top width="11%">
<P align=center><FONT>105,4</FONT></P></TD>
<TD vAlign=top width="13%">
<P align=center><FONT>-15,1</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="76%">
<P><FONT>������ �������� �� ������� �������� ������ ���������� ���</FONT></P></TD>
<TD vAlign=top width="11%">
<P align=center><FONT>110,0</FONT></P></TD>
<TD vAlign=top width="13%">
<P align=center><FONT>73,0</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="76%">
<P><FONT>������ �������� ���������� ��� � �������� ����������� ������� (����)</FONT></P></TD>
<TD vAlign=top width="11%">
<P align=center><FONT>215,4</FONT></P></TD>
<TD vAlign=top width="13%">
<P align=center><FONT>57,9</FONT></P></TD></TR></TBODY></TABLE></CENTER>
<P align=justify><FONT>� I �������� 2005 ���� (�� ��������� �� 01.04.2005) �������� � �������� ������� ����������� � 836 ������������ ������ 37 ������������ ������ (352 ������������ ����) � 78 �������� ������������ ������ (484 ������������ ����). </FONT></P>
<P align=justify><FONT>�������� �������, ��������������� �������-�������� ��������, ��-�������� �������� ������� ����������� ������ &#8220;������� ������������ ����&#8221;, &#8220;��������&#8221;, ������-�������� ���� ��������� ������, &#8220;����&#8221;. �������� ����� ������ �����-���������� �� ������ �������� ���� �����-����-����, ���� &#8220;�����-���������&#8221;, �����������-������������ ����, ��� ����-�����-���������. </FONT></P>
<P align=justify><FONT>�������� ����� �������-�������� �������� � ���������� ��-�������� ���������� ������� � ����������� ������ &#8211; ����������� (���� �������� �� ������� ��������� 97,5&nbsp;%, �� ������� &#8211; 96,1&nbsp;%). �������� ��� ��������� ����� ����� � ����� ������ �������� � �������� ������� ��������: ������� ��� &#8212; 64,3 % �� ����������� � 64,7 % �� ������������; ���� &#8212; 35,3 % �� ����������� � 34,8 % �� ������������.</FONT></P>
<P align=justify><FONT>�������-�������� �������� ����������� � 48 ������ ����������� �����, � ��� ����� � 8 ������ &#8220;������&#8221; �����, ����� �������� � �������� ��-�������� ������������ (0,01&nbsp;% �� �������� � 0,01 % �� ��������) ��-�� �� ������ �����������. �������� ����������� �� ����� �������� � &#8220;�������&#8221; �������� ���� ������� ������ &#8220;������� ������������ ����&#8221;, &#8220;��������&#8221;, ����� &#8220;��� ���� &#8211; �����-���������&#8221;, &#8220;����������&#8221;, &#8220;�����������-������������ ����&#8221;, &#8220;�����-����-����&#8221;.</FONT></P>
<P align=justify><FONT>� �������������� ����� � I �������� 2005 ���� ��������� �������� ����������� ������ �� 24,8 % ������ � �� 23,6 % ������ ������������� �� ��������� � ������� �� IV&nbsp;������� 2004 ���� (������� 4).</FONT></P>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR><B>
<P align=right><FONT>������� 4<BR></FONT><FONT>���. �������� ���</FONT></P></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></B>
<CENTER>
<TABLE cellSpacing=1 cellPadding=4 width=580 border=1>
<TBODY>
<TR>
<TD vAlign=top width="65%"><B>
<P align=center><FONT>����������</FONT></B></P></TD>
<TD vAlign=top width="16%"><B>
<P align=center><FONT>I �������</FONT></P>
<P align=center><FONT>2005 ����</FONT></B></P></TD>
<TD vAlign=top width="19%"><B>
<P align=center><FONT>IV �������</FONT></P>
<P align=center><FONT>2004 ����</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="65%">
<P><FONT>���������<B> </B>�������� ����������� ������, �����</FONT></P></TD>
<TD vAlign=top width="16%">
<P align=center><FONT>1557,6</FONT></P></TD>
<TD vAlign=top width="19%">
<P align=center><FONT>2071,5</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="65%">
<P><FONT>������������� �������� ����������� ������, �����</FONT></P></TD>
<TD vAlign=top width="16%">
<P align=center><FONT>1562,9</FONT></P></TD>
<TD vAlign=top width="19%">
<P align=center><FONT>2046,6</FONT></P></TD></TR></TBODY></TABLE></CENTER>
<P align=justify><FONT>��������� ����������� �������� ����������� ������ � �������������� ����� � ������ ������������ �������� ����������� ������ �������� �� �������� 1 � 2.</FONT></P>
<P align=justify><FONT>� I �������� 2005 ���� ��������� ����������� ����������� �������� ����������� ������ � ����� �������������� ������ ��������:</FONT></P>
<P align=justify><FONT>- ������� � ���������� ���-���������� (34,6 %),</FONT></P>
<P align=justify><FONT>- ������� � ������-���������� (33,7 %),</FONT></P>
<P align=justify><FONT>- ����������� �� ������� ����� ���������� ���-���������� (18,9%).</FONT></P>
<P align=justify><FONT>��������� �������� ������������ �������� ����������� ������ ��������������� ������� ����:</FONT></P>
<P align=justify><FONT>- ������� ���������� �����-���������� (40,4 %),</FONT></P>
<P align=justify><FONT>- ������ ������ ���������� �����-���������� � �� �������� ������ (25,4 %),</FONT></P>
<P align=justify><FONT>- ������� ������ ������-���������� (22,7 %).</FONT></P>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR><B>
<P align=right><FONT>������ &#8470;1</FONT></P></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></B>
<P align=center><FONT></FONT></P><B>
<P align=right><FONT>������ &#8470;2</FONT></P></B>
<P align=center><FONT></FONT></P><B></B><I><SUP>
<P>1 </SUP>������ ��������� ��� ����� ����������� ������-��������� ����� ��������� ��, ������� ����������� ��� ������� �������� �������� ����������� ������ � ����� �� ���������� ���������.</P><FONT></FONT>
<P align=right><FONT>����� ����������� ����������� ���������<BR></FONT><FONT>�������� � ������������� ��������<BR></FONT><FONT>�������� ���������� ����� ������ �� �����-����������</FONT></P></I>


</body>
</html>

