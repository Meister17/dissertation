<!--
ID:35820110
TITLE:������� ����������� ���� ���������� ������� ��� ��������� ������������ ���������� �������
SOURCE_ID:3221
SOURCE_NAME:������� � ������
AUTHOR:�.�. ������
NUMBER:34
ISSUE_DATE:2005-12-10
RECORD_DATE:2006-02-02


-->
<html>
<head>
<title>
35820110-������� ����������� ���� ���������� ������� ��� ��������� ������������ ���������� �������
</title>
</head>
<body >


<P align=justify>����� �� ��������� �������� ������������ ���������� � ������������� ��������� �������� � �������� ���������� ��������� �������� ����������� ���������� ����������� ���� �����, �������������� ������������ ������������ �������� �������� �� �����. ��� ���� ��������� �������� ���� ����� ������ � ������ ���� ��������������� ������������� ������� ����������� ����� ������������� ��������, �. �. ���� ���������� ����������� ����������� �������� �� ������������ ���������, ������������������ ��������� � ��� ��������������� �������������, ������� ����� ������� � ������ ������ ������������� ������������ � ��.</P>
<P align=justify>� �������� �������� �������� ������ ������, ������������ ��� ������������ �������������������� ���������, �����������, ��� �������, � ������������ � ������������ ���������� ���� ����� ��� ������������������ �����. ������ ������������ ���������� �������, ��� �� ������� ����������� � ������� ����������� ������ ����� ������� ��������� ����� ������.</P>
<P align=justify>������ ������������� ������� ���������� ������� ������������ �������� ������ ����� ��������������� �, �������������, ������� ������� � ����������� ����� ������������� �����. ���, ��������� �� ����� � �. ����������� ���������� ������ ���� ����� ��������� �� ������ ����������������� ������, ��������������� �� ����� ������������� ����������� �������� (0,12).</P>
<P align=justify>�������� ������������ �������� ���� ���������������� ����� ������� �������������� � ������ ��� �������� � ���������� ������ � ����� ��������� ����������� ������� � ������������ �������.</P>
<P align=justify>������������� ����� ������ � ���������������� ����������� ����� ������������� ����� �� ������ ������������ ������������� ������������� ��������� ��������. �� �������� �. ����������, ����������� ����������� ������������� ��������� �������� ���������� 0,08 (���� ������������� 12,5 ���). �� ������ �. ����������, ������������ ���� ����� �������, ����������� ����������� ������������� ��������� �������� ���������� 0,04 (���� ������������� &#8212; 25 ���).</P>
<P align=justify>�.�. ����������, ��������, � �������� ������������ ������������� ���������� ��������� ������� ���� ��������������� �������� �������, ������ 20 �����. �� ������� �.�. �������, ���� ������������� ���������� 10 ���. �.�. ������������� ���������� ������������ ����������� ����������� ��������������� ���������������� �����, �������� � ����� ������ � ������� ��� ��������: 0,03 &#8212; ��� ������� ���� ����� � 0,08 &#8212; ��� �������; ���� ������������� �������������� 33,3 � 12,5 ���. ��� ���� ��� ������� ����� �� �������� ��������, ������������ ��� ����� �����-������� ��� �� ������� �� �� ������������� � �������������������� ������������. ������� (��������) ���� ��������������� ��� ����� ������� �������� �����.</P>
<P align=justify>����������� ������� ��� ������-������������ ������������ �. ���������� ����������� ������� ��� �������� ���� ������� ���� ��������� ��������.</P>
<P align=justify>������ ������ ����������� ��� ������� ��������� ��������� �������������������� ������. �������� ������������� ������������ ��������� ���� ����� ��� ������������������ �����.</P>
<P align=justify>�<SUB>1</SUB>= �� -�<SUB>�</SUB>,</P>
<P align=justify>��� �<SUB>1</SUB> &#8212; ���� �����, ���.;</P>
<P align=justify>�� &#8212; ���������������� �����, ���./��;</P>
<P align=justify>�<SUB>�</SUB> &#8212; ���� �������������, ���.</P>
<P align=justify>������ ������ ������� ���� ����� �������� �� �����������, ���������� ������������� ������������ ������. ��� ������� ���� ����� ������ ���������������� ����� ������������ ������������ ���������������� ����� (��), ������������ �� �������:</P>
<P align=justify>�� = �<SUB>3</SUB>-(�<SUB>��</SUB>+�<SUB>��</SUB>+�<SUB>�</SUB> + �<SUB>��</SUB>)*�<SUB>�</SUB>,</P>
<P align=justify>��� �<SUB>3 </SUB>&#8212; ���� ������� �������� ��������, ���./�;</P>
<P align=justify>�<SUB>��</SUB> &#8212; ������� ���������� (��������, ���������, ������� � �.�.), ���./�;</P>
<P align=justify>�<SUB>��</SUB> &#8212; ������� ���������� (���������� � �������� � ��������������� ������������), ���./�;</P>
<P align=justify>� <SUB>�</SUB> &#8212; ������������������� �����, ���./�;</P>
<P align=justify>�<SUB>��</SUB> &#8212; ����� �� ������ ��������, ���./�;</P>
<P align=justify>�<SUB>�</SUB> &#8212; ����������� ����������, �/��.</P>
<P align=justify>������ ������ ����������� ��� ������� ���� ����� �� �������, ��������� �� ��������������������� �������, � �������� �� ������������� ������� ���������������� � ����������� ��������.</P>
<P align=justify>����� ���������� ��������� �������� ��������� ������ (������������) ������������� ������. ����� ������� � �������� �������� �������� ����������� ������������ ������������� � ������� ��������� �� ����� ������ � ������ ������ � ��������� ���� ������������ ��������� ��������.</P>
<P align=justify>����� ��������� ����� ������������� � ����� ������ ������������� �� �������:</P>
<P align=justify>�<SUB>0</SUB> = �:�,</P>
<P align=justify>��� �<SUB>�</SUB> &#8212; ����� ������ �������������;</P>
<P align=justify>� &#8212;�����;</P>
<P align=justify>� &#8212; ���������.</P>
<P align=justify>��������� ��� ������� ��������� �������� ������ ������������ ������� � ����������� �������, �� ����� ������ ������������� ������ ������������� �������� �������� ��������� ������ �� ��� ����� ����������, ������������ � ���������� �������.</P>
<P align=justify>������ ������������� ��������� �������� (��������� ���������� �<SUB>�</SUB>) &#8212; ��� ��������� ��������� ������ �� ������������ ����� � �������� ����� ��������� �����. ��� ������� �� ���� ������: ������ �������� � ������������ ����� ���������. ����� ������� ������� ��������, ��������� ���������� ����� ���������� �� ���������� ��������.</P>
<P align=justify>������ ������������� �� ����������� ������� �<SUB>�</SUB> ������������ ���������� �������� ����������� �� ������ ������� � ����� ��������� ����������� ������� � ��������� � ���� ����� ������� � ������� ��������� �������.</P>
<P align=justify>��������� ����� ��� ������� ������ ��������������������� ���������� ����� ����� ��������� ��������� ���������� ������� ������������ �� ��������� ��� �������������� ���������. �������������� ��������� ������� ������ ������ ������ ���� ������ �������� �������, ������� ����� ���� ������������ ��� ���������� �����������.</P>
<P align=justify>����������� ��������� �������� ������� ��� ��������� ��������� ����� ���� ���������� �� �������:</P>
<P align=justify></P>
<P align=justify>��� <I>�&#8212; </I>�������� �������, ���������������� ��� �����;</P>
<P align=justify><I>�<SUB>�</SUB> &#8212; </I>�������������� ��������� ������� ������;</P>
<P align=justify><I>� </I>&#8212; ����������� ��������� ��������� ������� ���������� �������� (���������� ���������� ������);</P>
<P align=justify><I>S &#8212; </I>��������� �������� ��������, ��������� � ����������� ������� ������;</P>
<P align=justify><EM>I </EM>&#8212; ������� �����, ��������� �� ������������ ������� ������;</P>
<P align=justify><I>V<SUB>�</SUB> </I>&#8212; ������������������ �������� ��������, ��������� � �������������� ������������ ������� ����������;</P>
<P align=justify><EM>F<SUB>�</SUB></EM> &#8212;������������������ �������� ���������, ������������� ����� ��������� � ������ ��������� ��������� ������� ���������� ��������;</P>
<P align=justify><I>r&nbsp;&#8212; </I>����������� ������� ���������� ������ �� ������� (���������� ���������� ������);</P>
<P align=justify><I>� &#8212; </I>���������� �������� ���������� ��������� � ������� ����;</P>
<P align=justify><I>w &#8212; </I>���������� ��� ������������;</P>
<P align=justify>� &#8212; ������� ������ �������� (���������� ���������� ������).</P>
<P align=justify>� �������������� ��������� ���������� ������� ������ ������ ��� �������� ��������� ��-�� ������� ���� ��������: ������� ����������� ������� ������� � ������� ����������� ������� &#8212; ���������������� �������, ��������������� �� ���������� �����������. ������� �������������� ��������� ������������ ������� �������� ����������� ������� ����������� ������� ������� � ����������� &#8212; ����������� ������� ����������� �������. ������� ��� ����������� �������� �������������� ��������� ������� ������� ������������ ������� �� ������� ������� (�.�. �������������� ��������� ��� ����� ������, ������������� ����������� ��������), ����� ���� ���������� ����� ������� ������ �� ������ ����������� �������.</P>
<P align=justify>������������� ����������� �������������� ��������� ��������� �������� ��������� � 3-� �������:</P>
<P align=justify>����� ��������� ������� �������� ������� �������������� �����������; �������� ������; ����������� � �������� �������� ������������.</P>
<P align=justify>���������� ��������� �������� � ������ ������� ������� ����������� ������� �������.</P>
<P align=justify></P>
<P align=justify><I>�������&nbsp;</I>1 (���. 1).</P>
<P align=justify></P>
<P align=justify>��� <I>�� - </I>�������������� ��������� ���������� �������;</P>
<P align=justify><I>�� </I>- �������� ��������� ���������� �������;</P>
<P align=justify><EM>r</EM> - �������������� ���������� �������� � ����������� �������;</P>
<P align=justify><I>t<SUB>�&nbsp;</SUB></I>&nbsp;- �������� ����� ���������� ���������� ������� (���);</P>
<P align=justify><I>t </I>- �������� (�������������, �����������) ����� ������� ������� (���); ��������������, ��� <I>t</I><I><SUB><I>� </I></SUB>&gt; <I>t </I>;</I></P>
<P align=justify><I>� - </I>������ �������, � �������� ��������� ������ ���������� (���);</P>
<P align=justify><I>�� - </I>����������� �����������, ����������� ������� ������������ ������ �� ���� �� �������������� ��������� �������:</P>
<P align=justify></P>
<P align=justify>����������� ������ ������������ ������ ����������, �� ������� ��������� ��������� ����� ������ ��� ��������� ��� ���� �� 1 %.</P>
<P align=justify>������������ ������ �� ���� � ����� ������������ �� �������:</P>
<P align=justify></P>
<P align=justify>��� <FONT face="Times New Roman">&#916;</FONT><I>Q<SUB>d</SUB> </I>- ��������� ������: &#916;<I>�&nbsp;- </I>��������� ����; <I>Q<SUB>d</SUB> -</I>����� � ����� <I>d; P - </I>����.</P>
<P align=justify></P>
<P align=justify><I>������� 2 </I>(���. 2).</P>
<P align=justify></P>
<P align=justify>���&nbsp;<I>Cpm </I>- ������������� �������� ��������� ���������� �������;</P>
<P align=justify><I>��� &#8212; </I>������������ �������� ��������� ���������� �������;</P>
<P align=justify><I>��� </I>= <I>��� </I>��� <I>t </I>&gt; <I>1.</I></P>
<P align=justify>����� ��������, ��� �������� �������������� ���������, ��������� �� ���� ���������, ��������� ���� ��� <I>t = t<SUB>�</SUB>; </I>��� ��������� ��������� <I>t &lt;t<SUB>�</SUB>&nbsp;</I> <I>������� 1 </I>������ ����� ������ ����� ������� (�� ��������� � <I>��������� 2) </I>������ �������������� ���������. �������� �������������� �������� � ���� ������� ����������� �������� ��������, ���������� � ������ �� ���: ������� �� ��� <I>t = </I>0 �������� �������������� ���������, ������ ���� ��� �� �������.</P>
<P align=justify>���� �� �������, �� ����� ��������� <EM>������� 1</EM>, ���� ������� &#8212; <I>������� 2. </I>����� ����, ���� ��� <I>t </I>= 0 �� ������� �<SUB><EM>�</EM></SUB> = 0, �� �������� ��� <I>������� 3, </I>���������� ������ ���� ������������ ����� <I>1 � 2 ����������.</I></P>
<P align=justify><I>������� 3.</I></P>
<P align=justify></P>
<P align=justify>����������� ������ ��������� �������� �������� ������������� �� ���������� <I>t, </I>������������� ������� ������� ���������� <I>t<SUB>�.</SUB> </I>�� ����� �������� ��������� ���������� ��� ������ ���� ��������� ������ �������������� ���������, ��������� �� ��� ����������� �������� �� ���������������, � ��������������. ������� �� ����� ���������� ������� ������������ ����� ������� ������ ������������� ���� ��������� �������� ������������� � ����� ��� ����� �������� �� ������ �������� �������������.</P>
<P align=justify>����� �������, ������ ��������� �������� ��������������������� ���������� ������ ������������� ������ �� �� ������������, �������� ������������ ������������� � ������ ������������ ��������������������� ������������� ����������.</P>
<P align=justify>��� ���������� ������ � �������� ������� ���������� ������� ��������������������� ���������� ���������� �����, ������������� �� ����� �������������, ������ �� ������� �������� ��������������� ������. ��������� ������, ��� �������, ������������ � ������������ � ������������ ���������� �������.</P>
<P align=justify>�� ����� ������������� ����� ������������ ���������� ������� ����� ��������������:</P>
<P align=justify>- �� �������������������� ������ (�����, ��������, ��������, ������ � ����������� ����������);</P>
<P align=justify>- ����� ��� ����������� ��������������������� ���������� (������� � ������������� ���������, ��������-������������, �������-�������������� ���������, �����������-��������������������� �����������, �����������-��������������� ���������, �����������, � ����� ����������� �� ������������ � ����������� �������������������� ���������);</P>
<P align=justify>- �����, ������� �������������������� ��������, ��������������, ��������-������������� ���������������, ��������������� ��� ����������� ������ ������ �� ����������� ��������������� ���������, ������������� � ����������� �������, ��������� �������.</P>
<P align=justify>�������� ��������� ���������� ������� ��������������������� ���������� ������������ �� ��������� ��������� ��� �������������������� ������ � ������ ��� ����������� ��������������������� ���������� � � ������ ������������ ������������� ������ ������ ���������� �������.</P>
<P align=justify>����� ���������� �������, ������� ����������� ��������������������� ����������, ����������� �������� ������ ����������� ��� ��������������� ��� ��������� ��������� ��������.</P>
<P align=justify>����� ���������� �������, ������������ � �������� �������������������� ������, � ��������� �������, ������� ����� ���� ������������ ��� ������������ ������������������� ������������ (��������, ������� ���������), �����������, ��� �������, ������� ��������� ������ ��� ������� ������������� ��������� �����.</P>
<P align=justify>����� ���������� �������, ������� �������������������� ��������, ��������������; ���������� ���������, ������� �� ����� ���� ������������ ��� ������������ ������������������� ������������; ��������-������������� ���������������, ��������������� ��� ����������� ������ ������ �� ����������� ��������������� ���������, ������������� � ����������� �������, �� �������� ��������������� ������. ��� ���� �������������� ���� ������ ���������� ������� ����������� ��� ������ ��������� ���������� ������� � �����.</P>
<P align=justify>����������� ��������� ����� ������ ��� ��� ����� ��������������� � ������������� ������������ �������������� ��������� ������������� ���������� �������, ��� � �������� ����� ���������� �� ��������� ������������� ������������� ��������� ��������. ������ � ��� ������ � ������� ��������� �������� �� ���� ���������� �� �� ����� ���� �������������. ���� ��������� �������� ���������� ����� ������������ � ���� �����. � ����� �������, ��������������, ������� �������������������� ������������, ����� ���������� �������� ��������� ������ � ��������� ����� ��� ��������� �������� ���. � ������ �������, ������������� ������� ����� ��������� �������� ��������� ��� ���������������������������.</P>
<P align=justify>��� ���������� �������� ������ ����� ��������� ����� �������, �������������� �� ������ ����������� �������� ��������� ����������, �� � �������� ��������������� ��������� � ��������� � ���������� ���������� ������������ �������� � ��������� ��������.</P>
<P align=justify>���������� ��������� ���� ������������� �� �����, �������� ��������� ���������������� ��������� � �������� �� ��������� ���� �������� ��������� ��������� &#8212; ����������� ����� � �������� ���������������� ������ �, ��� ���������, &#8212; ���������� �� � �������� ������ ����� ���������� ������������ ��� �����.</P>
<P align=justify>� ����� � ���� � �������� ��������� ������� ��� ����������� ��������� ��������� ���������� ������� ����� ������������� ������������ ����������� ������ ������������ � �������� ���������.</P>
<P align=justify>�����, ����� ������ ����� ������� ������������� ���������� ������������� ��������� ��������� � �������� �������������� ��������� �����.</P>
<P ali
gn=justify>����� �������� �������, ��� ������ ���� �������, ��� �������, ���� ��������: ��������������� (�����) � ������������ (��������������).</P>
<P align=justify>� ������������ ����� ���� �� ���� �� �������� �������� �� ������������ ������������� � ������ ����������� ����� ����������� �����, ������ �������������� ��������������������� ������������ � ���������� ������� � �������� �� �������, � ����� � ��� ���������� ����������� ������������� ��������� � ���������. ����� ���������� ����� �� ��������� �������� ������� ��������� ��������, � ����� ���������� &#8212; ������������������ ��������� �����.</P>
<P align=justify>������� �� ������ ����� �������� �������� ������� ��������� ����������� ����������� ������������ ��������� ���������� ������������, ��� ��� ��������� ������������ �������� � ��������� ������ �����, �������� ��������, ������������ ������������� ��������������. ����������������� ����� � ���������� ��������� �������� ����� �������������� ������������ ������������� ����������������, ����� ����������� �������� �������� �������� ������������������ ������������� ���������� � ������������ ��������� ����� �������; ����� ����� ����� ������� ��������� �������� ������ ���������� �������; ��������� ������������ ���� ����������� ������������� ��������� ���������� ���������� �������� �� �������������, �������������, ���������� � ����������� �������������� ������������.</P>


</body>
</html>

