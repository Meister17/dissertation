<!--
ID:38540399
TITLE:������� ���������� �����
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:23-24
ISSUE_DATE:2007-12-15
RECORD_DATE:2007-12-12


-->
<html>
<head>
<title>
38540399-������� ���������� �����
</title>
</head>
<body >

<B>
<P align=justify><FONT size=+0>15.11.2007</FONT></P></B>
<P align=justify><FONT size=+0>����� ���������� �����-����������� ����� ������� ���������� ��������� �������� �� 7 ���� ���., ����� ���������� 19,950 ��� �������������� ������������ �����.</FONT></P>
<P align=justify><FONT size=+0>�� 1 ������ 2008 �. ����� ���������� ����� ���������� ������ � ������ ������ �������� ����������, � �������� ��� �������� ����� ������� ������ � ���������� �������������� �����.</FONT></P>
<P align=justify><FONT size=+0>����� �������� ���������� ��������� ����� � ������ 3 ���� ���. (3 ��� ��., ����������� ��������� 1 ���. ���. ������), ������ ��������� 5 ��� � ������� �������.</FONT></P>
<P align=justify><FONT size=+0>��������: �����-���������� ����.</FONT></P><B>
<P align=justify><FONT size=+0>19.11.2007</FONT></P></B>
<P align=justify><FONT size=+0>���� "���������� �������" �������� ����������� �������.</FONT></P>
<P align=justify><FONT size=+0>���������� ��� ����� ������ ������� ������� � ��������������� ����������� ��������� ������������ �������� ����� "���������� �������".</FONT></P>
<P align=justify><FONT size=+0>�� ���� �������� �������������� ������� ������������ �����������, ������� ����� �������� �� ����� 141 ��� ���. ����� �������, �� ��������� �� 19.11.2007 �. ����������� ������� ����� �������� 2,1 ���� ���., �������� ������� &#8211; 999 ��� ���.</FONT></P>
<P align=justify><FONT size=+0>�� ������ ����������� ������������ ����������� "���������� �������" ������� ������, ���������� ������������ �������� ��������������� � ���������� �����, �������� �������� �� �������� ������������ ����� ��������� � ���������� �����������. ��� ����� ��� ����������� ��������� ����� ����� ������� � ���������� ��������� ��� ����������� ��������.</FONT></P>
<P align=justify><FONT size=+0>��������: ���� "���������� �������".</FONT></P><B>
<P align=justify><FONT size=+0>20.11.2007</FONT></P></B>
<P align=justify><FONT size=+0>��������� ��������� �������� �������������� �� ��������� �� 15.11.2007 �. �������� 1,118 ���� ����. ���.</FONT></P>
<P align=justify><FONT size=+0>�� ������ 2007 �. ����� �������� ��������� 469 ��� ����. ���, ����� �������, �� 10 ������� ���� �������� �������� 128%.</FONT></P>
<P align=justify><FONT size=+0>��������� ���������� �������� ����� �������� ��������������� �������, ��������� � �����������, ��������� ��������-�������, ��������� �����. �� ��������� � 2006 �. ��������� �������� �� ���������� ��������� ���������, � ��������� ����� �������� ���� �������� ���������� ��������������� � �����������. ���������� �������� ����� � ������ 2007 �. ����������� � ��������� ������������ � ������������ �� ���������� ������.</FONT></P>
<P align=justify><FONT size=+0>��������: �������������.</FONT></P><B>
<P align=justify><FONT size=+0>21.11.2007</FONT></P></B>
<P align=justify><FONT size=+0>�� ������ ������� 2007 �., ��������� �������� �����-����� ����� �� 9,3% &#8211; �� 118,4 ���� ���. ��������� �������� ����������� ����� ���������� �� 9,1% &#8211; �� 112,3 ���� ���. ��� ����� �������� ������� ����� ������� ���������� ����� &#8211; �� ������� �� ����� ���������� �� 13,9% � �������� 6,1 ���� ���.</FONT></P>
<P align=justify><FONT size=+0>�� ������ ��������� 2007 �. �����-���� ���������� ��������� ���������� ����� ���������� ��������. ���, � ���� �� ������� ����� ��������, �������� ����������� �����, ����� �� 14,3%, ���������� &#8211; �� 65,9%."�������� ����� ������� ���������������� �������� ����� � ������� ������������, ������� ���������� �� �������� ����� ��������� ��������� � �� ��������� ������� �� ������������ ����������", &#8211; �������� ������ ����-��������� �����-����� ����� ��������.</FONT></P>
<P align=justify><FONT size=+0>� ���������, ���� ������ �����-���� ��������� �������� ������������� ���������, ��������������� ��� ����������� �������� � ����� ��������, � ��� ����� ������������� ����������, � ����� "��������-������ ��� �������", ������� ��������� ������������� �������� �������� �� 75 ��� ���. ��� ����������� ����� ������������ ��������� ������ &#8211; ����� 2 ������� ��� � ������� �������������� ������ ����������.</FONT></P>
<P align=justify><FONT size=+0>������ � ���� ���� ������� ��������, ��������������� ������������� ��� �������������� ������ � �������� �������, &#8211; ������� �� ������� ������������ � ������������; �� ���� ����������� �������� ��� ����������� ��� ���� ������� ������.</FONT></P>
<P align=justify><FONT size=+0>��������: �����-����.</FONT></P><B>
<P align=justify><FONT size=+0>23.11.2007</FONT></P></B>
<P align=justify><FONT size=+0>�����-���������� ���� �������� ����������, ������������ �� ��������� ������ � �������� ������� ����.</FONT></P>
<P align=justify><FONT size=+0>� ������ ���������� II ��������� ������ ���������������� ���������� ���������� ���������� � �������������� ����� ������ ��������� ������������������� ���� � �����-���������� ������.</FONT></P>
<P align=justify><FONT size=+0>���� ���������� &#8211; ���������� ������� ��������� ������ � �������� ������� � �����-���������� ���������� ������ &#8211; ���� � ��������� ��������.</FONT></P>
<P align=justify><FONT size=+0>��� ������� ��������� ����� ������� ��������, ����������� �� ���������� ���� ��� ������� ������������ � �����������������, ��������� ���������, � ��� ����� � ��������� ��������. �� ����������� ���� ����� ������, ��������������� ������������ ������ � �������� �������, �������� 1,5 ���� ���.</FONT></P>
<P align=justify><FONT size=+0>������ ������������ ����� ����� ����������� �� ���������� ���������� ���������� ��-�� ���������� � ��� ��������� ������� ���� ������������� ����� �����������, ������� ���������� �� �����.</FONT></P>
<P align=justify><FONT size=+0>"����������� ���������� ��������������� ������� ���� �������: ���� ��������� ������������������� ���� ������ ����� ��������� ����������� � ������������� ����� �� �������, ����������� ��������� ������ � �������� �������", &#8211; ������� ������� ��������.</FONT></P>
<P align=justify><FONT size=+0>��� ���� ������������� ������ �������������� �����-����������� ����� � ����� ��������� ������������������� ���� ����������� � ������� � ����� �������� ������������� ���� ���������������� �� ���������� ������, ��� ��� ������ ��������� ����������� ���� ������ � ���������� ��������.</FONT></P>
<P align=justify><FONT size=+0>��������: �����-���������� ����.</FONT></P><B>
<P align=justify><FONT size=+0>23.11.2007</FONT></P></B>
<P align=justify><FONT size=+0>���������������� � NationalCity Bank (Cleveland, USA) ��������� ���������� � �������������� ����������������� ������������� ����� � ������� 15 ��� ����. ��� �������� OPIC &#8211; Overseas Private Investment Corporation (��������������� ���������� ���������� ������� ����������, ���).</FONT></P>
<P align=justify><FONT size=+0>���������������� ��������� ������������ ������������ �������� ��� ���������� �������� ������������ ����������� ������ � �������� �������.</FONT></P>
<P align=justify><FONT size=+0>OPIC &#8211; ��������������� ����������, ����������� �������������� ��� � 1971 �., ������������� �������� ������� � ���������� �������, ���������� � ������� ������������ ������������, � �������������� ���������� � ���������� ����������� �� ������������� ������. ������ OPIC �������� �������������� �������������� �������� � ������������ ������������ ����������� ��������� ����� ������������� � ����������� �������������.</FONT></P>
<P align=justify><FONT size=+0>������ ������ (������������� � ������ ������������� ������� �� ������� ���������� ������, ���������� ���������� � ��������� ������� ���) �������� �������������� �������� ������ �������, ������� ���������� ���������������� � ���������� ����������.</FONT></P>
<P align=justify><FONT size=+0>��������: ����������������.</FONT></P><B>
<P align=justify><FONT size=+0>23.11.2007</FONT></P></B>
<P align=justify><FONT size=+0>������������ �������� ���������� (��� ���) &#8211; �������������� ����������� ���� ������ ������� ����� ���������������� ������������ ���������� �������������� ������ � ��������-������ ��� �� ������ ��������� 2007 �.</FONT></P>
<P align=justify><FONT size=+0>������� ���������������� ���������� ��� ������������ � ��������� ����������: MAX (��������� ����� �������� �� ����� ������ �����), ��� (����� �������� � ������� �������� �� �������������� �����), OTC (����� �������� � ������� �������� �� ����������� �����), GMS (����� �������� � �����������), CRS (����� �������� � �������������� ������� � �����������), BRK (���������� ��������). ��� ���������� ��������� ����������� ���������� ������������ 220 ���������� ��������� ����������� � �������������� �������� &#8211; ������ ��� ���.</FONT></P>
<P align=justify><FONT size=+0>� �������� ���-��������� ���������� ����������� ����� ��������� �������: ��� TOP-30 MAX (��������� ����� �������� �� ����� ������ �����) &#8211; 22-� �����; ��� TOP-30 OTC (����� �������� � ������� �������� �� ����������� �����) &#8211; 7-� �����; ��� TOP-20 CRS (����� �������� � �������������� ������� � �����������) &#8211; 12-� �����.</FONT></P>
<P align=justify><FONT size=+0>��������: �����������.</FONT></P><B>
<P align=justify><FONT size=+0>26.11.2007</FONT></P></B>
<P align=justify><FONT size=+0>�� ��������� �� 1 ������ 2007 �., ����� �������, ����������� ��������� �� ������� � ��������� ������ � ��������� �������� �����, �������� 8, 805 ���� ���., ��� �� 5,633 ���� ���. ��� ����� � 3 ���� ��������� ����������� ���������� ����� �����.</FONT></P>
<P align=justify><FONT size=+0>���������� ���� ������ ������� �������� � ��� "���" ������������ ������������ ��������� �����, ������������ �� ������������ ���������� ������������ ����������� ������ � �������� �������. ��� "���" �������� 72-� ����� � �������� ������.�� "100 ���������� ������ �� �������� �� ��������� ������" � 87-� ����� � �������� ��������� "�������������������" "Top1000 ������ �� ��������� ����� �� ������ ��������� 2007 ����". ������ ������� ��� "���" �� 01.11.2007 �. ��������� 17519535 ���. ���., ����������� �������� ����� (�������) &#8211; 1006020 ���. ���.</FONT></P>
<P align=justify><FONT size=+0>��������: ��������� �������� ����.</FONT></P><B>
<P align=justify><FONT size=+0>26.11.2007</FONT></P></B>
<P align=justify><FONT size=+0>�������� ������� ��������������� �� ����� 2007 �. ����� �������� ��<BR>5,7 ���� ���.</FONT></P>
<P align=justify><FONT size=+0>��������� ���������� ��������� �������� ����� �������� ����������� ����� �� 23 ������ 2007 &#8470; 267-�� "� �������� ��������� � ����������� ����� "� ����������� ������� �� 2007 ���", � ������� ������������� ���������� ��������� �������� ��� "��������������" �� 5,7 ���� ���.</FONT></P>
<P align=justify><FONT size=+0>�������� ��� ���������� ��������� �������� ����� ����� ������������ ��� ���������� ������� ������������ ������������ (����������) ��������, ������ ��������� ��������, �������������������� ������������, � ����� ������������ ������� ��������-���������� ������������.</FONT></P>
<P align=justify><FONT size=+0>� ������� ��������� ���� ��� � ������ ���������� ������������� ������������� ������� "�������� ���" ����������� ������� ������ �������� �������� ����� ���� �������������� � ����� ���, ��������� ����� ���������� � ��������� ����� ���������� ����� �������� ���� ���������������� � ������. ����� ����, ����� �� �������� ������������� ����������� ��������� �������� ����� ��������-��������� ������������ ��� ����� ������ ��������������������� ����������.</FONT></P>
<P align=justify><FONT size=+0>�������������� � 1 ������ �� 26 ������ 2007 �. ����� �������� ���������, ������� ������ ��������� ���������, &#8211; 20,7 ���� ���., ������������ (����������) ���������� &#8211; 5,7 ���� ���., �������������������� ��������������� ������������ &#8211;<BR>3,1 ���� ���. � ������ ���� ���� ����� ����������� ��������-��������� �������� �� ����� ����� 3 ���� ���.</FONT></P>
<P align=justify><FONT size=+0>� ����� �� ������ �� 1 ������ 2007 �., ��������� �������� ��������������� ��������� ������� ��������� �������� 265,2 ���� ���. �����������, ��� � 2008 �. ����� ���������� �������� �������� �� 380 ���� ���.</FONT></P>
<P align=justify><FONT size=+0>� ��������� ����� �������� ���������������������� ���������� 21 ���� ���.</FONT></P>
<P align=justify><FONT size=+0>��������: ���c�������������.</FONT></P><B>
<P align=justify><FONT size=+0>27.11.2007</FONT></P></B>
<P align=justify><FONT size=+0>���� "������� �������� ������" ����� ������� ����������� ���� MasterCard.</FONT></P>
<P align=justify><FONT size=+0>����� MasterCard Maestro, Standard � Gold ������������ �������� ������ � �������� ���������� ��������� BSGV, �������������� � ������������ �� ������ ����� � ��������� ��������.</FONT></P>
<P align=justify><FONT size=+0>����� ��������� ������� ���������� ���� MasterCard ����� ����� ���� ��� ��������� BSGV �� ���� ���������� ���������� � ������������ �������� ������ ��������� ����� ��������� ������� MasterCard Worldwide. �� ������������ ��� BSGV ���� ������ ��������������� ����� ������ ��������� ������� Visa. �� 1 ������ 2007 �. ������ �������� ����� 120000 ���� Visa Electron, Classic � Gold.</FONT></P>
<P align=justify><FONT size=+0>�� ������ ���������� ������������ ���������� � �������� ���������� �� ������ � �������� ��������� ������� �����, ������ ������ � MasterCard �������� ��� BSGV �� ������� ��������� �����, ������� ��������� ������� ������������ �������� � ����� ���������� ���������� ����������� ���� ����� ������� �����. "�� ����� ���������, ������������ ���� MasterCard ����� �� ����, ��� ���� Visa, ��������� �������� ���������� ���������� �������� � ��������� �����, ������� ���� ������� ������� ������ � ������� Visa � MasterCard", &#8211; ������� ������ �����.</FONT></P>
<P align=justify><FONT size=+0>BSGV �������� 100%-��� �������� ���������� ����������� ���������� ������ "������� ��������", ������� � ������� ����������� ���� ������������ ����� ���������� ��������� ����������� ��������, ��� ����� ����, ����������� ������ � ������������ ����� ��������� � �������� �.�������.</FONT></P>
<P align=justify><FONT size=+0>BSGV ���������� ����������� ������ � ������� ���������� ���������, ������� �������� ����� ������������ ������ ���������� � ��� ������: "��������", "���������" � "�������". ������ � ������� "��������" ������� ����� �������� ����� MasterCard Maestro, ����� "���������" �������� � ���� ����� MasterCard Standard, ����� "�������" &#8211; ����� MasterCard Gold. ��� ���� ���� ����� ����������� ������ ��<BR>2 ���� � ����������� ����������� �� ����� ����. ������ � ����� ����� ���� ������ ��������� �������� ������ � ������� ��������-������� BSGV Direct, ����������� �������������� ��������� ����� � ��������� ���������� ������ ����� � ������ ��������� ������� ����� ��������.</FONT></P>
<P align=justify><FONT size=+0>� ��������� ����� � ���������� � ��������� ������ MasterCard ������ ����� ���������� ��������� � ������������ ����� ���� �� ��������� �������.</FONT></P>
<P align=justify><FONT size=+0>��������: ���� "������� �������� ������".</FONT></P><B>
<P align=justify><FONT size=+0>27.11.2007</FONT></P></B>
<P align=justify><FONT size=+0>�������������� ���� ����� �������������� ����������� � ������� ��������-�������� Raiffeisen Connect. ������� �������� � 26 ������ 2007 �. ��������� �������������, ������� �����, ������������ ������� RC, ������ �������� � ��� ����� ���������.</FONT></P>
<P align=justify><FONT size=+0>� ����� �������, ����������� ������� ������ � ���������� ����������� ����� � �����. ��� ����� ���������� �������� ����������� ���� �� ������� "������� �����" � ������� �� ������ ����� "��������� �����". ���������� � ����������� ����� ��������� � ����� ���� � �� ����� ����� ����� �����������, ����� �� ��������������� ������.</FONT></P>
<P align=justify><FONT size=+0>� ������� ����� ��������� ������������ ����� ������������ ���������, ����� ���� ������� ������� ������ ����������� ��������� �� ���� "������ ������". ��������������� �� �����, ������ ������ ������ � ������� � ����� ������ � ������������ ��������. ��� ������� �������� ��� �������� ���������, �������� � ������, ��� ������� ���� � ��� �������� ��������� �� ������ � �� �����.</FONT></P>
<P align=justify><FONT size=+0>��� ������������� �������� � ������, ������������ �� ������ �����, ������� ������ ����� ������ ������, �� ������� ���������� ����������� � ����� � ��������. ��� ����� ������� ������� ��������������� �������� ������������� ������� ����
���� ���������. ����� �������� ����� ������� ��� ����� �������� �����, � ������� ������ ������ ������������ � ������� "������� ������������", � ������ "������".</FONT></P>
<P align=justify><FONT size=+0>����� ��������, ����������� ��������� ������� � ������� Raiffeisen Connect, ����������: "������� ������� ������������ �Raiffeisen Connect, �� ��� ����� ��������� ������� ���� ��� ����� ����� ��������� ��� ������������ � ������������� ��������. ������� �������� ����������, ��� ������������� ���������� ������������ �������� � ������ ������������ ��������, ���������� ���� ���� �� ����� ���������� �����. ��, ��� ��������� ����, ������� ���� �������� ���������� � ���� ����� ������������".</FONT></P>
<P align=justify><FONT size=+0>��������: �������������� �������.</FONT></P><B>
<P align=justify><FONT size=+0>29.11.2007</FONT></P></B>
<P align=justify><FONT size=+0>��� "���� ���� ����" �������� ����������� ���������� � �������������� � ��������������� ����� ������������ � ������� ������� ��������� ����� &#8211; ����������� ������������ � ����������� ����������.</FONT></P>
<P align=justify><FONT size=+0>� ��������� ����� �������������� ���� ����� �������� ����� ��������, ��� "�����", "����-������������", ��������� �������� "����", ����������� �������� "����������", "���-������������", ����������� ���������� �������� "��������", ��������� ����� "����������� ���� �. ���������", ��������� ����� "�������", ��������� ������������ "������", ��������� ������ "����������", ��������� �������� "��� ���", ��������� ������������ "���������", ��������� ������ "����� ����", ��������-����������� �������� "������ ����", ��������-���������� ����������� "����� �����", ��������� ������ "�������", ��������� ������������ "�������� ���", ��������� ������������ "������", "��������� ��������� ��������", ��������� "������� ������������", ���������� ������ "������� ����" (������ ����), ���������� ���� Solhouse, ��������� ������������ "�������", ��������� ������������ "������", ��������� ���������� "���� ������", ��������� ������������ "���������� ��������� ����� ������������", ��������� ������������ ������ ���� "�������� ��������", ��������� ��������� "������", ����������� �������� "������ � ��������".</FONT></P>
<P align=justify><FONT size=+0>������ � � ������ ���� ��������� �������������� �������� �������� ��� �������� ���� ���� �����, ��������������� � ������ ��������� ���������� ������������:</FONT></P>
<P align=justify><FONT size=+0>- ��������� ������ ��� ������� ������� �� ��������� �����;</FONT></P>
<P align=justify><FONT size=+0>- ���������������� ���������� �������, ��������� ������ ������;</FONT></P>
<P align=justify><FONT size=+0>- ������ ��� ����� ����� ������������ �� ����� ����.</FONT></P>
<P align=justify><FONT size=+0>��� �������� �������� ������ � ���������� ����� ����������� � ���������� �������� "������ ����", � ������ � ���, ��� ������ �������� ������ �������� ����� �� ������� ������� �� ������ ������������ �� ��� �� �������� � � �� �� ����������� �������� �����, ��� � ��������������� � �����:</FONT></P>
<P align=justify><FONT size=+0>��������������� ������� ����� ������ �������� � ������� 60 ����� �� ��������� ������ ��������. ��������� ������� � �������������� ������� &#8211; � �������<BR>48 ����� � ������� �������� �������� ����� ������� ������ ����������� ����������; ������� ��������� ���������� �������� ����� ��, ��� � ���� ���� �����; ������������� � �������� ��������� � ������ ���������� ������� �� ����� ���������� ��� ����� �� ������ �� ������; ������ ������������.</FONT></P>
<P align=justify><FONT size=+0>��������� ������ ���� ���� ����� �������� � ������ � �������� ��� �� ���� �� ���� �� 30 ���. ���� �� �������� ����������� ��������� �������� ���� ���� ����� ������� � ���, ��� ������� ������ �� ������� �� ������� �� ����� ������������, ��� ��������, ��� ������� ����� ����� ����� ��������� ������ ��� ���������� ������������ ������ �� 1 ��� ��� �� 30 ���.</FONT></P>
<P align=justify><FONT size=+0>��������: ���� ���� ����.</FONT></P><B>
<P align=justify><FONT size=+0>29.11.2007</FONT></P></B>
<P align=justify><FONT size=+0>� 1 ������� �� ���� ���������� ���� "����� ������" ������� ��� 24 ������ ������������ �������� �������� � ���� ��������� ��������.</FONT></P>
<P align=justify><FONT size=+0>�������� ��������, ������������ ����� ������ ������ � ������ ��� 24, ��� �������� �����, �������������� �������� �������� � ���� ��������� �������, ��������������� �������� �����. ������� ����� 10 ��� ������� ���������� �������� ���������� ������� ����� �������� ���������. ��������� ��������� �������� ����������� ������� (��������� �� ������ �������� ����������� ����� �����) � �������� ���������� ��������� ������������ ������ ������� �������� ����� ��������� �����, ��� ������ ������ �������� ���������� � ���������.</FONT></P>
<P align=justify><FONT size=+0>����� ������� ��� 24 ����� �������� ������� � ��������� ����� ����� ��������� ��� 24 � �������� ������ �������� (cash-in), � ������������ ������ �����, ����� �������� �� ��������� ������, � ����� ����� ���� CONTACT.</FONT></P>
<P align=justify><FONT size=+0>��������: ��� 24.</FONT></P><B></B>

</body>
</html>

