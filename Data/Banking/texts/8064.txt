<!--
ID:38562239
TITLE:������ - ���������� ������� �����
SOURCE_ID:10908
SOURCE_NAME:������������ ���������� ������
AUTHOR:
NUMBER:12
ISSUE_DATE:2007-12-31
RECORD_DATE:2007-12-18


-->
<html>
<head>
<title>
38562239-������ - ���������� ������� �����
</title>
</head>
<body >


<DIV class=article>
<DIV class=header>
<H2><FONT size=3>���������� ������ ������� �������� ����������� �������, � ���� ����������� �������� �� "��� ������"</FONT></H2></DIV>
<P align=justify><EM>������ ����: ������� ������</EM></P>
<DIV class=body>
<P align=justify><B>� ������ ���� ��������� ������������� ������ ���������� ���� �� ��� � ���������� ������. ������ ����� ����� ������������ �� �������� �������� � ����� ��������� ��������� �������. � ��� �� �������, ��� ��� ���������� ���������� ������.</B></P>
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<DIV class=vrez2>
<H3><FONT size=3><FONT size=3>����� </FONT></FONT></H3>
<P align=justify><B>������ ����������</B> ������� � 1971 ���� � ������. ����������� - ���������� ��������������� ����������� (�/������), ����������� INSEAD (���). � �����-������� � 1990 ����.</P>
<P align=justify>� 1999 �� 2002 ��� - ������� �������� ������ &#171;���������&#187;. � 2003 �� 2005 ��� - ������ � Axel Springer Russia, �������� ������� ������ �������� Newsweek � Forbes, �� ���-����� (������ &#171;������&#187;), ������������ �������� �� &#171;����������� �����&#187;. � 2005 �� 2006 ��� - ������� �������� ������� Smart Money. � ������ 2007 ���� ������� �� ������ � �������� &#171;��� ������&#187;.</P></DIV></BLOCKQUOTE>
<P align=justify>- ���� ��������� ����������� �������� � �������� �������� &#171;��� ������&#187; ��������� ���������. �������� ��� ������� � SmartMoney ��� ���� ����������� ������������, �� ��������� �����������. � ����������, ��������� ��� ��� ����� ����� � ���� ��������. &#171;��� ������&#187; ����� ���������� ��� ������ &#171;���������&#187; � �������� �������� �������� ���������� �����. ����� �������� �����, ��������� �������� �����, ����� ��������. ������, ��� ���� ��� ���� ��������. ���� ����� ����� �� ���� ������������ �������. �� ��������� ����������� ����������� ��� ��������� ��� ����. � �� ����� ����� �����, ������ ��� � ��������, ������� �������� � ��������� �������� ����������, �� ����� ��������� ����������� ����������� ��, ��� ��� �� �����. ��� ���� ��������� ����������, ��� ��� �������� �������, ����������� ������� �����-�� ���� ������ ������.</P>
<P align=justify><B>���: ����������?</B></P>
<P align=justify><B>�. ����������:</B> � ���� �������� ����� ��� ��������� �������, ��� � ���������� ���� ����� ���������. ��������� ����������� ������� �� ��� ������. ������ �� - ����� ����� ������. �� �������� �� ������ ��������, ������� �� ������ ������� �� ������ �����. �����, ��������, ���������� �����������. ���� ����������� ���� �������, ����� ������ �������� 48 ������� � �� ������ �� ��� ������ ���� ���������� ��� �������, �� �����-������� ������ ��� ������. ���� ��������� ��������� ��������� ��������� �� ����� ����������� ���������� ������������, �� ��, ����� �� ���������, ����������� ����� �����, ������� ���� �� �������� � ������������� �� ����������� ����������. �� ��� ������� ����� ����� ������ ��� �������. ��� ��� ��������� ������� ������������ � ������� ���������, ��� � ���� ������� ��������� ������ �� �������� �������. ��� ���� ������� �� ����� �����, �� ����� ���������� � ������ ������ �������. �������, ��� �������� � ���� ����� ���� �� ������������ �������.</P>
<P align=justify><B>���: ������� ������ ������ �������?</B></P>
<P align=justify><B>�. ����������: </B>����� ������ ����������� ���, ��� �� ������ ������ ���������. ������ � ����. ��� ��������� ������ ���� �������������� IT�������, ������ ��� ������������ ���������� �������, ������� �������� � ����������� ��������� ������, ��� ���� �� ��������.</P>
<P align=justify>���� �� � ����� ����� � ��� �� �������� ������� �����-������� ���������.</P>
<P align=justify>�� ���� ������������ ������ ����������, ��� ���� ������ ��������� � ������� �������������, ������� ������� �������� ����� ��������� IT-��������������, ����� ���� ������ ���������� ����� ����������� ������ ����������� �� ����� ����������.</P>
<P align=justify>��� ��� � ��� ����� �������, ������� ������� ������ �� ���, �� ��������� ������ ��� ���������. �� ����� ����, ��������, �������� � ���� �������� ������������ ������ ������������������ ���-����� ����������� ��� ������ ����, ������� ����������� � ���� �������� � ������ � ��������� � ���������� �� ������������ ������� � ����������� ������.</P>
<P align=justify><B>���: ���������, ��� �� ����� �����?</B></P>
<P align=justify><B>�. ����������: </B>��� ������������� ������. ��-������, ����� ����� ����� �� ������� ������, ��� ���� ������������. � ���������, ������ ���������� ��������� 500 ��. ������, � ��� ����� ���� ��������-����, ��� �����, ���� �� ��������, ������ ����. ��� �������� ���������� ������� ��� ����� �������� �������� �����, ����� ����������� ������. ��� ����� ����� ��������������� ����������� ���������� ����, ������� ��� ������� �������� ��� ������ ���� ����, � ��� ������ ������������ ������. ��� �������������� �������, ������� ������� ������� ��������, ����� �� ���������� � ��� ������. � ����� ����� ������� ��-������� ����� �������� ���������� �����������: �������� ����� ���������, ��-������� ����� ��������� ������� ������������.</P>
<P align=justify><B>���: �� ��������, ������ - ������� ����? �� ������� ��� ������������ ������ ��������� ������?</B></P>
<P align=justify><B>�. ����������: </B>������� ����� ������ ������� ��������� ��� � ���-��� � ��������� ���� ������, ��� �����������. ����� ����, ��� ����������� ������ ������� �� ��������, ��������� � �������� ������ ���� ����� �������� � ��� �����, � �� ��� ������, � ����.</P>
<P align=justify><B>���: ����� ������� ���������� ���������� ����� �������?</B></P>
<P align=justify><B>�. ����������:</B> �����, ��� ������ ���������� ���������� �� ����� ���� ������� �� ������ ��� �������� ������ ������, �������� � &#171;���-30&#187;. ����� � ���� ��� ���� ����� ����� ������������ �� ������, �� �� ������ ������� �������� � �������, ����� ���-�� ����������. � � ������ ������� ��������� �������. ���� �� �� � ��������� ����� �������, ���� � ���� ����� ���������� ��������, � ������������ ����������� �� �� ������ ������ �������� �� �����. ���� ���-������ ����� � ������� �����, � ����� ������ ������������ ��� ����, ��� ��������� � ����� ������, ������� ������ ��� �� ������ ��������� ����. ��������� � ��� ��� ������ �����������, �� ����� �������� ����������� �������, ��� ����� ����� �����-�� �������� ������������. ��������, ����� ���� ������� ��������� �������������� ��������, ��� ��������� ������� ������ � ��������, ��������� ����������������� ����������������, ������ ���������. ��� ���� �� ����� ���� ������� ������� �����, ��� ����������� ����� ���������� � ���, ��� �������� ���� ���������, � ���, ��� � ��� �����������.</P>
<P align=justify><B>���: ��� ���-���� ��������� � ���� �����������?</B></P>
<P align=justify><B>�. ����������:</B> ��������� ��� ���� ����� ��������������� ������� �� ���������, �����, ����� ���� �������� � �������������� ����� � �����, ����� ������ ���������� � ���� ������ ���������, � �� ������ ��������. ������ ���, ����� �� ������� � ���������, ��� ��������: &#171;��� ������ ����� ����� ��� ���&#187;, � ��� 300 ���, �� ���������, ��� �� ����� ���� �� ����� ��� ��� ���� ������. � �������, � ��� � ����� ���������� ������� ������ �������, ������� �������� �������� �� ������ �������. ���-���� ��������� ������ ������ ����.</P>
<P class=vrez align=justify>����� ��������� � ���� �����������, ����� ��� ���� - ��������������� ������� �� ���������, �������� � �������������� ����� � �����, ����� ������ ���������� � ���� ������ ���������, � �� ������ ��������.</P>
<P align=justify><B>���: ��� ������ �������� ��������� ������������ ������?</B></P>
<P align=justify><B>�. ����������:</B> � ��������� ������� ��������� ������������ �������� ���������� ������������, ���-���� ����� ������� ������. �� � �������� ������������ - � ������ �������� ���� � ����.</P>
<P align=justify><B>���: ��� ���� ������������ �����, ������� �������, ��� ����� ��������� ������� ������-��������������� ����? ������ ��� ��������?</B></P>
<P align=justify><B>�. ����������: </B>�������� ���� � ������� ��������� ��������� ���� - � ����� ������ ��������� ����� ������, ����� ��� ������������, �� ����� ���������� ��� ��������� �� ����������� �����, ���� �� ����� ��������� �����������. ����� ������, ���������� ������������� �� ���� ���� ��� �������, ������� �� ����� ��� �������. ����� ����� ����� &#171;����������&#187;, ��� ������ ��������� ���� ��������� �����. ����� ����, � �����-�� ������, �������� ������� �����, ����������� ����. ����������� ����� �������� � ������� ��������� � �������� ���������. ���� �� ������ ���� ������-��������������� ������, �� ���� �����, ����� ���� ���� ����� � ������ ����������� ������� �������. �� ������ ����� ��� �� ���� �������� �� ���������� �����������. ��� ������ ����.</P>
<P align=justify><B>���: ����� ����� ����� ����� ������������ �� ������?</B></P>
<P align=justify><B>�. ����������:</B> �������, �� �����. ���� �� ������ �������������� � ���� �������, �� ������ ���� ����� � ����, ��� ��� ������� � ������. ��, � ������ �������, ��������, � ������ �������������� � �������� ��������� �����, � ������� ���� �������, � � ������� �� ���. ���� �� ������ ����� �� ����� ��� ������ ������ ������� ����������, ��� �������� ������, ������� ����� $2 ����., � �� $500 ���., ��, ��������, � �������� �� ����� ����������. ������ ����������� ������� ������� ��������� ������������� �����.</P>
<P align=justify><B>���: � ��� ������ ���������� ��������, �� �� � ������ ���� ���� ������� ������.</B></P>
<P align=justify><B>�. ����������:</B> � ������ � ���� ��� �� ���� ������������ ������ � ���������� ���������. � �� �������� ���� ������, ���� �������, ��� ��� �������� �� ���� �����. � ������ �� ������� � ���� �� ����������, ��� ��� ���� ����� ���� ���������� 40-�������� ������� � ������.</P>
<P align=justify>����� ������������� ����������� �� ���� ��������� ����������� ���������� ���������. ��� ����� �����, ��� HSBC � Royal Bank of Scotland, ��������� ������� �������� �������� ������ 500 ��. ������. ��� ���������������� ���, ��� ��������, ��� �������� ������� ����� ����� ������ �������. ����� ������� ������ �������� �������� � �� � ��� �� ��������, ����������, ������.</P>
<P align=justify>���� �� �� ������ ���������� � �����������, ��� ������ ���� ����������� ���������� ����� �����, ������� ������ ��������. �������, ��� ��� ������, �� � ��������� ��� ������ ������ � ����, � �� � ������. ��� ��� ��� ��� ������� �� ��������� � ���, ��� ����� ������������ �� ���������� ���������, �� ����������.</P>
<P align=justify></P></DIV></DIV>


</body>
</html>

