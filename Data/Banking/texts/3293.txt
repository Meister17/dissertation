<!--
ID:35836838
TITLE:�������� ����������� ���� ���������� ���������� ������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:1
ISSUE_DATE:2006-01-15
RECORD_DATE:2006-02-07


-->
<html>
<head>
<title>
35836838-�������� ����������� ���� ���������� ���������� ������
</title>
</head>
<body >


<B>
<P align=center>���� ���������<BR>�� ������� 2006 �.</P></B>
<TABLE cellSpacing=1 cellPadding=7 width="100%" align=center border=1>
<TBODY>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center>&#8470;<BR>�/�</P></TD>
<TD vAlign=center width="15%" height=28>
<P align=center>����</P></TD>
<TD vAlign=center width="41%" height=28>
<P align=center>���� ��������</P></TD>
<TD vAlign=center width="39%" height=28>
<P align=center>��������� ����������</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center>1</P></TD>
<TD vAlign=center width="15%" height=28>
<P>06.02&#8211; 07.02</P></TD>
<TD vAlign=center width="41%" height=28>
<P>����� ���� �������� � ����� ����� ������������</P></TD>
<TD vAlign=top width="39%" height=28>
<P>������������ � ���������� ��������� ������������� ������, ����� ��������� ���������</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=43>
<P align=center>2</P></TD>
<TD vAlign=center width="15%" height=43>
<P>07.02&#8211;09.02</P></TD>
<TD vAlign=center width="41%" height=43>
<P>������������ ������� �������� � ����� � ������ ���������� ����� ������</P></TD>
<TD vAlign=top width="39%" height=43>
<P>������������ � ����������� ���������� �������������, ������� ����������, ���������-������������ �� ������ � ����������� ������, ������������ ��������� � ���������, �������������</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=17>
<P align=center>3</P></TD>
<TD vAlign=center width="15%" height=17>
<P>07.02&#8211;09.02</P></TD>
<TD vAlign=center width="41%" height=17>
<P>������� ����������� ����������� ���������� � ����������� �������� ������ � �����</P></TD>
<TD vAlign=top width="39%" height=17>
<P>������������ �������� �����, ���-����</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=17>
<P align=center>4</P></TD>
<TD vAlign=center width="15%" height=17>
<P>09.02&#8211;11.02</P></TD>
<TD vAlign=center width="41%" height=17>
<P>���������� ������� ��������� ������������� � ��������� ��������</P></TD>
<TD vAlign=top width="39%" height=17>
<P>������������ � ����������� �������������, �������������� �������� ��������, ������������� ��������</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center>5</P></TD>
<TD vAlign=center width="15%" height=28>
<P>10.02&#8211;11.02</P></TD>
<TD vAlign=center width="41%" height=28>
<P>����������� ������ �� ��������������� ����������� (���������) �������, ���������� ���������� ����� � �������������� ���������� � ��������� ������������</P></TD>
<TD vAlign=top width="39%" height=28>
<P>������������ ������������� ������ ������������ �����, ��������� &#8211; ��������, ����������� ��������</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=17>
<P align=center>6</P></TD>
<TD vAlign=center width="15%" height=17>
<P>10.02&#8211;11.02</P></TD>
<TD vAlign=center width="41%" height=17>
<P>����������� �������� ����������� ����������� ����������: �������������� ������������� ������������</P></TD>
<TD vAlign=top width="39%" height=17>
<P>������������ ������� � �������� �����, ������������ � ����������� ������������� �����</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center>7</P></TD>
<TD vAlign=center width="15%" height=28>
<P>11.02</P></TD>
<TD vAlign=center width="41%" height=28>
<P>����� ������� � ���������� ������������� �������</P></TD>
<TD vAlign=top width="39%" height=28>
<P>������������ ���� ����������� ������������, ����������� ������������� �����, ����� ������� � �������� ���������� ������, ����������� �������� � ������, ��������� �������� � ������� ����� �����</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=29>
<P align=center>8</P></TD>
<TD vAlign=center width="15%" height=29>
<P>13.02&#8211;14.02</P></TD>
<TD vAlign=center width="41%" height=29>
<P>������������� �����: ����� ����������� ��� ������������� �����</P></TD>
<TD vAlign=top width="39%" height=29>
<P>�����������, ������������� �� ������������ ������������� ��������; ������������� ���������� �� ������������ ����������� ���; �������� ������ �� ��������� ������������� ������������� ��������</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center>9</P></TD>
<TD vAlign=center width="15%" height=28>
<P>13.02&#8211;16.02</P></TD>
<TD vAlign=center width="41%" height=28>
<P>���������� �������� � ����� � �����, ��������������� ��������� �����������</P></TD>
<TD vAlign=top width="39%" height=28>
<P>������������ � ����������� ������������� ����������� �������� � ������, ����������� ���������� ������</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center>10</P></TD>
<TD vAlign=center width="15%" height=28>
<P>13.02&#8211;15.02</P></TD>
<TD vAlign=center width="41%" height=28>
<P>��������������� � ������������� ���� � �����</P></TD>
<TD vAlign=top width="39%" height=28>
<P>������� ����������, ������������ � ���������� ������, ������������ ����������������</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center>&#8470;<BR>�/�</P></TD>
<TD vAlign=center width="15%" height=28>
<P align=center>����</P></TD>
<TD vAlign=center width="41%" height=28>
<P align=center>���� ��������</P></TD>
<TD vAlign=center width="39%" height=28>
<P align=center>��������� ����������</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center>11</P></TD>
<TD vAlign=center width="15%" height=28>
<P>13.02&#8211;14.02</P></TD>
<TD vAlign=center width="41%" height=28>
<P>����������� ������������ �������� � �������������� ������</P></TD>
<TD vAlign=top width="39%" height=28>
<P>������������ �������� � �������������� ������, �������������� ������ ������, ���������� �� ������������ ���������� ����</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=17>
<P align=center>12</P></TD>
<TD vAlign=center width="15%" height=17>
<P>16.02&#8211;17.02</P></TD>
<TD vAlign=center width="41%" height=17>
<P>��������� � ������� ������������� ����� �������� ���������� ����</P></TD>
<TD vAlign=top width="39%" height=17>
<P>������������ � ���������� ����������� � ������������� ����������� �������� � ������, ����������� ������, ������������ ������������ ���������� ����</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center>13</P></TD>
<TD vAlign=center width="15%" height=28>
<P>16.02&#8211;17.02</P></TD>
<TD vAlign=center width="41%" height=28>
<P>���������� ���������� ����������� (������. ���������)</P></TD>
<TD vAlign=top width="39%" height=28>
<P>����������� ������, ������������ ����������� � �������������� ���������� � ������������</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center>14</P></TD>
<TD vAlign=center width="15%" height=28>
<P>16.02&#8211;17.02</P></TD>
<TD vAlign=center width="41%" height=28>
<P>����������������� ��������� � ������������� �������</P></TD>
<TD vAlign=top width="39%" height=28>
<P>������������ � ���������� ������� ��������� � ����������������� ��������� ������, � ����� �����������</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center>15</P></TD>
<TD vAlign=center width="15%" height=28>
<P>18.02</P></TD>
<TD vAlign=center width="41%" height=28>
<P>������������ �������� �� ��������� ������ �� ������</P></TD>
<TD vAlign=top width="39%" height=28>
<P>��������� ���������� � ��������������� ������������� �����, ������������� ����������� �������� � ����������� �����</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=17>
<P align=center>16</P></TD>
<TD vAlign=center width="15%" height=17>
<P>20.02&#8211;21.02</P></TD>
<TD vAlign=center width="41%" height=17>
<P>������ ������������� ������������ � ������ ������������������ ����������� ���������</P></TD>
<TD vAlign=top width="39%" height=17>
<P>������������ � ����������� ��������� � �������������� ������������� �����</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center>17</P></TD>
<TD vAlign=center width="15%" height=28>
<P>20.02&#8211;22.02</P></TD>
<TD vAlign=center width="41%" height=28>
<P>��������� �������� � �����</P></TD>
<TD vAlign=top width="39%" height=28>
<P>������������ � ���������� ��������� ������������� ������</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center>18</P></TD>
<TD vAlign=center width="15%" height=28>
<P>20.02&#8211;22.02</P></TD>
<TD vAlign=center width="41%" height=28>
<P>���������� ������ ���������� ������������</P></TD>
<TD vAlign=top width="39%" height=28>
<P>������������ � ����������� ������������� �����, ����� �� ���������� ����������� �������, ����������� ��������, �������� � ������-����</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=18>
<P align=center>19</P></TD>
<TD vAlign=center width="15%" height=18>
<P>20.02&#8211;22.02</P></TD>
<TD vAlign=center width="41%" height=18>
<P>������� � ��������: ����������������, ���� ��������� ������ � ��������� ������������</P></TD>
<TD vAlign=top width="39%" height=18>
<P>������������ � ���������� ������������� ������, ������������ ��������� � ��������������� �������������</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=17>
<P align=center>20</P></TD>
<TD vAlign=center width="15%" height=17>
<P>22.02</P></TD>
<TD vAlign=center width="41%" height=17>
<P>�������� � ������ ������ ������� ������ �����. �������� ����������� ����������� ������� ������ �����</P></TD>
<TD vAlign=top width="39%" height=17>
<P>�����������, ���������� � ������� ��������</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=17>
<P align=center>21</P></TD>
<TD vAlign=center width="15%" height=17>
<P>26.02&#8211;01.03</P></TD>
<TD vAlign=center width="41%" height=17>
<P>�������� ������������� ������������ �����</P></TD>
<TD vAlign=top width="39%" height=17>
<P>������������ � ���������� ����������� �����</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=29>
<P align=center>22</P></TD>
<TD vAlign=center width="15%" height=29>
<P>27.02&#8211;28.02</P></TD>
<TD vAlign=center width="41%" height=29>
<P>������� ������������ ��� �����: ����������, ���������� � �������� �������</P></TD>
<TD vAlign=top width="39%" height=29>
<P>����������� ��������� � ����������� ������������� �����</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=29>
<P align=center>23</P></TD>
<TD vAlign=center width="15%" height=29>
<P>27.02&#8211;28.02</P></TD>
<TD vAlign=center width="41%" height=29>
<P>��������� ������ ���������� ������������ ��� ����������� � ��������� �������� ��� ������ ���������� ������� �����</P></TD>
<TD vAlign=top width="39%" height=29>
<P>����������� �� ������ � ���������</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=29>
<P align=center>24</P></TD>
<TD vAlign=center width="15%" height=29>
<P>27.02&#8211; 01.03</P></TD>
<TD vAlign=center width="41%" height=29>
<P>����������� ������ � ���������� ����������� �������</P></TD>
<TD vAlign=top width="39%" height=29>
<P>������������ ������������� ����������� ��������, ����-���������</P></TD></TR></TBODY></TABLE>
<P align=justify>���������� �� ����� ��� ���: www.ibdarb.ru</P><B><I>
<P align=justify>105187, ������, ��. ������������, 38<BR>(095) 365-3105, 366-5302, 366-2351<BR>����: (095) 365-0107, 365-1107<BR>E-mail:info@ibdarb.ru<BR>http://www.ibdarb.ru</P></B></I>


</body>
</html>

