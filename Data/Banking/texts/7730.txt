<!--
ID:37528404
TITLE:���� - ������ �� ������� ��������
SOURCE_ID:10908
SOURCE_NAME:������������ ���������� ������
AUTHOR:�������� ����������
NUMBER:4
ISSUE_DATE:2007-04-30
RECORD_DATE:2007-04-12


-->
<html>
<head>
<title>
37528404-���� - ������ �� ������� ��������
</title>
</head>
<body >


<DIV class=article>
<DIV class=header>
<H2><FONT size=3>������� ����� ���������� ����� -&nbsp;&nbsp;�������-��������� �&nbsp;&nbsp;����������� �����������</FONT></H2></DIV>
<DIV class=body>
<P align=justify>&#171;������ ����� ������ ������������� ������� � ���������� � 90-� ���� ��������� ������ &#171;����������&#187; ��������&#187;, - �������� ������������ �������������� ������ &#171;���������� ����� ������������&#187; ���� ��������. �� ��� ������, ������������� ����� ����� ��������, ��� ������� �������� � ��������������� �����, - ��� �������������� �������, ������� ������� ���������� ��� �������� � ����� ������������ ������.</P>
<P align=justify>������ ��� ������������ ���������, ������� � 2000 ���� �������� ������������ ������� � ������-��������, ������� ����� �������� ����������� ���������.</P>
<P align=justify>&#171;� ��������� ����� ����������� � ��������� �������������, - �������� �������� ������������ ���������� ������������ ��������� ������������ &#171;���������&#187; ������ ������. - ���� ������ ���������� �������������� ���������, �����������, ��������, ��������� � ������� �����, �� ������ ������ ��������� ��� �����������. ������� ����� ����� ���������� ������������ ���������� ������� �� 2006 ��� ���������� ����� �� 50%&#187;.</P>
<P align=justify>����, ������� ������������ ��������� &#171;����������� ����&#187; ���������� &#171;�����-������������&#187; ������ �����������, - ��� ���� ���� � ������ �� ����� ������� ��������, ������� ���������� ����� �������. ���� ��������� �����������, ����������� �� ����, ��������� � ����������� ���������, ������� �����, ������� ������ � ��������������. ������� ��������� ����������� ���������� ��������, ������������� �� �������-���������� �����������. ������ ����� �� ���� ����������� �������� ������� ������� ($50 ���. �� ��� �������� 3 ���. ���������� ������, ������� �� 1 ������� �����). ������� ������� ������� � �����-2.</P>
<P align=justify>��� �����, ������� ����� ���������� �� �������, ������� �������� ������ �������������� �� �������� �����: ������������ ���������� ���� � ������� �����, ���������, ����������� �������. ����������� ������� �� ���������� ������� �������� ����������� � ����������� ���������������� ������ ������������.</P>
<P align=justify></P>
<P align=justify></P></DIV></DIV>


</body>
</html>

