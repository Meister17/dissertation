<!--
ID:38588529
TITLE:���������� � ����������� � �������������� ��������� ����������� �� 1 ������ 2007 ����*
SOURCE_ID:3144
SOURCE_NAME:������ � ������
AUTHOR:
NUMBER:11
ISSUE_DATE:2007-11-30
RECORD_DATE:2007-12-24


-->
<html>
<head>
<title>
38588529-���������� � ����������� � �������������� ��������� ����������� �� 1 ������ 2007 ����*
</title>
</head>
<body >


<B>
<P align=center><FONT>����������� ��������� �����������</FONT></P></B>
<TABLE cellSpacing=1 cellPadding=7 width="100%" align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><B>
<P align=center><FONT>1.</FONT></B></P></TD>
<TD vAlign=top width="84%" bgColor=#ffffff><B>
<P><FONT>���������������� ��** ������ ������ ���� �� ��������� ��� ������� ��������������&nbsp;�������������� �������, ����� <SUP>1</B></SUP></FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>1310</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>� ��� �����:</FONT></P></TD>
<TD vAlign=bottom width="10%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%" bgColor=#ffffff>
<P><FONT>&#8211;&nbsp; ������</FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>1258</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>&#8211;&nbsp; ������������ ��</FONT></P></TD>
<TD vAlign=bottom width="10%"><B>
<P align=center><FONT>52</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff>
<P align=center><FONT>1.1.</FONT></P></TD>
<TD vAlign=top width="84%" bgColor=#ffffff>
<P><FONT>���������������� �� �� 100-���������� ����������� �������� � ��������</FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>61</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>1.2.</FONT></P></TD>
<TD vAlign=top width="84%">
<P><FONT>��, ������������������ ������ ������, �� ��� �� ���������� �������� ������� � �� ���������� �������� (�&nbsp;������ �������������� �������������� �����)</FONT></P></TD>
<TD vAlign=bottom width="10%"><B>
<P align=center><FONT>2</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%" bgColor=#ffffff>
<P><FONT>� ��� �����:</FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>&#8211;&nbsp; �����</FONT></P></TD>
<TD vAlign=bottom width="10%"><B>
<P align=center><FONT>1</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%" bgColor=#ffffff>
<P><FONT>&#8211;&nbsp; ������������ ��</FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>1</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=bottom width="10%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><B>
<P align=center><FONT>2.</FONT></B></P></TD>
<TD vAlign=top width="84%" bgColor=#ffffff><B>
<P><FONT>������������ ��, ������������������ �� 1.07.2002 ������� ��������</FONT></B></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>0</FONT></B></P></TD></TR></TBODY></TABLE><B>
<P align=center><FONT>����������� ��������� �����������</FONT></P></B>
<TABLE cellSpacing=1 cellPadding=7 width="100%" align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><B>
<P align=center><FONT>3.</FONT></B></P></TD>
<TD vAlign=top width="84%" bgColor=#ffffff><B>
<P><FONT>��, ������� ����� �� ������������� ���������� ��������, ����� <SUP>2</B></SUP></FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>1145</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>� ��� �����:</FONT></P></TD>
<TD vAlign=bottom width="10%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%" bgColor=#ffffff>
<P><FONT>&#8211;&nbsp; �����</FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>1101</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>&#8211;&nbsp; ������������ ��</FONT></P></TD>
<TD vAlign=bottom width="10%"><B>
<P align=center><FONT>44</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff>
<P align=center><FONT>3.1.</FONT></P></TD>
<TD vAlign=top width="84%" bgColor=#ffffff>
<P><FONT>��, ������� �������� (����������), ��������������� ����� ��:</FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>&#8211;&nbsp; ����������� ������� ���������</FONT></P></TD>
<TD vAlign=bottom width="10%"><B>
<P align=center><FONT>911</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%" bgColor=#ffffff>
<P><FONT>&#8211;&nbsp; ������������� �������� � ����������� ������</FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>764</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>&#8211;&nbsp; ����������� ��������</FONT></P></TD>
<TD vAlign=bottom width="10%"><B>
<P align=center><FONT>297</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%" bgColor=#ffffff>
<P><FONT>&#8211;&nbsp; ���������� �������� � �������������</FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>&#8211;&nbsp; ����������</FONT></P></TD>
<TD vAlign=bottom width="10%"><B>
<P align=center><FONT>4</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%" bgColor=#ffffff>
<P><FONT>&#8211;&nbsp; �������� <SUP><STRONG>3</STRONG></SUP></FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>194</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>3.2.</FONT></P></TD>
<TD vAlign=top width="84%">
<P><FONT>�� � ����������� �������� � �������� ��������, �����</FONT></P></TD>
<TD vAlign=bottom width="10%"><B>
<P align=center><FONT>199</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>�� ���:</FONT></P></TD>
<TD vAlign=bottom width="10%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%" bgColor=#ffffff>
<P><FONT>&#8211;&nbsp; �� 100-����������</FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>61</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>&#8211;&nbsp; ����� 50 ���������</FONT></P></TD>
<TD vAlign=bottom width="10%"><B>
<P align=center><FONT>22</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff>
<P align=center><FONT>3.3.</FONT></P></TD>
<TD vAlign=top width="84%" bgColor=#ffffff>
<P><FONT>��, ���������� � ������ ������ &#8211; ���������� ������� ������������� ����������� �������, �����</FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>915</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=bottom width="10%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><B>
<P align=center><FONT>4.</FONT></B></P></TD>
<TD vAlign=top width="84%" bgColor=#ffffff><B>
<P><FONT>������������������ �������� ������� ����������� �� (��� ���.)</FONT></B></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>721 244</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=bottom width="10%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><B>
<P align=center><FONT>5.</FONT></B></P></TD>
<TD vAlign=top width="84%" bgColor=#ffffff><B>
<P><FONT>������� ����������� �� �� ���������� ���������� ���������, �����</FONT></B></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>3431</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>�� ���:</FONT></P></TD>
<TD vAlign=bottom width="10%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%" bgColor=#ffffff>
<P><FONT>&#8211;&nbsp; ��������� ������ <SUP><STRONG>4</STRONG></SUP></FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>829</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>&#8211;&nbsp; ������ �� 100-���������� ����������� �������� � �������� ��������</FONT></P></TD>
<TD vAlign=bottom width="10%"><B>
<P align=center><FONT>155</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><B>
<P align=center><FONT>6.</FONT></B></P></TD>
<TD vAlign=top width="84%" bgColor=#ffffff><B>
<P><FONT>������� ����������� �� �� �������, ����� <SUP>5</B></SUP></FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>3</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=bottom width="10%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><B>
<P align=center><FONT>7.</FONT></B></P></TD>
<TD vAlign=top width="84%" bgColor=#ffffff><B>
<P><FONT>������� ������-������������ �� ���������� ���������� ���������</FONT></B></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>0</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=bottom width="10%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><B>
<P align=center><FONT>8.</FONT></B></P></TD>
<TD vAlign=top width="84%" bgColor=#ffffff><B>
<P><FONT>����������������� ����������� ���������� ��, ����� <SUP>6</B></SUP></FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>793</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>� ��� �����:</FONT></P></TD>
<TD vAlign=bottom width="10%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%" bgColor=#ffffff>
<P><FONT>&#8211;&nbsp; �� ���������� ���������� ���������</FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>747</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>&#8211;&nbsp; � ������� ���������</FONT></P></TD>
<TD vAlign=bottom width="10%"><B>
<P align=center><FONT>33</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%" bgColor=#ffffff>
<P><FONT>&#8211;&nbsp; � ������� ���������</FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>13</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=bottom width="10%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><B>
<P align=center><FONT>9.</FONT></B></P></TD>
<TD vAlign=top width="84%" bgColor=#ffffff><B>
<P><FONT>�������������� ����� ��, �����</FONT></B></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>18 091</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>� ��� ����� ��������� ������</FONT></P></TD>
<TD vAlign=bottom width="10%"><B>
<P align=center><FONT>8369</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><B>
<P align=center><FONT>10.</FONT></B></P></TD>
<TD vAlign=top width="84%" bgColor=#ffffff><B>
<P><FONT>������������ ����� ��� ��������� ���� ��, �����</FONT></B></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>14 979</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>� ��� ����� ��������� ������</FONT></P></TD>
<TD vAlign=bottom width="10%"><B>
<P align=center><FONT>11 049</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><B>
<P align=center><FONT>11.</FONT></B></P></TD>
<TD vAlign=top width="84%" bgColor=#ffffff><B>
<P><FONT>��������-�������� ����� ��, �����</FONT></B></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>1439</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>� ��� ����� ��������� ������</FONT></P></TD>
<TD vAlign=bottom width="10%"><B>
<P align=center><FONT>0</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><B>
<P align=center><FONT>12.</FONT></B></P></TD>
<TD vAlign=top width="84%" bgColor=#ffffff><B>
<P><FONT>������������ ����� �� (��������), �����</FONT></B></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>334</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>� ��� ����� ��������� ������</FONT></P></TD>
<TD vAlign=bottom width="10%"><B>
<P align=center><FONT>0</FONT></B></P></TD></TR></TBODY></TABLE><B>
<P align=center><FONT>����� �������� � ���������� ����������� ���</FONT></P></B>
<TABLE cellSpacing=1 cellPadding=7 width="100%" align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><B>
<P align=center><FONT>13.</FONT></B></P></TD>
<TD vAlign=top width="84%" bgColor=#ffffff><B>
<P><FONT>��, � ������� �������� (������������) �������� �� ������������� ���������� �������� �&nbsp;������� ��&nbsp;��������� �� ����� ��������������� ����������� ��������� ����������� <SUP>7</B></SUP></FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>163</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=bottom width="10%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><B>
<P align=center><FONT>14.</FONT></B></P></TD>
<TD vAlign=top width="84%" bgColor=#ffffff><B>
<P><FONT>������� ������ � ����� ��������������� ����������� ��������� ����������� � ���������� �� ���&nbsp;������������ ����, ����� <SUP>8</B></SUP></FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>1801</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>� ��� �����:</FONT></P></TD>
<TD vAlign=bottom width="10%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%" bgColor=#ffffff>
<P><FONT>&#8211;&nbsp; � ����� � ������� (��������������) ��������</FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>1403</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>&#8211;&nbsp; � ����� � ��������������</FONT></P></TD>
<TD vAlign=bottom width="10%"><B>
<P align=center><FONT>397</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%" bgColor=#ffffff>
<P><FONT>�� ���:</FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD></TR>
<TR
>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>&#8211;&nbsp; � ����� �������</FONT></P></TD>
<TD vAlign=bottom width="10%"><B>
<P align=center><FONT>2</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%" bgColor=#ffffff>
<P><FONT>&#8211;&nbsp; � ����� �������������</FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>395</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>� ��� �����:</FONT></P></TD>
<TD vAlign=bottom width="10%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%" bgColor=#ffffff>
<P><FONT>&#8211;&nbsp; ����� �������������� � ������� ������ ������</FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>343</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%">
<P><FONT>&#8211;&nbsp; ������������ � ������ ������ (��� ����������� �������)</FONT></P></TD>
<TD vAlign=bottom width="10%"><B>
<P align=center><FONT>52</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%" bgColor=#ffffff><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="84%" bgColor=#ffffff>
<P><FONT>&#8211;&nbsp; � ����� � ���������� ���������������� � ����� ������ ��������� ��������</FONT></P></TD>
<TD vAlign=bottom width="10%" bgColor=#ffffff><B>
<P align=center><FONT>1</FONT></B></P></TD></TR></TBODY></TABLE>
<P align=justify><FONT>*&nbsp;���������� ������������ � ��� ����� �� ��������� ��������, ����������� �� ��������������� ��������������� ������ �� �������� ����.</FONT></P><B>
<P align=justify><FONT>��������� � �������</FONT></P></B>
<P align=justify><FONT>** �� &#8211; ��������� �����������. ������� &#8220;��������� �����������&#8221; � ��������� ���������� �������� � ���� ���� �� ��������� �������:</FONT></P>
<P align=justify><FONT>&#183; &nbsp;����������� ����, ������������������ ������ ������ (�� 1.07.2002) ��� �������������� �������������� ������� � ������� ����� �� ������������� ���������� ��������;</FONT></P>
<P align=justify><FONT>&#183; &nbsp;����������� ����, ������������������ ������ ������ (�� 1.07.2002) ��� �������������� �������������� �������, �������, �� ���������� ����� �� ������������� ���������� ��������;</FONT></P>
<P align=justify><FONT>&#183; &nbsp;����������� ����, ������������������ ������� �������� (�� ���������� � ���� ������������ ������ &#8220;� ������ � ���������� ������������&#8221;) � ������� �������� ����� ������ �� ������������� ���������� ��������.</FONT></P><SUP>
<P align=justify><FONT>1 </FONT></SUP><FONT>����������� ��, ������� ������ ������������ ���� �� �������� ����, � ��� ����� ��, ���������� ����� �� ������������� ���������� ��������, �� ��� �� ��������������� ��� ����������� ����.</FONT></P><SUP>
<P align=justify><FONT>2 </FONT></SUP><FONT>����������� ��, ������������������ ������ ������ (�� 1.07.2002) ��� �������������� �������������� ������� � ������� ����� �� ������������� ���������� ��������, � ����� ������������ ��, ������������������ ������� �������� � ���������� �������� ����� ������ �� ������������� ���������� ��������.</FONT></P><SUP>
<P align=justify><FONT>3 </FONT></SUP><FONT>�������� � ������� 1996 �. � ������������ � ������� ����� ������ �� 3.12.1996 &#8470; 367.</FONT></P><SUP>
<P align=justify><FONT>4 </FONT></SUP><FONT>����������� ������� ��������� ������, ��������� � ����� ��������������� ����������� ��������� ����������� � ���������� ���������� ������. �� 1.01.1998 � ����������� ���������� � ��������� ������������ �� ������ ������ ����������� ����� ���������� ���������� ��������� ������ &#8211; 34 426.</FONT></P><SUP>
<P align=justify><FONT>5 </FONT></SUP><FONT>����������� �������, �������� ����������� �� �� �������.</FONT></P><SUP>
<P align=justify><FONT>6 </FONT></SUP><FONT>� ����� ���������������� ���������� �� �� ������� �������� �����������������, �� ������� ��������� � ���� ������ ����������� �� �������� �� �� �������.</FONT></P><SUP>
<P align=justify><FONT>7 </FONT></SUP><FONT>����� ���������� ��, � ������� ������ ������ ���� �������� (������������) �������� �� ������������� ���������� �������� (������� ��, �� ������� � ����� ��������������� ����������� ��������� ����������� ������� ������ �� �� ����������), &#8211; 1575.</FONT></P><SUP>
<P align=justify><FONT>8 </FONT></SUP><FONT>����� 1.07.2002 ������ � ����� ��������������� ����������� ��������� ����������� � ���������� ��������� ����������� ��� ������������ ���� �������� ������ ����� ��������������� ����������� ��������� ����������� � ����� � �� ����������� �������������� �������������� �������.</FONT></P><I>
<P align=right><FONT>�������� ����������� ������������� �������������� ������������<BR>�&nbsp;����������� ������������ ��������� ����������� ����� ������</FONT></P></I>


</body>
</html>

