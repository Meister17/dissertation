<!--
ID:36286121
TITLE:���������� � ����������� � �������������� ��������� ����������� �� 01.03.2006 �.*
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:8
ISSUE_DATE:2006-04-30
RECORD_DATE:2006-06-01


-->
<html>
<head>
<title>
36286121-���������� � ����������� � �������������� ��������� ����������� �� 01.03.2006 �.*
</title>
</head>
<body >


<TABLE cellSpacing=2 cellPadding=9 width=100% align=center border=1>
<TBODY>
<TR>
<TD vAlign=top colSpan=5><B>
<P align=center>����������� ��������� �����������</B></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=center><A name=DDE_LINK1></A></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center>�� 01.01.2006</P></TD>
<TD vAlign=top width="15%">
<P align=center>�� 01.03.2006</P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><B>
<P align=justify>1. ���������������� ��*<SUP>*</SUP> ������ ������ ���� �� ��������� ��� ������� �������������� �������������� �������, �����<SUP>1</SUP></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>1 409</B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=right>1 399</B></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>&#61548; ������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 356</P></TD>
<TD vAlign=top width="15%">
<P align=right>1 345</P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>&#61548; ������������ ��</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>53</P></TD>
<TD vAlign=top width="15%">
<P align=right>54</P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<DIR>
<P align=justify>1.1. ���������������� �� �� 100%-��� ����������� �������� � ��������</P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>42</P></TD>
<TD vAlign=top width="15%">
<P align=right>43</P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<DIR>
<P align=justify>1.2. ��, ������������������ ������ ������, �� ��� �� ���������� �������� ������� � �� ���������� �������� (� ������ �������������� �������������� �����)</P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right></P>
<P align=right></P>
<DIR>
<P align=right>2</P></DIR></TD>
<TD vAlign=top width="15%">
<P align=right></P>
<P align=right></P>
<DIR>
<P align=right>2</P></DIR></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>&#61548; �����</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<DIR>
<DIR>
<P align=right>2</P></DIR></DIR></TD>
<TD vAlign=top width="15%">
<DIR>
<DIR>
<P align=right>1</P></DIR></DIR></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>&#61548; ������������ ��</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<DIR>
<DIR>
<P align=right>0</P></DIR></DIR></TD>
<TD vAlign=top width="15%">
<DIR>
<DIR>
<P align=right>1</P></DIR></DIR></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<DIR>
<DIR><B>
<P align=justify>2. ������������ ��, ������������������ �� 01.07.2002 ������� ��������</P></DIR></DIR></B></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>0</B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=right>0</B></P></TD></TR>
<TR>
<TD vAlign=top colSpan=5><B>
<P align=center>����������� ��������� �����������</B></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><B>
<P align=justify>3. ��, ������� ����� �� ������������� ���������� ��������, �����<SUP>2</SUP></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>1 253</B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=right>1 244</B></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>&#61548; �����</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 205</P></TD>
<TD vAlign=top width="15%">
<P align=right>1 197</P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>&#61548; ������������ ��</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>48</P></TD>
<TD vAlign=top width="15%">
<P align=right>47</P></TD></TR>
<TR>
<TD vAlign=top colSpan=5><I>
<P align=right>�����������</I></P></TD></TR>
<TR>
<TD vAlign=top width="69%">&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center>�� 01.01.2006</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center>�� 01.03.2006</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<P align=justify>3.1. ��, ������� �������� (����������), ��������������� ����� ��:</P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>&#61548; ����������� ������� ���������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 045</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>994</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>&#61548; ������������� �������� � ����������� ������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>827</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>830</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>&#61548; ����������� ��������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>301</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>294</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>&#61548; ���������� �������� � �������������</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>����������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>4</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>4</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>��������<SUP>3</SUP></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>180</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>182</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<P align=justify>3.2. �� � ����������� �������� � �������� ��������, �����</P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>136</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>137</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>�� ���:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>�� 100%-���</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>41</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>43</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>����� 50%</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>11</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>12</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<P align=justify>3.3. ��, ���������� � ������ ����� &#8211; ���������� ������� ������������� �����������</P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>930</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>930</P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify>4. ������������������ �������� ������� ����������� �� (��� ���.)</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>444 377</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>451 976</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify>5. ������� ����������� �� �� ���������� ��, �����</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>3 295</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>3 282</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>�� ���:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>��������� ������<SUP>4</SUP></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 009</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>985</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>������ �� 100%-��� ����������� �������� � �������� ��������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>29</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>30</P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify>6. ������� ����������� �� �� �������, �����<SUP>5</SUP></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>3</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>3</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify>7. ������� ������-������������ �� ���������� ���������� ���������</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>0</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>0</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify>8. ����������������� ����������� ���������� ��, �����<SUP>6</SUP></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>467</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>473</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><B>&#61548;</B> �� ���������� ���������� ���������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>422</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>428</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><B>&#61548;</B> � ������� ���������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>31</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>31</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><B>&#61548;</B> � ������� ���������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>14</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>14</P></TD></TR>
<TR>
<TD vAlign=top colSpan=5><B>
<P align=center>����� �������� � ���������� ����������� ���</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR><B>
<P align=justify>9. ��, � ������� �������� (������������) �������� �� ������������� ���������� �������� � ������� �� ��������� �� ����� ��������������� ����������� ��������� �����������<SUP>7</SUP></P></DIR></B></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right></P>
<P align=right>154</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right></P>
<P align=right>153</B></P></TD></TR>
<TR>
<TD vAlign=top colSpan=5><I>
<P align=right>�����������</I></P></TD></TR>
<TR>
<TD vAlign=top width="69%">&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center>�� 01.01.2006</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center>�� 01.03.2006</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR><B>
<P align=justify>10. ������� ������ � ����� ��������������� ����������� ��������� ����������� � ���������� �� ��� ������������ ����, �����<SUP>8</SUP></P></DIR></B></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>1 687</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=center>1 699</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<DIR>
<P align=justify><B>&#61548;</B> � ����� � ������� (��������������) ��������</P></DIR></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1305</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1315</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><B>&#61548;</B> � ����� � ��������������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>381</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>383</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>�� ���: � ����� �������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>0</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>2</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>� ����� �������������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>381</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>381</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>����� �������������� � ������� ������ ������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>337</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>337</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>������������ � ������ ������ (��� ��������-<BR>��� �������)</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right></P>
<P align=right>44</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right></P>
<P align=right>44</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><B>&#61548;</B> � ����� � ������������ �������� ���������� � ����������</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<DIR>
<P align=justify><B>&#61548;</B> � ����� � ���������� ���������������� � ����� ������ ��������� ��������</P></DIR></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right></P>
<P align=right>1</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right></P>
<P align=right>1</P></TD></TR></TBODY></TABLE><SUP>
<P align=justify>*</SUP> ���������� ������������ � ��� ����� � �� ��������� ��������, ����������� �� ��������������� ��������������� ������ �� �������� ����.</P><B>
<P align=justify>��������� � �������.</P></B><SUP>
<P align=justify>*</SUP>* �� &#8211; ��������� �����������. ������� "��������� �����������" � ��������� ���������� �������� ���� �� ��������� �������:</P>
<DIR>
<DIR>
<P align=justify>&#61548; ����������� ����, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� ������� � ������� ����� �� ������������� ���������� ��������;</P>
<P align=justify>&#61548; ����������� ����, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� �������, �������, �� ���������� ����� �� ������������� ���������� ��������;</P>
<P align=justify>&#61548; ����������� ����, ������������������ ������� �������� (�� ���������� � ���� ������������ ������ "� ������ � ���������� ������������") � ������� �������� ����� ������ �� ������������� ���������� ��������.</P></DIR></DIR><SUP>
<P align=justify>1 </SUP>����������� ��, ������� ������ ������������ ���� �� �������� ����, � ��� ����� ��, ���������� ����� �� ������������� ���������� ��������, �� ��� �� ��������������� ��� ����������� ����.</P><SUP>
<P align=justify>2 </SUP>����������� ��, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� ������� � ������� ����� �� ������������� ���������� ��������, � ����� ������������ ��, ������������������ ������� �������� � ���������� �������� ����� ������ �� ������������� ���������� ��������.</P><SUP>
<P align=justify>3 </SUP>�������� � ������� 1996 �. � ������������ � ������� ����� ������ �� 03.12.1996 &#8470; 367.</P><SUP>
<P align=justify>4 </SUP>����������� ������� ��������� ������, ��������� � ����� ��������������� ����������� ��������� ����������� � ���������� ���������� ������. �� 01.01.1998 �. � ����������� ���������� � ��������� ������������ �� ������ ������ ����������� ����� ���������� ���������� ��������� ������ &#8211; 34 426.</P><SUP>
<P align=justify>5 </SUP>����������� �������, �������� ����������� �� �� �������.</P><SUP>
<P align=justify>6 </SUP>� ����� ���������������� ���������� �� �� ������� �������� �����������������, �� ������� ��������� � ���� �
����� ����������� �� �������� �� �� �������.</P><SUP>
<P align=justify>7 </SUP>����� ���������� ��, � ������� ������ ������ ���� �������� (������������) �������� �� ������������� ���������� �������� (������� ��, �� ������� � ����� ��������������� ����������� ��������� ����������� ������� ������ �� �� ����������), &#8211; 1478.</P><SUP>
<P align=justify>8 </SUP>����� 01.07.2002 � ����� ��������������� ����������� ��������� ����������� ������ � ���������� ��������� ����������� ��� ������������ ���� �������� ������ ����� ��������������� ����������� ��������� ����������� � ����� � �� ����������� �������������� �������������� �������.</P>


</body>
</html>

