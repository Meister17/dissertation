<!--
ID:35599882
TITLE:�������� � ������������
SOURCE_ID:3217
SOURCE_NAME:���������� ������ �����
AUTHOR:������� ����������, ������������

NUMBER:10
ISSUE_DATE:2005-10-31
RECORD_DATE:2005-12-01


-->
<html>
<head>
<title>
35599882-�������� � ������������
</title>
</head>
<body >


<P align=justify><STRONG><EM>���������� ����� ������������� ������ ������� ��������� ��������������� �����������. ���� ����� ��������� � ����������� ���� ������������� ����� &#8212; ���������. � ���������� �������� ���������� ��������� ����� �������������� ���������. ���������� ��������� �� ���.</EM></STRONG></P>
<P align=justify>������� ����� ����������, ��� ������������� ������, ��� � ����������� �������� ������������ ������. ��������, ������������� �� ���� ����������� ������ �������� �� ���������� ������ ������, � ������ � ��� � ���� ���������� ����������� ����� �� �����, ����� �� ���������, ����� ����� �� ���� ���������� ��������� � ������������, � ������ � � ������������, ���������������. � ����������� �������� ����������� ������ �� ����� �������� ������������ ������: ���� ���� ����� ���������� �������� �� � ����� ������-�����������, � � �����������. �, ��� ��� ���������������� ������������� ������ ���������� ��� �����, ����� ����� ����� ������� � ������������ �����, ����������� �� ����� ������ ����������.</P>
<P align=justify>�������������� ����� ����� ��������� ����� ������������� ������������ � �������� �������� � ���������� ����� ��� ������������ �� ������� ��������.</P>
<P align=justify>��������� ����� ����������� ��� ����������� � ����������� �������� �� ���������� ������. ������ � ��� ������� ��������, ��� ���������� ����������������, � ��������� �� ��, ��� ���������� �������� ������������� ������ ���������� �� ����, � ��������� ����� ���������, ������������� �� ������������ ����, ������������ � ��������������� ������ �� ������.</P>
<P align=justify>��������� � ����������� ����� ������������� ���������:</P>
<P align=justify>���������;</P>
<P align=justify>��������;</P>
<P align=justify>�������������.</P>
<P align=justify>��� ���� ��������� ����� ����� �����. ��-������, � ��� ������ ���� - �������������� ��������� ����������� � �����������. ��-������, ����� ������������� �� ������� ����������� ������������ ������ ����������� �� ����� ��������������, � ���������������� ����� (������� ��������, ����� ����������, ����� ����������). �-�������, ��������� ��������� �� ���� �������� (����������������� ����). ������ �������� ���������, �������� � ������������� ����� � ��� ������������ �������.</P>
<P align=center><STRONG>������� ���������</STRONG></P>
<P align=justify>�� ������� �������� ���� ������� (����������) ��������� ��������� �� ����� � �� ���� ������ ������� (����������) ������������ ����������� ��������.</P>
<P align=justify>������� ��������: ���������� � ����������. ����������� � ������ ������ ����� ���� ��� ��������, ��� � ����������.</P>
<P align=justify>������� ��������: ���������� ����������� ��������.</P>
<P align=justify>��� ������������ ���������� ����� ��������, ������� ���������, �������� ��� ���������� ����� � ����������� ����������. ���� ��������� �� ������ ���������� ��������, �� � ��������� � �����������, �������� � ��������� ������ � ����������, ���������� ������������� ������, �� ����������� ����������� ���������. ������ �������� ������������, �����������, ������������� ����� ������������� ��������� ��������� ������.</P>
<P align=justify>�������� ����� �������� ��������� ����������� � ���, ��� ����� � ����������� �� ������, ����������� ����������, ��������� ��������������� � ����������. ���������� ��� ���� �������� ��������� ������ ����� �����, ���������� ��� ���������� ������.</P>
<P align=justify>����� � ����������� ����������� ���������� ������ ������������� ����������� ��� ����������, ��������������� ������������� �� ���������� ��� ��� ���� ��������. ���, ���������� ������:</P>
<P align=justify>����� ��������� ������ ��� ���������, �� ����������� ������� �����������, ������������ � ������������ �� ��. 187 � 976 �� ��;</P>
<P align=justify>�������� ���������� �� ��� ���������� �������� � ���� ���������� ���������;</P>
<P align=justify>���������� ���������� ��� ����������� ��� ���������� �� �������, ����������� �� ���������� ���������;</P>
<P align=justify>�� ���������� ��������� ��� ��� ����������� �������� ��������� �� ��� ���������� ��� ����������� ���������� ���������� ������������, ���� �������� ������� �� �����, � ����������� ����� � ����������� �������������� ����������, ���� ��� ��������� �� �������� �������� ��� ��������� ���������.</P>
<P align=justify>���������� ����������� ������ ����������� ������������ (������������) �� ���������� ����������� ��������, ��������������� ��������� ���������.</P>
<P align=justify>���� ���� �� ������������� ���������, ���������� ������:</P>
<P align=justify>�������� ����������� ��������������, ���� ������� ��������� �������� ����������;</P>
<P align=justify>��������� ����������� ���������� ��������;</P>
<P align=justify>������������ ����������� ����������, ������������ ��� ����������;</P>
<P align=justify>��� ����������� ������� �� ����������� ��� ����������� �� � ������������ � ��������� ���������.</P>
<P align=justify>����� �������, ���������� �� ������ ���������� ����������� ��������������, �� � ������������ ��������, ���������� ������������: ������� �� �����, ���������, ������������� � ��������� ������ � �.�.</P>
<P align=justify>���� ����� �������� ��������� ����������� ����� ����������, � ����� ����� ���������� � ������������ ������ (�����������������), ������������ ������ ���������, � ��������� ����������� ��� �����������. ����� ������������� ������������� ������ ���������� �������� ��������� �����, ��� ��� ������� ����� ��������� �������������� ����������� ������� ��������. ����� �������, ��� ���������� ����������� ��������� ������� ��������� �� ��������: ����������� ����� ������ ������ ���, ����� ���������� ���-�� �������, ����������� ��������.</P>
<P align=justify>������������� �������� ��������� �������� ����������� �������� �������� �� ���������� �����������, ��������� ���������������� ���� �������� ������� ��������� �������� ��������� ���������� ������ ������������ �������� (��������, �������� ��������� �� ����� ������������� �� ��������� �������), �� ����� ������� ���������� ����� �� ������.</P>
<P align=center><STRONG>������� ��������</STRONG></P>
<P align=justify>�� ������ �������� ���� ������� (�����������) ��������� �� ��������� ������ ������� (���������) �� �������������� ��������� ���� ��� ��������� ������ �� ������ �����, �� �� ���� ���������.</P>
<P align=justify>������� ��������: �������� � �����������.</P>
<P align=justify>������� �������: ���������� ������.</P>
<P align=justify>� ������� �� �������� ��������� ��� ���������� �������� �������� ����������� ��������� ������ �� ������ �����, �.�. �� ������ �� ��������� � �������� � ������� ����� �������� �������� (���������). ������ ��������� ����� �������������� ����������� �������� �������������� �������� ����, ��� ������ ���� (���������� ��� ��������) �� ��������� �� ����������� ������ ��������������� � ��������� � ����� ����������.</P>
<P align=justify>����� � ����������� �� ������, ����������� ������������� � ������� �����, ����������� �����������. ��� ������� ��������� ���� � ��� �������, ����� �������� ��� ������ � ������ ��� ������� � ������� ����� � ���������������� ��������� �� �� ����������.</P>
<P align=justify>��� �� ��� � ��� ���������� �������� ���������, �������� ������ ������������ �� ������ ��������� ��������������, �� � ���������� ���������� �� ������� (� ��� ����� �� ������������� ������������� ������).</P>
<P align=justify>�������� ����� �������� ���������� ����� ���� �������� �������� ������������ ��������: ����������� ��������� �� ���������� �� ��������� ������������ ������ � ���������� �� � ����������� �������� ����. ��� ���� ����� ������������� �� ������� �� �������� ������ �������� � ��������. ��� �� ������������� ���������� �� ������ �������� ��������. ��� �� ����� ��� �������� �������� &#8220;�� ������&#8221;, ���������� ����� � ���� ������ ����������, � ����� �������� �� ������ ����� �� ������� � �� ������ �������� �� �������� ��������� ����. � ������ ��������� � ������ ������ ��������� ����������� �� ����������� � ����������� ������, �������������� � ���������������, ��������, �� ������������ ��� ������������ ���������� �������� (�����-�������, �������� � �.�.) ����� ���� �����������. ����� �������� �� ������ ������������.</P>
<P align=justify>����� �������, ���������� �������� �������� ������� ��������������, �������� �������� ���� �� ��������� ���������������� �����������, � ��� �����������, ������� ����������� ����������� (�����������) � �������� �����������. ������ ������� ����������� ������������ ������ ��������� �������������� ����������� �� ����� ���������� ��������� �������� (�����-�������) ����� �������������� � ������������ �����.</P>
<P align=center><STRONG>������� �������������</STRONG></P>
<P align=justify>�� �������� ������������� ���� ������� (�����) ��������� �� �������������� ��������� �� ��������� ������ ������� (����������) ����������� � ���� �������� �� ������ �����, �� �� ���� ���������� ���� �� ����� � �� ���� ����������.</P>
<P align=justify>������� ��������: ��������� � �����.</P>
<P align=justify>������� ��������: ���������� ����������� � ���� ��������.</P>
<P align=justify>������ ����� ������������� ����� ��������� �� ������ �����������, �� � ���� (�����������) ��������. ����� �������, �� ����� �� ������, �������� �� ������ ����� ��� ����������, ��������� �������, �� �, ��������, ������������� ���������� ������, � ��� ����� �����:</P>
<P align=justify>��������������� �������;</P>
<P align=justify>����������� �������� � �����������;</P>
<P align=justify>������ ������������� ��������;</P>
<P align=justify>���������� � ��� ��������������� ������������;</P>
<P align=justify>�������������� �������������� ������;</P>
<P align=justify>����������� ������� ���������� (����������) � ���������� &#8220;�� ������ ������&#8221;.</P>
<P align=justify>��� ����, ���� ���� ������� ����� �������� ��� ��������������� �����������, ����� ��������� ����������� ���� ������������� �� ���������� ����������.</P>
<P align=justify>������ ������ ��������������� ���������� �������� � �������� ������:</P>
<P align=justify>�� ����� ���������� - ����������� � ��������� �� ������ ���������� ���������;</P>
<P align=justify>�� ������ ����� - ����� � ����������� �� ������ ��������� �� ���� �����.</P>
<P align=justify>����� ������ �������� �������������� ��������������� �������� �������� �������������. �� ������� �������, ���, ����� �� �� ��������� ������� �� ���������, ������� �� ���������� � ���������� �������� ����� ���������.</P>
<P align=justify>������������ �������� ������������� �������� ����������� ����������� ����������� �� ����������� ������������� �� ��������� ����������� ��������� ��������� � ������� ��������, ������������ �� ������������ ����������, ���� �������������� �� ���� ���������� �� ��������������� ������������, ����������� ���, ��� ������������ ����� �� ���������� ��������. � ������ �������, �� ����� ��������������� ������������� ������ �� ��������� � ������� ������������ ����������� ��������� �������������, ������� ������ ����������� �� ����������, ��������� ��� �������� ����������� � �����������, ��������� � ��������. ������ � ������� ����� ������������ ��������� ����� ������������ � �� ��������� ������������� ��������� ����������� ����� ����������� � �������.</P>
<P align=justify>������� ��������, ��� ��������� ������������� ����� ������ �� ������� ��� ���������� ����� �� ����� ���� ���������� ������������ ���������� ������� ��� �����������. ���� �� �������� ����� ��� �� ���� �������� � �������, ��� ���������� ���������� � ���������� �� ��������.</P>
<P align=justify>������������ �������� ������������� �������� ����� ��� �������� ��������: ��������� ����� ������� � ����������� �� ������������ � ����������� ������ ��� ����-���� ��������� � �������� ������. ������ ����� ������������ �������� ���������� (���� ������������� �������� � ��������� � ���� ��������) ������ ���������� �����. ������ ������� ������������ ����������� ��� ���������� ��������� � ����������� ��������������� ��������� ��������� ��������� ����������: ��� ������������� ����������� ������ ���� ����� ���������, � �������������� ��������� ����������, � ������� ���������� ��������, �� ��������� ��� ����������.</P>
<P align=justify>����, ��� ���� ����� ������, ����� ������� ��������� ��������, ���������� ����� ������������, ����� ����� ��������������� ������� ����������� ����� �� ����, � �� ��� ����� �������� ����������.</P>


</body>
</html>

