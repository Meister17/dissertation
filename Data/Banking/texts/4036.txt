<!--
ID:37999648
TITLE:����������� ����������� ��������� ����������� �� �������� ������������������� ��������� ��������* �� ��������� �� 01.06.2007 �.
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:13
ISSUE_DATE:2007-07-15
RECORD_DATE:2007-08-07


-->
<html>
<head>
<title>
37999648-����������� ����������� ��������� ����������� �� �������� ������������������� ��������� ��������* �� ��������� �� 01.06.2007 �.
</title>
</head>
<body >


<TABLE cellSpacing=2 cellPadding=4 width=100% border=1>
<TBODY>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="26%">&nbsp;</TD>
<TD vAlign=top width="55%" colSpan=4><FONT face="Times New Roman CYR">
<P align=center>���������� ��</FONT></P></TD>
<TD vAlign=top width="12%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR">
<P align=center></P>
<P align=center>&#8470;</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR">
<P align=center>�������� ���������</P>
<P align=center>��������</FONT></P></TD>
<TD vAlign=top width="28%" colSpan=2><FONT face="Times New Roman CYR">
<P align=center>�� 01.01.2007</FONT></P></TD>
<TD vAlign=top width="28%" colSpan=2><FONT face="Times New Roman CYR">
<P align=center>�� 01.06.</FONT>2007</P></TD>
<TD vAlign=top width="12%"><FONT face="Times New Roman CYR">
<P align=center></P>
<P align=center>���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR">
<P align=center>�/�</FONT></P></TD>
<TD vAlign=top width="26%">&nbsp;</TD>
<TD vAlign=top width="14%"><FONT face="Times New Roman CYR">
<P align=center>����������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT face="Times New Roman CYR">
<P align=center>�������� ���</P>
<P align=center>� �����, %</FONT></P></TD>
<TD vAlign=top width="14%"><FONT face="Times New Roman CYR">
<P align=center>����������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT face="Times New Roman CYR">
<P align=center>�������� ���</P>
<P align=center>� �����, %</FONT></P></TD>
<TD vAlign=top width="12%"><FONT face="Times New Roman CYR">
<P align=center>(+/&#8211;)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR">
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR">
<P align=justify>�� 3 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%"><FONT face="Times New Roman CYR">
<P align=right>43</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right>3,6</P></TD>
<TD vAlign=top width="14%"><FONT face="Times New Roman CYR">
<P align=right>41</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right>3,5</P></TD>
<TD vAlign=top width="12%">
<P align=right>&#8211;2</P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR">
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR">
<P align=justify>�� 3 �� 10 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right>87</P></TD>
<TD vAlign=top width="14%">
<P align=right>7,3</P></TD>
<TD vAlign=top width="14%">
<P align=right>74</P></TD>
<TD vAlign=top width="14%">
<P align=right>6,3</P></TD>
<TD vAlign=top width="12%">
<P align=right>&#8211;13</P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR">
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR">
<P align=justify>�� 10 �� 30 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right>168</P></TD>
<TD vAlign=top width="14%">
<P align=right>14,1</P></TD>
<TD vAlign=top width="14%">
<P align=right>142</P></TD>
<TD vAlign=top width="14%">
<P align=right>12,2</P></TD>
<TD vAlign=top width="12%">
<P align=right>&#8211;26</P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR">
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR">
<P align=justify>�� 30 �� 60 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right>182</P></TD>
<TD vAlign=top width="14%">
<P align=right>15,3</P></TD>
<TD vAlign=top width="14%">
<P align=right>170</P></TD>
<TD vAlign=top width="14%">
<P align=right>14,6</P></TD>
<TD vAlign=top width="12%">
<P align=right>&#8211;12</P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR">
<P align=center>5</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR">
<P align=justify>�� 60 �� 150 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right>226</P></TD>
<TD vAlign=top width="14%">
<P align=right>19,0</P></TD>
<TD vAlign=top width="14%">
<P align=right>221</P></TD>
<TD vAlign=top width="14%">
<P align=right>19,0</P></TD>
<TD vAlign=top width="12%">
<P align=right>&#8211;5</P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR">
<P align=center>6</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR">
<P align=justify>�� 150 �� 300 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right>217</P></TD>
<TD vAlign=top width="14%">
<P align=right>18,3</P></TD>
<TD vAlign=top width="14%">
<P align=right>241</P></TD>
<TD vAlign=top width="14%">
<P align=right>20,7</P></TD>
<TD vAlign=top width="12%">
<P align=right>+24</P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR">
<P align=center>7</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR">
<P align=justify>�� 300 ��� ���. � ����</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right>266</P></TD>
<TD vAlign=top width="14%">
<P align=right>22,4</P></TD>
<TD vAlign=top width="14%">
<P align=right>277</P></TD>
<TD vAlign=top width="14%">
<P align=right>23,8</P></TD>
<TD vAlign=top width="12%">
<P align=right>+11</P></TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="26%"><B><FONT face="Times New Roman CYR">
<P align=justify>����� �� ������</B></FONT></P></TD>
<TD vAlign=top width="14%"><B>
<P align=right>1189</B></P></TD>
<TD vAlign=top width="14%"><B><FONT face="Times New Roman CYR">
<P align=right>100</B></FONT></P></TD>
<TD vAlign=top width="14%"><B>
<P align=right>1165</B></P></TD>
<TD vAlign=top width="14%"><B><FONT face="Times New Roman CYR">
<P align=right>100</B></FONT></P></TD>
<TD vAlign=top width="12%"><B>
<P align=right>&#8211;23</B></P></TD></TR></TBODY></TABLE><FONT face="Times New Roman CYR" size=2><SUP>
<P align=justify><FONT face="Times New Roman CYR">*</FONT> </SUP>�������� �������, �������� �������� �������� �����������, ������� � ����� ��������� ����������� � ������ � ����� ��������������� ����������� ��������� ����������� ����� ����������� ������ � �������������� �������������� ������.</P></FONT><I><FONT face="Times New Roman CYR">
<P align=right>"������� ����� ������", </FONT>2007<FONT face="Times New Roman CYR">, &#8470; 35</P></I></FONT>


</body>
</html>

