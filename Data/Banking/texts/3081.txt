<!--
ID:35367515
TITLE:����������� ����������� ��������� ����������� � �������� ������������������� �������� ��������* 
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:13
ISSUE_DATE:2005-07-15
RECORD_DATE:2005-10-04


-->
<html>
<head>
<title>
35367515-����������� ����������� ��������� ����������� � �������� ������������������� �������� ��������* 
</title>
</head>
<body >


<P align=center>
<TABLE cellSpacing=2 cellPadding=4 width=609 border=1>
<TBODY>
<TR>
<TD vAlign=top width="5%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="26%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="56%" colSpan=4>
<P align=center><FONT>���������� ��</FONT></P></TD>
<TD vAlign=top width="12%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="5%">
<P align=center><FONT>&#8470;</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>������������</FONT></P></TD>
<TD vAlign=top width="28%" colSpan=2>
<P align=center><FONT>�� 01.01.2005</FONT></P></TD>
<TD vAlign=top width="28%" colSpan=2>
<P align=center><FONT>�� 01.05.2005</FONT></P></TD>
<TD vAlign=top width="12%">
<P align=center><FONT>���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%">
<P align=center><FONT>�/�</FONT></P></TD>
<TD vAlign=top width="26%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>����������</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>�������� ���</FONT></P>
<P align=center><FONT>� �����, %</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>����������</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>�������� ���</FONT></P>
<P align=center><FONT>� �����, %</FONT></P></TD>
<TD vAlign=top width="12%">
<P align=center><FONT>(+/&#8211;)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%">
<P align=center><FONT>1</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=justify><FONT>�� 3 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>73</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>5,6</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>67</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>5,2</FONT></P></TD>
<TD vAlign=top width="12%">
<P align=right><FONT>&#8211;6</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%">
<P align=center><FONT>2</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=justify><FONT>�� 3 �� 10 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>133</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>10,2</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>123</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>9,5</FONT></P></TD>
<TD vAlign=top width="12%">
<P align=right><FONT>&#8211;10</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%">
<P align=center><FONT>3</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=justify><FONT>�� 10 �� 30 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>232</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>17,9</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>230</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>17,9</FONT></P></TD>
<TD vAlign=top width="12%">
<P align=right><FONT>&#8211;2</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%">
<P align=center><FONT>4</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=justify><FONT>�� 30 �� 60 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>225</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>17,3</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>224</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>17,4</FONT></P></TD>
<TD vAlign=top width="12%">
<P align=right><FONT>&#8211;1</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%">
<P align=center><FONT>5</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=justify><FONT>�� 60 �� 150 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>211</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>16,2</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>214</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>16,6</FONT></P></TD>
<TD vAlign=top width="12%">
<P align=right><FONT>+3</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%">
<P align=center><FONT>6</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=justify><FONT>�� 150 �� 300 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>191</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>14,7</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>195</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>15,1</FONT></P></TD>
<TD vAlign=top width="12%">
<P align=right><FONT>+4</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%">
<P align=center><FONT>7</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=justify><FONT>�� 300 ��� ���. � ����</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>234</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>18,0</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>235</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>18,2</FONT></P></TD>
<TD vAlign=top width="12%">
<P align=right><FONT>+1</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="26%"><B>
<P align=justify><FONT>����� �� ������</FONT></B></P></TD>
<TD vAlign=top width="14%"><B>
<P align=right><FONT>1299</FONT></B></P></TD>
<TD vAlign=top width="14%"><B>
<P align=right><FONT>100</FONT></B></P></TD>
<TD vAlign=top width="14%"><B>
<P align=right><FONT>1288</FONT></B></P></TD>
<TD vAlign=top width="14%"><B>
<P align=right><FONT>100</FONT></B></P></TD>
<TD vAlign=top width="12%"><B>
<P align=right><FONT>&#8211;11</FONT></B></P></TD></TR></TBODY></TABLE></P><I><SUP>
<P align=justify><FONT face="Times New Roman CYR">*</FONT> </SUP>�������� �������, �������� �������� �������� �����������, ������� � ����� ��������� ����������� � ������ � ����� ��������������� ����������� ��������� ����������� ����� ����������� ������ � �������������� �������������� ������.</P><FONT></FONT>
<P>"������� ����� ������", 2005, &#8470; 24</P></I><I></I>


</body>
</html>

