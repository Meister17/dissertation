<!--
ID:36082310
TITLE:�������������� ������� � ���������� ��������� �������� ����������� ������� �������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:�.�. �������, ����������� ������������ ������������� ����� ���������� ������������ �.�. ����������, ����������� ���������� ������ ������������� ����� ���������� ������������
NUMBER:4
ISSUE_DATE:2006-02-28
RECORD_DATE:2006-04-10


RBR_ID:2039
RBR_NAME:���������� �������
RBR_ID:4928
RBR_NAME:��������� � ������� ��������
-->
<html>
<head>
<title>
36082310-�������������� ������� � ���������� ��������� �������� ����������� ������� �������
</title>
</head>
<body >


<P align=justify>�������� ����������� �������, ����������, ������ �����, ��� ��������� ��� �������������� ���� � ���������, � ���� ������������� ������ ������� � ���������� ������� � �� �������, � ��� ����� � ������ ��������������� ����� ��������, ����������� ����������, ��������, ������������, ���������� ��������������, �������������� ���������� �������������� ����� ���������-�������������� �������� ������ � �� ��������. ��� ���� ��������, � ����� ������������, � �������� ���������-������������� �������� � ������ �������� ����������� ���� � �������� �������������� ������������� ������������� �� ������������ ������ ������������������� ��������� ���������-�������������� �������� �, ��� ��������� �� �����, ��������� �������� ����������� �������.</P>
<P align=justify>������� ������ �������� ����������� �������, ������������� � ���������� ���������-������������� ��������� �������, ������� ��������� ��������� ���� ���������������� ������: ������������ ������� ��������������� ������, ��������������� ���������� ����� ������, ��������� ����������� � ������� ���������� ����������. �������� ���������� ������ ����������� �� ������, �� ��� ������, �������� ���������� ������������ ������������ ��������� �� �������� ����������� ������� � �������.</P>
<P align=justify>��������������, ��� �������� �������� ������ ��������������� �� ����������� � ���������� ���������-�������������� �������� �������� ���������� ���������, � �������� ��������� �������� ����������� ������� ������ �������������� ���������� ���������-������������� ����� �������, ���������� ������� ���������� ����� ��������� �������.</P>
<P align=justify>��������, ��� ������� ������� ��� � �������������� ����� � ����� ������������ ���������, ��� � � ����������� ���������� ��� ����������, ��������� �������������� ������, ���������� �� ���������� �������� �������� ��������� ���������, �������� ����������� �������. � �� �� ����� �������� �������� ����� �������������� ���������� ������ ����� ����� ��������������, ������� � �� ������������, ���������� �� ����� ������������� �������� � ������ � ����������� ���������.</P>
<P align=justify>�� �������� � ������������� ��������� � ���������� ����������� ���������� �� ���������-�������������� ��������, � ��������� ������������ ��������� �� �������� ����������� ������� �������������� �������������� �������� ��������� �������:</P>
<P align=justify>&#61548; <U>�������������</U>, ��������������� ����������� ��������� � �������� �������� ����������� ������� ���������;</P>
<P align=justify>&#61548; <U>�������</U><B><I>,</B></I> ������������ �������� ����, ������, ��������� ���������� ���������� ���������;</P>
<P align=justify>&#61548; <U>��������������</U>, ���������� �������� ���������� � ����������� ����������� �������� ������������� ����������� �������.</P>
<P align=justify>������� ��������������<B><I> </B></I>������� �������� ������ ��������� ����������� ������� ������� � ������� ������ � �������� �������������� ������ �� ��� ������ �� ������� ��������� � ��������, ��������� �������, �������������� ��� ������������ ����������������, ������� � ���������� ��������, ����������� ������� �� �������������� ������� ����������� ��������, ����������� ������� ���������� ����� � ����������������� ��������� ��� �������� ����������� �������, ��������� ��������������� � ������������� ����� ����� ���������� ����� � �������.</P>
<P align=justify>������ ������ �������������� ����������� �������� ������������ ������ �� �������������� � ������������ ���������, ��������������� ������������� ���������� ������������� (����������������� ������������ ����������� ��������), ������� ������� ����������� ������� �� ��������� ������� (��������� �������/ �������� � �������� ������������� ��������), ������������ ����� (������� ������������ ���������� ��������) � ��.</P>
<P align=justify>������������� ����������� ��������� ����������� �� ���������� ���������� ���������� ������������ ������ �� ����������� ����������� �������� ��������. ����� �� ������ ������� �������������� ������-������ ��� ���������-������� ����������� �������� ����������� ���������� ����� &#8211; ��������� ����������� &#8211; ����������� �� ����������� ���������� ���������� ������������ ��������� ��������� ����� ���������� ��������. � ����������� �� ����������� ����������, ������������ ���������� ������ �������� ����������� ���������� ��������. �� ���� ������ ����������� ��������� �������� ������� (���� �� ����������), ��������, � ������������� �������� � ������� ����� ��������� �����������, ����������� �� ����� ���������� ����� ��������������� ��������� �����������, �������������� ������������� � ���������� �������� ������������ ������������� ���������� �������������, ���������� ������������ ��������� ����������� � ��������������� �������� �� ������� �������, �� �������������� �� �����-���� �������� ������������ ����������� �����.</P>
<P align=justify>� ������� ����� ������������ �������������� � ������������ ��������� �������� ����������� �������. � ��������� ����� ����� ��������� ����������� "���������� �������� ����������� ������� ���������� ���������".</P>
<P align=justify>�������� ����������� �������� ����������� ������� ������������ ������ �� ��� �������� �������, ���������� � �������� ������� ��������� ���������� �����. ������������� � ����������� ������� ���������� ������������ ����� �������� ��������� ������������ ����������� ��� �������� �� ������������� �����������:</P>
<P align=justify>&#61548; ����������������� ��������;</P>
<P align=justify>&#61548; ��������� ������ �������������;</P>
<P align=justify>&#61548; ���������� ��������� ����;</P>
<P align=justify>&#61548; ��������� �������������� ���� (������������� ���������� � ����������, ���������� � ��������� �������� ���������� �����);</P>
<P align=justify>&#61548; ����������� ���������� ������������;</P>
<P align=justify>&#61548; ������������ ������������ �����;</P>
<P align=justify>&#61548; ������� ����������� � �������� �������.</P>
<P align=justify>��� ���������� ��������� ���������-�������������� �������� ���������� ���������, ��� �������� ����� ��������� ����������� ��� �������������� �������� �������� ���������� �������. ������� ��������� ����������� � ���������� ��������� �������� ��������, ������������� �� ��� ������� ������������� ����������� ���������, �� ������ ��������� � �������� �� ��������������. �������� �������������� �� ������� ���� ���������-�����������������, �� ����������� �� ����������� �����, �� ����, �������� ������������ ������� ������������������� ������������ � ������ ������ �������������� ������ ��� ��������� ������������ �������, ���������� �� ���� �����������.</P>
<P align=justify>����������� ����� ����������� �������� ����������� ��������, ��������� � ����������� ������� ���������� �������, � �������������� ��������� ������������:</P>
<P align=justify>&#61548; ���������� ��������� ����� ��������� �����������, ����������� ������������� ������� ���������, ������ ������� �� ��������� ��������������� ��������� (� ����� �������������� ��������������� �������� � ����������� ��������, �������� �� ���������� ������� � ���������� �������, ������������� ���������� ����� �������� ��������� � �.�.);</P>
<P align=justify>&#61548; ���������� ���������� ����� ���������� ������������� � ������������� �������� ������ � ������������� ���������� �������������� ���������� � ������� ������� ��������� ����������� � ���������� ��������� �������� ��������. ����� ���������� �� ������ ��������� �����������, �.�. ������ ����������� �� ������������������ � ������������������� ��������;</P>
<P align=justify>&#61548; ����������-�������� ���������, �.�. �������� �� ������������ ������ �������� ����� ������� ��������������� ������, �������������� �������� ������� ��� �������� ��������� ����� ���������� ��������, ��������������� ���������� ����, ��������� ��������� ���������� ����������, ����������� ����������� ���������� � �.�.</P>
<P align=justify>������������ ���������� � ����������� ���������� ������������ ��������� �������� ����������� ������� �������� ������ ��� ������� ��������� ������� ����������� ����������. ��� ���� ��� �������������� �������������� ������� � �������� ������������������ �������� ���������� � ���������� ��������� ����������� ����������� ���������� �� ������������ ������. ����� ����, ���� ��������� ����� ������������ �������� ��� �������� ������������ ���������� ���������� ��� ��������� ����������� ������������ ����������������� ����� ������� ��������� � �������������� ����� ����������� ������������� ����������� �����, � ������ �������, � ���������, �������� �������� ���� ��������� �������, ���������� ���������� �� �������������� ������������, ���� ��� ���������������� ������������, ���������������� ����������� ���� � ������ ����, �������������� �������� � ���������� ����������� �������.</P>
<P align=justify>����� �������, ����������� �������� ����� �������� ������ ���� ����������� ����� �������� ������, �������� �������, ������������� ������������� � ������� ����������������� ���������, �������������� �� ������������, ������������ �� ���������� �������� � ���������� ����������� �������, �������� ��� ���� � ��������� �������.</P>
<P align=justify>��� ������� ���������� � ���������� ������������ ��������� �������� ����������� �������, ������ ����� �������� �����, ��������� ������ ������������ ��������� �����:</P>
<P align=justify>&#61548; ������ ��������������� ������, ����������� ������� � ���� ��������� ����������� � �������� ������������ � ���������� ��������� �������� ����� ������ ������������� � ���������� � ��������� �������� ���������� � �� ����������� �������, ��� ������������ ��������� �������� �������� ��������� � ��������� ����������� �� ����� ����������� � ������� ����������������� ������;</P>
<P align=justify>&#61548; �����������, ����������� ���� �������� �������������� ����������� ������������ ������������ �������� ������������� ���������, ��� ������������ �� ��������;</P>
<P align=justify>&#61548; ���������� ���������� � ���������� ��������� ������������ ������� ��������������� ����� ����� ����������������� ��������� � ���������� ���� ��������������, ���������������, ����������� �����������;</P>
<P align=justify>&#61548; ���������� ��������� ����������� � ��������� ���������� ������� � ����� �������� � ������ � ���������-�������������� �������� ������������ ��������� �� ���������� ��������������� � ������������ ����������� ����������;</P>
<P align=justify>&#61548; ����������� � ��������� ���������� ���������-������� ����������� �������� ��������� ������� � ������ �� ���������� ������ ������������ ����������������� ���������� �������������, ��������������� ������������ � ���������� � ��������� ������������, ���������� ��������� �� ������������;</P>
<P align=justify>&#61548; ������������� ��������� ������ � ����������� ����������� ����������� ������ ����� ������������� ��������� ����������� � ���������� ����� �������������� ������, ���������� �� �� ����������, ��-������, ����� "�����������" �, ��-������, ����� ������������ �����;</P>
<P align=justify>&#61548; � ������� ������-������������ ����� ������� ����������� �������������� ������������ ��������� �����������, ��� �������, �� �����������, ���������� ����������� ������- � ������������ ����� � ������;</P>
<P align=justify>&#61548; ����� ����������� ������� �������� ����������� ���������� ������ �������� ����������, � ��� ����� �������������� ������-������ ��������� �����������, ������ �������������� ������ � ��������� ����������� ������ ��������� �����������; ������ ��� ����� ������������� �� �������� �������� �������� ������-������������ � ������������� ���������� ������-�����, ���������� ���������-���������������;</P>
<P align=justify>&#61548; ���������� ������������� ������������� ��������������, �������� �������� � ��������� ������������ �������������� �������������-�������������� � ���������-������������� ������ ��������������� ����������;</P>
<P align=justify>&#61548; ���������� �������� ������ ��� �������� �������������� ���� ����� ������ (��� ������, ����������� �� ������ �� �������� � ���������� ���������� �������) ����� ����� �������� ������� ��� ��������������� ���������� � ��������� ���������, ����������, ����������, ������� � �������� ���������� ������������ �������� �������� ����������� �������, ��� ������������� ��������� � ������� � �������� ��������������� ���������� ����� ������.</P>


</body>
</html>

