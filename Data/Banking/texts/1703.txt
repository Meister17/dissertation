<!--
ID:36678517
TITLE:���������� ��� �������, � �� ��������
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:������ �������
NUMBER:7
ISSUE_DATE:2006-07-31
RECORD_DATE:2006-09-13


-->
<html>
<head>
<title>
36678517-���������� ��� �������, � �� ��������
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgwhite>
<TD></TD></TR>
<TR class=bodycolor>
<TD>
<P align=justify><I>������ ��� �� ���������� ������������� �������� &#171;����� ���������� ����������&#187; ���������� ���������� ������ �� ���������� � ��������. ��� ���������� ����� �������, ��� ��������� ������� �������� &#8212; �������� ���� � ���������� �������� ���� � ��������� ����������� �������. � ���� ���� �������� ��������� ����������� ���������� ������� (������ ��� ���� �������������� ���� �����������) � &#171;�������� ��� �������� ��� �����&#187; ��������� ����� 140 �������������� ������ � ��������������������� ��������. </I></P></TD></TR>
<TR class=bgwhite>
<TD>
<P class=Main align=justify>�� �������� �� ������� ������ ����� ����� � ��������� ���������. ������ ��������� ��� �� ������ ��� � ������ ���� ����� �����. �������, ������ �� ����� ������������ �������� ����� ���������, ��� ������ �� �������� � �����. 
<P class=Main align=justify>�� �����, ��������� �� ������� �������, ���������� ������ ���� ������������� ����. �������� ��������� ���� ������������ �������� ��������� ����������� �� ��� � ����� �������� ������ � �������������� �����������. �������������� ������ �������� ������ IT-������������ ������ ������ �� �������� � ���������� �����������, ������� ����������� ������ ���-�� � ���� ��� �����������. ����� ���������� ���������� ����, ������, ���������� � ������� ���������������� ���� �� ����������, ���������� ���������� ������ �� ��������� � ��������� &#8212; ��� ���� ������� ���������� � ������������������ �������������� �����. ������� ������������� ���� ������ ������ � ���������, � ������ ��� ���������� �������� �������������� ����������. �������-�� ����������� � ������������ ������ ������, ��� ���������� IT-������������� � ��� ��� ������������ ��� ��������� � ���������� �������. 
<P class=Main align=justify>� ������������ � ������ �������� ���������� ���� ����������� � ���������� � ��������. ����������� ������� ������ �� ������ ����. �� � ��� ��������������� ����� ��������� � ���� ������? � ��� ���������� ������ ��������� ����������� ����� �������. 
<P class=Main align=justify>��� ������ ��� ����� ��������� �������, ������������ ��������� ���. � ����� ����������� �� ������� � ���, ��� ������-��������� ���������� IT-���������. � �������� ��������������� ����� ����������, ������� ������ ������������. ����� �� � ����� �������� �������� �� ����������. 
<P class=Main align=justify>�������� ��������� ������ �������� �� Baring Vostok Capital Partners &#8212; ������ �������� ���. 
<P class=Main align=justify>��� ���� ������ �������� � �������� ��� � ��������� � �����������, � �� � ����� ������������, ����������, �������������� � �������! 
<P class=Main align=justify>���� ����� �� �������� �������� ������������� ������������������ IT-��������, c�������� ������ �����, ��� ������������ ������������� ���������� � ���������� �������� � ����� ������ ��� ����� �������� �������. 
<P class=Main align=justify>� �����, �������������� ���������� ������� � ������ ������� ������������, ������� ������ ���������� ���������� ������������. �, ����������, � ���� ���� ������� ������ ���������� ����������.</P></TD></TR></TBODY></TABLE>


</body>
</html>

