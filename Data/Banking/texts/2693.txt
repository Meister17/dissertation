<!--
ID:37754599
TITLE:����-����� ������� � ������
SOURCE_ID:11528
SOURCE_NAME:���. ����� � ������� ���
AUTHOR:
NUMBER:5
ISSUE_DATE:2007-05-31
RECORD_DATE:2007-06-06


-->
<html>
<head>
<title>
37754599-����-����� ������� � ������
</title>
</head>
<body >


<H1><FONT size=3>� ���������� ������ &#171;���&#187; ����������� �������� ��� ��������� &#171;�����-������� �� ������ ������������ ������&#187; � �� ������ ��������� ������� ������������� ����������� ���������� ������ �����������. �� ������ ������, ������ ����� ��������� ���� ����� ������ ������� �� ������ � �������� �� ����-�������� ������ ����������� ������ ����� �� ����� &#171;������������� ����� ��������� �������&#187; ���������� ����������: &#171;� ����� � ������������ ����������� �������� � ���������� ����� � ���������� ����������� � ������������� �������� �������, ���������� ����� ���������� � �������� ������� � ����� �����. ���� ������������� ������. ����������� ������������������ � ���������� ����� ��������������� �����&#187;. � ������ ���� ������� ���������� �������� � ��������� ����� ��� �������������. </FONT></H1>
<P align=justify><FONT>���� � ���, ��� ������ �� ����������� ������������� �������� ������� �������� ������� �������� ��������, �� ������� ����� �������� ��������, ��� ��������� �� ������������� � �����, �� ����� ����� ������������� ��������. � ���������, ����� ������ ������������ ����� � �������������� ����������� ����������� � ����������� �������. ��� �, ������� ����, ��� ����� ������������ ���������� �� ��� ��� ����� � �������������� ������������. �������� �������� �� ��������, ���� �������� � �������, ���� ��������� ��������� � � ��� ��� ������� � ���������� ����������. �� �������, ��� �������� ���������� �������� ����� ������� ������ �����. </FONT>
<P align=justify><FONT>���� ��������� ����-�������� ����������� ����������������� ����� � ��������� &#8470; 5, ��� ���� �������������. ��� ������ �� ����� ������ ��������� � ���� ��������, ���������� ����� ������������ � ������. </FONT>
<P align=justify><FONT><STRONG>������ ����������, </STRONG><STRONG>��������� ��������, </STRONG><STRONG>���� �������</STRONG>, <STRONG>����� �������, </STRONG><STRONG>���� ���������</STRONG> </FONT></P>
<H1><FONT size=3>��� ������� �� ������������������ ������ </FONT></H1>
<P class=avtor-name align=justify><FONT>������ ����������, �.�. ������������ ��������� ������������ ������������� ����� ��� &#171;������������ ���� ��������� �������&#187; </FONT>
<P align=justify><FONT>���� �� ������ www.nat-bank.ru �� ���������� � ������� 2007 ����. ���� ��������������� ������������ ������ � ����� �������� 2006 ����. ��������� ���� �������� ��������, ���������� �������. ���� �� ��������� ����� � ������ ���������� �����, �� ���������� ������������ ����������, ��������� ���� �� �������� ���������� ������� ������������� ����������� �������. ���� ����� �� ���������� ����������� � ������� ���������, ������� ���������� ���� ����. </FONT>
<P align=justify><FONT>�� ����� ����� ��������� ����� ����� ��������, ���������������� ��� ���������������. ����� �������� �� �������� � ������� �� ��� ����������� ��� &#171;����&#187; (���). </FONT>
<P align=justify><FONT>�������� �� ����� ����� ���������� � ����-��������� ������ �� ������������� �������� �������, � ��� ����� � ������� � ����� ������� ��������. </FONT>
<P align=justify><FONT>�� ����� ������� ���������� ��������, ������� �� �������� ���������� �����. �� ��� �� ����� ��� &#171;����&#187; �������� �������������. </FONT>
<P align=justify><FONT>� ������ ����� ���� ���������� �������� ���� ������, � ����� ����� � ���������� ���������� ��� �. ������ ��������� � ������ ����� �������� ���������. </FONT>
<P align=justify><FONT>���� ����� ��������� � ���������� � ���������� � ����������� ������ �����. ������ ����� ���� ����������. ������ ����� ��������� ���� ���� ���������� ������ �� �������� ������� ����������, ����� ���� ������� �������� �� www.nbvk.ru. </FONT>
<P align=justify><FONT>������� ��� ��������������� ��������, ��������� ���� ������� ��������� �����. �� �� �� �����, ��� �� ���� �����. ��������, ��� �� ���� ������ ������� ������������������ ������. </FONT></P>
<H1><FONT size=3>��������� ������� ��������������� ����� ��������� ������������ ������</FONT></H1>
<P class=avtor-name align=justify><FONT>��������� ��������, ����������� ��������� &#8470;5 ����������� ��� ����� ������ </FONT>
<P align=justify><FONT>��� ���������� ��������, ����� � ����� ������������ ������ ���������� ����������� ��������. ����������� ���� ����� ��������� ��������� ������������� ������������ � ������������� �������� � ��������� � �������, ��������������� ������, ������������ ����������������� � ����, ��������� �� ���������� ������ ��������, ��������������� ���� � ��� ������������. </FONT>
<P align=justify><FONT>� ���������, � �������� ��������� �����������, ��������� ������� �������������� ����� ��������� ������� ��������������� ����� ��������� ������������ ������. � ��� ����� ���������� ��� �����������. �������� ��������� 9 ��. 4 ������������ ������ �� 26.07.2006 </FONT>
<P align=justify><FONT>&#8470;135-�� &#171;� ������ �����������&#187; ��� ���������������� ������������ ���������� ����� �������� ������������� ��������� (������ ���), ������� ���������� �� ��������� ����������� ��� ������������� ������������������� ������������, ������������ ���������������� ���������� ���������, ������� �������� �������, ����������� �����������������, ���������� � �������������� � ��������� ��� ����� ��������� ������ ������ ������������� ��������� &#8212;&nbsp;����������� ���� ������� ��� ����� ������� ���� �� ������� ���������. </FONT>
<P align=justify><FONT>���������� � ���� ���������� ������ ����� ������� ����� �� ���� ���������������� �����������. �� ����� ������ �������� ���������� ��������� �������� � ������, ������� ���� ����� �� ��������� � ���� �� ����� �� ��� ��������. ����� �� �������� �������� ������������ ����� ������� ����������, ����������� �� ����� www.nat-bank.ru. </FONT>
<P align=justify><FONT>�� ����� ������������ ��������� ��� &#171;������������ ���� ��������� �������&#187;. ��� ���� ��������, ����������� �� ���: �������, ��� � �������� ������������, ���������� ��������, ���������� ���������� &#8212; �� ������������� ����������������. ����� ����, �� ����� �������, ��� ���� ������ � ������� ����������� �������, � �� ��� ����� ������������ ���������� ������� ������� �������. �� ����� ���� ���� ���� �� ����� �������� �� ����������� �� ������ �������� ������� ���������� ���, ��������������, �� �������� � ���������� ���. </FONT>
<P align=justify><FONT>� ����� � ���������� ���������������� ��� &#171;����&#187; (���) ��������� � ��������� &#8470;5 ���� � �������, ��� �������� ��������� � ������ ���������� ��� � �������� ������� ������ � ������ ���� � �������� ���������, � ����� ������� ���� �� ��������� � ��������� ������������. ����� ���� ������������� ����� ��������� ������ �������-���������� Slavhost, ��� ���������� ����. ��������� ����� ���� �� ����. ������ ����� ����� ��������� ���� ���� �������� �� ���� �� ������ �� �������� �������� Mastername. </FONT>
<P align=justify><FONT>����� ����, �� ���������� �����, ����� ������������� ������������ � �������� ����������� ������������� � ���������� ������������ �����, �������� ����������� �� �����, ��������� ������������ ��� &#171;����&#187; ������ ������������� ����������� ����, ������������� �� ������ www.nbmc.ru </FONT></P>
<H1><FONT size=3>����� ������ ����������� �����</FONT></H1>
<P class=avtor-name align=justify><FONT>���� �������,&nbsp; ������������ ������ ������������ ������ �� ���������� ������ </FONT>
<P align=justify><FONT>���� ���� ����� � ������� ��������, ������ ����� ������ ������������ ����� ��������� ��������, ��������� � ������: �������� �� ��� � ��� ����������������? ������ ��������, ��� �� ���� ����� ����, ������� �� ����� � �������� �������� ���������. ���� �� �� �������� ������������ ����������� � ���������� ���������, ����� ���������� �������, �� ����� ����� ����. ����� ���� � ������������������ ������, ������ ��������� � �������� ����������� � ��������, ��������� ��� ���������, ��� � ������������ ����� �����, � �������� �������� � ��������� ���������������. �� ������ ������������������ ������ ������� � ������� ����, �������� ������ ���� �������� �������� � ������� ������� � ���, ���������� �� ��������� ����. ������ �� �������� ��� ��������� ������������ �� ����� � �����. �� ��� ����� ���� � ����� ����������� ����������� ������������, ����� ������� ����������� ����������, �������� �������� �� ��������� ���������. </FONT>
<P align=justify><FONT>����� ��������, ��� ���� ���� ���, ������, �������� � ����������� ����������� � ��������, �� ��� ����� �� ���������������� ����������� �������������� � ������������� ��� �������������� ���������. </FONT></P>
<H1><FONT size=3>����� ��������������� ��������, ���� ������ ���������</FONT></H1>
<P class=avtor-name align=justify><FONT>����� �������, ����������� �������� ������������ ���� &#171;�������� ������&#187; </FONT>
<P align=justify><FONT>� ����������� ����� ������ �������� ������������ ���������� �������. ������ ���������� ��� � ������� �������� ������� �� �������� ����. � � ����� ���� ������ ����������� �������. </FONT>
<P align=justify><FONT>��-������, ��� ���� ����� ���-�� ��������������� �������� ���������������, ���� ��� ������� ������ ��������� �� ���������. ��� ����� ���� � ��������� �����������, � ������ ���������� ��������, ������������ �� ������ ������� ��������� ����� ��� ������������� ����������� �������� ������������������ ������� � ��� ������������. �������� ���������� ���������� ���� ����, ������ ���������, ��� ���-�� �� ���������������� ����������� ��� ����� ������� ������� ���� &#171;�� ����&#187;. </FONT>
<P align=justify><FONT>��-������, �� ����������� � ������� �������� ���������� ��������� ���� � ���������� ����� ��� ��� ������� ������������ ������ �� �������������. ����� ����, &#171;�����������&#187; �� ����, ��� � ����������� ������ ������ ���������� &#171;�����&#187;, �� �������� �� ���� ����������� ���������. ��������, ������ ����� ������� ������������������ �������, �� ����, ������������� ���������������, � ����������� ��������������� �� ���������� ������������� �������������� ��������. </FONT>
<P align=justify><FONT>������ � ��� ������ ��� ����, ��� ����� �������� ���� ���������� �������� ������ ������� � ����������� ������������� ��������� ����� (��. 180 �� ��) ���������� �������� ���� � ������������, ��������������� �� ������� ������������� ��. 174 �� ��. &#171;����������� (���������) �������� ������� ��� ����� ���������, ������������ ������� ������ ���������� ����&#187;. </FONT>
<P align=justify><FONT>����� (��� ������� ������� � ���������� �������� �������) � ��������� ���������������. ��� ������������� ������, ���������� ��� �������� ��������-������, ���������� ������� � �.�., ����������� ����� �������� �����������, ������� �������� ����� �� ���� ���������. ���� ���� ������ ��������� � ����� ���� ���������������� �� ���������� ���. ����� ����, ����������� ����� �������� �������� &#171;��������������� �����������&#187; � � ������ ������� ��������� &#171;�� ���������� �������������&#187; �����������. ���� ���� �� ����, ����� ���������� ���� ������������ �� �����, ���� �� ���-�� �� ����������� ��� � ��������. </FONT>
<P align=justify><FONT>������ ������, ��� ��� ��������� ������ � �����������, ��������� ��������� ������������� ������, ������� ���� ������������������ ������� ��������� �� �����. </FONT>
<P align=justify><FONT>�������, ��� ���� ���� ��� �� ������� ���������� � �� �������� ������ ��� ������������ ��������, ��� ���� �� ���-������ �������. </FONT></P>
<H1><FONT size=3>������ ��� ����� �������� </FONT></H1>
<P class=avtor-name align=justify><FONT>���� ���������, ��������� ���������� �������� � ������� �� �����&nbsp; ���������� ����� ��� </FONT>
<P align=justify><FONT>���� ����� ��������, ��� ������ �������� �������� ��������� ����������� � ����� ������� ����� ������ ���, ��� ��� �����������, �� ����������� ������� ����� �������� ��������� � ���. �������� ����� ����� �������� � ������������ � ������� � ���������������� ����������� � ������� ���������� ������������ �����. ����� ����� ����� ��������������� � ���, �� �������� � ������ ��������������� ������. </FONT>
<P align=justify><FONT>���� �� ������������ �� ������ ����� ���, ��� ������ �� ���������, ����� ���� ���������� � �������� �� ���� ��������� �������. ����� ��������������� ���������� ������������� � ��������������� ���������. ������ ����� �����, ��� �� �� �������� ������������ ��������� �������������, &#8212; �� ����� ������ �������������� ���� ���������������� �����������. ������� �����, ������� �� ��������� ��������������� ����������, ���������� ������ ���������, �������� ���. </FONT></P>


</body>
</html>

