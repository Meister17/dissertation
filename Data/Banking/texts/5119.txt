<!--
ID:36057196
TITLE:����������� �� ���-����-3
SOURCE_ID:3126
SOURCE_NAME:������� ����������� ����
AUTHOR:
NUMBER:1
ISSUE_DATE:2006-01-31
RECORD_DATE:2006-04-04


-->
<html>
<head>
<title>
36057196-����������� �� ���-����-3
</title>
</head>
<body >


<P align=justify><EM>������������� �������������� �����������-����������� � ���-����� ����� �������� ��������� ���������� ������ � ���������� ���������� � ���������� ������. ������������ � ������� ���� ����������� ���������� � ������ �����, ����������� ����� �������������� ������ ���������� ������, ��� ��� ������������� ����������, ��� � ��� ������ ����������� ���������� ����������� �������� � ������ ������������� ��������� �� ����������� �������.</EM></P><B>
<P align=center>III ������������� �����������-�����������<BR>&#8220;�������������� �����������<BR>� ���������� ������� ������ � ����� ���&#8221;</P>
<P align=center>5-7 ������ 2006 ����, ���-����, ����� &#8220;�������-�������</P></B>
<TABLE cellSpacing=1 cellPadding=7 width="100%" align=center border=1>
<TBODY>
<TR>
<TD vAlign=top colSpan=2><B>
<P align=center>5 ������ (�����)</B></P></TD></TR>
<TR>
<TD vAlign=top colSpan=2><B>
<P align=center>��������</P></B><I>
<P align=center>����������� ������������ &#8212; ����� ��������,<BR>��������������� ������ ��� �� �������� ���������� ����� (FSVC)</I></P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>09.00</P></TD>
<TD vAlign=top width="82%">
<P align=justify>����� ��������, FSVC</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>09.10</P></TD>
<TD vAlign=top width="82%">
<P align=justify>����������� ������������� ���</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>09.35</P></TD>
<TD vAlign=top width="82%">
<P align=justify>������ ������, �� ��</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>09.55</P></TD>
<TD vAlign=top width="82%">
<P align=justify>��������� �������, ���������� &#8220;������&#8221;</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>10.15</P></TD>
<TD vAlign=top width="82%">
<P align=justify>������ ���������, ���������� &#8220;���-�������&#8221;</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>10.30</P></TD>
<TD vAlign=top width="82%">
<P align=justify>������� �������</P></TD></TR>
<TR>
<TD vAlign=top colSpan=2><B>
<P align=center>������������ �������������� � ���������� �����<BR>������ � ����� ���</P></B><I>
<P align=center>������������ &#8212; ��������� �������</I></P></TD></TR>
<TR>
<TD vAlign=top width="18%" rowSpan=6><B>
<P align=center>���� ��� ����������</B></P></TD>
<TD vAlign=top width="82%">
<P align=justify>����������� ������ � ��������������</P></TD></TR>
<TR>
<TD vAlign=top width="82%">
<P align=justify>����� ���������� ���������������</P></TD></TR>
<TR>
<TD vAlign=top width="82%">
<P align=justify>������ ��� ���� ������� � ��������</P></TD></TR>
<TR>
<TD vAlign=top width="82%">
<P align=justify>����������� ������� ���������� ��� ���� (����������� ��������, ��������)</P></TD></TR>
<TR>
<TD vAlign=top width="82%">
<P align=justify>��������� IPO (Initial Public Offering &#8212; ��������� ���������� �����)</P></TD></TR>
<TR>
<TD vAlign=top width="82%">
<P align=justify>��� ������ ���������� ���������� (����) ����� ���������������</P></TD></TR>
<TR>
<TD vAlign=top colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>11.30</P></TD>
<TD vAlign=top width="82%">
<P align=justify>����</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>11.45</P></TD>
<TD vAlign=top width="82%">
<P align=justify>���������� ����</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>12.00</P></TD>
<TD vAlign=top width="82%">
<P align=justify>�������������� ��������, ���</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>12.15</P></TD>
<TD vAlign=top width="82%">
<P align=justify>���� ���</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>12.30</P></TD>
<TD vAlign=top width="82%">
<P align=justify>���������� ����</P></TD></TR>
<TR>
<TD vAlign=top colSpan=2><B>
<P align=center>��� ������� ���������� � ���������� �����<BR>������ � ����� ��� ����� ����������������</P></B><I>
<P align=center>������������ &#8212; ������ ���������</I></P></TD></TR>
<TR>
<TD vAlign=top width="18%" rowSpan=4><B>
<P align=center>���� ��� ����������</B></P></TD>
<TD vAlign=top width="82%">
<P align=justify>������������� ���������, ����������� ��� ������ ����������</P></TD></TR>
<TR>
<TD vAlign=top width="82%">
<P align=justify>���������, ����������� ������ ����� ����� �����������</P></TD></TR>
<TR>
<TD vAlign=top width="82%">
<P align=justify>������� ���������� ������������</P></TD></TR>
<TR>
<TD vAlign=top width="82%">
<P align=justify>��������� ������������� �������������� ����������</P></TD></TR>
<TR>
<TD vAlign=top colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>14.30</P></TD>
<TD vAlign=top width="82%">
<P align=justify>������������� ������� ������������ �������� ��</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>14.45</P></TD>
<TD vAlign=top width="82%">
<P align=justify>������������� ������������ �������� �� � ���</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>15.00</P></TD>
<TD vAlign=top width="82%">
<P align=justify>���������� ����</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>15.15</P></TD>
<TD vAlign=top width="82%">
<P align=justify>���� ���</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>15.30</P></TD>
<TD vAlign=top width="82%">
<P align=justify>��������</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>16.15</P></TD>
<TD vAlign=top width="82%">
<P align=justify>����������� ����</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>16.30</P></TD>
<TD vAlign=top width="82%">
<P align=justify>�������������� ��������</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>16.45-17.00</P></TD>
<TD vAlign=top width="82%">
<P align=justify>���������� ������</P></TD></TR>
<TR>
<TD vAlign=top colSpan=2><B>
<P align=center>6 ������ (�������)</B></P></TD></TR>
<TR>
<TD vAlign=top colSpan=2><B>
<P align=center>���������� ������� ������ � ����� ���:<BR>�������� � �������� � ������������� ���������� ��������</P></B><I>
<P align=center>������������ &#8212; ������ ���������</I></P></TD></TR>
<TR>
<TD vAlign=top width="18%" rowSpan=3><B>
<P align=center>���� ��� ����������</B></P></TD>
<TD vAlign=top width="82%">
<P align=justify>������������� ������������� ��������� �� ������������� ������</P></TD></TR>
<TR>
<TD vAlign=top width="82%">
<P align=justify>���������� �������� � �� ������� �� ����������</P></TD></TR>
<TR>
<TD vAlign=top width="82%">
<P align=justify>�������� �� ��������������� ������ ������������ ���������������� ��� ����������</P></TD></TR>
<TR>
<TD vAlign=top colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>09.00</P></TD>
<TD vAlign=top width="82%">
<P align=justify>������ ���������</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>09.10</P></TD>
<TD vAlign=top width="82%">
<P align=justify>������� ����</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>09.30</P></TD>
<TD vAlign=top width="82%">
<P align=justify>���������� �� ���������� ������� ���������� (����)</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>09.50</P></TD>
<TD vAlign=top width="82%">
<P align=justify>������������� ���������� ����������</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>10.10</P></TD>
<TD vAlign=top width="82%">
<P align=justify>���������� ����</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>10.20</P></TD>
<TD vAlign=top width="82%">
<P align=justify>��������� ������� ���</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>10.30</P></TD>
<TD vAlign=top width="82%">
<P align=justify>����������� ��������</P></TD></TR>
<TR>
<TD vAlign=top colSpan=2><B>
<P align=center>����������� �������������� � ������������ �����</P></B><I>
<P align=center>������������ &#8212; ��������� �������</I></P></TD></TR>
<TR>
<TD vAlign=top width="18%" rowSpan=4><B>
<P align=center>���� ��� ����������</B></P></TD>
<TD vAlign=top width="82%">
<P align=justify>��� ������������ ����� ���������� �� ������� ���������������?</P></TD></TR>
<TR>
<TD vAlign=top width="82%">
<P align=justify>�������������� �������, ������������ � ��������</P></TD></TR>
<TR>
<TD vAlign=top width="82%">
<P align=justify>����������� ����� � ��������</P></TD></TR>
<TR>
<TD vAlign=top width="82%">
<P align=justify>����� � ��������� �������</P></TD></TR>
<TR>
<TD vAlign=top colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>11.15</P></TD>
<TD vAlign=top width="82%">
<P align=justify>���� ��� Wachovia</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>11.35</P></TD>
<TD vAlign=top width="82%">
<P align=justify>���������� ����</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>11.55</P></TD>
<TD vAlign=top width="82%">
<P align=justify>���������� ����</P></TD></TR>
<TR>
<TD vAlign=top width="18%">
<P align=center>12.15</P></TD>
<TD vAlign=top width="82%">
<P align=justify>���������� ����</P></TD></TR>
<TR>
<TD vAlign=top colSpan=2><B>
<P align=center>������</P>
<P align=center>������� �� ����������� �������. ����� �� ���?</P></B><I>
<P align=center>�������������� &#8212; ����� �������� (FSVC)<BR>� ��������� �������� (���������� &#8220;������&#8221;)</I></P></TD></TR>
<TR>
<TD vAlign=top width="18%" rowSpan=4><B>
<P align=center>���� ��� ����������</B></P></TD>
<TD vAlign=top width="82%">
<P align=justify>������� �� ������� ����� ����� � ������������?</P></TD></TR>
<TR>
<TD vAlign=top width="82%">
<P align=justify>� ��� ������������ ������� ������ ����� ������?</P></TD></TR>
<TR>
<TD vAlign=top width="82%">
<P align=justify>������� �� ���� ��� ������������������ ������?</P></TD></TR>
<TR>
<TD vAlign=top width="82%">
<P align=justify>������ �� ������������� ����� �����, ������� ������� � ����������?</P></TD></TR>
<TR>
<TD vAlign=top colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="18%" rowSpan=4>
<P align=center>16.30</P></TD>
<TD vAlign=top width="82%">
<P align=justify>�������������� ��������� � �����������</P></TD></TR>
<TR>
<TD vAlign=top width="82%">
<P align=justify>����� ��������</P></TD></TR>
<TR>
<TD vAlign=top width="82%">
<P align=justify>������ ��������</P></TD></TR>
<TR>
<TD vAlign=top width="82%">
<P align=justify>��������� �������</P></TD></TR></TBODY></TABLE><B>
<P align=center>����� ������������� ������ ����������<BR><FONT face=Wingdings>(</FONT> (495) 916-17-51 E-mail: cms@asros.ru</P></B>


</body>
</html>

