<!--
ID:36714802
TITLE:�� ������ �������� �� ������������� ���������� ��������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:13
ISSUE_DATE:2006-07-15
RECORD_DATE:2006-09-22


-->
<html>
<head>
<title>
36714802-�� ������ �������� �� ������������� ���������� ��������
</title>
</head>
<body >


<P align=justify><FONT>� ����� � ������������� ���������� ����������� �������, ������������ ���������� ������������, � ����������� ����� ����� ������, �������� ������������� ���������� ��� � ������� �������, ���� ������ ������� �������� �� ������������� ���������� �������� � ��������� ������ � ���:</FONT></P>
<TABLE cellSpacing=2 cellPadding=7 width=610 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>&#8470;</FONT></P>
<P align=center><FONT>�/�</FONT></P></TD>
<TD vAlign=top width="37%">
<P align=center><FONT>����, ���</FONT></P></TD>
<TD vAlign=top width="23%">
<P align=center><FONT>�����</FONT></P></TD>
<TD vAlign=top width="17%">
<P align=center><FONT>����<BR>�����������</FONT></P></TD>
<TD vAlign=top width="16%">
<P align=center><FONT>��������-�������<BR>�����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>1</FONT></P></TD>
<TD vAlign=top width="37%">
<P><FONT>���-����</FONT></P></TD>
<TD vAlign=top width="23%">
<P align=center><FONT>������</FONT></P></TD>
<TD vAlign=top width="17%">
<P align=center><FONT>21.03.1991</FONT></P></TD>
<TD vAlign=top width="16%">
<P align=center><FONT>1421</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>2</FONT></P></TD>
<TD vAlign=top width="37%">
<P><FONT>����-����</FONT></P></TD>
<TD vAlign=top width="23%">
<P align=center><FONT>������</FONT></P></TD>
<TD vAlign=top width="17%">
<P align=center><FONT>09.06.1994</FONT></P></TD>
<TD vAlign=top width="16%">
<P align=center><FONT>2872</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>3</FONT></P></TD>
<TD vAlign=top width="37%">
<P><FONT>���������</FONT></P></TD>
<TD vAlign=top width="23%">
<P align=center><FONT>���������</FONT></P></TD>
<TD vAlign=top width="17%">
<P align=center><FONT>29.06.1994</FONT></P></TD>
<TD vAlign=top width="16%">
<P align=center><FONT>2940</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>4</FONT></P></TD>
<TD vAlign=top width="37%">
<P><FONT>�������</FONT></P></TD>
<TD vAlign=top width="23%">
<P align=center><FONT>������</FONT></P></TD>
<TD vAlign=top width="17%">
<P align=center><FONT>28.05.2002</FONT></P></TD>
<TD vAlign=top width="16%">
<P align=center><FONT>3412</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>5</FONT></P></TD>
<TD vAlign=top width="37%">
<P><FONT>�����-�����</FONT></P></TD>
<TD vAlign=top width="23%">
<P align=center><FONT>������</FONT></P></TD>
<TD vAlign=top width="17%">
<P align=center><FONT>20.12.2000</FONT></P></TD>
<TD vAlign=top width="16%">
<P align=center><FONT>3355</FONT></P></TD></TR></TBODY></TABLE><I>
<P align=right><FONT>�����-������ ������������<BR></FONT><FONT>������� � ������������ ������<BR></FONT><FONT>����� ������ �� 16, 23.06.2006</FONT></P></I>


</body>
</html>

