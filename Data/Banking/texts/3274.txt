<!--
ID:35789900
TITLE:����������� ����������� ��������� ����������� �� �������� ������������������� ��������� ��������* �� ��������� �� 01.11.2005 �.
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:24
ISSUE_DATE:2005-12-31
RECORD_DATE:2006-01-26


-->
<html>
<head>
<title>
35789900-����������� ����������� ��������� ����������� �� �������� ������������������� ��������� ��������* �� ��������� �� 01.11.2005 �.
</title>
</head>
<body >


<TABLE cellSpacing=2 cellPadding=4 width=609 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="5%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="26%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="56%" colSpan=4>
<P align=center><FONT>���������� ��</FONT></P></TD>
<TD vAlign=top width="12%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="5%">
<P align=center><FONT>&#8470;</FONT></P>
<P align=center><FONT size=+0>�/�</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>������������</FONT></P></TD>
<TD vAlign=top width="28%" colSpan=2>
<P align=center><FONT>�� 01.01.2005</FONT></P></TD>
<TD vAlign=top width="28%" colSpan=2>
<P align=center><FONT>�� 01.11.2005</FONT></P></TD>
<TD vAlign=top width="12%">
<P align=center><FONT>���������</FONT></P>
<P align=center><FONT size=+0>(+/&#8211;)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%"></TD>
<TD vAlign=top width="26%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>����������</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>�������� ��� </FONT><FONT>� �����, %</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>����������</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>�������� ��� </FONT><FONT>� �����, %</FONT></P></TD>
<TD vAlign=top width="12%"></TD></TR>
<TR>
<TD vAlign=top width="5%">
<P align=center><FONT>1</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=justify><FONT>�� 3 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>73</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>5,6</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>57</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>4,5</FONT></P></TD>
<TD vAlign=top width="12%">
<P align=right><FONT>&#8211;16</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%">
<P align=center><FONT>2</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=justify><FONT>�� 3 �� 10 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>133</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>10,2</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>109</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>8,7</FONT></P></TD>
<TD vAlign=top width="12%">
<P align=right><FONT>&#8211;24</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%">
<P align=center><FONT>3</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=justify><FONT>�� 10 �� 30 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>232</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>17,9</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>212</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>16,8</FONT></P></TD>
<TD vAlign=top width="12%">
<P align=right><FONT>&#8211;20</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%">
<P align=center><FONT>4</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=justify><FONT>�� 30 �� 60 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>225</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>17,3</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>217</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>17,2</FONT></P></TD>
<TD vAlign=top width="12%">
<P align=right><FONT>&#8211;8</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%">
<P align=center><FONT>5</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=justify><FONT>�� 60 �� 150 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>211</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>16,2</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>223</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>17,7</FONT></P></TD>
<TD vAlign=top width="12%">
<P align=right><FONT>+12</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%">
<P align=center><FONT>6</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=justify><FONT>�� 150 �� 300 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>191</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>14,7</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>200</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>15,9</FONT></P></TD>
<TD vAlign=top width="12%">
<P align=right><FONT>+9</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%">
<P align=center><FONT>7</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=justify><FONT>�� 300 ��� ���. � ����</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>234</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>18,0</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>242</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right><FONT>19,2</FONT></P></TD>
<TD vAlign=top width="12%">
<P align=right><FONT>+8</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="26%"><B>
<P align=justify><FONT>����� �� ������</FONT></B></P></TD>
<TD vAlign=top width="14%"><B>
<P align=right><FONT>1299</FONT></B></P></TD>
<TD vAlign=top width="14%"><B>
<P align=right><FONT>100</FONT></B></P></TD>
<TD vAlign=top width="14%"><B>
<P align=right><FONT>1260</FONT></B></P></TD>
<TD vAlign=top width="14%"><B>
<P align=right><FONT>100</FONT></B></P></TD>
<TD vAlign=top width="12%"><B>
<P align=right><FONT>&#8211;39</FONT></B></P></TD></TR></TBODY></TABLE>
<P align=right><FONT><EM>"������� ����� ������", 2005, &#8470; 61</EM></FONT></P><SUP>
<P align=left><FONT face="Times New Roman CYR">*</FONT> </SUP>�������� �������, �������� �������� �������� �����������, ������� � ����� ��������� ����������� � ������ � ����� ��������������� ����������� ��������� ����������� ����� ����������� ������ � �������������� �������������� ������.</P>


</body>
</html>

