<!--
ID:36057184
TITLE:����������� ���� � ������ &#151; �����
SOURCE_ID:3126
SOURCE_NAME:������� ����������� ����
AUTHOR:
NUMBER:1
ISSUE_DATE:2006-01-31
RECORD_DATE:2006-04-04


-->
<html>
<head>
<title>
36057184-����������� ���� � ������ &#151; �����
</title>
</head>
<body >


<P align=justify><STRONG>� ������ ����������� ��������� ���: ���� �������������, ��� ��� ����� ������ � ������, ������ ������� ������� ������������� ������. �� ������� ������������, �� ������ ��������� ������������, ��������� ��� ����������� �������, �, ����� ����, �� ���������� ������ ����� ��������������. ��� ����������, � ������� �� ���������� �������� ��� ��������, &#8212; �. �. ��������, ���������, ��������� ���������, ������ ����-��������� ���������� ������������ ������ ������.</STRONG></P>
<P align=justify><B>&#8212; ������, ������� ������� ����: ��� ���� � �������� �������� ��������? �� �������: ������������� ��������� ������� ��������, �� ����, �������� ������, ���� ���. ���������, ������ �� ����, �� �����������?</P></B>
<P align=justify>&#8212; ��������� � ����������� ������ ������������� ������������� ������� ���������-�������������� ��������, ���, � ��� ����� ���� ��������� �� ��������. ������� �������� � ������ �������������� ������ �� �������� ��� �� ������, �������, ������ �����, �������� ���������, &#8212; ������ ��������������� ���. ������� �������� � ������, � ����� �������, ������������ ���������, ����������� ��� �� ��������; ���� ��� ���������� ������������ ��������: ���� � ��� � ���������� ���� ���� ������� ��������, �� � ��������� � ��� ��������� ������.</P>
<P align=justify>����� ����� ����� ��������, �� ����� ��������� �������� ���������, �������� �������� ������ �������������� �����, �������� ��������� ���������� �����, � �. �. �����������, ������ &#8212; �����������. ������� �� ���� �������, ����� ������ ������ �������� ��������, ������ ��� ��������, ������ �� ������ �������������� �����: ���� �������� �������� �������� ������������ �� ������, ��������, 6%, �� ��������������, ������� ����� � ��������� �����, �����, � ����� �������, ������������ ������������� ���� �� ���� ������, �, � ������, &#8212; �� ��������� ������� �����.</P>
<P align=justify><B>&#8212; ����� ��������, �� ����, ������� ���������� �����, ������-�� �� �������� ����� ���. ����� ������� � ��� ������� ��������?</P></B>
<P align=justify>&#8212; ��� ���� ���� �� �� ����������, � � ������� ������� ������, � ����, � ������� ����� ����� ������� ��������� ����� ����� ���. ��� ��� �������� &#8212; �� ������� ��������. ��� ��� ��, ��� ����� �� ����� �������, �������� ������, �������� ����� 11% &#8212; ��������� ��������. �� �������, �� ����� 2005 ���� �������� � ��� �������� �������� 11,5% &#8212; � ������� ���������� ��� �������� �� ������ �������� ����. �� ���� � ���, ��� � ����� 2004 ���� ����������� ���� �������� �������� �� �������� 8%; �� ���� �������� �� ���������, ��, � ������ �������, �� ��������� ���������� �������� ���� ���������� �������� ����.</P>
<P align=justify>��� ������ ��������, ���� �� �������� ����� �� ���� ������ � ���� �� ����� ���� � �� ����� ��� ��������. � �������� ������� ���� ���������� ���������� ����� 2%, ���� �� ���� &#8212; ��� ��� ������, ��� ����� �������� ������ � ���������. ���� � ���, ��������, �� ������ ���������� �������� �������� ��������� ��� ��������, � � ������� �� ��� ��������� ��������� �����. ������� ������� ����� ����������� ����������� ��������� ������� (���������� ���) ������ ������� ������, �� ���� ������� ����� ����� ��������. ��� ���� ��� �������? ����� ����������� ����� ����� ������, ����� ����������� ������� ����� �������, � ������, ����������� � �������� �����������. ����� �����, �������� ������, ����������� ������ ��� ����� �� ������� 4-5%, � � ������, ��� ��� ��� ����� ����� � ��������� ���� ����� �� ����� �� ������� ��������.</P>
<P align=justify><B>&#8212; ������ � ������ ����������� ��������� ���, ���� �������������, ��� ��� ����� ������ � ������. ������ ������� ������� ������������� ������. �� ������� ������������, �� ������ ��������� ������������, ��������� ��� ����������� �������, �, ����� ����, �� ���������� ������ ����� ��������������.</P></B>
<P align=justify>&#8212; ���� �� ���� � ����������� �������. ��������������� ������������ ������������� ����� ������ �����������, � ���������� ������ �� �������� ����� ��������� � ��������. ������, ����� �� ������� � ������������� �������, &#8212; ��� �����, � ����������� ��������������� ������ ������ �� ��� �������� �������� &#8212; &#8220;���������� ��� ������ ������ � ���������&#8221;. ������ ������������ ������������� �������, ��� �������� �������� ������ ��������� ������. �� ��������� 2 ���� ����� ������������ ���������, �� ������� ����, �������� &#8212; ��� ����� ������� ����� �����.</P>
<P align=justify>� �����, ��������� ��������� ���������, ��� �������� ���� � ������: ��� �� �������, ����� ������� ����� � ������, ��� ������, ��� �� ����� � ���������� ����. ���� �������� ������ � ��������� ��������, ��, �������, ������� �������, &#8212; � �� ���� ��������� ������������, ��� ��������� ��������, &#8212; �� ������������ ������� �������, ����������� � ������ � ��� ������� ������� ������. �� � ��� ����� ������� ��������� �����, �, ������ �������, ������ � ���� �������� ������������ � ��������������� ������������. � � ����� �� �������� ��������, ��� ������ 2005 ���� ����������� ��������������� ������ ��������� � ����������� ������ ����������� ������������ ��� ���������.</P>
<P align=justify>� ����������, �����, ����� ������� ������ � ���� ����� ������, ��� ����������� ������� � ���������� ���������, ����� ����� �� ��������. �� � ���������, � � ���������� �����! � ��������, ����� �� ����������� ������ &#8212; ��� ������ ������� ���������� ������. ������� ����� ��������� ���������� ������ �����, ������� ������� ������ ����� �������.</P><I>
<P align=right>&#8220;����� ������&#8221;, 11.11.2005</P></I>


</body>
</html>

