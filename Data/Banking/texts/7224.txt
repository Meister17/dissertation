<!--
ID:35936825
TITLE:�������� ��������
SOURCE_ID:10908
SOURCE_NAME:������������ ���������� ������
AUTHOR:����� �����������, ������������ ���������� PR
NUMBER:2
ISSUE_DATE:2006-02-28
RECORD_DATE:2006-03-03


-->
<html>
<head>
<title>
35936825-�������� ��������
</title>
</head>
<body >


<H1 class=art_title><FONT size=3>��������� ������ ����������� � ������� ������������ ����� �����</FONT></H1>
<P align=justify><FONT size=+0>� ������� 2005 ���� ��� &#171;����� ����&#187; (�. ������) ���� ��������� ������������ ���������� ������ � ��������� &#171;���� � �������� ������������ ��������, ������� ���������� ����� � �������� ��������� ������&#187;. </FONT></P>
<DIV class=lead><FONT><STRONG>� ��������, �������� ������������ � ���������� ����� ����� �� ����� ���� �������� 64 500 �������. ��� ���� ��� ������� ����������� ����������� � ���������, �� ������� ����� ������.</STRONG></FONT></DIV>
<DIV class=artic_body>
<P class=bukvitza align=justify><FONT>����������� ����������� ����� ����� ������������� �������, �������� ������ ������� - ������������ ����������� �������� � ����������� ����������� ���� � ���������. </FONT></P>
<P align=justify><FONT>������� � 1979 �. ���� ��������� ����������� ��������� ������������ ���������, ������� ���������� &#171;��������� �� ������� �����&#187; � ����������� ������� ����� 50 ���. �����. � ���� ���������� ������������� ��������, ������� &#171;�������������� �������&#187; �����, ����������� ������������� ����� ��������� ��� ��������� � ���������. ������ ������ &#171;��������� �� ������� �����&#187;, ���� ����������� ������� �� ��������� � ����� ���������� � ������ �������. ��������� ������������ ����� �������� � �������� ��������� - �� ������������� ������� �� ��������� ����� ����. ���� ������ �� ���������� ������������, ��������� �� ������, ���� � ��� � ���������� ������, � ����������, � ����������������. </FONT></P>
<P align=justify><FONT>�� ���� ������������ ��������� ����� � �� ���� � ����� �����. ��� ������ ����� ���������� � ��������� �� ��������� � ���������� �����, � ���������-�����, �� ����� ���������, ����� �������� � � ���������, � ����� �� ������������� ���������. </FONT></P>
<P align=justify><FONT>������ ��� ������ ����������� ����, ����� ��� ���� �������� - ������ ����������� ��������� � ����, ������������ ��� �� � ������������ ���������� ����� ��� �������, � � �������� �������� ����������. ������������, ������������� �� ������� �����, �� ������ ���������� �������� ������������ ��� ���������� ����������� �����, ��� ��������� ������� �� ��� ����, ��������� ����� ������ �������������� � ��������� ���������. </FONT></P>
<P align=justify><FONT></FONT>
<DIV class=pica><FONT> </FONT>
<P class=capton align=justify><FONT>��� ������ (1967).<BR>��� ��������, 1992. (c) ��������� Deutsche Bank </FONT></P></DIV>
<P align=justify><FONT>� ����� ����� �� ����������, ��� ��������� �������� ����� ���������, ������������ ����������� ���� ��� �����������, ���������� �� ��������� �� ������������ ��������. � ������ ����� ����� ������ ������� ����� ����������, ��� ������ ������������ �� ��� ��� ���� ����� ������. </FONT></P>
<P align=justify><FONT></FONT>
<DIV class=pica><FONT> </FONT>
<P class=capton align=justify><FONT>����� ������ (1867-1956).<BR>������� ����, 1910. (�) ��������� Deutsche Ba </FONT></P></DIV>
<P align=justify><FONT>� ������ ����� ��������� ������� �� ��������� ����� �����. ��� �� ������ �������� ����� ������ �����, �� � �������� ������ � ����. �����, ����� ���� ������� ���������� ��� ���� ��� ����� �����������, �� ������ ������� �������, ������� ��, ��� � ������ ������ �� ������������, � ��������� ������������� �������������� ������. �����, ��� ��� ������� � ����� �����������, � ����� ������. ������� ������� ������������ ��������� � ������� ������������, ���������� �, ��� �����������, �����. ������ ����� � ��������� ��������� ��������� ������ �������� ������ � ��������� ����������� ����� ��������� ����������, ������� �������� ������ �� �������� ������������, ��������� ������������ �� ���������� � ��������� ��������������. ������ �� ������ ���������� �������� ����� ������������ ��������� ������, ���������� ������������ ���������� � ������. </FONT></P>
<P align=justify><FONT>������ �� ��������� ����� ������������ � ������, � ��� ����� � �����-������������� �������� (2002) � ���������� ����� ��������������� �������� ��. �.�. ������� (2004-2005). </FONT></P>
<P align=justify><FONT>���������� ��������� ���������, ��������� � �������� � ���������� ������, - ������ �������� � ������� ������������ ����� �����. � 1983 �. ���� ������ ������� ����� ��� � �������� �������� ���������� �������� � ������. ����� ������ ���� ����� �������� &#171;�������� ���������&#187; � ������. ����������� � ������� ��������� ��������� ��� ���������� � ������������ ���, ��� ���� ������������ ����������, � ���������� �������� ��������� ��� ���� ��� ������������� - �������������� ����������� � ����� ������� � ���������. </FONT></P>
<P align=justify><FONT>������ � ���������� ��������, ��� �� ���� ������������ �������� � ����������� ���� ����� ��������, ����������� �� ������ ���� ��������, ����������������, ��������. </FONT></P>
<P align=justify><FONT></FONT>
<DIV class=picA><FONT> </FONT>
<P class=capton align=justify><FONT>������� ���������� (1866-1944).<BR>�������� � ������� ������, 1911. (c) ��������� Deutsche Bank </FONT></P></DIV>
<DIV class=picA>
<P class=capton align=justify><FONT></FONT></P></DIV></DIV>
<P class=capton>����� ������ ������� (1880-1938).<BR>���� ����� ����� � ������ ������, 1921. (c) ��������� Deutsche Bank</P>


</body>
</html>

