<!--
ID:36292594
TITLE:������ �� ���������� ���������� ������� ��������������� � �������� ������������� ���������� ������? 
SOURCE_ID:3126
SOURCE_NAME:������� ����������� ����
AUTHOR:
NUMBER:3
ISSUE_DATE:2006-03-31
RECORD_DATE:2006-06-02


-->
<html>
<head>
<title>
36292594-������ �� ���������� ���������� ������� ��������������� � �������� ������������� ���������� ������? 
</title>
</head>
<body >


<B>
<P align=center>������� �����������, ���������, ������� ��������� �������������� �������� ������ ������</P></B>
<P align=justify>&#8212; ��� ����� ��������� ��������� �����������. ��-������, �������, ��� ���������� ������ � ����� �������� ����������� �������� ���� �� �������, ��� ������ ����������� �������, ����� ������, �������� �������� �����, ������� ������ � �������� ��� ��� �������� ������� ������ ����������� ���������� ��������� �������, ��� ���. �� ���������� ������� �������� ����� ��� ������ ����������� ������� ����� �������� �������� �������, �� 40%, � ���������� ���� ���� ��� ������. �� ���� ���� ������������ ���� ����������� ���������� �� ���� ����, ��� �� ����� ������� ������ ������, ������� �������������� � �������� �����������. ������ �� ���������� ��� ����� ��������, ��� ��� ���������� ������������ �� �����. ���� ���� �� ����� �� ����� ����� �� �������������, � ����������� �� �������� ������, �� ������ �������� � ���������� ����������� ������ ����������� �����������, �.�. ��������� ���������� ���������� ������� ���������. ��� ����� ������� ��������� ������, ��������� ������ �������� �������, �������� ���� ��� �������� � ������������� ��������� &#8211; ������������ ��� ������������ �������� ��� ������������ ���. ��������������, ��� ����� ������������� ��������� ���������� ������ �� ���������� �������� �����, � ���������� ������� ���������� �������� � ���������� ����� &#8211; ������ �� ��� ��������������� � �������� ������������� ���������� ������(������, ��� ��������, ������ � �������� ��������� �������������). �� ����, �� �������� � ������ �������, ����� ����� ���������� �������� ����� �������� ���������������� ���� ���������� �������, ������, � ���������. � � �����, ��� ������, ����� ����� ������������� � ����� �����, ��� ������ ������, ��������, IPO �������������� ����� �� �������� ������� ��������.</P><B>
<P align=justify>&#8212; � ����� ������� ����� ��������� ��� ����������� ��������� ����������� ����������?</P></B>
<P align=justify>&#8212; � �����, ���, ������ ������� ������� �������� ������, ���������� ������� � �����, ��������� ����� ������� ���������� ����������. ������, ���� �� ��������������� � ������ ���� ��������, ��� ���� �� ����� ���-���� ����������� �� ��� ��� ���� ������, �� ����������� ����� ������������� � ���������������, �����, ������ �����, ����������� � ��� ������, ������� ����� �������� ���������� ��������� ������������������, ��������� ������������� ������ � ���������, ���������� �� ������ ������, �������� �������� ���������� �������� � ��� �����.</P><B>
<P align=justify>&#8212; ����� ��������� ����� � ����� ����������� ���������� � ����������?</P></B>
<P align=justify>&#8212; ������ ����� ����������� ���������� �� ���� ��������, ��� � ���������� ����������, ������� ��, ���� ������ � �������� ����, ��� ��� ���������� ������, ��� ���������� ��������� � ����� ����������� ������������ ������� �� ��������� � ���������� � �����. ������� ����������, ���������� ������ ��������, � ��� �������� ����� ����� ��������� ��������������. ��������� � ��� ��-�������� ���������� ������ ��������� 1200, ���� �� ��� ��� ������ �����������. ��������, ��� � �������� ����� ���������� ������ � ����������� ����������� ����� ������� ����� ��������� ������� � ����������. ������� � ������������� ���������� � ������ ���� � ����� ���� &#8212; ����� � ������ �����, �� ���� ����-�� ����������. ����������� ���������� ����, � �����-��, ������ �� ��� �� ����������� &#8212; ���������� ������� � ��� ��� �� ������� � ���� ���������� ��������� ��� �����. ������� ���� ����� ����������� ���������� ������� ����������� �������� 15 ���, � ��� ��� ����, ��� ��� �� ������ ����������� ����������� ��������� ��������� �������, �������� ������� ��������, ������� �������� �� ������ ���������� ��������� ��������. ������� ���������� ���������� ������� ��������� � ��� ����������� ����������, ������� ���� � ������ � �������� �������� ������������ ����������� ��� ������������ �������� ��� ����������� � ���������� ������ � ���. ������� ������ ����� ���� � �� �� &#8212; ��, ��� ��� ��������� ��������� ���� � ����������� ����� ����������� �������, ��� ��������� � �����.</P>


</body>
</html>

