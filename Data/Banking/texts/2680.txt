<!--
ID:37754586
TITLE:��������-�������: ���������, �� �� �������?
SOURCE_ID:11528
SOURCE_NAME:���. ����� � ������� ���
AUTHOR:����� ������, �������� ������������ ��������� ���������� ���������� ������������ ��������� &#171;������� ��&#187;, ������ ���������, ������� ������������ ��������� ���������� ���������� ������������ ��������� &#171;������� ��&#187; 
NUMBER:5
ISSUE_DATE:2007-05-31
RECORD_DATE:2007-06-06


-->
<html>
<head>
<title>
37754586-��������-�������: ���������, �� �� �������?
</title>
</head>
<body >


<H1><FONT size=3>���� �� �������� �����, � ������� ��� ������ �����. �� ������� ����, ����� ������� ���������� � &#171;��������&#187;. � �� ����������� &#8212; ���� ��� ���� �� ��������� ��������� ������, ��������������� �������� �����������, ������� ����������, � ����� ����, ����������� ������������� �������. ������ ����� ���� ���� ���� ����� � ����� ��������� � ��������� &#171;�������&#187;, ���������� ������ ��������� ��������, ������� ��� �������������. </FONT></H1>
<P align=justify><FONT>������ ��������-������� &#8212; ������ ����. ������ �� ����� �� ��� ��� ����������� ����� ������ ���������� ����� ��� ����� �����, �������, �������� � �������������� ����� ������, �� ����� �� ����� �����? ����� �����, �� ��-������ ������ �������? ���� �������������, ����� �������? �� ���� � � &#171;�����������&#187; ������, � � ������������� ������ �������� �� ���� �� ������ ������������, �� ���� ������������ ������������� ������� ���������� ����������. </FONT>
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<P class=vinos align=justify><FONT><STRONG>��������-������ �� ��������� ������������ ���������� � ������ � ������� � ������ �����e </STRONG></FONT></P></BLOCKQUOTE>
<P align=justify><FONT>���� � ���, ��� <STRONG>������� ���������� ������</STRONG> &#8212; �� ������� ����������, ����� ������� �������� � ���������� ������ �� ��������� (����� ��� ��������), ��� � ������� � �������� �������� � ������� ������� ��������. ����� ������ ��������� ������� �� ����������� ���� &#8212; ���� ������� ���� ������ (�������� ��������� �����, ����� ����� � �����), ���� �������� �� ���� ���������� �������������. ����������, ��� ����� �������������������� ����� � ����������� &#8212; ����� ����������� eye contact, ����� ����� ����� � ������, ��������, ��� ��� ���. ��������-������ �� ��������� ������������ ����� ���������� � ������ ������. ��������, ��� �� � ����� ����������, &#8212; ����������� ��������� ��� ��������� ����������� � ����� �������� � �������� (�� � ��������). ������ ���������� ������� �������� ��������� ����� ����������� ������ &#8212; �� �����������, �������� � �� ���� &#171;��������� ������&#187;, �� ������� ��������� ����������� � ��������������� &#171;������&#187;. </FONT>
<P align=justify><FONT>������ � ����� ���������� �������� � �������� ����� ��������. Feedback � ���������� ���������� ����� ����������� ��� ������ ���������. �������� ��� �������� ����������� &#8212; �������� ������� ����� �� ������� ��������, � ������, ������ ��� ��������� ������ �������� ����������������� ��� �������� � ��������������� �������������. </FONT>
<P align=justify><FONT>������ ������ ����� ��������, �������, ������ � ����������, ��� � ������. ������ ������������� � �������� �� ��� � ������� �������������� � �������������� ������� ������. �� ���� �������� ��������� � PR-������������, �������������� ���������, � ������� ��������� ������� ����������, �� �� ������. </FONT>
<P align=justify><FONT>���� �� ��-���� ������������� ���� ��� <STRONG>�������������� ����� ������ ���������� ���������</STRONG>, ��������� ���������. ��-������, ����� ��� ���� �����, � ���� ��������� � ���������� ��� ���������� &#171;4�&#187;, � ��-������ &#8212; ������������� ���������� ���������� ���-����������. </FONT>
<P align=justify><FONT>������������� ���������� �����, ��� �� �������, &#8212; ���� �� �������� <STRONG>����������� � �������� ��������-��������</STRONG>. �����������, ����� ���������� ������ �������������, �������� ������ ��������� ������� �� ����� ������������ ���������, �������� ������������ ������� ����� ���������. �������� �� ����� ���� ���������� ������, ���� ��������� ���� ������ �� �������������� � ����� ������ �� �����������. ������� �������� ����� ��� ����, ���, � ������� �� �������� ������������, � ���������� �������� ������ �������� ����� ��������� (��������, ���������, ����, ������), � ����������, ������������������ �������� ��������-��������, � ����� �� ������� �������� ������������� ������������� �������, ���� � ����������� ��������� �������� ��������� ������������������: ������ ������, � ����� ��� ����� � �� �������. </FONT>
<P align=justify><FONT>������������� �� ���������� &#8212; ������ ������ ������������: ��� �� ������ �� ��� ������ �����, ��� ��� �������� � ���������� ��������������, ������� ����� ������ �������� �� �������, �� ����, ���� �� ����� ���� ������. ���� ���� �������������� ���������� ���������� � ����������� ������������ &#8212; ������ ���������� ��������� ����� ���������� ����������� (��������, �����, �������������), ������� �������� ������. </FONT>
<P align=justify><FONT>����� �� ������ &#8212; ������������� ��������. �������� ���� ������ ������ �������� ���� ������ � ������������ ����������� ������: � ����� ����, ����� ����� �� ������? � ����� ������ ��� ������ ����� ����, ����� ����������� ��������� ������ ������ ���-�� �������� � ��������, � � &#171;��������&#187; �������� ��������, &#8212; �������� ��� ���������� �����������, ����� ����� � ������ �������� ������������. </FONT>
<P align=justify><FONT>�, �������, ��������������� &#8212; ������������ ������, ������� ����� ��������� ������ �� ���������� ������ � �����, �� ������������� ��������-�������. ������� � ������ ����� ��� ������ � �������������� �������� ������, ��������������� �� ��������-�������� ����������� ������ ����� � ����� ����� ��� � ����. ����� ����, ����������� ����� ������� �������������� �������� ��������� ������. ����������� �� � � ��������� ������� ���������� �������� ��������-�������. </FONT>
<P align=justify><FONT>������ ����������� �� ���� �������� ���������� ��������-����� �� ��������-������ ������������� ���������. ������ ���������� ������������ �������, ��� ����� ���� ������� � ������ ��� �� �����������, ��������� ����� �������� � ���� �����, ������ ���������� ������������ ������ � ��������� ����� � ��������� ������������� IT-������. �, ������, ��� �� �������������, �� ��� ���������. ������� &#8212; �� ��������� � ���������. </FONT></P>
<H2 class=text-blue-deep align=center><FONT size=3>��������� ������� ������ </FONT></H2>
<H3 class=text-blue-deep align=center><FONT size=3>�������-���� ��������� &#171;������ ��&#187; �� 14.05.2007 </FONT></H3>
<TABLE cellSpacing=0 cellPadding=0 width="95%" align=center border=1>
<TBODY>
<TR class=bg-gray-middle>
<TD>
<P class=&#171;table&#187; align=justify><STRONG><FONT>������������</FONT></STRONG></P></TD>
<TD>
<P class=&#171;table&#187; align=justify><STRONG><FONT>�������</FONT></STRONG></P></TD></TR>
<TR>
<TD>
<P class=&#171;table&#187; align=justify><FONT>��� &#171;������&#187;</FONT></P></TD>
<TD>
<P class=&#171;table&#187; align=justify><FONT>�++</FONT></P></TD></TR>
<TR>
<TD>
<P class=&#171;table&#187; align=justify><FONT>��� &#171;�����������&#187;</FONT></P></TD>
<TD>
<P class=&#171;table&#187; align=justify><FONT>�</FONT></P></TD></TR>
<TR>
<TD>
<P class=&#171;table&#187; align=justify><FONT>�� &#171;���� ���������&#187;</FONT></P></TD>
<TD>
<P class=&#171;table&#187; align=justify><FONT>�</FONT></P></TD></TR>
<TR>
<TD>
<P class=&#171;table&#187; align=justify><FONT>���� ���������� ��������������</FONT></P></TD>
<TD>
<P class=&#171;table&#187; align=justify><FONT>�</FONT></P></TD></TR>
<TR>
<TD>
<P class=&#171;table&#187; align=justify><FONT>�����������</FONT></P></TD>
<TD>
<P class=&#171;table&#187; align=justify><FONT>�++</FONT></P></TD></TR>
<TR>
<TD>
<P class=&#171;table&#187; align=justify><FONT>������� ������������ ����</FONT></P></TD>
<TD>
<P class=&#171;table&#187; align=justify><FONT>B+</FONT></P></TD></TR>
<TR>
<TD>
<P class=&#171;table&#187; align=justify><FONT>�� &#171;������&#187;</FONT></P></TD>
<TD>
<P class=&#171;table&#187; align=justify><FONT>�++</FONT></P></TD></TR>
<TR>
<TD>
<P class=&#171;table&#187; align=justify><FONT>������������ �������� ���������� ����</FONT></P></TD>
<TD>
<P class=&#171;table&#187; align=justify><FONT>�</FONT></P></TD></TR>
<TR>
<TD>
<P class=&#171;table&#187; align=justify><FONT>������������</FONT></P></TD>
<TD>
<P class=&#171;table&#187; align=justify><FONT>�+</FONT></P></TD></TR>
<TR>
<TD>
<P class=&#171;table&#187; align=justify><FONT>�����������</FONT></P></TD>
<TD>
<P class=&#171;table&#187; align=justify><FONT>�++</FONT></P></TD></TR>
<TR>
<TD>
<P class=&#171;table&#187; align=justify><FONT>������� ��Ĩ������ ���������� ��������</FONT></P></TD>
<TD>
<P class=&#171;table&#187; align=justify><FONT></FONT></P></TD></TR>
<TR>
<TD>
<P class=&#171;table&#187; align=justify><FONT>����������</FONT></P></TD>
<TD>
<P class=&#171;table&#187; align=justify><FONT>�++</FONT></P></TD></TR>
<TR>
<TD>
<P class=&#171;table&#187; align=justify><FONT>���������� ���������� ��������</FONT></P></TD>
<TD>
<P class=&#171;table&#187; align=justify><FONT>�</FONT></P></TD></TR>
<TR>
<TD>
<P class=&#171;table&#187; align=justify><FONT>������������� �������������� ��������</FONT></P></TD>
<TD>
<P class=&#171;table&#187; align=justify><FONT>�++</FONT></P></TD></TR></TBODY></TABLE>
<P align=justify><FONT>� &#8211; ������� ������� ���������/������������������/�������� ����� </FONT>
<P align=justify><FONT>� &#8211; ������������������ ������� ���������/������������������/<BR>�������� ����� </FONT>
<P align=justify><FONT>� &#8211; ������ ������� ���������/������������������/�������� ����� </FONT></P>


</body>
</html>

