<!--
ID:36676198
TITLE:�����������
SOURCE_ID:3126
SOURCE_NAME:������� ����������� ����
AUTHOR:
NUMBER:5
ISSUE_DATE:2006-05-31
RECORD_DATE:2006-09-13


-->
<html>
<head>
<title>
36676198-�����������
</title>
</head>
<body >


<P align=justify><FONT face=Wingdings>&#178;</FONT> <B>��������� &#8212; ������������ ��������� ����� ������� ��������� ������ � ������������� ���������� ��������� ������� �������� ������� ������������ ������ &#8212; ������� ������� ������������ ����� ������� ����������� II �������. </B>������� ������ ���������������������� ������, ���������� ����������� � ��������������. ������� ������� ������������ ����� ������� ����������� ������������ ��� ��������, ��� � �������� ����, ����� ������������� ��� ����������� �������� ����� ������, &#8212; ��������� � ��������� � �������. ����� �������� ��������� ������� ������� ������������ ������ 27-28 ������� 1988 ���� � ������ � 1000-����� �������� ����. ���� ��� ������ ����������� ���������� ����������������� ����� &#8220;���������� &#8212; �����-�������&#8221;, � ������ ������� �������� ����������� ������������ ������ � ��������� ��������-���������, ���������� � ��������� ������� ����� � ����������. ��� ������ ����� ������� ������������ ����� ������� �����������, ���������� ����������� ����� &#8220;�� �������� � ������ � �������� ����������������� ������������&#8221;. ������� III ������� ���� ������� ��� 15 ������ 2002 ����.</P>
<P align=justify><FONT face=Wingdings>&#178;</FONT> <B>������� CONTACT � ��������� ������ &#8220;�������&#8221; ������������� ����� �������� ����������� ������������ �������� ��������� ������. </B>��������� ����� ����� ������ �������� � ��������-�������� �� ����� ��������� ������ &#8220;�������&#8221;, � �������� �������� �� ���� ��� ����� &#8212; � ����� ������ ����� �������� CONTACT. CONTACT &#8212; ������ ������� �������� ��������� � �������� �� ���� ���� ������-��������������� � ������ ��������������� ��������� ������������ ������� � ������ �������������� �������� �������� &#8212; ��������� ������, ������� �������� ��������-���������, � ����� ������ ����������������, ���������� � ����������������. � ������� ������� ����� �������� ������ ������� �����, ������������ ��, ��������� &#8220;����������� ��������&#8221; ���������� �����, �������� �������. ���������� CONTACT, � ����� ������� ����� ����������� ������, �������� ���������� ��������, ������������ � ������������� ��������, ��������� ���������, ����������� �����, ���������� �������� � IP-���������.</P>
<P align=justify><FONT face=Wingdings>&#178;</FONT> <B>� 22 ������ �� 1 ��� � ������� CONTACT ������������ 46 ����� �������. </B>26 �� ��� ��������� � ������, ��������� &#8212; � ������� �������� ���������: ������� ������������, �����������, �������� � �������. ��������������� �������� ������� CONTACT ������ ����� ������ �������� � �������� (������������ ����), � ����� ������ �������� (������������� �������, �������). ��������� ���� �����������, �� ������, ��������� ����� ���������� ������� CONTACT. ��� � ��������� ������ ������ ����� ������-���� � ���� &#8220;����������&#8221;.</P>
<P align=justify><FONT face=Wingdings>&#178;</FONT> <B>������ � ������� ������� CONTACT, ��������� �������������� � ��������� Moneta Express (���), ����� ��������� ����� ����, �������� � �������. </B>� � ������ �������� �������, ������������ �� CONTACT&#8221;, ���������� ������� ���������.<B> </B>� ���������� ���������� � ������� ����������� ���������. �� 10 ��� ������ �� ���������� ����� �����������, ��� &#8220;���������&#8221; �������� �� ����� ������ ��������� �������� � ���� �������. � 14 �� 21 ������ ������� CONTACT ����������� 26 ������ �������� � 5 ������� ����, &#8212; ������, ������� �����������, �������, � ������. ������� CONTACT ����� �������� ������� ��� ���� �������: ����� (�������), ������� (��������� �������) � �������������� (��������), ������. ����� �������, ������� CONTACT ���������� ����������� � ����� ������ � �������, �������� ����������� � ������� � �������.</P>
<P align=justify><FONT face=Wingdings>&#178;</FONT> <B>18 ������ ���������� ������� ����� �������� ���������� ������������. </B>�������� ������� ����� � 2005 ���� ���������� � 2,35 ����. ������� �� ������ 2005 ���� ������� �� 8%, ������ ������� &#8212; �� 93%, ������ ������ &#8212; �� 104%, � ������� &#8212; �� 167 ���������. ��� ���� � ����� ������� ����� ������������� ���� ������������ �������. ����� ��������� ���������� ��� ������������ �������� �������� ���������� ����������. ���������� ���� ������ (����� ������ ������ ������, ��� � �������� ���������), �������� � �����, � 2005 ���� �������� 1200, ��� ������� � ������� ������� ���������. ��������� ����������� � ��������� ������ ������������� ������� �������� ��������� � �������� CONTACT. ����� ���������� �� ������� � 2005 ���� �������� 900 ���. ����., � � ������ � ����� 2005 ���� �� ���� 2006 ���� ���� ���������� ������ 1 ����. ��������. �� ������� CONTACT � ������ ������������ ����� 400 ������ � ���������� ����������� ������, ����� ���, ������ � �������� ���������. ����� �� ������� ������ � ���������� ��������� ������� �� ��������. � 2005 ���� �� ����� ����������� � 14 ���. � ������� ����� ���������� ����� ������ � ���. � ���� 2005 ���� ����, ��� ����� � ����������� ����� ������� CONTACT, ������� ������ ������������ �� ����������� � ������������� �������� �� ��������� � �������� ���������� ������� CONTACT � ������������ � Total Quality Management � ������������ ��������� �������� ISO 9001-2000. ���� ���� ������������ ��������� �� ����� ������� � ����� � CONTACT �� ������� ��� ������������� ������, ��� � ���������� ���������. � 1996 ���� ���������� ��������� ����� �������� ������������� �������� &#8220;������������������� �����&#8221;.</P><I>
<P align=right>����� ��������, ��������� ���������� ����������</P></I>


</body>
</html>

