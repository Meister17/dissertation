<!--
ID:37287366
TITLE:������ ������
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:
NUMBER:10
ISSUE_DATE:2006-10-31
RECORD_DATE:2007-02-15


-->
<html>
<head>
<title>
37287366-������ ������
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgcolor1>
<TD></TD></TR>
<TR class=bgwhite>
<TD>
<P align=justify><STRONG>������ �������<BR></STRONG><EM>������������ ������������� ��</EM> 
<P class=Subheader align=justify><STRONG>�� ����������� ������&#8230; </STRONG>
<P align=justify>�������-������� ������� ������������� � ���������� � ������� ���� ������� ����������� ����������� �� �������������� ���������. 
<P align=justify>&#171;�������� ���� ��� �������. �� ����������� ������, �� ���� ����������. �� ��� ���� ������ ���� ����������� ��������, ������� �� �������� ��� ��������� ���������������� ����&#187;, &#8212; ������ �� �� ��������� �������������. 
<P align=justify>�. ������� ����� ����������, ��� ���� ������ ������ ������� ����, ������������ �� �������� ������ �� �������� ��� ��������� ������� ���������: &#171;� �� ������� �������, ������� ��� ����� ��������� ����������� � ������� ���� ������ �������� (�������� �������������� � �������������� ���������)&#8230; � ����� ��� ������ �������� � ������� ������ ��������&#187;. 
<P align=right><EM>���������, 14 ��������</EM> 
<P align=justify><STRONG>��������� �����<BR></STRONG><EM>����-������� ������������� �� </EM></P>
<P class=Subheader align=justify><STRONG>��������� ����� ��� �������� �������� �������������� ������� </STRONG>
<P align=justify>� ��������� ���� ����� ������� ������� ������� ��������������, �������� �������������� �������, ��������������� ���� ������� �� ����������� ������ � ���, ������ ����-������� �. ����� �� ����������� UBS: &#171;��� ����������� ��� ����� ��������, ��������� ����� ��� �������� �������� �������������� �������, �� ����� ������ ������� ��� ����, ����� �� ���� ������� ��������������&#187;. �� ��� ������, &#171;�������������� ������� ������������ �����, ���� � ������ �������� ��� �� ����� � 2007-2009 ����� �� $27 �� �������, ����� ���������� �����, �������� ����� � $300 ����.&#187; 
<P align=right><EM>���������, 14 ��������</EM> 
<P align=justify><STRONG>������� ������<BR></STRONG><EM>������� �������� �� </EM></P>
<P class=Subheader align=justify><STRONG>������ � ��������� &#8212; ��� ������ � ��������� </STRONG>
<P align=justify>�������� �������� ������� ������� ������� �������������, ������ ������� �������� �� ������������� ��������� � ������ ���������. 
<P align=justify>������� ������ �������, ��� ������� �������� � ������ �� ������ ������� ��������� 5,1% �� ��������� � 5,5% �� ��� �� ������ 2005 ����. � ����� 2006-�� ������� ������� �������� ����� ��������� 7,7%. &#171;�� �����, ��� �������� ��� ����� ��������, � ��� ������, ��� ������� ����� �������� &#8212; ��� ������ ������ � ���������&#187;, &#8212; ������� ��. 
<P align=justify>�� ������ �. �������, ���� �� ������ � ������ ��� ������ ���� �� ������� ���� ����� ����� ����� ��������. ���, �� 1 �������� �������� ��������� 7,1%, � ���� &#171;����������&#187; ������������ &#8212; ����� 0,2%. 
<P align=right><EM>���������, 13 ��������</EM> 
<P align=justify><STRONG>����� ��������<BR></STRONG><EM>������ ����������� ������������ �������� ������� �� �� ��������� ������������ � ���������� ������ </EM></P>
<P class=Subheader align=justify><STRONG>����� ���������� ������ �� ��������� ��� ��������, �������� � ��������� � ��������� ������������� </STRONG>
<P align=justify>����� �������� ������ �����������, ��� ��������� ������������ &#171;�������� ���������� �������� ������������ ������� �� ���������������� ������������ ��� ��������, ����������� � ��������� ����� � ��������� �������������&#187; � ����������, ��� �������� ���������������� ��������������� ����������� ������� �� ��������� �� ������� ���� ��� ������������� � ������������� �����. ������ �������� ���� �� ����� ���������������� �� ���������� �������� �����, �������, �� ������ ��������, �������� � �������� ����������������� �������������� � ������������� �������������� ��������. 
<P align=right><EM>���������, 19 ��������</EM> 
<P align=justify><STRONG>������� �������</STRONG><BR><EM>������ ����������� ������������ ����� ������</EM></P>
<P class=Subheader align=justify><STRONG>���� ������ ����������� ���������� � ������� ����� $10 ����. �� �������� ����� </STRONG>
<P align=justify>��� ����� �. ������� ����������� �� ��������� ����������� �������� �������. �� ��� ������, ����������� �������� ������ ��������: � ���������, � ������ ��������� �� ������ $12 ����. � �� ������ ������ ������� ��������� �������� $3 ����. ����������. � ����� � ��������� ��������� ���� ������ &#171;�������� ������� �������������� �� �����&#187;. 
<P align=right><EM>���������, 19 ��������</EM> 
<P class=Subheader align=justify><STRONG>��������� �� ��������� ����������� �������� ������ ���������������� </STRONG>
<P align=justify>&#171;���� ��������������� �������� ��������, �� ������ ����������� ������ � �������� ������ ����������������&#187;, &#8212; ������� ������� �������. 
<P align=justify>����������� �������� �� �������� �������� ��������� 7,1%, ��� �� 0,3 ����������� ������ ����, ��� �� ��������������� ������ �������� ����. ��� ���� ���� ������ ���� �� ����������� ������� �� �������� �� 2006 ���, ������� ���������� 8,5%. ������ � ��� �� ���������, ��� ������� �� �������� ����� ���� ����������� ����� ��������� ������ �� ��������. 
<P align=right><EM>���-�������, 19 ��������</EM></P></TD></TR></TBODY></TABLE>


</body>
</html>

