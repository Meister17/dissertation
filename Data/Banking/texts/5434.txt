<!--
ID:36796191
TITLE:�� ����������� �� ����������� &#147;�������� � ����������� �������� ���������� ������������ � ������&#148;, �. ������, 21.02.2006

SOURCE_ID:3126
SOURCE_NAME:������� ����������� ����
AUTHOR:�.�������, ����������� ����������� ��������� �������, �������� ������������ �������� 
NUMBER:6
ISSUE_DATE:2006-06-30
RECORD_DATE:2006-10-12


RBR_ID:2039
RBR_NAME:���������� �������
RBR_ID:4928
RBR_NAME:��������� � ������� ��������
-->
<html>
<head>
<title>
36796191-�� ����������� �� ����������� &#147;�������� � ����������� �������� ���������� ������������ � ������&#148;, �. ������, 21.02.2006

</title>
</head>
<body >


<P class=a style="MARGIN: 6pt 0cm; TEXT-INDENT: 0cm"><STRONG>7-10 �������� � ���� ������� IV ������������� ���������� ����� &#171;����� ������ &#8212; XXI ���&#187;. � ������ ������ ��������� VIII ���������� ����������� &#171;�������� ������� � ������ � ��������� �������������� �����&#187;. ��������� ���������, ����������� ���� ����.</STRONG></P>
<P align=justify>�� ���������� ��������� ������� ������������ ������ &#8220;��������� � ���������� ����� &#8212; ��������� ������&#8221; �������������� � ������������ � ���������� ���������� �� ���������� ������������� �������, � ���������������� ����� �������� ��������� ������� ��������, �������� � ����� 2005 ���� �����, ���: &#8220;�������� ����������� ����������������� �������� � ��������� ������������� � ��������� �������&#8221;; &#8220;�������� ����������� �������� �������-������������� ��������� ��������� �������&#8221;; &#8220;�������� ����������� �������� ����������������� ���������&#8221; � &#8220;��������������&#8221;.</P>
<P align=justify>������� �� ���������� ������������� ������� � ��������� ������� � ������� ���� �������� 6404 ���. ���., � �. �. �� ���� ������� ���������� ������� &#8212; 5958 ���. ���., ������������ ������� &#8212; 446 ���. ������. � ������ ���������� ������������� ������� �������� ��������� �����������: � ����� ���������� ������� ���������� ��������� ������������ ����������� &#8212; �������������� ��������� �������� � ������ ��� ������������� ��� ������������ ����� � ������������ ������� ���������������� ��������� ��������; ������ ��������� ����������� ����� �������� ����� �������������� �������� ��� ������ ������� ������ �� ���������� ������� ������� ������ � ������� ������������ �� ����; � ����� ���������� ������� ��������� ������������� � ������������ �������� ������������ �������������� �� ���� ���� ���������� �������������� ������������� ����� 5 ����. ������.</P>
<P align=justify>��������� �������� ����� ���������� �� ���������� ����� �����������, ��� ���������� ���������� �������� ��� ������������� �����; �������������� ���������� ������ �� ������������ �������� �� ����������� ��������� �������� ������������ ���������������; ���������� �������������� ������� � ���������� ��������� �����; ������������ ������������ ��������������. ����� ����, �� ���������� ��������������� ������������ �� ����������� ������ ��������� ��������� ������� &#8212; ���������, ���������, �������, ���������� �� �������� �������, � ��������� ������� ������������� �������� � ������ 900 ���. ������.</P>
<P align=justify>� ���������� ������������ ������� ��������, ��� � �������� ����� �� ���������� ��������� ������� ��������� ����������� ��������� ���������� ��������� ������������. �� 9 ������� 2005 �. ���������� ������������ ������ ������� ������ ����� 2 ���. �������� (� �. �. ���������) �������� �� ����� 1 ����. 957 ���. ������. ��������, ��� �� 01.01.2006 �� ���������� ��� ��������� ������� ����������� � ����� ���������� 1 ���. 215 ���. ��. ������, ������� �������� �������� ������������ 11% �� �����������.</P>
<P align=justify>����������� ������ ����������, ��� ������� �� ���������� ������� ����������� ����� ��������. �������� ��������, ��� � � ����� �� ������ &#8212; ������ ������ ���������, ������� �������� � ������� ������ �� ��������. � ���� ����� ��������� ������� ��������������� �������� �������� �� ��������� ����������� ������ ����� ����������� ����������� ��������� �������� �������� ��� ����� �� ������� �������. ������� ���, ������������� ������� �������������� ����������� �� ��������� ���������� ��������� ��������������. � �������� ������� �������� ���������� ��������� ������������, � �. �. �� ������� ���������� ������������ ��������� ��������, ������������ ��������� ������������� �� ���������� ������ ��������� ���������� �������. �������� ��� ������� ���������� ������ ���������, ����������� � ��������� �������� ������� � ��������� ������������ ��������� ���������: ������� ����� � ������� ����������� � �������� ���������.</P>
<P align=justify>� ������ ������������ &#8220;����������� ������ ������� �����&#8221;, � ����� ����������� ��������� &#8220;���������� �������� ���� �� 2010 ����&#8221; �������������� �������������� �� ���� ������� ������������, ���������� � ������� �������� �������� �� ������������ �����, � �. �. �� ��������� ������� ������, ��� ��������� ���������� �������: 10% &#8212; �� ���� ������������ ������� � 30% &#8212; �� ���� ������������� ��� ��������.</P>
<P align=justify>� ��������� ����� ������������� ������ �� ��������� ���� ������ ������� �����, ��������� ��������� � ��������� ������� ��������� �� ����������� ��������� ������� � �������������� ������� ��������������, � �. �. ��������������� ������� ��������� ������ (���� �� ����� ������ ������� �� ����������� ������) � ������� 40% �� ���� ���������� �������.</P>
<P align=justify>� 2004 �. ������������� ��������� �������, ��������� ���� �������� ��������� ������������� � ��������� �� ���������� ��������� ������������ (����) ��������� ������������� ���������� � ��������������, � ������������ � ������� ��������� �������� ���� �������� ������������ ���������� �� �������� ������� ������������� ���������� ������������. � 2005 �. ����� ���������������� ��������� �������� �������� �� ���������� ���� � ��������� ������� �������� 9,5 ���. ������. ��������� 16 ��������� ��������, ����������������� ������ &#8220;��������������&#8221;, ��������������� ���������� � �������� ���������� ������.</P>
<P align=justify>� 2006 �. � ��������� ������ ��������� ������� � ������������ � ��������� ��������� ������ 5 ������. ����������� ����� ���������� ��������� ������������ �� ��������� ��������� � ������� � 2006 �. ������ ��������� �� 986 ���. �� 1 ����. 232 ���. ������. ��� ����������� ������ ������ ��������� �� ��������� �����, ����������� �������������� ��������� ������� �� ��� ���� �� ���������� �������. � ����� ��������� ���������� ���������������� ��������� �������� �������� � ������������ �� ����������� ��������� ����������� ������ ��������� �������� ���������� ��������� ������������ � ��������� ������� � ������ ������������� ������ &#8220;� �������� ������� ��� ���������� ��������� ������������ � ��������������� ��������� � ��������� �������&#8221;. ������ ������������� �������� ���������� � � ��������� ����� ����� �������� �� ������������ ���������������� ��������.</P>
<P align=justify>��������� �������� ������������ �����, �� ��� ������, ����������� �� ������ ������. ������������� ��������� ������� ������ ������� �������� ������� � ��������� ������ ���� �������� �������, ����������� ����������� ��������� ����������� �� ���������� ������������� ������� &#8220;��������� � ���������� ����� &#8212; ��������� ������&#8221;. ���, �� ���������� �������-���������� ����� ��������� ������, ������� ������� ������, ������� ������������� ����������� ����� �� ��� ������� � ���������� ������������ ��������. ��� ����������� ������� ������ ����� �� ���������� ������������ ��������, ���� ����� ���� ���������� ����������� ������.</P>


</body>
</html>

