<!--
ID:37137445
TITLE:���������������������: ������� ��������� � �� ������
SOURCE_ID:3144
SOURCE_NAME:������ � ������
AUTHOR:�. �. ����, �������� ������������ ����������-�������� �������� ����� ������, �. �. �����, ��������� ������ ����������� ���������� �������� ������ ���������� ������������ ��������������������� ����
NUMBER:12
ISSUE_DATE:2006-12-31
RECORD_DATE:2007-01-11


RBR_ID:4935
RBR_NAME:������ � ����������� ������������ �� ��
-->
<html>
<head>
<title>
37137445-���������������������: ������� ��������� � �� ������
</title>
</head>
<body >


<P align=justify>������ ��������, ��� ��������������������� �� ��� �������, ������� � ������� ������ ���������, ������������ ��������� ������ �������� � �����������. �� ������ ��� �������� � ���� ������. ���� �������, ����� ���������� ����� ���� ����������� � ������������, ���������� ����������� ��������� ��������� ���������� ��������.</P>
<P align=justify>� ���� ��� �������� ����� ������ �� ��������� �� ����������� ���������� ���������������� � ��������������� �������������, �� ��� �� ������, ��� ������ � ���� ���� ����������. ���������� �������� ����� ��������� ����� ��������� ������, �������� ������� � ������������ ������. ������� � ������ ��������������������� ��������������� ��� ������ ������������.</P>
<P align=justify>�������� ������� ��������, ������ � ��������� �������������� ��� ���������� ��������������������� ���������� �����������, ������� ������� �������� &#8220;��������������&#8221;. ��� �������, ��� �������������� ������������ ������� � ���������� �������������� �������� � ����������� ������� ��� � ����� ������ �������� ������, ��� � � �������� ����������� �� ���������.</P>
<P align=justify>���������� ����� &#8220;�������������&#8221; ��� ������ � ����������������������, ����������, ���������� ��� ���������� � ����������� ������� �������� � �������� �����. ��� ��������� ����������� ��������� � ��������������������� ���� ������ ��������� �������� ���������� ���������� �������� ������, ���������� � ���������� ������� ������. � ������ ���� ������ ����������� ������������ ����� �������� � ������������ �������� ��� ���� ������������ ���������, ����������� ������� ������������ ����������� � �������� �������� �������� ���������. ���������� �������� ����� ������������ �� �������� ������������� � ������. ����������, ������ �������� ��� ���������� ����������� ��������� ������ &#8211; �������� ������ ������.</P>
<P align=justify>� ������ ������� � �������� �������� ����������� �� �������� �������� � �������� �������� �������, ��������� ��� ���������� ������ ��������� ��������� ������: �� ��������� � ��������� ������� ������ ���������� �������� �������� ������� ���������� ���� ��� �� ������������� ��������, ��� � �� ��������� �������. ������� ��-�� ����� ���������������� ���������� ������ � ���������� ����������� �� ������������, � ���� ������ � �������� � �������� �������� ������.</P>
<P align=justify>������� ���������� ����������: ��� ��������� ������� ���������� ��, ��� ����������� ��������� �������� ������� � �������� ������ ������������� �������� ������. ��������� ���������� ��������� ����������� ������, ������������������ � ���������� ������� ������, ��� �� ���������� ���������� ��� ��������� �� ���������� ������. ��� ����������� ���, ��� �� ���� ������������� ��������� � ��������� ������ ��������� ��� ������ �������� ����������� �����, ��� ������� ����������� �� ���������� � �������� ����������� ������.</P>
<P align=justify>���������� ��������� ���������������������, ���������� ������ ������, ����������, ��� ������ � ��������� ������������� ������������ ���������� ���������� ���������� �������� ������ ����� ������ ��������� ���������� ������� ���������� ������������������. � �� ����������� ����� ������ ��� ��� ���������� ���� �� ����� ���������.</P>
<P align=justify>���������� �������� �������� ������� ����� ������, � �������������, � �� ������� ���������, ����� ���������, ��� ������� ������������� ������������ ��������������� ���� ����� ������� ������������������: ���������� ������ �����������. ������� �� ����������� ���������� � ���� ������ ���������� �������� ������� �������� �������� ��������� ���� ����������� �����, ���� ��������� � ������ ���������, � �������� ������������ ���� �� ��������, ������� �������������� ������������� ������. ����� ����, ���������� � ��� ����� �������� ������� ������ �������� �������������� ��������� �����������. � ����� � ���� �������� ����� ����������� ������������ ������� ������� ���������, ��������� ������� � �������� �� ����������� ����� ��������� ��������.</P>
<P align=center></P>
<P align=center>���. 1. �������� ��������� ���������� ���������� �������� ������ ����� ������, ���������� � ���������� ������� ������ �� ��������� 10 ���</P>
<P align=justify>� ������� ��������� ���������� ���������� �������� ������ ����������� ���������� ������������� �������� �������� ���������. ������ ����� ��� ������� � ���, ��� ���������� ������������ ��������� �������� ������� �������� �������� �������� ��� ������� ������� ����������� ���������� �����. ����� ������� ����������� � �������� 90-� ����� �������� ����, �� �� ����� ���������� � � ���� ���. ���, ���������� � 2005&#8211;2006 ��. ���� ���������� �������� � ��������� ������ �������� ������ ��������� �������� ������� ������� 1997 �. ��������� 1000 ������. ������ ���� �������� �� ����� �������� � ���������������� �����, �������� �������, ���������� � ��-����� �������� ������� � �������������� �������� �������� �����; �������� ������� ������� ����� ������ ������ ������ � ����������; ���������� ������������� � ������� ���������. �� ���������� ������������������ �������, �������������� � ��������� �������� ����������, ������������ ������� ���� ��������� ������� ������������� ������� ������� � ���������� �������. � ������ ������� ��� ������� ���, ��� ������ �������� ���������� � ����������� ���������� ������� � ��������������� ���������� ������� � ���������� ������� �������� ���������� ����������� ��������� ���������� ����� ����� � ������ ����������, ����������� �������� �� ������� �������. ��� ��������� ������������ ������������ ����� ���������� ����� ��������� �����. ����� �� ����� ���� � �������� ���� ��������� �������� ������ ����� ��������� �� ������ � � ������ ������������ ��������.</P>
<P align=justify>����� �������, ���������� ������ ���������� � ������ �������� ���������� ���������� ����� ������� �� ������ ����, ������� ����� ������ ���� ������������ &#8211; �������������� �������������� ������������. ������ ��� ��������������������� �������������� �������� �������, ��������� ����������� ��������� ���������������� ����������� ����������� ���� ������� ��������� ���������, ��������� ���� �������� �������� ������������ ���������� ������� ���� ����� ������� ���������.</P>
<P align=justify>� �������� ��� ���� ������ ���� ������������ �������� �������� ������� �� ������� ���������. �������� ���� ��������� ������� ��������� �������� ������, �� ������� ������������� ����������� �� ����� ���� ����������������� ��������� �����������. ��������� ��������� ������������ ����� ��������, �� ������� ����������� �������������� 1 &#8211; 2 ��������. � ��� �� ��������� ��� ����� ������� �� ����������, �� �������� ��������. �&nbsp;���������� ��������� ����������� ����������� ����� �������, ���������� ������� ����� ����� �������, ������� ������������ �������� � ���, ��� �������� ��������. �� ������� �����, ��� �� ��������� ����� ����� ������� ��������� ���������� ������ ����������� �������.</P>
<P align=justify>����, � ��������� ������� ������� ������ ��������� ��������� �� ������� ��������������������� ����� ���� ���������. ������������ �� ������� ����� � � ������� ����������� ��������� � ��������� ��������, ������� &#8220;��������� �� ����������&#8221; �� ����������. �������� ��, �� ������������ ����� ��������� ���������� �������� ��������. ������ ��� ��������� ���������� � ������ &#8220;����������������&#8221; ���������� ����������, ��� �� �������� ������ �� ��������� �������� �������, � ��� �� ��������� ��������� ���� ������� �������� ����������� �������.</P>
<P align=justify>��� �� ������� �������� ������� ��������� �������� ��� ��������� ���������? ��� ����������� ������������� ���������� ������ �������������� �������������� ���������� ������� ���������������������, ��������� ���� ��������. ��� ���� ��� ����������� � ������������� ������� ������� �� ��������� ��������.</P>
<P align=justify>�� ������������ ������ ��������������� �����, ����� ���������, ��� � ������������ � ����������������� � ������������ ������ ��������� �����, � ��� ����� � ������, ������ �� ���������� �������� ������ ����� �� ����, � �������� ������� �������� ���� ���������� ��������. ����������, ��� ���������������� � �� ������ ��������� ��������� ����� � ������� ����������� �������� � ����� �����, � ����� � �������� ���������� ����������: �������������� ��������� ��������� ���, ��� �� ������ �������� � ����� ��������.</P>
<P align=center></P>
<P align=center>���. 2. �������� ��������� ������� ��������� ���������� ������� ����� ������</P>
<P align=justify>������ � ���, ��� ���������� � ����������� ������������, ������������ ��� ������<SUP>1</SUP>, � ����� ��������� �� ����������������� � ������� ������ ������� ������������ ��������� ��������� �� ������������ �������� �� ���� ��������� �� ����� � ���� ������������ ������������ ������ � �� &#8220;�������������&#8221; �� �����������. ���������� ����� ������� �������� ���������� �����-���� �������� ����������� �������� ������ ��� ������� ������� �� ������. ������ � ����� ����� ������� ����� � ���, ��� ���������� �������� ����� ������� ���� � ������ ������� �������������� �������.</P>
<P align=justify>������� �������� ��� ������ ������ ��������������������� ����� ���� ���������� ���������� �������� ������, ���������� � ��������� ������� ������. ����� ����, ��� � ���������� ��� ������ ����������� ��� ������������ � ������ ��������, ������� ��, ������� ������ ��������������� � ������������ ��� ������������, �������� � ��������������� (�.&nbsp;�. �� ��������� ����� ���������), �������������� �������������� ������������ �������, ����������� ������ ������: ���������� ���������� �������� ���� �� ��������� �������� �����, ������� ���� �������� � ��������� ������� ����������� ������������ ������ � ������ ������� �����.</P>
<P align=justify>���������� �������� �������� ������� � ����� ������� �������� ����� � ���������. � ����� ����, ���������� ���� ����������� �������� ����� ���������� ����� � ������ � ��������� ������� �������� ����� � ��������� �������. ������� ��� ������� ������ �� ������� ��������������������� ������� ���������� ��� ��� ����������. ����� ����������� ����� �� �������� ��������������� ������� ����� ��������� ���������� ��������, �.&nbsp;�. �������, ��� ����� ��������� ����� ����������� � ��������� � ������������ �����. ��� ��������� �������� � ������ � � �������, �������� � ���� ��������� ������ ����������� ������, ���� ���������� ������ �� ������� ��������� � ���������� ���������.</P>
<P align=right>�&nbsp;�&nbsp;�&nbsp;�&nbsp;�&nbsp;�&nbsp;� 1</P><B>
<P align=center>����������� ������� �������� � ��������� ������� � ��������� � ������ � ��</P></B>
<TABLE cellSpacing=1 cellPadding=7 width=100% align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="51%">&nbsp;</TD>
<TD vAlign=top width="25%">
<P align=center>������</P></TD>
<TD vAlign=top width="25%">
<P align=center>��������</P></TD></TR>
<TR>
<TD vAlign=top width="51%">
<P>���������� ��������</P>
<P>� ���, ��.</P></TD>
<TD vAlign=bottom width="25%">
<P align=center>54 556</P></TD>
<TD vAlign=bottom width="25%">
<P align=center>579 000</P></TD></TR>
<TR>
<TD vAlign=top width="51%">
<P>����� ������� �</P>
<P>���������, ���� ��.</P></TD>
<TD vAlign=bottom width="25%">
<P align=center>5,0</P></TD>
<TD vAlign=bottom width="25%">
<P align=center>10,7</P></TD></TR>
<TR>
<TD vAlign=top width="51%">
<P>������� ������������� ���������� ��������</P></TD>
<TD vAlign=bottom width="25%">
<P align=center>1:100 000</P></TD>
<TD vAlign=bottom width="25%">
<P align=center>5:100 000</P></TD></TR></TBODY></TABLE>
<P align=justify>�� ������� �����, ��� ���������� ���������� �������� ������� � ������ �������� ����������� ���������� ����������� �� �������, �������� � ���� ����. ������, �� ������ ������, ���� �� �������������� ������ ����� �� ���������� ������ � ������, ����������� ������ �� ������� ��������� ������������� �������. ��� ��������, ������� ����� � ������������ �������������� ������� ������ � ����� �� ��������. ������� �������������� �������������� ������ ����� ����������� �����������, ������� �������� �� ����������� ������� ������, ���������� ���������� ��������� ������� ���������.</P>
<P align=justify>��� ���� ��������� ����, ������ �� ��������� ����� ������ ������ ����� ��������. ������� � ��� ������ ������������ ������������ ��������� �� ������ ����� ��������� �� �������� �������, �������� �� �������������� �������. �������, �������� �������� �� ��� �������� ������� ������� ���������.</P>
<P align=justify>��������� �������� ���������� ������� ��������� ��� � ����� ������, ��� � �� ������� �������� �������� ������������, �� ����� �� ������������� ������������� ����� ������� ������� ������� ��������. ������������� ����� ���������� �������� ��, ��� ��, �� ������ ������ ����������, �������� ������� �������� �������������� ����� ��������� �����������.</P>
<P align=justify>�� ������ ����������� ������ ��������������� ���������� (www.gks.ru), � 2005 �. ������� ������� �������� �������� ������� �������� 8&nbsp;554,9 ���. ��� ������������� ����� ���������� �� �������� �� ���� ������������ �������������� ��������� �� 2005 �., �������������� ����������� �������������� ��������� Mercer Consulting (www.mercer.com). ������������ ���� ������ �������� ��, ���, ��-������, � ��� ��������� �������� ������� �������� �� ����� ��� �� ��������� �������, �, ��-������, ��� ���������� ������ ����������� �� ������������, �������� � ���� ���� (�� 53&nbsp;577 ���� � ������� �� 18&nbsp;889 ���� � ����������).</P>
<P align=justify></P>
<P align=center>���. 3. ����������� ��������� � �������� ������� �������� � �������, �������� � ���� ����</P>
<P align=justify>������� ���������� ������ �������� �� ���� ���������� ������� ���� �� ������������, ��������� ��� ����� �� ��������� ������� ���������� ����� ��������� �����-���� ������. �������, ��� ���� ���������� ������� � ����� ����������� ��������� �������� ��������� � ��������� ����������� �������, ����� ���������, ��� �� �������� ���, ����������� ��� ������-���� �����������, ����� ������������ ��� ��, ��� � ����������� ��������� ����� ����������� � ����� ������������ ��������� ���� �����, �������� � ���� ����. � ����� � ���� ���. 3 �������� ������� � ����������� ��������� ���������� ����������� ����������, ������������ ������ ������. ������, ����� � �������� �������� ������������ ������������� ����������� ���������, ����� ��������� ���������� �������� ������� �������� ��� ���� �����, �������� � ��������. ��� 2005 �. ��� ��������� 39&nbsp;505,65 ���� � ���, ��� 3&nbsp;292,14 ���� � �����.</P>
<P align=justify>����� �����, ������������� ���������� ������� �����, ��������� ���������� ��������� ��������. �������� ��������� �������� �������� ������� ����� ���� ��������� ��������, ���������� �� �� �������� ��� � ����� ���������� ���������� ��������� �������.</P>
<P align=right>� � � � � � � 2</P><B>
<P align=center>������������� ���������� � 2005 �. ���������� �������� ������ ������ � �� �� ���������</P></B>
<TABLE cellSpacing=1 cellPadding=7 width=100% align=center border=1>
<TBODY>
<TR>
<TD vAlign=top colSpan=13>
<P align=center>������</P></TD></TR>
<TR>
<TD vAlign=top width="23%">
<P align=justify>�������, ���.</P></TD>
<TD vAlign=top width="14%" colSpan=2>
<P align=center>10</P></TD>
<TD vAlign=top width="13%" colSpan=2>
<P align=center>50</P></TD>
<TD vAlign=top width="11%" colSpan=2>
<P align=center>100</P></TD>
<TD vAlign=top width="13%" colSpan=2>
<P align=center>500</P></TD>
<TD vAlign=top width="13%" colSpan=2>
<P align=center>1000</P></TD>
<TD vAlign=top width="13%" colSpan=2>
<P align=center>5000*</P></TD></TR>
<TR>
<TD vAlign=top width="23%">
<P>�������� ���, %</P></TD>
<TD vAlign=top width="14%" colSpan=2>
<P align=center>1</P></TD>
<TD vAlign=top width="13%" colSpan=2>
<P align=center>3</P></TD>
<TD vAlign=top width="11%" colSpan=2>
<P align=center>20</P></TD>
<TD vAlign=top width="13%" colSpan=2>
<P align=center>10</P></TD>
<TD vAlign=top width="13%" colSpan=2>
<P align=center>66</P></TD>
<TD vAlign=top width="13%" colSpan=2>
<P align=center>0,01</P></TD></TR>
<TR>
<TD vAlign=top colSpan=13>
<P align=center>�������� (������ �������� 2005 �.)</P></TD></TR>
<TR>
<TD vAlign=top width="23%">
<P>�������, ����</P></TD>
<TD vAlign=top width="12%">
<P align=center>5</P></TD>
<TD vAlign=top width="11%" colSpan=2>
<P align=center>10</P></TD>
<TD vAlign=top width="11%" colSpan=2>
<P align=cente
r>20</P></TD>
<TD vAlign=top width="11%" colSpan=2>
<P align=center>50</P></TD>
<TD vAlign=top width="9%" colSpan=2>
<P align=center>100</P></TD>
<TD vAlign=top width="11%" colSpan=2>
<P align=center>200</P></TD>
<TD vAlign=top width="11%">
<P align=center>500</P></TD></TR>
<TR>
<TD vAlign=top width="23%">
<P>�������� ���, %</P></TD>
<TD vAlign=top width="12%">
<P align=center>1</P></TD>
<TD vAlign=top width="11%" colSpan=2>
<P align=center>6</P></TD>
<TD vAlign=top width="11%" colSpan=2>
<P align=center>16</P></TD>
<TD vAlign=top width="11%" colSpan=2>
<P align=center>62</P></TD>
<TD vAlign=top width="9%" colSpan=2>
<P align=center>9</P></TD>
<TD vAlign=top width="11%" colSpan=2>
<P align=center>5</P></TD>
<TD vAlign=top width="11%">
<P align=center>1</P></TD></TR>
<TR>
<TD vAlign=top colSpan=13>
<P align=center>�������� (������ �������� 2005 �.)</P></TD></TR>
<TR>
<TD vAlign=top width="23%">
<P align=justify>�������, ����</P></TD>
<TD vAlign=top width="12%">
<P align=center>5</P></TD>
<TD vAlign=top width="11%" colSpan=2>
<P align=center>10</P></TD>
<TD vAlign=top width="11%" colSpan=2>
<P align=center>20</P></TD>
<TD vAlign=top width="11%" colSpan=2>
<P align=center>50</P></TD>
<TD vAlign=top width="9%" colSpan=2>
<P align=center>100</P></TD>
<TD vAlign=top width="11%" colSpan=2>
<P align=center>200</P></TD>
<TD vAlign=top width="11%">
<P align=center>500</P></TD></TR>
<TR>
<TD vAlign=top width="23%">
<P align=justify>�������� ���, %</P></TD>
<TD vAlign=top width="12%">
<P align=center>1</P></TD>
<TD vAlign=top width="11%" colSpan=2>
<P align=center>6</P></TD>
<TD vAlign=top width="11%" colSpan=2>
<P align=center>28</P></TD>
<TD vAlign=top width="11%" colSpan=2>
<P align=center>56</P></TD>
<TD vAlign=top width="9%" colSpan=2>
<P align=center>5</P></TD>
<TD vAlign=top width="11%" colSpan=2>
<P align=center>2</P></TD>
<TD vAlign=top width="11%">
<P align=center>2</P></TD></TR></TBODY></TABLE><SUP>
<P align=justify>*</SUP> �������� �� ��, ��� �������� ����� ������ ��������� 5000 ������ ���� ������� ���� � �������� 2006 �., �������� � ������ ���������, ����������� �� ������ �������� ������� ������� 1992 �., ����������� ��� � 2005 ����.</P>
<P align=justify>����� ����, ����������� ����������� ���� ��������� �� ����� ����� (www.ecb.int) �������� �� ����������, &#8220;������� �������&#8221; �� ������ ��������� 2005&nbsp;�. � �������, �������� � ���� ������ ����������� ������, �������� 53,3 ����, � �� ������ &#8211; 55 ����. �������� �������� ������ ���������� ���������� �������, ���������� � ������� ����� ���������, ����� ������� ������������� �������� �� 54 ����. ���������� �������������� &#8220;������� �������&#8221; �������� ���������� ������. �� ������ 2005 �. �� �������� 727 ������.</P>
<P align=justify>���� �������� �������� ������� ������ �����, �������� ���������, ����� ���� �� ��� ���������� ������� �����, ��������� ����������� ����������. ��� ����� �������� ��� �������� ���������� 0,016, ��� ������ &#8211; 0,085. ���������� �������� � ����� �������� ���� ������������ ��������������, ������� ������������� ���������� ��������� ��������� ����� � ������.</P>
<P align=justify>����� �������, ����� ������� ������ �� ������� ���������� �������� ������ ����� ���� ��������� ��� ������������ ���� �������� � ��������� ������� �� ����������� ���������� ����������. � ����� ��������� ������� ����� ���� �������� � ��������� ����:</P>
<P align=justify></P>
<P align=justify>��� <I>F</I> &#8211; ����� ���������� ���������� �������, ������������ �� ���;</P><I>
<P align=justify>n<SUB>i</I></SUB> &#8211; <I>i</I>-� ������� �������;</P><FONT face="Times New Roman CYR">
<P align=justify>&#957;</FONT><SUB>i </SUB>&#8211; �������� ��� �������� ������� <I>i</I>-�� �������� �������;</P><I>
<P align=justify>V<SUB>b</I></SUB> &#8211; ���������� ������� � ���������;</P><I>
<P align=justify>L<SUB>��</I></SUB> &#8211; �������� ������� ���������� �����.</P>
<P align=justify>� �������� ��������� �������� ���������� ������������ ����������� ��������� �������������� ������������ ������������ ����� � ���, ��� �� ����������� ���� ����� ���������� �������� �������� ���� � ����������� ������� �������������� � ��������� ����� �� ��������� ��������� ���������� �� ������� ������ �� ������� ��������� ������ ������������ �����. ��� ��������� ������� ����� � ���, ��� �� ������� ������ ��� �� ������� ���������� ��������, ����������� � ������ � ��������� ����.</P>
<P align=justify>������ � ������, ���������� ��������, ��� ����� ���� ��� ���� ����� ������������ �� ��������� ���� ������� ������� ������ ��������� 1,87<FONT face=Symbol>&#180;</FONT> 10<SUP>-7</SUP>. ������ �� ����������� ���� ��� ������� ����� � 4 ���� � ���� ��������� ����������� ����������� ���������� �����, �������� � ���� ����. �� ���������� ������� � � ���, ��� ������� ��������������������� � ������ ���������� ���������� ���������� ������� ����������� ��������������� ���������� ������ ������� �������. �������� ������������ ������, ���� ������ � �������� 2004 �. ���� � ��������� ���������������� ��������, ���������� ��������� ���������� �������� ���������. ����� �������� �������� �� ���������������� �������� ������� ������������� ������ ����� �� ���������, ��������� ���� �������� �� ����������� ���������� ����� ����������� ����� � ��������. � ��� ������������ ���� ��������� �������� �������������� ����� ���������� �������� � ������.</P>
<P align=justify>������� ��������, ��� ��������� ��������� ������� ������ ����� ����� ������������ ��� ������ ������� ���������� ��������, ������������ ��� �������� ����������� ������� ������� � �������� ���������� ��� ��� ���������� ���������������������. ���� ������� �������� ���������� �� ��������� ��������� ����������� ��������, �� ����������� ���� ����� ����������� ���������������� ������ ������� � ������� ������, �.&nbsp;�. ����� 5 &#8211; 7 ��� ����� ����� � ��������� �������� ������� ���������� �����. ����������� ������� ������ ����� ���������� �������� ������ ��������������� � ���, ��� �� ������������ ����� ��������� ���������� �������� ��� ���������� ������ ���������������������. � ���, ��������, ����� ������� ����� ���������� ����� ������������ ���� ������� ��� �� ������ ������ �������������� ��������.</P>
<P align=justify>����� ��������� �������������� ����������� ������������� ������� �������� �������. ������ ��� ����, ������� ���������, ����� ���������� ��������� ���������������� �������: ���������� ����� �������� ��� ������������ ��������� ������� ���������� �����������. ��� ����������� ���, ��� �������� � ��������� ����������� �� ������� �� ���������� ��������� ��������� ����������� �� ����� ������������ ����������. ��������, ���, �������� ����������� � �������������� ������� ���� �����������, ������ ���������� ����� �������� �� ��� ��������, ��� �������� ��������� ������������ ���� �������� ���������.</P>
<P align=justify>������� ����, ����� ������� ����� � ���, ��� ��� �������� ���������� ���, ������������ �� ������������ ���������������������, ����� ���� �������� ������� ������ �������� �� ������� ���������� �����. � �������� ��������� �������������� ���� ������� ������ �������������� ��������� ������ ������������ ��������� ����������, ������� ������� ��������� �� ������ ����� �������� � ���������, �� � �� ���������� ���������. ������ ���������� ������ ����� ���� ������ �� �������� ������ � ������� �������������, ������������ ��� �������� ����������� ������ ������� � ����������������� �������� ������.</P>
<P align=justify>� ��� �� ����� ������������ ������� ��� ������� ���������� ������� ������ ����� ��������������� ���� ��� ���� �� ��������� ��������� ������ ��������� ���������������������. ������ �� � ���� ���� �� �������� ����� �� ������������� ���� �������� �� ������ ��������. ��������, ��� �������� ���������� ���� ������� ������������ ����� ����������� ������ ��� ������������ � ������� ������������� � ��������� ���� ������� ������ �������� �� ������� ���������� �����.</P><SUP>
<P><FONT size=2>1</FONT></SUP><FONT size=2> ��.: ��������� �.&nbsp;�. � ��. ��������� ������ (���������������������) / ��� ���. ����. �.&nbsp;�.&nbsp;��������.&nbsp;&#8211; �.: �������. &#8211; 2002.</FONT></P>


</body>
</html>

