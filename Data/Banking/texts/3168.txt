<!--
ID:35462109
TITLE:���������� � ����������� � �������������� ��������� ����������� �� 01.07.2005 �.*
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:17
ISSUE_DATE:2005-09-15
RECORD_DATE:2005-10-27


-->
<html>
<head>
<title>
35462109-���������� � ����������� � �������������� ��������� ����������� �� 01.07.2005 �.*
</title>
</head>
<body >


<TABLE cellSpacing=2 cellPadding=9 width=614 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top colSpan=5><B><FONT face="Times New Roman CYR,Times New Roman">
<P align=center>����������� ��������� �����������</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=center></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=center>�� 01.01.2004</FONT></P></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=center>�� 01.07.</FONT><FONT size=2>2005</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>1. ���������������� ��</FONT><FONT face="Times New Roman CYR,Times New Roman" size=1>*</FONT><SUP><FONT face="Times New Roman CYR,Times New Roman" size=3>*</SUP></FONT><FONT face="Times New Roman CYR,Times New Roman" size=2> ������ ������ ���� �� ��������� ��� ������� �������������� �������������� �������, �����<SUP>1</SUP></B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>1 666</B></FONT></P></TD>
<TD vAlign=top width="15%"><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>1 458</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>� ��� �����:</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify></FONT><FONT face=Wingdings size=2>l</FONT><FONT face="Times New Roman CYR,Times New Roman" size=2> ������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>1 612</FONT></P></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>1 406</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify></FONT><FONT face=Wingdings size=2>l</FONT><FONT face="Times New Roman CYR,Times New Roman" size=2> ������������ ��</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>54</FONT></P></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>52</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<DIR><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>1.1. ���������������� �� �� 100%-��� ����������� �������� � ��������</P></DIR></FONT></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>32</FONT></P></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>39</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<DIR><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>1.2. ��, ������������������ ������ ������, �� ��� �� ���������� �������� ������� � �� ���������� �������� (� ������ �������������� �������������� �����)</P></DIR></FONT></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right></P>
<P align=right></P>
<DIR>
<P align=right>4</P></DIR></FONT></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right></P>
<P align=right></P>
<DIR>
<P align=right>1</P></DIR></FONT></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>� ��� �����:</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify></FONT><FONT face=Wingdings size=2>l</FONT><FONT face="Times New Roman CYR,Times New Roman" size=2> �����</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<DIR>
<DIR><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>3</P></DIR></DIR></FONT></TD>
<TD vAlign=top width="15%">
<DIR>
<DIR><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>0</P></DIR></DIR></FONT></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify></FONT><FONT face=Wingdings size=2>l</FONT><FONT face="Times New Roman CYR,Times New Roman" size=2> ������������ ��</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<DIR>
<DIR><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>1</P></DIR></DIR></FONT></TD>
<TD vAlign=top width="15%">
<DIR>
<DIR><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>1</P></DIR></DIR></FONT></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify><FONT size=2><STRONG>2. ������������ ��, ������������������ �� 01.07.2002 ������� ��������</STRONG></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT size=2><STRONG>2</STRONG></FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right><STRONG>2</STRONG></P></TD></TR>
<TR>
<TD vAlign=top colSpan=5><B><FONT face="Times New Roman CYR,Times New Roman">
<P align=center>����������� ��������� �����������</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>3. ��, ������� ����� �� ������������� ���������� ��������, �����<SUP>2</SUP></B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>1 329</B></FONT></P></TD>
<TD vAlign=top width="15%"><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>1 281</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>� ��� �����:</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify></FONT><FONT face=Wingdings size=2>l</FONT><FONT face="Times New Roman CYR,Times New Roman" size=2> �����</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>1 277</FONT></P></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>1 232</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify></FONT><FONT face=Wingdings size=2>l</FONT><FONT face="Times New Roman CYR,Times New Roman" size=2> ������������ ��</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>52</FONT></P></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>49</FONT></P></TD></TR>
<TR>
<TD vAlign=top colSpan=5><I><FONT face="Times New Roman CYR,Times New Roman">
<P align=right>�����������</I></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=center>�� 01.01.2004</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=center>�� 01.07.</FONT><FONT size=2>2005</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>3.1. ��, ������� �������� (����������), ��������������� ����� ��:</P></DIR></FONT></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify></FONT><FONT face=Wingdings size=2>l</FONT><FONT face="Times New Roman CYR,Times New Roman" size=2> ����������� ������� ���������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>1 190</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>1 116</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify></FONT><FONT face=Wingdings size=2>l</FONT><FONT face="Times New Roman CYR,Times New Roman" size=2> ������������� �������� � ����������� ������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>845</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>830</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify></FONT><FONT face=Wingdings size=2>l</FONT><FONT face="Times New Roman CYR,Times New Roman" size=2> ����������� ��������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>310</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>310</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify></FONT><FONT face=Wingdings size=2>l</FONT><FONT face="Times New Roman CYR,Times New Roman" size=2> ���������� �������� � �������������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>����������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>5</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>4</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>��������<SUP>3</SUP></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>176</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>182</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>3.2. �� � ����������� �������� � �������� ��������, �����</P></DIR></FONT></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>128</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>133</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>�� ���:</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>�� 100%-���</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>32</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>38</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>����� 50%</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>9</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>8</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>4. ������������������ �������� ������� ����������� �� (��� ���.)</B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>362 010</B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>403 638</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>5. ������� ����������� �� �� ���������� ��, �����</B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>3 219</B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>3273</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>�� ���:</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>��������� ������<SUP>4</SUP></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>1 045</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>1 010</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>������ �� 100%-��� ����������� �������� � ��������<BR>��������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>15</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>21</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>6. ������� ����������� �� �� �������, �����<SUP>5</SUP></B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>4</B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>3</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>7. ������� ������-������������ �� ���������� ����������<BR>���������</B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>0</B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>0</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>8. ����������������� ����������� ���������� ��, �����<SUP>6</SUP></B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>219</B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT size=2>
<P align=right>405</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>� ��� �����:</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify></FONT><B><FONT face=Wingdings size=2>l</B></FONT><FONT face="Times New Roman CYR,Times New Roman" size=2> �� ���
������� ���������� ���������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>176</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT size=2>
<P align=right>360</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify></FONT><B><FONT face=Wingdings size=2>l</B></FONT><FONT face="Times New Roman CYR,Times New Roman" size=2> � ������� ���������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>30</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>32</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify></FONT><B><FONT face=Wingdings size=2>l</B></FONT><FONT face="Times New Roman CYR,Times New Roman" size=2> � ������� ���������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>13</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>13</FONT></P></TD></TR>
<TR>
<TD vAlign=top colSpan=5><B><FONT face="Times New Roman CYR,Times New Roman">
<P align=center>����� �������� � ���������� ����������� ���</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT size=2><STRONG>9. ��, � ������� �������� (������������) �������� �� ������������� ���������� �������� � ������� �� ��������� �� ����� ��������������� ����������� ��������� ����������� <SUP>7</SUP></STRONG></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT size=2><STRONG>335</STRONG></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT size=2><STRONG>178</STRONG></FONT></P></TD></TR>
<TR>
<TD vAlign=top colSpan=5><I><FONT face="Times New Roman CYR,Times New Roman">
<P align=right>�����������</I></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=center>�� 01.01.2004</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=center>�� 01.07</FONT><FONT size=2>.2005</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT size=2><STRONG>10. ������� ������ � ����� ��������������� ����������� ��������� ����������� � ���������� �� ��� ������������ ����, �����<SUP>8</SUP></STRONG></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT size=2><STRONG>1 416</STRONG></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><STRONG><FONT size=2>1 631</FONT></STRONG></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>� ��� �����:</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=left><B><FONT face=Wingdings size=2>l</B></FONT><FONT face="Times New Roman CYR,Times New Roman" size=2> � ����� � ������� (��������������) ��������</P></FONT></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT size=2>1047</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT size=2>1258</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify></FONT><B><FONT face=Wingdings size=2>l</B></FONT><FONT face="Times New Roman CYR,Times New Roman" size=2> � ����� � ��������������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>364</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>372</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>�� ���: � ����� �������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>0</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>0</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>� ����� �������������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>364</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>372</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>� ��� �����: </FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>����� �������������� � ������� ������ ������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>326</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>330</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify>������������ � ������ ������ (��� ����������� �������)</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>38</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>42</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=justify></FONT><B><FONT face=Wingdings size=2>l</B></FONT><FONT face="Times New Roman CYR,Times New Roman" size=2> � ����� � ������������ �������� ���������� � ����������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR,Times New Roman" size=2>
<P align=right>4</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><B><FONT face=Wingdings size=2>l</B></FONT><FONT face="Times New Roman CYR,Times New Roman" size=2> � ����� � ���������� ���������������� � ����� ������ ��������� ��������</P></FONT></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1</P></TD></TR></TBODY></TABLE>
<P align=right><FONT face="Times New Roman CYR,Times New Roman"><I>"������� ����� ������", </FONT>2005<FONT face="Times New Roman CYR,Times New Roman">, &#8470; 39</P></I></FONT><FONT face="Times New Roman CYR,Times New Roman"><SUP><FONT face="Times New Roman CYR,Times New Roman">
<P align=justify><EM>*</EM></SUP><EM> ���������� ������������ � ��� ����� � �� ��������� ��������, ����������� �� ��������������� ��������������� ������ �� �������� ����.</EM></P><B>
<P align=justify><EM>��������� � �������.</EM></P></B><SUP>
<P align=justify><EM>*</EM></SUP><EM>* �� &#8211; ��������� �����������. ������� "��������� �����������" � ��������� ���������� �������� ���� �� ��������� �������:</EM></P>
<DIR>
<DIR></FONT><FONT face=Wingdings>
<P align=justify><EM>l</EM></FONT><FONT face="Times New Roman CYR,Times New Roman"><EM> ����������� ����, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� ������� � ������� ����� �� ������������� ���������� ��������;</EM></P></FONT><FONT face=Wingdings>
<P align=justify><EM>l</EM></FONT><FONT face="Times New Roman CYR,Times New Roman"><EM> ����������� ����, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� �������, �������, �� ���������� ����� �� ������������� ���������� ��������;</EM></P></FONT><FONT face=Wingdings>
<P align=justify><EM>l </EM></FONT><FONT face="Times New Roman CYR,Times New Roman"><EM>����������� ����, ������������������ ������� �������� (�� ���������� � ���� ������������ ������ "� ������ � ���������� ������������") � ������� �������� ����� ������ �� ������������� ���������� ��������.</EM></P></DIR></DIR><SUP>
<P align=justify>1<EM> </EM></SUP><EM>����������� ��, ������� ������ ������������ ���� �� �������� ����, � ��� ����� ��, ���������� ����� �� ������������� ���������� ��������, �� ��� �� ��������������� ��� ����������� ����.</EM></P><SUP>
<P align=justify>2 </SUP><EM>����������� ��, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� ������� � ������� ����� �� ������������� ���������� ��������, � ����� ������������ ��, ������������������ ������� �������� � ���������� �������� ����� ������ �� ������������� ���������� ��������.</EM></P><SUP><FONT face="Times New Roman CYR,Times New Roman">
<P align=justify>3 </SUP>�������� � ������� 1996 �. � ������������ � ������� ����� ������ �� 03.12.1996 &#8470; 367.</P><SUP>
<P align=justify>4 </SUP>����������� ������� ��������� ������, ��������� � ����� ��������������� ����������� ��������� ����������� � ���������� ���������� ������. �� 01.01.1998 �. � ����������� ���������� � ��������� ������������ �� ������ ������ ����������� ����� ���������� ���������� ��������� ������ &#8211; 34 426.</P><SUP>
<P align=justify>5 </SUP>����������� �������, �������� ����������� �� �� �������.</P><SUP>
<P align=justify>6 </SUP>� ����� ���������������� ���������� �� �� ������� �������� �����������������, �� ������� ��������� � ���� ������ ����������� �� �������� �� �� �������.</P><SUP>
<P align=justify>7 </SUP>����� ���������� ��, � ������� ������ ������ ���� �������� (������������) �������� �� ������������� ���������� �������� (������� ��, �� ������� � ����� ��������������� ����������� ��������� ����������� ������� ������ �� �� ����������), &#8211; 1447.</P><SUP><FONT face="Times New Roman CYR,Times New Roman">
<P align=justify>8 </SUP>����� 01.07.2002 � ����� ��������������� ����������� ��������� ����������� ������ � ���������� ��������� ����������� ��� ������������ ���� �������� ������ ����� ��������������� ����������� ��������� ����������� � ����� � �� ����������� �������������� �������������� �������.</P></FONT></FONT></FONT></FONT>


</body>
</html>

