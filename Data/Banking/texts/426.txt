<!--
ID:37289189
TITLE:�������� ������ ����������� ������ �������� � ������� ��������
SOURCE_ID:3096
SOURCE_NAME:����������� ���������
AUTHOR:�.�. ������, �������
NUMBER:12
ISSUE_DATE:2006-12-31
RECORD_DATE:2007-02-15


-->
<html>
<head>
<title>
37289189-�������� ������ ����������� ������ �������� � ������� ��������
</title>
</head>
<body >


<P align=justify><STRONG><EM>� ������ ���������� ������������� ����������-�������� �����, ������������ ����� �������� � ������� �������� ������������ ������. ����������� ����������� ���������.</EM></STRONG></P>
<P align=justify>� ��������� ����� ������������ ����� ������������ ����� ���� �� ����� �������� ��������� ���������� ����������� ����������� �����. � ����� ������������� ����������� �������� ���������-������������� ������������ ������������� ����� ������ �������� � ������� ��������, ����� ������� ��� �� ����� ������ �����, ��� � ��� ��� ��������� �������������, ��� ��������������� � ���, ��� ��� �������� - ������� �������� ������ ����������� �����. � ����� � ���� ������� �������� ����������� �������������, ������� � ��������������� ������������ � �������������� ����������, �� ������ ������� ��������� ����� ������ �����, � ��������� ������������ �����, ��������� �������������� ������� � ����� ��������������, ������������, ����������� ���������� �������� � ������ �������� � ������� ��������.</P>
<P align=justify>���������� ������������ ������������� ���������� � ����������� ������������� ���������������� ���������� ��� ���������������� ��������������� ����������� ������� ����������� ��������, ������� ������� �� ���� �������� �����������: ���������������� ����������� �������� (�� ������� ����� ������ � ������ �������������� �������) � ������������������ ����������� �������� (����������� � �������� ������ � ������ ���� ������������������ ��������).</P>
<P align=justify>������������� ������������� �������� ������ ��������� �����������, � ��������� �� ��������� � ������� ��������, �������� �������� ����� ����������� �������� � ������������ ������������ ��������� �������� ����������, �������������� ����������� �����������. ����� �������� � ������� �������� ������������ �� ������� ��������������� � ����������� �����, ������������ ����������� ������������, � ��� ����� �� �������� (����������) ����������� ������������.</P>
<P align=justify>����������� � ������� ������ ������������ �������� ���������, ������������ ����������� ������������, �� ��� ������:</P>
<P align=justify>��������������� ���� (����������� ����� �� 7.08.01 �. &#8470; 119-�� (� ���. �� 2.02.06 �.) "�� ����������� ������������" � ��.);</P>
<P align=justify>����������� ��������� ������������ �������;</P>
<P align=justify>����������� ������� (���������) ����������� ������������.</P>
<P align=justify>�������� ������ &#8470; 119-��, ��������� � <STRONG><EM>������ ������</EM></STRONG>, ����� ������������ ����� ������������������� ������������ �� ����������� �������� �������������� ����� � ���������� (�������������) ���������� ����������� � �������������� ����������������. ����� ������������� ���������� �����������, � ��� ����� ���������, ���������� � ����� ��������� ������ � ������������� ���������� (�������������) ���������� � ������������ ������� ������� �������������� ����� ���������������� ���������� ���������. ����� �������, � ����������� ���������� ������ ���� ������������� ������ ����������� ����������� �� ������ � ������������� ���������� (�������������) ���������� ����������� ����, �� � � ������������ ������� ������� ��� �������������� ����� ���������������� ���������� ���������. � ������ &#8470; 119-�� ����� ���������� ����� � ����������� ����������� ����������� � ����� � ����������� ���������� ��� � ���, ����������� ������� �������� ����������� �����.</P>
<P align=justify>����������� ������� �� 2.12.90 �. &#8470; 395-1 (� ���. �� 27.07.06 �.) "� ������ � ���������� ������������" �����������, ��� ������������ ������ ��������� ����������� �������� �������� ����������� �������� (������������� ������). � ������ ������ ���������� �������� ������������� ���������� (�������������) ���������� ��������� �����������, ���������� �� ������������ ����������, ������������� ������ ������, �������� ���������� � ��������� ����������� ��������.</P>
<P align=justify>� ������ ��������������� ����������, �������� � ������ ������, ����� ������� ����������� ����� �� 8.08.01 �. &#8470; 128-�� (� ���. �� 27.07.06 �.) "� �������������� ��������� ����� ������������", ������� ���������� �������� �������, ��������� � ��������������� ����������� ������������, ������� �������� ������������� ��������������, ���������� ������������� �������, ������� ������������� �������������� (�������� � ����� �������� ��������, ������������ ���������� � �������, �������� ������� � ������ �������� � ��.).</P>
<P align=justify>�� <STRONG><EM>������ ������</EM></STRONG> ����������� ����������, ������������ ����������� ������������, ������� ����������� ���� ������������ ������ �� ���������������� ������������� ����������� ������������ (������������� ���������� ���������), � ����� ����������� ��������� ������� ������, ������������ ������� ���������� ����������, �������� � ��������� ������������ ���������, ������� ������������ ���������������� ����������� ��� ������� ������, ������������ � ������ ������ �� ����������� ������������ ������� ������ � ��.</P>
<P align=justify>����������� ���������, �������� � <STRONG><EM>������ ������</EM></STRONG>, ��������:</P>
<P align=justify>����������� ������� (���������) ����������� ������������;</P>
<P align=justify>���������� ������� (���������), ����������� � ���������������� ����������� ������������, � ����� ������� (���������) ����������� ����������� � �������������� ���������. ������� (���������) ����������� ����������� � �������������� ��������� ������� �������� ���������������� ����������� ������.</P>
<P align=justify>����������� ������� (���������) ����������� ������������ ������������ �� ������ ������������� ���������� ��������� � ����� ������������ �������� ��� ����������� �����������, �������������� ���������, � ����� ���������� ���, �� ����������� ���������, � ��������� ������� �������, ��� ��� ����� ���������������� ��������. ����������� � ���������� ��������� ����������� ������� (���������) ����������� ������������ ���������� �������������� ������������� ���������� ��������� �� 23.09.02 �. &#8470; 696 (� ����������� � ������������ �� 4.07.03 �., 7.10.04 �., 16.04.05 �., 25.08.06 �.). � ��������� ����� ���������� � ��������� 31 ����������� ������� (��������) ����������� ������������. ����������� ������� (���������) ����������� ������������ ������������� ���� � �������� �������� ������ ���������� (�������������) ����������, � ����� ���������� � ����������������, ������������ ������, ����������� ��������������, ���������� ����������� �������������, ����� � ���������� ������������ ����������, ������� ����������� �������� �������� ������, ������ ����������� ������ � ������� ����������� �������� ���������� ��� � ��.</P>
<P align=justify>���������������� ����������� ������������, � ����� ����������� ������������ � �������������� ��������� ��������� ������������� ��� ����� ������ ���������� ������� (���������), ������� �� ������ ������������� ����������� �������� (����������) � ���������� ������� �� ����� ���� ���� ���������� ����������� ������ (����������) ����������� ������������.</P>
<P align=justify>���������� ������� ������������ ������������� ����������� ������������ � �����, ���������� �������� ��� �������� �����, ����������� ��������������� � ����������� �������� �������� ������������ ������ � ������� ��������. ����������� �������� ����� 4 ������ ����������. � ������ ������ ��������� ��������� <STRONG><EM>������������ ��������</EM></STRONG> (����������� ������, ������������� ������������� ���������� ���������), ���������� ������������ ��� ����� ��������� �����������, ��� � �������� ������ � ������� ��������, � ��� �����:</P>
<P align=justify>�� ��;</P>
<P align=justify>����������� ����� �� 2.12.90 �. &#8470; 395-I (� ���. �� 27.07.06 �.) "� ������ � ���������� ������������";</P>
<P align=justify>����������� ����� �� 25.02.99 �. &#8470; 40-�� (� ���. �� 20.08.04 �.) "� ����������������� (�����������) ��������� �����������";</P>
<P align=justify>����������� ����� �� 26.12.95 �. &#8470; 208-�� (� ���. �� 27.07.06 �.) "�� ����������� ���������";</P>
<P align=justify>����������� ����� �� 22.04.96 �. &#8470; 39-�� (� ���. �� 27.07.06 �.) "� ����� ������ �����";</P>
<P align=justify>����������� ����� �� 16.07.98 �. &#8470; 102-�� (� ���. �� 30.12.04 �.) "�� ������� (������ ������������)";</P>
<P align=justify>����������� ����� �� 11.11.03 �. &#8470; 152-�� (� ���. �� 27.07.06 �.) "�� ��������� ������ �������".</P>
<P align=justify>�� ������ ������ ����������� ���������� ��������� <STRONG><EM>����������� ���������, ������������ ������ ������</EM></STRONG> (���������, �������, ����������, ��������, ������, ������������ ������������), � ������ ���������, �������� ������������ ���������� ������������� ������� �������� � ������� ��������, � ��� �����:</P>
<P align=justify>������� ������� �������������� ����� � ��������� ������������, ������������� �� ���������� ���������� ���������, ������������ ���������� ����� ������ �� 5.12.02 �. &#8470; 205-� (� ���. �� 11.04.05 �.);</P>
<P align=justify>��������� ����� ������ �� 9.07.03 �. &#8470; 232-� "� ������� ������������ ���������� ������������� �������� �� ��������� ������" (����������� �� 1 ���� 2006 �.);</P>
<P align=justify>��������� ����� ������ �� 20.03.06 �. &#8470; 283-� "� ������� ������������ ���������� ������������� �������� �� ��������� ������" (��������� � 1 ���� 2006 �.);</P>
<P align=justify>��������� ����� ������ �� 26.03.04 �. &#8470; 254-� (� ���. �� 20.03.06 �.) "� ������� ������������ ���������� ������������� �������� �� ��������� ������ �� ������, �� ������� � ������������ � ��� �������������";</P>
<P align=justify>���������� ����� ������ �� 16.01.04 �. &#8470; 110-� (� ���. �� 20.03.06 �.) "�� ������������ ���������� ������";</P>
<P align=justify>��������� ����� ������ �� 30.12.99 �. &#8470; 103-� (� ���. �� 1.06.05 �.) "� ������� ������� �������������� ����� ��������, ��������� � �������� � ���������� ���������� ������������� �������������� � ���������� ������������";</P>
<P align=justify>������ ���� ������ �� 16.03.05 �. &#8470; 05-4/��-� (� ���. �� 12.01.06 �.) "�� ����������� ���������� ������� ������ ����� � ����������� ���������� ������ �����";</P>
<P align=justify>������������ ���� ������ �� 26.02.99 �. &#8470; 195-� "�� ����������� ������������ ������������ �� ���������� ����������������� ����������� ����� ������ ����� ������������ ������ "�� ������� (������ ������������)".</P>
<P align=justify>���������, �������� � ������ ������, <STRONG><EM>��������������� ��������������� ������������� �������</EM></STRONG>. ��������, ������ ����������� ����� ���� ��������� �� �������������� (����������) ��������, ��������� � ������������, ��������� �� ������ � ������� ��������, ������� ������� ���������� (��������������) ������������, ��������� � ������������ �������� �� ��������� ������ ��� ����������� ������ �����. ���������� � ����������� ���������� ���������� � ������������ ������ ������������ � ������� ������������ � ����������� ����������������� � ������������ ������ ����� ������.</P>
<P align=justify>��������� ������ ����������, ��� �������, ��������� <STRONG><EM>������� ��������� ��������</EM></STRONG>. ��� ����� ���� ������������ ���������� ����������� �����������, �����, ������� ��������� ������, ������� ������������ ������ �� ����������� �����������, ������ ���������� ����������� ��������, ������� ������ ��� ���������� ��������� ����������� �������� � �.�. ������� ��������� �������� ������������ ���������� � �������� ����������� �������� ��� � ����� ����������� �����������, ��� � ��������������� � ����������� ������������ �����.</P>


</body>
</html>

