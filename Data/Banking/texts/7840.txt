<!--
ID:37999307
TITLE:����� ��� �������?
SOURCE_ID:10908
SOURCE_NAME:������������ ���������� ������
AUTHOR:
NUMBER:6
ISSUE_DATE:2007-06-30
RECORD_DATE:2007-08-07


-->
<html>
<head>
<title>
37999307-����� ��� �������?
</title>
</head>
<body >


<DIV class=article>
<DIV class=body>
<UL>
<LI>������ � ������ �������� ������ ���� ����������� ��������? 
<LI>�������� �� &#8470;115-�� ���������� �������� ��������� �������? 
<LI>��� ���������� ��� �������� ������� ����������� ��������? 
<LI>����� �� ������������ ��������� �������? </LI></UL></DIV>
<H2><FONT size=3>������������ ��������� ������� ����� ����� </FONT></H2>
<P align=justify><B>����� �������</B>, ������������ ���������� ���������� � �������� ������� ��� &#171;���������&#187;</P>
<P align=justify>����� ����������� �������� ������. �� �� ����, �� ������ ������, �������� ���������� ��� ���� ���������. �������� ���������� ����� �������� ������� � ��������� ������ ������ � ���������� ���������� � &#171;��������� ���������� � ��������&#187;. ������, ������� � ����� � �������� ����� ������ ����, ��� ��������������� ������������� ����������� �� �� ������ ���������� ���������, ��� � �������� �����, ��� ��������� ����� ����� � ������ ������.</P>
<P align=justify>�������� �������� ������� � ������������� ������� �������� �������������� ������������ ����. ���������, ��� �������, ��������� ���� �������� ���, ��� ��������� ��������� �����������. ������� ����������� ���������� ������������ ��������������� ��������������� � ���������� �������� ����������� ������ ���� (������������ �������������� ���������� ���������) � ����������, � ������� ��� ����� � ��������� ����������� ��������� ����� ������������ ���� ���, ��� ��������� ������ ���� ������.</P>
<P align=justify>����� ��������� ���� �������, �������� �������� ������ ���������� ����������. ������� ���� ����������� ���� �������, ����������� � &#171;���������&#187; ������. ������, ������ ������, � ���������� �������� ������ ��������� �������� ������ �� ���������� �����������. �� ����� ��� ����� � �� ������������. ��� ��������� �������� ����� ������ ����.</P>
<P align=justify>����� ������� ����������� �������� �� ����� � ����� ��������? ���� ���� ���� � ������� � ������� ������� �����������, ����������� �� ������ ������, � ��������� ����������� ���������� � ������������ �� ���������� ������������ ������� ��� ���������� ����������� � ���������� �� � �������� ������, �� ���, ��� �����, ��� ��������.</P>
<P align=justify>���� �� ����� ����� �������, ��������������� �� ���������� ��������� ����������� � �������, �� ����������� �������� � �������� � ���������� ����������� �������� ��� ���������, �� ����������� ��������� ����. � ��� ����� � ����������������� ���������.</P>
<P align=justify>����������, ����� � ��������� ������� ������ ���������������� ���� ����������� � ��������������� � ���������� �����. ������ ���� ��������� ���� �� �������� ��������� ����� ����������� ����������, ����� ����������� ����� �� ��������, ��������, 1% �� ������� ������ �������������� ��, ��������, ��� ��� ����� � ��������� �� ��������� �������� � ������ ����� � ������� � ����������� ����� �� ���������. ������� �������� ���������� ����������������� ������� (� ����������� ��������� � ��������� �������������� ������������� ������) ����� ���� ������ ����������� � � �����.</P>
<P align=justify>������������ ������� ����� �����, ���� �� ������ �� ����� ���� ����������� ��������� ��������� ���������� �������, ����������� � �������, ���������� ��������� ������������� � �������� ��������������� �� ����������� ������� � ���������� ���������������� �������� � ����������, ������������ ������� � ��������� ��������������� ���������������� ��������������-�������� �����.</P>
<H2><FONT size=3>����� ������ ���������� ����������� ������� </FONT></H2>
<P align=justify><B>�������� ������</B>, ����������� ���������� ���������� ��������� �������� ����� &#171;�����������&#187;</P>
<P align=justify>���� �������� � �������� ������� ���, �� ��������� ���� �� ����������� �����, ������ �����, ��������� ���������� �������� - ������ � ���� ���������� �������� - �������, ����������� - ����������� ��������, ������. �������, ������, �������� ������� ����������� ������� �� ���������� ������, �. �. ����� ���������� ������� ����������, ��������� ���������� ������� � ������ ������� � ���������� ����������� ���������� ���������� ��������, � ������ ������� �� �������� ������������� � ������������ ��������.</P>
<P align=justify>� �� ������, ��� ����� &#8470;115 ����������� ���������� �������� ��������� �������, � ���� �� ������� ��, ������� �������� ���, ����� ��� ������� �� ����. ������������� ���� ���, �����, ����������, ����, �� ��������� �������, �������� �������� ���� ��� ����� ���������� ������� ��� ���������� &#171;�����������&#187;.</P>
<P align=justify>��� �������� ������� ����������� �������� ���������: �� ����������� �� ��, ��� �������� ������, - ������������ ���������� �������� ���������, �� ������ - �������� �������� � �������������� ������ ��� ����������� ��������, ����� ����, ��������, ������ �� �������� ����� � �������� ���� ������ �� ����������� ����������� ��������, ����������� �� ����������� ���� ���������� ��������.</P>
<H2><FONT size=3>����� ������� �������� ��������� ��������� ����� </FONT></H2>
<P align=justify><B>������� ���������</B>, ����������� ���������� ���������� ���������� �������� ����� &#171;���������� �������&#187;</P>
<P align=justify>��������� ���������� ����� ���� ����������� �������� ����� �������, ��-������, ������������� ������������ �����������: ����������� �������� � ����� ������������ ������������� ��. ��� ������� ������ � ������ ���������� ��-�� �������������� �����, �� ���� ���������� ����� ����������� ���������� �������� ������� ����� ������������.</P>
<P align=justify>��-������, ���������� � ��������� �������� - ���������� ����������� ����, �������� ��� ���������� &#171;������������&#187;. � 80% ������� ����������� ����� &#171;���������� �������� �� ����� - �������� - ������ ��������&#187;.</P>
<P align=justify>��� �������� ����������� ����������� �������������� ������� ����������� �������� �� ������� ����������� � ������������� ����������� ������ � �������� �������� ���������� �������� � ��������� ��������� ���������.</P>
<P align=justify>������� ��������� ���������� ��������� (���������� ����� ���� �����) � ���, ����� �������� ����� � ����������� �������� ����� $20 ���. (���� ������ ������������� �����) ���� ������� ��������� � ������ ����e���� �����.</P>
<P align=justify>������ ���������� ��������� ��������� ��������� ������� � ������ � ������������� �������� � �����, ��������� ��������� ��������� ��������, ��������� ������������� �����, ������������� � �������� �������������� ������� ��� ������ � �������. ��� ��� ������� ������� ��������������� ������� � ����� � ������� ��������� �������� � ������ ����� ����������������.</P>
<H2><FONT size=3>��������������� ������ ������������ � &#171;������������&#187; </FONT></H2>
<P align=justify><B>���� �������</B>, ������������ ������ ����������� �������� ��� &#171;��� � ���� ����&#187;</P>
<P align=justify>�������� �������� �������� ������ ����� ������������ ��� ��������� �������� �������. ���, ������ ��������� �� �� ��������������� ��������� ����� ������������ ������� �� ������������ ������ �������� ��� ����� ��������� ���������� �������� ����� &#1028;15 ���. � ���� ����� �� �������� �������� ����� ������� ������ � �������� ���������.</P>
<P align=justify>��������, � ���� �������, ������������� �������� �������������� � ���������� � �������� ��������, ������� ������ ���������� ������� ����� � ������ �� �������� �������� ���������� �������� ��������. ��� ���������� ��� ����, ����������� �� ��� �������� ���������� ��������� ����� �� ��� ������ �������, � ��� ������ ��������. ���� ��������� ��� � ��������� ������ �����, ��������������� ������� �������� ��������� ��������, ����� ��, ��������, ��� ������ ������ ��� ��������. �, ��� ���������, ��������� ��� ������� �������� �� ������� ������, ������� �� &#171;��������� ��������������&#187;. ��������� ���������� ����� ������ ������: �������� �� ���������� �������� � ��������� � ����������� ������� �������������� ��� ���. ����� �������, �������� ������ ����������� �����������, �������������.</P>
<P align=justify>������ �������� �������� � ��, ��� ������� ���-�� ���� � ����� � ����������� � ��������� ���� �� ������ �� ����������� �������� �� ��. ������������ ���������� ������ ��������� ������ ���������� �������� � �������������� ��������� � ����, � ���������� �������� ��������� � ������� ������. ����� �������, � ����������� ��������������� ����� � ���������� ������ ���������� ����������� ��� ������������ �������������� ��������� �������.</P>
<P class=autor align=justify>����� �������� ��������� &#171;���-����������&#187;, ���������� ��� ���</P>
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<DIV class=vrez2>
<H3><FONT size=3>������ </FONT></H3>
<P align=justify>������� � ����� � �������� ����� ������ ����, ��� ��������������� ������������� �� �� ������ ���������� ���������, ��� � �������� �����, ��� ��������� ����� ����� � ������ ������</P></DIV></BLOCKQUOTE></DIV>


</body>
</html>

