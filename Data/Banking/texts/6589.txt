<!--
ID:37467215
TITLE:���������� ������� ����������� ������� � ��������� �������� �����
SOURCE_ID:3144
SOURCE_NAME:������ � ������
AUTHOR:�. �. �����������, ����������� ���������� ���������� ������������ � ��������������� �������� �� &#147;��������� �� ����������� �������&#148;
NUMBER:3
ISSUE_DATE:2007-03-31
RECORD_DATE:2007-03-29


-->
<html>
<head>
<title>
37467215-���������� ������� ����������� ������� � ��������� �������� �����
</title>
</head>
<body >


<P align=justify>� ���������� ��������������� ��������� �� ������ ������� ��������������� ������ ���������� ��������� � ������ 2003 �. ��������������� ����� ������������ �������� ��� ������ ����� &#8220;� ����������� ������� ���������� ��� � ������ ���������� ���������&#8221;<SUP>1</SUP>.</P>
<P align=justify>�������� ���������, ��������������� ���������� ������� ����������� �������, ����� ���� ���������������� ������ �� ������������� ���� ������������ ������ &#8470;&nbsp;177-�� � ���������� ���������, � ��� ����� � �������� ����������������� ����������, ����������������� � ������������ � 2001&nbsp;�. ������� ���������� ������������ &#8220;������������� �� �������� ����������� ������ ����������� ���������&#8221;<SUP>2</SUP> � ������� ������ ���� ��������� �������� ����� � �������������� ��������� �����, �����, ��� ��.&nbsp;������, �. ��������-����, �.&nbsp;������ � ��.</P>
<P align=justify>� ������ ������ ��������������� � �������� �� ��������������, ������� ��������� � ��������������-�������������� ���������� �������.</P>
<P align=justify>� ������� ������������� �������, ����������� ������������ ������������� ��������������� ��������� � �������� � �������� ������ ����������� ���������<SUP>3</SUP>, � �������� <I>������</I> ������������ ������������ ������� ��� �������� ������� ����������� ��������� ������� �������� �������� <B><I>�������� ���������� �������� � ���������� ����� �������� ������������ �����</B></I>. ���, � ������ �������, �������� ������������� ����, ����� ����������� ������� �� ��������� �������� ����������, � ����� �������������� �������������� ����������� ������������ ������������ ���������� �������.</P>
<P align=justify>���� �������� � ������ ��������� � ���������� ������� ����������� �������, �� ������ �� ����������� � ���, ��� ������������ ����������� ��������� ������� ������ � ������� �� ������ ���������� � ����� ��������� �������������� ���������� ������������ ������������ ���������� �������, � ��������� ��������� �� ����������� ������� ��� � �������� ����������� ������������ (�����������) ��� ����������� ������ ������ �������������� ���������� ����������������� ����������� �� �����.</P>
<P align=justify>��� �������� �������������� �����, ������� ���������� ����� �������� ����������� �������, � ������ &#8211; ������ ���� � �������� ��������� ���������� ���������� ������ � �������������� ����������� ���������� ��������� � ���������� ������� ���������� ���������, �� ��� �������� ������ �������������. � �������� �������������� ����� ����� �������� ��������� �����. � ��������� ����� ��������� ����������� ��������� ������� ����� 90% ������ ���������� ���, ��� ���� �� ����� ��� ���������� 62,4% �� ���������� ������ ������� ��������� � ���������� ������<SUP>4</SUP>. � ���, ��� �������� ������� ����������� ������� ������������� ����������� ���������� ��������� � ���������� �������, ��������������� ��, ��� �� ������ � ������ 2004&nbsp;�., ����� ������� ������ �����������, �� ���� 2006&nbsp;�.<SUP>5</SUP> ����� ������� ���������, ������������ �������, ����� � 1518 ���� ���. �� 3126 ���� ���., ��� �� 106% (����� ��� � ��� ����).</P>
<P align=justify>������� ��������, ��� ����� ��� �������� ������� ����������� ������� �� ��������������� ��������� ���� ������, ������������ �������� ���������, � ������������ ��������� ������� � �������, �����������������, � ��� �����, ���������� �������� ������ ������ ������ �� ������� ���������� ��� �������������� ������������� ��������� ��������, ����� � ����� ����������� �����, ���������������� � ���������������. ���� ��������, ���������� ���������� ������� ���������� � ������� ����� � ����������, ���������� ������������ ����������, ������������� ������ ������, � ����� ����������� ���������� ������������, ����������� �� ���� ����������: ������������� ��������, �������� �������, �������� ����������, ���������� �������� � ����������� (������ ������� CAMEL), &#8211; ������������� ������� �������� ������ ���������������� ������.</P>
<P align=justify>���������� �� �� &#8220;��������� �� ����������� �������&#8221; ������� �������������� ����������� ������ ����� ������������� ��������, ����������� � ����� ���� ���������� �����, ����� ������� ����� ������� ���, ������, �����, ������, ���������, ������� � ������ ������. ����� �������, ���������� ������� ����������� ������� �� ������ ����� � �������� ���������� ���������, ��� ���� ������� � ����.</P>
<P align=justify>���������� �� ����� ���������� ��������� ����������� ��� ���� � �� �������� ������ ���������������� � ���� ���������, ��� �� ����� ������������ ����, ����� �������� �����, ��� ���������� � ������ �� ���������� ������� � ����������� �������, ������������ ���������� ����������� ��������� � ��������� ������, ������� ��� �������� ���� �������� ������� � ������, ��� ����� ����� ��������� ����� � �������� ��������� ����� ���������� � ����� ������ ����������� ������ ��������.</P><I>
<P align=justify>������</I> ������������� �������� <B><I>�������� � ������ �������� ������������� ������������ ������� ����������� ��������� � ������������ ���������� �������� �����</B></I>, ���������� ����������� ����������������, ������������������� �������� � �������� �������. ���������� ����� � ����������� ������� �������� ��������� �������� ����� ������� ����������� �������, ������� � ���������� ������, ������������ �� �����������������, ����������� ������������ ����� ������ &#8211; ��������� �� ����������� �������, �������� ��������� � ��������, ������������� ��� ����������� ���������� �����, ������� ����� �������� ����������� �������. ���� �������� � ������ ������� ��������� &#8211; ���������� ����������� ���������� ��������������� ��������� �����������, �� ��� ����� ���������������� �������������� &#8211; � ����������� ������ &#8220;� ����������������� (�����������) ��������� �����������&#8221;, � ����� � ������ ��������������� � ��������� ����������� �����.</P>
<P align=justify>��� �������� ������� � ������ ����������� �������� ��������������, ��, ��� ���������� ������ ��� ���� ���������������� ���������� ������� ����������� �������, �������������� � ��� �������� ������� ��������� ������������ ����������� ���������� �������� ����������� ������� ����������� �� ��� ����� � �����. ����� ����, ��������� ��������� �� ����������� ������� ��� ��� ������ �������� ���������� ��������������� �������� ������������ ���������� ����� �������� ���� � ����������������� �������������������, ������� ��������, �������� � ������� ���������� ��������������� ������, � ����� ������ ���� ���������� � ������ ��������� ���������� ������.</P>
<P align=justify>���� ���������� � <I>�������</I> ������������, ���������� <B><I>�������� ������� �������� ��������� ������� ���������� ���, ������������ �� ������������ ���� ������������� ���������� ������������������ ������</B></I>, �� ����������� ���������������� �������� ���� ������ ���������� �������� ������������ � ������ �������, � ����� �������� �����, ����������� ����������� ���� ���������� ��������� ������������ ����������� �� ���������� ��� ��� ���� ������� � ������������ ��������� �����������. � ����� ������������� ������ ������������ ����� ������, �� ��� ������, ��������������� ���������� ����� ������, �������� �������� �� ����� ���������� ���������� ��������, � ����� ���������������� � ������ ��������� ������������.</P>
<P align=justify>� �� �� ����� � ��������� ������� ����������� ������ ��������� �� ����������� ������� ���������� ����� ��� �������, � ��� ����� ���������� ������������� ��������������� ����������� ����������������� ����, ������������ �� �������������� �������������� �������� ���������� � ������������� ���������� ������ �� ������ ������� � ��������������� ����������� ��������� �����������.</P>
<P align=justify>��� ���������� ��������, ������� �����, ���������� ����������� ������� ��� ��������� ���������, ����� ������������ ������������� �������� �������, ������� ���� ����������� �������, ���������� � ������� ���������� ���������������� ������� �� ������ �������� ������������� ��� ��������������� ����������. ��� �����, �� ��� ������, ������� ���������� ��� ��������� �������� � ������������ � ������������� ������������ � ��������� ����� ���������� ����������� �� ���������� �����.</P>
<P align=justify>������� ������� ������, ������������ ����������������� ������� ����������� ���������, � ���������������� ���������� ������, ������� ��������, ��� ������ ������ ���� �� ������� ������ ����������, ���� ��������� �������� ����������� ��������� �� ����������� ������� � ������ �� ����������� ������������ � ������ ������ ������� ������, ������������ ���������� ���������, ��������������� ������ ������ (� �������� ���������).</P><I>
<P align=justify>���������</I> ������������ &#8211; <B><I>��������������� �� ������� ������� ����� ���������� ����������</B></I> � ������ ����������� � ������ ������. ���� � ������� ������ ����������, �������������� ���������� ���������, � ���� ����������� ����� �������� 2 ���� ��� �� ���� ��������� (� ������� �� 1,61), �� � ������ �� �������� 2006&nbsp;�. ��� ���������� ����� 0,66, � ����� ���������� ������ ���������� �� 190&nbsp;���.&nbsp;���. ������� �� 1,05&#8211;1,1 ��� �� ���� ���������, ��� ����������� ����� ������ �������������� ������ � � ������� ���� ���� ������������������<SUP>6</SUP>. � ����������� � ����� 2007&nbsp;�. ������� ���������� ���������� �� 400&nbsp;���. ���. ���������� ������� ����������� ������� ����� �� �������, ��������������� �������������� &nbsp;&#8211; ����� 2 ����� ��� �� ���� ���������.</P>
<P align=justify>���� �������� � ����������� ��������� � ������� ����������� ��������� ����, ��� ����������, ����������, ��, �������� ������������ ������������� ���������� �����������, �������������� ����������� �� ��������� ����� ������������ �������� � ������� ������ ������, ��������� � �� ��������� ������� ���������� ����������� ������, ���� ������ ������� � ������ ����� �����������, ���������� �� ���������� ���������.</P><B>
<P align=center>������ �� � ��������� � ������� �� ����������� ������ ���������� � ��� �� ���� ���������<BR><BR></B>
<TABLE cellSpacing=1 cellPadding=7 width="100%" border=1>
<TBODY>
<TR>
<TD vAlign=center width="28%">
<P align=center>������</P></TD>
<TD vAlign=center width="33%">
<P align=center>����� ����������/���<BR>&nbsp;�� ���� ��������� (%, 2003 �.)</P></TD>
<TD vAlign=center width="39%">
<P align=center>���������:<BR>��� �� ���� ��������� (����, 2003 �.)</P></TD></TR>
<TR>
<TD vAlign=center width="28%">
<P align=left>�������<SUP>1</SUP></P></TD>
<TD vAlign=bottom width="33%">
<P align=center>0,71</P></TD>
<TD vAlign=bottom width="39%">
<P align=center>28,000</P></TD></TR>
<TR>
<TD vAlign=center width="28%" bgColor=#ffffff>
<P align=left>����������</P></TD>
<TD vAlign=bottom width="33%" bgColor=#ffffff>
<P align=center>0,71</P></TD>
<TD vAlign=bottom width="39%" bgColor=#ffffff>
<P align=center>28,000</P></TD></TR>
<TR>
<TD vAlign=center width="28%">
<P align=left>��������</P></TD>
<TD vAlign=bottom width="33%">
<P align=center>0,76</P></TD>
<TD vAlign=bottom width="39%">
<P align=center>26,200</P></TD></TR>
<TR>
<TD vAlign=center width="28%" bgColor=#ffffff>
<P align=left>���������</P></TD>
<TD vAlign=bottom width="33%" bgColor=#ffffff>
<P align=center>0,91</P></TD>
<TD vAlign=bottom width="39%" bgColor=#ffffff>
<P align=center>27,600</P></TD></TR>
<TR>
<TD vAlign=center width="28%">
<P align=left>������</P></TD>
<TD vAlign=bottom width="33%">
<P align=center>0,92</P></TD>
<TD vAlign=bottom width="39%">
<P align=center>29,800</P></TD></TR>
<TR>
<TD vAlign=center width="28%" bgColor=#ffffff>
<P align=left>�������<SUP>2</SUP></P></TD>
<TD vAlign=bottom width="33%" bgColor=#ffffff>
<P align=center>1,07</P></TD>
<TD vAlign=bottom width="39%" bgColor=#ffffff>
<P align=center>6,000</P></TD></TR>
<TR>
<TD vAlign=center width="28%">
<P align=left>�������</P></TD>
<TD vAlign=bottom width="33%">
<P align=center>1,08</P></TD>
<TD vAlign=bottom width="39%">
<P align=center>18,600</P></TD></TR>
<TR>
<TD vAlign=center width="28%" bgColor=#ffffff><B>
<P align=left>������<SUP>3</B></SUP></P></TD>
<TD vAlign=bottom width="33%" bgColor=#ffffff><B>
<P align=center>1,10</B></P></TD>
<TD vAlign=bottom width="39%" bgColor=#ffffff><B>
<P align=center>5,300</B></P></TD></TR>
<TR>
<TD vAlign=center width="28%">
<P align=left>�����</P></TD>
<TD vAlign=bottom width="33%">
<P align=center>1,16</P></TD>
<TD vAlign=bottom width="39%">
<P align=center>34,700</P></TD></TR>
<TR>
<TD vAlign=center width="28%" bgColor=#ffffff>
<P align=left>����<SUP>4</SUP></P></TD>
<TD vAlign=bottom width="33%" bgColor=#ffffff>
<P align=center>1,24</P></TD>
<TD vAlign=bottom width="39%" bgColor=#ffffff>
<P align=center>16,100</P></TD></TR>
<TR>
<TD vAlign=center width="28%">
<P align=left>������</P></TD>
<TD vAlign=bottom width="33%">
<P align=center>1,44</P></TD>
<TD vAlign=bottom width="39%">
<P align=center>13,900</P></TD></TR>
<TR>
<TD vAlign=center width="28%" bgColor=#ffffff>
<P align=left>��������������</P></TD>
<TD vAlign=bottom width="33%" bgColor=#ffffff>
<P align=center>1,67</P></TD>
<TD vAlign=bottom width="39%" bgColor=#ffffff>
<P align=center>26,900</P></TD></TR>
<TR>
<TD vAlign=center width="28%">
<P align=left>����������</P></TD>
<TD vAlign=bottom width="33%">
<P align=center>2,00</P></TD>
<TD vAlign=bottom width="39%">
<P align=center>12,500</P></TD></TR>
<TR>
<TD vAlign=center width="28%" bgColor=#ffffff>
<P align=left>������<SUP>5</SUP></P></TD>
<TD vAlign=bottom width="33%" bgColor=#ffffff>
<P align=center>2,05</P></TD>
<TD vAlign=bottom width="39%" bgColor=#ffffff>
<P align=center>4,200</P></TD></TR>
<TR>
<TD vAlign=center width="28%">
<P align=left>�������</P></TD>
<TD vAlign=bottom width="33%">
<P align=center>2,37</P></TD>
<TD vAlign=bottom width="39%">
<P align=center>25,700</P></TD></TR>
<TR>
<TD vAlign=center width="28%" bgColor=#ffffff>
<P align=left>�����<SUP>6</SUP></P></TD>
<TD vAlign=bottom width="33%" bgColor=#ffffff>
<P align=center>3,08</P></TD>
<TD vAlign=bottom width="39%" bgColor=#ffffff>
<P align=center>4,700</P></TD></TR>
<TR>
<TD vAlign=center width="28%">
<P align=left>������� ����������</P></TD>
<TD vAlign=bottom width="33%">
<P align=center>3,16</P></TD>
<TD vAlign=bottom width="39%">
<P align=center>7,900</P></TD></TR>
<TR>
<TD vAlign=center width="28%" bgColor=#ffffff>
<P align=left>�������</P></TD>
<TD vAlign=bottom width="33%" bgColor=#ffffff>
<P align=center>3,70</P></TD>
<TD vAlign=bottom width="39%" bgColor=#ffffff>
<P align=center>7,200</P></TD></TR>
<TR>
<TD vAlign=center width="28%">
<P align=left>��������</P></TD>
<TD vAlign=bottom width="33%">
<P align=center>3,70</P></TD>
<TD vAlign=bottom width="39%">
<P align=center>5,400</P></TD></TR>
<TR>
<TD vAlign=center width="28%" bgColor=#ffffff>
<P align=left>������</P></TD>
<TD vAlign=bottom width="33%" bgColor=#ffffff>
<P align=center>4,57</P></TD>
<TD vAlign=bottom width="39%" bgColor=#ffffff>
<P align=center>22,600</P></TD></TR>
<TR>
<TD vAlign=center width="28%">
<P align=left>������</P></TD>
<TD vAlign=bottom width="33%">
<P align=center>4,69</P></TD>
<TD vAlign=bottom width="39%">
<P align=center>4,800</P></TD></TR></TBODY></TABLE></P><SUP>
<P align=justify><FONT size=2>1</FONT></SUP><FONT size=2> ������� �� ���� �� ���� ������ ����������� ���������, ���� ������ �����������.<BR><SUP>2</SUP> � 2004&nbsp;�. ����� ���������� ��������� 6,391 ����, � 1.01.06 �������� �����.<BR><SUP>3</SUP> �� ������ ������ �������� �� �������� ����������������� �� ����� 2006&nbsp;�. &#8211; ���� ����� 200 ���. ���. &#8211; ��� �������������� � ������� �� ������� �� (��� 190 ���. ���. &#8211; 1,05%).<BR><SUP>4</SUP> ������ ���� ������� ����������� ������.<BR><SUP>5</SUP> � 2004&nbsp;�. ����� ���������� ��������� 8,597 ����, � 1.01.06 �������� �� 15,000 ����.<BR><SUP>6</SUP> � 2004&nbsp;�. ����� ���������� ��������� 14,481 ����.</FONT></P><I>
<P align=justify><FONT size=2>��������</FONT></I><FONT size=2>: ������������ ������� �� ��������� ������� ������������ �� ������� ������ ���������� (������ 2006&nbsp;�.).<BR>��.: ec.europa.eu/internal_market/bank/docs/guarantee/report_en.pdf.</FONT></P>
<P align=justify>��������� (<I>�����</I>) ��������������� � �������� ������������� ��������������� �������� &#8211; ��� <B><I>����������� ������� ������� ����������</B></I> �� �������������
� ���������. � ����������� ����� ���������� ��������� �����������. ��-������, ��� ��������� ���������� �������������� (� ������) ������� ���������� ������� � ����� ���������, ����������� � �����, � ������� ��������� ��������� ������. ��-������, ��� �������� �� ������ ����� ��������� �� ���� �������� ������������ ��������. ����� ����, � ����� ������ ��������� ���������, �� ���������� ��������� ���������������� ������� �������� � �������� ����� �������������� ����������, ��� ��������� �� ���������� ���� ������������ � ���������� ������.</P>
<P align=justify>���������� ����� � ����������� ������� �������� ������ ������������� ������ � �������� ������� ���������� �� �������������� �������. ������� �����������, ��� ������� ���������� �� ������� ������������ � ������� ���� ���� �� ��� �������������� ���������� ����������� ���������� (��������� �� �����, ������������� ����������, � ����������, �������������� ��� ��������), �� �� ����� 14 ���� �� ��� ����������� ���������� ������. ����� �������, ������� � ����� ������� ����������, ������������ ���������� ������� � ����������� �������, ������������ ������� ������� ���������� (� ����������� ���� �� ��� ���������) � ��������� ������������� ������ ���������� ��������.</P>
<P align=justify>� �� �� ����� ��������, ������������ � ��������� �������, ����� ���������� ��������� ������� ������������� � ����������� ������� �� ������ � ����� �������� (������� &#8211; ���, ������ � ��������� ������), ����������� ��� ����� �������� ���������� �������� � ����������� �������������� ��������� (��������, � ��� �������� ����� ����������� �������� �������� ��������� � ������ �����. ��� �������� ���������� � ���� ������ � ����� ��������), ���� �������������� � ��� ������������� ��-�� ���� �����������, ������������ � ����������� ����������������.</P><I>
<P align=justify>������</I> ������������ �������� <B><I>�������� ��������� ����������</B></I>, ��������� � ������������ ���������� ������, � ����������� ������� �����������&nbsp;&#8211; ����������� ��������� � ���������� � ������ � ��������� �������. � �������� ��������, ������������� ���������������, ������������� ���������� ����������� ��������� ������������ ��������� ������� � ��������� ����������, �������� � ��������� ���������� ������, � ����������� � ������ �����, ������������ �� �� �������, � �������������� ��������� ������ �������� �� ���� ����������� �����������, � ����� � ����������� ������ � ������ �������� ��� �������� ��������� �� �������������� ���������� ����������.</P>
<P align=justify>������ � �������� ��������� ����������, ������� ��������, ��� � ��������� ���� ����� ������ ������� ����������� ������������ � ���� ����������������� ����������� ������� � ������������� ������������ ��������� �����������. ���������� ������� ������ �� �������� ������ �� ������������� ��������� ���������� ����������. ������� ���������� � ��������� � �������� ���������� ������ II, ���������� ����������� �������� �������� ���������� ������� ������. � ��������� ������� ����������� ������� ���� �������� � �������� ����� ����� ���������� ������, ���������� ��������� �� ���������� ����. ��� ����� ���������� ��������� ��������� �� ����������� ������� ��������� ������ ����� ������������� ������������ �� ������� ���������� �� ������� � ���������� ��������� ������������ ����� ������������� ����������� �������.</P>
<P align=justify>� �� �� ����� ������ �� ��������, ��� ���������� ����� ���������� � ������ � �� ���������� ���������, ������� ���������� ���������, �������������� �� ����������������. ����������� � ������ ������ &#8220;�����������&#8221; ��������� ����������, �� ��� ������, �������� ������� �����. ����� ����, ������������� ������� ����� �������������� ��������� � �������� ������ �������, ������� �������� ���������� �������� ��� ��������� (�� ������� ���������� ��� �� ���� �������� �������), �� ��������� ��� ����� �������� ������� ���������� ���������� ����������, � ��� ��� ���� ����������.</P>
<P align=justify>����� �� ������������ �������������� ���������� (<I>�������</I>) �������� <B><I>�������� ��� ���������� �������� ����������� ��������� ������������ ���������</B></I>, ������������ ������������� (����������� �� ��������� ���������� ��������� � ������ ��������������� ������� � ����� ����������� ���������), ���������� (���������������� ������, ���������������� ��������� ��������� ������� � ���� ������) � ��������������.</P>
<P align=justify>��� �������� �������������, �� ��� ���������� � ���������� ��������. ��-������, ��� ������������� �� ���������� ������ � ���������� ������� (������������ ��������), ��� ����������� ������������ ������������� ����� ���� ��������� ��������� � ������������ ���������, ��������� �� ���� ��������������� �����������.</P>
<P align=justify>��-������, ����������� &#8211; ���������� ��������� ������ ���� �������� �� ������������� ��������, ���� ����������� �������������, � ������ �������������, ������������ ����, ������������� �������� ������������ ���������� � ������������ ����� � ���, �������� ������ � ������ ������������ �� ������� ������� � ����������.</P>
<P align=justify>�-�������, ����� ���������� �����������, ����������� ����������������� �������, ������ �������� �� �������������� ��������� �����������, � �� ������ ������ ��������� �� ���� �� ������������� ������������� �������� � ��������� ������� ������ �� ��������� ������� ����������� ��������� � ������������ ���������� �������. ��� ����� ������������� ��������� ������ ������ ���������� �������� �������������, �������� � ������ ������ ���������� ����������� ����������, �� �� �� ����� ����������� �������� ��� ���, ��������� � ������� ��� ����������� ��������.</P>
<P align=justify>��������� � ���������� ��������� ��� ���������� �������� ����������� ������� ��������� �������� ���������� ����������� ������������, ����������� �� ��������� ���� ������������ ������, � ������ 27 �������� ����� �����������, ��� <I>����������� ������ ��������������� ������, ������ ��������������� ������ ��������� ���������� ���������, ������ �������� �������������� � ���� ������ �� ����� ����� ����������� � ������������ ��������� �� ���������� �������������� ������������ �� ��� ������� � ����������</I>.</P>
<P align=justify>����� ������ ���������� ��������� ����������� �������������� ������������� ���������� ��������� � ������������ �������������, ���� ������ � ���� ��������� (����������� �������� ������ � ����� ���������� �� ���������). ������������� ������ � ����� �� ������. ����� ������������, ������� �� ������, ����� ������� ���������� � ������ ����������� ����������, � ����� ��, ��� ����� �� �������� �������� ����������, ������� ������ �������� ��������� �� ��������� � ��� ������.</P><I>
<P align=justify>�������</I> ������������ ��������� � ������������� �����������<B><I> ������� �������������� ����� ������������ &#8211; ������������ ��������� � ���������� ��������� ���������, ��������� �������</B></I>. ��� ������������ ���������� �� ���, ��� ������������������� �������� ���� ���������� ������� ����������� ���������� ������������ ����� ��������� � ������������� ���������� ������������� ������ ����������� ��������� � ������ ���������� ������ (��������, ���� ��������� ����� �������������� ������� ��������, �� ������������ ����������� ������ �������, ���� ���� ����������� ����� ��������������� ��������� ����������� ��� ����� ��� �������� ��������� �������, ������� � ���������� �� ����� ���� ���������� �� ������� � �����������).</P>
<P align=justify>� ������ ������� ��������� ��������� ��������� � ������� �� ������������� ��������� ����������� �� ���������: ��� ������� ��������� �� ���� ������. ������������� �������������� ����� ���������� �� ����������� ������� � ������ ������ ������������ ��������������. ������ ����� �������������� ���������������� ������� 27 ������ � ����������� �������. ����� ����, ������� ����������������� ������� � ������ ������ ���������� ��������� &#8211; ������ ���������� ���� �������������� ����� ������, ��� ����� ��������� ���������� ������������������� �������� ���� ���� ����������� ��� ����������� ��������� ������� � ��� ������������ ��������, ������������� �������� ����� ������.</P><I>
<P align=justify>�������</I> ������������ (������������ �� ������ ���������� �������� ����� � �������������� ��������� �����) ����������� � ���, ����� <B><I>������� ����������� ��������� ����������� ������ ����� ����, ��� ���������� ������������ ���������� �������</B></I>, ���������� ����������� ���������� �������� � ���������� ������� &#8220;�������&#8221; �� ������ ������, ������� � ������ �� ��������� � ������� ����������� ��������� ����� ������� ��������� ���������� ������� �� �� ���������� ������������ �, ��� ���������, �� ������� ������� � ��� �� ������� ���������.</P>
<P align=justify>�������� � ����������� ���������� ������� ����������� ������� ����������� � 2004&#8211;2005&nbsp;��., ����� ����������� ����������� ������� 1998&nbsp;�. ���� � �������� ��� ����������. � ���, ��� ���������� ������� ����� ����� ����������, ��������������� � ��, ��� � ������ ����� �������� ������ � ����������� ������� ����������� �� ����������� ���������� �����-����� ������� ������ (�� ����������� ��� ����������� &#8220;������� �������&#8221; �������� 2004&nbsp;�., �� �� ��������� ��� �� ����, ��� ������������ ������� ����������� ������� ���� ���������). � ��������� ���� ���������� ������������ ���� �������� ������, �� ������� � ������������ �������.</P>
<P align=justify>� ���� ��������, � ����� �������� ��, ��� � ������� ����������� ������� ����������� ������ �����, ��������� ���������� �������� ����� ������, ����� ������������ �� ��, ��� � ������������� ����������� �������� ������ ������������ ���������� ������� ����������� ������� �����������. �� ����� ������������� ��������� �� ����������� ������� (���� �������� ��, ��� �� ��� ����� ��������� ��������� ��������� ������� � �������-�����������) �������� ������ ����� ������������� ����������� ������� �� ���� �� ����� ������������� �������������� &#8211; ������������� ���������� ���� ������� �����. �� ������� ���������, ����� ������� ����� � ������� � ��������� ���� ����� ����������� ��� �������� ��������� ��������, ��������� � �������� ���������� �� ������� � ������������� ��������� ��� �������������� ����������� ������.</P>
<P align=justify>����� �������, � ����� ���������� ������� ����������� ��������� �� ����������� �� ���������� ������������� ������ ������� ��������. ��������� ����������, � ������� ���� ������� ����, �� ��������� ������������� ����������� ������� �� �� ���������������� � ������������. � �� �� ����� ����������� ������ ��������� ���������� ������������� ����������� ����������������� ������������ ���������������� � ����������� ������� � �� ������������ �������, ������� ����� ���� ������ � ������ ����������� ��������������� ���� ��� � ��������������� ���������� � ������ � ����������� ����.</P>
<P align=justify>������ ������ &#8211; ��� <I>����,</I> ������� �������� ����� �������� ����������� �������, � �������, ������� ��� ��������� ��� ������-���������� &#8211; ����������� ���������� ����� � ������������ ���� ����� &#8211; ����������.</P>
<P align=justify>���� ���������� ����, ������ ��������������� � ������ �������, � ������������� � ���������� ������ � ����������� �������, �� �� �� ������ ����� ��� �����, ��� ���������� ������������ ������������ ���������� (��� ����������) ������� ��� �������� ������ ������������ ������� ��� ���� ���������� �����. ���, �����������, �� ��������������� � ���, ��� ��� ���� ����� �������� �� �����. ����� ����, ����� �������, ��� ���������������� � ������ ���� ��������� ������� � ���������� ������� � �������������� ����������� � ��� ���������� ��������� ������������, ��� ����� ����������� ����, ������������ �� ���������� ���������� ������������.</P>
<P align=justify>� �� �� �����, ���� ������� ��������� ����� �������� ����� ����, �� ��� ��������� ��-����� ��������� �� ��, ����� ���������� ����������� ��������� � ������������ ���� �������� �� ������ ���������� ���� �����, ��������, � ����� ��� ������� � �������������� ������� ������ �� ����, ��� ��������� �� ����������� ��������� ������ ����������� ��������. ���� ����� � �������� ������� ���� ����� �����, ��� ������ ��� ���� ���, �� ������, ��� � ����������� ������� ��������, ��� �������, �� ��������� �� ����������� �����: ��������� ������� � ������������ ��������� ��������������� ���� ������� �������������� ����������� &#8211; ����� �������� ���������� ������ (� ������) ��� ���������� � ������ ��������������� ��������� ���� ���������� ������� ����� ��� ����� ������� ����������� �����.</P>
<P align=justify>����� ����, ���������� ���� ����������� ������������ ���������� ������� ������ � ������������ ����� ������������ ��������� (� � ������ ����������� � ����������������) ����������� ��������� ���, ������������ �� ����������� � ���������� ���� ������������, � ����� &#8211; �� ������ ������������� �������� ������ �� &#8211; ����� ���������� �������� ���� ���������� ������������ ������� ����������� ���������� ������������ (�������������, ��������� ��������� ���������, ���������� ������ � ����������� ���������) �� �������������� ������� ��� ��� �����������. ����������� ����������������, �� ��� ������, ������� ������ �� ������������� ���� ������������� �����������, �� ����� ������� �������������� �����, ��� ���������� � ���������������� ���� ���������� ������������ �����.</P>
<P align=justify>��� ���� ����, ���������������� � ������� ��������, &#8211; ��� ������������ ������������ �����. �&nbsp;����� �������, ������������ ������� ��������� ������ ���������� ���������, ����������� � ������, ��������� ������ � ������� ������ ������������� � �������� �������� �� �����. ��, � ������ �������, ���� ��������� �� ��� ���� � ����� ������� �����, ��������� ������ � ������ � ������ ������������ ���������� �����, ��������� �� �����. ��������, �� ���� ������� �� ��������� ���������, ��������������� ��� �� ������� ������ (�� ����� 20&nbsp;���. ����) � �������� ���������� ��������, ��������� ������ � ������������.</P>
<P align=justify>��������� ������ &#8211; <I>��������</I> � �������. ����� ��, ���� �������� �� ���� ����������� ������������ ������������ ���������� ������� � �������� ������ ������������ ������� ��� ���������� �����, ����� ��������� ������ � ��������������� �������� � ������� ����������� ������� (� ����������) �� ���������� ����������� ������������� �������, ��� ������� � ���� ����� ������. �����������, �������� ����� ������ �������� ����������� ������������ ������� �� �� ������������� � �������������� ���������� � ����� ������������������ � ���������� �����������.</P>
<P align=justify>������������ ������������������ ����� ���� ������� � <I>� ���������������� � ��������</I> <I>� ������� �������������� ���������� � ��������������� ���������� ����������</I>. � ������ ������� ��� ��������� � �������� ��������� � ���������� ��������������� ���� ����, ����������� ������������� ����� ���������� � ���� ��������� ���������������� ������, ��� ������� ������� ����������� ���� ���������������� ����� � ������������� ��������� ��� ������������ �����-�������������; �������� &#8220;�����-������&#8221;; ����� � ����������� ����� ������� � ����� ��������� ��� ����������� (� ��� ����� � ������ ��������� ������) ���� ��������� �������� ����������; �������������� �������, � ����� �������� ���������� ������ ��������������� ��������� � ���������� ���� ���� ������ ����������� ����� ��� ���������� ������������ ������� ������� (��������, ������������ ����� ���������� ���� �����������, �������� �������� �� ��� ��������� ��������, ���������� �� ������������ �������, ���������������� ������������, �������������� ����������� � ��.). �����, ��� ��������������, ����� ����������� ������� � ���� ��������� �� ���������������� ��������� �����������, ������� � ������ ����� ����������� ������� 1998 �. ������������ � ����� �������� ����� ��� ���������� ��������, ���������� ���������, � ��� ����� ����������� ���������� � �������������� ����������� �������������.</P>
<P align=justify>��� �������� ���� ��������� �������� ��������������, ������� ���������� ������ � ����������� ����������������� ���������������� � <I>�������� ������������ ������</I> ��� � ����������� ��������� �����������, � ��������� � ����� �������� ����������� � ��������������� �� ��������� ������ �� ����������� �� ������������� � ���������� (������ ������ ����������). ����������� ������������� �������� ����� �������� � ���� ��������� ���� �������
��� �����, � ��������� ���, ���������� ����������� ��������������� ������������� ������ �� ������-��� ������ ����� �/��� ��� ����������.</P>
<P align=justify>��� ����� ������������, ������������� � ����� ������ ����������� ������������������ ��� ����������������, ��� � ��������, �������� ������ � <I>������� ����������</I> �� �������. ����� ����� ���� ��������� ��������. ��-������, �������������� ����������� ���������� ������, ������������ �� ����������� ���������� ������ ���������� �� ������, ������������� � �������� ������ ���� �������� ��������� (����, ���� ����� � �������� ��������� ��, �� ����������� 20 ���. ����, ���, �� ������ ������, ����� ��������������� � ������������ �������� ���� ����� ��������� ������������ ����������). ��-������, ������������� ������ ������������� � ���������, � ��� ����� �� ������������ ������, ������ � ��������������� ����������� ����� � �� �������� � ����� ���������������� ��� ����������� ������������ ���� � ����������� ���, ��� ��� ������� � ����������� ����� ����. (����� ����� ����� ����� �� ������� ������� ���� ����������� �����, �������� ��� �� ������, ��� � ����� ���������� �������� �������� � ����� �����������, ������� ����������� ������� �������������� ����� �� ���������� �������.)</P>
<P align=justify>������ ��������, �������������� ��������� ������������� ���������������� ���������� ������� ����������� �������, ����� �� ��������� �������������� ������� ��������� �� ����������� ������� � ���������� ������, ������������ ��� � ���� ������, �����������, ��� ������� �������� ���������� ������ ������������������. ��� ��������� �� ����� ��������� ������������ ����� ����������� �������, ��������� ���� �� ����������� ����� ����� ����������� ����������� � ��������� �� ������� ����������. ��� ����� ���������� � ������ ��������� ���������� �������� ������� � ���������� ������ � ���� ��������� ���������� �������� ��, ��� ��� ��������� �� ������� � �������� � ������ ������������������ ��������� ������� &#8211; � ������ ������ ������ � ��������� ������� ����������� ������� (��� ����������� ���� ������, �������� � ��������� ������ �����).</P>
<P align=justify>������ ��������, � ������� ��� ���������� ����, �������� ����������������� �������������� � ����������� �������� ����� ����������� ������� ����������� ���������� ������������. ��� �������� ��� �������� ������ ������������� ���������� ��������, ��� � ������������� ���������� ������ �� ��� ����� ������� � ������������. ����� ��� ����� ��������� ������� � ����� ����������� (� ������������) ��������������� ������ �� ��������, ������������� �������� ������ �� ������, � ����� ������ � �� ��������, ��������� ������������� �������� � ������� �� ������� ��������, ������������ ������� �������������� � ��������, �������������� �������� � ����� ������ ����������� �������������� ������������ ���������� ���������� �������.</P><SUP>
<P align=justify><FONT size=2>1 </FONT></SUP><FONT size=2>����������� ����� &#8220;� ����������� ������� ���������� ��� � ������ ���������� ���������&#8221; &#8470;&nbsp;177-�� ������ ��������������� ����� 28 ������ 2003&nbsp;�., ������� ������� ��������� 10 ������� 2003 �., �������� ����������� ���������� ��������� 23 ������� 2003&nbsp;�.</FONT></P><SUP>
<P align=justify><FONT size=2>2 </FONT></SUP><FONT size=2>Guidance for Developing Effective Deposit Insurance Systems. Financial Stability Forum, 2001.</FONT></P><SUP>
<P><FONT size=2>3 </FONT></SUP><FONT size=2>��., ��������: Garcia G. Deposit Insurance: A Survey of Actual and Best Practices, IMF, 1999.</FONT></P><SUP>
<P align=justify><FONT size=2>4</FONT></SUP><FONT size=2> ������ ��������� �� ����������� ������� �� �������� 2006&nbsp;�.</FONT></P><SUP>
<P><FONT size=2>5</FONT></SUP><FONT size=2>&nbsp;�� ������ ����� ������ �� 1.01.2004&nbsp;�. � 1.07.2006&nbsp;�. </FONT></P><SUP>
<P><FONT size=2>6 </FONT></SUP><FONT size=2>�� ������ �� �� ������ 2004 �. ���������������� �������� ����� ���������� � ����� �� �� ���������� 1,61%, �� 10 ����� ������ ������������ ���������� &#8211; 3,46%.</FONT></P>


</body>
</html>

