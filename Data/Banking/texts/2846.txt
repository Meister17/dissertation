<!--
ID:35129976
TITLE:���������� ���������� ������� � �������� �����
SOURCE_ID:3113
SOURCE_NAME:����������� � �����
AUTHOR:�.������
NUMBER:6
ISSUE_DATE:2005-06-30
RECORD_DATE:2005-08-04


RBR_ID:4933
RBR_NAME:������ ����������� ��������� ������
-->
<html>
<head>
<title>
35129976-���������� ���������� ������� � �������� �����
</title>
</head>
<body >


<DIR>
<DIR><FONT face="Times New Roman"><B></DIR></DIR></B>
<P align=justify>�������� ������������ ���������� ���������� ������� � �������� ���� �������������� �� ������ 60-70-� ����� XX ����. �������� ������ ������� ����������� ���������� ������, ������� ���������� ��������� ������ ���������� ��������� (��������, ��������� � �������) � ������ �������<SUP>1</SUP>. ������ ����������� ������������ �� ����� ���� � ������ ���� ��������� � ���������� ��������, � � ����� �������� ������ &#8212; ���������� ������ 1998 ���� (�������� ������ � ��������������� ������������� ��������� ��������� �������� ���������� ���������� �����������) � ����������� ����������������. ���������� ���������� �����, ����������, �������� � ���������� ������, �������� � ������ ����, ��� �������� ���������� ����������� ��������� �� ������ ������������.</P>
<P align=justify>���������� �������� ��������� ������������ ������ �� ��������, ������������ ������� � �������� ������ ������������ ���������� ��������, &#8212; <B><I>����������� �������� �� �������������� ���������� �������� (&#8220;����� ���������� �������&#8221;).</P></B></I>
<P align=justify>������ ����� ������ ��� ������������ ������ ������ (�������) ����� �������, ������� ������������� ������� ��� ������ ����<SUP>2</SUP>. � ��� ������ ����� ���� ����������� ���� ��������. ����� ���������� �������� �������������� ����� ���� ������ �������, ������� ��������� ����������������� � ����� ������ ����� (���. 1).</P>
<P align=justify>����� ���������� ������� (����� ��������� �������), � ���� �������, �������������, ��� ������ ����������� ����� ��������� ������� ������� �� ���������� ������������ �������, � ��� ������� ������������ ������ &#8220;�����������-������������&#8221; ������ �����, ������������ ��� ���������� ������� �� ������ �������. ������ �������� �������� &#8220;������ ������ ������&#8221;, ��� ��� ���������� ������� �� ������� ������ �������������� �� ���������� ������� �� ������ �������. ��������� �������������� ������� � ������ ������� � ����� ������ ����������� � ������������, ����������� ����� ������������� ������� �� ����������. ���, ��������, �������� ����� ������� �� ������� �� ������������� � �� ������� ������ ������������ �� �������� �������� ������ �������, ������� ����� &#8212; �� ������������ �������� ������ ������� � �������������� ������������� ����.</P>
<P align=justify>��� �������� ������� �� ������� � �������������� ������, �� ������� �� ����� ��� �� ������������ �������� ������ ������� � ������ ����, � ������� &#8212; �� ���������� �������� ������ ������� � � ������������ ��������. �������� ������������ ��������, �������� ������������� ����� � ��������������� ������� ������������ ��������������� �� ������������ � ���������� ������ � ������������, � ����� ������������ ������ ����� � �������������� ���� � ����� ���������� ������� ����� (���. 2).</P>
<P align=justify>�������� �� ��, ��� ����� ���������� ������� ����� ���� ���������� ��� ����� ������������� �� ��������� � ������ ������ �����, ���� ��������� ����������� � ���������������� ����� ������������. ������������� �� ����������� ���������� ����������� �� ������������ ��������������� ������������ ����� ��� ��� �� ����� ��������?</P>
<P align=justify>��������� ����� ������ �� ������� ����������� ��������� ������� � �������� ����� �. �������� � ����� ���������� &#8220;���������� ������� ��� ��������� ����� ���������� �������� � ��������� �����&#8221; �������� ������������� ��� �������. ����� ��������, ��� <I>&#8220;���������� ���� ������� ����������� �� ������� �������� �����������, � ������� &#8212; �� ������� �������� ��������� �� �������������. ��� ������ ��� ����������� �������, ��� ��� � ����� ������ ���������� ����������� ����������� ���� ������ ������������ �������� ������������ ������� ��� �������� ��������� ��������&#8221;<SUP>3</SUP>.</I></P>
<P align=center></P>
<P align=justify><B>���. 1.</B> ������ ������ ����� �������</P>
<P align=center><B></P>
<P>���</B>. <STRONG>2</STRONG>. ����� ���������� �������</P>
<P align=justify>� ���� �� ������ ��������� ������ ����������� ������������ ������������� �������� �����, ��� ���� ����������� ��������� ������� � �������� � ���� �������� ��������� ����������� ���������� ��������� �������������� ������ � ��������, ��� �������� ����������� ������ ������.</P>
<P align=justify>� �� �� ����� �. �������� ���������: <I>&#8220;����������� ���, ����������� ���������, �������� � ����������� ������ � ��������, ������������ �������� L (t) (����������������� �������), K(t) (����������� ��������� ��������)&#8221;<SUP>4</SUP>. </I>����� ����� � (t) ������������ �������, ��������������� ������� ��������� ������������ �������� ��������, k<SUB>ij</SUB> (t) &#8212; ���� �������� j-�o ����, ����������� i-� �������� �������� � ������ ������� t.</P>
<P align=justify>����� �������, ������������, ��� �� ���� �������, ������������� �� ������������ ����������� ���� ������, ����� �������� ������������� ���������������� ��������� �����������. ����� ���� �������������� ����� � ��������� ���� ������� ���������� �������, ������������ ����� ������, �������� ������� ����� ������� ����������� ��������� ������� � �������� �� ���� ����������� ���������� ������� �� ����������� �������������.</P>
<P align=justify>� �������� �������������� ���������� ������. ��������, ��������� ����������� ����� ��� ���� �������� ������������ &#8212; �<SUB>1</SUB>, �<SUB>2</SUB>, �<SUB>3</SUB>, ������ ���������� �������� �������� �<SUB>1</SUB><I> </I>�� ����� ��������� 200. ���������� ������������ �������������� ���������� 30%, 10%, 0% �������. ��������� ���� ������������ �� ���� �������� ��� ����� &#8212; �<SUB>1</SUB><I> </I>(200), �<SUB>2</SUB> (300), �<SUB>3</SUB> (100) � ���������������� �������� ��������� 5%, 10%, 1%.</P>
<P align=right><EM>������� 1</EM></P>
<P align=center><STRONG>����������� ���� ���������� ��������</STRONG></P>
<P align=justify>
<TABLE cellSpacing=1 cellPadding=2 width=309 border=1>
<TBODY>
<TR>
<TD vAlign=top width="12%" bgColor=#ffffff height=26 rowSpan=2><B><FONT face="Times New Roman">
<P>������</B></FONT></P></TD>
<TD vAlign=top width="17%" bgColor=#ffffff height=26><B><FONT face="Times New Roman">
<P align=center>�������</B></FONT></P></TD>
<TD vAlign=top width="12%" bgColor=#ffffff height=26><B><FONT face="Times New Roman">
<P align=center>�<SUB>1</SUB>,%</B></FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=26><B><FONT face="Times New Roman">
<P align=center>�<SUB>2</SUB>,%</B></FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=26><B><FONT face="Times New Roman">
<P align=center>�<SUB>3</SUB>,%</B></FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=26 rowSpan=2><B><FONT face="Times New Roman">
<P align=center>����� ������</B></FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=26 rowSpan=2><B><FONT face="Times New Roman">
<P align=center>������</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="17%" bgColor=#ffffff height=13><B><FONT face="Times New Roman">
<P align=center>������,%</B></FONT></P></TD>
<TD vAlign=top width="12%" bgColor=#ffffff height=13><B><FONT face="Times New Roman">
<P align=center>5</B></FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=13><B><FONT face="Times New Roman">
<P align=center>10</B></FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=13><B><FONT face="Times New Roman">
<P align=center>1</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" bgColor=#ffffff height=13><FONT face="Times New Roman">
<P>a<SUB>1</SUB></FONT></P></TD>
<TD vAlign=top width="17%" bgColor=#ffffff height=13><B><FONT face="Times New Roman">
<P align=center>30</B></FONT></P></TD>
<TD vAlign=top width="12%" bgColor=#ffffff height=13><FONT face="Times New Roman">
<P align=center>100</FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=13><B><FONT face="Times New Roman">
<P align=center>0</B></FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=13><FONT face="Times New Roman">
<P align=center>100</FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=13><B><FONT face="Times New Roman">
<P align=center>200</B></FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=13><B><FONT face="Times New Roman">
<P align=center>60</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" bgColor=#ffffff height=14><FONT face="Times New Roman">
<P>a<SUB>2</SUB></FONT></P></TD>
<TD vAlign=top width="17%" bgColor=#ffffff height=14><FONT face="Times New Roman">
<P align=center>10</FONT></P></TD>
<TD vAlign=top width="12%" bgColor=#ffffff height=14><FONT face="Times New Roman">
<P align=center>100</FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=14><FONT face="Times New Roman">
<P align=center>300</FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>0</B></FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>400</B></FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>40</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" bgColor=#ffffff height=14><FONT face="Times New Roman">
<P>a<SUB>3</SUB></FONT></P></TD>
<TD vAlign=top width="17%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>0</B></FONT></P></TD>
<TD vAlign=top width="12%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>0</B></FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>0</B></FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>0</B></FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>0</B></FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>0</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="30%" bgColor=#ffffff colSpan=2 height=14><B><FONT face="Times New Roman">
<P>����� �������</B></FONT></P></TD>
<TD vAlign=top width="12%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>200</B></FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>300</B></FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>100</B></FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>600</B></FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=14><FONT face="Times New Roman">
<P align=center>100</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="30%" bgColor=#ffffff colSpan=2 height=15><B><FONT face="Times New Roman">
<P>�������</B></FONT></P></TD>
<TD vAlign=top width="12%" bgColor=#ffffff height=15><FONT face="Times New Roman">
<P align=center>10</FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=15><B><FONT face="Times New Roman">
<P align=center>30</B></FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=15><B><FONT face="Times New Roman">
<P align=center>1</B></FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=15><B><FONT face="Times New Roman">
<P align=center>41</B></FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=15><B><FONT face="Times New Roman">
<P align=center>59</B></FONT></P></TD></TR></TBODY></TABLE></P>
<P>� ������ �. ��������� ������� ����� ������������� ������� � (t) ���������� ����:</P>
<P align=center></P>
<P align=justify>� ������ ���� �� �������� �������� ������������ ����� �������� ����������� ���������� ��������, �� ����� ��������� ��������� ������� ������� �� ������� �� �������� ��������� ������� � �������� � �����. ������� ������������ ������� ���� ������� ���������, ������������� �� �������������� ����������� �����������, �������� ��� ���� ���������� ������ �������� ������������, ���������� ���� ������� � (t) ����������� ���:</P>
<P align=center></P>
<P align=justify>����������� �� ������ ����� ���������� ������� �������������� ���� ������������� �������� ������� � ����. 2.</P><I>
<P align=right>������� 2</P>
<DIR>
<DIR>
<DIR></I><B>
<P>�������������� ���� ���������� ��������</P></B></DIR></DIR></DIR></FONT>
<TABLE cellSpacing=1 cellPadding=2 width=309 border=1>
<TBODY>
<TR>
<TD vAlign=top width="12%" bgColor=#ffffff height=26 rowSpan=2><B><FONT face="Times New Roman">
<P>������</B></FONT></P></TD>
<TD vAlign=top width="17%" bgColor=#ffffff height=26><B><FONT face="Times New Roman">
<P align=center>�������</B></FONT></P></TD>
<TD vAlign=top width="12%" bgColor=#ffffff height=26>
<DIR><B><FONT face="Times New Roman">
<P>�<SUB>1</SUB>,%</P></DIR></B></FONT></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=26><B><FONT face="Times New Roman">
<P align=center>�<SUB>2</SUB>,%</B></FONT></P></TD>
<TD vAlign=top width="13%" bgColor=#ffffff height=26><B><FONT face="Times New Roman">
<P align=center>�<SUB>3</SUB>,%</B></FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=26 rowSpan=2><B><FONT face="Times New Roman">
<P align=center>����� ������</B></FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=26 rowSpan=2><B><FONT face="Times New Roman">
<P align=center>������</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="17%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>������,%</B></FONT></P></TD>
<TD vAlign=top width="12%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>5</B></FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>10</B></FONT></P></TD>
<TD vAlign=top width="13%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>1</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" bgColor=#ffffff height=13><FONT face="Times New Roman">
<P>a<SUB>1</SUB></FONT></P></TD>
<TD vAlign=top width="17%" bgColor=#ffffff height=13><FONT face="Times New Roman">
<P align=center>30</FONT></P></TD>
<TD vAlign=top width="12%" bgColor=#ffffff height=13><FONT face="Times New Roman">
<P align=center>0</FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=13><FONT face="Times New Roman">
<P align=center>200</FONT></P></TD>
<TD vAlign=top width="13%" bgColor=#ffffff height=13><FONT face="Times New Roman">
<P align=center>0</FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=13><FONT face="Times New Roman">
<P align=center><STRONG>200</STRONG></FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=13><FONT face="Times New Roman">
<P align=center>60</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" bgColor=#ffffff height=13><FONT face="Times New Roman">
<P>a<SUB>2</SUB></FONT></P></TD>
<TD vAlign=top width="17%" bgColor=#ffffff height=13><FONT face="Times New Roman">
<P align=center>10</FONT></P></TD>
<TD vAlign=top width="12%" bgColor=#ffffff height=13><FONT face="Times New Roman">
<P align=center>200</FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=13><FONT face="Times New Roman">
<P align=center>100</FONT></P></TD>
<TD vAlign=top width="13%" bgColor=#ffffff height=13><FONT face="Times New Roman">
<P align=center>100</FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=13><FONT face="Times New Roman">
<P align=center><STRONG>400</STRONG></FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=13><FONT face="Times New Roman">
<P align=center>40</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" bgColor=#ffffff height=14><FONT face="Times New Roman">
<P>a<SUB>3</SUB></FONT></P></TD>
<TD vAlign=top width="17%" bgColor=#ffffff height=14><FONT face="Times New Roman">
<P align=center>0</FONT></P></TD>
<TD vAlign=top width="12%" bgColor=#ffffff height=14><FONT face="Times New Roman">
<P align=center>0</FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=14><FONT face="Times New Roman">
<P align=center>0</FONT></P></TD>
<TD vAlign=top width="13%" bgColor=#ffffff height=14><FONT face="Times New Roman">
<P align=center>0</F
ONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>0</B></FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=14><FONT face="Times New Roman">
<P align=center>0</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="30%" bgColor=#ffffff colSpan=2 height=14><B><FONT face="Times New Roman">
<P>����� �������</B></FONT></P></TD>
<TD vAlign=top width="12%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>200</B></FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>300</B></FONT></P></TD>
<TD vAlign=top width="13%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>100</B></FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>600</B></FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=14><FONT face="Times New Roman">
<P align=center>100</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="30%" bgColor=#ffffff colSpan=2 height=14><B><FONT face="Times New Roman">
<P>�������</B></FONT></P></TD>
<TD vAlign=top width="12%" bgColor=#ffffff height=14><FONT face="Times New Roman">
<P align=center>10</FONT></P></TD>
<TD vAlign=top width="14%" bgColor=#ffffff height=14><FONT face="Times New Roman">
<P align=center>30</FONT></P></TD>
<TD vAlign=top width="13%" bgColor=#ffffff height=14><FONT face="Times New Roman">
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=14><FONT face="Times New Roman">
<P align=center>41</FONT></P></TD>
<TD vAlign=top width="15%" bgColor=#ffffff height=14><B><FONT face="Times New Roman">
<P align=center>59</B></FONT></P></TD></TR></TBODY></TABLE><FONT face="Times New Roman">
<P align=justify>��������� ��������� ����������� ���� ������������� ������������ ������� � ��������� ���������� ���������� ��������� (����. 1).</P>
<P align=justify>���������� � ����. 1 ���� ��������� � ������� ������ ����������, ��������� �������� ������� ������� ������������ � �������� �������� ������. ��������� ������ ��������� ��� ���� 100, ��������� ������� &#8212; 41, ������� &#8212; 59.</P>
<P></P>�������� �� ��, ��� �� �������� ������� ������������ �������� ������������ (� �������, ��������, �� ��������������� ����� ������� ������ �<SUB>1</SUB> �� �������������� ��� ���������� � ������ �<SUB>1</SUB>), �������, ��������������� ����������� � ����. 2 �����, ����� ���������� 59. ����� ����, �� ���������� � ����������� ����� ������� � ��������, ��� ��� �������� �������� �� ������� � �������� �������� ��������, �. �. ���� ����������� ������ � ������� �� ������, �� ������������� ����������� ���������, ��� <I>�������� �������� � ���������� �����������.</I>
<P align=justify>� ������� ����������� ������������ ����� �������� ������ ��������������� ��������, � ����� ������ ����� ����� ����������� �������� � ����� ����� ����������, � �������� ��� ���� �������� �� �����. �������������� ��������� ���� ����� ���������� �������, ���������� �� ����������� ���������� ��������, �������� ������ ������ ����� � ���� ����� ������� �����������, ��������� � �������������� ������������ �������� ������� �� ���������� � ������������.</P>
<P><SUP>1&nbsp;</SUP> ��. ����� ��. ���������� ��������� � ������������ �����. �.: Catallaxy, 1994. �. 395. 
<P><SUP>2</SUP> ������ �.�., ���-��� �. �. ����������� ������ � ���������� ����.&nbsp;�.: �����-�, 2000. �. 215.</P><SUP>
<P align=justify>3</SUP> �������� �. �. ���������� ������� ��� ��������� ����� ���������� �������� � ��������� ����� // ���������� ����. 1998. &#8470;�.�. 13.</P><SUP>
<P align=justify>4</SUP> ��� ��. �. 14.</P></FONT>


</body>
</html>

