<!--
ID:38525724
TITLE:��� � ���������, ��� ������ ������������� ���� ���� ������ � �����?*
SOURCE_ID:11528
SOURCE_NAME:���. ����� � ������� ���
AUTHOR:���� ��������, �������� ���������� �������� ���� � ��������, �������� ���������� �������� ����������, �.�.�., ��������� �����-�������������� ���������������� ������������ 
NUMBER:8
ISSUE_DATE:2007-08-31
RECORD_DATE:2007-12-10


-->
<html>
<head>
<title>
38525724-��� � ���������, ��� ������ ������������� ���� ���� ������ � �����?*
</title>
</head>
<body >


<P class=vrez align=justify>� ���������� ������� �� ����������, ������ ���������� ��� ������� � ������ �������������� �������� ������� ��������������� ���� � ��������� �������������� ��������, ������� �������� ���������� � �������� � ���� ������������, �������� ���������� ���� ����������. ��� ������ ��� ����� ����������� ������� ������������ �������� � ���������� ����� �� ������� ������ ���������� ��������� � ����������� �������������� �������. 
<P align=justify>������� �� ����� ��������� ����� ��������� ������� ������� ������������� �������. ���� � ����� ��������������� ��������� ��� ������ � �������, ��� �������� ����. ���� ��������������� �� ������� ���������� ����, �� ������� ��������� 30-� ����� �������� ���� ��������� �������������� ��������. 
<P align=justify>������������ ������ �����, ��� � ��������� ���� ����� ����������� ����� ��������� �� ���� ��������, ����-�� ������� ��������� ���������. ���������� �� ������� ���� ����� � ������� ��������� ������������������ �� ��������� � �� ���������� &#8212; ���������������� � ������������ ���������, ������� ������� �������, ��� ������ ����������� � ������ ������� � ����� ������. 
<P align=justify>�� ������ ��� ������� � ���, ��� ���������� ����������� ����������� � ����� ���������� ���������� �, ������� �������� ������, � ����� �� �������-������ ��������� �� ��������� �����, ����� �� ������� �� ����� �� �������������� �����������, ���������������� ��������������� � ����������� ������������... &#171;�������� ���������&#187; &#8212; ����� �� ������������� ������������� ��� �� ����� � ������������� �������� &#171;�������������&#187; �������������, �������������������� ����������� &#8212; ���� ���� �� ��������. ����������� ������� ������ ����� ������ ������, �� ���� �� �� �� ������, ��� ��������� �������, � ��������, ����� � ������ ����������� � ���� ���! ����� ���� ��� �� ����� ������ ������������� ������� &#171;������������&#187; � ������� ������� ���������. 
<P align=justify>�������� ��������� ��������� ��������� �� ������� ��� ����������� &#171;�������� ���������&#187;. ����������� ����������� ���������������-����������, � ��� ������� �������� ���������� �������, ��� � 2000 ��� ����� ������: &#171;Quo vadis?&#187; 
<P align=justify>��� ������������� ������������ ������������� ����������, �������� �������� �� ����������? ������ ������� ��� ��������� �������. 
<P align=justify>������ ����� &#8212; ���������� �� ������� ���������������, ���������� &#171;��������������� �������&#187;, ������� ���� � ������, ������ � ����������� �������� ����������, � ��� ����� ������� ����������. ������� �������� ������������, ��� ����������� ��������� ����������� �������� ���������� ������������������. � ���� � ������ �������������� ��������. ��������� ������� � ������ ������� �������. 
<H3><FONT size=3>�������� ������������ </FONT></H3>
<P align=justify>����� ��������� �������� ������������� ���������� �������� �� ���������, ������� ��������� ���������� �������� ���������������: 
<P align=justify>&#8226; ��� ������� ����������� � �������� ���������� ����������� � ��������� ������������� �������� �������, ��� ��������� ������ &#171;����������� �������� �������&#187; ���������������; 
<P align=justify>&#8226; ����������� ��� ���������� ��������������, ������� �������� ���������� ������� ������ ������������� �����������; 
<P align=justify>&#8226; ����� ����������� ��������� ��� ��������, ����������� � ����� �������� �������������� �������; 
<P align=justify>&#8226; ����� �������������� ���������� ���������� ������������ ��������� �� ���������, ��������� ���������� �������� �������� ����������� �������� � ��������� �������� ����� �������� ��������������, ����������� � ����������� ����������, ������� ������ ���� � ��������� ���������� ��������� �������; 
<P align=justify>&#8226; ����� ������ ������ ������������� ������������� ���������� ��������� ��������� � ������� ��� ����������, ��� � ���������� ��������. 
<P align=justify>������� ����� ����������� ������ ������������� ���������� ����� ������� � ��� �����. 
<H3><FONT size=3>&#171;�� ��������&#187; </FONT></H3>
<P align=justify>�������� ����������� &#8212; � ������ ������ � �������� ������������� ����������� ��������������. 
<P align=justify>������ ������������� ��������� ��������������� ������� (���) &#8212; ������� ��������, ����������, ����� ������������ �������� � ������������� �������� �� ����� �� ������ � �������� �������� ��������, ��������� ��������������� ������������� � ���������-������������� ����������, ����������� ��������. 
<P align=justify>������� ����� ���� ���������� 
<P align=justify>����� ��������� ������ ������������� ���������� ���������� �� ������� ������ � �������� ��������� ��������������. Ÿ �������� ��������� ���� � ������� &#8212; ������ ������������� ��������� �������������� ��������: 
<P align=justify>&#8226; ������� � ������ ��������� ������ �� �������; 
<P align=justify>&#8226; ���������� ���������������� ����� ���� ������� � ������ �������; 
<P align=justify>&#8226; ��������� �������� ������ ������� � ���������� ��������������; 
<P align=justify>&#8226; ����������; 
<P align=justify>&#8226; �������������� � ��������� ��������� ����������. 
<P align=justify>�� ������� ������� �������� ������� ������ ������������� ��������� ���������� �������������� ��������� �������: 
<P align=justify> 
<P align=justify>
<P align=justify>��� PRj &#8212; ������ ������������� ��������� j-�� ������� (j=1 &#8212; ������������� ������, 2 &#8212; �������������, 3 &#8212; ������������); Dit &#8212; ������ �� i-�� ������� � ���� t, Zit &#8212; ������� �� i-�� ������� � ���� t, Nlit &#8212; ��������� ������� �� i-�� ������� ���� t; Pkt &#8212; ������ �� ���������� ������� �������� � ������� ����, ���������� �� � ���� t, ?tk &#8212; ���� ������� ��������, ������������ �� k-� ��� �������������� ������������ � ���� t; Kj &#8212; ����� ����� ����� �������������� ������������, �������������� �� � ������ j; St &#8212; �������� ������ �������� � ���� t; Ikt &#8212; ���������� �� k-�� ������� � ���� t. 
<P align=justify>������� � �������� �� ������� ������ �������� ������� ��������� ������, ��� �������������� �������� ��������� � ������� � ����� ������� ���������� ��������. ����� ��� ��������������� ���� ������ �� ���������. ��� ���������� � ��������� ��� ���������� � ���������� �������� &#8212; ��������� ������� ��� ��������������� �������, ������� ����������� ������ ��� ��������� ������������� ��������. ��������� ��������, �������� ��� ������������� ��������. ������, ��������, � ����������� ������ ��������� ����������� ������������� �������, ������� ��������� ��� �� ������ ���������� � ������, ����� ������, �� �����? ��� ���������� ���������� �� ���������? 
<P align=justify>��������� ������� �������������� ���������� ��� �� ���� ������ ������ � �������� �����. �������� � ��������� ������� &#8212; ������ ��� ����� ������ ������������� ����������� ������� ������� ���� ����� ������� ������ � ����������� ������. ����������, ������������ ��������� &#8212; ������ � ���, ��� ��� ������������� �� ������������� ��� ��������. 
<H3><FONT size=3>��� &#171;�������&#187; ������? </FONT></H3>
<P align=justify>����� ��������� ������� ������������� ������������� ��������� � ���������� ��������: 
<P align=justify>&#8226; ������ ������������� ��������� ��������������; 
<P align=justify>&#8226; ��������������� ������ �� ���������������� (�������������� ������ � ���������� �������������� �������������� ��������); 
<P align=justify>&#8226; ������ ��������� ��������������� ������� (�������); 
<P align=justify>&#8226; ���� ����������� ���������� � ������ ���; 
<P align=justify>&#8226; ���������� ������ ����������. 
<P align=justify>������ ���������� �������������� ��������� �������. 
<P align=justify>������ ������� � �������� ��� ��������� �������������� ����������� ���������� &#8212; ��� ��� � ������ ���� � �������� ���������. ���� ��������� ���������� ���������� �������� ����������� �� ������� �� ������ ������������ �� ����� � ��� �� ������� ���������� � ���������� ������������, ������������ � �����, ����������� � ���������� �������������. 
<P align=justify>���� ���������� ��������� ��������� ���������, � �� ������������� ������������ �� �������, ��� ���� ����������� ����� ������� �������� ����������� � �������, ������ ����� ������ &#8212; �� �������. ��� ��� ��, ��� ���������� ��������� �� ����� ���������, � ��� �������� ������ �����, ���������������� ������ ������. �������� ����������, ��� ����� ������� ������ ���� ������������� ��������������� ��������, �� &#171;��������&#187; ����� � ������ ����������. 
<H3><FONT size=3>���� ������� ������� </FONT></H3>
<P align=justify>������ ������ ��� ��������� ������ ������� ����������� �������� �� ��� �������� ����������: ������������� (���), ������������� (2-3 ����) � ������������ (3-5 � ����� ���). 
<P align=justify>��� ������ ������ ������� �� �������������� ����������, ���������� ����������� ������. 
<P align=justify>���� �������������� �������� �� ���������������� ����� ��� ���� ������� ������� ��������� �������� �������� �� �������� � �������, ��� ����������� ������ ��� ����������� ������� � ������ ������������� ����������. ������� �����������, ��� ��������������� ������ �� ���������������� �� ������� ��������� ���������� �������� ��������. �������� ��������� ��� ����� ��� ���������������� &#171;����� ����&#187; � ��������, ����������� � ��������� ��������������, ����� ���� ������ ����� ������� ��-�� �������� ���������� �������. ���������������� ��� ������� � ������� �� ����������� ���������� �������������. ��������������� ����� ������� �� ��������� ������, ��� �� ������ ����� ������� �������� ������ ���������, �� � ������������� �������� ������������� ���������� � �����, �������� ����. 
<P align=justify>���������� � ��������� �������. ��� �������� � ���������. 
<P align=justify>� ���������� ���������� ��� �������� ����������� �������������� ������� � ������� ���������� ��������, ��������� �� ������� ��������������� � ������� �������������� ����������. 
<P align=justify>&#8226; <STRONG>�������� ������ �����������</STRONG>. � �������� ��������� ������, ������� �������� ����������� � ������ ����������� ���� ������ ���� �� ������ ������������� ������� ��� ����� �������� � ��������������. �� ������� �� ������ ��������������� ������������� ���������� &#8212; �� ������������ � �� �������� ���� �� 11 ��� (� &#171;����������� ��������&#187; �������). ��� ���������� ������������� ����������. 
<P align=justify>&#8226; <STRONG>�������� �������������� ����������.</STRONG> ������� �� �������� ��������������� ������� ���������� �� ��������� ��������, ��� �� �� �������� � �� 40 ��� &#8212; ����� �������, ������ ������� ���������� &#171;�� �����&#187;. ������� �� �� ����������� ��� ����������, ��� ��� ����� ��� ���� �������������� ���������� ��������� 103%, � ����� ���� ��� �������� 590%! 
<P align=justify>�������, ����������� �������. ��������� �������� ������������� ����������� ��������������� ��� ����������� ������������� �������������� ��������. 
<P align=justify>��� �������� ��������� ������� �������������� ����������, ���, ������ ����� �������, ������������ �������� ���������, ��������� ��� ��������� ��������� ������������� ���������� ������� � ������, ��� � ������ �������� ������������� ��� ��������. �������, ��� ���������� � ����� ����������� �������� � ������� ��������������� �������� �� ���� ������� ��������������. ����� �����, ��� ����� ��������� �������� �� ��������, � �� ����������� �������. 
<P align=justify>�, �������, �������� ��������, ���������� �� ������� ������, ������ ����������� ����������� � �������������� &#8212; � ������, ���������� ������������ ���������� �������� ���������. 
<P align=justify>�������������� ���������� ������������ �� ���� ����� ��������� ������� �������������� ���������� ��������� ������ ������������� ��������, ����������� �������������� ������ � �������� ����������� ������������, ������������ ������ ���� ���������� ��������������� ��������, ����������� ������� ����� ���������� � �������� ������������ �������� � �������������� �������� �������� � ����� � �����. 
<P align=justify>*<EM>&nbsp;���������. ������ ��. � ��� &#8470;6 � &#8470;7 �� 2007 ���. </EM></P>
<H3>
<TABLE cellSpacing=0 cellPadding=0 width="95%" align=center border=1>
<TBODY>
<TR>
<TD>
<P class=table align=justify></P></TD>
<TD>
<P class=table align=justify><STRONG>��������� 1</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>��������� 2</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>��������� 3</STRONG></P></TD></TR>
<TR>
<TD>
<P class=table align=justify>������������� �������������� ��������� �������� � ��������� 3 ����, ���. ���.</P></TD>
<TD>
<P class=table align=justify>������������ ���� ������ ����� �������� �������������</P></TD>
<TD>
<P class=table align=justify>������������ ���� ���� �����������</P></TD>
<TD>
<P class=table align=justify>������������ ��������, ������������ �������������</P></TD></TR>
<TR>
<TD>
<P class=table align=justify>����� �������</P></TD>
<TD>
<P class=table align=justify>16 000</P></TD>
<TD>
<P class=table align=justify>17 000</P></TD>
<TD>
<P class=table align=justify>12 000</P></TD></TR>
<TR>
<TD>
<P class=table align=justify>��������� �������������� ������� �� ����������������</P></TD>
<TD>
<P class=table align=justify>4 000</P></TD>
<TD>
<P class=table align=justify>3 000</P></TD>
<TD>
<P class=table align=justify>6 000</P></TD></TR>
<TR>
<TD>
<P class=table align=justify>��������� ��������� �������</P></TD>
<TD>
<P class=table align=justify>4 500</P></TD>
<TD>
<P class=table align=justify>3 000</P></TD>
<TD>
<P class=table align=justify>3 500</P></TD></TR>
<TR>
<TD>
<P class=table align=justify>����������� � �����������</P></TD>
<TD>
<P class=table align=justify>12 000</P></TD>
<TD>
<P class=table align=justify>11 000</P></TD>
<TD>
<P class=table align=justify>12 500</P></TD></TR>
<TR>
<TD>
<P class=table align=justify><STRONG>���������� ��������</STRONG></P></TD></TR>
<TR>
<TD>
<P class=table align=justify>������ ������������� ��������� (���) ���. ���.</P></TD>
<TD>
<P class=table align=justify>12 500</P></TD>
<TD>
<P class=table align=justify>12 000</P></TD>
<TD>
<P class=table align=justify>9 000</P></TD></TR>
<TR>
<TD>
<P class=table align=justify>������ ����������, %</P></TD>
<TD>
<P class=table align=justify>104,17</P></TD>
<TD>
<P class=table align=justify>109,09</P></TD>
<TD>
<P class=table align=justify>72,00</P></TD></TR>
<TR>
<TD>
<P class=table align=justify>����� �������, %</P></TD>
<TD>
<P class=table align=justify>166,67</P></TD>
<TD>
<P class=table align=justify>181,82</P></TD>
<TD>
<P class=table align=justify>144,00</P></TD></TR>
<TR>
<TD>
<P class=table align=justify><STRONG>�������� ����������� ��������� �� �������� ������������ ���������� (����� ���������)</STRONG></P></TD>
<TD>
<P class=table align=justify>&#8470;1 (12 500)</P></TD></TR></TBODY></TABLE></H3>
<TABLE cellSpacing=0 cellPadding=0 width="95%" align=center border=1>
<TBODY>
<TR>
<TD>
<P class=table align=justify><STRONG>���������� �������������</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>������������� �������� ���������� ������������� ��������������</STRONG></P></TD></TR>
<TR>
<TD>
<P class=table align=justify>R�=���/? Ik</P></TD>
<TD>
<P class=table align=justify>������ ������ ����������, ������� �������� ������������� ��������������, ������������ ������������� ���������� � ������ �������� �������</P></TD></TR>
<TR>
<TD>
<P class=table align=justify>R��=? �� i (1-?i)/ ? Ik</P></TD>
<TD>
<P class=table align=justify>������������ (����������) ����������</P></TD></TR>
<TR>
<TD>
<P class=table align=justify>R�����=? ��j �����/ �� �����</P></TD>
<TD>
<P class=table align=justify>��������������� ������ �� ���������������� ������ ����������� � �������� ��������������</P></TD></TR>
<TR>
<TD>
<P class=table align=justify>Rs=ST ����/? Ik</P></TD>
<TD>
<P class=table align=justify>����������������� ������������� ���������� (������������� ������� ���������� �� ��������� �������)</P></TD></TR>
<TR>
<TD>
<P class=table align=justify>���</P></TD>
<TD>
<P class=table align=justify>���� �����������, �������������� �� ������ ������� �������������� ����������. ����� ������� (����), � ������� ���=? Ik</P></TD></TR></TBODY></TABLE>
<H3>&nbsp;</H3
>
<P align=justify></P>
<TABLE cellSpacing=0 cellPadding=0 width="95%" align=center border=1>
<TBODY>
<TR>
<TD>
<P class=table align=justify>������� ������� �������������� <STRONG>���������� ����������</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>����������</STRONG></P></TD></TR>
<TR>
<TD>
<P class=table align=justify>? �� i (1-?i)</P></TD>
<TD>
<P class=table align=justify>1. ����� ������ ������� �� ��������� ��� ������� �� ���� ����������� ������ (0, T) �� ������� ��������������� ����� ������� (����������� (1-?i))</P></TD></TR>
<TR>
<TD>
<P class=table align=justify>? ��j �����</P></TD>
<TD>
<P class=table align=justify>2. �������������� ������� �� ���������������� ����� ������� � ������ ��������� � ������� �� ���� ����������� ������ (0, T). ��� ���� ����������� ������� ��������, ���������� ��������, ����� ��������.</P></TD></TR>
<TR>
<TD>
<P class=table align=justify>ST ����</P></TD>
<TD>
<P class=table align=justify>3. ��������� ��������� �������� ��� ��������������� ������� (�������) � ����� ������������ ������� (T)</P></TD></TR>
<TR>
<TD>
<P class=table align=justify>? Ik</P></TD>
<TD>
<P class=table align=justify>4. �������������� ������� �� ����� �� ���� ����������� ������ (0, T).</P></TD></TR></TBODY></TABLE>
<H3>&nbsp;</H3>
<P align=justify>�������� ��������� ����� ����� �� ��� ���������. ��������� � 1 ���������� �� �������� ���������� � ������������ � ���������� ����� �����������. ��������� � 2 ��������������� �������������� ����������. ��������� � 3 &#8212; ������� ������-�������. � ������ ���� ��� ������� �������� ������ �������. ����� �� �������� �������?</P>
<P align=justify>��������� ������� ������ ������������� ����������, ������� ��������� ��������. 
<P align=justify> 
<P align=justify>
<TABLE cellSpacing=0 cellPadding=0 width="95%" align=center border=1>
<TBODY>
<TR>
<TD>
<P class=table align=justify></P></TD>
<TD>
<P class=table align=justify><STRONG>������� �������</STRONG></P></TD></TR>
<TR>
<TD>
<P class=table align=justify><STRONG>���������</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>�������������</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>�������������</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>������������</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>����� ���</STRONG></P></TD></TR>
<TR>
<TD>
<P class=table align=justify>� 1 &#8212; ����� ������������� ���������������� ���������</P></TD>
<TD>
<P class=table align=justify>$12 ���.</P></TD>
<TD>
<P class=table align=justify>$16 ���.</P></TD>
<TD>
<P class=table align=justify>$85 ���.</P></TD>
<TD>
<P class=table align=justify>$113 ���.</P></TD></TR>
<TR>
<TD>
<P class=table align=justify>� 2 &#8212; ����� ��������� � ��������� ������������ �� �������� ������ &lt; 3 ���</P></TD>
<TD>
<P class=table align=justify>$15 ���.</P></TD>
<TD>
<P class=table align=justify>$30 ���.</P></TD>
<TD>
<P class=table align=justify>$11 ���.</P></TD>
<TD>
<P class=table align=justify>$56 ���.</P></TD></TR>
<TR>
<TD>
<P class=table align=justify>� 3 &#8212; ����� �������������� ���������</P></TD>
<TD>
<P class=table align=justify>$18 ���.</P></TD>
<TD>
<P class=table align=justify>$4 ���.</P></TD>
<TD>
<P class=table align=justify>$1 ���.</P></TD>
<TD>
<P class=table align=justify>$23 ���.</P></TD></TR></TBODY></TABLE></P>
<H3><FONT size=3>Investment Efficiency Calculations by Full Economical Results (FER) methodology (Dollars in thousands)</FONT></H3>
<P align=justify></P>
<TABLE cellSpacing=0 cellPadding=0 width="95%" align=center border=1>
<TBODY>
<TR>
<TD>
<P class=table align=justify><STRONG>���� (Years)</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>0</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>1</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>2</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>3</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>4</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>5</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>6</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>7</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>8</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>9</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>10</STRONG></P></TD>
<TD>
<P class=table align=justify><STRONG>11</STRONG></P></TD></TR>
<TR>
<TD>
<P class=table align=justify>������ �� ��� (FER)</P></TD>
<TD>
<P class=table align=justify>-3,680</P></TD>
<TD>
<P class=table align=justify>-3,425</P></TD>
<TD>
<P class=table align=justify>-1,812</P></TD>
<TD>
<P class=table align=justify>1,585</P></TD>
<TD>
<P class=table align=justify>7,038</P></TD>
<TD>
<P class=table align=justify>14,958</P></TD>
<TD>
<P class=table align=justify>25,840</P></TD>
<TD>
<P class=table align=justify>39,561</P></TD>
<TD>
<P class=table align=justify>56,689</P></TD>
<TD>
<P class=table align=justify>77,905</P></TD>
<TD>
<P class=table align=justify>104,029</P></TD>
<TD>
<P class=table align=justify>+134,611</P></TD></TR>
<TR>
<TD>
<P class=table align=justify>������ �� NPV</P></TD>
<TD>
<P class=table align=justify>-4,600</P></TD>
<TD>
<P class=table align=justify>-3,892</P></TD>
<TD>
<P class=table align=justify>-3,301</P></TD>
<TD>
<P class=table align=justify>-2,810</P></TD>
<TD>
<P class=table align=justify>-2,399</P></TD>
<TD>
<P class=table align=justify>-2,058</P></TD>
<TD>
<P class=table align=justify>-7,773</P></TD>
<TD>
<P class=table align=justify>-1,536</P></TD>
<TD>
<P class=table align=justify>-1,195</P></TD>
<TD>
<P class=table align=justify>-1,030</P></TD>
<TD>
<P class=table align=justify>-893</P></TD>
<TD>
<P class=table align=justify>-779</P></TD></TR></TBODY></TABLE>


</body>
</html>

