<!--
ID:35451363
TITLE:��� �. �������� ������������ ������������ ����� � ������� ������������: ����� ����������� �����
SOURCE_ID:10030
SOURCE_NAME:�����: ������� ����. ����� ���
AUTHOR:�.�. ��������, ����. ����. ����, ����� ���
NUMBER:4
ISSUE_DATE:2005-08-31
RECORD_DATE:2005-10-25


RBR_ID:1093
RBR_NAME:���������� ����
RBR_ID:2043
RBR_NAME:������������� ���������� ���������
-->
<html>
<head>
<title>
35451363-��� �. �������� ������������ ������������ ����� � ������� ������������: ����� ����������� �����
</title>
</head>
<body >


<P align=center>Kuhn B. <FONT face="Times New Roman">The Communication policy of the European Central Bank: A review of the first five years // Intereconomics. &#8211; Hamburg, 2005. &#8211; Vol. 40, &#8470; 1. &#8211; P. 2</FONT>2 &#8211; 28.</P><FONT face="Times New Roman">
<P align=justify>� ������ ����������� ������������ </FONT>Deutsche Bundesbank<FONT face="Times New Roman">, ���������� ������������� ������������ ���������� ��������� (��������) �.���� ������������� ���������� ���� �������� ������������ ������������ ����� (���) � ������� ��������������� ���������� � ����� ��������� � ��������.</P></FONT><FONT face="Times New Roman">
<P align=justify>��������� ������ � �������� ��� ��������:</P>
<UL>
<UL>
<P align=justify>
<LI>����������� ������������ ���, ��������� ������������� ����������� ������������ � ���������� ������ ��������; 
<P align=justify></P>
<LI>���������� � ������������� �������� � ���������� �������� ��������� � ��������; 
<P align=justify></P>
<LI>������������� ���� ��� �������� � ���� ���� ����������� �������������� ������������, ������������� �� ��������� � ���, � ������ �������� ���� ����� �� ����� �� ����, ��� � ������; 
<P align=justify></P>
<LI>�������� ���� ���� ��� ��������� ������������� � ������������ ���������� � ��; 
<P align=justify></P>
<LI>���������� ����, ����� ��������� ����� ���� ���� ��� ����� ������ ������ � ����� ������ ������������ ������. </LI></UL></UL>
<P align=justify>� ���������� ������� ��� ������� ������ ������ ����� ���� �����. ������ �����, ��� ������� ���������� ������������ ���: � ��� 2003 �. ������� �������� � ���� ���� �� ��������� 2% � ���. ������ � ��� �� �� ��������� �� ������������ ����������, ������������ ����� �������������� �����, ������ ��� �������� ��������������� ��������, �������� ���������, ���������� ������ �����, ������� � ���������� ������������, ������� ������� �����������. � ���������� ������������� � ��������� ���� ��������� ����� ���� �� ��������� � ������� ����������� ��� ��������� ���������� ������� �������, � �� �������� ������� ����. ���������� ������������� �������� � ������ �� �������� ��� ������� ������������ � ������������� ������� � �������� � ���, ��� ������������� ���������� ��������.</P>
<P align=justify>� ������� ������ ���� ��� ���������������� ��� �������� �������� ������� ����������� �������������� ���� ���������������� �� ������� � ������� ������� ������ ������ �� ������� �������������� ��������� ����� ���� ���� �� ��������� � �������� ������������� ��������.</P>
<P align=justify>��� �������������� ���������� � ����� ������������ �� ������ ����� �������� �������� ���������� (���), �� � ����� ���������� ������������ ��������� (</FONT>Monthly Bulletin<FONT face="Times New Roman">) � ������� ������� (</FONT>ECB Annual Report<FONT face="Times New Roman">), ����������� �����-�����������, ��������� ����������� ������������� ����� � �.�. � 2002 �. � ������ ����������� �������� ���� �������� ������� &#8220;</FONT>Euro 2002 Information Campaign<FONT face="Times New Roman">&#8221;, ������������������ ����� ������� ������� ����� ��, � ��� ����� �� ������� ���� ������� �������-�������������� �����. � ������� �� ���������� ��������� � ���������� ��� ��������� ��������� ������������� ��������� � ����� ���������� �������� � ���������, � ������� ������������ ����� ��������� � ����������� �������. ���������� � ��������� ��� ��������� ��������� ������ ����� ���������� ������ �������� ���������� ������.</P>
<P align=justify>�������������� ������� ������� ��������������� ����� ��������� �������������: 1) ������������� ����������, ��� ������������� ������ �� ��� ���������� &#8220;�������� �����������&#8221; (</FONT>active receivers<FONT face="Times New Roman">) ����������; 2) ��������� ������� ����� ����������� ���������� �����������; 3) ��������������� ���������� ����� ������ ���������������, � �� ������������� ��������. � �������� ����������� ���������� ���������, ��������, ���������� ������������� �������� ����� ��� ������������ ���������, �� �� �������� ����� ������������. �������� � ��� ���������� �� ������ ����������� ������ ����� ���� ����. ����� ������� ������� ��� � ������ ������� �������� ����� ���������, ��� ���������� ��� �������, � ��� ��������� � �������� ���� ��������� ����������� �������������� �������, ������� �������� �� ���������� ����� � ������ ����� ���� ����. ���������� ��� ��������� ������� �� ��������� ����������� ����������, �� �������� ������������� ������. ��� ������ ������������ ��� �.-�. �����, �������� ����� &#8211; ��� �� �����������, � ���������� ������ �� ���������� ������������ ���. ������ ���� ����������� ����� ��� �� ���������� ��������� ����� �� ����������� ��� � ���, �������� ������ �������� ���� � �������� ������ � ������ 2002 ����.</P>
<P align=justify>� ���, ��� �������� ������ ��� �� ������ �������� � ������ �����������, ��������������� ������ � ������������ �������� ���� � ��������. �������� ����������� ����������� � ��������� � �������� ������ ���� ��������� ���. ����� ����������� ��������� � ����� � ������������ � ���, �������� � ��� ���������� &#8220;���������� ������&#8221;. ����� ���, ������������, ����������� � 2003 �. </FONT>Deutsche Bank<FONT face="Times New Roman"> � ����������� �������������� ���������� ��������, �� ����������� ����� ����� ��������� ���� � ���������� ���. � ���������, ���� ����� �� ��� ������� ������� ������� �� ��������� ���������. � ���������� �������������� ������� �������� �������� ����� ����� ���� ��� ��������� ������</FONT>.<FONT face="Times New Roman"> �������������� ���� ������� ����������� ���, ��� �������� �������� ���� � �������� ������ ������� ����� ��������� �������� ����������� ���������� � ������ ������. ����������� ������� ��� �� ��� ������ ���� ����� ������, � � ������ 2004 �. ���� ���������,</FONT> <FONT face="Times New Roman">��� ������ � ���� �������� &#8220;�������� ��������� ��������� �������� ���������, � �� ��������� ��� ������������ �����&#8221;.</P>
<P align=justify>�� ������ </FONT>Eurobarometer<FONT face="Times New Roman">, � 1994&#8211;2003 ��. ���� ���������� �������� ���� ����� ������� �� �������� � 53% �� 68%, � � ������� ���� ���� &#8211; � 60% �� 76%. � ������� 1 ������������ ������ � ���� ���������, ������������ �� � ������ ���� � ��������� ������� �� (����� �������� � ������ 2003 �.).</P>
<P align=justify>��� ���������� ���������� ������������, � ������� ������� ���� ����, ������� ��������, ���� ���������, ����������� �������� ����, ���� �������� ���������� �� ���� ����: � �������� &#8211; 70% ������ 76%. ������������� ��������� ������� ����� ��������� � ���� �� ������ ����������� ����������� ������������ � ������ � �� �����������, ������� �������� �������� ����������� ���������� � ���� ��� ��������� (������� 2). ����� �������, ��� ������� ���������� � ����� ������������ ��� ������� ������� � �������� ����� �����������, ����������� �� �����������, ����� � � ������.</P></FONT><FONT face="Times New Roman">
<P align=right>������� 1</P><B>
<P align=center>&#8220;��&#8221; � &#8220;������&#8221; ���� � ������� �� </B>(<STRONG>� ��������� �� ����� ����������� ���������)</STRONG></P></FONT>
<P align=center>
<CENTER>
<TABLE cellSpacing=1 cellPadding=7 width=531 border=1>
<TBODY>
<TR>
<TD vAlign=top width="40%"><B><FONT face="Times New Roman" size=3>
<P align=center>������</B></FONT></P></TD>
<TD vAlign=top width="20%"><B><FONT face="Times New Roman" size=3>
<P align=center>��</B></FONT></P></TD>
<TD vAlign=top width="20%"><B><FONT face="Times New Roman" size=3>
<P align=center>������</B></FONT></P></TD>
<TD vAlign=top width="20%"><B><FONT face="Times New Roman" size=3>
<P align=center>�� �����</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><FONT face="Times New Roman" size=3>
<P>����������</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>88</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>10</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>2</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><FONT face="Times New Roman" size=3>
<P>�������</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>86</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>12</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>2</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><FONT face="Times New Roman" size=3>
<P>������</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>82</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>13</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>5</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><FONT face="Times New Roman" size=3>
<P>��������</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>78</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>14</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>8</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><FONT face="Times New Roman" size=3>
<P>�������</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>76</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>18</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>6</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><FONT face="Times New Roman" size=3>
<P>�������</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>76</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>20</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>4</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><FONT face="Times New Roman" size=3>
<P>����������</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>76</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>20</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>4</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><FONT face="Times New Roman" size=3>
<P>���������</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>76</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>22</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>2</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><FONT face="Times New Roman" size=3>
<P>�������</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>72</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>18</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>10</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><FONT face="Times New Roman" size=3>
<P>��������</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>70</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>22</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>8</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><FONT face="Times New Roman" size=3>
<P>������</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>70</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>27</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>3</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><FONT face="Times New Roman" size=3>
<P>����������</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>67</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>30</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>3</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><B><I><FONT face="Times New Roman" size=3>
<P>�� 12 ������� ���� ����</B></I></FONT></P></TD>
<TD vAlign=top width="20%"><B><I><FONT size=3>
<P align=center>76</B></I></FONT></P></TD>
<TD vAlign=top width="20%"><B><I><FONT size=3>
<P align=center>18</B></I></FONT></P></TD>
<TD vAlign=top width="20%"><B><I><FONT size=3>
<P align=center>6</B></I></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><FONT face="Times New Roman" size=3>
<P>�����</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>53</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>39</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>8</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><FONT face="Times New Roman" size=3>
<P>������</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>41</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>48</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>11</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><FONT face="Times New Roman" size=3>
<P>��������������</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>24</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>63</FONT></P></TD>
<TD vAlign=top width="20%"><FONT size=3>
<P align=center>13</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><B><I><FONT face="Times New Roman" size=3>
<P>�� �� � �����</B></I></FONT></P></TD>
<TD vAlign=top width="20%"><B><I><FONT size=3>
<P align=center>68</B></I></FONT></P></TD>
<TD vAlign=top width="20%"><B><I><FONT size=3>
<P align=center>27</B></I></FONT></P></TD>
<TD vAlign=top width="20%"><B><I><FONT size=3>
<P align=center>5</B></I></FONT></P></TD></TR></TBODY></TABLE></CENTER><FONT face="Times New Roman">
<P align=right>������� 2</P>
<P align=center><STRONG>�������� �������� ��������� ���������� � ��������� � ������� ��<BR>(� ���������)</STRONG></P></FONT>
<CENTER>
<TABLE cellSpacing=1 cellPadding=7 width=427 border=1>
<TBODY>
<TR>
<TD vAlign=top width="75%"><FONT face="Times New Roman" size=3>
<P>�����������</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>59</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="75%"><FONT face="Times New Roman" size=3>
<P>���������� ������</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>35</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="75%"><FONT face="Times New Roman" size=3>
<P>�����</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>23</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="75%"><FONT face="Times New Roman" size=3>
<P>�������</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>19</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="75%"><FONT face="Times New Roman" size=3>
<P>��������</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>18</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="75%"><FONT face="Times New Roman" size=3>
<P>��������</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>15</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="75%"><FONT face="Times New Roman" size=3>
<P>������� � ������ �������</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>12</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="75%"><FONT face="Times New Roman" size=3>
<P>����� � ��������� ���������</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>11</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="75%"><FONT face="Times New Roman" size=3>
<P>��������������</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>7</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="75%"><FONT size=3>
<P>CD-ROM</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>5</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="75%"><FONT face="Times New Roman" size=3>
<P>������������ ���� ������</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>4</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="75%"><FONT face="Times New Roman" size=3>
<P>�������</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>3</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="75%"><FONT face="Times New Roman" size=3>
<P>�� �������� ������� ����������</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>6</FONT></P></TD></TR></TBODY></TABLE></CENTER><FONT face="Times New Roman">
<P align=justify>����� �������� ������� ������� ��������� ��
 � ��� � ��� ��������, ��������� �� ��������� �� ��������� � ���� ������ ����������, � ��� ����� �������� ����������� �������� ����� &#8211; ��������. ����������� ����� ������������ ��� �� ������������ �����������, ��� � �������������� �������������</FONT> <FONT face="Times New Roman">������ &#8211; �������, ����������, ���������������� � �������������� �������������� �����������. ��� ��� ����������� ����� ���� �������� � � ����, ������� ������������ ����� ���� �� �������� �������� ���������. ����������� �������� ������������ ���� �� ������ � ����� ������ ����������� �� ������� �������� �������, ��������� � ��������� �������. ��� ��������� � ���� ����� ������������� �������, ��� ��������� ����������� ������������ � �������� ������������ � ������ ������. ������ � ���� ��� � ������ ����� �������������� ����, ��� ����������� ������� ����� ������������� ���� �� � ����������� ��������� (&#8220;����������� �����&#8221; ��� &#8220;���������&#8221;), � � ����������� ������� &#8211; &#8220;�����������������&#8221;, &#8220;���������������&#8221; � �. �. ������������ � ���� ��������� ������� � ����������� ��� � ��� ��������.</P>
<P align=justify>��� ������ ������������� ������� ��������� ���� � �������� ������� ���� ����� �������� � ������� ������������. ��� ���� ����� �������� ��� �������: 1) ���������� &#8220;�����������&#8221; ���������� � ��� �� ��������� ������� ��� �, ��������, � ����� ��������; 2) ���������� ������� � ������������ ��������� � ���������� ������������ ���, ������� ������������� �������� ������; 3) �������� ������������ ������������� ������ � ���� � ������� ��������� ��������, ������� ����� � ������� ����������. � ���� ��������� �������� ������� ������������ ������������� �������, ���� ����, ������� ��� �������� ��� ����� ��������� ��������� ����: &#8220;���� &#8211; ���� ������&#8221; (</FONT>The euro: our money<FONT face="Times New Roman">). ����������� ����� ��������� ����� ���� ���� ����� �������� ������ ���������� � ����� ��� � ������ ������������ ��������������� ������������.</P>
<P align=justify>��� ���������� �������������� ��������, ������������������ �������� ���, �� ������� ��� ������ �������� </FONT>Bundesbank<FONT face="Times New Roman">, ��� �������� &#8220;��������� ��������&#8221; �������� ����������� ������������ ��� � ������� ���������� ������ �������������� �� ������������ �������. ������ � ������������ ��� ����������� ��� ���������� �� ������ ����������������� ������������ ���������� ������, �� � ��������� ������ ������� ������������ ��������, �� ���� ��������� ��������, ����������� �������� ����������� ��������� ������� ���. � ��������� ����� </FONT>Bundesbank<FONT face="Times New Roman"> � ��� �� ������ ������������� ���������� ����������� ��������� ���������. ��������, ���� ��������� ��������� ������������, ���, ��������� �������� �������-�������������� ����� � �� &#8220;����������� ������������&#8221;, ������������ ������� ������ ����� ��������������� �� ������������ � ������� ��������. ������, ��������, ����������, ��� ������������ ������� ����� ���������� � ����������� ������������� ��� � �������������� ���������� ���</FONT> <FONT face="Times New Roman">������� ���� &#8211; ����������� ������������ ���. ���������� ��������� ������ ������ ��������, ��� ���� ������ ���������� � �������� ��������, ����� ��� ���������� ��������� �� ��, ��� ������������ �����, �������� ��� ���������� �������� �� �������, ������� ���������� ������ ���������� ��� ��������� ������ ������.</P>
<P align=justify>� ����� ���������� �������� ��� � ������� ������������ �� ��������� ��������� ����������� ������������. ���� �������� � ���������� ��������� � �������� �������� ��� � ��������� �������� ��� ��������, �� � ��������������� ������� ����� ��������� ���� ������� �� ��� ������. ��� ����������� ���� �������� ���������� ��������� � ��������� ���� ������ ����������, � ��������� � ��� ������ ���� ���� ������������ � ��������� ���. ��������� ������ ������� ������������ ��������� � ���� ����� ������ ��������� ������������� ��� � ������������� ��� �������� ����������� ������������ ���.</P></FONT>


</body>
</html>

