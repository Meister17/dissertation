<!--
ID:37145129
TITLE:��������� � ���������� ��������� �����������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:1
ISSUE_DATE:2007-01-15
RECORD_DATE:2007-01-15


-->
<html>
<head>
<title>
37145129-��������� � ���������� ��������� �����������
</title>
</head>
<body >


<P align=justify>���������� ����������� (�����������) ������������ ������, �������� �������� �� ���������� ���������� ��������, ����������, ��� � ������������ � ��������� ������������ ����� �������� ���������� ��� ��������� ��������������� (���������) ����������� ����� � ��������� ���������������� (����������) �������� ���������� ��������� ��������� �����������:</P>
<TABLE cellSpacing=2 cellPadding=7 width=602 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="7%"><FONT size=3>
<P align=center>&#8470;</P>
<P align=center>�/�</FONT></P></TD>
<TD vAlign=top width="27%"><FONT size=3>
<P align=center>������������</P>
<P align=center>���������</P>
<P align=center>�����������</FONT></P></TD>
<TD vAlign=top width="13%"><FONT size=3>
<P align=justify>��������-������� �����</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>����<BR>������ ��������</FONT></P></TD>
<TD vAlign=top width="39%"><FONT size=3>
<P align=center>�����, �������, �.�.�. ����������� ������������ (�����������).</P>
<P align=center>���� ������������ ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%"><FONT size=3>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="27%"><FONT size=3>
<P>���� ���������� � ������������</FONT></P></TD>
<TD vAlign=top width="13%"><FONT size=3>
<P align=center>2102</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=justify>09.08.2006</FONT></P></TD>
<TD vAlign=top width="39%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>123022, ������, �/� 30;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>62 ���</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%"><FONT size=3>
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="27%"><FONT size=3>
<P>�����</FONT></P></TD>
<TD vAlign=top width="13%"><FONT size=3>
<P align=center>938</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=justify>23.01.2006</FONT></P></TD>
<TD vAlign=top width="39%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>123022, ������, �/� 39;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>60 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%"><FONT size=3>
<P align=center>&#8470;</P>
<P align=center>�/�</FONT></P></TD>
<TD vAlign=top width="27%"><FONT size=3>
<P align=center>������������</P>
<P align=center>���������</P>
<P align=center>�����������</FONT></P></TD>
<TD vAlign=top width="13%"><FONT size=3>
<P align=center>��������-������� �����</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>����<BR>������ ��������</FONT></P></TD>
<TD vAlign=top width="39%"><FONT size=3>
<P align=center>�����, �������, �.�.�. ����������� ������������ (�����������).</P>
<P align=center>���� ������������ ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%"><FONT size=3>
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="27%"><FONT size=3>
<P>������� ���� ������������� �����</FONT></P></TD>
<TD vAlign=top width="13%"><FONT size=3>
<P align=center>2701</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=justify>20.09.2006</FONT></P></TD>
<TD vAlign=top width="39%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>123022, ������, �/� 39;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>60 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%"><FONT size=3>
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="27%"><FONT size=3>
<P>�����������-������</FONT></P></TD>
<TD vAlign=top width="13%"><FONT size=3>
<P align=center>3146</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=justify>11.10.2006</FONT></P></TD>
<TD vAlign=top width="39%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>123022, ������, �/� 66;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>60 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%"><FONT size=3>
<P align=center>5</FONT></P></TD>
<TD vAlign=top width="27%"><FONT size=3>
<P>�������-�����������-��� ����</FONT></P></TD>
<TD vAlign=top width="13%"><FONT size=3>
<P align=center>2823</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=justify>27.09.2006</FONT></P></TD>
<TD vAlign=top width="39%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>123022, ������, �/� 11;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>60 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%"><FONT size=3>
<P align=center>6</FONT></P></TD>
<TD vAlign=top width="27%"><FONT size=3>
<P>���������-���������</FONT></P></TD>
<TD vAlign=top width="13%"><FONT size=3>
<P align=center>3021</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=justify>27.09.2006</FONT></P></TD>
<TD vAlign=top width="39%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>123022, ������, �/� 4;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>60 ����</FONT></P></TD></TR></TBODY></TABLE><I>
<P align=right>"������� ����� ������", 2006, &#8470; 67&#8211;69</P></I>


</body>
</html>

