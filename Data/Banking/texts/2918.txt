<!--
ID:35326183
TITLE:������������� ���� � �����
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:
NUMBER:4
ISSUE_DATE:2005-04-30
RECORD_DATE:2005-09-23


-->
<html>
<head>
<title>
35326183-������������� ���� � �����
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgwhite>
<TD>
<P class=Rubrika align=justify>��������� ������ �������� � ����� &#8470;102</P>
<P class=question align=justify>� ������������ �� ������� 8 ������ &#8470;202&shy;�� �� 29.12.2004 ��������� ������ ������� �� ��������� ������������������ �� ���������� ������ (������ ���� ���� ������������������) ������������ �� ���� ������������. ����� ��������� ������ ����� &#8470;102 &#171;������ � �������� � �������&#187; ������� ������������ ��� ��������� ������ ������? 
<P class=Main align=justify>�������� ��������� &#8470;205&shy;� �������� � ������������� ����� ���������� � ������������ � �� ������������� ���������. ����� ����, ��������� ���� ������� � �������� �� ��������� ������ ������������, ��� �������, �� �� ���������. ������ � �������, �� ������� ����� �� ����� ���� �������� � ���������� ������, ��������� �� ������ �� ����� ������ (������) ������� ��� ��������. 
<P class=Main align=justify>����� �������, ������� �� ��������� ������ ������� �� ��������� ������������������ �� ���������� ������ �� ������ ��� ���, ������������� �� ���� ������� ������������, ������� ��������� �� �/� 70206 &#171;������� �� ���������� ��������&#187; � �������� � ����� &#8470;102 �� ������� 26214 &#171;�������������� ������ ���������� ����&#187;. 
<P class=Rubrika align=justify>������������ ������������� 
<P class=question align=justify>��������� ����� ������ &#8470; 1399&shy;� �� 24.03.2004 ������� ���������� ����� � ��������� ��������������� ������� �������������� ����� ��������, ��������� � �������������� ������� ������ �� ������������ ����� ���������� �� ������� ��� ���������� ������������ � �������� �����, � ��� ����� � � ����� ��������, ��������� � ��������������� ��� ������� ��������� ���������� (�����������). 
<P class=question align=justify>�������� ��������� 1.2.2 ����� �������� ���� ������ ����� ������������� ���� ������������� �������� ��� ����������� �������� �� ���������� ������������� ���� ���������� � ������������ � ��������� ��������, ����� ���������� �� �������� �����������. ������ � ������ ��������� ��������� �������������� ��������� ����� ����������� ���� �������� ��������� ����� �� ���������������. ��� ��������� �������� ������������ ������������� ��� ������������� ��������� � � ������ ������������� �������� (����� � ������������ � ��������� ���������� ������������ ���������� ������������� � ������ ��������������� �������, ����� ����������� ������ ���������� ����������� ������)? 
<P class=Main align=justify>����������� 12 � ��������� &#8470;205&shy;� �� ������������� ��������� ����, �������� �� ������ �� ����� �������� � ������������� ����� ����������, �� ����� �� ����� ������������ ������������� ��� ��������������� ���������� ��������� (���������) ����� ������������. � ������ 4.75 ����� II ����� ��������� (� �������� �������� &#8470;1399&shy;� �� 24.03.04.) ����� ��������, ��� ����� �� ������� ����� 478 &#171;�������� � ������������� ����� ����������&#187; ����������� ������ ��� ��������� ������������� ���� ���������� ���������� (����������), �� ���������� ���������� (�����������) � �������� � ������� ��&shy;�� ������������� ���������. 
<P class=Main align=justify>����� �������, ��� ������������ ���� ������� ����������� ���������� ����� ������ ��� ��������� � ������������� ����� �������������� ������������ ������������� �� ������ ���������� ������. � �� �� ����� ��������, ��� ���� ������ ����� ����� ���������������� ������������� ���� ��������, � ��� ����� � ������ �� ������������ ���� ����������. 
<P class=Rubrika align=justify>�������� � ������������ ��������� 
<P class=question align=justify>����� �� ���� �������� �������� �������� �� ������ �����, �� �� ���� ��������� �� ��������� ����� �� �������������� ��������� ������ �� ������������ (�������) ����� �� ����������� ��������? 
<P class=question align=justify>�� ����� ����� ����������� �������� �������� � ������� � ��������� � �������� ������ �� �������� ��������? 
<P class=question align=justify>�� ����� ����� ������� ����� ���� ����� �� ����������� ��������, ������������� �� �������� ��� �������? 
<P class=Main align=justify>����� ���������� ������ �� ������������ (�������) ����� �� ����������� �������� �� ������ �����, �� �� ���� ��������� �� �������������� �� ��������� ��������� �������� ������������� ������� 990 �� ��. 
<P class=Main align=justify>���� �������� �������, ����������� � ������������ ����� (������������) �� ��������� ����������� ��������� �������� �������������� �� �/� 30601 � 30606. 
<P class=Main align=justify>����� ����, ��� ������� ������������� ������������ �������� �� ��������������� ������������ ����������� ����� ������ � �� ��������� ������� �� �������������� �����, ���������� ������� ������������ �������� �������������� � ������������ � ����������� ����������������� ���������� ��������� �� ��������� ����������� ��������� ��������. ������������� ���������� ��������� ������ � ������������ ���������� ����������� ��������� �� �� ���������� ������������ ����������� ������� �����������. 
<P class=Main align=justify>� ������������ � ������� 1.3 ����� I ��������� &#8470;205&shy;� ���� ������ �������������� ����������� � ��������� � ������� �������� ����������� ��������� � ������������� ����� ������������ �������� �� ������������ (�������) ����� �� ����������� ��������. 
<P class=Main align=justify>�������� ���������� ������ 996 �� �� ����, ����������� � ������������ �� ��������� ���� ������������� ������ �� ���� ���������, �������� �������������� ����������. ����������� �� ������ �������� � ����� ������� ������������� ��� ��������� ��������.&nbsp; 
<P class=Rubrika align=justify>�������� ����� �� ���� ��������������� ������� 
<P class=question align=justify>��� ���� ����� �������� � ����� �����������. ��� ���� &#8212; �������� ����������, ���������� �� ���, &#8212; �����������, ��� ����� ���� �����������, �� �������� � ������ ��������������� ������� ����������� ���. �������� ������ ���������� ������������� �������� ����������� (����������) ��� ���������, � ����� � ��� ���������� ��������� ����� ������ ������� � ������������ ������ ������ ��� ��������� ������. ���������� �� �������� ��������� ������������� �� ������������� ����� 91803 &#171;�����, ��������� � ������&#187; � ��������� � ������� ���� ���? 
<P class=Main align=justify>��� ����, ��� ����������� � ������� ���������� ��� �� ����� ������������� ������������ � �� ������ ��������������� � ������������ � ������������ �� &#171;� ��������������� ����������� ����������� ���&#187;, �������� ����������� ���������� ��� �������� ������� � �������� � ����������� � �������������� ����� ����� ��������� ����������� �� ���� ��������������� ������� (��� ��� ���������� &#8212; �� ������). 
<P class=Main align=justify>���� ������ ��������������� ������ � �������� � ������� �������� � �����, �������� ������� �� ��������� 5% �� �������� ��� ����������� ������� (��������), �������� ������������ �������� ���������� ���������� ��� ������� ��������. 
<P class=Main align=justify>�������� ����������� �������������, ����������� 5% �� �������� ����������� ������� (��������), ������������ ��� ������� ��������������� ���������, ������������������, ��� ��������� ������������� �� ���� �������� ���������� �/��� � ������ ���������� ����������� (��������). 
<P class=Rubrika align=justify>���������� �������� ������� 
<P class=question align=justify>���� � ������� ���� �������� ������, ����� ������������� �� ������� ���������������� 31 ������� 2004 ���� � ���� ��� �� ���������� ����� 60401 &#171;�������� �������� (����� �����)&#187;. 
<P class=question align=justify>����� �� ���� ����� � ������ 2005 ���� ���������� �� �������������� ������ ���������� (�� 1.01.2005), ���� ������� � ������&shy;��������� �������� 31.12.2004, � �������� ���������� ������ ���������� ��������� ����� �������� ���� &#8212; �� ���� �������� ���������� � ������� ������������� ����� �� 2004 ��� � ������������ � ��������� ����� ������ &#8470;1530&shy;�? 
<P class=question align=justify>����� �� � 2005 ���� �������� ������� ��������� ��������� �� ���� ���������� � ������ ��������������� �������� � ������������ � �. 3.1 ��������� &#8470;215&shy;� �� 10.02.2003 ����� ������������� �������� �������������� ������ �� 2004 ��� ����������� ������������? 
<P class=Main align=justify>� ������������ � �. 2.8 ���������� 10 � ��������� &#8470;205&shy;� ���� ����� ����� �� ���� ������ ���� � ��� (�� 1 ������ ��������� ����) ������������� ������� �������� ������� �� ����������������� (�������) ��������� ����� ���������� ��� ������� ��������� �� ������������� �������������� �������� �����. 
<P class=Main align=justify>����� �������, �� ������ ���������� ���������� �������������� � �������� ���� ������ �� ��������� �� 1.01.2005 � �������� �� ���������� � ������� ������������� ����� �� 2004 ���. 
<P class=Main align=justify>�������� �. 3.1 ��������� &#8470; 215&shy;� ������� ��������� ��������� �� ���� ����������, ������������ �� ���� ������ ���� � ��� (�� 1 ������ ��������� ����) � ������������ � ������������ ������ ����� ������, ���������� � ������ ��������������� <STRONG>�������� �� ���� ������ ���� � ��� ���� �� ��������� ������ ���������� �������� �������������� ������, ��������������� ����������� ������������.</STRONG> 
<P class=Main align=justify>�������������, ���������� ��������������� �������� �� ���� ���������� ����� ���� ����������� �� ����� ������ �������� ���� (��������) ����� ��������� �������������� ������������ ���������� �� �������� �������, ����������� ���������� ������������� ���������� (� ��������� �������� ������������, ���������� ������� �� ���������� � �������������� ������� � ���������� ��� ����).</P></TD></TR></TBODY></TABLE>


</body>
</html>

