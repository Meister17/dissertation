<!--
ID:35283699
TITLE:XIV ������������� ���������� �������� (���-2005)
SOURCE_ID:3144
SOURCE_NAME:������ � ������
AUTHOR:
NUMBER:6
ISSUE_DATE:2005-06-30
RECORD_DATE:2005-09-14


-->
<html>
<head>
<title>
35283699-XIV ������������� ���������� �������� (���-2005)
</title>
</head>
<body >


<P align=justify>� �����-���������� 1&nbsp;&#8211;&nbsp;4 ���� 2005 �. ��������� XIV ������������� ���������� �������� (���-2005). ���� ��������� &#8211; <B>&#8220;�����. �������������. ���������&#8221;.</P></B><I>
<P align=justify>�� ��������� ��������� ���������� ������� ���� �������� ���������� ������� �������� ���������� �������, ��������� �� ���� � �������� ���������, ������� ����������������� ������������� ���������� ������������. ����� ���������� ������������ ������������ ���� ��������� ���������� ���������:</P>
<OL>
<P align=justify>
<LI>��������� �������� ����������� ������� ������ �� ����������� �����;</LI>
<P align=justify>
<LI>���������� ������� ������������� ������������ ��������� �����������;</LI>
<P align=justify>
<LI>��������� � �����;</LI>
<P align=justify>
<LI>�������������� ����� � ����������� �������.</LI></OL>
<P align=justify>� ������ ��������� ������� ������� ������������ ������������ �����, ������������� ���������� � ���������� ������, ������������� ���������� �����������, ���������� ����������, ����������� ���� � ������ ���������� ��������.</P>
<P align=justify>����������� ���������� �������������� ����������� ��������� �������� ��������� ���������� ��������� <B>�. �. �����</B>, ������������ ������ ��������� ������������ �������� ���������� ��������� <B>�. �. �������</B>, ������������ ������������� ���������� ��������� <B>�. �. �������.</B> ����� ����������� ��������� ��������� ���������� �����-���������� <B>�. �. ���������.</P></B>
<P align=justify>���� ����������� ��� ���������� ���������� ���-2005.</P></I>


</body>
</html>

