<!--
ID:35964728
TITLE:�������� &#151; � ������ ������
SOURCE_ID:3106
SOURCE_NAME:���������� ���������
AUTHOR: ��������� ���������, ����������� �������� ��������� 
NUMBER:2
ISSUE_DATE:2006-02-28
RECORD_DATE:2006-03-13


-->
<html>
<head>
<title>
35964728-�������� &#151; � ������ ������
</title>
</head>
<body >


<DIV class=line>
<DIV class=line2><STRONG>��������� ��� &#171;����� ���������&#187;: ����� �������� �������� ��������� ���������� ������� �����. �������� � ����� ��������� ���������� ���� �������� � ����. </STRONG></DIV></DIV>
<DIV class=pad>
<P align=justify>���� ��������� ������� &#8212; ��� ��������� ����� ���, ��� ��� � ���������. ���� ��� ������������ � �� ���� ���, �� �� ���������� ����� &#171;���� ������&#187; ���������� ����� �� ����� �������� �� ����� ������������ ���������. 
<P align=justify>����� ���� &#8212; ������� ������. �� �����, � ������� ������� ���� ��������� ����� �� ���� ������, ��� ��������, ���������� ����� ��������� �������� ��������� � �������� ������. ����� ������� ���� ���� ����� &#171;������������&#187;: ��� ���������-��������� ��������� ��������� ����� ���� � ����������� �������. � ������, ��� ������, �������������� �������� ��� ������ ������ �� ���-���������� ��������� �������� ����� �� ������������ �������. 
<P align=justify>���� ���� ������: ��� ����������� ���� � ���-������������� �������������, ���������������� � ���� ������ ������-���������. ���� ��������� ��������, ��� �� ���-���������� ����� �������� �������� ������������ ����������. 
<P align=justify>��� ������ �������� �������� �������� � �� ��� ����� �������� ��������� �� ��� &#8212; ���� �����. �� 15 ��� ������������� ������������ ���������� ������� ��������� ��������, ���� �� �����, ��������� �������� ������������ ����������. � ��������� � ��������� � ������ ������ ������� ����� ������ ����� ���������� � ��������, � � ������ &#8212; ���� ������ ����������, ������� ���������, ��� �� ����� �����, �� ��� ���� �� ��������� �� ������������, �� ������������. 
<P align=justify>�� ���� �������� �������� �����������, ����� ����������� &#8212; &#171;����� ����� ������&#187;, ������, ����� ��������. � �� ������ ��������� � ������� ��������. � ��� ������ ���������� ���������-���������� ���, ��� �������, ���� ������ �������� ���������, ������ �������� �� 2007 ���? ��������� ��� &#171;����� ���������&#187;: ����� �������� �������� ��������� ������� ������ ��������� ���������� ������� �����. � ���� ����� ���������� ���������� ������� &#171;�����������&#187; � �����������, �������� �� ��������� ���������� ���� �������� � ����. ���� ������, ��� ��������� �� ����������, � �����, ����� ������������� ����������. �� ����� � ��� �� ��� ��� ��������?</P></DIV>


</body>
</html>

