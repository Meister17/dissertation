<!--
ID:37283088
TITLE:� ���������� ����������� ������
SOURCE_ID:3096
SOURCE_NAME:����������� ���������
AUTHOR:
NUMBER:11
ISSUE_DATE:2006-11-30
RECORD_DATE:2007-02-14


-->
<html>
<head>
<title>
37283088-� ���������� ����������� ������
</title>
</head>
<body >


<DIV align=justify>
<P>������ �������� ������ � ���������� ���� ���������������� ����� �������� �� ��������� ���������� ��������� ���������� ����������� ������ (20 �������� � 4 ������� 2006 �.), � ��������� ��������� � �������� ����� �������� ������� ��������� ��������. � ������ "�������� �����" �������� ���������� �������� �� �������� ���������� ����������� ��������, �������� �������� �������� � ��������, ����������� � ���� ��������. ������� ������� � ��������������� ���������� �������� ����������� ��� ������ �������� �� �������� �������� � ���������������� ����� � ���������� ������������ ������� � ������������ �� �������� ���� ������ ����.</P>
<P>������� �������� ��� ��������� �������� ����������� � ������������� ������ �����, � ����� ����������� ����� ��������������������� � ���������� ���������������� ������������ �� ����� ������ ��� ������ ���� ����� ���������� ��������� �� ������ ����������� ����������� ��� ���������� �������� ��������������� ��������� ����������� � ����������� �������. ������������ ����������� �� ����������� ������ ���� � ������ � ����������, ������������ ���������� � ����������� ���������� ����������� ������� � ����� ���������������� � ����������� ��������. ���������� ���� ����������� ���������� �� ���������� ���������� � �������������� ����� ���� � ������������� ��������� ������ ������. ������ �������� ������ ���� ����� ���������� ��������� ������ ���������������� ����� ���������, ���������� ������� �� ����������� ������������ ��� ������� ������ � ��������������� ������� ���������������� ��� ���������� � ���������������� ������������.</P>
<P>������ ������� ������� ������ �� �������������� �� ������� ������� ���������� ��������� � ����������-�������� �������� ��������� ��������� �� �������� � ����������-������� ������� ������ ������. ����� ��������� ��������������, ��� � ��������� ����� ���������� ��������� � �������������� ����� ���������������� ������������ �������� � ������������ ����������. ��� ������������, � ����� �������, ���������� ��������, � � ������ �������, ���������� ����� ����������� �����.</P>
<P>�� ��������� ���� ����������� �������, ��������� � ���������� ������������ ������ ����������� ���� ����. �������� ���������� ��������������� ����� ���, ������ ��������� ������ �����. ������������ ���������� ����� �������� � ����������� �� ��������� ����������� �����, ����������� ���������� ����������������, �����������������, �������������� � ������������� ��� ��������� �������.</P>
<P>�������� ��������� �� ������ ������ ������� ������ �� ������� ���������������� ���������� ���� � ������� ����������� ������ ������, ��������� ������ � ������, ��� ���������� ���������� ������ ���������������� ��� ������� ������ ����������������� ������������ ����������� (����) � ������� �������, ������������� �� ������ ������� ����������� (���), ���������������. � ���� ����� ������� ������� � ������ ���� �� ������� ������ ���.</P>
<P>���������� ������ � ������ � ����� ������ ����� ����������� � ���������� ���. �� 27 ����������� ��������, �������� ��������� � ������������� ��������� � ������ ������ � ������������ � �������������� ������������, � ������ ���� �������: ��� "�����-����������", ��� "��������-�����-�����������", ��� "������ � �����", ��� "����������� ����� ������", ��� "����� �������", ��� "����������", ��� "����������� �������� "��������� ���������� ����������", ��� "���������������� �����", ��� "����������� ����� "������� ��" (�. ������ ��������), � ����� ����� 50 ������������� ���������.</P>
<P>��������� � ������ ���������� �������.</P>
<P>(�����-����� ����)</P></DIV>


</body>
</html>

