<!--
ID:37333710
TITLE:���������� � ����������� � �������������� ��������� ����������� �� 01.01.2007 �.*
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:4
ISSUE_DATE:2007-02-28
RECORD_DATE:2007-02-26


-->
<html>
<head>
<title>
37333710-���������� � ����������� � �������������� ��������� ����������� �� 01.01.2007 �.*
</title>
</head>
<body >


<TABLE cellSpacing=2 cellPadding=9 width=614 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top colSpan=5><B>
<P align=center><FONT>����������� ��������� �����������</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=center><A name=DDE_LINK1><FONT></FONT></A></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center><FONT>�� 01.01.2006</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>�� 01.01.2007</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><B>
<P align=justify></A><FONT>1. ���������������� ��*<SUP>*</SUP> ������ ������ ���� �� ��������� ��� ������� �������������� �������������� �������, �����<SUP>1</SUP></FONT></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right><FONT>1 409</FONT></B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=right><FONT>1 345</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify><FONT>� ��� �����:</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="15%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify><FONT>������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>1 356</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right><FONT>1 293</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify><FONT>������������ ��</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>53</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right><FONT>52</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<DIR>
<P align=justify><FONT>1.1. ���������������� �� �� 100%-��� ����������� �������� � ��������</FONT></P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>42</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right><FONT>52</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<DIR>
<P align=justify><FONT>1.2. ��, ������������������ ������ ������, �� ��� �� ���������� �������� ������� � �� ���������� �������� (� ������ �������������� �������������� �����)</FONT></P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT></FONT></P>
<P align=right><FONT></FONT></P>
<DIR>
<P align=right><FONT>2</FONT></P></DIR></TD>
<TD vAlign=top width="15%">
<P align=right><FONT></FONT></P>
<P align=right><FONT></FONT></P>
<DIR>
<P align=right><FONT>1</FONT></P></DIR></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify><FONT>� ��� �����:</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="15%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify><FONT>�����</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<DIR>
<DIR>
<P align=right><FONT>2</FONT></P></DIR></DIR></TD>
<TD vAlign=top width="15%">
<DIR>
<DIR>
<P align=right><FONT>1</FONT></P></DIR></DIR></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify><FONT>������������ ��</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<DIR>
<DIR>
<P align=right><FONT>0</FONT></P></DIR></DIR></TD>
<TD vAlign=top width="15%">
<DIR>
<DIR>
<P align=right><FONT>0</FONT></P></DIR></DIR></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<DIR>
<DIR><B>
<P align=justify><FONT>2. ������������ ��, ������������������ �� 01.07.2002 �������</FONT></P>
<P align=justify><FONT>��������</FONT></P></DIR></DIR></B></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right><FONT>0</FONT></B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=right><FONT>0</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top colSpan=5><B>
<P align=center><FONT>����������� ��������� �����������</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><B>
<P align=justify><FONT>3. ��, ������� ����� �� ������������� ���������� ��������, �����<SUP>2</SUP></FONT></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right><FONT>1 253</FONT></B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=right><FONT>1 189</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify><FONT>� ��� �����:</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="15%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify><FONT>�����</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>1 205</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right><FONT>1 143</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify><FONT>������������ ��</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>48</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right><FONT>46</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="15%" colSpan=2><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="15%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top colSpan=5><I>
<P align=right><FONT>�����������</FONT></I></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center><FONT>�� 01.01.2006</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center><FONT>�� 01.01.2007</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<P align=justify><FONT>3.1. ��, ������� �������� (����������), ��������������� ����� ��:</FONT></P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="15%" colSpan=2><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>����������� ������� ���������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>1 045</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>921</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>������������� �������� � ����������� ������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>827</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>803</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>����������� ��������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>301</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>287</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>���������� �������� � �������������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="15%" colSpan=2><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>����������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>4</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>4</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>��������<SUP>3</SUP></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>180</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>188</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<P align=justify><FONT>3.2. �� � ����������� �������� � �������� ��������, �����</FONT></P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>136</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>153</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>�� ���:</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="15%" colSpan=2><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>�� 100%-���</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>41</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>52</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>����� 50%</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>11</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>13</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<P align=justify><FONT>3.3. ��, ���������� � ������ ������ &#8211; ���������� ������� ������������� ����������� �������, �����</FONT></P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>930</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>924</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify><FONT>4. ������������������ �������� ������� ����������� �� (��� ���.)</FONT></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right><FONT>444 377</FONT></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right><FONT>566 513</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify><FONT>5. ������� ����������� �� �� ���������� ��, �����</FONT></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right><FONT>3 295</FONT></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right><FONT>3 281</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>�� ���:</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="15%" colSpan=2><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>��������� ������<SUP>4</SUP></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>1 009</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>859</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>������ �� 100%-��� ����������� �������� � �������� ��������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>29</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>90</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify><FONT>6. ������� ����������� �� �� �������, �����<SUP>5</SUP></FONT></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right><FONT>3</FONT></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right><FONT>2</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify><FONT>7. ������� ������-������������ �� ���������� ����������<BR>���������</FONT></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right><FONT>0</FONT></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right><FONT>0</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify><FONT>8. ����������������� ����������� ���������� ��, �����<SUP>6</SUP></FONT></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right><FONT>467</FONT></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right><FONT>699</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>� ��� �����:</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="15%" colSpan=2><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>�� ���������� ���������� ���������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>422</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>657</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>� ������� ���������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>31</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>29</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>� ������� ���������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>14</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>13</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify><FONT>9. �������������� ����� ��, �����</FONT></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right><FONT>0</FONT></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right><FONT>15 007</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify></B><FONT>� ��� ����� ��������� ������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>0</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>7 282</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify><FONT>10. ������������ ����� ��� ��������� ���� ��, �����</FONT></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right><FONT>0</FONT></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right><FONT>15 885</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify></B><FONT>� ��� ����� ��������� ������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>0</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>11 983</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify><FONT>11. ��������-�������� �����, �����</FONT></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right><FONT>0</FONT></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right><FONT>996</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify></B><FONT>� ��� ����� ��������� ������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>0</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><FONT>0</FONT></P></TD></TR></TBODY></TABLE><I>
<P align=right><FONT>�����������</FONT></P></I>
<TABLE cellSpacing=2 cellPadding=9 width=614 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top colSpan=3><B>
<P align=center><FONT>����� �������� � ���������� ����������� ���</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>�� 01.01.2006</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>�� 01.01.2007</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR><B>
<P align=justify><FONT>12. ��, � ������� �������� (������������) �������� �� ������������� ���������� �������� � ������� �� ��������� �� ����� ��������������� ����������� ��������� �����������<SUP>7</P></DIR></B></SUP></FONT></TD>
<TD vAlign=top width="15%"><B>
<P align=right><FONT></FONT></P>
<P align=right><FONT>154</FONT></B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=right><FONT></FONT></P>
<P align=right><FONT>155</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR><B>
<P align=justify><FONT>13. ������� ������ � ����� ��������������� ����������� ��������� ����������� � ���������� �� ��� ������������ ����, �����<SUP>8</P></DIR></B></SUP></FONT></TD>
<TD vAlign=top width="15%"><B>
<P align=right><FONT></FONT></P>
<P align=right><FONT>1 687</FONT></B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=right><FONT></FONT></P>
<P align=right><FONT>1 758</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>� ��� �����:</FONT></P></TD>
<TD vAlign=top width="15%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="15%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<DIR>
<P align=justify><FONT>� ����� � ������� (��������������) ��������</FONT></P></DIR></DIR></TD>
<TD vAlign=top width="15%">
<P align=right><FONT>1305</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right><FONT>1366</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>� ����� � ��������������</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right><FONT>381</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right><FONT>391</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>�� ���: � ����� �������</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right><FONT>0</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right><FONT>2</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>� ����� ������
�������</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right><FONT>381</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right><FONT>389</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>� ��� �����: </FONT></P></TD>
<TD vAlign=top width="15%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="15%"><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>����� �������������� � ������� ������ ������</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right><FONT>337</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right><FONT>341</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><FONT>������������ � ������ ������ (��� ��������-<BR>��� �������)</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right><FONT></FONT></P>
<P align=right><FONT>44</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right><FONT></FONT></P>
<P align=right><FONT>48</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<DIR>
<P align=justify><FONT>� ����� � ���������� ���������������� � ����� ������ ��������� ��������</FONT></P></DIR></DIR></TD>
<TD vAlign=top width="15%">
<P align=right><FONT></FONT></P>
<P align=right><FONT>1</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right><FONT></FONT></P>
<P align=right><FONT>1</FONT></P></TD></TR></TBODY></TABLE><SUP>
<P align=justify><FONT>*</FONT></SUP><FONT> ���������� ������������ � ��� ����� � �� ��������� ��������, ����������� �� ��������������� ��������������� ������ �� �������� ����.</FONT></P><SUP>
<P align=justify><FONT>*</FONT></SUP><FONT>* �� &#8211; ��������� �����������. ������� "��������� �����������" � ��������� ���������� �������� ���� �� ��������� �������:</FONT></P>
<DIR>
<DIR>
<P align=justify><FONT>����������� ����, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� ������� � ������� ����� �� ������������� ���������� ��������;</FONT></P>
<P align=justify><FONT>����������� ����, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� �������, �������, �� ���������� ����� �� ������������� ���������� ��������;</FONT></P>
<P align=justify><FONT>����������� ����, ������������������ ������� �������� (�� ���������� � ���� ������������ ������ "� ������ � ���������� ������������") � ������� �������� ����� ������ �� ������������� ���������� ��������.</FONT></P></DIR></DIR><SUP>
<P align=justify><FONT>1</FONT></SUP><FONT>����������� ��, ������� ������ ������������ ���� �� �������� ����, � ��� ����� ��, ���������� ����� �� ������������� ���������� ��������, �� ��� �� ��������������� ��� ����������� ����.</FONT></P><SUP>
<P><FONT>2</FONT></SUP><FONT>����������� ��, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� ������� � ������� ����� �� ������������� ���������� ��������, � ����� ������������ ��, ������������������ ������� �������� � ���������� �������� ����� ������ �� ������������� ���������� ��������.</FONT></P>
<P align=justify><FONT><SUP>3</SUP> �������� � ������� 1996 �. � ������������ � ������� ����� ������ �� 03.12.1996 &#8470; 367.</FONT></P>
<P><FONT><SUP>4</SUP> ����������� ������� ��������� ������, ��������� � ����� ��������������� ����������� ��������� ����������� � ���������� ���������� ������. �� 01.01.1998 �. � ����������� ���������� � ��������� ������������ �� ������ ������ ����������� ����� ���������� ���������� ��������� ������ &#8211; 34 426.</FONT></P>
<P><FONT><SUP>5</SUP> ����������� �������, �������� ����������� �� �� �������.</FONT></P>
<P><FONT><SUP>6</SUP> � ����� ���������������� ���������� �� �� ������� �������� �����������������, �� ������� ��������� � ���� ������ ����������� �� �������� �� �� �������.</FONT></P>
<P align=justify><FONT><SUP>7</SUP> ����� ���������� ��, � ������� ������ ������ ���� �������� (������������) �������� �� ������������� ���������� �������� (������� ��, �� ������� � ����� ��������������� ����������� ��������� ����������� ������� ������ �� �� ����������) &#8211; 1532.</FONT></P><SUP>
<P align=justify><FONT>8</FONT></SUP><FONT> ����� 01.07.2002 � ����� ��������������� ����������� ��������� ����������� ������ � ���������� ��������� ����������� ��� ������������ ���� �������� ������ ����� ��������������� ����������� ��������� ����������� � ����� � �� ����������� �������������� �������������� �������.</FONT></P>


</body>
</html>

