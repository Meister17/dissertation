<!--
ID:36996095
TITLE:������-���������
SOURCE_ID:11517
SOURCE_NAME:���
AUTHOR:����� ������
NUMBER:12
ISSUE_DATE:2006-12-31
RECORD_DATE:2006-11-29


-->
<html>
<head>
<title>
36996095-������-���������
</title>
</head>
<body >


<P align=justify>����������� �������� �������� "������ ������" ���� ������� �������, ��� ����������� � ���������� ���� � ����</P>
<P align=justify>�������� "������ ������" ����� �� ������ ������ �������� �� ���������� ����� ��������� ��������� ������ ����. �� ������� �������� 11 ��� ����� � ��������� ����������� ���� "������" �� �������� ����� � ������, �, ����� ���� ���������� ���������������, ��� �������� ��������� � ���������� ������. ������ �������� ��������� ������ � ������� � ����� ������������� �������. ����������� �������� "������ ������" ���� ������� ����������, ��� ��� �� ������������� ������ �� ������� ���������� �������� ������� � �������.</P>
<P align=justify>���� �������������, ����������, ��� �� ��������� � �������� "������ ������".</P>
<P align=justify>��� ������ � "������ �������" �������� ����� ���������. � ���� ����� �� ���������� �������������, � ��� ���� �����, ����� ���� ������� ������� � ���������� ��������, �� ��������� ���������� ���� �������� �������� � ����������� ������ �� ����� �������. ������ ����������� �� ������ ������� �������������� �������� ���� ��� ����������� ������� ���� ������ �������, � � ���������� � ������ ���������� ����� ��������������� ��������� ��������, ������� �������� ����������� �����. ����� ���������� �� ������������ ���� ��������� �� ���� ������������ ���������.</P>
<P align=justify>��� ������������ ����� ���������� ����� ��������� �������?</P>
<P align=justify>������� ��������� ����� ����������� ���������� ����� � 1,5 ���� ��������, ������, �� ������ ���������� ������� �������� ������ ������� ����������, �� ���������� ����� 3,5 ���� ��������. �, �������, �������� ������ �� ������ ������. ����� ������ � ������� �� 20-30% � ���, � ����� ����� �� ���������. � ����� luxury, �� ���������� ��������� ������������, �� 70% ��������� ��������� ���������� 30% �������������. � ����� �� ��� ��������� ��������: 30% ������� � 70% ���������� ���������.</P>
<P align=justify>����� �� ����������, ������ ���� ����������� ��������� �� �����?</P>
<P align=justify>�� �������, ���� ����������� �� ��������� ����� ���������� 30% �� ��� ������. ����������, ��� ����� �����, � ��� �������� ������ ����������, ���� ������������� ����� ������� �������. �������������� "�����������" �� ������ ���, ��������������� ������, ����� ��� �� ��������� ��������� ������� � ��������� ������. �, ��� �����, ����������� ������������ ��������� ��� �����������, ��� ��� ������ ����������� ���������� �� ������������ �������, � ����� ���� ��������.</P>
<P align=justify>�������� �������� � ������� ������� ��� ������������ ��������� � ������ ��������� �� Carrera � Carrera. ������ �� ����������� ��������� ���� �������, � ��� �� ������� �������������������� -����, �������?</P>
<P align=justify>� ������������� ��������������� �� ����� ��������, � ��� ���� ����� �����, ��� Jacob � TechnoMarin. ����� �� ����������� ������������ ������� �������� IceLink. ��� �������� ����, �� ���� ����� ������������� �������� ����� �������������. �� ��� ���� �� ������, ��� ������ ��������� ���� ������ ������� ���������� ���� ������� ���.</P>
<P align=justify>� �������� � ����������� ��������� �� ����������?</P>
<P align=justify>�� ����� ������������� ���� ������ ������� ������ ������� �� �������� ��������������, ��������� ����� �� ��� ������ �� ���� �� �����������, �� ������� � �������� ���������. ������ � ��� �������� ����������� ���������� ���������, � "������ ������" ������ � ���� ���� �����. � ��� ���� ����������, � ������� ����������� ������������� ����� ���������� � ���������������� � ��������. � ������ �������� - ���������� ��������� ��� ��������� ��������. �� �������, ��� ��� ����� �������������� ��������� �������� ���������� ��������� ������� � ����.</P>
<P align=justify>������� �������� ������� ������� � ����� ������������� ��������. ������ ���� ������� ����� �������?</P>
<P align=justify>����� ������� �����������. � ������ ����������������� �������� ������������ � �������� ������� ��������� ������� � ���� �� ��������� ���������. ����� �����������, �� ������ ������, ������ ����� ���. ����� "������ ������" �� ������ ������ ��� ������������ ��� ����������� �������� ������ � ��������. ��� ��������� ������ ������������, ��� ������� ��� � ��������� � ������ � �����, ��� � � ����������� �� ������� �����, ��� ������� ������ � ������ ��������� ��������� ��� ������� ����� � ��������� ����� ������� �����������. �� ���������� ���������� ����� ������������� ������� ��� ����, ����� ������������� ����� ��� ����� �������� ���������� �����������, ���� ����������� � ���������� ���� � ����. ���� ���-�� ������ �� ����� ��������� ���� ������� ���������, �� ���� ��� ���������, � ����� ���� ������� ����� ������ ���������� ����� ������������� ���������, �� ��� ����� �����, ��� �� ����������.</P>
<P align=justify>�� ������ ������� ������ ��������� ����������?</P>
<P align=justify>��� � ��, ��� ����� ��������� � ��� ���� � ���������, � ��, ��� ������������� ����� ������� ��� ��������. �� ��������� ��������� ��� �������� ���������� ��������� ���������. ��-������, �� ���������: ��������� ��������� ������� � ��������������� �����, ������� �������� ���� ������� � ������� ����������� � ����������� ����������� ��������� ���������. � ��-������, �� �������� ���� ��������� �����������: ��������� ������� ��� ������ ������ is ����, ������� ���� ����������� �� ������� ������� ������ ������ ��������� �� �����, ����� � �� ������� �������.</P>
<P align=justify>������������ ����������� ����������� ���-�� ���������� �� ������ ���������� ��� ���������?</P>
<P align=justify>���������� ���������� ������������ ����������. � �������� ������ ���� ������ � ������ ��������� ����� �������� � �������� � ������ ��� � ������� ���������: ����� ������, ���������������� � ��������. � � ���� ��������� �������� �� ����� ������������� � ������������, ��� ��������� ��� ����������.</P>
<P align=justify>��� ��� ��� �������� ������ � ��������, ��������� ���������� ���������?</P>
<P align=justify>��� ���� ��� ������ ����� ������, �� ������ ��������, ������� ������ ������ �������� � �������� ������������.</P>
<P align=justify>� �� ���������� ��, ��� ��������?</P>
<P align=justify>���������, �������������� � ��������� "������ ������", ������������ �� ������ ����� ��������, �� � ����. �, ������� ��, � �� ���� ������� ����� ��������� ��������� � ���������� ��� ������������� ��������� �������.</P>
<P align=justify>������� ������ �������� ����������� �����, ��������� ���������?</P>
<P align=justify>�� �������, ��� �������. � ����� �������� �������, ��� ���������, �������� ���� �������.</P>
<P align=justify>�� ����� ��������� �� ������. ��� ���-�� ����������� � ������, ������ �����?</P>
<P align=justify>�������������� ������� ����� ��� ������������� ������. ����, ��� ��������, ������ ������ � ������ �������� ����������.</P>


</body>
</html>

