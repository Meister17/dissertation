<!--
ID:36129483
TITLE:���� �� ����� �������
SOURCE_ID:10013
SOURCE_NAME:������������� ���������� ������
AUTHOR:
NUMBER:12
ISSUE_DATE:2005-12-31
RECORD_DATE:2006-04-21


-->
<html>
<head>
<title>
36129483-���� �� ����� �������
</title>
</head>
<body >


<P align=justify><B><FONT>���� ��������������, ���������� </FONT></B><B><FONT>� 1993 ����, �������� ����� ������ </FONT></B><B><FONT>������ ��������. ��������� ��������� </FONT></B><B><FONT>� ������� ����������, ����������� </FONT></B><B><FONT>��������� ���������� ������ </FONT></B><B><FONT>�����������.</FONT></B></P>
<P align=justify><B><FONT>��� �������� ����� ���������� </FONT></B><B><FONT>��������� �� ������������ �����? ��� </FONT></B><B><FONT>��� ���������� �� ��� �������? �� ���� </FONT></B><B><FONT>&#8212; � �������� ����� �����������, </FONT></B><B><FONT>������������ ��������� ������������� </FONT></B><B><FONT>����� "��������������".</FONT></B></P>
<P align=justify><FONT size=+0><STRONG><EM>���������� ����� ����������, ������������ ��������� �� &#171;��������������&#187;.</EM></STRONG></FONT></P>
<P align=justify><FONT size=+0><STRONG><EM>����� ���������� ����� ��� ������ �����������: ����������� � ����������.</EM></STRONG></FONT></P>
<P align=justify><FONT size=+0><STRONG><EM>Ÿ ���������� ������� �������� � ���������, �� �������� ��������� ������. ����� ����� ���������� ��������� ��� ����������� ������������ ������������ ��������� � ��������-�����. � 1998-� �� ���������� ������� � ���������� ������ �������-����� � �������, � ����� ��� ���� - ������ ���-����� � �����. ������ ������, ����� ���������� ������ ��� �� ������ ����� ����� 37 �������� ���-�����.</EM></STRONG></FONT></P><B><FONT></FONT></B>
<P align=justify><B><FONT>����� �������� �������</FONT></B></P>
<P align=justify><FONT>&#8212; <I>�������� �� ���������, ������������ � �������������� ������� �����, �� ����� ��� �������?</I></FONT></P>
<P align=justify><FONT><B>����� ����������: </B>� ����� � �����, ���. ����-���������� ��-�������� ������������ �� ����� ������� ������ ��������� � ������������� ������ �������� ����� ������������� � ������� ��������. ������ ��� ������� � ������� �� �� ����������. ����� ����, �� ��������� ������ ���� ����� �� ������ �������������� ������������� ������� ������ �� ���� ���������� ����� ������������ � ��������.</FONT></P>
<P align=justify><FONT>������� ����� ���������� ������ �������� ���� ��� ���������������������� ������ �������, ����������������� ������� ������������ ���������� ���������� ������������ ��������� �����, ��������� �� �������� � ��������� ��������� �� �����������. ����� ����� �������� ������� ������ �����, � ����������� �� �������� �� �����. ������ ���������� �������� �����, �� ������� ��������� ����, �������� ���������� ����� ��������� � ������� �� ����������� ����� ��������������� � ������ ����������� �����.</FONT></P>
<P align=justify><FONT>� ����� ����������� ������� ����������� ��������������� ���������. ����� ������� ���-���������� ��������� ����, �������, �������� �� ���������� ������� �������, ������� ���� ������ �� ����������� ���������� ����������. � ������������ ������-���������, ������� ������������ ����� �����������, �������� ����� ��������� �������� ����� � ������� ����������� �����.</FONT></P>
<P align=justify><FONT>&#8212; <I>�� ������� � ����������� ���������� ����������� ����� � ��������. �� ����������� ��������� ��� ������������ ������� �����, ��� ��������� �����?</I></FONT></P>
<P align=justify><FONT><B>����� ����������: </B>���� ��������� �� ������ ������������ ������, � ��� ���� 11 ��������. �������������� ����� ������� ������������������ ������, ��������� �� ����� ����������� ����� ������������� ��������, ��� ����������� �������������� �������. ������������ � ���� �� ������� ��������� ��������� ������� ���������� �����, � ���������� �������� ������� �������� ����� �� </FONT><FONT>������ ������������� ������ �� ������������ ������� ���.</FONT></P>
<P align=justify><FONT>�� ����� �������� ����������� ����������� ���������� ���������� ����. ��� ���� �� ��-�������� ������� ������, ���������� � ������������� ����������, ��������� ����������� ������������� � �� �������� ����, ������� ���� �������� � ��������� �����.</FONT></P>
<P align=justify><B><FONT>�� �������� �����������</FONT></B></P>
<P align=justify><FONT>&#8212; <I>�������� �� �� ����������� � ������� �������?</I></FONT></P>
<P align=justify><FONT><B>����� ����������: </B>����������, �������, ��������� ��� ����� - ���������� ���� ����� � ��� ��� ���� �������. ���������� �� ����, ����� ������-������� �����, ������������� �� ���� ������������������, �� ������ ������������ ���������� �������� ������������. ���� ������������ �� ����� � ���, ��� �� ������ ����� ��������� ���������� ������������ ����������� ��������������� ���������, � ��� ��� ��������� �������� � ���� �� �������������� ��� ������.</FONT></P>
<P align=justify><FONT>�������������� ������� ��������� � �������� ��������������, �������� ��������� � �������������� ���������� ������� ����������� ����������, � ��� ���������� ����� �������. ����</FONT><FONT>���������� ������� ���������� ����� � ������ ����, ��� ������������� ���������� ������������ ��������������� ��������� ����������� ����������, ������� ���� ����� ���������� ����������� �������������� ������� � � ������������ ��������.</FONT></P>
<P align=justify><FONT>&#8212; <I>��� �� ���������� � ������� ����������� � ���������� �������?</I></FONT></P>
<P align=justify><FONT><B>����� ����������: </B>�� ������������� ����� � ��������������� �������� ��� ��������� � �������� �����������. �����������, ������ ��������� ������������� ������������, ��������� �������� ������������ ������, � ���, �����������, ������ �� �������� �� ���������� �����. ����������� ���������� � ������� ���������� ������� ������������ ���� ���������, ������� ��� �������� �����, ��������������� ���� ������, ������� </FONT><FONT>������������ ���������� ��������������� ��������. ����������, ����� ���� ����� �������� � ������ ��������� ��������, ��������� � ����������� ��� ���� ���������� ����������� �����.</FONT></P>
<P align=justify><B><FONT>����������� �������</FONT></B></P>
<P align=justify><FONT>&#8212; <I>�� ��� ����� ���� ������� � ������� ����������� �������. ������� �� ��� ����� ������� ������ �������? ������������� �� �� ���� ������� ��������� � ���������� ������� � �����, � � ������ �����, � ��������� ?</I></FONT></P>
<P align=justify><FONT><B>����� ����������: </B>������� � ������� ����������� ������� ���������� ��� - �������� ������� ��� ������, ������� �������� � ��������� �������� ����� ���������� �����. �� ��������� ��� ���� ���� �������� ���������� ����� ��� � ������� ��� �������� ���������� �������. ���� ���������� ���� �� ������ ��������� �� �������, ������� ���������� � ��� ���� ����� ����� ��� ����� ������� ��������, � ������ ������� � ��������.</FONT></P>
<P align=justify><FONT>��������� ������� ����������� ����� ���������� ����� �����, � ����������� �������� ������� �� ����������� ����� � ������� ����������� ������� ���������� ��� ����������� � �����������. ��� ����� ��� ������: ������ ������� ��� � ����� ��������� ������.</FONT></P>
<P align=justify><FONT>��� ��������������� ����� ����� ������ ������������ ������, ��������� � ����������� ������ ����� ��������� ������������ ������� ��� �������, ��� � �������� ����. ������ ��� ����������� ������ � �������� ���������� ����� ��������� �� ������������ �������� ����� ������������ ����������� �������������� ������. � ������� �� ������������ ��������������� ����, �������������� ������������ ��� ����������� �������������� ��� ������������ �������� �����, ��� � ��� ��������. ��� �������� ����� ����������� �������������, ��������� ����� ��������� ��������� ��������� ����������� ��������� �������� ���������� �������. � ��������� ����� ������������ ���������� �������, ����� ������������������� ����� ������������� � ������, ����� ����������� �� ��������, � �������������� ����� � �����.</FONT></P>
<P align=justify><FONT><STRONG><EM>����� �������</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>�������������� - ������������� ������������ ���� � ���������� �����, ����������� �� ���������� ������ �� ����� - ���������� �� ������������.</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>�������������� - ����������� � ��������� ������������� ���������� ����, ��������� �������� �������� �����������, ������������� �� ���� ���������� ����� ������.</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>�������������� ��������� ������ ���� ����� �� ������ ������������� ������� ������ ������ �� ���� ����������� ���������� ����� ������������ � ��������, ��������� ������������ ����������� � �������� ������������ �������� ���������.</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>�������� �������� ������������</EM></STRONG></FONT></P>
<P align=justify><U><FONT><STRONG><EM>���� � �������</EM></STRONG></FONT></U></P>
<P align=justify><FONT><STRONG><EM>������� �������� � ����� �������� ���������� �������� ��� �������.</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>������� ����� ������� ������ ������� ������ ������ ���������� ��������� ��������.</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM><U>���� � ������� ��������</U></EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>���� ������������ ������� ������� ��������������� �� ������ ���������� �� �������. �������� � ����� ������������ �������� �������������� - ������� ������� ������ ����� � ����������.</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM><U>���� � ������������ ���������</U></EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>���� ������������ � ������������ ���������� ��������������. �� �������� ��������� ������������ ������������ �������� � ��������� �� ������� ��������� ���������� �����.</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM><U>���� � ����������</U></EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>���� ������� � ������� ����������� � ������� �������� ����������� ������������� �� �������� ���������� ����, �������� ����������� ������ - ������� � ��������� ����������� ���������� ����������.</EM></STRONG></FONT></P>
<P align=justify><STRONG><EM><FONT>������������ </FONT><FONT>��������</FONT></EM></STRONG></P>
<P align=justify><FONT><STRONG><EM>��������� ��������� �����������, ����������� ������������ ��������� �� &#171;��������������&#187;. ����� ��� ������ �����������: �������������� � �������������. � ���������� ������� �������� ����� 10 ���. �� ��� ����� ������ ���� �� ���������� ������ ����������� ���� �� ����������� ������������ ���������. � 1999 ���� � ������� 5 ��� ������� ����-�����������, ����������� ��������� �������� ����������. � ��������������� ��������� ���������� ����� �����.</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>��������� ���������: � ��������� ����� �������������� ������� �������� ������������� ������� ����� �������� ���������� ����� ���� �� ����� ��� ��������, ��� �� ��� ������������. ���� ������ ������ - ��������������� ���������� ���� � ���������� ������������ ������� � ����� ��� ������ ����� ��������.</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>&#8212;&nbsp; ��� �� ���������� ������� ����������������� ������� ��� ������� ����� ?</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>��������� ���������: � ��� ���� ����������� �������������� �������� ������ ����������������� ������� � ������ ���������� �������� �������������� � ���������� �������. �������� � ���������� ��������� ������, ������������ � �������� ���������� ����� �������� ������� �����, �� �������� ��� ��� ����������� ���������. � ������ ��������� ���������� ����������� ���������� ����� ���� ����������� � ���� �������.</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>&#8212;&nbsp;�� ���������� ������� ���������� ������ ����, ��� ������ ������?</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>��������� ���������: �� ��������� ���� ����, ��� ���� ������ ����� ����� ������������. ����, ��� � ��� ������ ���������.</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>����� � ��������</EM></STRONG></FONT></P>
<P align=justify><STRONG><EM><FONT>������ ���� ����������, ����������� ������������ ��������� </FONT><FONT>�� &#171;��������������&#187;.</FONT></EM></STRONG></P>
<P align=justify><STRONG><EM><FONT>������ ���� ���������� �������� ���������� ����������� ����������� ����� </FONT><FONT>� �����������.</FONT></EM></STRONG></P>
<P align=justify><STRONG><EM><FONT>� ���������� ������� � 1993 ����, ������ ���� �� �������� ����������� �� </FONT><FONT>����������� ������������ ���������. ������� � ������ &#171;���������� ������&#187;, </FONT><FONT>&#171;��������&#187;, &#171;�����������&#187;. � ����� �������������� �������� ������� �������� </FONT><FONT>���������� �������.</FONT></EM></STRONG></P>
<P align=justify><FONT><STRONG><EM>&#8212; ������������� �� �� ���� ������� ��������� � ������ ����� � ����� � ����������� � ���?</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>���� ������: ��� ���� �������� � ���������� ���������� �����, � ����� ������� ���������� ������� ���������. ������� ���������� � ���, �������, �������� ������� � �����, ���� � �� ���������� ���������� �������� �������, �� ��� � �� ����� ����. ���� ���������� ����� � ������� ����������� ������� ��� ��� ���������� ������������ ����� �� �����.</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>&#8212; ����� �������� �� ����������� ���������� �����������?</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>���� ������: �� ���������� ������������ ���������� ��� �� ������ ������������: ������� � ��������� ��������������� ������������, ����������������, ��������� �������. � ��������� ����� � ����� ���������� ������� �������� �������� �� ������� ��������������. ����� �� �������� �������� �������� �� ����������� ������� � ������� ���������� ����.</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>� �������� ��������� ������ ����� ������������ � ��������. �� �������� ���� �������� � � ������, � � ��������. �� ��������� ���������� ������������ ���� ��������� �������� � ���������� �� ����������� � �������� ������������ ������. � ���� ������� ���� ������������ ������������.</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>&#8212; ����� ������� ��� ������� ������ ?</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>���� ������: ���� ������� ������� ������� �� 3 ������.</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>��-������, ��� ������������ ������� �����. ������ ��������� ������ �� ���� ������, ������ � ����������� �����������. ����� �������� �������� ��� ����� ���� ��������� �� ������ ����������, � ������ ����������, ����� � ����� �������.</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>������ ����� ����� ��������� ��������� -��� ���������� ����������� - ������������� �������� �����.</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>������ ������ �������� �������� � ��� �� ��������, ������������ ��������, ��������, ����������� ����� �� ������������ ������, �������� ������, ��� ��������� ������ �� ������ � �� �����.</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>��������� ��������</EM></STRONG></FONT></P>
<P align=justify><STRONG><EM><FONT>��������� ����� ����������, �.�.�., ����������� ������������ ��������� </FONT><FONT>�� &#171;��������������&#187;.</FONT></EM></STRONG></P>
<P align=justify><STRONG><EM><FONT>���������������� ������������ ������ � 1996 ���� ����� ��������� </FONT><FONT>�������������� �������� ���������� � ����� &#171;�������&#187; � ��������� ���������� </FONT><FONT>����������. �������� � ������������� �������������� �����, ��������������� </FONT><FONT>� ��������� ����������� ������������ ���������. � ����� �������������� �������� </FONT><FONT>���� ������ ��������, ���������� ������������ ����������� ���.</FONT></EM></STRONG></P>
<P align=justify><FONT><STRONG><EM>&#8212; �������� ���� �������� ���������� �� ��������, ��������� � �������������� ���������� ?</EM></STRONG></FONT></P>
<P align=justify><STRONG><EM><FONT>����� ���������: ���� ��������� ��������� ������� ���������, �� �������� �������������� ������� ���������� ����� 20 - 25% ���������� ��������. ���� ����� ��������� ���������� �����, � � �� ����� ������ ��� ������������� ������ � �������� �������, ��� � ������� ������������ �����������. ����������� �������� � ����� ���������� �� ��������: ��������� ��������� ��������� ������� �� ����� �� 300 ����� ������, � �������, ���������� ������ ������� ���������, ��������� � 400 ��������� ������
. � 2006 ���� ����� ����������� �������� ��������� �� ��������� ����� ��������� ��������� ��� ������������� ��������. ���� ����� ��������� �������� ����������� �� ������ �������, ������� ���������� </FONT><FONT>�������������, ������ �������� � ������� ���������, � ����� � ����������� �������� ������� �� ������ ��������.</FONT></EM></STRONG></P>
<P align=justify><FONT><STRONG><EM>&#8212; ��������� �������� ����������� ��� ����������� � �������� ����������� ��� ������������� ��������� ?</EM></STRONG></FONT></P>
<P align=justify><FONT><STRONG><EM>����� ���������: �������� ���� ���������� �������� �� ����� �������� �������� ���������� �� ������, � �� ���������� �������� - �� �������. � 2006 ���� ���� ��������� ����������� ��������� ���� ���������� ��������, ������� ����������� ���������� ����� �����. ������� ��� ����� �������� ��������� �������� �������� ������ &#171;�������� ��������&#187; � �������� ����� � �������������� � ���������� � ������ ������������ ������������.</EM></STRONG></FONT></P>
<P align=right>������ �������</P>


</body>
</html>

