<!--
ID:37510463
TITLE:���������� � ����������� � �������������� ��������� ����������� �� 01.03.2007 �.*
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:7
ISSUE_DATE:2007-04-15
RECORD_DATE:2007-04-09


-->
<html>
<head>
<title>
37510463-���������� � ����������� � �������������� ��������� ����������� �� 01.03.2007 �.*
</title>
</head>
<body >


<TABLE cellSpacing=2 cellPadding=9 width=100% align=center border=1>
<TBODY>
<TR>
<TD vAlign=top colSpan=5><B><FONT face="Times New Roman CYR">
<P align=center>����������� ��������� �����������</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR">
<P align=center><A name=DDE_LINK1></FONT></A></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=center>�� 01.01.2007</FONT></P></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR">
<P align=center>�� 01.03.</FONT>2007</P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><B><FONT face="Times New Roman CYR">
<P align=justify></A>1. ���������������� ��</FONT><FONT face="Times New Roman CYR">*</FONT><SUP><FONT face="Times New Roman CYR">*</SUP></FONT><FONT face="Times New Roman CYR"> ������ ������ ���� �� ��������� ��� ������� �������������� �������������� �������, �����<SUP>1</SUP></B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR">
<P align=right>1 345</B></FONT></P></TD>
<TD vAlign=top width="15%"><B><FONT face="Times New Roman CYR">
<P align=right>1343</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR">
<P align=justify>� ��� �����:</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR">
<P align=justify></FONT><FONT face=Wingdings>l</FONT><FONT face="Times New Roman CYR"> ������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>1 293</FONT></P></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR">
<P align=right>1 291</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR">
<P align=justify></FONT><FONT face=Wingdings>l</FONT><FONT face="Times New Roman CYR"> ������������ ��</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>52</FONT></P></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR">
<P align=right>52</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<DIR><FONT face="Times New Roman CYR">
<P align=justify>1.1. ���������������� �� �� 100%-��� ����������� �������� � ��������</P></DIR></FONT></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>52</FONT></P></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR">
<P align=right>52</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<DIR><FONT face="Times New Roman CYR">
<P align=justify>1.2. ��, ������������������ ������ ������, �� ��� �� ���������� �������� ������� � �� ���������� �������� (� ������ �������������� �������������� �����)</P></DIR></FONT></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right></P>
<P align=right></P>
<DIR>
<P align=right>1</P></DIR></FONT></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR">
<P align=right></P>
<P align=right></P>
<DIR>
<P align=right>2</P></DIR></FONT></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR">
<P align=justify>� ��� �����:</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR">
<P align=justify></FONT><FONT face=Wingdings>l</FONT><FONT face="Times New Roman CYR"> �����</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<DIR>
<DIR><FONT face="Times New Roman CYR">
<P align=right>1</P></DIR></DIR></FONT></TD>
<TD vAlign=top width="15%">
<DIR>
<DIR><FONT face="Times New Roman CYR">
<P align=right>2</P></DIR></DIR></FONT></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR">
<P align=justify></FONT><FONT face=Wingdings>l</FONT><FONT face="Times New Roman CYR"> ������������ ��</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<DIR>
<DIR><FONT face="Times New Roman CYR">
<P align=right>0</P></DIR></DIR></FONT></TD>
<TD vAlign=top width="15%">
<DIR>
<DIR><FONT face="Times New Roman CYR">
<P align=right>0</P></DIR></DIR></FONT></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<DIR>
<DIR><B><FONT face="Times New Roman CYR">
<P align=justify>2. ������������ ��, ������������������ �� 01.07.2002 �������</P>
<P align=justify>��������</P></DIR></DIR></B></FONT></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR">
<P align=right>0</B></FONT></P></TD>
<TD vAlign=top width="15%"><B><FONT face="Times New Roman CYR">
<P align=right>0</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top colSpan=5><B><FONT face="Times New Roman CYR">
<P align=center>����������� ��������� �����������</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><B><FONT face="Times New Roman CYR">
<P align=justify>3. ��, ������� ����� �� ������������� ���������� ��������, �����<SUP>2</SUP></B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>1 189</B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=right>1 183</B></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR">
<P align=justify>� ��� �����:</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR">
<P align=justify></FONT><FONT face=Wingdings>l</FONT><FONT face="Times New Roman CYR"> �����</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 143</P></TD>
<TD vAlign=top width="15%">
<P align=right>1 138</P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><FONT face="Times New Roman CYR">
<P align=justify></FONT><FONT face=Wingdings>l</FONT><FONT face="Times New Roman CYR"> ������������ ��</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>46</P></TD>
<TD vAlign=top width="15%">
<P align=right>45</P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top colSpan=5><I><FONT face="Times New Roman CYR">
<P align=right>�����������</I></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=center>�� 01.01.2007</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=center>�� 01.03.</FONT>2007</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR><FONT face="Times New Roman CYR">
<P align=justify>3.1. ��, ������� �������� (����������), ��������������� ����� ��:</P></DIR></FONT></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify></FONT><FONT face=Wingdings>l</FONT><FONT face="Times New Roman CYR"> ����������� ������� ���������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>921</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>922</P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify></FONT><FONT face=Wingdings>l</FONT><FONT face="Times New Roman CYR"> ������������� �������� � ����������� ������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>803</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>799</P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify></FONT><FONT face=Wingdings>l</FONT><FONT face="Times New Roman CYR"> ����������� ��������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>287</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>287</P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify></FONT><FONT face=Wingdings>l</FONT><FONT face="Times New Roman CYR"> ���������� �������� � �������������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify>����������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>4</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>4</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify>��������<SUP>3</SUP></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>188</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>189</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR><FONT face="Times New Roman CYR">
<P align=justify>3.2. �� � ����������� �������� � �������� ��������, �����</P></DIR></FONT></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>153</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>157</P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify>�� ���:</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify>�� 100%-���</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>52</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>52</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify>����� 50%</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>13</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>17</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR><FONT face="Times New Roman CYR">
<P align=justify>3.3. ��, ���������� � ������ ������ &#8211; ���������� ������� ������������� ����������� �������, �����</P></DIR></FONT></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>924</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>925</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B><FONT face="Times New Roman CYR">
<P align=justify>4. ������������������ �������� ������� ����������� �� (��� ���.)</B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>566 513</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>588 365</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B><FONT face="Times New Roman CYR">
<P align=justify>5. ������� ����������� �� �� ���������� ��, �����</B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>3 281</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>3 277</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify>�� ���:</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify>��������� ������<SUP>4</SUP></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>859</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>849</P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify>������ �� 100%-��� ����������� �������� � �������� ��������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>90</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>92</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B><FONT face="Times New Roman CYR">
<P align=justify>6. ������� ����������� �� �� �������, �����<SUP>5</SUP></B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR">
<P align=right>2</B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR">
<P align=right>2</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B><FONT face="Times New Roman CYR">
<P align=justify>7. ������� ������-������������ �� ���������� ���������� ���������</B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR">
<P align=right>0</B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR">
<P align=right>0</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B><FONT face="Times New Roman CYR">
<P align=justify>8. ����������������� ����������� ���������� ��, �����<SUP>6</SUP></B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>699</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>714</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify>� ��� �����:</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify></FONT><B><FONT face=Wingdings>l</B></FONT><FONT face="Times New Roman CYR"> �� ���������� ���������� ���������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>657</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>669</P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify></FONT><B><FONT face=Wingdings>l</B></FONT><FONT face="Times New Roman CYR"> � ������� ���������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>29</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>32</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify></FONT><B><FONT face=Wingdings>l</B></FONT><FONT face="Times New Roman CYR"> � ������� ���������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>13</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>13</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B><FONT face="Times New Roman CYR">
<P align=justify>9. �������������� ����� ��, �����</B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR">
<P align=right>15 007</B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR">
<P align=right>15 491</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B><FONT face="Times New Roman CYR">
<P align=justify></B>� ��� ����� ��������� ������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>7 282</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>7389</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B><FONT face="Times New Roman CYR">
<P align=justify>10. ������������ ����� ��� ��������� ���� ��, �����</B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR">
<P align=right>15 885</B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR">
<P align=right>15 796</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B><FONT face="Times New Roman CYR">
<P align=justify></B>� ��� ����� ��������� ������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>11 983</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>11 895</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B><FONT face="Times New Roman CYR">
<P align=justify>11. ��������-�������� �����, �����</B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman
 CYR">
<P align=right>996</B></FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><B><FONT face="Times New Roman CYR">
<P align=right>1106</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B><FONT face="Times New Roman CYR">
<P align=justify></B>� ��� ����� ��������� ������</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>0</FONT></P></TD>
<TD vAlign=top width="15%" colSpan=2><FONT face="Times New Roman CYR">
<P align=right>0</FONT></P></TD></TR></TBODY></TABLE><I><FONT face="Times New Roman CYR">
<P align=right>�����������</P></I></FONT>
<TABLE cellSpacing=2 cellPadding=9 width=100% align=center border=1>
<TBODY>
<TR>
<TD vAlign=top colSpan=3><B><FONT face="Times New Roman CYR">
<P align=center>����� �������� � ���������� ����������� ���</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">&nbsp;</TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR">
<P align=center>�� 01.01.2007</FONT></P></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR">
<P align=center>�� 01.03.</FONT>2007</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR><B><FONT face="Times New Roman CYR">
<P align=justify>12. ��, � ������� �������� (������������) �������� �� ������������� ���������� �������� � ������� �� ��������� �� ����� ��������������� ����������� ��������� �����������<SUP>7</P></DIR></B></SUP></FONT></TD>
<TD vAlign=top width="15%"><B><FONT face="Times New Roman CYR">
<P align=right></P></FONT>
<P align=right>155</B></P></TD>
<TD vAlign=top width="15%"><B><FONT face="Times New Roman CYR">
<P align=right></P></FONT>
<P align=right>158</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR><B><FONT face="Times New Roman CYR">
<P align=justify>13. ������� ������ � ����� ��������������� ����������� ��������� ����������� � ���������� �� ��� ������������ ����, �����<SUP>8</P></DIR></B></SUP></FONT></TD>
<TD vAlign=top width="15%"><B><FONT face="Times New Roman CYR">
<P align=right></P></FONT>
<P align=right>1 758</B></P></TD>
<TD vAlign=top width="15%"><B><FONT face="Times New Roman CYR">
<P align=right></P></FONT>
<P align=right>1 761</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify>� ��� �����:</FONT></P></TD>
<TD vAlign=top width="15%">&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<DIR><FONT face="Times New Roman CYR">
<P align=justify></FONT><B><FONT face=Wingdings>l</B></FONT><FONT face="Times New Roman CYR"> � ����� � ������� (��������������) ��������</P></DIR></DIR></FONT></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR">
<P align=right>1366</FONT></P></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR">
<P align=right>1369</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify></FONT><B><FONT face=Wingdings>l</B></FONT><FONT face="Times New Roman CYR"> � ����� � ��������������</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right>391</P></TD>
<TD vAlign=top width="15%">
<P align=right>391</P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify>�� ���: � ����� �������</FONT></P></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR">
<P align=right>2</FONT></P></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR">
<P align=right>2</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify>� ����� �������������</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right>389</P></TD>
<TD vAlign=top width="15%">
<P align=right>389</P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify>� ��� �����: </FONT></P></TD>
<TD vAlign=top width="15%">&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify>����� �������������� � ������� ������ ������</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=right>341</P></TD>
<TD vAlign=top width="15%">
<P align=right>341</P></TD></TR>
<TR>
<TD vAlign=top width="69%"><FONT face="Times New Roman CYR">
<P align=justify>������������ � ������ ������ (��� ��������-<BR>��� �������)</FONT></P></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR">
<P align=right></P>
<P align=right>48</FONT></P></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR">
<P align=right></P>
<P align=right>48</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<DIR><FONT face="Times New Roman CYR">
<P align=justify></FONT><B><FONT face=Wingdings>l</B></FONT><FONT face="Times New Roman CYR"> � ����� � ���������� ���������������� � ����� ������ ��������� ��������</P></DIR></DIR></FONT></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR">
<P align=right></P>
<P align=right>1</FONT></P></TD>
<TD vAlign=top width="15%"><FONT face="Times New Roman CYR">
<P align=right></P>
<P align=right>1</FONT></P></TD></TR></TBODY></TABLE><FONT face="Times New Roman CYR">
<P></P><FONT face="Times New Roman CYR">
<P><SUP>*</SUP> ���������� ������������ � ��� ����� � �� ��������� ��������, ����������� �� ��������������� ��������������� ������ �� �������� ����.</P>
<P><SUP>*</SUP></FONT><FONT face="Times New Roman CYR">*</FONT><FONT face="Times New Roman CYR"> �� &#8211; ��������� �����������. ������� "��������� �����������" � ��������� ���������� �������� ���� �� ��������� �������:</P>
<DIR>
<DIR></FONT><FONT face=Wingdings>
<P align=justify><SUP>l</SUP></FONT><FONT face="Times New Roman CYR"><SUP> ����������� ����, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� ������� � ������� ����� �� ������������� ���������� ��������;</SUP></P></FONT><FONT face=Wingdings>
<P align=justify><SUP>l</SUP></FONT><FONT face="Times New Roman CYR"><SUP> ����������� ����, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� �������, �������, �� ���������� ����� �� ������������� ���������� ��������;</SUP></P></FONT><FONT face=Wingdings>
<P align=justify><SUP>l </SUP></FONT><FONT face="Times New Roman CYR"><SUP>����������� ����, ������������������ ������� �������� (�� ���������� � ���� ������������ ������ "� ������ � ���������� ������������") � ������� �������� ����� ������ �� ������������� ���������� ��������.</SUP></P></DIR></DIR>
<P><SUP>1 </SUP>����������� ��, ������� ������ ������������ ���� �� �������� ����, � ��� ����� ��, ���������� ����� �� ������������� ���������� ��������, �� ��� �� ��������������� ��� ����������� ����.</P>
<P><SUP>2 </SUP>����������� ��, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� ������� � ������� ����� �� ������������� ���������� ��������, � ����� ������������ ��, ������������������ ������� �������� � ���������� �������� ����� ������ �� ������������� ���������� ��������.</P><SUP><FONT face="Times New Roman CYR">
<P align=justify>3 </SUP>�������� � ������� 1996 �. � ������������ � ������� ����� ������ �� 03.12.1996 &#8470; 367.</P><SUP>
<P align=justify>4 </SUP>����������� ������� ��������� ������, ��������� � ����� ��������������� ����������� ��������� ����������� � ���������� ���������� ������. �� 01.01.1998 �. � ����������� ���������� � ��������� ������������ �� ������ ������ ����������� ����� ���������� ���������� ��������� ������ &#8211; 34 426.</P><SUP>
<P align=justify>5 </SUP>����������� �������, �������� ����������� �� �� �������.</P><SUP>
<P>6 </SUP>� ����� ���������������� ���������� �� �� ������� �������� �����������������, �� ������� ��������� � ���� ������ ����������� �� �������� �� �� �������.</P></FONT></FONT>
<P><SUP>7</SUP> ����� ���������� ��, � ������� ������ ������ ���� �������� (������������) �������� �� ������������� ���������� �������� (������� ��, �� ������� � ����� ��������������� ����������� ��������� ����������� ������� ������ �� �� ����������) &#8211; 1538.</P><SUP>
<P align=justify>8</SUP> ����� 01.07.2002 � ����� ��������������� ����������� ��������� ����������� ������ � ���������� ��������� ����������� ��� ������������ ���� �������� ������ ����� ��������������� ����������� ��������� ����������� � ����� � �� ����������� �������������� �������������� �������.</P></FONT>


</body>
</html>

