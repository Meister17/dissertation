<!--
ID:35314604
TITLE:������������ �������
SOURCE_ID:3126
SOURCE_NAME:������� ����������� ����
AUTHOR:
NUMBER:5
ISSUE_DATE:2005-05-31
RECORD_DATE:2005-09-21


-->
<html>
<head>
<title>
35314604-������������ �������
</title>
</head>
<body >


<P align=justify><A name=_Toc70486537><A name=_Toc93813539></A><B>� ������� ���������� ������ ������, ������������ ������������� �������� ���������� ������ �� ��������� ��������.</B> �������� �������������, �������� ����� ����� ���������� �� ��������� ���������� ������ � ����������� �������� � ������������ �� ���������� ������ ��������� �� ����� ����������. �� ������ ������� �������������, ������ �������� ������ ��������� ����� 50% �� ������ ���������������� �� ��. ����������� ����� �� ��������� �������� ����� ����� ���� ��������� ���� ��� � �����. ��������������, ��� �������� ����� ��������������� �� ������������ ������� �� ��������������� ���������� ���. ������������ ����� ������ �� ������������ ������ ������ ���������� � ��������� �����.</P><B>
<P align=center>� �������� �� ��������� ������������ � ���������� ������</P></B>
<P align=justify>&#252; <B>������� ������������ ������� �������������, ������������ �������� ������ �����, ������������ �� ��, ������ ������ ��������, �������� ������� ��� ��������</B>. ��� ����������� �������� ���� ����������� � ������������ ��������. �������������� �������� �������� � ������ 39 � 46 ������ &#8220;� ����������� ����� ���������� ��������� (����� ������)&#8221;, ������������ �� ���������� ������� ������ �����, ������������ � �������� �������� ������ ������ �� ������������ �������-��������� �������� �� �� &#8212; �������� �� �������� �����. ������������ ���������� ��������� ������� &#8220;�������� �� �������� �����&#8221;, ����������� ����� ����� ������ ������������ ������������� �������� � ������� ��������, �������� ������� ��������������� ������� ����������. ������������ ������ ��������� � �������� ���������� ��������, ����������� �� �� ������������ �� �������� ����� ������������� �������� � ������� �������� � ����������� ������� �������� ������. � ������������ � ����������� �����������������, �� �� ����� ����� ������������ �������� �� �������� �����, � ��� ����� � �������������, ������ � ���������������� ������� �������� � ����������� ����� ������.</P>
<P align=justify>&#252; <B>������� ������������ ������� �� ������ ������ �������� � ����� &#8220;� ����� ������ �����&#8221;, ����������� ����������� ������� ����� ����� ���������</B>. �������� ������������� ����������� ������ ������������� ���� ����������, ������� �������� ��������, � ����������� ������������� ������������ �� ����������. ��������� ������ �� ���������� � ��������� ������������ ����� ���� ������ ������ ������, ���������� ���������, ���������� � �������� �������� ����� ����� � �� ������ ��������� �� ��������� ��������� � ��������� �����. ������������ ������������� ����������� ������� ��������� � ��������� ���������� ��� �� ���������� ��������� ���������, ��� � ��������. �������� ������������� ����������� ������� �������� ��������� � �������� ������� ������� ���������, ����������������� ��������� �������������� �����������.</P><B>
<P align=center>� �������� �� ������� � �������</P></B>
<P align=justify>&#252; <B>������� ������������ ������� �� ������ ������ �������� � ����� ������ ���������� ������� ��, ���������� �������� ���������� ������ �� �������</B>. � ���������, ������� ������� ���������� ��������������� ������, ������� ���������������, ��� 10% �� ��������� ����������� ����� ����� ���������� �� �������������, � ��������� 90% ����� ���������������� � ������������� �������. ����� ����, ������� ��������� ��������, � ������������ � ������� � 2006 ���� �� ������ ������� �������� ����� ��������� �� 50% �� ���������������� ����.</P>
<P align=justify>&#252; <B>������� ������������ ������� � ������ ������ �������� � ���������������� � �������� �������������, ������������ �� ������������� ����������� � ������������� ��������� ����� �������� ��������</B>. �������� ��������� ����������, ����������� �������� ����������� �������� � ����������� ������� ����� �����������. � ���������, � ���� ������������� �������� ��������, ��������� � �������� ������ � ��������� ������� ������ ����� � ����������� �������� ������� ��� ����������� ��� � ��������� ������������ �� �������. �������� ��������, ����������������� ������� � �������� ��� ������������� ������������ ���������������, ����������� � ���� ����������� ���������������� ��, � ����� ��������, ��������� � ���������� ���������-����������� �� ���� ������-���������� � ������ �� ��������� ������ ����, �� ����������� ����������� 5 ���. ��������. ����� ����, �������� �������� �� ���� ��������, ��������� � ����������� �������� ���� ������� ��������� �������.</P>
<P align=justify>�������������� ������������ �������� �����-���������� ������ ������������ ��������� ���� �������� ��� ������������� ���������� ������. ��������, ��� �������� ��������-������������ �� ���������� �� ������������, ����������� � ������ �����, ����������� ���������, ��� ��� ������������ � ������ ��������� ��� ������� ����� ����������� ����������. ����� ������������ �������� �������� �������, ��� ������� �������-��������� ����� ����� �������������� �������� ����������� �������. ����������� �������������� �������� ����������� ������� � ��������� ������������ �������� � ��� ���������� ������� � �������� ����� � ���� ���������� ������������ ������� ��� ������������� ����������.</P>
<P align=justify>������������ �������� ������� ����� � �� ����������� ������ (����������� � �������������) �������� ����������� ������, ������ ��, ������� � ���������� ������ ����� � ������������� �����. ����������, ��� �������� ����� ������� �� 10 ���. ����., � �����, ����������� ��������� ������, �������� ��������������. ������ �����, ������������� ������� ������ �������� ������ ��, � ����� ������� � ���������� ������ ����� � ������������� �����. ���� ������� ���������� ���� ������������ ������� ������ �������� ����������� ������. ��� ���� ������������ ��������� ������������� ��������������� ����������� ������ ������ �� � ���������� ������ ����� � ������������� �����.</P>
<P align=justify>����� ����, ����������������� �������� ���������� �������� ����, �������� ������������� �� ����� �� ����� ���� ��������������. ������������ ���������� �������� ��������� ������ �������� ������� ��������� ��������. � �������� ������ ���� ������ �� ������������. ����� ������������ ��������� ���������, ����������� ����� ���������������� � ������� � ������ � ���������������� � ��������������� ����������� �������, ���������� ���������� �����.</P>
<P align=justify><B>������� ������������ ������� � ������ ������ �������� � �� �� � ����� &#8220;� ������ � ���������� ������������&#8221;, ����������������� ������ �������� ���������� �� �������� ������� �����</B>. � ��������� ����� �� �� ��������������� ������ ���������� �� �������� ������� ��� ����� � ������� 20 ���. ���., � ���������� ���������������� ��������� ��������� ����������� ���������� ���� �� �������� ������ ������� � ������� 10 ���. ������. �� ������ ���������, ������������ ����� � ������ ���� ������������� ��������� �������� ����������� ������� � ��.</P>


</body>
</html>

