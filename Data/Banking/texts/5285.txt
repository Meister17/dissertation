<!--
ID:36436525
TITLE:��������� ������������� ������ �� �������������� �������� � ���������-���������� ����� ������
SOURCE_ID:3126
SOURCE_NAME:������� ����������� ����
AUTHOR:
NUMBER:4
ISSUE_DATE:2006-04-30
RECORD_DATE:2006-07-10


-->
<html>
<head>
<title>
36436525-��������� ������������� ������ �� �������������� �������� � ���������-���������� ����� ������
</title>
</head>
<body >


<B>
<P align=justify>14 ������ � ������ ������ ��������� ��������� ������������� ������ �� �������������� �������� � ���������-���������� ����� ������.</P></B>
<P align=justify>���� ��������� &#8220;���������� �������� �������� &#8212; ��������� &#8220;���������� ������&#8221; ��� ���������. ���� ���������, ������������� ����������� ������������ ������ ������: ������������ ����� ���������� ������ � ���������, ������ �����, � ���������� � ��������, ������������ ������� �� ���������� ������������� ����������, ���������� � ��������� ���������, �������������� ��������� � ������ � ������������ ������� � ���������-���������� �����, ���������� ������ ������ ������ ��� ��������� ��������������� ����������.</P>
<P align=justify>�������� ������� ������-���������� � ������������ ��������������� ������� ���� ����������� ����������� ������ �. �. ������� �� ����� ���������� ������� � ��������������� ������� ������. ������ ��� �������������� ��������������� ��������, ����������� ���������� �������� ��������, ����� ����� ��� ��������� ������. ���������� ������ ������ ���� �����.</P>
<P align=justify>�� ���������, ������� ������ ��� ����������������� ������������ ������, ����� ������ ���������� �����-����� <B>�. �. ������</B>, ���������: ����������� �������� �������������� ������ �������� ������������� ������ <B>�. �. �������</B> � ������������ ������ ����������� � ������� ������ <B>�. �. �����</B>, ����������� ��������� &#8220;���������� ������&#8221;, ���� ������������� ������, ����������� ������������ ��������� ����������� <B>�. �. ������</B>, ������������ �� &#8220;��������� &#8220;��� �����&#8221; <B>�. �. �����</B> � <B>�. �. ���������</B>, ����������� �������� ���������� ������������� ������ ���������� <B>�. �. �������</B>, �������������� �������� ������������� ����� ���������� ���������� ����������� <B>�. �. ���������</B>, ����������� ������������ ��������� �������� ����� �������� <B>�. �. ����</B>, ������������ ������ ������������ ������ ��������� �� ����������� ������� <B>�. �. ����������</B>, ��������� ������ ������� � ������ � ��������������� ����� &#8220;������&#8221; <B>�. �. ������</B>, ������������ �����-������ �������������� �������������� ����� <B>�. �. ��������</B> � ��.</P>
<P align=center>* * *</P>
<P align=justify>������ �������� ������������ � ������������� ������, � ��������, ���������, ��������� ������ �� �����. ���� 20% ��������� ����� ������, ������� �� ���������� ��������� ����������, � ������������ �������, ��������������� ������������� �������� ��������� �������� ���������. ����� ��������� ����������� �� ������ ������ ����������� ���������� �������� ���������, ������� ����������� ��������� &#8220;���������� ������&#8221;, ����������� ������������ ��������� ����������� &#8212; ���� �� ������ ���������� ����� ������. ��� ������ �� ������ ����, � ������ ������� �� ��������. ���� ��� �������� ������������ � ����� �������. ����������� ��������� ����� �������� 8-11 ������� ������������������� ������� ���������, ������� ����� ��������� ��� ������ ������������ ���������.</P>
<P align=justify>���������� ������� ���������� � �������� ������ ����������� � ����������� ������ ������������ ������ � �������� �������, ����������� ��������� ����������� ��������� �����������, �������� ���� �����, ������������ ��������������� ����������� &#8220;��������� &#8220;��� �����&#8221;, ������� ����� ����������� � ������������� ��������� &#8220;���������� ������&#8221;. � ��� ���� ����������� ���� ���������� � 40 �������� ������ ����������� ������������������� ���������. �� ������������, ��� ����� ���������, ������������ ����������� &#8220;������&#8221;, �������� �������� �� ������ �������� ���������� ��������, �� � ����� ����� �������� � ���������-���������� �����. ��� ��� ����� ��� �������������� ������������ ���������� � �������� ���������, ��� �������� ������.</P>


</body>
</html>

