<!--
ID:37492976
TITLE:&#171;���������� �����������&#187; � ����� ������
SOURCE_ID:11528
SOURCE_NAME:���. ����� � ������� ���
AUTHOR:
NUMBER:3
ISSUE_DATE:2007-03-31
RECORD_DATE:2007-04-04


-->
<html>
<head>
<title>
37492976-&#171;���������� �����������&#187; � ����� ������
</title>
</head>
<body >


<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<P class=vrez align=justify><STRONG>�� 2 �� 4 ����� � ������ ������� ������ XV ������������� �������� &#171;���������� �����������&#187;. </STRONG></P></BLOCKQUOTE>
<P align=justify>������ ������������, ����� �� ����� �������� ����������� ����� ���� �������� ���������� �������� � ������������� ����������� �� ��������� ��� ����������� ������� ������� ����� ���������: ����������� ���������, ������, ���������� ������� &#8212; � �����������: ������� ����� ��� �������������� �������� � ������ �������. 
<P align=justify>���������� �������� ��������� � ���������� �������, �������� ��� �� ����, ���������: ������������ ������ � �������� ���������� ����� ����������, ������ �� ��� ������� ������ ��������� ����, ����� ����������� ��������� � ������������ ����� ������������. ������� ���� ������� ������ � �����, ����� ��������� ������� �������� �����, ������ ������ � ����������������� ���������� �����. �� ���� ������� �� �����-����������� �� ������ �������� �������� ��������� ��� <B>������ ����������:</B> <EM>&#171;� ����� �������������� ���������� ��������, ��������� ����, ��� ������� ���������� � �������� ������, � ������� &#8212; ������� � ����&#187;.</EM> 
<P align=justify>����� �������� ����� ���������, ������� �������� ���� �������������� ����, ��� �������� � ��������� ��� �����. �������� &#171;���������� �����������&#187; ������������ ����������� ���� �������� ���������������� ������������ �� ���������� ������������ ���������� � ����� ����� &#8212; � ��������� ������� ���������� ,��� ������ � ������������ ��������. 
<P align=justify>�� �����-����������� ����� �������� <B>�������� �������, </B>����������� ������������ �������� �������� �� ��������� ������������ � ���������� ������ � ��������� ���������� &#171;������&#187;, �����������, ��� ������ ��������� ������� ������ �������. ������ �� ������������ � ������� ��������� ��������� �������������� ��������� ��������������, ������� ������ ������������� �������, ��������� � ������� � �������� �������� ��� ��������, �� � ������. 
<P align=justify>� ���� �������� �����, ������������ ���������� �������� ���������� �������, ����������� ���������� ���������� �������� � ������� �� ����������� ���������� ���������������� ���<B> ������� ��������</B> �������, � ���������, ��� &#171;�����������&#187; ������� �� ����� ���� �� ���������� �� ������ ������, � ������ ������ ��������������� ������������ ����������� ������. � ������� ���������� ������: &#171;&#8230;���� �� ������ ������ ���-�� ����� &#171;������� ������ � ����&#187;, ����� ������������ � ����, �� ���� ����������� &#171;������� ��� 9%&#187;, ������ ���������, ��� ����� �������� ������ ������� ��� 9%, ����� �������������� ������� ���� ��� ����� � ������&#8230;&#187;. �� ���� ��� ��������, ��� ����������� ��� ����������� ������� ���������� ��������� ��������� ��� ������, ������ ��� � ���-��� ������� �� ������� 100x100, ������� ���������� ������, ������ �� ���������� ����������� ����������. ����� ���, ��������� �������� � ������� ���� �������� �������: ����� � ������� ���� ���������� ������������� �� ����������� ������� ��������� �� ����������� � ������ $11,3 ���. 
<P align=justify>������� ���� ������ ����� ������-�������, ������� ������������ ������� ��������� �����������. �������������, ������������ �������� ������� ������ ������������ � �������� ����� ������� � ���������� ����������� ����������� ���������� ������� ���������. � ���������, ���� ��� � ������������ ������� ���������� �� �������� ����� � ����� FOREX, �������� �������� ����� � ����, ������������� ���������� � ������ ��������� ���������� ��������. 
<P align=justify>��������� �������� ������ � ������ ������, � ��������. 
<P class=vinos-spravka align=justify>������ ��� ������ �������� ������� � ������ ��������. ���� ��������� ����� ����������: ������� ���� ������������ ��������� ������ ������� � ��������� ���������, � �� ������ ���������� ������������� ���������. ��� �������� ����� �������� ��� � ������� � ��������� ������. � �������� ��� ����������<BR>��� ��� ��� ��� �������� ������������� ����������� �� ���� &#171;���������������� ��������� ������������ � �����&#187;. ������ ��� ���������� ��������� ������, ��������� ������: &#171;��������� �� �� �������� ������-���� �����?&#187; � �� ��� ��� �� ���� �������� �� ������ �������������� ������. ������ ���� ���������, ������� �������, �� ����������, ������� �������� �� ��� ����������� ����������� �����. � ����� ���� ������� ������� ���������� ����������� �������.<BR>� ������������ ������� ������������ �������� ������ ������������� � ��������� ������.</P>


</body>
</html>

