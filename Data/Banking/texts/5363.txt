<!--
ID:36676178
TITLE:����� ������ &#151; ������ ���������� �� ������� ��������� ���������� � ������ ��� ��������� ���������� ������������, ������������� ��������� ��� � ������ ������
SOURCE_ID:3126
SOURCE_NAME:������� ����������� ����
AUTHOR:
NUMBER:5
ISSUE_DATE:2006-05-31
RECORD_DATE:2006-09-13


-->
<html>
<head>
<title>
36676178-����� ������ &#151; ������ ���������� �� ������� ��������� ���������� � ������ ��� ��������� ���������� ������������, ������������� ��������� ��� � ������ ������
</title>
</head>
<body >


<P align=justify><STRONG>���������� ��������� � ����������� ��������������� ������ ������ ���������� ������ ������ &#8212; ������ ���������� �� ������� ��������� ���������� � ������ ��� ��������� ���������� ������������, ������������� ��������� � ��� � ������ ������. � ������ ���������:</STRONG></P>
<P align=justify>� ����� �������������� ��������� ���������� ������������� �������������� ����������� ��� ������������� ���������������� ������������ �������� ����� ����� ������ &#8212; ������ ���������� �� ������� ��������� ���������� � ������ ��� ��������� ���������� ������������, ������������� ��������� ��� � ������ ������ (������ �� 26.05.2005 &#8470; ��/7235/77-�).</P>
<P align=justify>������ ���� ���������� �������� �� ��������� �������:</P>
<UL>
<P align=justify>
<LI>��������� �� ���� ��������� ����������� ��������� ���� ������������ ��� �������������� ��������������� ��������?</LI>
<P align=justify>
<LI>�������� �� ��������� ����������� �� ������������� ����������� ��������������� ������� ��������������� ���� ����������?</LI></UL>
<P align=justify>�� ����������� ������ ���������� ������� &#8470;&#8470; 1, 2.</P><B>
<P align=right>������� &#8470; 1</P>
<P align=center>�����, ���������� ������ �� ������� ��������� ����������<BR>� ������ ��� ��������� ���������� ������������,<BR>������������� ��������� ��� � ������ ������</P></B>
<TABLE cellSpacing=1 cellPadding=7 width=648 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="6%" height=78><B>
<P align=center>&#8470;</P>
<P align=center>�/�</B></P></TD>
<TD vAlign=top width="30%" height=78><B>
<P align=center>������������ �����</B></P></TD>
<TD vAlign=top width="19%" height=78><B>
<P align=center>���������������</B></P></TD>
<TD vAlign=top width="26%" colSpan=3 height=78><B>
<P align=center>��������� �� �� ������������ ��� � �� ��<BR>(������ �� 26.05.2005<BR>&#8470; ��/7235/77-�)<BR>��� �������������� ��������������� ��������</B></P></TD>
<TD vAlign=top width="20%" colSpan=2 height=78><B>
<P align=center>�������� �� ��������� �����������<BR>�� ������������� ��� ��������������� ���� ����������</B></P></TD></TR>
<TR>
<TD vAlign=top width="54%" colSpan=3 height=16>
<P></P></TD>
<TD vAlign=top width="6%" height=16><B>
<P align=center>��</B></P></TD>
<TD vAlign=top width="7%" height=16><B>
<P align=center>���</B></P></TD>
<TD vAlign=top width="13%" height=16><B>
<P align=center>��������</B></P></TD>
<TD vAlign=top width="8%" height=16><B>
<P align=center>��</B></P></TD>
<TD vAlign=top width="13%" height=16><B>
<P align=center>���</B></P></TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>���������������</P></TD>
<TD vAlign=top width="19%">
<P align=justify>������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>�����-���������� ����</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. �����-��������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>���� &#8220;������������&#8221;</P></TD>
<TD vAlign=top width="19%">
<P align=justify>������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>��������������������</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>���� &#8220;������������&#8221;</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ������������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>��������� ���� ������������� � ��������</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ������������</P></TD>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">
<P align=center>�</P></TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>�������������</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ���</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>���������������</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ���������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>���� &#8220;�����&#8221;</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ���������� ����</P></TD>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">
<P align=center>�</P></TD>
<TD vAlign=top width="8%">&nbsp;</TD>
<TD vAlign=top width="13%">
<P align=center>�</P></TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>���������� ��������� ����</P></TD>
<TD vAlign=top width="19%">
<P align=justify>������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>������ ����</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ������ ��������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>���� &#8220;�������&#8221;</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. �������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>������������</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ���������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>��������� ������������� ����</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ������</P></TD>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">
<P align=center>�</P></TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>��������������</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>��������������</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ������������</P></TD>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">
<P align=center>�</P></TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>���-����</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. �������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>������������������</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. �����</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>�� &#8220;�������������&#8221;</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ����-���</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>��������������</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ��������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>��� &#8220;������ ��������������&#8221;</P></TD>
<TD vAlign=top width="19%">
<P align=justify>������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>�������� ����</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>�����������</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ���������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>����������������</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ������ ��������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>�� &#8220;����������&#8221;</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ������ ��������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>�� &#8220;���������&#8221;</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ������������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>��� &#8220;�������&#8221;</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. �����������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>������ ������������</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. �����</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>�� &#8220;������ ������&#8221;</P></TD>
<TD vAlign=top width="19%">
<P align=justify>������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">&nbsp;</TD>
<TD vAlign=top width="13%">
<P align=center>�</P></TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>��� &#8220;�������������&#8221;</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>��������� ������������� ����</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ���������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>���������</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ���������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>�������</P></TD>
<TD vAl
ign=top width="19%">
<P align=justify>�. ������ ��������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>���������� ���� &#8220;�������&#8221;</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. �����</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>������������� ������������ ������������� ����</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. �����������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>�� &#8220;������&#8221;</P></TD>
<TD vAlign=top width="19%">
<P align=justify>������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>����������</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ������</P></TD>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="7%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">&nbsp;</TD>
<TD vAlign=top width="13%">
<P align=center>�</P></TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>��� &#8220;������&#8221;</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ����������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="30%">
<P align=justify>�����������������������</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�. ����������</P></TD>
<TD vAlign=top width="6%">
<P align=center>�</P></TD>
<TD vAlign=top width="7%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="8%">
<P align=center>�</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR></TBODY></TABLE><B>
<P align=right>������� &#8470; 2</P>
<P align=center>�����, �� ���������� �� ����� ���������������� ������������ �� ��������� �� 10.04.2006,<BR>� ��� ����� �� ��������������� ������������ ���������<BR>��� ������������ ����������� ��������</P></B>
<TABLE cellSpacing=1 cellPadding=7 width=648 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="9%"><B>
<P align=center>&#8470; �/�</B></P></TD>
<TD vAlign=top width="44%"><B>
<P align=center>������������ �����</B></P></TD>
<TD vAlign=top width="46%"><B>
<P align=center>���������������</B></P></TD></TR>
<TR>
<TD vAlign=top width="9%">
<P align=center>1</P></TD>
<TD vAlign=top width="44%">
<P align=justify>�������������</P></TD>
<TD vAlign=top width="46%">
<P align=center>�. �����������</P></TD></TR>
<TR>
<TD vAlign=top width="9%">
<P align=center>2</P></TD>
<TD vAlign=top width="44%">
<P align=justify>�� &#8220;���������&#8221;</P></TD>
<TD vAlign=top width="46%">
<P align=center>�. ���������</P></TD></TR>
<TR>
<TD vAlign=top width="9%">
<P align=center>3</P></TD>
<TD vAlign=top width="44%">
<P align=justify>����������</P></TD>
<TD vAlign=top width="46%">
<P align=center>������</P></TD></TR>
<TR>
<TD vAlign=top width="9%">
<P align=center>4</P></TD>
<TD vAlign=top width="44%">
<P align=justify>�����-����</P></TD>
<TD vAlign=top width="46%">
<P align=center>������</P></TD></TR></TBODY></TABLE>


</body>
</html>

