<!--
ID:36104010
TITLE:�������������� �������� �� ������������ �������� �����  � 2005 ����. �������� ������
SOURCE_ID:3099
SOURCE_NAME:������ �����-����������
AUTHOR:
NUMBER:1
ISSUE_DATE:2006-03-31
RECORD_DATE:2006-04-14


RBR_ID:2038
RBR_NAME:���������� �����
RBR_ID:4928
RBR_NAME:��������� � ������� ��������
-->
<html>
<head>
<title>
36104010-�������������� �������� �� ������������ �������� �����  � 2005 ����. �������� ������
</title>
</head>
<body >


<P align=justify>� 2005 ���� ���� ������� ��� � ����������� �����, ������������� ����������� ������ ���������� ���������, ��������� � 27,7487 ���. �� 1 ������ �� 28,7825 ���. �� 31&nbsp;������� (�� 1,0338 ����� ��� �� 3,72&nbsp;%).</P>
<P align=justify>� ������ � 1 ������ �� 31 ������� 2005 ���� ���� ������� ��� �� ������ �������� ������ �� ����������� USD/RUB_UTS_TOD ��������� � 27,9538 ���. �� 11 ������ �� 28,7397 ���. �� 29 ������� (�� 0,7859 ���. ��� �� 2,8&nbsp;%); ���� ���� �� ������ �������� ������ �� ����������� EUR/RUB_UTS_TOD ��������� � 36,7517 ���. �� 11 ������ �� 34,1233 ���. �� 29 ������� (�� 2,6284 ���. ��� �� 7,6 %).</P>
<P align=justify>���� ������� ��� �� �����-������������� �������� ����� (����� &#8211; ����) �� ����������� USD/RUB_TOD ��������� � 27,9300 ���. �� 14 ������ �� 28,8275 ���. �� 22&nbsp;������� (�� 0,8975 ���. ��� �� 3,21 %); ���� ���� �� ������� �������� ������ ���� �� ����������� EUR/RUB_TOD ��������� � 36,6939 ���. �� 12 ������ �� 34,1150 ���. �� 27&nbsp;������� (�� 2,5789 ����� ��� �� 7,0 %)<I>.</I></P>
<P align=justify>��������� ������ ������ ����������� ������� �� ���� (������� ������ �� ��� � ������� �������� ������) �� 2005 ��� ��������: 16706,097 ���. �������� ���; 2204,419<FONT color=#808000>&nbsp;</FONT>���. ����. �������� ������ �������� � 2005 ���� 539,713 ����. ���.</P>
<P align=justify>�� ��� ������� �� ����������� USD/RUB_UTS ��������� 16682,483<FONT color=#808000> </FONT>���. �������� ���; �� ����������� EUR/RUB_UTS &#8211; 2186,515<FONT color=#808000> </FONT>���. ����.</P><B>
<P align=right>������ &#8470;1</P></B>
<P align=center></P><B>
<P align=right>������ &#8470;2</P></B>
<P align=center></P>
<P align=justify><A name=_982574767></A>� 2005 ���� ��������� �����-������ ������ �� ���� ���������: 12,377 ���. �������� ���; 9,574 ���. ����.</P><B>
<P align=right>������� &#8470;1</P>
<P align=center>����������� �����-������ ������<FONT face=Symbol>*</FONT> �� ���� �� 2005 ���</P></B>
<CENTER>
<TABLE cellSpacing=1 cellPadding=7 width=563 border=1>
<TBODY>
<TR>
<TD vAlign=top width="24%"><B>
<P align=justify>�����</B></P></TD>
<TD vAlign=top width="39%"><B>
<P align=justify>����� ������, ���. USD</B></P></TD>
<TD vAlign=top width="37%"><B>
<P align=justify>����� ������, ���. EUR</B></P></TD></TR>
<TR>
<TD vAlign=top width="24%">
<P align=justify>������ 2005</P></TD>
<TD vAlign=top width="39%">
<P align=justify>0,713</P></TD>
<TD vAlign=top width="37%">
<P align=justify>1,060</P></TD></TR>
<TR>
<TD vAlign=top width="24%">
<P align=justify>������� 2005</P></TD>
<TD vAlign=top width="39%">
<P align=justify>0,288</P></TD>
<TD vAlign=top width="37%">
<P align=justify>0,527</P></TD></TR>
<TR>
<TD vAlign=top width="24%">
<P align=justify>���� 2005</P></TD>
<TD vAlign=top width="39%">
<P align=justify>2,238</P></TD>
<TD vAlign=top width="37%">
<P align=justify>0,481</P></TD></TR>
<TR>
<TD vAlign=top width="24%">
<P align=justify>������ 2005</P></TD>
<TD vAlign=top width="39%">
<P align=justify>0,392</P></TD>
<TD vAlign=top width="37%">
<P align=justify>0,672</P></TD></TR>
<TR>
<TD vAlign=top width="24%">
<P align=justify>��� 2005</P></TD>
<TD vAlign=top width="39%">
<P align=justify>1,058</P></TD>
<TD vAlign=top width="37%">
<P align=justify>0,631</P></TD></TR>
<TR>
<TD vAlign=top width="24%">
<P align=justify>���� 2005</P></TD>
<TD vAlign=top width="39%">
<P align=justify>0,055</P></TD>
<TD vAlign=top width="37%">
<P align=justify>0,962</P></TD></TR>
<TR>
<TD vAlign=top width="24%">
<P align=justify>���� 2005</P></TD>
<TD vAlign=top width="39%">
<P align=justify>1,103</P></TD>
<TD vAlign=top width="37%">
<P align=justify>0,727</P></TD></TR>
<TR>
<TD vAlign=top width="24%">
<P align=justify>������ 2005</P></TD>
<TD vAlign=top width="39%">
<P align=justify>2,544</P></TD>
<TD vAlign=top width="37%">
<P align=justify>0,648</P></TD></TR>
<TR>
<TD vAlign=top width="24%">
<P align=justify>�������� 2005</P></TD>
<TD vAlign=top width="39%">
<P align=justify>0,429</P></TD>
<TD vAlign=top width="37%">
<P align=justify>0,977</P></TD></TR>
<TR>
<TD vAlign=top width="24%">
<P align=justify>������� 2005</P></TD>
<TD vAlign=top width="39%">
<P align=justify>1,725</P></TD>
<TD vAlign=top width="37%">
<P align=justify>0,803</P></TD></TR>
<TR>
<TD vAlign=top width="24%">
<P align=justify>������ 2005</P></TD>
<TD vAlign=top width="39%">
<P align=justify>1,098</P></TD>
<TD vAlign=top width="37%">
<P align=justify>1,124</P></TD></TR>
<TR>
<TD vAlign=top width="24%">
<P align=justify>������� 2005</P></TD>
<TD vAlign=top width="39%">
<P align=justify>0,734</P></TD>
<TD vAlign=top width="37%">
<P align=justify>0,962</P></TD></TR></TBODY></TABLE></CENTER>
<P align=justify>������� ���������� �����-����� ������<SUP>*</SUP> �� ���� �� 2005 �. �� ������ &#8220;������� ���&#8221; �������� 0,221 ���. �������� ���; �� ������ &#8220;����&#8221; &#8212; 0,074 ���. ����. � 2005&nbsp;�. �� ��������� � 2004 �. ��������� ���������� ������� ���������� �����-������� �������� ������ �� �������� ��� �� 0,055 ���. �������� ���; �� &#8220;����&#8221; &#8212; �������������� ���������� ������� ���������� �����-������� �������� ������ �� 0,003&nbsp;���. ����.</P>
<P align=justify>���� ��������� ������� �������� ������ ������� ��� � ���� �� ������ ���� � 2005 �.:</P><B>
<P align=right>������ &#8470;2</P></B><B>
<P align=center></P>
<P align=right>������ &#8470;3</P>
<P align=center></P></B>
<P align=justify>�������� � �������� ������� ��������� ����� ���������� ����������.</P>
<P align=justify>�������� ����� ����������� ������ ���������� ����������� ���������� ������������� ����� � ������� ����������� ������. � ����� �������� �������� ���������� �������� ������, ����������� �����-�������� �� ������ � 2005 �., ���������� ��������� �����: �����������-������������ ����, �����-������, �� &#8220;���-�����-���������&#8221;, ����������, ������������� ���� ���, ������-�������� ���� ��������� ��.</P><B>
<P align=right>������ &#8470;4</P>
<P align=center></P><SUP>
<P align=justify><FONT face=Symbol>*</FONT> </SUP>��� ����� ������ �� ������� ������ �����-������� �������� ��� � ���� ����� ���� � ���� ������ �� ���.</P></B>


</body>
</html>

