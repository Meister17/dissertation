<!--
ID:36057226
TITLE:�����-����������� ���������� ������ �. �. ������
SOURCE_ID:3126
SOURCE_NAME:������� ����������� ����
AUTHOR:
NUMBER:1
ISSUE_DATE:2006-01-31
RECORD_DATE:2006-04-04


-->
<html>
<head>
<title>
36057226-�����-����������� ���������� ������ �. �. ������
</title>
</head>
<body >


<B>
<P align=justify>31 ������ � ������ ������ ��������� �����-����������� �. �. ������. ��������� �������, ��� ������������ ������� ������ � 2005 �. � ������� �����, ����������������� � ������������ ������������� ���������� � ������.</P></B>
<P align=justify><FONT face=Wingdings>&#178;</FONT> <B>���� ��� �� ��������� ��� �������� 6,4%.</B> &#8220;��� �������, ���� � ����, ��� ������������� ������������� 5,9 ��������&#8221;, &#8212; ������ �. �. �����. ����� �� ������� ������ �������� ����������� ��������� �����, ������� � 2005 ���� &#8220;�������� ���������� ������ � ��� ����, � ��� ����� ������&#8221;, ������� �������� ������ &#8212; ����� �����. � �� �����, ��� � ����� ����� ���� �������� ����� 54%, � ������ �� ������ 88%.</P>
<P align=justify>��������� ��������������� ��� &#8220;������������, ������ ���������&#8221; ���� �������������� �������� �����������. &#8220;�������, �� � 2000 ���� �������� � 12 ����., ������� ��� 185 ����.&#8221;, &#8212; ������ ��. �� ����� ������� ���� ����������������� ����� � ��� ���������� �����������: ������� ���������� ����� �������� �� 9,8%, ������ ��������� &#8212; �� 8,7%, ������� ������ ������ &#8212; �� 13%. ������ � �������� ���� �� ������� ��������� � ��������������� ��������� ��������. &#8220;��� �� ������, ��� ������������� �� ������ 8,5%, � � ��� ���������� 10,8%&#8221;, ������ �� � �������, ��� �� �������� ������, ��������, �������� �������� 10,9%. &#8220;���� ������, ��� � 2004 ����, �� ���-���� ���� ������������&#8221;, &#8212; ������ ���������. �� ������� ����� �������� ���� � ������������, � ��������� ������������ ����� �����, ��� ��������� ��������� �� ���������������� �������� ��������� ������.</P>
<P align=justify><FONT face=Wingdings>&#178;</FONT> <B>������ ��������� � ��������� ����� ������� ���������� ���� �������� � �������� ��������� 2,5 ����. ����</B>., <B>������� �. �. �����</B>. �� ������ �������� � ��������� ��������� � ����������� ����������� ������ �������� � �������� ��������� � 1,5 ����. ����. (1 ����. ����. &#8212; ���� ������, 0,5 ����. ����. &#8212; ���� ����������). ����������� ���� ������ �������������� ����������� � �������� � ������ �������� ��������� � ������������� ������� � ���.</P>
<P align=justify>��� ��� ������ ������, ���� ����, ������� ������������� ��������� � ����� �� ������ ������������, �� � �������� ��������� ����� � ���, ����� ��� �������� ����� ��� �����������, ����� ������� ��� ����������� � ������� ��-����, ����������, � �� ���, ��� � ��� �������, &#8212; �� ��������� ���� � ���, ����� �������� ����������� ������������� ������ ����� � ����� ������������ � �������� �������� ������������� ����, ��� �������� �� �������� �����, &#8212; ��� ��� ������ ������ � ������ ����, ����������, ����� �������������� ���������� ����� ������...</P>
<P align=justify><FONT face=Wingdings>&#178;</FONT> <B>� 2005 �. ���������� ��������� � �������������� ���������� ��������� �� �������������� ����</B>. ���, ���������� � �������� ������� � �������� ���� ��������� 121 ����. ����., �� ��� ������ 8,7 ����. ����. ���� ���������� �� �������� ��������������������� ��������������, ��������� &#8212; � ����� ����������, ��������� ��������� � �����. �� ������ ����������, �� �������� ������ � 2005 �. ������ 6-� ����� �� �������������� �����������������. � ����� � ���� ������������� �� ������� ��� ��� ��� �������� ���������� ������������� ���������: ������� ������ ������������� ���� ��� ������� ��� �������� ��������� � ������, ���������� �������, �������, ������ � ����������; ������ �������������� ����, � ������� �� ������ ����� ����� ���������� 2,5 ����. ����.; ������� ��������� ��������� ����������������������� �������. ����� ����, ������ �. �. �����, ����������� ����������� ��� ����������� ������� �������� �� ����������� �������� ���������� ��������� �� �������������� ����.</P>
<P align=justify><FONT face=Wingdings>&#178;</FONT> <B>�. �. ����� ����������, ��� ��������� ����� ����� �������, �� ��� ���� ���������� �������� ��� ��������� ����������� ����� �������.</B> �� �������, ��� � ������ ��������� �������� �� ��������� ���������� ����� 36,8% ���. �� ��� ������, ���� ������� ��� ����������� � ������ �� ������� ��� �� ��������������, �� ��������� �������� ���� �� ����, &#8220;�� ��� ����� ��� �������� ����� ������� ��� ����� ������������� ���������, ��� ����������&#8221;. &#8220;� ������ �������, ����������� � ����������� ��������, ��������� �������� ������&#8221;, &#8212; ������������� ����� ����������� �����������. &#8220;��������� �������� � ��� ����� ������� ��� ����� ������������� ���������&#8221;, &#8212; ���������� ��. ��������� �������, ��� ������ �������� ���� ��� ����� ������, � ��� ����� ���, �������������� ����������� �������� ����� � ���������, ��� ���������� ������ � ���������. &#8220;�� ������ ��������, ��� ����� � �������� ������, ������ �� ��������� �� ������������, ������� ��� ������, ������� �������� � �������� � �������, ��� ��� ��� ����� � �������&#8221;, &#8212; ������ ���������.</P>
<P align=justify><FONT face=Wingdings>&#178;</FONT> <B>&#8220;�� ����� � ������ �������� � ������ ���������������� ���������� ������������ ������&#8221;, &#8212; ������ ��������� ��. </B>�� ��������, ��� � 2007 ���� � ������ ����������� ��������� ��� ����������� �� ������ � ��������� ����������, � ��� ����� ����� ����� �����������, ��������� � ��������������� ���� ������� ��� ���������� �������� ��������.</P>
<P align=justify><FONT face=Wingdings>&#178;</FONT> <B>���� �� �� ���� ����������������� �����, ��, �����, ��� ������������ ����� �� ������� �� ����� � �� ��� �������� ��������, ������� �� ������� ����� �� �������� ����, &#8212; 10,9%, ��� ���� �� ������.</B> �� ��� ���� �� �������, �������� �������� ������ �������� &#8212; ��������� ��������. �� ��� ������� � �������� ������ �������, ������� � ���, ��� � ��� ������� ������� ������ ���������������� � ������, �� �������� ������� ������. ��, ������ ��� 12-13%, � � ������ �������� ���� ����, ��-�����, 15. �� ���� �������� �� ����� � ��� �����, ��� �� �����, &#8212; 3, 5 ��� 6%, &#8212; �� ��� ��� � ������� ������ � ������ �����, �� ��� ��� �������� �� ������ ���������������, � ����������� ���������� �� ������ ��������� ��������� ������� ������������ ������������� �����, ������ �� ������ �������� ������� ������� ������ &#8212; �� 6-7 ���������. ��� ������� ����������� ������, ��������� � ��� ����� ����� ������������. �� ��� ��� ������������� ��� ���������.</P>
<P align=justify><FONT face=Wingdings>&#178;</FONT> <B>��������� �� ������, ��� �� �������� �������� � ��� ������ �� ���������� ��� ��� ��������.</B> ����������� ����������� ������ ��� ����������� � ���������� ������ � ��� �����������. �. �. ����� � ��������� ��� ������, ��� �������� �������� ����������� ������ � �� &#8212; �����������, ��� ��� &#8220;�� ���� ����������� �������������� ���������� ������&#8221;.</P>


</body>
</html>

