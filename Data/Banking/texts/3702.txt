<!--
ID:36860973
TITLE:���������� � ����������� � �������������� ��������� ����������� �� 01.09.2006 �.*
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:20
ISSUE_DATE:2006-10-31
RECORD_DATE:2006-10-27


-->
<html>
<head>
<title>
36860973-���������� � ����������� � �������������� ��������� ����������� �� 01.09.2006 �.*
</title>
</head>
<body >


<TABLE cellSpacing=2 cellPadding=9 width=614 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top colSpan=5><B>
<P align=center>����������� ��������� �����������</B></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=center><A name=DDE_LINK1></A></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center>�� 01.01.2006</P></TD>
<TD vAlign=top width="15%">
<P align=center>�� 01.09.2006</P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><B>
<P align=justify></A>1. ���������������� ��*<SUP>*</SUP> ������ ������ ���� �� ��������� ��� ������� �������������� �������������� �������, �����<SUP>1</SUP></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>1 409</B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=right>1 370</B></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>l ������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 356</P></TD>
<TD vAlign=top width="15%">
<P align=right>1 317</P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>l ������������ ��</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>53</P></TD>
<TD vAlign=top width="15%">
<P align=right>53</P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<DIR>
<P align=justify>1.1. ���������������� �� �� 100%-��� ����������� �������� � ��������</P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>42</P></TD>
<TD vAlign=top width="15%">
<P align=right>50</P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<DIR>
<P align=justify>1.2. ��, ������������������ ������ ������, �� ��� �� ���������� �������� ������� � �� ���������� �������� (� ������ �������������� �������������� �����)</P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right></P>
<P align=right></P>
<DIR>
<P align=right>2</P></DIR></TD>
<TD vAlign=top width="15%">
<P align=right></P>
<P align=right></P>
<DIR>
<P align=right>1</P></DIR></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>l �����</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<DIR>
<DIR>
<P align=right>2</P></DIR></DIR></TD>
<TD vAlign=top width="15%">
<DIR>
<DIR>
<P align=right>1</P></DIR></DIR></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>l ������������ ��</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<DIR>
<DIR>
<P align=right>0</P></DIR></DIR></TD>
<TD vAlign=top width="15%">
<DIR>
<DIR>
<P align=right>0</P></DIR></DIR></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<DIR>
<DIR><B>
<P align=justify>2. ������������ ��, ������������������ �� 01.07.2002 �������</P>
<P align=justify>��������</P></DIR></DIR></B></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>0</B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=right>0</B></P></TD></TR>
<TR>
<TD vAlign=top colSpan=5><B>
<P align=center>����������� ��������� �����������</B></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><B>
<P align=justify>3. ��, ������� ����� �� ������������� ���������� ��������, �����<SUP>2</SUP></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>1 253</B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=right>1 211</B></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>l �����</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 205</P></TD>
<TD vAlign=top width="15%">
<P align=right>1 163</P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>l ������������ ��</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>48</P></TD>
<TD vAlign=top width="15%">
<P align=right>48</P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top colSpan=5><I>
<P align=right>�����������</I></P></TD></TR>
<TR>
<TD vAlign=top width="69%">&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center>�� 01.01.2006</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center>�� 01.09.2006</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<P align=justify>3.1. ��, ������� �������� (����������), ��������������� ����� ��:</P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>l ����������� ������� ���������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 045</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>930</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>l ������������� �������� � ����������� ������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>827</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>820</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>l ����������� ��������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>301</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>289</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>l ���������� �������� � �������������</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>����������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>4</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>4</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>��������<SUP>3</SUP></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>180</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>186</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<P align=justify>3.2. �� � ����������� �������� � �������� ��������, �����</P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>136</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>144</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>�� ���:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>�� 100%-���</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>41</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>50</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>����� 50%</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>11</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>12</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<P align=justify>3.3. ��, ���������� � ������ ������ &#8211; ���������� ������� ������������� ����������� �������, �����</P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>930</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>930</P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify>4. ������������������ �������� ������� ����������� �� (��� ���.)</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>444 377</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>481 844</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify>5. ������� ����������� �� �� ���������� ��, �����</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>3 295</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>3 273</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>�� ���:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>��������� ������<SUP>4</SUP></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 009</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>914</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>������ �� 100%-��� ����������� �������� � �������� ��������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>29</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>85</P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify>6. ������� ����������� �� �� �������, �����<SUP>5</SUP></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>3</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>2</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify>7. ������� ������-������������ �� ���������� ���������� ���������</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>0</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>0</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify>8. ����������������� ����������� ���������� ��, �����<SUP>6</SUP></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>467</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>535</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><B>l</B> �� ���������� ���������� ���������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>422</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>494</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><B>l</B> � ������� ���������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>31</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>28</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><B>l</B> � ������� ���������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>14</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>13</P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify>9. �������������� ����� ��, �����</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>0</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>13 428</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify></B>� ��� ����� ��������� ������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>0</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>6 416</P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify>10. ������������ ����� ��� ��������� ���� ��, �����</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>0</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>16 566</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify></B>� ��� ����� ��������� ������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>0</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>12 749</P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify>11. ��������-�������� �����, �����</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>0</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>857</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify></B>� ��� ����� ��������� ������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>0</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1</P></TD></TR></TBODY></TABLE><I>
<P align=right>�����������</P></I>
<TABLE cellSpacing=2 cellPadding=9 width=614 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top colSpan=3><B>
<P align=center>����� �������� � ���������� ����������� ���</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%">&nbsp;</TD>
<TD vAlign=top width="15%">
<P align=center>�� 01.01.2006</P></TD>
<TD vAlign=top width="15%">
<P align=center>�� 01.09.2006</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR><B>
<P align=justify>12. ��, � ������� �������� (������������) �������� �� ������������� ���������� �������� � ������� �� ��������� �� ����� ��������������� ����������� ��������� �����������<SUP>7</P></DIR></B></SUP></TD>
<TD vAlign=top width="15%"><B>
<P align=right></P>
<P align=right>154</B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=right></P>
<P align=right>158</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR><B>
<P align=justify>13. ������� ������ � ����� ��������������� ����������� ��������� ����������� � ���������� �� ��� ������������ ����, �����<SUP>8</P></DIR></B></SUP></TD>
<TD vAlign=top width="15%"><B>
<P align=right></P>
<P align=right>1 687</B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=right></P>
<P align=right>1 733</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%">&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<DIR>
<P align=justify><B>l</B> � ����� � ������� (��������������) ��������</P></DIR></DIR></TD>
<TD vAlign=top width="15%">
<P align=right>1305</P></TD>
<TD vAlign=top width="15%">
<P align=right>1345</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><B>l</B> � ����� � ��������������</P></TD>
<TD vAlign=top width="15%">
<P align=right>381</P></TD>
<TD vAlign=top width="15%">
<P align=right>387</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>�� ���: � ����� �������</P></TD>
<TD vAlign=top width="15%">
<P align=right>0</P></TD>
<TD vAlign=top width="15%">
<P align=right>2</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>� ����� �������������</P></TD>
<TD vAlign=top width="15%">
<P align=right>381</P></TD>
<TD vAlign=top width="15%">
<P align=right>385</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%">&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>����� �������������� � ������� ������ ������</P></TD>
<TD vAlign=top width="15%">
<P align=right>337</P></TD>
<TD vAlign=top width="15%">
<P align=right>340</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>������������ � ������ ������ (��� ��������-<BR>��� �������)</P></TD>
<TD vAlign=top width="15%">
<P align=right></P>
<P align=right>44</P></TD>
<TD vAlign=top width="15%">
<P align=right></P>
<P align=right>45</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<DIR>
<P align=justify><B>l</B> � ����� � ���������� ���������������� � ����� ������ ��������� ��������</P></DIR></DIR></TD>
<TD vAlign=top width="15%">
<P align=right>1</P></TD>
<TD vAlign=top width="15%">
<P align=right>1</P></TD></TR></TBODY></TABLE><SUP><FONT face="Times New Roman CYR" size=2>
<P align=justify>*</SUP> ���������� ������������ � ��� ����� � �� ��������� ��������, ����������� �� ��������������� ��������������� ������ �� �������� ����.</P><SUP>
<P align=justify>*</SUP></FONT><FONT face="Times New Roman CYR" size=1>*</FONT><FONT face="Times New Roman CYR"><FONT size=2> �� &#8211; ��������� �����������. ������� "��������� �����������" � ��������� ���������� �������� ���� �� ��������� �������:</FONT></P>
<DIR>
<DIR></FONT><FONT face=Wingdings>
<P align=justify><FONT size=2>l</FONT></FONT><FONT face="Times New Roman CYR"><FONT size=2> ����������� ����, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� ������� � ������� ����� �� ������������� ���������� ��������;</FONT></P></FONT><FONT face=Wingdings>
<P align=justify><FONT size=2>l</FONT></FONT><FONT face="Times New Roman CYR"><FONT size=2> ����������� ����, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� �������, �������, �� ���������� ����� �� ������������� ���������� ��������;</FONT></P></FONT><FONT face=Wingdings>

<P align=justify><FONT size=2>l </FONT></FONT><FONT face="Times New Roman CYR"><FONT size=2>����������� ����, ������������������ ������� �������� (�� ���������� � ���� ������������ ������ "� ������ � ���������� ������������") � ������� �������� ����� ������ �� ������������� ���������� ��������.</FONT></P></DIR></DIR><SUP>
<P align=justify><FONT size=2>1 </FONT></SUP><FONT size=2>����������� ��, ������� ������ ������������ ���� �� �������� ����, � ��� ����� ��, ���������� ����� �� ������������� ���������� ��������, �� ��� �� ��������������� ��� ����������� ����.</FONT></P><SUP>
<P><FONT size=2>2 </FONT></SUP><FONT size=2>����������� ��, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� ������� � ������� ����� �� ������������� ���������� ��������, � ����� ������������ ��, ������������������ ������� �������� � ���������� �������� ����� ������ �� ������������� ���������� ��������.</FONT></P></FONT><FONT face="Times New Roman CYR">
<P><FONT size=2><SUP>3</SUP> �������� � ������� 1996 �. � ������������ � ������� ����� ������ �� 03.12.1996 &#8470; 367.</FONT></P><SUP><FONT face="Times New Roman CYR">
<P align=justify><FONT size=2>4 </FONT></SUP><FONT size=2>����������� ������� ��������� ������, ��������� � ����� ��������������� ����������� ��������� ����������� � ���������� ���������� ������. �� 01.01.1998 �. � ����������� ���������� � ��������� ������������ �� ������ ������ ����������� ����� ���������� ���������� ��������� ������ &#8211; 34 426.</FONT></P><SUP><FONT face="Times New Roman CYR">
<P><FONT size=2>5 </FONT></SUP><FONT size=2>����������� �������, �������� ����������� �� �� �������.</FONT></P><SUP><FONT face="Times New Roman CYR">
<P><FONT size=2>6 </FONT></SUP><FONT size=2>� ����� ���������������� ���������� �� �� ������� �������� �����������������, �� ������� ��������� � ���� ������ ����������� �� �������� �� �� �������.</FONT></P></FONT></FONT></FONT></FONT>
<P align=justify><FONT size=2><SUP>7</SUP> ����� ���������� ��, � ������� ������ ������ ���� �������� (������������) �������� �� ������������� ���������� �������� (������� ��, �� ������� � ����� ��������������� ����������� ��������� ����������� ������� ������ �� �� ����������) &#8211; 1513.</FONT></P><SUP>
<P><FONT size=2>8</FONT></SUP><FONT size=2> ����� 01.07.2002 � ����� ��������������� ����������� ��������� ����������� ������ � ���������� ��������� ����������� ��� ������������ ���� �������� ������ ����� ��������������� ����������� ��������� ����������� � ����� � �� ����������� �������������� �������������� �������.</FONT></P>


</body>
</html>

