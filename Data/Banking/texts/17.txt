<!--
ID:35773888
TITLE:Facing the future
SOURCE_ID:3240
SOURCE_NAME:Banker
AUTHOR:
NUMBER:951
ISSUE_DATE:2005-05-31
RECORD_DATE:2006-01-23


RBR_ID:1093
RBR_NAME:���������� ����
-->
<html>
<head>
<title>
35773888-Facing the future
</title>
</head>
<body >


<P align=justify>During the first year of refoundation and of the new single brand, Millennium bcp continued to be very determined in implementing the strategic agenda announced in 2003, based on capital discipline and on a focus on domestic retail banking in Portugal, Poland and Greece, as part of the mission that was established. This determination has made an unquestionable contribution to the improvement of the competitive position of the bank, to the perfecting of the service of excellence provided to customers and to business, profitability, efficiency, asset-quality and solvency indicators. This took place in a framework marked by moderate recovery of economic activity, an increase of the structural challenges facing sustained development and by alterations to the regulatory and competitive framework of the financial sector.</P>
<P align=justify><STRONG>Capital discipline</STRONG></P>
<P align=justify>Measures of great importance were implemented within the scope of the first strategic pillar: capital discipline. Three major examples: the sale of the insurers held by Seguros e Pensoes, the sale by Bank Millennium of its 10% holding in the Polish insurer PZU, and the sale of the non-auto consumer credit loan portfolio sourced by third parties. Important capital management operations were also undertaken, in which the synthetic securitisation of corporate loans - innovative on the Portuguese market - stands out. These initiatives strengthened capital ratios, in keeping with the commitment assumed with the shareholders.</P>
<P align=justify>As part of the second strategic pillar, Millennium bcp continued initiatives directed at increasing the profitability of the banking operation in Portugal. Here, the focus is on the Banco Comercial Portugues refoundation project, put into motion with the introduction of the new single brand. The launch of the Millennium Programme during 2004, was designed to foster employee development and to provide customers with growing levels of innovation and advantages, leading to greater involvement with the bank and to the creation of greater shareholder value.</P>
<P align=justify><STRONG>The Millennium Programme</STRONG></P>
<P align=justify>The Millennium Programme includes a number of measures directed both at increasing commercial dynamics and at organisational development. Attention is also drawn to the performance of the new brand, which has already risen to a leading position in terms of awareness and spontaneous recall, supported by very powerful media campaigns, to the optimisation of the domestic distribution network, to the rationalisation of resources, to the adoption and harmonisation of service standards. At the same time, it is also due to the implementation of measures designed to ensure greater differentiation and adequacy of the offer, supported by the specific reality of the various customer segments.</P>
<P align=justify><STRONG>International operations</STRONG></P>
<P align=justify>International operations, the third strategic pillar, also posted remarkable improvements, as seen in the significant increase of the operating profits of Bank Millennium and, in fact, that NovaBank achieved break-even during the last quarter of 2004, thus bringing forward by one year the initial plans. These results in Poland and in Greece are indicators of the success of the international strategy, based on the construction of a multi-domestic banking operation focused on various countries of the European Union. Following the initial stages of restructuring and the launch, Banco Comercial Portugues is now in a position to benefit from busi-ness in markets that, together, have a total of more than 60 million cit-izens, with prospects considered among the most promising in a Europe enlarged to 25 members as from 2004.</P>
<P align=justify>In brief, the results achieved out- performed not only the established goals but also the expectations of Portuguese and foreign financial analysts, reflecting the success of the strategic measures adopted as from the end of 2003, such as the bank's refoundation programme and the launch of the single brand, on the one hand, and a focus on the core retail banking business, both in Portugal and in Poland and Greece, selected as strategic growth markets, on the other.</P>
<P align=justify>Reflecting its history and the steps that were taken during 2004, the bank is facing the future.</P>


</body>
</html>

