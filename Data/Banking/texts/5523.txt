<!--
ID:36799664
TITLE:�����������
SOURCE_ID:3126
SOURCE_NAME:������� ����������� ����
AUTHOR:������� �������, �������� ������������ ��������� 
NUMBER:7
ISSUE_DATE:2006-07-31
RECORD_DATE:2006-10-13


-->
<html>
<head>
<title>
36799664-�����������
</title>
</head>
<body >


<B>
<P align=justify>������������ ������������ ��������� ����� ��������� ����� ��������. </B>� 1980 ���� �������� ���������� �������� ����������� ������� (����) �� ������������� &#8220;������� ����������� �������&#8221;, � 1995 ���� &#8212; ���������� �������� ��� ������������� �� �� ������������� &#8220;��������� �� ����������� � ���������� ����&#8221;. � 1995-1996 ��. �������� ��������-����������� ������������ ������������� �����. � 1996 �� 2005 ��. ��������������� �������� ��������� ���������� ������������� ������, �������� ���������� ������������� ������, ���������� ������ ����������� ���� � ��������� ��������, ����������� �������� ���������� � �������� ���������� �� &#8220;��������-����&#8221;. � 2005 ���� ������� � ��� &#8220;�����������&#8221; �� ��������� ��������� ������������ ���������. �� ���������� ������������ ������������ ��������� �������� ������� ����������� �����.</P>


</body>
</html>

