<!--
ID:35702572
TITLE:������� ���� � ������������
SOURCE_ID:3126
SOURCE_NAME:������� ����������� ����
AUTHOR:
NUMBER:12
ISSUE_DATE:2004-12-31
RECORD_DATE:2005-12-27


-->
<html>
<head>
<title>
35702572-������� ���� � ������������
</title>
</head>
<body >


<B>
<P align=justify>26 ������ � ������������ ������ ������� ���� �� ���� &#8220;� ��������� ������ � ������� ����������� �������&#8221;. ��� �������������� ��������� <I>���������� ������������ ������ ������</I> � ��������� ���������� ������� �����.</P></B>
<P>����������� ���� ������������� 25 ������ ���������� �������, � �.�. ������������� ������������� ����, �������������, ����� &#8220;������&#8221;, &#8220;��������� ��������&#8221;, &#8220;������&#8221;, &#8220;�����&#8221;, &#8220;������������&#8221;, ���������������, �������������� ��������� ����, &#8220;��������� ���� ��������� ������&#8221;, � ����� ������������� �������� ����������� ������. �� �������� �������� ������������� ������ ������� ������, �������, ������������, �����, �������� � ������. ��� ������� ���� <B><I>������ ����-��������� ���������� �.�. ��������</B></I>. � �������������� ������ � ���������� ��������� �.�. �����������, ����������� ���������� �� �� �� �� ������������� �������. �� �������� ���� ��������� �.�. ��������, ��������� ���������� ����������� � �������������� ��������� ����������� ������������ �������������� ������������ � ����������� ������������ ��������� ����������� ����� ������, �.�. ������������, �������� ������������ ��������� ��������� �� ����������� �������, �.�. ���������, ������� ��������� ��������� �� ����������� �������. � ���� ��������� ����������� ����� ������� ���: ����������� ��������� ������ ������ �������������� ���������� � ������������ ������ ����������� � ������� � ������� ����������� �������; ������� ������� � ������ ��������� �������; ������� ������� ���������� �� �������, ������� ������� �������, ������ ������� �������� ��������� �� ����������� ������� � ��������� ������ � ��. � ���������� �������� �������� ������� ������� ������������� �� ��, ���, �������������� �������������� �����, �������������� �������������� �����, ���������� � ������ ���������.</P>


</body>
</html>

