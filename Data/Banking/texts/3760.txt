<!--
ID:37036853
TITLE:��������� � ���������� ��������� �����������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:23
ISSUE_DATE:2006-12-15
RECORD_DATE:2006-12-08


-->
<html>
<head>
<title>
37036853-��������� � ���������� ��������� �����������
</title>
</head>
<body >


<P align=justify>���������� ����������� (�����������) ������������ ������, �������� �������� �� ���������� ���������� ��������, ����������, ��� � ������������ � ��������� ������������ ����� �������� ���������� ��� ��������� ��������������� (���������) ����������� ����� � ��������� ���������������� (����������) �������� ���������� ��������� ��������� �����������:</P>
<TABLE cellSpacing=2 cellPadding=7 width="100%" border=1>
<TBODY>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>&#8470;</P>
<P align=center>�/�</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>������������</P>
<P align=center>���������</P>
<P align=center>�����������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>��������-������� �����</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P align=center>����</P>
<P align=center>������ ��������</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P align=center>�����, �������, �.�.�. ����������� ������������ (�����������).</P>
<P align=center>���� ������������ ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>953</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P>25.12.1997</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>109240, ������, �/� 30;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>�����������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>1890</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P>06.06.2006</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>123022, ������, �/� 45;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>61 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>�����</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>1041</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P>04.09.2006</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P>236029, �����������, ��. �������, 81, ��. 44;</P>
<P>���. (4112) 32-33-17,<BR>(917) 301-02-39;</P>
<P>������ �������� ��������;</P>
<P>2 ������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>3169</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P>30.08.2006</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>109240, ������, �/� 30;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>60 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>&#8470;</P>
<P align=center>�/�</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>������������</P>
<P align=center>���������</P>
<P align=center>�����������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>��������-������� �����</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P align=center>����</P>
<P align=center>������ ��������</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P align=center>�����, �������, �.�.�. ����������� ������������ (�����������).</P>
<P align=center>���� ������������ ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>5</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>������-����</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>1138</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P>10.03.2005</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>123022, ������, �/� 39;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������</P>
<P>60 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>6</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>���</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>3362</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P>30.08.2005</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P>119034, ������, �����������<BR>���., 2;</P>
<P>���������� ���������� �� ������:</P>
<P>107045, ������, �/� 4;</P>
<P>���. 8-916-114-23-54, 201-30-32;</P>
<P>���������� ������� ����������;</P>
<P>2 ������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>7</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>����������� ���� ��������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>2792</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P>14.07.2006</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>123022, ������, �/� 4;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>61 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>8</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>����-����</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>2092</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P>04.10.2006</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>123022, ������, �/� 4;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>60 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>9</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>���������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>2991</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P>23.08.2006</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>123022, ������, �/� 7;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>60 ����</FONT></P></TD></TR></TBODY></TABLE><I>
<P align=right>"������� ����� ������", 2006, &#8470; 61, 64, 65</P></I>


</body>
</html>

