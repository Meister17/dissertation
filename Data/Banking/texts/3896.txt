<!--
ID:37510454
TITLE:��������� � ���������� ��������� �����������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:7
ISSUE_DATE:2007-04-15
RECORD_DATE:2007-04-09


-->
<html>
<head>
<title>
37510454-��������� � ���������� ��������� �����������
</title>
</head>
<body >


<P align=justify>���������� ����������� (�����������) ������������ ������, �������� �������� �� ���������� ���������� ��������, ����������, ��� � ������������ � ��������� ������������ ����� �������� ���������� ��� ��������� ��������������� (���������) ����������� ����� � ��������� ���������������� (����������) �������� ���������� ��������� ��������� �����������:</P>
<TABLE cellSpacing=2 cellPadding=7 width=100% align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>&#8470;</P>
<P align=center>�/�</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>������������</P>
<P align=center>���������</P>
<P align=center>�����������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>��������-������� �����</FONT></P></TD>
<TD vAlign=top width="16%"><FONT size=3>
<P align=center>����</P>
<P align=center>������<BR>��������</FONT></P></TD>
<TD vAlign=top width="39%"><FONT size=3>
<P align=center>�����, �������, �.�.�. ����������� ������������ (�����������).</P>
<P align=center>���� ������������ ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>���-����</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>1421</FONT></P></TD>
<TD vAlign=top width="16%"><FONT size=3>
<P>15.06.2006</FONT></P></TD>
<TD vAlign=top width="39%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>109240, ������, �/� 48;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>61 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>&#8470;</P>
<P align=center>�/�</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>������������</P>
<P align=center>���������</P>
<P align=center>�����������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>��������-������� �����</FONT></P></TD>
<TD vAlign=top width="16%"><FONT size=3>
<P align=center>����</P>
<P align=center>������<BR>��������</FONT></P></TD>
<TD vAlign=top width="39%"><FONT size=3>
<P align=center>�����, �������, �.�.�. ����������� ������������ (�����������).</P>
<P align=center>���� ������������ ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>����-���������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>576</FONT></P></TD>
<TD vAlign=top width="16%"><FONT size=3>
<P>17.01.2007</FONT></P></TD>
<TD vAlign=top width="39%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>109052, ������, �/� 48;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>60 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>����-����</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>2717</FONT></P></TD>
<TD vAlign=top width="16%"><FONT size=3>
<P>26.12.2006</FONT></P></TD>
<TD vAlign=top width="39%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>109052, ������, �/� 48;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>60 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>��-����� (���)</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>3462-�</FONT></P></TD>
<TD vAlign=top width="16%"><FONT size=3>
<P>13.12.2006</FONT></P></TD>
<TD vAlign=top width="39%"><FONT size=3>
<P>107078, ������, ��. ����� ���������, 12, ���. 2�;</P>
<P>���������� ���������� �� ������:</P>
<P>107078, ������, �/� 281, �.�.�����;</P>
<P>���. (495) 783-39-90;</P>
<P>����� ��������� ���������;</P>
<P>60 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>5</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>������� ������������ ����</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>2541</FONT></P></TD>
<TD vAlign=top width="16%"><FONT size=3>
<P>15.06.2001</FONT></P></TD>
<TD vAlign=top width="39%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>109240, ������, �/� 48;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>6</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>�����</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>209</FONT></P></TD>
<TD vAlign=top width="16%"><FONT size=3>
<P>09.08.2006</FONT></P></TD>
<TD vAlign=top width="39%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>109052, ������, �/� 48;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>61 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>7</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>�������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>2575</FONT></P></TD>
<TD vAlign=top width="16%"><FONT size=3>
<P>26.12.2006</FONT></P></TD>
<TD vAlign=top width="39%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>109052, ������, �/� 48;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>60 ����</FONT></P></TD></TR></TBODY></TABLE><I>
<P align=right>"������� ����� ������", 2007, &#8470; 13&#8211;15, 16</P></I>


</body>
</html>

