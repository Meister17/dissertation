<!--
ID:35168045
TITLE:������� ������ �� ������������ �������
SOURCE_ID:3187
SOURCE_NAME:����������� ���������� � �������������� ���������� � ������������ �����
AUTHOR:�. ��������, �����-����, ��������� ������ ���������� ������������� �������, �.�.�. �. ��������, �����-����, ����-��������
NUMBER:4
ISSUE_DATE:2005-08-31
RECORD_DATE:2005-08-15


RBR_ID:4932
RBR_NAME:������������� ���� � ������
RBR_ID:4933
RBR_NAME:������ ����������� ��������� ������
-->
<html>
<head>
<title>
35168045-������� ������ �� ������������ �������
</title>
</head>
<body >


<FONT face="Times New Roman"><I><B>
<P align=justify>������������� ������� ������ �� ������������ ������� �������� ����������� ��� ������� ���������� ������ �� �������. ���������� ����� ������� ������, ������� �� ��������� � ������������� ����� ��� ����������� �������������� ������ ������������ ������ �����. � ���������� ������<SUP>1</SUP> ��������������� ������� ����� � ������� ������ �� ������������ �������, ������� ���������� ���������� ������. � ������ ���������� ��������������� ��� ���������� ������� ������ �� ������������ �������, �� ���� ������ �� ������� ������ ������, ��� ���������� ����������� &#8220;���������� ������&#8221;.</P></B></I>
<P></P><B>��������</B> 
<P align=justify>� ��������� ����� ���������� ���������� ������������� ������� ������: � ���������� ����������� ���������� �������, ����������� � ������������ ������<SUP>2</SUP>. ���������� ������� �� ����������� ������� ����������� 2-� ���������� �� ������������� ��������, � �������, � ���������, ��������������� ���������� �� ��������� �������� �� ������������ �����. � �������� &#8220;�����������&#8221; ��������, ������������ �������, ��� ��������������� ������� � ������ ������������ ������������� ������, ������� ���������� �� ��������� � ���������� �������������� ������, ����������� ��� ��������. �������� �� ��, ��� �� ��������� ���������� ������ � ������ �������� ��� ��������� ���, ���� ������������ ���������� ������ �� ������������ ������ &#8212; ������ ��������� ������, ��� ��� ���� �� ������� ������� � ���, ��� ������� ������� ������ ������� ����� �������� ����� �����, � ��������� ���������� ������� ����������� ������������� ��� ����������� �������������� ��������. �������� �������� ������ � ��������������� ���������� ������ ����� ����� ������ � ������� ���������� ������� ������.</P><B>
<P>������� ������</P></B>
<P align=justify>������� ������ &#8212; ��� ������ �� ������������ ������� ������ ������ � ���������� ����������, ������� ������������� ������ ������ � ����������� ������� (�������� ������ �����).</P>
<P align=justify>���������� ������������� ������ �� ������������ ������� �������� 8 ����� ������� � 7 ������-������ (���� ��������� ����������� ������� � ����� �� ������������, �� ��������� ���������� ��������� ������). � ������ ��� ���� 56 ��������� ����� ������� ����������� ���������� �������������� ������ ��� ������������� ������������� � �������� VaR, ��� �� �������� ����������������. ��������� ��������� ����������� ���������� ������, � �������� ������� ���������, ������ ��������� ���� ����� ��������� ��� ����� ������ ����� ������ �������� ����� ��������� �������. ������� ������ ��� �������� ������� �� ����� ���������� ��������� (��������, ������������� ��� ������� � ������� ������-������ ��� ��������� �� ���� �������) ����� �������� ����������, ��� ��� ����� ����������� ����������� �������, ��������� � ���������� ����� ������. �������� ������ �������� ��������� �������� ��� ��������� ����������� �����������, � ���������� ������� ������ ������ �������� ���������� ���������� ����������� ������.</P><B>
<P>���� ������� ������</P></B>
<P align=justify>������� ������ ����� ��������� �� ��������� ����� �� ���� ���������. ���� ������� �������� �� ���.</P>
<P align=justify>1. <B>���� ������, ������� ���������� ����� ����������� ������ </B>(�.�. �������� ����������).</P>
<P align=justify>����� ���� ������, ����������, �����������:</P>
<UL>
<LI>����������� �������; 
<LI>��������, � ������� �������� ��� ����������������; 
<LI>�������, ������� � �������� ���������������� �������.</LI></UL>
<P align=justify>��� �� �����, �������� ������� (� �����������) ������� ����������� �� ���� �� ��� �����������.</P>
<P align=justify>��� ������� ������, ��� � ��� ����������, ����� �������� ����������:</P>
<UL>
<LI>���� ������� ������� ������������ ��������, �� ���������� ������ ������� ������������� �������; 
<LI>���� ����������� ���� �������������, �� ���������� ������� ����, ��� ������������ ������� �� ����� ������ � ������-������ � �.�.</LI></UL>
<P align=justify>�������, ������������ �� �������� ������, �������� �������� �������, �� ����������� ��� ������ � ������/���, �������� ������, ���������� ����������� �� ������������ ������, ����������� ���������� ������ ����������� �� ������� ���� � �.�.</P>
<P align=justify>��� ���� ������ � ������� ���� ������ �� �������� ���������� &#8212; ��� ����� ������� ������ �������� ������ ������ (����� �������� � &#8220;�������&#8221;) �� ������������ ������� ������ ������.</P>
<P align=justify>��������� �������� ������ � ������� �� �������� ���������� � ������� � �������, �� ������ ������� ����� ���������� � ������������ �� ����������� ������. ����������, ������ �� ��� ������������ ������ �������� ���������� �������� � ������ �� ��� ��������� ������ ���������� �������� ������. � ������ ����������� ����������� ������<SUP>3</SUP> ���� ��� ������������ ��������������, � �������� ����� ����������� ������ ����������� �������� � ��������� ���������, ������������. ������� ��������� ������ ������ ���� �������� ������������� � �������, ������� ����� �����. �� ����� ��������������� �����������, ��������� ������, �� ������� ��������� ������������� ��������� �������� (�� ������� ������, ��, �������� ���� ������), ����� ������������ � �����, ��������� �� �������� ����������, ��� ��� ��� ��������� �� ������������� �������� ���������� ���� ������.</P>
<P align=justify>������ ��������, ��������� ������ �� ������� ������������ �������, ������ �� �������� ����������, ���������� ����� ����������� ����������� ��� �������, ��� ���� ������, ��� � ����������� ������ �����.</P>
<P align=justify>2. <B>���� ������, ��������� ������������ �������������� �������� </B>(Operational Risk Exchange (ORX), Global Operational Loss Data (GOLD), MORE (Multinational Operational Risk Exchange) � ��.) �� ���������� ������ &#8212; ���������� ���� ������. ����������� ������ �������� ������� ������������� ����� � ���������� ���������.</P>
<P align=justify>��� ����������� ������� ��� ���������� � ��� � ���������� ������, �� ��������� ������� ����������� ������� �������, �������� �������� ������������� ������� ������������� ������������� ������� ������ ��� ������������ ���������� ����������� ������ � ��� �����������, ��� ��� ������ �����������.</P><B>
<P>�������� ������� ������ <SUP>4</P></B></SUP>
<P align=justify>�������� ���������� ������� ������ � �������� �� ������������� �� ������ ��� ���� ������� ���� �����<SUP>5</SUP> ��� ��������� � ���������� ���������� ������. � ��� ���������:</P>
<UL>
<LI>������������ �������������; 
<LI>���������� ����� ������ ��� �������� ����� (������ ����������); 
<LI>������� �������� ������� ����� ������ (� �������� ������ � �������� ����� 2000 �.); 
<LI>��������� �����������; 
<LI>��������������� �������� ������������� � ������ �������; 
<LI>������� ������� ��������� (������ �� $1 ���); 
<LI>������� ����������� ����������� �������, ��� ������ �������; 
<LI>���������/������������� ��������� ������ ������ ������; 
<LI>���������� ������ � �������������� ����������� (exposure indicators), ����������� �������������� �������� ������ �� ��������� � ������ ������.</LI></UL>
<P align=justify>������ ���������� ����� �������� � ����, ��� ������ ������ ��� ������� ������� ����� ������ ����������, �������������� �� ���� � ������, �� � ����������� �������.</P><B>
<P>������� ������</P></B>
<P align=justify>����������� ����������� �� ��������, ��������� ���� ������ �� ������������ �������, ������������� � ������ ������ ��� ���������� ������, ��� ��� ������� ��� � �������� �� ������� �������� ������ � ����� �������� �� ������ ���������� ���������, �� � �������� ������ �������� ���������. �������, �� ������� ����� ������ ������ ��������� � ���������� ������, ��������: �������� ����� ���������� ������� ������, �� �������, �������� ����� ����� � ��������� ���������, ������ �������� ������. ���� ����� ���� ������ �������� ��������� ����� �������, �� ����� ������ �� �������� &#8220;������ �����, ��� ��� ��������� ������, �������� ���������� �����&#8221; � ������� ��������� ��������� ��������-����� �������.</P>
<P align=justify>���� ��������� ������� �������, �������� � ���� �� ������������ �������, ��������� �� �������� ����������.</P>
<P align=justify>���������� �������� ������� (�� ����� ������ &#8212; � ������������� ����������� �������� �� ����������� �������).</P>
<P><STRONG>���������� �������������</STRONG></P>
<P align=justify>������ �������� ���������-&#8220;���������&#8221; �� ���������� ����-����������� ��� ������ ���-����������. 1982 ���, ������� ������������� ����, ��������� � ���, ����� &#8212; $6,5 � 18 ���.</P>
<P align=justify>�������� �������� ������� �� ������, ������� ���� ����������� ������ �������. � ���������� ��������������� �������� �� ����� ������ ����� ��������� ���������. 1994 ���, ������� ������������� ����, ��������� � ��������������, ����� &#8212; ����� &#8364;9,3 ���.</P>
<P><STRONG>������� �������������</STRONG></P>
<P align=justify>����� ��������� �� ���������� �����. 1983 ���, ����������� ��������� ����, ����� &#8212; ����� $7 ���.</P>
<P><STRONG>��������� ��������� ����������������, �������� �����</STRONG></P>
<P align=justify>������������ ���� ���������� ��������� ����������� �����������, ����� ������� ��� � ������������� ������ ������������ ������. 2001 ���, ������� &#8212; ����� $22 ���.</P><B>
<P>�������� �����, ������� �������� � ��������� � ���������</P></B>
<P align=justify>�� ������� ���������� ������������� ���� �������� ����� �� ���������� ������������� ������������� ������������ �����. ���� �������� ������������� ���������� ����������� ������������� ������ � �������� ������������� ������������� �� �������������, ����������� ������. 2001 ���, ����� &#8212; $200 ���.</P><B>
<P>���������� �������� � ���������� ���������</P></B>
<P align=justify>����������� ���� ���������� ����������� �� ��������� � ��������� ������ �� ���������� ������. ����� �������� �� ����� ����������� ����������� ����� 30 ����� �������� ����� �������� ����� ����� ������� ��-�� ��������� �������� ��������� �����. ����� �������� &#8364;750 �����.</P><B>
<P>����� ������������ �������</P></B>
<P align=justify>� ���������� ������ ������ ���������� ������ ������������� �����. ������� ������������� ������ ��������� ���������� � ������������ �����������. ����� &#8212; ����� $1 ���.</P><B>
<P>���� ������ � ���������� ������������</P></B>
<P align=justify>���� ����������� � ������� ��������� ��������� �����. �������� �� ����������� ������ �������� � �� ���������� ������ ���� ���������� ����� 4 �����. ������ ������ �� ����������.</P>
<P align=justify>������� �� �������� 1, 2 ������������� ������� ������� ������ �� ������������ �������, ��������������� ����� �� ��������� ��������.</P>
<P align=justify>������ �� ������� 2 ���� ������������� � ���, ��� ������� ������ �������. ���������� 28 ������� ���������� 90% �� ����� ����� ������ �� ���� �������. ���� ��������� ����� �������� � ����������� ����������� ��� ���������� ������ (������ � ���������� ������ &#8220;������������&#8221; ��� ����).</P>
<P align=justify></P>
<P align=center>��� 1. ���������� ������������������ ������� �� ��� � ����� ����� ������ �� �������� �� ��� <SUP>6</P></SUP>
<P>������� ����� ������ &#8212; &#8364;111 ��� (������� � ����������� ������ ����������� ��� &#8364;0).<BR>������� �����, �������� ������� � ����������� ������, &#8212; &#8364;134 ���.<BR>������������ ����� &#8212; &#8364;6,2 ����.<BR>������� &#8212; &#8364;5,5 ���.</P>
<P></P>
<P align=right>�������</P>
<DIR>
<DIR><B>
<P>����� (&#8364; ����) � ���������� ������� �� ������-������ � ����� ������<SUP>7</P></B></SUP></DIR></DIR></FONT>
<TABLE cellSpacing=1 cellPadding=2 width=551 border=1>
<TBODY>
<TR>
<TD vAlign=top width="16%" bgColor=#ffffff height=87>
<DIR><FONT face="Times New Roman">
<P>������-����</P></DIR></FONT></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=87><FONT face="Times New Roman">
<P align=center>���������� �������������</FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=87><FONT face="Times New Roman">
<P align=center>������� �������������</FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=87><FONT face="Times New Roman">
<P align=center>��������� ��������� ����������������</FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=87><FONT face="Times New Roman">
<P align=center>�������� �����, ���������</FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=87><FONT face="Times New Roman">
<P align=center>����� ������������ �������</FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=87><FONT face="Times New Roman">
<P align=center>���� ������ � ���������� ������</FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=87><FONT face="Times New Roman">
<P align=center>���������� ��������, ���������� ���������</FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=87><FONT face="Times New Roman">
<P align=center>����� �� ������-�����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="16%" bgColor=#ffffff height=17 rowSpan=2><B><FONT face="Times New Roman">
<P>������������</P>
<P>����</B></FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>2,19</B></FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>1,96</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17>
<P></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>0,10</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17>
<P></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17>
<P></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17>
<P></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>4,25</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>22</B></FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>16</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17>
<P></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>4</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17>
<P></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17>
<P></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17>
<P></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>42</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="16%" bgColor=#ffffff height=17 rowSpan=2><B><FONT face="Times New Roman">
<P>�������������� ������</B></FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>0,99</B></FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>0,00</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>0,02</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>1,01</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17>
<P></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17>
<P></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>0,01</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>2,04</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>7</B></FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><FONT face="Times New Roman">
<P align=center>8</FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><FONT face="Times New Roman">
<P align=center>7</FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>8</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><FONT face="Times New Roman
">
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><FONT face="Times New Roman">
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><FONT face="Times New Roman">
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>37</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="16%" bgColor=#ffffff height=17 rowSpan=2><B><FONT face="Times New Roman">
<P>�����������-</P>
<P>��� ����</B></FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>3,87</B></FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><FONT face="Times New Roman">
<P align=center>0,22</FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>0,05</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>0,63</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17>
<P></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17>
<P></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>0,01</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><FONT face="Times New Roman">
<P align=center>4,78</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>39</B></FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><FONT face="Times New Roman">
<P align=center>5</FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>10</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>21</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17>
<P></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17>
<P></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><FONT face="Times New Roman">
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>79</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="16%" bgColor=#ffffff height=17 rowSpan=2><B><FONT face="Times New Roman">
<P>���������</P>
<P>����</B></FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>5,04</B></FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>1,88</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><FONT face="Times New Roman">
<P align=center>1,51</FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>8,97</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>0,01</B></FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17>
<P></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>0,00</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><FONT face="Times New Roman">
<P align=center>17,41</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>32</B></FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>32</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><FONT face="Times New Roman">
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>27</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><FONT face="Times New Roman">
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><FONT face="Times New Roman">
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><FONT face="Times New Roman">
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>98</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="16%" bgColor=#ffffff height=17 rowSpan=2><B><FONT face="Times New Roman">
<P>����� �� ����</P>
<P>������</B></FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>12,09</B></FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>4,06</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>1,59</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>10,71</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>0,01</B></FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=17><B><FONT face="Times New Roman">
<P align=center>0,00</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><FONT face="Times New Roman">
<P align=center>0,03</FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=17><FONT face="Times New Roman">
<P align=center>28,48</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%" bgColor=#ffffff height=18><B><FONT face="Times New Roman">
<P align=center>100</B></FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=18><B><FONT face="Times New Roman">
<P align=center>61</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=18><B><FONT face="Times New Roman">
<P align=center>19</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=18><B><FONT face="Times New Roman">
<P align=center>60</B></FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=18><FONT face="Times New Roman">
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="10%" bgColor=#ffffff height=18><FONT face="Times New Roman">
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=18><FONT face="Times New Roman">
<P align=center>8</FONT></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff height=18><B><FONT face="Times New Roman">
<P align=center>256</B></FONT></P></TD></TR></TBODY></TABLE><FONT face="Times New Roman">
<P align=justify>� ������� ���������� ���������� � ����� (� &#8364;���� &#8212; ����� ������) � ���������� (� ������ &#8212; ����� ������) ������� � ������� ������-������ � ����� ������ (� ������������ � �������������� ����������� ��������).</P>
<P align=justify>���������, ��� � �� ����� ������, � �� ���������� &#8220;��������&#8221; ��������� ������-����. ��� � �����-�� ������� ����� ���� ����������� ����������� ����������, �� ������� ������������ �������, �� � � ������, ��������� ���������� ��������� (��. [3]), �� �� �������: ��������� ���� ��������� ������ � �� �����, � �� ���������� �������. ����� �������, ��� �������� ������ ��� ��������� �������� ������������ ������ (29% �� ����� ����� �� ������ [3]) � ������� �� ����������� ���������� ������, ��� ������� ������� � ��������� ������ ������, ��� ������ �����.</P>
<P align=justify>���������� �������������� ������ �� ������ ������-������ � �� ����� ������: �� ����� ���������� ����� ���� ��� �� ������� � ���������� ������, ������ �� &#8220;�����&#8221; � ����� ������ ������ ����������� ���, ��������, �������� ���������. ��������� ���������� � ���������� ������� ��������� ���� ����� ������ �������� ����� ���������� ������ (���� ����� ������� ����������� ������, �� �������� ����� ���� ������������� �������� ����� ������ ���� ������� ��������� ������� �����). ����������, ��� ����� ��������� ������ �������� � ��������� �� ������ � ������ �������������� �������.</P><B>
<P>��������� ����������</P></B>
<P align=justify>1. Basel Committee on Banking Supervision. The New Basel Capital Accord //Bank tor International Settlements, April 2003 (http:// www.bis.org/bcbs/cp3tull.pdf).</P>
<OL start=2>
<LI>Basel Committee on Banking Supervision. Working Paper on Regulatory Treatment of Operational Risk //Bank for International Settlements, September 2001 (http:// www.bis.org/publ/bcbs_wp8.pdf). 
<LI>Basel Committee on Banking Supervision. The Quantitative Impact Study for Operational Risk: Overview of Individual Loss Data and Lessons Learned //Bank for International Settlements, January 2002 (http://www.bis.org/ bcbs/qis/ldce2002.pdf). 
<LI>M.R.A. Bakker. Quantifying Operational Risks within Banks According to Basel II //Delft Institute of Applied Mathematics in cooperation with PricewaterhouseCoopers, September 2004.<I> 
<LI>Haas M., Kaiser T. </I>Tackling the Insufficiency of Loss Data for the Quantification of Operational Risk//University of Frankfurt; KPMG, August 2002 (http://195.37.61.1 4:81/preprints/db/ preprint_data/cae_pp_0024_haas_2002-08- 04.pdf).<I> 
<LI>Haas M. </I>Loss Data: a Look at the Current Situation //University of Frankfurt, 2004 (http:// www.ifk-cfs.de/papers/Haas.pdf).<I> 
<LI>Kaiser T. </I>Statistical Methods for Measuring Operational Risk //KPMG Deutsche Treuhand- Gesellschaft, May 2002. </LI></OL><I>
<DIR>
<DIR></I></DIR></DIR>
<P style="MARGIN-RIGHT: 0px"><SUP>1 </SUP>��.: 6/2004, �. 100-110.</P>
<P style="MARGIN-RIGHT: 0px"><SUP>2 </SUP>����������� ������������ ������, �������� ���������� ��������� �� ����������� �������: ���� ������ ���������� ������ � ������ ������-���������, ���������, �������������� ������ ��� ������� �������.</P>
<P></P><SUP>3</SUP> Bankers Blanket Bond (BBB).<SUP> 
<P></P>4</SUP> �� ������������� ��������, ������������� ������� ������ �� �������� �� ���������������, �.�. ������������� �� ������������� ��� ���������� ���������� ������ ������������ ��������������, � ��� ����� � ���������� ����������. <SUP>
<P>5</SUP> ��.: 6/2004, �. 100-110.</P>
<P align=justify>
<P><SUP>6</SUP> ��� ���� � ������� ���� ��������� �������, �� ������� ����� �� ������� (� ������, ���� �� ���������� ���� ������� ��� �� ������ ����������� ������� �� ���� �������� ������ ����� � �.�.).</P>
<P align=justify>
<P><SUP>7</SUP> ��� ��������������� �������� ��������������� �������, ������� ������� � ������������� ���� ������� ����������. � �������, ��� ���������� ���������� �������, �� �� ���������� ����� ������, ������ � ������� ������ �� ����. � ������� �� ��������� �������, ���������� � ��������� ����������: Corporate Finance, Trading &amp; Sales, Payment &amp; Settlements, Agency Services, Asset Management, Retail Brokerage.</P></FONT>


</body>
</html>

