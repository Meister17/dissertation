<!--
ID:37437143
TITLE:������, ��� ����������!
SOURCE_ID:11517
SOURCE_NAME:���
AUTHOR:�������� ���������
NUMBER:4
ISSUE_DATE:2007-04-30
RECORD_DATE:2007-03-22


-->
<html>
<head>
<title>
37437143-������, ��� ����������!
</title>
</head>
<body >


<P align=justify><STRONG>������������ �� �������������� ��������� ������������ ������������ ���� ������ �������� �����. ������ ����������� ���������� �������� ��-�������� ������������ �� ������ �� ��������� ��������</STRONG></P>
<P align=justify>�� ��������� ������� �������� ������� ���� �������������� ����� ������������� ��������� ��������. ���� � 2005 ����, �� ������ ������������ ��������� "������� ��", ���������� ����������� �������������� ���������������� �������� ������� ������������ ������� �������� ������� ��������, �������� 11,8%. �� ������ ����������, ��������� �������� ��������� �����-����� ������� ��������� ������ ���������� � ������ ������� ��������� ����������� �������������� ������������. ��� ����� ��������� ��� ����������� �����������, ������� ����� �� ������� ������ � ������ � ������� �� ����� ���������� ���������� ����, ��������������� �� ������� �� �������������� ��������.</P>
<P align=justify><STRONG>��������� ���������</STRONG></P>
<P align=justify>��������, ��� ����������� ��������� ������������ � ����� ������ ����������� ����, ��� �� ������. "� ���� ��� ������ �������������: ������ ����������� ������ ��������� �����", - ������� ����������� ������������ ��������� ��������� ������ "���������-������" ��������� ��������. �� ����������� �� ������� �� ����� ����� ��������� �������. ������ �����������, � ������� "����������", �������, ��� �������, ����� ����� ���� ������������ ������ �� ��������� ���������, ������. ����������� ������������ "�����������" ���������� ������� �������� � �������� "���": "��� ����� ������ �� ����� �������������, ��� ������". ��, �������, ��� ����� �� ���������, �� ��� �� ����� ��, ��� ��� ������ ���������� ����������� ����������� ���������� ���������, � ������� ����� �������� ��������� �����������. �� � � �����������, ������� ��� ��������� �������� ������ �����������, ����� ���������� ��������, ���� �������� ����� ���������� ������������� ���������� �������������� ������������.</P>
<P align=justify>�� ������� �������������� - ��� ���������. ������� �������� �� ������������ ������ ����� �� ��������. �� ������� �������� "����", ������������� ����� ���������� �� �� ��������, ������� ����� ���� ����������������, -� ������� 7-10%. ������ � ����������� ������������ ���������� �� ���������� ���� �� ������� �������. ��������, ������� ����� �� ������ ���������� �������� ������� ���������� - �� ���� ���� ������ ������ �� ����.</P>
<P align=justify>����������� �������� ����� ����� ������� ������������, ��� ��� ��� �������� ������� - ����������� ������������ ��������. "�� �� ����� ���� ��������� ��������� �������� ��������, ��������� �������� ������", - ������ �� "���". �������� ����� �������� ��������� �������� ����� ��������� � ��������������� �, ��������������, ������������� ���������� ������������ (�� ��������� � ���������� �������� ���������� 55% ����������). ��� �������� ����������� ������������ ��������� �������� "������ ����� �����" ������ ��������, � ����� �������� �� ���������� ��������, ��������� ��������� �� ��������� ����� ���� ������ �������������. �� ���� ���������� - ������ ������, ��� ��� � ���� ���� ����������� �� "������ ����� ���������� ��������", ��� � ��������� �������� �������� �����, �������� �������, �� ��������� �-�� ���������, "��� ����� ��������������, ��� � ����������".</P>
<P align=justify>��������������� �������������� ��������� �������������� ������ "��������� �����������". ��������, � �� �������� �� ������ 2005 ���� ������������� ������� ����� ����������������� ����� ��������� ��������� - ������ �� ����� ��������� �����. "� ���� �������� ������ ���������, - �������� �������� ������������ ��������� ���������� ���������� ��������� "������� ��" ����� ������. - ����� ���� �� ��� ����, ��� �� �������� �������� ����� ������ ������������ �������� �� �� ���-���������� � ��� ���� �������������� � ����� ���������� ��������". �� ����� 2005-�� � ��������� ������� "��������� �����������" 30% �������� ����� - ��� ����� ����, ��� ��������� �������, ���������� � ���� ������� ������. ������������� �������� ������ ���������� ������, �, �������� �������� �-� ������, �� ������� �� ��� � 2006-� ������� ��������� ������� ��������� ��������� �����, ������� ����������� ��������� �������� ������ �� ��������. "� ����� �� �������� �������� ������������", -���������������� "���" ���������� ������ � ������� ���� ����������� ������������ ��������� "��������� �����������" ������ �������.</P>
<P align=justify>��������� ����������� ��������� ����� ����� ����� �������� ����� � "�����������". � "������������" ������������ ���� ������� ���������� �������� � �������� ������ ������. "���������� �� ���������� � ������ ������ ������������ ������� � �������� ������� ���� �������� �������, ����������� ��� ���������� ��������� ��������", -�������� "���" ������������ ���������-��������������� ������������ �������� ������� �������. � ��������� � �������� ��������, ��� �����, ���� ���� ���� ������. � �������������� ������������ "�����������" ������ � ����������� ���������� �� ��������� ���� ������ �������� � ����� � ������������. ������� ���������� ��������� ������� "�����" � "��������" ���������� ���������� � ������ ������ ����������� "��������" � �������. ����� ���������� � ������������ ��������� ����� ������������ ������ ������� ����� "����-��������". � �������� �������� ����� ��� ��������, � ������������ ���� ���������� �� ������������� ������ ������ (����� � ���������) � �������. �� ������������� ���� ���������� ���������� "���" � ��������� ����� �������������� ������������.</P>
<P align=justify><STRONG>���� � �����</STRONG></P>
<P align=justify>����� ��� �������� � ������ ���������� ���������� ����� ���� ������� �� ������ � ���, �� ��� ������������ �����������, �� � � ���, ��� ��� ��� ������. �������� ������������� ��������� "������� ��", ����������� ��������� �������� ������������ ���������� �� ���������� ������ ����������, ������ ���������, � ��������� "�����������" � "����������", ���������� ������������ ��, � �� ������� ������ ������ 5-10% �������. � ����� ���������� ����������. "����� ��������� ���������� ������� �����������, -������� ������ ���������� �������. - ��������, ���� �� ������ ������� � ���, ��� ����������� �������� �������������� ��������� ���������, ������������ ���� ������ �� ���������� ���������� ������������� (������). �� ������ ������, �������� ������ ����� �������������� � �������� ��, �� ������ 2006 ���� ���������� �� ��������� �������������� ���������� ����������� ������� �� ���������� �� ��������� � ������ ����� ��� ����������� ���� �� �������� ��� ������ ��������� �������".</P>
<P align=justify>�� ������ �-�� ��������, �������� � ���������� ����������� ��������� �� ���������� �������� - ���������� ������� ������������ � ��������� ������ ��� ������� ��� ����������� ������������ ��������. �� ��� ������� ��������, �������� ���������� ��������� ���������, ��������� ����������� �� - ������ �������. ���� �� ������ ����������� - ���, ��� �������, ���� ���������� ����������� �����, ������� ������������� � �������� �����. ���������� ����������� ������������� ���������� �� �������� ������ ���, ��� �������� �������� ��� �������������� � ��� ������� � ���������� �� ����������� �����, ��� ����������� �����. "���� ���� ����������� �������������� ���������� ����������� ��������, �������� �������, ����� � �� ����� ���� �������", - ���������� ����������� ������������ ��������� �� ��������� � �������� �������� "����" ^^^^ ������ �����. "� ��� ��� ������� �����, ��� ������ �������� ��� �������, - ������ ��� ��������� ��������. - �� � ��������� ����������� ����� �������� ���������".</P>
<P align=justify>������� ����������� �����, � ������� � ������ ��������� ������� �� ������� ����������, �������� ������ �������� ������������ �����. � ����� ������ �� ���� ��� ����������� ���������� ����� 1%. �������� ������� ����� ������ �� ���������, ���� ����������� ������� ����� ������: ���������� ��������, ������������� ������ �������� ������, ��������� � ������������ �����������, ������� ������� ��������. ������, ��� ��� ������, ����� ������ �������� � �������� �� ����������� - �� ��� �������� ��� ������, ������ ��� ��� ������� ������. "������������ ����������� ����� �� ����� ���� ����� ��������, ��� �������, ��� ������ �������!" - ����������� ����� ������.</P>
<P align=justify>������� � ���������� ����������� ����� ������������� ��������� ������ ��� ������������: ��� ����������� �������� ������� �� ����������, ��������� ��� ������� �����; ��� ������� �����, ��������� ��� ����� ����������� �����; � ����� ���, ��������� ��� ��������� ��������; ��� ��������� ��������, ��������� ��� ����������� �������� ������� �� ����������. ���� �����������, ����� ���������� ����������� ���������� ������� ���������� �����������. ���� ������������� ����������� ����� - ��� ����� �������, ������� ������������� �������� �������������� ������������ ��������, ������ ��� �� ��� ����� ������ ����������.</P>
<P align=center><STRONG>��������������� ����������</STRONG></P>
<TABLE cellSpacing=1 cellPadding=7 width=100% align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="40%">
<P>��������</P></TD>
<TD vAlign=top width="20%">
<P>����������, ���� ���.</P></TD>
<TD vAlign=top width="20%">
<P>����� �� ����������, ���� ���.</P></TD>
<TD vAlign=top width="19%">
<P>����������, %</P></TD></TR>
<TR>
<TD vAlign=top width="40%">
<P>�� "��������"</P></TD>
<TD vAlign=top width="20%">
<P>36,9</P></TD>
<TD vAlign=top width="20%">
<P>5,7</P></TD>
<TD vAlign=top width="19%">
<P>15,4</P></TD></TR>
<TR>
<TD vAlign=top width="40%">
<P>" ����� �����������"</P></TD>
<TD vAlign=top width="20%">
<P>2,9</P></TD>
<TD vAlign=top width="20%">
<P>5,5</P></TD>
<TD vAlign=top width="19%">
<P>12,4</P></TD></TR>
<TR>
<TD vAlign=top width="40%">
<P>�����</P></TD>
<TD vAlign=top width="20%">
<P>14,5</P></TD>
<TD vAlign=top width="20%">
<P>1,8</P></TD>
<TD vAlign=top width="19%">
<P>12,4</P></TD></TR>
<TR>
<TD vAlign=top width="40%">
<P>"����������"*</P></TD>
<TD vAlign=top width="20%">
<P>23</P></TD>
<TD vAlign=top width="20%">
<P>1,6</P></TD>
<TD vAlign=top width="19%">
<P>7,0</P></TD></TR>
<TR>
<TD vAlign=top width="40%">
<P>"����-��������"</P></TD>
<TD vAlign=top width="20%">
<P>7,2</P></TD>
<TD vAlign=top width="20%">
<P>0,5</P></TD>
<TD vAlign=top width="19%">
<P>6,9</P></TD></TR>
<TR>
<TD vAlign=top width="40%">
<P>������-��������� ��������*</P></TD>
<TD vAlign=top width="20%">
<P>4,6</P></TD>
<TD vAlign=top width="20%">
<P>0,3</P></TD>
<TD vAlign=top width="19%">
<P>6,5</P></TD></TR>
<TR>
<TD vAlign=top width="40%">
<P>�����</P></TD>
<TD vAlign=top width="20%">
<P>10,5</P></TD>
<TD vAlign=top width="20%">
<P>0,6</P></TD>
<TD vAlign=top width="19%">
<P>5,7</P></TD></TR>
<TR>
<TD vAlign=top width="40%">
<P>"��������� �����������"</P></TD>
<TD vAlign=top width="20%">
<P>2</P></TD>
<TD vAlign=top width="20%">
<P>0,1</P></TD>
<TD vAlign=top width="19%">
<P>5,0</P></TD></TR>
<TR>
<TD vAlign=top width="40%">
<P>"������������"</P></TD>
<TD vAlign=top width="20%">
<P>2,1</P></TD>
<TD vAlign=top width="20%">
<P>0,1</P></TD>
<TD vAlign=top width="19%">
<P>4,8</P></TD></TR>
<TR>
<TD vAlign=top width="40%">
<P>"����"</P></TD>
<TD vAlign=top width="20%">
<P>12,9</P></TD>
<TD vAlign=top width="20%">
<P>0,5</P></TD>
<TD vAlign=top width="19%">
<P>3,9</P></TD></TR>
<TR>
<TD vAlign=top width="40%">
<P>�� "��������"*</P></TD>
<TD vAlign=top width="20%">
<P>2,6</P></TD>
<TD vAlign=top width="20%">
<P>0,1</P></TD>
<TD vAlign=top width="19%">
<P>3,8</P></TD></TR>
<TR>
<TD vAlign=top width="40%">
<P>"�����������"</P></TD>
<TD vAlign=top width="20%">
<P>30,8</P></TD>
<TD vAlign=top width="20%">
<P>1,1</P></TD>
<TD vAlign=top width="19%">
<P>3,6</P></TD></TR></TBODY></TABLE>
<P align=center><STRONG>����� ������ ���������</STRONG></P>
<P align=center>���� � �������������� ��������</P>
<TABLE cellSpacing=1 cellPadding=7 width=100% align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="23%">
<P align=justify>��������</P></TD>
<TD vAlign=top width="22%">
<P align=justify>��������������� � ������������� ������ �����, %</P></TD>
<TD vAlign=top width="19%">
<P align=justify>�����, ���� � ������������� ���������,%</P></TD>
<TD vAlign=top width="15%">
<P align=justify>���������� ���������, %</P></TD>
<TD vAlign=top width="22%">
<P align=justify>��� ��������� (�������)</P></TD></TR>
<TR>
<TD vAlign=top width="23%">
<P align=justify>�����</P></TD>
<TD vAlign=top width="22%">
<P align=justify>32,2</P></TD>
<TD vAlign=top width="19%">
<P align=justify>6,6</P></TD>
<TD vAlign=top width="15%">
<P align=justify>21,5</P></TD>
<TD vAlign=top width="22%">
<P align=justify>��������������</P></TD></TR>
<TR>
<TD vAlign=top width="23%">
<P align=justify>"����������"</P></TD>
<TD vAlign=top width="22%">
<P align=justify>5,4</P></TD>
<TD vAlign=top width="19%">
<P align=justify>17,9</P></TD>
<TD vAlign=top width="15%">
<P align=justify>57,4</P></TD>
<TD vAlign=top width="22%">
<P align=justify>���������</P></TD></TR>
<TR>
<TD vAlign=top width="23%">
<P align=justify>"����-��������"</P></TD>
<TD vAlign=top width="22%">
<P align=justify>0,0</P></TD>
<TD vAlign=top width="19%">
<P align=justify>21,2</P></TD>
<TD vAlign=top width="15%">
<P align=justify>24,7</P></TD>
<TD vAlign=top width="22%">
<P align=justify>�����������</P></TD></TR></TBODY></TABLE>
<P align=center>***</P>
<P align=justify><STRONG>��� ������?</STRONG></P>
<P align=justify>� 2006 ���� 19 ��������� �������� �������� �������� ��-�� ���������� ��������������. ������������ ����������� ������ ���������� ������� ���� �������-��������, ������� �� ������ "���" � ���, �� ������ �� ����� �������� � ��������� �������������� ������������� ���� ��������, �� ���� ������� �� �����, ������� ������������� �� ������� �������� �������� ����� ��������� ��������. ��� �� ����� ������ ��������� ����� ��������, ��� ��������� ��������� ����� ������� � �� ������ ����������� ����������, ���������� ���� �� ���������� ������ ���� ����� ������� � �������� - ���������� ���������� ��������������� ������ ��� ������� ������������ � ������ ������������. � ������� ����, �������� ������� �������, �������� � ���� ����� �������, ��� ������������ �� �����, ���������� � ���������� ��������� ��������. � ���� ����������� ��������, ��� ����� �������� ������, �� ��������������� ����������, ���� "������������" ��������, ��� ��������� ����������� �������� �����������. "� �������, -������������ "���" ������������ ���������-��������������� ������������ "������������" ������� �������, - ����������� ���������� �� 20% �������� �� �������� � ����� ����� ��� �������� � ��� ���� ������ 10% - � ���������� ��� ������ ������ ��������, ���������������� �� �������� � ����������� � ������������� ���� "�" ������� ������". ����������, ������ �� ����������� ������������ ��������� "��������� �����������" ������ �������, ��� ����������� ����� ������������� � ������ ������ �������� ������ ���������. "������� ����� �������� �������� ������� � ���������� � ���������, ���������� ������� � ����� ����, ��� �����", - �������� ��.</P>
<P align=justify><STRONG>����� ����� �������� ����������</STRONG></P>
<P align=justify>��������������� "�����������" ���������� �������</P>
<P align=justify>� ��������� ���� ���������� ����� �������, ��� ��������������� �����������, ����������� ������� �������������� ��������� ��������. � ������� � �������� �������� -�������, ����������, ����������� ��� ������������ - �� ���������� �� 95 �� 105% (��� ������ �����������, ��� ������� ����� �������� ����������), � ��� ��������, ��� ������� �� ��������� ������������ ���������� ���� �� ����� 5% �� �������, � � ���� ������� �������� �� ��������� ��������� �������� ������, � �������� ������� ������������ �������������� �������".</P>


</body>
</html>

