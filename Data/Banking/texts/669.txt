<!--
ID:35572650
TITLE:�������� �. �., ��������� �. �. ������� ���������� �� ���������� �����
SOURCE_ID:10030
SOURCE_NAME:�����: ������� ����. ����� ���
AUTHOR:����� �������� &#150; �.�. ��������, ����. ����. ����, ����� ���
NUMBER:5
ISSUE_DATE:2005-10-31
RECORD_DATE:2005-11-24


RBR_ID:1093
RBR_NAME:���������� ����
RBR_ID:2046
RBR_NAME:�������������� � ���������� �����, ������������ � ������ ����������
-->
<html>
<head>
<title>
35572650-�������� �. �., ��������� �. �. ������� ���������� �� ���������� �����
</title>
</head>
<body >


<P align=center>Johnston R. B., Nedelescu O. M. The impact of terrorisme on financial markets // IMF working paper. &#8211; N. Y.: IMF, 2005. &#8211; 22 p.</P><FONT face="Times New Roman">
<P align=justify>� ������ ���������� ������� ������ ����������� ������������ �������� � ���������� ������ ���, �������������� �� ������������� ����������� &#8220;���������� � ������������� ������� ������ ������ ����������&#8221;, �������������� </FONT>NATO Defense College<FONT face="Times New Roman"> � ������������ � ������� 2004&nbsp;�. � ����.</P></FONT><FONT face="Times New Roman">
<P align=justify>���������� ��������� ����� ���� ��������� � ��������� ������������ � �������� ����� (��� ��� ��������� � ���������� ���������������� ����� 11 �������� 2001&nbsp;�.), ���������� ��� ������������. � ��� ����� ��� ����� ��� �������� ����� ���� ���������� ������������ ��� ���������������, �������� � �������� ����� �� ���� ����������. ����� ����, ��� ����� �������������� ��� ������ ��� �������� ������� ���������������� �����������. � ������ ������ ���������� ��������� ��������������� ��� ������ ���������������� �����.</P>
<P align=justify>�������� ���������������� ����� ������� ������� �� ������������ � ���������� ��������� � ���������� �������. �� ����������� ����� ������-, ������- � ������������ ��������.</P><I>
<P align=justify>������ ������������� ����� �� ����� ����������, </I>���������� � ������ ����� � ����������� ���������, �������� ������� �������������� ���������: �� �������������� ���������������� � ���� �������� � ������ ������������. ������ ������������� ����� �� ���������� �������������� ������������� ���������������� ����� � ��������� ��������������� ������������ ���������. ������ ������������� ����� �� ����������������� ���� 11 �������� 2001&nbsp;�. ��������, �� ������ ����, 27,2 ����. ����. (����� 0,25% ��� ���), � ��� ����� ������ �������� ������� 14 ����., ������� ����������� &#8211; 1,5 ����., ������������ ������������� &#8211; 0,7 ���� � ������ ������ �� ��������������� ���������� �������� &#8211; 11 ����. ��������.</P><I>
<P align=justify>��������� ������ �� ����������</I> ����������� �� ��������� ��������� � ������������� �������, ���������� � �������� ������ ������� �� ������� ������������ � ����������. ��� ��������� ���������� ��������� � �����������, ��� �������� ���������� ������ ����� ������������ � ���������� ���������. ����� ����, �������� ������ ������� ���������� ����� �������� � ������ ������� ��� �� ������ � � &#8220;������� � ��������&#8221;, ��� ���������� � ���������� ���������� ��������� ���� �������� � ������������� ������ ������ � ����������� ������ (������ � ����������������� ���������), ��� �������� ���� �� ����������� �������������. �������� � ������������� ���� �������� ����� �������� � ��������� ������� �� ����� ��������, ��� ������� ���������������� �����, �� �������, ��� ��������, ���������� � ����� �� �����, � ������������ ������ (�� ���� �� ����������� � �������� ��������������). ���������������� ����� 11 �������� 2001&nbsp;�. ������� ������� �� ������ ����������� �������� ������ ����� �������� ������ ������ ���������� ���������� ������� � ���������, ����������� �� ����� ������� ���������. ������������� ����� ����� ������� ����� ���������� �������� �������� ������ � &#8220;������� � ��������&#8221; �� ���������� ������. �������� �� ������������ ����������, ��������� ������ 11 ��������, ���������� ����� � ����� ������������������ ����������� ������ �����������������.</P>
<P align=justify>������������ ����������� ���������������� ����� �������� � ����������� ������� �� ������������������, ������� ����������� � ��������� �������������� ��������, ��������� � ������������ ������������, ������ ��������� ������, � ����� ���������� � ���� �������� �� ���� �������������������� ��������.</P>
<P align=justify>������������ ����� ���������� ����������� ����� ��� ���� ���� �������� � ������� ������������� ���������������� ����� ���������� ��������� ������ (���) ��� ���������� �� ���. ������ ������� ����� ��������� �������� ���������� ����������� �������� �� ���������� ������ ���-�����. ���������� ����� ��� ������� ���������������� ������� ����������� �������������� � ���������� ����� </FONT>Bank of New York<FONT face="Times New Roman">. ���� </FONT>Bank of New York<FONT face="Times New Roman"> � </FONT>J.P. Morgan Chase<FONT face="Times New Roman"> &#8211; ��� ������� ����������� ����� ��� �������� �� ����������������� ������ ������� &#8211; ������ ��������� ���� �������� � ������ �������������, ������������ ���������� ����������� �������� ������� �������� ���������������� � ������� ���������� �������� ������ �� ������ ������� � ���������� ������ �� �����������. ����� ����������������� ������ ����� ����� ��� �������� ������� �������������� ���������� ����, ������������� � ���. ������������� ������������� ���������� ����� ��������� ��������� ���������� � ��������-����������, ���������� � ������ ������. ���������������� ����� ������������ �������� � �� ������ ���������� �����, ��������</FONT>,<FONT face="Times New Roman"> �� ����� ���� � ����� ����������. ������ ��������� �������� ����������� ����� ��� � 50 ����. ����., ��� � ������ ������� ����������� ������������ ����������� ��������� ������.</P>
<P align=justify>��������� ���������������� ����� 11 �������� 2001&nbsp;�. ���� ��������� � 9 ����� ���� �� �������� �������, ���-�������� �������� ����� (</FONT>NYSE<FONT face="Times New Roman">) � </FONT>NASDAQ<FONT face="Times New Roman"> � ���� ���� �� �����������. ��� ����������� ���� ������ 17 ��������, ����� ������������ ����� ��������������� �������� ������� � ��������� �� ������ ������� � ������ (</FONT>Securities and Exchange Commission, SEC<FONT face="Times New Roman">). �������� ������� � ������ ��� ����� ������� ���������: � 17 �� 21 �������� 2001&nbsp;�. ������ </FONT>Standard and Poor<FONT face="Times New Roman">'s �������� �� 11,6% � ������ </FONT>NASDAQ<FONT face="Times New Roman"> &#8211; �� 16,1%. �� ��� �� ������ ����������� ������ </FONT>Dow Jones Euro STOXX<FONT face="Times New Roman"> �������� �� 17,1%. ��� ���������: ����� ������� � ������� 11 ����� 2004&nbsp;�. ���� ������ �������� �� 3% � �� ����� ����� ��������� �� �������� ������. �������� ����������� ���� �������� (11 �������� 2001&nbsp;�. � 11 ����� 2004&nbsp;�.) ��� ������ ������ ����� ����� ����������� ���������</FONT> <FONT face="Times New Roman">�������: ��-������, ����� � ���-����� �������� ������������ ���������� ���������� �������, ����� ��� �� ����������� � ������� ����� ������������ ������; ��-������, ����� 11 �������� ����� ����� � ��������, ����� ������������ � ������� ��������� ����������</FONT> <FONT face="Times New Roman">� ��������� �����, ����� ��� ������� � ������� ����������� � ������ ����������� �������������� �������; �-�������, ������ � �������, � ������� �� ������� � ���-�����, �� ��� ������� ��������������� �� ��������� ������ ���������� ������ � �� ���������� ������ �� ������������.</P>
<P align=justify>� ���������� ������� ��������� ����� ������������, ����������� ������ ������� �������� �� ���������� �����. � ����� �� ��� ������������ �������� ��� ������� ����������� ���������� ������� �� ��������� ������� � ������ ������ �� ��� �������� ������. � ��������� ������� ���������� ��������������� ������ �� ���������� ����������� ���������� �� ������ ������� ���������-����������� ������� ����� ������� 11 �������� 2001 ����.</P>
<P align=justify>���������������� ����� 11 �������� ������� ���������� ����������� �� ��������� ����������� ����������� �����. ������ �������� ��������� ��� ����, ��� ���������� ����� ��� ������� ������� ������������� ������� ������� � ������� ������ ����������� ������� ������� ���������� �� ������������ ������. ���� �� ���������� �����</FONT> <FONT face="Times New Roman">����� ����������� � ���, ��� ����������� ��������� ������� (���) ��� ����� ������ ������������� ����������� ����, ����� ��������� � ��������������� ��������� ����� ��������� ���������-����������� �������.</P></FONT><B><FONT face="Times New Roman">
<P align=right>�������</P>
<P align=center>���������� �� ������� ���������� �������� � ������ ������ ���������-����������� �������</P></B></FONT>
<TABLE cellSpacing=1 cellPadding=7 width=638 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="26%" rowSpan=2>&nbsp;</TD>
<TD vAlign=top width="56%" colSpan=3><B><FONT face="Times New Roman" size=2>
<P align=center>���������� �� �������� ������ (� ����.)</B></FONT></P></TD>
<TD vAlign=top width="19%" rowSpan=2><B><FONT face="Times New Roman" size=2>
<P align=center>����� ���� �������� � ������������ ������</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="19%"><FONT face="Times New Roman" size=2>
<P align=center>� ���� �������</FONT></P></TD>
<TD vAlign=top width="19%"><FONT face="Times New Roman" size=2>
<P align=center>�� 6-� ����</FONT></P></TD>
<TD vAlign=top width="19%"><FONT face="Times New Roman" size=2>
<P align=center>�� 11-� ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="26%"><FONT size=2>
<P align=justify>NYSE</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 4,79</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 6,69</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 0,45</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>13</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="26%"><FONT face="Times New Roman" size=2>
<P align=justify>������</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 10,09</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 8,64</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 14,14</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>27</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="26%"><FONT face="Times New Roman" size=2>
<P align=justify>���������</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 10,06</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 14,54</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 15,79</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>42</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="26%"><FONT face="Times New Roman" size=2>
<P>����������� ������ </FONT><FONT size=2>Bloomberg</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center></P>
<P align=center>- 8,54</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center></P>
<P align=center>- 11,50</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center></P>
<P align=center>- 14,82</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center></P>
<P align=center>40</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="26%"><FONT face="Times New Roman" size=2>
<P align=justify>���������</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 6,17</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 6,43</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 11,35</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>31</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="26%"><FONT face="Times New Roman" size=2>
<P align=justify>��������</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 5,79</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 14,18</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 28,55</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>107</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="26%"><FONT face="Times New Roman" size=2>
<P align=justify>�����</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 6,50</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 1,70</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 12,18</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>26</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="26%"><FONT face="Times New Roman" size=2>
<P align=justify>�������</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 7,87</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 11,02</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 14,34</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>30</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="26%"><FONT face="Times New Roman" size=2>
<P align=justify>����� �����</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 13,33</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 13,78</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 19,84</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>28</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="26%"><FONT face="Times New Roman" size=2>
<P align=justify>��������</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 2,83</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 3,73</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 6,23</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>86</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="26%"><FONT face="Times New Roman" size=2>
<P align=justify>�����-������</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 5,20</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 13,36</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 18,68</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>65</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="26%"><FONT face="Times New Roman" size=2>
<P align=justify>���������</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 3,98</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 9,46</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 11,07</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>26</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="26%"><FONT face="Times New Roman" size=2>
<P align=justify>����� ��������</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 3,67</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 11,39</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 14,93</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>33</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="26%"><FONT face="Times New Roman" size=2>
<P align=justify>������������</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 5,27</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 14,43</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>- 11,00</FONT></P></TD>
<TD vAlign=top width="19%"><FONT size=2>
<P align=center>162</FONT></P></TD></TR></TBODY></TABLE><FONT face="Times New Roman">
<P align=justify>� ����� ������� 11 �������� 2001&nbsp;�. ������������������ ����������� ���� ������, ����������� �� �������� ���������� �������, ����� � ������ ������ ��������� ���, ����������, ��� ��� �������������� ������� ��� �� ���������� ������, ��� � �� �� ���������, ���������� ����������� � ����������� �������� ����. ����� ������������� ����� � ������, ��� ���������� �����������, ������ &#8220;������� ����&#8221; (</FONT>discount window<FONT face="Times New Roman">) ��� �������� �������, ����� ������������� ����������� ������ � ��������� ���������. ��� �������������� ����������� ��������� ������� ���� ������������ ��������� ����������� ��������� �����: �� ��������� �������� ������ ��������� �� ��������� �� �������� ������� ���������� ������. � ���������� ����� �������� ��������� ������� ���������� � 100 ����. ����. ��� ������������� ����������� ��������� ����� ����� ����� �������� � �������������, ������������� �� �������.</P></FONT>
<P align=justify>SEC<FONT face="Times New Roman"> ����� �������� ������������ ������ ����� ��������� ��
���� �������������� �������� � ����� ������ ����� � ������ ������������ ����������� ����� ��� ����������� �� �������� ���� (</FONT>share buy-back<FONT face="Times New Roman">). � �� �� ����� ������ ������������� � ���������� ������ � ����� ����������� ���������� ������������ ���������� ������ ��������� ������ ��� ����������� ������������ �����������, ������������ �� �������. ��������� � ������������ �������� ������ ���������� ������ �� �����������. ������������� � �������� ��� �������� � ��������� ����������� ��� &#8220;���������� � ��������� ���������&#8221;, ����� ������������� ����������, ������������� � ������� ��������� ���������, ������� ��� �� � ��������� ����������. ����� ��� ����� 11 �������� 2001&nbsp;�. � ��� ��� ������ ����� � ����������� ���������� � ����������� �����, </FONT>Terrorism Risk Insurance Act<FONT face="Times New Roman">, ����������������� �������� ���������� ������� ���������� ����� ��������������� � ������� ������������ ������ �������������� ���, ����������� �� �������. ������ ��������� �����, ���� ����������� ���� ������- � �������������� ���������, ������������ �� ����������� �������������� �����, ������������ � ���������� �������. � ���, � ���������, ��������� �������� ������� ������ ���.</P>
<P align=justify>�������� ������� ������� ���������� ����� � ���������� ���������, ��� ����� ������������ � �������� ����������� ������������� ������ �����. �� ������������� ������ ���� ������� ���� ��� ����������� ���������� ��������� �������, ��������� ������ ������� � ����������� ������������</FONT> <FONT face="Times New Roman">���������� ������. ���������� ������ ������� �����: ������, ���� ����, ���������, ������ � �������������� &#8211; �� ������� ��� ����������� �������� ������������ ���� ��������� ������� � ������� ������� ���������� ������. ��������� ����� �������� ���� ������� �����, �������, ����� �����, ����� �������� � ������. ����� ������ ������ �������� �������� �������� ������� � ����������� ����������, ��� ����������� �������� ���� � ����������� ����������� ������ (���) � ������ ������ �� ����� � 80 ����. ����., � ����� �������� ������� �� ����� ������������ ���������� �� �������� ���� � ������ ������.</P>
<P align=justify>� ������� �� ���������� ������������� ���, �������� � ��� � ������ ������� ����� ���������������� ����� 11 �������� 2001&nbsp;�., ����� ������� 11 ����� 2004&nbsp;�. � ������� �� �������������� ������� ��� (� ���������, ��� �� ����� ���������� ������), �� ����������� �������� ������ ���������� ������ ������������.</P>
<P align=justify>������������� �������, ��������� � ���������, ��������� ������� ��� ������, ���������� ����������� ����������� ���������� ��������. ��-������, ���� ��������������� � ������ ���� ����������� ������ � ����������� ������������ ���������� ������� � �������� �������. �������� ��� �������� � ��������� ��������� � ����������� �������� � ���������� ������ � ������������� ��������, ��� ������� ��������� ���� � �������������� ������� � ����� � ������� ��� ������������ ���� �������, ����������� ������ ����������� ��������� �������� � ������������ ��� ����� ��������� ������. ��-������, ����������� �������� ��� �������� � ������������ �������������� ����������� ���������������� ���������� ������ ����� ���������� � ����������� ������������ � ����������� ������� �������������.</P>
<P align=justify>�� ������� ������, ��� ���������������� ����� �������� ������ �� ������������ ����� � ������ ���������� ���������. � 1996&nbsp;�. ����� ������� ����������� ��������� ����������� ���� ��� �����, � ���������� ���� ���� ���������� ���� ������ � ������������ ������� �����. ��� ������ ������ � �������������� ���� ������ �����. ������ ������� ��������� ��������� � ���, ��� ����� ���������������� ���� ����� ���� ��� � ��������� ����.</P>
<P align=justify>��� ����������� ����������� ���������������� ���� ������ ����������� ������������� ������ ������������� ��������� ������������ ����. � ����� ������ ���� ������� ������� ���������� �� ���������� ����� ������ �� ����� ����� ��������, ���������� �: �) ������ ����������� ���������� ���������������� ����� ��� ������� ������������� �����; �) ��������� ���������� ��� �� �������������� �������������� ����������.</P>
<P align=justify>�) ���������� ������� �� ����������� ������� (</FONT>Basel Committee on Banking Supervision<FONT face="Times New Roman">) ��������� ��������� ��� �������, ���������� ������� ������������� �����, � ������� ���� ������� ����������� ������� � �������� ������������� �����, ����������� ����������� ����������������� ����. ������ ������� ������ ������� �� ���, ����� ��� ����� ����������� ����������� ������ �������������, �����������, ����������� � �������� �� ������������ ������, ��������� � �����������, � �������� �� � ����� ������� ���������� �������. ���� ����� ������ ����������� ����, �������������� �� ����������� ����������� � ������������ ������ � ������ ���������� ���������� �������, � �������� ��������� ���������� ��������� ������-������.</P>
<P align=justify>� ���������� ������� ��� � ������ ������� ������������� ���������� ������, �������������� � 2002&nbsp;�. � ���, ����������, ��� ��� ����������� ������������� ����������� ����������������� ���������� ���������� ������ � ��� �������� �������������� ���������� ������ ����������: 1) ������������� ����������� ����� ������������ � ������� ����������� � ��������� ��������, ����������� ��� ����������� ������������ ���������� �������; 2) ����������� �������� �������������� � ������������� ���� ����� ������������; 3) ����������� ����������� �������������� �������������� ��������� ����� ������������ ��� �� ���������� ��������������; 4) ���������� ������������ �������� ������ ��������������.</P>
<P align=justify>�) �� ������ ������ ���������� ���������� ��� ������ � ���������� ����� (����), �������� ����� ����������� � ������� ����������� �������� ������������� ���������� ������ �� ����� �����������. �������������� ���������� ����� ����� ����� ���� � ���������� ����� � ����� ������ ���������� ��������� �������, �������� �� ������������� � �������� � �����, ��� ��� �� ���������� ������� ��������. ���������� ���������� ���������� �������������� ��� ����������� � �������� �������, ������� ��� �������� �� ���������� ����� ������������, ��� ��������� �����, ��������������, �����������, �������� �����������, ������� � ���������. ����� �������, ������� ����, ����������, ��� � ������ ���������� ��������,</FONT> <FONT face="Times New Roman">��������� � ��������� �����, ��������� ��������� ������� �������������� �������� �������, �������� � ���������� � ���������� �������, ������������ ����� � �.�. ����� ����, ���������� ���������� ������ �� �������� ������������ ��������� � �������, ��������</FONT> <FONT face="Times New Roman">�������� � ������������������ � �������������� �������������. ���������� ���� ���������� ����� ���������� ��������, �������� � ���������� ������� ����������� �������� � ������������� �����.</P>
<P align=justify>�������� �� ������������ �������� ����� ���������� ���������� ����� � ��������������� ����������, ��������� �������� ����� ������������� ����, ��������� � ���, ��� ����������, ������ ����������� ���������� ���������� �������, ���������� � ��������� �����. ����� ����, ����� �����, ����������� � ����� � ��������������� ����������, �������� �� ��������� � ������� ���������� �����, ���������, ��������, � ��������� �����������. ���, �����, ����������� �� ������ ����������� �������� 11 �������� 2001&nbsp;�., �� ��������� 10 ���. ����., ��� ��������� �� ������������� ��� ������� �������� �������� �������. ��� ��� ��������� ������ � ��������������� ����������, ��� ��� ������� ������������ ������������������ ������� � ��������� � �������������� ����������� ������������� �������.</P>
<P align=justify>������ ������� ����������� ������ ������ � ������ � ��������������� ���������� ���� ����������� ����, ������� ��� ������ ��� ����� ����������� 40 ������������ �������������� ��������� ����� �� ����������� ������ � ���������� ����� � ���������� ��������������� ������������ ��������. � ������� 1999&nbsp;�. �����������</FONT> <FONT face="Times New Roman">��������� ��� ������� ������������� ��������� �� ��������������� �������������� ���������� (</FONT>UN International Convention for the Suppression of the Financing of Terrorism<FONT face="Times New Roman">). � ������ ����� ����� �������� ��� ��������� �������������� ����� ������ ������. ����� ������� 11 �������� 2001&nbsp;�. �� �������������� 117 �����. �� ������ ������� ������������ �������� ���, ��������������� � 2003&nbsp;�., &#8220;�������� � ������ � ��������������� ����������&#8221; (</FONT>Progress in the war on terrorist financing<FONT face="Times New Roman">), ����� 11 �������� 2001&nbsp;�. 173 ������ ������� ������� � ������������ ��� ������������� ������� �����������, 100 ����� ������� ����� ��������������� ����, ����������� �� ������ �� �������� �� �������������� �����������, � ����� 100 ����� ������� ������ ���������� ��������.</P>
<P align=justify>� ����� 2001&nbsp;�. ���������� ���� ���� ��������� �� ���� ��������� � �� ����������� �������� ������ � ��������������� ����������. �� ��������� ��������� ���� � ������� 2001&nbsp;�. ���� ������� &#8220;������ ����������� ������������&#8221; �������������� �� ����������� ������ � ��������������� ���������� (� ������� 2004&nbsp;�. ��� ������������ ���� ��������� ��� ����� &#8211; ������ �� ������). ���� �������, ��� ��� ������������ ������ ��������� 40 ����� ���������������� ������������ �� ������ � ���������� �����. ����� ������������, ������������ ��</FONT> <FONT face="Times New Roman">���������, �������������� � ���������� �������� �� �������������� ����������, �������� ��������� ���������� � �������:</P>
<UL>
<P align=justify>
<LI>����������� ����������� ���� �� ����������� � ������������� ������������ � ���, ������������ ���; 
<P align=justify></P>
<LI>������� ����������� �������������� ����������, ���������������� ���� � ���������������� �����������; 
<P align=justify></P>
<LI>���������� � ������������ ������ �����������; 
<P align=justify></P>
<LI>�������� � �������, ������������� � ������ � �����������; 
<P align=justify></P>
<LI>������������ ������� ����������� ��� �������� �������� ������ ������ ������� � ������������ ������������, ���������� ������������� �������������� ����������; 
<P align=justify></P>
<LI>�������� ����������� ���������� ������ � ������������� �������������� ������ �������� ���������; 
<P align=justify></P>
<LI>������� ���� �� ������������� �������� ������� � ������������� ������ ����������� ������ �� �������� �������� ������� � �������� ����������; 
<P align=justify></P>
<LI>������� �� ���, ����� ����� �����������, �������� �������������, �� ����� �������������� ��� �������������� ����������; 
<P align=justify></P>
<LI>�������� ���� � �������� ������������� ��������������� �������� ������� � ������������ ������������ �� ������������. </LI></UL>
<P align=justify>� 2002&nbsp;�. ��� � ��������� ���� �������� ������������ � ��������� ����, � ����� �������� ������������ � ������������ ������� &#8220;� ����� ����&#8221;. � 2003&nbsp;�. ���� ������������ ���� 40 ������������, ������� � ����� �� �������� ����� ������������ �������, ����������� � ���������������� ������, ����� �������: ������, ��������� ������������, ������, ������������ ��������� ������� ��������� � �������, ��������, ���������, ��������, ��������� �������� � ������.</P>
<P align=justify>��� ���������� ���������� ������������, ����� ������ � ��������������� ���������� �� ������ ������� �� ������ ���� ������������� ���� ���������������� ������������, ������� ��� ��������� ��������� �� ���������� ��������. ������� �������� ��� ���� ����� ������� � ������ ������������� ���, ���������� � ���������� �������� �������� ����� ������������� ���������� � ������������ ����, ��� ������� ������������ ����������� ������������ ��������������� �������. � ������ 2004&nbsp;�. � ���-���� ���������� ������ ������������� ����������� �� ��������� ������������� �������� �������� ������� ������������ �������� �� ������, �������������� ��� � ����������� ������ ���. �� ����������� ����������, ��� ��� ������������ ���� ����� �������������� ��� ��������� ����� � ����� ������� ��� �������������� ����������. �� �������, ����� ������������ ������ � ������������� ������ ��������� �� 10 �� 50% �������� ������� �� ���������, ����� ����� ������� � 2001&nbsp;�. ���������� 72,3 ����. ��������.</P>
<P align=justify>�������� ������������ ���, ������������ �� ������ � ���������� ����� � ��������������� ����������, ��������� � ������ �������� �� ���������� ��������������� ���������� � ������������� (������� ������� � ������), �������� �� ���������� ����������, ������ � ����. ��� ���������� ����, ������������ ������ ����������� �������� ����� ����� �� ���� ������� ������� �� ����������� ������� � ������� ����������� ����������� ����������, ��������� � ������� � ���������� ����� � ��������������� ����������. � 2004&nbsp;�. �� ������� ���������� ���������� ��������� ��������� ��� (</FONT>US Office of the Controller of the Currency<FONT face="Times New Roman">) ���� </FONT>Riggs<FONT face="Times New Roman"> ����� ����������� �������� ������ ������ ����� ��������� ������������ � ������������ �������� �� ����� � 25 ��� ����. � ��� �� ���� �������� ��������� ���������� ����� (</FONT>Japan Financial Services Agency<FONT face="Times New Roman">) ������� ���������</FONT> Citigroup<FONT face="Times New Roman">, ������������ ����������� �������� ��������, ��������� ������ ��������� � ����������� ��������. </FONT>ABN Amro<FONT face="Times New Roman"> � </FONT>Union Bank of California<FONT face="Times New Roman"> ��������� ���� ���������� ������� ��������� � 550 ������� ����� ��������� ������, �������� ���������� ���� � ������, ������� ���� ������� � ������� ������ ������������ ���������� �� ������ � ���������� ����� � ��������������� ����������.</P>
<P align=justify>������ � ������� ��������, ���������� � �������������� ������� �������, ����� � ������ ���������� ��������� ����� �������� ���� ������� ��������� ��-�� ������� ������ �������������, ��� �������� �� �� ���������� ���� � �������� �������������. ���, ���� ����� ������ �� ���������� ������������� ������ �������� �� 3% �� ��������� ���� ����� ���������� ������ � ����������� ������� ��� ������������, � ������� ���� �������� ��������� ��������� ��������.</P>
<P align=justify>��������� ���, ��������� � ��������������� � ������� � ���������� ����� � ��������������� ����������, �������������� ����� �������� ���������� ����� �������� �� ����������� ����������� � ����� ������ � ���� (����� ���������-��������). ��� �������� ���� � �������� ����������� �������������� �������� �������������� ����������. � &#8220;������ ���������� ������ � ���������� ����� � 2004&nbsp;�&#8221; (</FONT>Global Anti-Money Laundering Survey<FONT face="Times New Roman"> &#8211; 2004), �������������� </FONT>KPMG International<FONT face="Times New Roman">, ���� ��������� ������ ������ 209 ������ �� 41 ������, ������� ����� ������� ������� � ����� 1000 ���������� ������ ����. 83% ������������ �������, ��� �� ��������� ��� ���� ������� �� ���������-�����, ��������� � ��������� ��� ������ ��������� ����� � �������������� ����������, �������� � ������� �� 61%.</P>
<P align=justify>�������� �� ���� ��������, ������� ������ ��������� ���������� � ���������� ������ � ����������� ����������������� � ������� ���������� ����������� ������� ������� � ������������ ������� ������������ � ����������� ���������� �������, ��� ������������ ����� ������������ ������������� �������� � ����� ��������� ������ �������������� �����. ������� ������� ��� ���� �������� ���������� ����������� ������� ����� ���������� � �������� ��������</FONT> <FONT face="Times New Roman">�������� ������������ ���, ������������ ������ ��������� ����� � �������������� ����������, ��� ��������� �������� ����������� ���������� ������.</P></FONT>


</body>
</html>

