<!--
ID:35278393
TITLE:����������
SOURCE_ID:3144
SOURCE_NAME:������ � ������
AUTHOR:
NUMBER:7
ISSUE_DATE:2005-07-31
RECORD_DATE:2005-09-13


-->
<html>
<head>
<title>
35278393-����������
</title>
</head>
<body >


<P align=right>���������� ����������</P>
<CENTER>
<TABLE cellSpacing=1 cellPadding=7 width=583 border=1>
<TBODY>
<TR>
<TD vAlign=center width="23%" height=46 rowSpan=2>
<P align=center>��� ����������</P>
<P align=center>�� �����</P></TD>
<TD vAlign=center width="77%" colSpan=4 height=46>
<P align=center>��� ��������� ����������� (�������)</P></TD></TR>
<TR>
<TD vAlign=center width="14%">
<P align=center>�� ����</P></TD>
<TD vAlign=center width="26%">
<P align=center>��������</P>
<P align=center>���������������</P>
<P align=center>���������������</P>
<P align=center>�����</P></TD>
<TD vAlign=center width="21%">
<P align=center>��������������� �����</P>
<P align=center>(/����������</P>
<P align=center>�����)</P></TD>
<TD vAlign=center width="16%">
<P align=center>���</P></TD></TR>
<TR>
<TD vAlign=center width="23%">
<P align=center>45277589000</P></TD>
<TD vAlign=center width="14%">
<P align=center>17786200</P></TD>
<TD vAlign=center width="26%">
<P align=center>1027739058324</P></TD>
<TD vAlign=center width="21%">
<P align=center>2291</P></TD>
<TD vAlign=center width="16%">
<P align=center>044525788</P></TD></TR></TBODY></TABLE></CENTER><B>
<P align=center>������������� ������<BR>(����������� �����)<BR>�� 1 ���� 2005 ����</P></B>
<P>��������� ����������� <B><U>�������� ����������� �������� ��������-����������<BR></U><U>���� &#8220;����������&#8221; (��� &#8220;����������&#8221;)<BR></B></U>(��������� (������ �����������) � ����������� ������������)<BR>�������� ����� <U>125252, �. ������, ��. ������������, �. 20/10 ���. 1.�</U></P><B>
<P align=right></B>��� ����� 0409806</P>
<P align=right>�����������/�������<BR>(���. ���.)</P>
<TABLE cellSpacing=1 cellPadding=7 width=652 align=center border=1>
<TBODY>
<TR>
<TD vAlign=center width="6%">
<P align=center>&#8470; �/�</P></TD>
<TD vAlign=center width="68%">
<P align=center>������������ ������</P></TD>
<TD vAlign=center width="13%">
<P align=center>������ �� �������� ����</P></TD>
<TD vAlign=center width="13%">
<P align=center>������</P>
<P align=center>�� ������ ��������� ����</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>1</P></TD>
<TD vAlign=top width="68%">
<P align=center>2</P></TD>
<TD vAlign=top width="13%">
<P align=center>3</P></TD>
<TD vAlign=top width="13%">
<P align=center>4</P></TD></TR>
<TR>
<TD vAlign=top width="6%"><B>
<P align=center>I</B></P></TD>
<TD vAlign=top width="68%"><B>
<P align=center>������</B></P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=center width="6%" height=10>
<P align=center>1</P></TD>
<TD vAlign=top width="68%" height=10>
<P>�������� ��������</P></TD>
<TD vAlign=top width="13%" height=10>
<P align=right>1 306 214</P></TD>
<TD vAlign=top width="13%" height=10>
<P align=right>1 767 298</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>2</P></TD>
<TD vAlign=top width="68%">
<P>�������� ��������� ����������� � ����������� ����� ���������� ���������</P></TD>
<TD vAlign=top width="13%">
<P align=right>1 859 319</P></TD>
<TD vAlign=top width="13%">
<P align=right>2 639 911</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>2.1</P></TD>
<TD vAlign=top width="68%">
<P>������������ �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>599 391</P></TD>
<TD vAlign=top width="13%">
<P align=right>478 251</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=12>
<P align=center>3</P></TD>
<TD vAlign=top width="68%" height=12>
<P>�������� � ��������� ������������</P></TD>
<TD vAlign=top width="13%" height=12>
<P align=right>2 349 887</P></TD>
<TD vAlign=top width="13%" height=12>
<P align=right>785 944</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>4</P></TD>
<TD vAlign=top width="68%">
<P>������ �������� � �������� ������ ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>1 997 401</P></TD>
<TD vAlign=top width="13%">
<P align=right>5 563 396</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>5</P></TD>
<TD vAlign=top width="68%">
<P>������ ������� �������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>29 225 116</P></TD>
<TD vAlign=top width="13%">
<P align=right>22 628 258</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>6</P></TD>
<TD vAlign=top width="68%">
<P>������ �������� � �������������� ������ ������, ������������ �� ���������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>7</P></TD>
<TD vAlign=top width="68%">
<P>������ �������� � ������ ������, ��������� � ������� ��� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>2 269 986</P></TD>
<TD vAlign=top width="13%">
<P align=right>1 705 485</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=17>
<P align=center>8</P></TD>
<TD vAlign=top width="68%" height=17>
<P>�������� ��������, �������������� ������ � ������������ ������</P></TD>
<TD vAlign=top width="13%" height=17>
<P align=right>3 120 684</P></TD>
<TD vAlign=top width="13%" height=17>
<P align=right>3 058 927</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>9</P></TD>
<TD vAlign=top width="68%">
<P>���������� �� ��������� ���������</P></TD>
<TD vAlign=top width="13%">
<P align=right>76 779</P></TD>
<TD vAlign=top width="13%">
<P align=right>75 047</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>10</P></TD>
<TD vAlign=top width="68%">
<P>������ ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>752 458</P></TD>
<TD vAlign=top width="13%">
<P align=right>793 395</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>11</P></TD>
<TD vAlign=top width="68%">
<P>����� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>42 957 844</P></TD>
<TD vAlign=top width="13%">
<P align=right>39 017 661</P></TD></TR>
<TR>
<TD vAlign=center width="6%"><B>
<P align=center>II</B></P></TD>
<TD vAlign=top width="68%"><B>
<P align=center>�������</B></P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>12</P></TD>
<TD vAlign=top width="68%">
<P>������� ������������ ����� ���������� ���������</P></TD>
<TD vAlign=top width="13%">
<P align=right>1 327</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>13</P></TD>
<TD vAlign=top width="68%">
<P>�������� ��������� �����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>6 640 703</P></TD>
<TD vAlign=top width="13%">
<P align=right>8 416 993</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>14</P></TD>
<TD vAlign=top width="68%">
<P>�������� �������� (����������� �����������)</P></TD>
<TD vAlign=top width="13%">
<P align=right>26 682 628</P></TD>
<TD vAlign=top width="13%">
<P align=right>22 416 597</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>14.1</P></TD>
<TD vAlign=top width="68%">
<P>������ ���������� ���</P></TD>
<TD vAlign=top width="13%">
<P align=right>16 724 017</P></TD>
<TD vAlign=top width="13%">
<P align=right>12 803 413</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>15</P></TD>
<TD vAlign=top width="68%">
<P>���������� �������� �������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>4 464 027</P></TD>
<TD vAlign=top width="13%">
<P align=right>3 225 560</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>16</P></TD>
<TD vAlign=top width="68%">
<P>������������� �� ������ ���������</P></TD>
<TD vAlign=top width="13%">
<P align=right>194 907</P></TD>
<TD vAlign=top width="13%">
<P align=right>102 327</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>17</P></TD>
<TD vAlign=top width="68%">
<P>������ �������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>392 430</P></TD>
<TD vAlign=top width="13%">
<P align=right>434 555</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>18</P></TD>
<TD vAlign=top width="68%">
<P>������� �� ��������� ������ �� �������� �������������� ���������� ���������, ������ ��������� ������� � �� ��������� � ����������� �������� ���</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>17 686</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>12 074</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=11>
<P align=center>19</P></TD>
<TD vAlign=top width="68%" height=11>
<P>����� ������������</P></TD>
<TD vAlign=top width="13%" height=11>
<P align=right>38 393 708</P></TD>
<TD vAlign=top width="13%" height=11>
<P align=right>34 608 106</P></TD></TR>
<TR>
<TD vAlign=center width="6%"><B>
<P align=center>III</B></P></TD>
<TD vAlign=top width="68%"><B>
<P align=center>��������� ����������� �������</B></P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>20</P></TD>
<TD vAlign=top width="68%">
<P>�������� ���������� (����������)</P></TD>
<TD vAlign=top width="13%">
<P align=right>2 334 000</P></TD>
<TD vAlign=top width="13%">
<P align=right>2 334 000</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>20.1</P></TD>
<TD vAlign=top width="68%">
<P>������������������ ������������ ����� � ����</P></TD>
<TD vAlign=top width="13%">
<P align=right>2 334 000</P></TD>
<TD vAlign=top width="13%">
<P align=right>2 334 000</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>20.2</P></TD>
<TD vAlign=top width="68%">
<P>������������������ ����������������� �����</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>20.3</P></TD>
<TD vAlign=top width="68%">
<P>�������������������� �������� ������� ������������� ��������� �����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>21</P></TD>
<TD vAlign=top width="68%">
<P>����������� �����, ����������� � ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>1</P></TD>
<TD vAlign=top width="68%">
<P align=center>2</P></TD>
<TD vAlign=top width="13%">
<P align=center>3</P></TD>
<TD vAlign=top width="13%">
<P align=center>4</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>22</P></TD>
<TD vAlign=top width="68%">
<P>����������� �����</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>23</P></TD>
<TD vAlign=top width="68%">
<P>���������� �������� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>1 498 520</P></TD>
<TD vAlign=top width="13%">
<P align=right>1 498 520</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>24</P></TD>
<TD vAlign=top width="68%">
<P>������� ������� �������� � ����������� �������, �������� �� ����������� �������� (�������)</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>395 017</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>182 085</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>25</P></TD>
<TD vAlign=top width="68%">
<P>����� � ���������������� ������� ������� ��� � ������������ ��������� ����������� (������������ ������ ������� ���)</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>759 120</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>350 623</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>26</P></TD>
<TD vAlign=top width="68%">
<P>������� � ������������� (������) �� �������� ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>367 513</P></TD>
<TD vAlign=top width="13%">
<P align=right>408 497</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>27</P></TD>
<TD vAlign=top width="68%">
<P>����� ���������� ����������� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>4 564 136</P></TD>
<TD vAlign=top width="13%">
<P align=right>4 409 555</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>28</P></TD>
<TD vAlign=top width="68%">
<P>����� ��������</P></TD>
<TD vAlign=top width="13%">
<P align=right>42 957 844</P></TD>
<TD vAlign=top width="13%">
<P align=right>39 017 661</P></TD></TR>
<TR>
<TD vAlign=center width="6%"><B>
<P align=center>IV</B></P></TD>
<TD vAlign=top width="68%"><B>
<P align=center>������������� �������������</B></P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>29</P></TD>
<TD vAlign=top width="68%">
<P>����������� ������������� ��������� �����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>11 074 101</P></TD>
<TD vAlign=top width="13%">
<P align=right>13 138 100</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>30</P></TD>
<TD vAlign=top width="68%">
<P>��������, �������� ��������� ������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>744 773</P></TD>
<TD vAlign=top width="13%">
<P align=right>177 919</P></TD></TR>
<TR>
<TD vAlign=center width="6%"><B>
<P align=center>V</B></P></TD>
<TD vAlign=top width="68%"><B>
<P align=center>����� �������������� ����������</B></P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=center width="6%">&nbsp;</TD>
<TD vAlign=top width="68%"><B>
<P>�������� �����</B></P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>1</P></TD>
<TD vAlign=top width="68%">
<P>�����</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>2</P></TD>
<TD vAlign=top width="68%">
<P>������ ������ � ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>174 425</P></TD>
<TD vAlign=top width="13%">
<P align=right>1 016 982</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>3</P></TD>
<TD vAlign=top width="68%">
<P>����������� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>4</P></TD>
<TD vAlign=top width="68%">
<P>������� ���������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>5</P></TD>
<TD vAlign=top width="68%">
<P>��������, �������������� �� ������ ����</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>6</P></TD>
<TD vAlign=top width="68%">
<P>������� �� �������������� ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>59</P></TD>
<TD vAlign=top width="13%">
<P align=right>290</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>7</P></TD>
<TD vAlign=top width="68%">
<P>���������� ����������� ���������� (��������) ����� �� ����������</P>
<P>(��������) �������� ��������������</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>21</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>216</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>8</P></TD>
<TD vAlign=top width="68%">
<P>������� �����</P></TD>
<TD vAlign=top width="13%">
<P align=right>2 857</P></TD>
<TD vAlign=top width="13%">
<P align=right>1 54</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>9</P></TD>
<TD vAlign=top width="68%">
<P>��
����� �� �������������� ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>10</P></TD>
<TD vAlign=top width="68%">
<P>������ �� �������������� ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>359</P></TD>
<TD vAlign=top width="13%">
<P align=right>12 658</P></TD></TR>
<TR>
<TD vAlign=center width="6%">&nbsp;</TD>
<TD vAlign=top width="68%"><B>
<P>��������� �����</B></P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>11</P></TD>
<TD vAlign=top width="68%">
<P>������� � ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>175 766</P></TD>
<TD vAlign=top width="13%">
<P align=right>819 622</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>12</P></TD>
<TD vAlign=top width="68%">
<P>������� �� �������������� ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>13</P></TD>
<TD vAlign=top width="68%">
<P>���������� ����������� ���������� (��������) ����� �� ����������</P>
<P>(��������) �������� ��������������</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>14</P></TD>
<TD vAlign=top width="68%">
<P>������ �� �������������� ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>15</P></TD>
<TD vAlign=top width="68%">
<P>������� �� �������������� ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>1 955</P></TD>
<TD vAlign=top width="13%">
<P align=right>211 578</P></TD></TR></TBODY></TABLE><B>
<P align=center>����� � �������� � �������<BR>(����������� �����)<BR>�� I-� ��������� 2005 ����</P></B>
<P>��������� ����������� <B><U>�������� ����������� �������� ��������-����������<BR></U><U>���� &#8220;����������&#8221; (��� &#8220;����������&#8221;)<BR></B></U>(��������� (������ �����������) � ����������� ������������)<BR>�������� ����� <U>125252, �. ������, ��. ������������, �. 20/10 ���. 1.�</U></P>
<P align=right>��� ����� 0409807</P>
<P align=right>�����������<BR>(���. ���.)</P>
<TABLE cellSpacing=1 cellPadding=4 width=652 align=center border=1>
<TBODY>
<TR>
<TD vAlign=center width="6%">
<P align=center>&#8470; �/�</P></TD>
<TD vAlign=center width="66%">
<P align=center>������������ ������</P></TD>
<TD vAlign=center width="14%">
<P align=center>������</P>
<P align=center>�� �������� ����</P></TD>
<TD vAlign=center width="14%">
<P align=center>������</P>
<P align=center>�� ��������������� ������ �������� ����</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>1</P></TD>
<TD vAlign=top width="66%">
<P align=center>2</P></TD>
<TD vAlign=top width="14%">
<P align=center>3</P></TD>
<TD vAlign=top width="14%">
<P align=center>4</P></TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="66%"><B>
<P align=center>�������� ���������� � ����������� ������ ��:</B></P></TD>
<TD vAlign=top width="14%">&nbsp;</TD>
<TD vAlign=top width="14%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>1</P></TD>
<TD vAlign=top width="66%">
<P>���������� ������� � ��������� ������������</P></TD>
<TD vAlign=top width="14%">
<P align=right>94 756</P></TD>
<TD vAlign=top width="14%">
<P align=right>80 700</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>2</P></TD>
<TD vAlign=top width="66%">
<P>����, ��������������� �������� (����������� ������������)</P></TD>
<TD vAlign=top width="14%">
<P align=right>1 603 320</P></TD>
<TD vAlign=top width="14%">
<P align=right>1 094 271</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>3</P></TD>
<TD vAlign=top width="66%">
<P>�������� ����� �� ���������� ������ (�������)</P></TD>
<TD vAlign=top width="14%">
<P align=right>0</P></TD>
<TD vAlign=top width="14%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>4</P></TD>
<TD vAlign=top width="66%">
<P>������ ����� � ������������� �������</P></TD>
<TD vAlign=top width="14%">
<P align=right>152 012</P></TD>
<TD vAlign=top width="14%">
<P align=right>71 526</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>5</P></TD>
<TD vAlign=top width="66%">
<P>������ ����������</P></TD>
<TD vAlign=top width="14%">
<P align=right>18 467</P></TD>
<TD vAlign=top width="14%">
<P align=right>15 410</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>6</P></TD>
<TD vAlign=top width="66%">
<P>����� ��������� ���������� � ����������� �������</P></TD>
<TD vAlign=top width="14%">
<P align=right>1 868 555</P></TD>
<TD vAlign=top width="14%">
<P align=right>1 261 907</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>1</P></TD>
<TD vAlign=top width="66%">
<P align=center>2</P></TD>
<TD vAlign=top width="14%">
<P align=center>3</P></TD>
<TD vAlign=top width="14%">
<P align=center>4</P></TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="66%"><B>
<P align=center>�������� ���������� � ����������� ������� ��:</B></P></TD>
<TD vAlign=top width="14%">&nbsp;</TD>
<TD vAlign=top width="14%">&nbsp;</TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>7</P></TD>
<TD vAlign=top width="66%">
<P>������������ ��������� ��������� �����������</P></TD>
<TD vAlign=top width="14%">
<P align=right>301 294</P></TD>
<TD vAlign=top width="14%">
<P align=right>48 069</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>8</P></TD>
<TD vAlign=top width="66%">
<P>������������ ��������� �������� (����������� �����������)</P></TD>
<TD vAlign=top width="14%">
<P align=right>624 478</P></TD>
<TD vAlign=top width="14%">
<P align=right>405 631</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>9</P></TD>
<TD vAlign=top width="66%">
<P>���������� �������� ��������������</P></TD>
<TD vAlign=top width="14%">
<P align=right>129 150</P></TD>
<TD vAlign=top width="14%">
<P align=right>178 035</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>10</P></TD>
<TD vAlign=top width="66%">
<P>����� ��������� ���������� � ����������� ��������</P></TD>
<TD vAlign=top width="14%">
<P align=right>1 054 922</P></TD>
<TD vAlign=top width="14%">
<P align=right>631 735</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>11</P></TD>
<TD vAlign=top width="66%">
<P>������ ���������� � ����������� ������</P></TD>
<TD vAlign=top width="14%">
<P align=right>813 633</P></TD>
<TD vAlign=top width="14%">
<P align=right>630 172</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>12</P></TD>
<TD vAlign=top width="66%">
<P>������ ������ �� �������� � ������� ��������</P></TD>
<TD vAlign=top width="14%">
<P align=right>322 368</P></TD>
<TD vAlign=top width="14%">
<P align=right>163 511</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>13</P></TD>
<TD vAlign=top width="66%">
<P>������ ������ �� �������� � ����������� �������</P></TD>
<TD vAlign=top width="14%">
<P align=right>181 407</P></TD>
<TD vAlign=top width="14%">
<P align=right>78 608</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>14</P></TD>
<TD vAlign=top width="66%">
<P>������ ������ �� �������� � ������������ ��������� � ������� ����������� �������������</P></TD>
<TD vAlign=top width="14%">
<P align=right></P>
<P align=right>294</P></TD>
<TD vAlign=top width="14%">
<P align=right></P>
<P align=right>910</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>15</P></TD>
<TD vAlign=top width="66%">
<P>������ ������ �� ���������� ����������� ������</P></TD>
<TD vAlign=top width="14%">
<P align=right>2 338</P></TD>
<TD vAlign=top width="14%">
<P align=right>7 618</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>16</P></TD>
<TD vAlign=top width="66%">
<P>������������ ������</P></TD>
<TD vAlign=top width="14%">
<P align=right>839 068</P></TD>
<TD vAlign=top width="14%">
<P align=right>323 259</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>17</P></TD>
<TD vAlign=top width="66%">
<P>������������ �������</P></TD>
<TD vAlign=top width="14%">
<P align=right>238 935</P></TD>
<TD vAlign=top width="14%">
<P align=right>49 000</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>18</P></TD>
<TD vAlign=top width="66%">
<P>������ ������ �� ������� ��������</P></TD>
<TD vAlign=top width="14%">
<P align=right>10 512</P></TD>
<TD vAlign=top width="14%">
<P align=right>3 381</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>19</P></TD>
<TD vAlign=top width="66%">
<P>������ ������ ������������ ������</P></TD>
<TD vAlign=top width="14%">
<P align=right>-43 180</P></TD>
<TD vAlign=top width="14%">
<P align=right>-14 884</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>20</P></TD>
<TD vAlign=top width="66%">
<P>��������������� &#8211; �������������� �������</P></TD>
<TD vAlign=top width="14%">
<P align=right>1 318 212</P></TD>
<TD vAlign=top width="14%">
<P align=right>777 832</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>21</P></TD>
<TD vAlign=top width="66%">
<P>������� �� ��������� ������</P></TD>
<TD vAlign=top width="14%">
<P align=right>-24 226</P></TD>
<TD vAlign=top width="14%">
<P align=right>-52 103</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>22</P></TD>
<TD vAlign=top width="66%">
<P>������� �� ���������������</P></TD>
<TD vAlign=top width="14%">
<P align=right>545 067</P></TD>
<TD vAlign=top width="14%">
<P align=right>313 640</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>23</P></TD>
<TD vAlign=top width="66%">
<P>����������� ������ (������� ����� �� �������)</P></TD>
<TD vAlign=top width="14%">
<P align=right>178 399</P></TD>
<TD vAlign=top width="14%">
<P align=right>72 217</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>24</P></TD>
<TD vAlign=top width="66%">
<P>������� �� �������� ������</P></TD>
<TD vAlign=top width="14%">
<P align=right>366 668</P></TD>
<TD vAlign=top width="14%">
<P align=right>241 423</P></TD></TR></TBODY></TABLE><B>
<P align=center>����� �� ������ ������������� ��������,<BR>�������� �������� �� �������� ������������ ���� � ���� �������<BR>�� 1 ���� 2005 ����</P></B>
<P>��������� ����������� <B><U>�������� ����������� �������� ��������-����������<BR></U><U>���� &#8220;����������&#8221; (��� &#8220;����������&#8221;)<BR></B></U>(��������� (������ �����������) � ����������� ������������)<BR>�������� ����� <U>125252, �. ������, ��. ������������, �. 20/10 ���. 1.�</U></P>
<P align=right>��� ����� 0409808</P>
<P align=right>�����������/�������</P>
<TABLE cellSpacing=1 cellPadding=4 width=652 align=center border=1>
<TBODY>
<TR>
<TD vAlign=center width="6%">
<P align=center>&#8470;</P>
<P align=center>�/�</P></TD>
<TD vAlign=center width="65%">
<P align=center>������������ ����������</P></TD>
<TD vAlign=center width="14%">
<P align=center>������ ��</P>
<P align=center>�������� ����</P></TD>
<TD vAlign=center width="14%">
<P align=center>������ ��</P>
<P align=center>������</P>
<P align=center>��������� ����</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>1</P></TD>
<TD vAlign=top width="65%">
<P align=center>2</P></TD>
<TD vAlign=top width="14%">
<P align=center>3</P></TD>
<TD vAlign=top width="14%">
<P align=center>4</P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=13>
<P align=center>1</P></TD>
<TD vAlign=top width="65%" height=13>
<P>����������� �������� (�������), ���. ���.</P></TD>
<TD vAlign=top width="14%" height=13>
<P align=right>5 415 840</P></TD>
<TD vAlign=top width="14%" height=13>
<P align=right>4 518 340</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>2</P></TD>
<TD vAlign=top width="65%">
<P>����������� �������� ������������� ����������� ������� (��������), �������</P></TD>
<TD vAlign=top width="14%">
<P align=right>13,5</P></TD>
<TD vAlign=top width="14%">
<P align=right>12,5</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>3</P></TD>
<TD vAlign=top width="65%">
<P>����������� �������� ������������� ����������� ������� (��������), �������</P></TD>
<TD vAlign=top width="14%">
<P align=right>10,0</P></TD>
<TD vAlign=top width="14%">
<P align=right>10,0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>4</P></TD>
<TD vAlign=top width="65%">
<P>��������� ������ �� ��������� ������ �� ������, ������� � ������������ � ��� �������������, ���. ���.</P></TD>
<TD vAlign=top width="14%">
<P align=right></P>
<P align=right>358 988</P></TD>
<TD vAlign=top width="14%">
<P align=right></P>
<P align=right>328 633</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>5</P></TD>
<TD vAlign=top width="65%">
<P>���������� �������������� ������ �� ��������� ������ �� ������, ������� � ������������ � ��� �������������, ���. ���.</P></TD>
<TD vAlign=top width="14%">
<P align=right></P>
<P align=right>358 988</P></TD>
<TD vAlign=top width="14%">
<P align=right></P>
<P align=right>328 633</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>6</P></TD>
<TD vAlign=top width="65%">
<P>��������� ������ �� ��������� ������, ���. ���.</P></TD>
<TD vAlign=top width="14%">
<P align=right>76 433</P></TD>
<TD vAlign=top width="14%">
<P align=right>94 318</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>7</P></TD>
<TD vAlign=top width="65%">
<P>���������� �������������� ������ �� ��������� ������, ���. ���.</P></TD>
<TD vAlign=top width="14%">
<P align=right>76 433</P></TD>
<TD vAlign=top width="14%">
<P align=right>94 318</P></TD></TR></TBODY></TABLE>
<P>������������ ��������� ������� ����� �������� _______________<BR>��� &#8220;����������&#8221; (�.�.�.) (�������)</P>
<P>�����<BR>������</P>
<P>������� ��������� ������� ������� ������������� _______________<BR>��� &#8220;����������&#8221; (�.�.�.) (�������)</P>


</body>
</html>

