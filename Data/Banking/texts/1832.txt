<!--
ID:37119186
TITLE:����� &#151; ������: ��, ��� ��� ���������
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:������ �������
NUMBER:6
ISSUE_DATE:2006-06-30
RECORD_DATE:2006-12-29


-->
<html>
<head>
<title>
37119186-����� &#151; ������: ��, ��� ��� ���������
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgwhite>
<TD></TD></TR>
<TR class=bodycolor>
<TD>
<P class=Main align=justify><I>� ������� ��������� ���� � ��������� ������������� �������� �������������� � ���������������� ������� � �������� ��������� ������������ ������ &#171;����� &#8212; ������: ���������� � ��������&#187;. �� 2 �� 4 ��� ��������� ���� � ������ ���� ��������� � ������� ����� �� ���������� ���� � ��� ��������� �������� ����� &#171;������&#187; ������ �������� ������������� ������������� �������� &#171;����� &#8212; ������: ���������� � ��������&#187;.</I></P></TD></TR>
<TR class=bgwhite>
<TD>
<P class=Main align=justify>� ������ ��������� ������� ������� ����� ������� ��������� �� ���������� �����. ������ �������� ������� ������� ������� � ������ ��������� ���������� ��� &#171;���������� �������� ������&#187; <STRONG>��������� �������.</STRONG> ��������, ��� ��������� �� �����, 24 ����� � ������ ���������� ���������� ���������� � �������������� ����� ��� &#171;���������� �������� ������&#187; � ������������� ���������� �������������� � ����������������. ��� ����� �������� �������������� � �������� ������ ������������� ������������ ����� ��� � �������, ��������� ��������� �������������� �������������� � ����� ���������������� ����������, ���������� � ���������� �������� �������� ������������ �������������� � �������������� �������� �������� ������������� �������������� ������������ �������. �������� �������� ����� ������� ������ ����������� ������ ��������������.</P>
<P align=justify>� ������ �������� ������� ��� ���� ���������� ��������� ������� � ��������� ���������� ����� ������� ���������� � ����������� ���������� ������� �������� ����� ������� ������ � ����������� �� �� ������ ���������� � �������������� ����� ��� &#171;���&#187; � �� &#171;������� �������� ������&#187;. 
<P align=justify>������� ����������� � �������� ����������� ������ �������� ���������� �� �������� ������� �� ������. � ���������, ����� ������� ���� � ��� �� ���� ������� ������ ������� ����� ��������������� ������, �� ������ �� ������� ���������� ��������� �������. ���������� &#171;������������� �������� &#8470;6&#187; �������� ������ ������������ � ��������. ��� ������������� ������� ����� ����� ���������� � ��������������� ������� � ��������� ���������� ��������� ������ � ������ � ����� ����. 
<P align=justify>�� ���������� � �������� ���������������� ������� �������� � ������������� ����� ������� � ������ �������-������� ����� ������ <STRONG>����� �����.</STRONG> ������ �� ������� � ������� �� ������� � �������� �� ��������� ������������. �������, ����� ����� � ������ ������ ����� &#171;�������� �����������&#187;. ������, � ��������� ������� ���������� ���������������� �������� �� $20 ��������� ��������� �������� ��������������. ������ &#8212; � ������ ��� ������. 
<P align=justify>�� �����, ������ �������, ������ �������������� �������� � ����������, �������� � ������������. ������������ �������� ������������� ����������-�������� ������� ��� �������� ��������� ������� ����. �� ��� ����� ������� ������� ��������� � ����� ����������� ����� ����-��������� ����, ������������ ��������� &#171;��������� �����&#187; <STRONG>������ ��������.</STRONG> �� ������� � ������������� �������� ������ ����������� �����, ������� ��������� �������������� �������������� ����� ������������ ��� ���������� � �������� �������. 
<P align=justify>������ ���� ���������� � ���� ���� ���������� � ����������� ���������. ��� �� ������� ��, ���� �� ����� ������� ������������ � ������ &#8212; ��������� � ������� �� ������� ������� ������������� ��������������. ��, &#171;�����&#187; ������ ����������� ��������� ��������, �� ���� ��� ����� &#171;�����&#187; ��� ����������� ������� �����. &#8230;�������, ����� �� ���� ���������: ���� ����� �������, ���� &#8212; ���������������, ���� � �����������.</P></TD></TR></TBODY></TABLE>


</body>
</html>

