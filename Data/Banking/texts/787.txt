<!--
ID:38437036
TITLE:��������������� � ������������������ ������� ������
SOURCE_ID:10030
SOURCE_NAME:�����: ������� ����. ����� ���
AUTHOR:����� ������ &#150; �.�. ��������, ����. ����. ����, ����� ���
NUMBER:3
ISSUE_DATE:2007-06-30
RECORD_DATE:2007-11-20


-->
<html>
<head>
<title>
38437036-��������������� � ������������������ ������� ������
</title>
</head>
<body >


<DIR>
<DIR>
<DIR><B><FONT></FONT></B></DIR></DIR></DIR>
<P align=justify><FONT>� �������, �������������� � ����������� ����� ������� &#8220;Revue Banque&#8221;, ��������������� ������� ��������� �������� ������� � ���������� � ���������� �����. ������ �������� ��������� �������� ������� ����������� ������������ � ������ ��.</FONT></P>
<P align=justify><FONT></FONT></P>
<P align=justify><FONT>� ��������� ���� ������� ������������ � ����� ������� � ���������� (���) ����������� ���������: � 2006 �. ����� ��������������� ������ �� ���� ���� ������ ��������������� ����� � 3800 ���� ���� (���������� ������ 2000 �. ��� ����������� ��������), � ��� ����� 1198 ���� ����������� �� ���� ������. �� ������ ����������� ��������� Dealogic, � ��������� ����� �������� �� ��� � ���������� ����� ��������� � 2006 �. 166,2 ���� ����, � ��� ����� � ���������� ������� 119,2 ����, ��� 79%. ����������� ��������� DBRS �������� ������ � ���, ��� � 2006 �. ����������� ����� ��������� 32 ���� ���� �� 73 �������� �� ���, � ��� ����� �������� � ����� ��������� ������ ���� ������������ � 15 �������, � � ����� �������������� ������������������ ���������� ����� (��������� � ��������������� �������� � ��.) &#8211; � �������� �������.</FONT></P>
<P align=justify><FONT>� 2007 �. ���� ������� �����������. � ������� 1 ���������� ������ � 10 ���������� �������� ������, ����������� � ������ � 1 ������ �� 15 ������� 2007 �. � ��� ����� ������������ ������ � 10 ����� ������� �������� ������ � 2006 �. � � ����� �������� ������� ��������� �� ���, ������� ����������� ����������� ����� � 20052006 ��.</FONT></P>
<P align=right><FONT>������� 1. </FONT></P><B>
<P><FONT>���������� �������� �� ������� � ���������� ������</FONT></P></B>
<TABLE cellSpacing=1 cellPadding=7 width=638 border=1>
<TBODY>
<TR>
<TD vAlign=top width="41%"><B>
<P align=center><FONT>����-����������</FONT></B></P></TD>
<TD vAlign=top width="38%"><B>
<P align=center><FONT>������������� ����</FONT></B></P></TD>
<TD vAlign=top width="22%"><B>
<P><FONT>�����</FONT></P></B>
<P align=center><FONT>(��� ����)</FONT></P></TD></TR>
<TR>
<TD vAlign=top colSpan=3><B><I>
<P align=center><FONT>�������� �� ������� ��������, �������������� � 1 ������ �� 15 ������� 2007 �.</FONT></B></I></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>Marfin Popular Bank (����)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>Bank of Cyprus (����)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>6117,15</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>Piraeus Bank (������)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>Marfin Popular bank (����)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>3382,15</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>Merrill Lynch (���)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>First Republic Bank (���)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>1393,20</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>Citigroup (���)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>Egg (��������������)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>871,62</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>Groupe Banque Populaire (�������)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>Foncia groupe (�������)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>803,27</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>Swedbank (������)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>TAS Kommerzbank (�������)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>761,31</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>Banco Bradesco (��������)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>Banco BMC (��������)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>287,99</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>KBC Group (�������)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>DZI Insurance (��������)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>260,0</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>Colonial bancGroup (���)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>Commercial Bankshares (���)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>244,63</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>Banco Sabadell (�������)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>TransAtlantic Bank (���)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>134,84</FONT></P></TD></TR>
<TR>
<TD vAlign=top colSpan=3><B><I>
<P align=center><FONT>10 ���������� ������� ������ � ���� � 2006 �.</FONT></B></I></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>Banca Intesa (������)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>Sanpaolo IMI (������)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>29600,70</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>Wachovia Corp. (���)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>Golden West Financial Corp. (���)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>20218,95</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>Natexis Banques Populaires (�������)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>CNCE (�������)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>17800,00</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>Bank of New York Inc. (���)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>Mellon Financial Corp. (���)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>12375,00</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>Capital One Financial Corp. (���)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>North Fork Bancorp. (���)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>10001,64</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>BNP Paribas (�������)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>BNL (������)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>8971,38</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>Banca Popolare di Verona (������)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>Banca Popolare Italiana (������)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>8180,69</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>Regions Financial Corp. (���)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>AmSouth Bancorp. (���)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>7713,53</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>Banche Popolare Unite (������)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>Banca Lombarda (������)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>6427,57</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%"><FONT face="Times New Roman">
<P><FONT>Cr&#233;dit Agricole</FONT></FONT><FONT> (�������)</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>Cariparma (������)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>5145,00</FONT></P></TD></TR>
<TR>
<TD vAlign=top colSpan=3><B><I>
<P align=center><FONT>���������� �������� ����������� ������ � 2005-2006 ��.</FONT></B></I></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>Natexis Banques Populaires </FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>CNCE (�������)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>17800,00</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>BNP Paribas</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>BNL (������)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>8971,38</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>BNP Paribas</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>Commercial Federal Corp. (���)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>1120</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%"><FONT face="Times New Roman">
<P><FONT>Cr&#233;dit Agricole</FONT></FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>Cariparma (������)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>5145</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%"><FONT face="Times New Roman">
<P><FONT>Cr&#233;dit Agricole</FONT></FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>Enporiki (������)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>2100</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P><FONT>Caisse d&#8217;Epargne</FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>CNCE (�������)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>5400</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="41%"><FONT face="Times New Roman">
<P><FONT>Soci&#233;t&#233; G&#233;n&#233;rale</FONT></FONT></P></TD>
<TD vAlign=top width="38%">
<P><FONT>HVB Spliska Banka (��������)</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>1000</FONT></P></TD></TR></TBODY></TABLE>
<P align=justify><FONT>������ �������� ���������� ����� �������� �� ��� �������� ��������� ���� �������� � ���������� ������ � 2006 �., ��� ��������� �� �������� �������� �� ����� ��������.</FONT></P>
<P align=right><FONT>������� 2</FONT></P><B>
<P align=center><FONT>������ ������� ���� ���������� ������ � 2006 �.</FONT></P></B>
<P align=center><FONT></FONT>
<CENTER>
<TABLE cellSpacing=1 cellPadding=7 width=523 border=1>
<TBODY>
<TR>
<TD vAlign=top width="47%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="26%"><B>
<P align=center><FONT>�������</FONT></P></B>
<P align=center><FONT>(���� ����.)</FONT></P></TD>
<TD vAlign=top width="26%"><B>
<P align=center><FONT>������� �� ��� </FONT></B><FONT>(� %)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P><FONT>Citigroup (���)</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>21,54</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>&#8211; 12,0</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P><FONT>Bank of America (���)</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>21,13</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>28,0</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P><FONT>JP Morgan Chase (���)</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>14,44</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>67,0</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P><FONT>ING (����������)</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>10,11</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>6,7</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P><FONT>Grupo Santander (�������)</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>9,99</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>22,1</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P><FONT>UBS (���������)</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>9,91</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>&#8211; 12,7</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P><FONT>BNP Paribas (�������)</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>9,61</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>24,9</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P><FONT>Goldman Sachs (���)</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>9,54</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>70,0</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%"><FONT face="Times New Roman">
<P><FONT>Cr&#233;dit Suisse Group (</FONT></FONT><FONT>���������)</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>9,16</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>94,0</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P><FONT>Wells Fargo (���)</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>8,48</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>11,0</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P><FONT>Deutsche Bank (��������)</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>7,87</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>70,0</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P><FONT>Morgan Stanley (���)</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>7,47</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>51,0</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P><FONT>Soci<FONT face="Times New Roman">&#233;</FONT>t<FONT face="Times New Roman">&#233; </FONT>G<FONT face="Times New Roman">&#233;</FONT>n<FONT face="Times New Roman">&#233;</FONT>rale (�������)</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>6,86</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>18,6</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P><FONT>BBVA (�������)</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>6,23</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>24,4</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P><FONT>ABN Amro (����������)</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>6,20</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>7,6</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P><FONT>Commerzbank (��������)</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>2,10</FONT></P></TD>
<TD vAlign=top width="26%">
<P align=center><FONT>35,9</FONT></P></TD></TR></TBODY></TABLE></CENTER>
<P align=justify><FONT>��� ���������� ������ ����. 1, ����� ������� �������� �� ��� �������������� � ��������� ���� � ������������ ������, ������ �������� �������� ������ ����������� ��������������� � ������������������ ��������. �� ������ �. ������� (�. P<FONT face="Times New Roman">&#233;</FONT>rbereau), ���������� ����������� ���������� ��������� (F<FONT face="Times New Roman">&#233;</FONT>d<FONT face="Times New Roman">&#233;</FONT>ration bancaire europ<FONT face="Times New Roman">&#233;</FONT>enne), ��������� ��� �������� ������ ����� ���������� ������������ � ������������ ���������. ���� ������� ����������� � &#8220;����� ������� ��������, ��������� � ������������ ������ ���������� ������ �� ���� �������� ���������, � ���������� ����������� ��������� �����������, � ���������� ���������� ���������, ������� ������������ ��� �������������� �������� �����&#8221;. ��� �������� ����������� �������, �� ������������ ����� ���, � ��� ����� � �� ������������� ������, ���������� �� &#8220;�������� �������������� ���������&#8221; �� ���� �������� ��������, ��������� � ����������� ������������ ������, ����������� ����������� ��������, ��������������� ����� �����, ��������� ���������� �������������� ��������. ���, ���������� ����������� ���� BNL, ����������� ���� BNP Paribas ������� � �������� ����� ������� ���� � ������ ������ ��������� ��������� � ����� � � ����� ������������� �� ������� ���������� ����. � ����� ����������� ���� ������������� ��� �������� ��� ��� � �������� &#8220;������� ��������� �����&#8221; � ������ � �������� ������������� � ����������� ���������� � ��� ���� ���������� � ���������������� � ������ �� ����� ���������� �����.</FONT></P>
<P align=justify><FONT>� ��� ������� ������������ � ���������� ������� ����������� � ������ 1990-� �����, ����� ���� �������� ������ ���������� (McFadden Act) � ������&#8211;�������� (Glass Steagall Act), ��� ������� ���� ��� �������� � ��� ������������� ������, ����������� ������� ��������������� � ���������� ����� � ���������� ��������, � ��������� ��� ������ �����������, �������� �� ���� ������� ������. ������� ������������ � ���������� ������� ��� ������ ������: ������� �� ���� ������ ������� ������ ���������� 40% ���������� ������� (� �� ��������������� ���������� ���������� 25%). ����������� ����� ��� � �� � �� ������ ��������
���. ���, �������������� ����� (marge d&#8217;intermediation), �������������� ��� ��������� ������� ����� ����������� ������ � ������������ �� ���������� � �������, ���������� ���������� �����, ���������� � ������������ ������ � ������� 3%, ����� ��� � ��������� &#8211; 1,9%, ���������� &#8211; 1,6%, � �������� � ����������� &#8211; ����� 1%. ����������, ���������� ��������� ���������������� �������� � ������� ����������� �������� (coefficient d&#8217;exploitation), ��������� � ������������ ������ 57%, ���������� &#8211; 53% (��������� ���������� � ����), ����� ��� � ����������� &#8211; 63,4% � � �������� &#8211; 74%. ������������ ����� ����������� ��������� ������������������ �� ������ �� ������������� (��. ����. 3).</FONT></P>
<P align=justify><FONT>��������� � ��-15 ������������� ����� 7000 ������, � � ��� &#8211; 9000, � ������ �� � ��� ����� ������������ ������� �������� ������������ � ���������� �������. ������������ ������������ ����� ����� �������� ����������� �����������, ��� ����� ��� � ��� ��������� ����������� ��������� �������������� ����� 10% ����� ����� �������. ������ ������� ����� ������������� � ������������ ������� &#8220;�� ������&#8221; ����� �������� ���� �����, ����� ���������� ������� �������������� ������� ����������� ������ ����� �����, ���, � ���� �������, ������ ��������� ������ ����� �������� � ������ �� ������� ����� ��������� ���������� �����. ����� ����� ����� ������� �������� ��� ������� ����������� ������: ����������� ������������ ����� �������� �� ���� ��������� ���������� ����� �� 50% �� 80% ����� �������.</FONT></P>
<P align=justify><FONT>� ���� �������, ����������� ����� ������� ��������� �� ����� ���������� ��������� � ����� ���. ������ ������ ������������ ������������ � ����������� ������ � ��������� ����� ������ ���������� ������ ����� ����������� � ��������� ������ (������, �������, ��������), ���� (�����, �����, ����� �����, �������) � �������� ������. </FONT></P>
<OL>
<OL>
<OL>
<OL><B>
<P align=right><FONT></FONT>
<LI><FONT>������� 3</FONT></LI></OL></OL></OL></OL>
<P align=center><FONT>���������� � ���� ����� �� ������ �������� �������������<BR></FONT></B><FONT>(���� ����., �� 14.02.2007 �.)</FONT></P>
<CENTER>
<TABLE cellSpacing=1 cellPadding=7 width=393 border=1>
<TBODY>
<TR>
<TD vAlign=top width="10%"><FONT>&nbsp;</FONT></TD>
<TD vAlign=top width="60%"><B>
<P align=center><FONT>�����</FONT></B></P></TD>
<TD vAlign=top width="30%"><B>
<P align=center><FONT>���� ����.</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="10%">
<P><FONT>1</FONT></P></TD>
<TD vAlign=top width="60%">
<P><FONT>Citigroup (���)</FONT></P></TD>
<TD vAlign=top width="30%">
<P align=center><FONT>266,2</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%">
<P><FONT>2</FONT></P></TD>
<TD vAlign=top width="60%">
<P><FONT>Bank of America (���)</FONT></P></TD>
<TD vAlign=top width="30%">
<P align=center><FONT>242,7</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%">
<P><FONT>3</FONT></P></TD>
<TD vAlign=top width="60%">
<P><FONT>ICBC (�����)</FONT></P></TD>
<TD vAlign=top width="30%">
<P align=center><FONT>211,9</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%">
<P><FONT>4</FONT></P></TD>
<TD vAlign=top width="60%">
<P><FONT>HSBC (��������������)</FONT></P></TD>
<TD vAlign=top width="30%">
<P align=center><FONT>208,9</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%">
<P><FONT>5</FONT></P></TD>
<TD vAlign=top width="60%">
<P><FONT>JP Morgan Chase (���)</FONT></P></TD>
<TD vAlign=top width="30%">
<P align=center><FONT>178,4</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%">
<P><FONT>6</FONT></P></TD>
<TD vAlign=top width="60%">
<P><FONT>Bank of China (�����)</FONT></P></TD>
<TD vAlign=top width="30%">
<P align=center><FONT>146,5</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%">
<P><FONT>7</FONT></P></TD>
<TD vAlign=top width="60%">
<P><FONT>China Construction Bank (�����)</FONT></P></TD>
<TD vAlign=top width="30%">
<P align=center><FONT>138,3</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%">
<P><FONT>8</FONT></P></TD>
<TD vAlign=top width="60%">
<P><FONT>UBS (���������)</FONT></P></TD>
<TD vAlign=top width="30%">
<P align=center><FONT>132,7</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%">
<P><FONT>9</FONT></P></TD>
<TD vAlign=top width="60%">
<P><FONT>MUFJ (������)</FONT></P></TD>
<TD vAlign=top width="30%">
<P align=center><FONT>131,9</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%">
<P><FONT>10</FONT></P></TD>
<TD vAlign=top width="60%">
<P><FONT>RboS (��������������)</FONT></P></TD>
<TD vAlign=top width="30%">
<P align=center><FONT>129,2</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%">
<P><FONT>11</FONT></P></TD>
<TD vAlign=top width="60%">
<P><FONT>Wells Fargo (���)</FONT></P></TD>
<TD vAlign=top width="30%">
<P align=center><FONT>121,6</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%">
<P><FONT>12</FONT></P></TD>
<TD vAlign=top width="60%">
<P><FONT>Grupo Santander (�������)</FONT></P></TD>
<TD vAlign=top width="30%">
<P align=center><FONT>119,9</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%">
<P><FONT>13</FONT></P></TD>
<TD vAlign=top width="60%">
<P><FONT>Wachovia (���)</FONT></P></TD>
<TD vAlign=top width="30%">
<P align=center><FONT>116,4</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%">
<P><FONT>14</FONT></P></TD>
<TD vAlign=top width="60%">
<P><FONT>BNP Paribas (�������)</FONT></P></TD>
<TD vAlign=top width="30%">
<P align=center><FONT>106,3</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%">
<P><FONT>15</FONT></P></TD>
<TD vAlign=top width="60%">
<P><FONT>Barclays (��������������)</FONT></P></TD>
<TD vAlign=top width="30%">
<P align=center><FONT>100,0</FONT></P></TD></TR></TBODY></TABLE></CENTER>
<P align=justify><FONT>��� ���������� ����������� ������, ��������� ����� �������� ������� ����� �� ������ �������� �������������, ��� ������ �� �������� ����������� ������������ � ������������������ ������ � �������� ������������� �������. � ������ 2007 �. ������������ ����� ICB� (Industrial &amp; Commercial Bank of China) �������� � ��������� ���������� ������������� ���� Bank Halim &#8211; ��� ������ ��������, ��� ������� ��������, �������� ��������� ������ �� ������������ ���������� ��������� ����������, �� ������� ��������� � ������, ����� ����������. � ����� ��������� &#8220;�����&#8221; �������� �������� �������� ����� ����������� &#8220;�������&#8221;, ��� Societe Generale, Dexia, Commerzbank, Barclays � ��. ��������, ��� ������� ��������� ������������ ������� ����� ������. ������������ ����� ���������� ����� ��� ���������� &#8220;���������� ��������&#8221;. � 2006 �. ����� �������� �������� ������ ���������� ������ � 498 ���� ����., ��� �� 87% ������ ������ 2005 �. ��� ���� ������ 2/3 �������� ���� ����������� �����.</FONT></P>
<P align=justify><FONT>�. �������� (Ch. McCreevy), ���� ����������� �������� (��), �������� ��������� ������� ����������� ����� ��, �������, ��� ������������ ����������� ������� �� �������� ���������, � ������ ��������� ��������� ������������� ����������� ��������� � ��������� �������������� ������� �����������. � ���� ����� ������ ������ ���������� ����� �� �������� �������� �����������, ��� ������������� �������� ���������� ��������� ��� ������� �� ���� �������� ������� ����������� ����� ���������� �����. ������ �����, ��� �������� ����� ��������� ���������� �����, ������� � ������� �� �������� ����������� ����� ���� �������� ������ �����������������. �� ����������� ���� ��������������� ���������� ������� ������������ ������� ��.</FONT></P>
<P align=justify><FONT>� ������������ � ����������� ������� ������������ ���� (1986 �.) ������ ���������� ����� �� ������ ��� ������������ � 1 ������ 1993 �. ������ �� �� ������ �� ��� ���. � �������� 2004 �. ����� ��������� ��������� � �������� ����� �� (������) ������� �� ������� ������� � �����������, ������� �� ���� ������������ ������������ ����������� �����. �� ������ ������ � ������ � ������������� �������� ������� ��������� �������� �� ��������� � ������������ � ���������� �����. �� ������������ ��������� �������� �� ��������������� ������� ������ ����� ���� ���������, ���� ��� ������� � ���������� ������. ��� ���� ���� ������������ ���������������� ����� ���������� 3 ������. �� ������ ���������, ���������������� ��������� ���������� �������� �� ������� �������� � ����, ��� ������ ����� ������� ����������� �� &#8220;�����������������, � �� �������������� ������������&#8221;.</FONT></P>
<P align=justify><FONT>12 �������� 2006 �. �� ���������� �������� ����������� ������ ������� �������� ��� ��������������� ���������� �� ���, ������� �������� �� ������ �����, �� � ��������� � �������������� ��������, �. �. �� ���� ��� �������� ����������� �����. ������ ��������������� ������ �������� �� ������ ��������� ���������:</FONT></P>
<UL>
<UL>
<P align=justify><FONT></FONT>
<LI><FONT>��������� � ���������� ��������� ����������;</FONT></LI>
<P align=justify><FONT></FONT>
<LI><FONT>��������� ��� �������������;</FONT></LI>
<P align=justify><FONT></FONT>
<LI><FONT>���������� ���������� ��������;</FONT></LI>
<P align=justify><FONT></FONT>
<LI><FONT>���� ��������� �����;</FONT></LI>
<P align=justify><FONT></FONT>
<LI><FONT>���� �������������� ����������.</FONT></LI></UL></UL>
<P align=justify><FONT>����������������� ������ � ���������� ��������� ����������� �� �������� ���, � ���� �� ������������ �����������. ������ ������� ������� ������� ��������� � ���������� ������ ��, � ��� ����� � ������. 13 ����� 2007 �. ����������� ��������� ������� ������������ �� ������. �������� � ���� ����� ��������� ��������� ����� ������ ������ ����� � �������� ���������-���������� ������. ������ ���� ����������� � ���������� ����������� ��� �������� ������� ����� ��������� ���������� �����. ��� ������ ���� ���������� � �������� ������������ � �������������� �� &#8220;����� ����� � ���������� ������� (2005&#8211;2010 ��.)&#8221;. ���� ����� ��������� �������������� ���������� � ������� ������������ ������������ � ������ ������� �� ��������� ������� � ������������ ���������������� ������������, ����������� ���������� ������ � �. �. �������, ��� �������� ������, ��������� � ������� �� ����������� ���� ������� ������������� ���������� ������������, ������� ��������������� � ������ ������������. � ���� �� ������� �������� ���������������� ������������ � ����������� ������� ��������: ����� ��������������� ��������, ���������� ������������ ���������������, ���������� 1/3 ��������������� ����� � ���; ����� ��������������� �������� � ������������ ����� �������� ������ ����� ��������� �� ����� � ������� ����� ������ � � ������ ���� &#8211; � ������� ����������� ������.</FONT></P>
<P align=justify><FONT>������ ��������� ������ � ������� ������������ ������� ����������� ������������ � ������ �� &#8211; ����������� ������������� � ������������ ���������� �������. ��� �������� � ������� ���������� � �������� � 1 ������ 2007 �. ��������� � ������������� ��������, ������� ������������� ����������� ������-2 � ������������ ��������� ������ ��������������� ������� �� 2011 ����. </FONT></P><B>
<P align=center><FONT>����������:</FONT></P>
<OL></B><FONT face="Times New Roman">
<P align=justify><FONT></FONT>
<LI><FONT>Fulconis-Tielens A. Fusions &amp; acquisitions bancaires: Paneurop&#233;ennes ou transatlantiques // Rev. Banque. &#8211; P., 2007. &#8211; N 689. &#8211; P. 26 &#8211; 27.</FONT></LI></FONT>
<P align=justify><FONT></FONT>
<LI><FONT>Casa &#8211; Emporiki: les dessous du deal // Ibid. &#8211; P. 28 &#8211; 31.</FONT></LI><FONT face="Times New Roman">
<P align=justify><FONT></FONT>
<LI><FONT>McCreevy Ch. La consolidation du secteur est garante d&#8217;une plus grande efficacit&#233; // Ibid. &#8211; P. 32 &#8211; 34.</FONT></LI>
<P align=justify><FONT></FONT>
<LI><FONT>D&#8217;Etigny H. Controle prudential des operations transfrontali&#232;res // Ibid. &#8211; P. 35.</FONT></LI>
<P align=justify><FONT></FONT>
<LI><FONT>P&#233;rbereau M. Comment expliquer l&#8217;ampleur des acquisitions paneurop&#233;ennes // Ibid. &#8211; P. 36-38.</FONT></LI>
<P align=justify><FONT></FONT>
<LI><FONT>Quignon L. Quels atouts pour les banques fran&#231;aises // Ibid. &#8211; P. 39.</FONT></LI>
<P align=justify><FONT></FONT>
<LI><FONT>Pujals G. Un record &#224; battre! // Ibid. &#8211; P. 40-42.</FONT></LI>
<P align=justify><FONT></FONT>
<LI><FONT>Villeroy de Galhau F. Int&#233;r&#234;ts pour les SFS: Tendance de fond // Ibid. &#8211; P. 44-45.</FONT></LI></FONT>
<P align=justify><FONT></FONT>
<LI><FONT>Ceddaha</FONT><FONT face="Times New Roman"><FONT> F., Bellen J.-B. Banque &#8211; assurance et plus si affinit&#233;? // Ibid. &#8211; P. 46-48.</FONT></LI>
<P align=justify><FONT></FONT>
<LI><FONT>Pujals G. La ru&#233;e vers l&#8217;immobilier // Ibid. &#8211; </FONT></FONT><FONT>�. 10-11.</FONT></LI></OL>
<DIR>
<DIR>
<P><FONT></FONT></P></DIR></DIR>


</body>
</html>

