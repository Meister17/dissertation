<!--
ID:37637812
TITLE:82.17 ���������� ���������� 
SOURCE_ID:3205
SOURCE_NAME:������� ��������� ���. �����
AUTHOR:
NUMBER:7
ISSUE_DATE:2006-07-31
RECORD_DATE:2007-05-11


-->
<html>
<head>
<title>
37637812-82.17 ���������� ���������� 
</title>
</head>
<body >


<B>
<P align=justify><FONT size=+0>04200513587</FONT>&nbsp;</B><FONT>����������� ������ ����������. <B>��������� ������ � ������������ ��������� ���������: </B>���... ����. ����. ���� /������ "���������� ��������������� ���������� �����������" (����). - �������� 2005.05.13.<BR>��� 35.08. 175 �.: 16 ���., 10 ��. - ��������.: 137 ����.<BR>����: ����������� ���������-������������� �������� ������������� ���������, � ����� ������������ ������������� ������������� � �������� �������� ��������� � ������� ���������� ������� �� ��������� ����������� ������� � ����������� ������ ��������� ��� ������ ����������� �� ����������. �������� ������������ ����� �������� ��������� ��� ������������� �������� ������������ ������������� ��� � ����� � �������� � ����� ��� ��������� ������� ����������� �� �������� ���������. ���������� ����������� ����� �������� (����������, ��������������, �����������). ������� ������� ����������� ��������� (������������� � ������� ����������), ������������� ��� ������������ ������������� �������� � �����. ��������� � ��������� �������� �������� �������� � ����� �� ����������������� ������ (������������ ������� ��������� � ������������� ���� ��������������� ��������). �������� ������������ ������������ ���� ��������� � ����� ����� �������� ����� ������� ����������; �������� ����������� �� ������ ������� ��������� � ����������������� �������, �� � ����� ��������� ��������� ���������� ���������, ��������� �������� ������� ����� ��������� ����� �������� ���������.</FONT></P><B>
<P align=justify><FONT size=+0>04200513662</FONT>&nbsp;</B><FONT>�������� �������� ����������. <B>���������� �������� ����������� �������������-������: </B>���... ����. ����. ���� /���������� ��������������� ����������� ����������� � ������������� (��������). - �������� 2005.04.25.<BR>��� 658.3:331.101.232. 174 �.: 17 ���., 14 ��. - ��������.: 160 ����.<BR>����: ��������� ���������-������������� ������������ ������������� ����� �������������-������ � ����������� �������� �������� ��������� �� ��������� ���������� � ������������ ��� ���������� � ������������� ��������. �������� �������� ������� � ���������� �������� ������������, ������������� � �������� ��������� ���������� �������������-������; �������� ��������� �������� � ���������� ������� "�������� ��������� �������������-������"; ���������������� ������� ��������� ��������� ���������� �������������-������; �������� ��������� �������� ����� �����������; ������� ������� ��������������� ����������� ������������ ���������� ��������� ������������� � ������� �������������-��������; ����������� ��������� ��������� ��������� �������������-������ � �������� ���������-������������� ������������� ������������ ���������.</FONT></P><B>
<P align=justify><FONT size=+0>04200514828</FONT>&nbsp;</B><FONT>�������� ����� �������. <B>��������� � �������������� ����� ��������������� ��������� �����������: </B>���... ����. ����. ���� /�������� ��������� ���������� ��������� ��� (�� ��� ���). - �������� 2005.05.23.<BR>��� 331.101.3:658. 196 �.: 14 ���., 15 ��. - ��������.: 110 ����.<BR>����������� �������� ��������� ����� ����������� �������� ����� ���������� � ����������� ���������� �� ������ ���������� � �������� ����� ������� ������������ �����������. ����: ���������� ���������-���������������� �������� � ����������������� ������� ��������� � �������������� ����� ��������������� ��������� �����������. �������� ��������� "�������������� ����", "��������� ��������������� �����" � "�������������� ��������������� �����". �������� ������������� ������� �������������� ������������ � �����������. �������� ����������� ������������, ���������, ������� � �������� ����� �������������� ���������� � ��������� ������������� �����������. ����������� ����������� �������� ������, ��������� � �������������� ����� ��������������� ���������.</FONT></P>


</body>
</html>

