<!--
ID:36872649
TITLE:���� ������� � ������?
SOURCE_ID:10908
SOURCE_NAME:������������ ���������� ������
AUTHOR:
NUMBER:10
ISSUE_DATE:2006-10-31
RECORD_DATE:2006-10-31


-->
<html>
<head>
<title>
36872649-���� ������� � ������?
</title>
</head>
<body >


<DIV class=article>
<DIV class=header>
<H2><FONT size=3>������������ ������������ ����� ���������</FONT></H2></DIV>
<DIV class=lead><STRONG>�������� ����, ������ ������ ������������ �������� �������������� ��������������� � ����������� �� �������� ������. ����� ����, ��� ������ - ������� �� ������� ���� ������������ ��� ����������� � ����������������.</STRONG></DIV>
<DIV class=body>
<P class=vrez align=justify>�� �����</P>
<H2><FONT size=3>6-7 ������, ������, �����������, ��� </FONT></H2>
<P align=justify><B>4-� ��������� ����� �� ��������� ���������� ������������� �������</B></P>
<P align=justify><B>�����������: </B>SourceMedia Inc., C&amp;E</P>
<P align=justify><B>��������� �������:</B> $1495</P>
<P align=justify><B>������ ���� �����:</B> ������������ ����� ������ �� ���� �������� � ���������� ���������� ���� � �����������. ��������� ���� ����������� ��������� ����� �������� � �������� �������������, ���������� �������� ���������� � �. �. � ���� �������� ����� ���������� ��������� ����� � ����� ��� ��� �����������.</P><B>
<P align=justify>��������� �����:</B> ������ - �� ������� ����, � ������������ � ��������� �� ��������. ���� ����� ������������� ����� � ��������� Boston Park Plaza Hotel &amp; Towers, ��� ����� ��������� �����. ������������ ����������, ��� ����� � ����������� ������ ��������� �������� � $300.</P>
<H2><FONT size=3>9-10 ������, ������, �������� </FONT></H2>
<P align=justify><B>����������� &#171;���������� �������� � ������ � ���&#187;</B></P>
<P align=justify><B>�����������:</B> Marcus Evans</P>
<P align=justify><B>������ ���� �����:</B> �������������� ������� ������������ �����������, ������������ ����������� ������������ ������ ����������� � ���������� ���������� �������� ��������� ������� � ����� ����������, ���������� ��������, ����������� ����������, � ����� ����������� �������������� ������.</P>
<P align=justify><B>��������� �����:</B> � ������ ������ ������������ ����������� ����� ������ ���� ����������� �����, ������� ������� �������� ������ �������� ���������. � ���� ���� ������� ����� ����� ���� ����� ��������� ��� ���������� �� ������ �����. �� ����� �� ������� ��������� �����. ������ � ��� ��������� ��, ��� �� ������������ � ������.</P>
<H2><FONT size=3>13-14 ������, ���������, ��������� </FONT></H2>
<P align=justify><B>1-� ������ ����������� �������������� � ����������� ������ � ���������</B></P>
<P align=justify><B>������������:</B> FINAS, ���, www.orgkomitet.com</P>
<P align=justify><B>������ ���� �����: </B>����� �������� ��������� ������ ������������� � ���������� �������, � ����� ����������� &#171;������������ �������� ���������&#187;, ������� �������� ��������� � ������ � 1994 �. ��� - ���� �� �������� ������������ ����������� �� ���������� �������� � ����. ����� ����������� ����� �������������, �������� �������, ��������� ������������� ����������, ����������� � ����������� �����, � ����� ������������� �� ��.</P>
<P align=justify><B>��������� �����:</B> ������ � ��������� � ������ �������� ����� ��, ��� � ������, - ����� ������� � ���������. ������ ��������� ���� �� ��������. ������� �������� ������� ����� �������� ������ ��������.</P>
<H2><FONT size=3>19-25 ������, ������, �������� </FONT></H2>
<P align=justify><B>����������� &#171;���������� ������������� � ������������� �����������, ���������, ��������� � ���������� ������������&#187;</B></P>
<P align=justify><B>�����������: </B>���</P>
<P align=justify><B>������ ���� �����:</B> ��������� ���������� ����������� ��� ����������� ������, ������� ��� ����� ���� ������ � ������������ ������������� ��� ������ ���������� �������� �� ���� �����. ���, ��� ����� ��������� � ���������� � �������� �������, ����� ��������� ���������� ������� �� �������. �� ��������� ����������� ��������� ������� ����������� �������� � ����������.</P>
<P align=justify><B>��������� �����: </B>��������� ��������� ������������� �� ������ ��� � ������������ � ������������ ���������-����������� ������������. �� �� ��� �� ������������� ��� ���������, ����������� � ���� �������� �����, � ������������ ����������� ����� ����������� ��� � ������ ����������������� ��������.</P>
<P class=vrez align=justify>�� �����</P>
<P align=justify><B>������ ���, ��� ������, ����� � ������ ���������� ����������� �� ����� ������ �� ����. ��� ������ �� ���. ������ ����, ����� ������ ������� �������, ����� ����������� � ������ ���� ����������� ����, ������ ��� ������ ������� ��� ����������� � ���������� ����� �����.</B></P>
<H2><FONT size=3>��������� ������ </FONT></H2>
<P align=justify>����-������ - ��� ����������� �� ������� � ������������ ��� ���� � ����� ���������� ������. ������ ������������ ������� ������ ���� ��������� ����� ��� ������� &#171;�������&#187;. ���������� ���� ������ &#171;����&#187; - Exployer � Atlantis - ���������� ����������� �������� ��������� � ����������� ������. ���������� ��������� �� �������, � ������ ��� ��������� ���������� �� �������. � ������� ��� ����� ������� ��� ���� �� ���� ��� � ��� ���� ��� ����������� �����, ��� ������������ ������ �������. �� �������, � ����� ������, ���������� �������������� � ������ - ����������� ������� ���� � ����������� ��� � ���������� �������, � ����� ��������� ������ � ����. ����� ���� ���� ����� ����������� ���������� ��������, ��������� �����, ��������, ����, ������. � ������ ����������� �������� �� ��� ��� � ��-��, ����� �������� ���� � ��������� ����� ����� ������� �������������� ���������� ����������� � ���������� ��������� �����: ������ � �����, � ����� ���������, ����� � ����������� ����. ������� ������ ����� � ����, ��� � ������ �������, �� ����� ����� �������, ��� ����� ������ ������������ ���������, ����������� ������� ������� ����� � ���������� �����.</P>
<P align=justify><B>��� ���������: </B>������� ������ - ������ ������ ��������� &#171;���������&#187; �� ��������� ���������� �� Boeing-767-200. ����� UN 9157/9158, ����� �� ������ � 22:05, ������ �� ������ � 11:30 + 1, ����� � ������� � 13:00, ������ � ������ � 19:20.</P>
<P align=justify><B>���� �������:</B> ������ ����-���� ������ &#171;����&#187; �� ���� ���� (������ ����������� �����, ������ - ���� �������) - &#1028;1200 � ����.</P>
<H2><FONT size=3>���� �� �������� </FONT></H2>
<P align=justify>� �������� ����� �������� ���� ���� ����, ����� �������� � ���������� ������ - �������� �������� ������ ���������. ��� � 1516 �. ����� �������� �����, �������������� ������ ������� ������ �� �����, ������ � �������. ������� � ������ ������ ���� ���� ����� ��������� ������. ������ ����������� � ������ ��������� ����� �����. &#171;�����������&#187;, ��� ����� �������� ����, ������ ��������� &#171;�� �����&#187; �� ����� ������ �����. ���� �� ������� ������� �����, ���� ���������, ���� ��� ������ ����. ������� ������� ���������� ���� ����, ������� ���������� ���������� ������� � ������������. ��� ���� ����������� �����������, ��� ��� ������ ����, ������� ����� ������ � ���������� �������, - &#171;���������� �����&#187;. ������ ��� � ������� ������� �� ������� ������, ������������ �������� ��� ����������. ����� �������� ���������� �������, � ��� ���� ��������� ��������� ����� - ���������, ����������� ��� �� ����. ������������� � ������ ���������� ����� �����: &#171;�������&#187; - ���� �� ��������, &#171;������&#187; - � ����� � &#171;�����&#187; - � ������.</P>
<P align=justify><B>��� ���������: </B>�������� �������� &#171;��������&#187; ������������ � ��������� Schonefeld, ������������� � ��������� ����� ������, � 20 �� �� ������.</P>
<P align=justify><B>���� �������: </B>����� � ������ ������� - � ������� &#1028;120, ��������� ������ � ������� ���� � ��������� - &#1028;25-30.</P>
<H2><FONT size=3>����������� � ����� </FONT></H2>
<P align=justify>���������� � ������� ����� ����� ��������� � ������� �����, � �������� ��� ��� ������, �� ������ 300 � ��� ������� ����. ������� ����� ���������� �������. ��������� ���� �������� ������������ �����, �� ���� ������� �������� ������ ��� ����������� ������� ������� ������������� ���������. ���������� �� ������ ����, ����� ������� ����� �� ����������� �����, ��������� ������� � �������� �������������� ���������� � ������� ����������� ��������. ��� �������, � ����� ������ ����������, ��� �� ���������, � ��������� ��� ���� ������. � ������ ���� ������� ��������� � �������� ������ �����, ���������� �������� � ��������� ���������. ����� ����� �������� � ��������������� ������, �� �������� �������� ���� ��������� $7 ���. ���������� ����� � �����, ��� ����������� � ��������� � ������� ������ ���������������� � ��������� �����������.</P>
<P align=justify><B>��� ���������:</B> ������� �� ������ � ����� ������������ ����� ��� ������������. � �� ����� ������������ Emirates, ������� �������� ������� ������ ������� � ��������. ������� �� ����� �� ����� ����� �� ������������ ���������� �� ������ E44.</P>
<P align=justify><B>���� �������:</B> ���������� � ����� 5* &#171;����� ����&#187; (��� ���������, ��������� ����, ���������-����, ������� � ������������ ������������ ����) - $135 � ����� �� ��������.</P>
<P class=capton align=justify>����� ��������</P>
<P class=vrez align=justify>�� �����</P>
<P align=justify></P>
<H2><FONT size=3>30 ������� - 2 ������, ������, �������������� </FONT></H2>
<P align=justify><B>4-������� ������� &#171;���������� � ����&#187;</B></P>
<P align=justify><B>��������� �������: </B>1999 �. ��.</P>
<P align=justify><B>�����������: </B>IASeminars</P>
<H2><FONT size=3>2-3 ������, ��������, ����� </FONT></H2>
<P align=justify><B>2-������� ������� &#171;����� ���������� ����&#187;</P></B>
<P align=justify><B>��������� �������:</B> $544</P>
<P align=justify><B>�����������:</B> IASeminars</P>
<H2><FONT size=3>4-10 ������, ���������� ������ </FONT></H2>
<P align=justify><B>���������� � ���������� �������� ���������� ������. �������� ���������������� ����������� �����</B></P>
<P align=justify><B>�����������: </B>&#171;������� �������-������&#187;</P>
<P align=justify>���.: (495) 765-36-15</P>
<H2><FONT size=3>13-15 ������, �����, ��� </FONT></H2>
<P align=justify><B>3-������� ������� &#171;����� ���� ��� ������������� �����&#187;</B></P>
<P align=justify><B>��������� �������:</B> $1500</P>
<P align=justify><B>�����������:</B> IASeminars</P>
<H2><FONT size=3>14-19 ������, ���������-��������-����� (��������-�������) </FONT></H2>
<P align=justify><B>������� &#171;������������ ������ �� ��������������� ����������� �������, ���������� ���������� �����, � �������������� ����������: ���������� ����&#187;</B></P>
<P align=justify><B>�����������:</B> ���, &#171;������� �������-������&#187;</P>
<P align=justify>���.: (495) 290-46-91, 765-36-15.</P>
<P align=justify></P></DIV></DIV>


</body>
</html>

