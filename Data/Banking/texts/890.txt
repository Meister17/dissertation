<!--
ID:36104025
TITLE:������������ ��������� ����������� (��������) � ����� �������� � �������������� ��������� ����
SOURCE_ID:3099
SOURCE_NAME:������ �����-����������
AUTHOR:
NUMBER:1
ISSUE_DATE:2006-03-31
RECORD_DATE:2006-04-14


RBR_ID:2039
RBR_NAME:���������� �������
RBR_ID:4928
RBR_NAME:��������� � ������� ��������
-->
<html>
<head>
<title>
36104025-������������ ��������� ����������� (��������) � ����� �������� � �������������� ��������� ����
</title>
</head>
<body >


<P align=justify>�� ����������� ������������ ������� ���������� �� IV ������� 2005 ���� �� ����� ���������� &#8470; 0409250 &#8220;�������� � ������������ ��������� ����������� (�� ��������) � ����� �������� � �������������� ��������� ����&#8221; ������������ ���������� ���������� ��������� �����������, �������������� ������� � ��������� ��������� ���� (���.1). ����� �� ��������� ������� �������� �������� ���� ���������� ������������� ����, �� ��������� �� 01.01.2006 ����������� 3,2 ���. ����, ��� ���� ������ �� ������������� ������ ��� ����� ���������� �������� ���������� �� 4,1 ����. ���. � �������� 67,6 ����. ���.</P>
<P align=center></P>
<P align=justify>�� ��������� �� 01.01.2006 � �����-���������� ������������� ����� 24-� ��������� ������, � ������ 11-�� �� ��� ������������� ����� ��������� �����. ����������� ����������� ��������� ����������� ����������� ����� ������������� ��������� ������ VISA Int. � MasterCard Int. 96,0&nbsp;% ���������� ���� ����������� � ������ ���� ���� ��������� ������ (���. 2), ����� ������, ����������� � �� �������������� �� ��������� �� 01.01.2006 ���������� 87,0&nbsp;% �� ������ ������ ��������, 5,6&nbsp;% �������� �������������� ����������� ���� ��������� ������� ��������������� ����� �� &#8220;�� ��������&#8221; (���.&nbsp;3).</P>
<P align=left></P>
<P align=right></P>
<P align=justify>����������� ����� (98,95&nbsp;%) �� 3,2 ���. ���������� ����, ������������� ���������� ������������� ������, ���������� �� ��������� �����. ���� ���������� ������������� ��������� ���� � ����� ������ ������� �� 01.01.2006 1,05&nbsp;%.</P>
<P align=justify>� �����-���������� ������� �������������� ������ ���������� �������� &#8211; ����������� ���������� ���� ��������� ������ &#8220;���������� �����&#8221; � EXI-Card. �� ��������� �� 01.01.2006 � ������ ���� ��������� ������ �������� 1139 ����.</P>
<P align=justify>�� IV ������� 2005 ���� ����� ���������� �������� �������������� ����������� ������ ���� �������� 3 256 ���. ���.</P>
<P align=justify>������ ������, ����������� � �������-��������� ����, � �������� �� ������ �������� �������� ������� � �������������� ��������� ���� �� IV ������� 2005 ���� ��������������� � ��������� ������� ������ �������� �� ������ �������� �������� ������� � ��������� ������ (���.&nbsp;4).</P>
<P align=center></P>
<P align=justify>�� ������ ������������� �������-����������� ����������� �������� �� ������ �������� �������� �������� �������. � IV �������� 2005 ���� ����� ����� ������ �������� �������� ������� �������� 87,5&nbsp;% �� ����� ������ �������� (��� ����� ���������� ��������).</P>
<P align=justify>�� ������, ������������� �������-������������� ����������� ������������� ������� ������� �������� �� ������ �������, �����, ����� � ������ ��������:</P>
<TABLE cellSpacing=1 cellPadding=7 width=648 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="41%" rowSpan=2>&nbsp;</TD>
<TD vAlign=top width="59%" colSpan=2>
<P align=justify>���� �� ������ ������ ����������� � IV ��. 2005 �. ������ (%)</P></TD></TR>
<TR>
<TD vAlign=top width="26%">
<P align=left>�� ������, ������������� �����������</P></TD>
<TD vAlign=top width="33%">
<P align=left>�� ������, ������������� �������������</P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P align=justify>1. ������ � �������� ����</P></TD>
<TD vAlign=bottom width="26%">
<P align=justify>5,7</P></TD>
<TD vAlign=bottom width="33%">
<P align=justify>38,5</P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P align=justify>2. ���������� �������</P></TD>
<TD vAlign=center width="26%">
<P align=justify>4.6</P></TD>
<TD vAlign=center width="33%">
<P align=justify>-</P></TD></TR>
<TR>
<TD vAlign=top width="41%" rowSpan=2>
<P align=left>3. ������ �������� ���. �������, �� ���:</P>
<P align=left>- ������ �������� � ��. ������</P></TD>
<TD vAlign=center width="26%">
<P align=justify>89,7</P></TD>
<TD vAlign=bottom width="33%">
<P align=justify>61,5</P></TD></TR>
<TR>
<TD vAlign=center width="26%">
<P align=justify>2,3</P></TD>
<TD vAlign=bottom width="33%">
<P align=justify>-</P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P align=justify>�����:</P></TD>
<TD vAlign=top width="26%">
<P align=justify>100%</P></TD>
<TD vAlign=top width="33%">
<P align=justify>100%</P></TD></TR></TBODY></TABLE>
<P align=justify>�� ����� ����� ������ �� ������ ������� � ����� 5,1&nbsp;% ���������� �� �������� ����������� � �������������� ����������.</P>
<P align=justify>������������� ���������� ��������� � ������������� �������� � �������������� ��������� ���� ����������� ���, ��� ����������� ���� �������� ��� ������� ���������� �����.</P>
<P align=justify>���������� �������-��������� ����� � �����-����������, ����������� ������ � �������������� ��������� ����, �� 01.01.2006 ��������� 8,5 ����� ���� (���.&nbsp;5).</P>
<P align=center></P>
<P align=justify>�� ��������� �� 01.01.2006 ���������� ����������� ���� � �����-���������� �������� 66 ��������� ����������� � �������. �� ��������� ������� ������ �� ������������� ����� ��������� ���� �� ���������� (���. 6, 7), ��� ������-�������� ���� ��������� ������, ���, ���������� ����, ���� �����-��������� � ���-���� ���. � ���������� ��������� ���������� �� ������ ������ ���������:</P>
<P align=justify>
<TABLE cellSpacing=1 cellPadding=3 width=319 align=center border=1 HSPACE="3">
<TBODY>
<TR>
<TD vAlign=top width="6%" height=39>&nbsp;&#8470;&nbsp;&nbsp;</TD>
<TD vAlign=top width="34%" height=39>
<P align=left>�������</P></TD>
<TD vAlign=top width="26%" height=39>
<P align=left>���������� �������� (���. ��.)</P></TD>
<TD vAlign=top width="34%" height=39>
<P align=left>����� ������ �� ������������� ������<BR>(���. ���.)</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=25>
<P align=center>1</P></TD>
<TD vAlign=top width="34%" height=25>
<P align=left>�-� ���� ��������� ��</P></TD>
<TD vAlign=top width="26%" height=25>
<P align=center>817,3</P></TD>
<TD vAlign=top width="34%" height=25>
<P align=center>21 090,3</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=13>
<P align=center>2</P></TD>
<TD vAlign=top width="34%" height=13>
<P align=left>���</P></TD>
<TD vAlign=top width="26%" height=13>
<P align=center>1028,1</P></TD>
<TD vAlign=top width="34%" height=13>
<P align=center>20 239,7</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=12>
<P align=center>3</P></TD>
<TD vAlign=top width="34%" height=12>
<P align=left>���������� ����</P></TD>
<TD vAlign=top width="26%" height=12>
<P align=center>473,8</P></TD>
<TD vAlign=top width="34%" height=12>
<P align=center>7 738,1</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=13>
<P align=center>4</P></TD>
<TD vAlign=top width="34%" height=13>
<P align=left>���� �����-���������</P></TD>
<TD vAlign=top width="26%" height=13>
<P align=center>339,0</P></TD>
<TD vAlign=top width="34%" height=13>
<P align=center>4 564,5</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=13>
<P align=center>5</P></TD>
<TD vAlign=top width="34%" height=13>
<P align=left>���-���� ���</P></TD>
<TD vAlign=top width="26%" height=13>
<P align=center>122,7</P></TD>
<TD vAlign=top width="34%" height=13>
<P align=center>1758,4</P></TD></TR></TBODY></TABLE></P>
<P align=justify>�� ��������� �� 01.01.2006 99&nbsp;��������� ����������� (������� ��) ������������ ��������� ��������� ����. ���������� ������-���������� ������, ��� ������, �������������� �������, ��������� ��������� ��� ����������� ���������� ����� ��������� ����, ��� �������, �������� ��������� ������ � �������� ���������� �������.</P>
<P align=justify>� IV �������� 2005 ���� �� ����� �������������� ���������� ����� ����������� ��������� � ���������� ������� ������, ����� ����� ������, ����������� ������� &#8211; �����������, ��������� 70,0 ����. ���.</P>
<P align=justify>����� �������, ���������� ������������� ���������� ������� (���������) � �����-����������, ����� �� ���������� (���. 8). ���������� ����� ��������: ������-�������� ���� ��������� ������ (33,7&nbsp;% �� ������ ������ ���������� ����������), ��� (23,5&nbsp;%), ���������� ���� (12,1&nbsp;%), � ���� �����-��������� (5,5&nbsp;%).</P>
<P align=justify>����������� ����� ��������, ����������� �� ������, ������������� ������� �����-����������, � ���������, ������������ �������������� �����������, ����� ������� �����, ��� ����� &#8220;����������&#8221; �� ��������� � �������, ����������� �� ����� ������ � �������-��������� �����, ������, ��� �����, ��������� � ��������� ������ ��������. ������������� &#8220;��������&#8221; � ������ �������� ����� �� ��������, ����������� � �������������� ��������� ����, ���������� ������� 1-2 ����. ���. (���. 9)</P>
<P align=center><FONT style="BACKGROUND-COLOR: #d5ffbb"></FONT></P>
<P align=center><FONT style="BACKGROUND-COLOR: #d5ffbb"></FONT></P><B>
<P align=center>������������� ������� ����������� IV ��. 2004 �. � IV ��. 2005 �.</P></B>
<TABLE borderColor=#ffffff cellSpacing=3 width=624 align=center border=1 HSPACE="12">
<TBODY>
<TR>
<TD vAlign=center width="58%" bgColor=#ffffff height=21><B>
<P align=justify>����������</B></P></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=21><B>
<P align=center>IV ������� 2004 ����</B></P></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=21><B>
<P align=center>IV ������� 2005 ����</B></P></TD>
<TD vAlign=center width="11%" bgColor=#ffffff height=21><B>
<P align=center>���� � %</B></P></TD></TR>
<TR>
<TD vAlign=bottom width="58%" bgColor=#ffffff height=21>
<P align=justify>���������� ������������� ���� (��.)</P></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=21>
<P align=right>2 566 328</P></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=21>
<P align=right>3 201 687</P></TD>
<TD vAlign=center width="11%" bgColor=#ffffff height=21><B>
<P align=right>124,7</B></P></TD></TR>
<TR>
<TD vAlign=bottom width="58%" bgColor=#ffffff height=21>
<P align=justify>������������� ������� ������� (���. ���.)</P></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=21>
<P align=right>328</P></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=21>
<P align=right>1214</P></TD>
<TD vAlign=center width="11%" bgColor=#ffffff height=21><B>
<P align=right>370,1</B></P></TD></TR>
<TR>
<TD vAlign=bottom width="58%" bgColor=#ffffff height=21>
<P align=justify>����� ������ (���. ���.)</P></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=21>
<P align=right></P></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=21>
<P align=right></P></TD>
<TD vAlign=center width="11%" bgColor=#ffffff height=21>
<P align=right></P></TD></TR>
<TR>
<TD vAlign=bottom width="58%" bgColor=#ffffff height=21>
<P align=justify>
<LI>�� ������ �������� � ������</LI></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=21>
<P align=right>61 915</P></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=21>
<P align=right>61 349</P></TD>
<TD vAlign=center width="11%" bgColor=#ffffff height=21><B>
<P align=right>99,1</B></P></TD></TR>
<TR>
<TD vAlign=bottom width="58%" bgColor=#ffffff height=21>
<LI>�� ������ �������� � ����������� ������</LI></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=21>
<P align=right>1 391</P></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=21>
<P align=right>1 601</P></TD>
<TD vAlign=center width="11%" bgColor=#ffffff height=21><B>
<P align=right>115,1</B></P></TD></TR>
<TR>
<TD vAlign=bottom width="58%" bgColor=#ffffff height=21>
<LI>�� ������ ������� � �����</LI></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=21>
<P align=right>3 513</P></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=21>
<P align=right>5 187</P></TD>
<TD vAlign=center width="11%" bgColor=#ffffff height=21><B>
<P align=right>147,6</B></P></TD></TR>
<TR>
<TD vAlign=bottom width="58%" bgColor=#ffffff height=21>
<LI>�� ���������� ��������</LI></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=21>
<P align=right>1 168</P></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=21>
<P align=right>3 256</P></TD>
<TD vAlign=center width="11%" bgColor=#ffffff height=21><B>
<P align=right>278,7</B></P></TD></TR>
<TR>
<TD vAlign=bottom width="58%" bgColor=#ffffff height=21>
<P align=justify>���������� �������-��������� ����� (��.)</P></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=21>
<P align=right>7 521</P></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=21>
<P align=right>8 532</P></TD>
<TD vAlign=center width="11%" bgColor=#ffffff height=21><B>
<P align=right>113,4</B></P></TD></TR>
<TR>
<TD vAlign=bottom width="58%" bgColor=#ffffff height=29>
<P align=justify>���������� ���������� (��.)</P></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=29>
<P align=right>1 541</P></TD>
<TD vAlign=center width="15%" bgColor=#ffffff height=29>
<P align=right>2 107</P></TD>
<TD vAlign=center width="11%" bgColor=#ffffff height=29><B>
<P align=right>136,7</B></P></TD></TR></TBODY></TABLE><I>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<P align=right>���������� ������������<BR>����������� ��������� ������ � ��������<BR>�������� ���������� ����� ������ �� �����-����������</P></I></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR>


</body>
</html>

