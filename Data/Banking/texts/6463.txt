<!--
ID:36589532
TITLE:����������� ���������� ��������� �����������. ���������
SOURCE_ID:3144
SOURCE_NAME:������ � ������
AUTHOR:
NUMBER:7
ISSUE_DATE:2006-07-31
RECORD_DATE:2006-08-21


RBR_ID:4932
RBR_NAME:������������� ���� � ������
RBR_ID:4933
RBR_NAME:������ ����������� ��������� ������
-->
<html>
<head>
<title>
36589532-����������� ���������� ��������� �����������. ���������
</title>
</head>
<body >


<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<P align=right>���������� ����������</P></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR>
<CENTER>
<TABLE cellSpacing=1 cellPadding=7 width=583 border=1>
<TBODY>
<TR>
<TD vAlign=center width="23%" height=37 rowSpan=2>
<P align=center>��� ����������</P>
<P align=center>�� �����</P></TD>
<TD vAlign=center width="77%" colSpan=4 height=37>
<P align=center>��� ��������� ����������� (�������)</P></TD></TR>
<TR>
<TD vAlign=center width="14%">
<P align=center>�� ����</P></TD>
<TD vAlign=center width="26%">
<P align=center>��������</P>
<P align=center>���������������</P>
<P align=center>���������������</P>
<P align=center>�����</P></TD>
<TD vAlign=center width="21%">
<P align=center>��������������� �����</P>
<P align=center>(/����������</P>
<P align=center>�����)</P></TD>
<TD vAlign=center width="16%">
<P align=center>���</P></TD></TR>
<TR>
<TD vAlign=center width="23%">
<P align=center>45</P></TD>
<TD vAlign=center width="14%">
<P align=center>18427242</P></TD>
<TD vAlign=top width="26%">
<P align=center>1027739369041</P></TD>
<TD vAlign=top width="21%">
<P align=center>3328</P></TD>
<TD vAlign=top width="16%">
<P align=center>044525101</P></TD></TR></TBODY></TABLE></CENTER><B>
<P align=center>������������� ������<BR>(����������� �����)<BR>�� 1 ���� 2006 ����</P></B>
<P align=center>��������� ����������� <B><U>�������� � ������������ ���������������� &#8220;����� ����&#8221; </U><U>(��� &#8220;����� ����&#8221;)<BR></B></U>(��������� (������ �����������) � ����������� ������������)</P>
<P>�������� ����� <U>129090, ������, ��. �������, ��� 4</P></U>
<P align=right>��� ����� 0409806<BR>�����������/�������</P>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<P align=right>(���. ���.)</P></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR>
<TABLE cellSpacing=1 cellPadding=7 width=652 align=center border=1>
<TBODY>
<TR>
<TD vAlign=center width="6%">
<P align=center>&#8470; �/�</P></TD>
<TD vAlign=center width="68%" colSpan=2>
<P align=center>������������ ������</P></TD>
<TD vAlign=center width="13%">
<P align=center>������</P>
<P align=center>�� �������� ����</P></TD>
<TD vAlign=center width="13%">
<P align=center>������ �� �����������-����</P>
<P align=center>�������� ���� �������� ����</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>1</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P align=center>2</P></TD>
<TD vAlign=top width="13%">
<P align=center>3</P></TD>
<TD vAlign=top width="13%">
<P align=center>4</P></TD></TR>
<TR>
<TD vAlign=center width="6%"><B>
<P align=center>I</B></P></TD>
<TD vAlign=top width="68%" colSpan=2><B>
<P align=center>������</B></P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>1</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>�������� ��������</P></TD>
<TD vAlign=top width="13%">
<P align=right>39 527</P></TD>
<TD vAlign=top width="13%">
<P align=right>62 009</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>2</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>�������� ��������� ����������� � ����������� ����� ���������� ���������</P></TD>
<TD vAlign=top width="13%">
<P align=right>3 747 582</P></TD>
<TD vAlign=top width="13%">
<P align=right>635 928</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>2.1</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������������ �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>590 840</P></TD>
<TD vAlign=top width="13%">
<P align=right>420 477</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>3</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>�������� � ��������� ������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>1&nbsp;068 807</P></TD>
<TD vAlign=top width="13%">
<P align=right>127 456</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>4</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������ �������� � �������� ������ ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>5&nbsp;864 629</P></TD>
<TD vAlign=top width="13%">
<P align=right>7&nbsp;420 921</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>5</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������ ������� �������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>23&nbsp;066 407</P></TD>
<TD vAlign=top width="13%">
<P align=right>17&nbsp;001 891</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>6</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������ �������� � �������������� ������ ������, ������������ �� ���������</P></TD>
<TD vAlign=top width="13%">
<P align=right>6 531</P></TD>
<TD vAlign=top width="13%">
<P align=right>37 598</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>7</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������ �������� � ������ ������, ��������� � ������� ��� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>174 609</P></TD>
<TD vAlign=top width="13%">
<P align=right>600 000</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>8</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>�������� ��������, �������������� ������ � ������������ ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>176 465</P></TD>
<TD vAlign=top width="13%">
<P align=right>180 340</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>9</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>���������� �� ��������� ���������</P></TD>
<TD vAlign=top width="13%">
<P align=right>30 086</P></TD>
<TD vAlign=top width="13%">
<P align=right>19 789</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>10</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������ ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>543 286</P></TD>
<TD vAlign=top width="13%">
<P align=right>265 913</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>11</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>����� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>34&nbsp;717 929</P></TD>
<TD vAlign=top width="13%">
<P align=right>26&nbsp;351 845</P></TD></TR>
<TR>
<TD vAlign=center width="6%"><B>
<P align=center>II</B></P></TD>
<TD vAlign=top width="68%" colSpan=2><B>
<P align=center>�������</B></P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>12</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������� ������������ ����� ���������� ���������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>13</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>�������� ��������� �����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>8&nbsp;320 394</P></TD>
<TD vAlign=top width="13%">
<P align=right>9&nbsp;983 641</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>14</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>�������� �������� (����������� �����������)</P></TD>
<TD vAlign=top width="13%">
<P align=right>21 966 377</P></TD>
<TD vAlign=top width="13%">
<P align=right>12&nbsp;908 237</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>14.1</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������ ���������� ���</P></TD>
<TD vAlign=top width="13%">
<P align=right>1&nbsp;507 001</P></TD>
<TD vAlign=top width="13%">
<P align=right>885 708</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>15</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>���������� �������� �������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>6 707</P></TD>
<TD vAlign=top width="13%">
<P align=right>38 641</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>16</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������������� �� ������ ���������</P></TD>
<TD vAlign=top width="13%">
<P align=right>3 535</P></TD>
<TD vAlign=top width="13%">
<P align=right>4 154</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>17</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������ �������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>41 979</P></TD>
<TD vAlign=top width="13%">
<P align=right>10 120</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>18</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������� �� ��������� ������ �� �������� �������������� ���������� ���������, ������ ��������� ������� � �� ��������� � ����������� �������� ���</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>45 062</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>19</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>����� ������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>30&nbsp;384 054</P></TD>
<TD vAlign=top width="13%">
<P align=right>22&nbsp;944 793</P></TD></TR>
<TR>
<TD vAlign=center width="6%"><B>
<P align=center>III</B></P></TD>
<TD vAlign=top width="68%" colSpan=2><B>
<P align=center>��������� ����������� �������</B></P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>20</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>�������� ���������� (����������)</P></TD>
<TD vAlign=top width="13%">
<P align=right>1&nbsp;237 450</P></TD>
<TD vAlign=top width="13%">
<P align=right>1 237 450</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>20.1</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������������������ ������������ ����� � ����</P></TD>
<TD vAlign=top width="13%">
<P align=right>1&nbsp;237 450</P></TD>
<TD vAlign=top width="13%">
<P align=right>1 237 450</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>20.2</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������������������ ����������������� �����</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>20.3</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>�������������������� �������� ������� ������������� ��������� �����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>21</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>����������� �����, ����������� � ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%" colSpan=2>
<P align=center>1</P></TD>
<TD vAlign=top width="68%">
<P align=center>2</P></TD>
<TD vAlign=top width="13%">
<P align=center>3</P></TD>
<TD vAlign=top width="13%">
<P align=center>4</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>22</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>����������� �����</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>23</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>���������� �������� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>24</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������� ������� �������� � ����������� �������, �������� �� ����������� �������� (�������)</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>11 323</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>18 213</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>25</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>����� � ���������������� ������� ������� ��� � ������������ ��������� ����������� (������������ ������ ������� ���)</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>2&nbsp;474 432</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>1&nbsp;957 526</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>26</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������� (������) �� �������� ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>633 316</P></TD>
<TD vAlign=top width="13%">
<P align=right>230 289</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>27</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>����� ���������� ����������� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>4 333 875</P></TD>
<TD vAlign=top width="13%">
<P align=right>3&nbsp;407 052</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>28</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>����� ��������</P></TD>
<TD vAlign=top width="13%">
<P align=right>34&nbsp;717 929</P></TD>
<TD vAlign=top width="13%">
<P align=right>26&nbsp;351 845</P></TD></TR>
<TR>
<TD vAlign=center width="6%"><B>
<P align=center>IV</B></P></TD>
<TD vAlign=top width="68%" colSpan=2><B>
<P align=center>������������� �������������</B></P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>29</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>����������� ������������� ��������� �����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>55 741&nbsp;749</P></TD>
<TD vAlign=top width="13%">
<P align=right>47&nbsp;996 066</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>30</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>��������, �������� ��������� ������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>4&nbsp;282&nbsp;894</P></TD>
<TD vAlign=top width="13%">
<P align=center>1&nbsp;923 492</P></TD></TR>
<TR>
<TD vAlign=center width="6%"><B>
<P align=center>V</B></P></TD>
<TD vAlign=top width="68%" colSpan=2><B>
<P align=center>����� �������������� ����������</B></P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=center width="6%">&nbsp;</TD>
<TD vAlign=top width="68%" colSpan=2><B>
<P>�������� �����</B></P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>1</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>�����</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>2</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������ ������ � ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>1&nbsp;120 927</P></TD>
<TD vAlign=top width="13%">
<P align=right>903 726</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>3</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>����������� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>4</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������� ���������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>5</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>��������, �������������� �� ������ ����</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>6</P></TD>
<TD vAlign=top width="68%" colSp
an=2>
<P>������� �� �������������� ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>7</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>���������� ����������� ���������� (��������) ����� �� ���������� (��������) �������� ��������������</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>317</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>2 787</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>8</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������� �����</P></TD>
<TD vAlign=top width="13%">
<P align=right>7 227</P></TD>
<TD vAlign=top width="13%">
<P align=right>23 349</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>9</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������� �� �������������� ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>10</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������ �� �������������� ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>15 880</P></TD>
<TD vAlign=top width="13%">
<P align=right>160</P></TD></TR>
<TR>
<TD vAlign=center width="6%">&nbsp;</TD>
<TD vAlign=top width="68%" colSpan=2><B>
<P>��������� �����</B></P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>11</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������� � ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>1&nbsp;050 229</P></TD>
<TD vAlign=top width="13%">
<P align=right>894 267</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>12</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������� �� �������������� ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>2 343</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>13</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>���������� ����������� ���������� (��������) ����� �� ���������� (��������) �������� ��������������</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>14</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������ �� �������������� ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>15</P></TD>
<TD vAlign=top width="68%" colSpan=2>
<P>������� �� �������������� ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>94 122</P></TD>
<TD vAlign=top width="13%">
<P align=right>33 412</P></TD></TR></TBODY></TABLE><B><B>
<P align=center>����� � �������� � �������<BR></B>(����������� �����)<BR>�� I ��������� 2006 ����</P></B>
<P align=center>��������� ����������� <B><U>�������� � ������������ ���������������� &#8220;����� ����&#8221; </U><U>(��� &#8220;����� ����&#8221;)<BR></B></U>(��������� (������ �����������) � ����������� ������������)</P>
<P>�������� ����� <U>129090, ������, ��. �������, ��� 4</P></U>
<P align=right>��� ����� 0409807<BR>�����������</P>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<DIR>
<P align=right>(���. ���.)</P></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR></DIR>
<TABLE cellSpacing=1 cellPadding=4 width=652 align=center border=1>
<TBODY>
<TR>
<TD vAlign=center width="6%">
<P align=center>&#8470; �/�</P></TD>
<TD vAlign=center width="68%">
<P align=center>������������ ������</P></TD>
<TD vAlign=center width="13%">
<P align=center>������ �� �������� ������</P></TD>
<TD vAlign=center width="13%">
<P align=center>������ �� �����������-���� ������ �������� ����</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>1</P></TD>
<TD vAlign=top width="68%">
<P align=center>2</P></TD>
<TD vAlign=top width="13%">
<P align=center>3</P></TD>
<TD vAlign=top width="13%">
<P align=center>4</P></TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="68%"><B>
<P align=center>�������� ���������� � ����������� ������ ��:</B></P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>1</P></TD>
<TD vAlign=top width="68%">
<P>���������� ������� � ��������� ������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>310 007</P></TD>
<TD vAlign=top width="13%">
<P align=right>168 628</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>2</P></TD>
<TD vAlign=top width="68%">
<P>����, ��������������� �������� (����������� ������������)</P></TD>
<TD vAlign=top width="13%">
<P align=right>135 089</P></TD>
<TD vAlign=top width="13%">
<P align=right>50 108</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>3</P></TD>
<TD vAlign=top width="68%">
<P>�������� ����� �� ���������� ������ (�������)</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>4</P></TD>
<TD vAlign=top width="68%">
<P>������ ����� � ������������� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>324 429</P></TD>
<TD vAlign=top width="13%">
<P align=right>261 139</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>5</P></TD>
<TD vAlign=top width="68%">
<P>������ ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>1 773</P></TD>
<TD vAlign=top width="13%">
<P align=right>1 221</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>6</P></TD>
<TD vAlign=top width="68%">
<P>����� ��������� ���������� � ����������� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>771 298</P></TD>
<TD vAlign=top width="13%">
<P align=right>481 096</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>1</P></TD>
<TD vAlign=top width="68%">
<P align=center>2</P></TD>
<TD vAlign=top width="13%">
<P align=center>3</P></TD>
<TD vAlign=top width="13%">
<P align=center>4</P></TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="68%"><B>
<P align=center>�������� ���������� � ����������� ������� ��:</B></P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>7</P></TD>
<TD vAlign=top width="68%">
<P>������������ ��������� ��������� �����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>155&nbsp;942</P></TD>
<TD vAlign=top width="13%">
<P align=right>68 675</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>8</P></TD>
<TD vAlign=top width="68%">
<P>������������ ��������� �������� (����������� �����������)</P></TD>
<TD vAlign=top width="13%">
<P align=right>246&nbsp;515</P></TD>
<TD vAlign=top width="13%">
<P align=right>73 564</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>9</P></TD>
<TD vAlign=top width="68%">
<P>���������� �������� ��������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>10</P></TD>
<TD vAlign=top width="68%">
<P>����� ��������� ���������� � ����������� ��������</P></TD>
<TD vAlign=top width="13%">
<P align=right>402&nbsp;457</P></TD>
<TD vAlign=top width="13%">
<P align=right>142 239</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>11</P></TD>
<TD vAlign=top width="68%">
<P>������ ���������� � ����������� ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>368&nbsp;841</P></TD>
<TD vAlign=top width="13%">
<P align=center>338 857</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>12</P></TD>
<TD vAlign=top width="68%">
<P>������ ������ �� �������� � ������� ��������</P></TD>
<TD vAlign=top width="13%">
<P align=right>-276 330</P></TD>
<TD vAlign=top width="13%">
<P align=right>71 861</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>13</P></TD>
<TD vAlign=top width="68%">
<P>������ ������ �� �������� � ����������� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>133 367</P></TD>
<TD vAlign=top width="13%">
<P align=right>-361 373</P></TD></TR>
<TR>
<TD vAlign=center width="6%">
<P align=center>14</P></TD>
<TD vAlign=top width="68%">
<P>������ ������ �� �������� � ������������ ��������� � ������� ����������� �������������</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>15</P></TD>
<TD vAlign=top width="68%">
<P>������ ������ �� ���������� ����������� ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>573 547</P></TD>
<TD vAlign=top width="13%">
<P align=right>343 412</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>16</P></TD>
<TD vAlign=top width="68%">
<P>������������ ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>423 211</P></TD>
<TD vAlign=top width="13%">
<P align=right>178 913</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>17</P></TD>
<TD vAlign=top width="68%">
<P>������������ �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>6 407</P></TD>
<TD vAlign=top width="13%">
<P align=right>5 152</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>18</P></TD>
<TD vAlign=top width="68%">
<P>������ ������ �� ������� ��������</P></TD>
<TD vAlign=top width="13%">
<P align=right>2 035</P></TD>
<TD vAlign=top width="13%">
<P align=right>904</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>19</P></TD>
<TD vAlign=top width="68%">
<P>������ ������ ������������ ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>121 480</P></TD>
<TD vAlign=top width="13%">
<P align=right>127 255</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>20</P></TD>
<TD vAlign=top width="68%">
<P>���������������-�������������� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>472 054</P></TD>
<TD vAlign=top width="13%">
<P align=right>362 169</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>21</P></TD>
<TD vAlign=top width="68%">
<P>������� �� ��������� ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>-42 003</P></TD>
<TD vAlign=top width="13%">
<P align=right>-5 566</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>22</P></TD>
<TD vAlign=top width="68%">
<P>������� �� ���������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>825 687</P></TD>
<TD vAlign=top width="13%">
<P align=right>326 942</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>23</P></TD>
<TD vAlign=top width="68%">
<P>����������� ������ (������� ����� �� �������)</P></TD>
<TD vAlign=top width="13%">
<P align=right>192 371</P></TD>
<TD vAlign=top width="13%">
<P align=right>96 653</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>24</P></TD>
<TD vAlign=top width="68%">
<P>������� (������) �� �������� ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>633 316</P></TD>
<TD vAlign=top width="13%">
<P align=right>230 289</P></TD></TR></TBODY></TABLE><B>
<P align=center>����� �� ������ ������������� ��������,<BR>�������� �������� �� �������� ������������ ���� � ���� �������<BR>�� 1 ���� 2006 ����</P></B>
<P>��������� ����������� <B><U>�������� � ������������ ���������������� &#8220;����� ����&#8221;,&nbsp; </U><U>(��� &#8220;����� ����&#8221;)<BR></B></U>(��������� (������ �����������) � ����������� ������������)</P>
<P>�������� ����� <U>129090, ������, ��. �������, ��� 4</P></U>
<P align=right>��� ����� 0409808<BR>�����������/�������</P>
<TABLE cellSpacing=1 cellPadding=4 width=652 align=center border=1>
<TBODY>
<TR>
<TD vAlign=center width="7%">
<P align=center>&#8470;</P>
<P align=center>�/�</P></TD>
<TD vAlign=center width="67%">
<P align=center>������������ ����������</P></TD>
<TD vAlign=center width="13%">
<P align=center>������ �� �������� ����</P></TD>
<TD vAlign=center width="13%">
<P align=center>������ �� �����������-���� �������� ���� �������� ����</P></TD></TR>
<TR>
<TD vAlign=center width="7%">
<P align=center>1</P></TD>
<TD vAlign=top width="67%">
<P align=center>2</P></TD>
<TD vAlign=top width="13%">
<P align=center>3</P></TD>
<TD vAlign=top width="13%">
<P align=center>4</P></TD></TR>
<TR>
<TD vAlign=center width="7%">
<P align=center>1</P></TD>
<TD vAlign=top width="67%">
<P>����������� �������� (�������), ���. ���.</P></TD>
<TD vAlign=top width="13%">
<P align=right>4&nbsp;333&nbsp;849,0</P></TD>
<TD vAlign=top width="13%">
<P align=right>3 407 025,0</P></TD></TR>
<TR>
<TD vAlign=center width="7%">
<P align=center>2</P></TD>
<TD vAlign=top width="67%">
<P>����������� �������� ������������� ����������� ������� (��������), �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>20,3</P></TD>
<TD vAlign=top width="13%">
<P align=right>20,2</P></TD></TR>
<TR>
<TD vAlign=center width="7%">
<P align=center>3</P></TD>
<TD vAlign=top width="67%">
<P>����������� �������� ������������� ����������� ������� (��������), �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>10,0</P></TD>
<TD vAlign=top width="13%">
<P align=right>10,0</P></TD></TR>
<TR>
<TD vAlign=center width="7%">
<P align=center>4</P></TD>
<TD vAlign=top width="67%">
<P>��������� ������ �� ��������� ������ �� ������, ������� � ������������ � ��� �������������, ���. ���.</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>0,0</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>376,0</P></TD></TR>
<TR>
<TD vAlign=center width="7%">
<P align=center>5</P></TD>
<TD vAlign=top width="67%">
<P>���������� �������������� ������ �� ��������� ������ �� ������, ������� � ������������ � ��� �������������, ���. ���.</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>0,0</P></TD>
<TD vAlign=top width="13%">
<P align=right></P>
<P align=right>376,0</P></TD></TR>
<TR>
<TD vAlign=center width="7%">
<P align=center>6</P></TD>
<TD vAlign=top width="67%">
<P>��������� ������ �� ��������� ������, ���. ���.</P></TD>
<TD vAlign=top width="13%">
<P align=right>46&nbsp;071,0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0,0</P></TD></TR>
<TR>
<TD vAlign=center width="7%">
<P align=center>7</P></TD>
<TD vAlign=top width="67%">
<P>���������� �������������� ������ �� ��������� ������, ���. ���.</P></TD>
<TD vAlign=top width="13%">
<P align=right>46&nbsp;071,0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0,0</P></TD></TR></TBODY></TABLE>
<P>������������ ���������&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;�. ��������&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; _______________<BR>(��� &#8220;����� ����&#8221;)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (�.�.�.)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (�������)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&n
bsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; �����</P>
<P align=right>������</P>
<P>������� ���������&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; �. �. ���������&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; _______________<BR>(��� &#8220;����� ����&#8221;)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (�.�.�.)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (�������)</P>


</body>
</html>

