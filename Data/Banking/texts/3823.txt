<!--
ID:37258508
TITLE:��������� ��������� �������������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:3
ISSUE_DATE:2007-02-15
RECORD_DATE:2007-02-08


-->
<html>
<head>
<title>
37258508-��������� ��������� �������������
</title>
</head>
<body >


<P align=justify>������������ ��������� ������������� �� ���������� ������������� ������� � ���, �������� �������� �� ���������� ���������� ��������, ���������� ���������� � ����������� ������������ ����� ���������� � ��������� ��������� ������������:</P>
<TABLE cellSpacing=2 cellPadding=7 width=100% align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="7%">
<P align=center>&#8470; �/�</P></TD>
<TD vAlign=top width="29%">
<P align=center>������������ ��������� �����������</P></TD>
<TD vAlign=top width="18%">
<P align=center>��������-������� �����</P></TD>
<TD vAlign=top width="16%">
<P align=center>���� ������ ��������</P></TD>
<TD vAlign=top width="31%">
<P>�.�.�. ������������ ��������� �������������, �������� �����, �������</P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>1</P></TD>
<TD vAlign=top width="29%">
<P>�������������</P></TD>
<TD vAlign=top width="18%">
<P align=center>3191</P></TD>
<TD vAlign=top width="16%">
<P align=center>20.12.2006</P></TD>
<TD vAlign=top width="31%">
<P>115093, ������, ��. �����, 18, ���. 1</P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>2</P></TD>
<TD vAlign=top width="29%">
<P>������</P></TD>
<TD vAlign=top width="18%">
<P align=center>3086</P></TD>
<TD vAlign=top width="16%">
<P align=center>20.12.2006</P></TD>
<TD vAlign=top width="31%">
<P>129085, ������, �������� �-�, 21, ���. 2</P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>3</P></TD>
<TD vAlign=top width="29%">
<P>��������</P></TD>
<TD vAlign=top width="18%">
<P align=center>235</P></TD>
<TD vAlign=top width="16%">
<P align=center>04.12.2006</P></TD>
<TD vAlign=top width="31%">
<P>101000, ������, ����������� ���., 10, ���. 4</P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>4</P></TD>
<TD vAlign=top width="29%">
<P>��-����� (���)</P></TD>
<TD vAlign=top width="18%">
<P align=center>3462-�</P></TD>
<TD vAlign=top width="16%">
<P align=center>13.12.2006</P></TD>
<TD vAlign=top width="31%">
<P>113114, ������, ��. ������������, 1/2, ���. 1</P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>5</P></TD>
<TD vAlign=top width="29%">
<P>�����-����</P></TD>
<TD vAlign=top width="18%">
<P align=center>2359</P></TD>
<TD vAlign=top width="16%">
<P align=center>20.12.2006</P></TD>
<TD vAlign=top width="31%">
<P>121151, ������, ���. ������ ��������, 23�, ���� 19</P></TD></TR></TBODY></TABLE><I>
<P align=right>"������� ����� ������", 2007, &#8470; 2, 4</P></I>


</body>
</html>

