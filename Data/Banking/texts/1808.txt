<!--
ID:36849000
TITLE:������ � ���������
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:������� ���������� ������������ �������� �������� &#171;������������ �����&#187;

NUMBER:5
ISSUE_DATE:2006-05-31
RECORD_DATE:2006-10-25


-->
<html>
<head>
<title>
36849000-������ � ���������
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR>
<TD><STRONG>����������� ������� ��� ������ �������� ������</STRONG></TD></TR>
<TR class=bgwhite>
<TD></TD></TR>
<TR class=bodycolor>
<TD>
<P class=Main align=justify><I>����������� ����� ��� ����� ����� ������������ ������ ������-��������. ������ ������ � ���������� ����������� ����������� ����������� �������� ��� ������������� � ��������������� �� ������������� �����. ��� ��������� � �������� �����, � ��������� ���������, ���������������� ��� ������ �������� ��������&#8230; </I></P></TD></TR>
<TR>
<TD></TD></TR>
<TR class=bgwhite>
<TD>
<P class=Main align=justify>� ��������� ����� �� ����� ��������� ��������� ���������� ������������� ��������������� (����). �� ��������� �������, ��� ���������� �� 90% ���������. �� ���� &#8212; ��� �� ������ ����� � �������� �����, �� � ����� ���������� ������ ��������������� ����������� ��������. 
<P class=Main align=justify>�������, ������������� ������������ ������������� ����� � ������ ����� �������� �� ������ ������� ������������������ �����, �� � �������� � ������ ��������� ����������, ������� ������ ���������� � ������������� ����� ��������. 
<P class=Main align=justify>������������� ������ ������������ � ������������� �������. � ��������� ���� ����������� �������� �������� ���������� �� &#171;������&#187; ����������� � ������� ��������������� �������, ��������� ���������� ������ �� ���� ����� ������������� �����. ������������� ������� �������� ������ ������ ������� ��������� ��� ����������� ������� ������� �� ��������������, ��� � ��������� ���� ��������������� �������. ���������� ��� ��������� ����������� ������������ � �������� ������. 
<P class=Subheader align=justify>������������ ������� &#8212; ������ �� ������� ����� 
<P class=Main align=justify>����������� ��� ������������ ������� ��� ������ ������� ������� ����� � ���� ������� �������� ������ ����������� �����. ��� �������, ��� &#8212; ������ ��� �������� ��������, ���������� �� ��������� POP3, � ����������� ������, �������������� � �������� ���������� � ������������ �������� ������� &#8212; MS Outlook � (������� ����) Lotus Notes. �������� ��, �������� �� ���� ��������. 
<P class=Main align=justify>�� ���������������� �� ����� �������. ����������� ����, ������������� ������������, �������� � ����������� &#8212; �� ��� ����� �������� � ������������������� ������ �� ������� �������. ��� ����, �� ����������� �������, �� 30% �������������, ������� ���� �������������� ������ � ��������� � ���� ������������ �����, �� ����� �������� ���. ��� ��� ����� &#171;������&#187; ������ � ����������� �������������� ����������������� ������� ����� �������� � ����������� �������. �������, ���� ���� ������ �������� � ������� ������, ��� ������������ ������� ������ ��������� �������� ���� � �� ����������� ������������� ������, ���� ����� �������� �� �������. 
<P class=Subheader align=justify>����������� ������� &#8212; ������ ������� ��� ����� 
<P class=Main align=justify>��� �������� ��-���������� ����������� ������� ���������� �������������� ��� ������ �� ���������� �������� ������, ��� � ��������� ������������� ��������� ������������. ������� ������ �������� &#8212; �������� ������������ &#171;����� �����&#187; &#8212; �������� ������ ��� ����. ����������������� ������ ������� ������� ����� ��������������. ��� ����� ����������� �� ��������� � ���������������� �������� �����. � ���������� ����������� ��������� ����������� ��������� ������������� ��������� � ����. �, �������, ��������� �� �������� ������ ��� ���� ������������ ������� �������������� ���������, �������� � ���������� �����������, ��������� �������� ������ ������������� �������� ������������� ����������� �����. 
<P class=Subheader align=justify>��������� ������� ������ 
<P class=Main align=justify>�������� ����. ����������� � ������� ���������� ������ ������������� ����� �� ������������ ���� ������ �������� ���������� �����������: �������� �� ������ ����� �/��� ��������� �������, ������������ � ������ �������� ����� ������ � ������ ��������� �������, ������������ ������������� ��������������� (����), � ����� ����� ������� ���������� ���������� � ���������� ������������� ����� ������. ���������� �������� ���� ������������� (�������������� � ���������), ����� �������� ������������� �������� ������. ����� ������������, ���� � ������� ������ ���������� � ���������������� �������� ��� �����������, �� ������ ������������ �� ��������� ���������� ���������� ������������ � �������� ��������� &#8212; �������� � ������������ ����������� �������������� ������������ ����.</P></TD></TR></TBODY></TABLE>


</body>
</html>

