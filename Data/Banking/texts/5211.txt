<!--
ID:36292593
TITLE:�������� ���������� ������� �� ��������
SOURCE_ID:3126
SOURCE_NAME:������� ����������� ����
AUTHOR:������� ��������, ����-��������� &#147;��������� ������ ������&#148;
NUMBER:3
ISSUE_DATE:2006-03-31
RECORD_DATE:2006-06-02


-->
<html>
<head>
<title>
36292593-�������� ���������� ������� �� ��������
</title>
</head>
<body >


<P align=justify>&#8212; ��� ���� ����� � ����� �����������, ������, ������ � ����������������� ������������ ������ �������� ������ ��� �� ������� 1998 ����. ���� ������ ���� ���������� � ���� ����� ���������� ��������������� �����������, �� ������, ������, ������� ������ ��������� � ���� ������� �����, ��������� ����� � 5 ���. ���� � ������� ��� ���������� ��������������. � ��������, ���������� ������ �������, �������, ��� ����������� (������� 600 ���������� ������) ��� ���������� ��� �������� ���������. ���������� ��������� ����������� � ���������� ������ ������� � ���������� ������� �� ��������� 3 ���������. ����� �������, ���� �� ����� �������� � ���������� ���������� ������������ �������� ���� ��������. ����, ������������ �������� ���������� ������� �� ��������.</P>
<P align=justify>�������� �������� �������� ������������ ��������� ���������� � ������������ ������� �������� � 5 ���. ���� ���� ��� ����� ����������� ������, ����� ��� ��� ������������ ��������� ����������� ������ ���������� ��������������� ��� �������, ���� �� ����� ������� ������� ������� ��������. ����� &#8220;��������� ��������&#8221; ���� � �������� ��������� ��� ������ ������, �� ������ ��������������, ��� �������� ����������� ��� ������ ������ ��������� ������. ���� � ���, ��� ���� �������� �������� ���������� ��� ������, ���� ����� �������� � ��������� �����, �������� ���� ������ ������� ���� 2004 ����. � ������ �������, � ������ ������, ��� �������, ����������� ������ �������������� �������, � ������, � ������ ������� � ��������� ������ �������� ������� � ����� ��������� ������ �������, � ����� ��������� ����� ��������. ��� ���� ������� ����� ������ ������ ����� �������� �������� ������ �������� ��������� ������ ������������ ���� ����� ��� ������������ �������, ������ ������� ����� ���������� �� ������ �� ������� �� ���������� �����������������, �� � ��-�� ������������� ������������� ������������ ����� ��� �������� ������ ��������. ��������������, ��� � ����� ����� ������ �������, ����������� � �� �����������, ����������� ������� �� ������������ � ����� ������� � ������������� ��������� �����������. ����� �������, �������� ���������� � ������������ ������� �������� ������ �������� �������� �������������� ����������� ��������� ��� ����� ������, � ������, ��� ����� �������� &#8212; ��� ����� �����������, ���������, ��������������� � ���������������. ���������� ��������, ��� � ����� ����� ���������� ��������� ��������� ���������� ���������, ������������ ��� ������������ ������� ������. ������� ����� � �� ��������� ��������������� ����������� ����� ������.</P>


</body>
</html>

