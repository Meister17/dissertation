<!--
ID:36772338
TITLE:������� ���� � ������� �������� (����� &#150; �������, 17&#150;24 ������� 2006 �.)
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:18
ISSUE_DATE:2006-09-30
RECORD_DATE:2006-10-06


-->
<html>
<head>
<title>
36772338-������� ���� � ������� �������� (����� &#150; �������, 17&#150;24 ������� 2006 �.)
</title>
</head>
<body >


<B>
<P align=justify>���������� ���������� ������, ������������ ����������������� ������������ ������ � ������ "������� �������&#8211;������" </B>��������� � <B>������������ ���������� ��������� </B>� <B>����������� ��������� ������ </B>���������� ����� ������������ ������� ������� � ������� ����� � ������� ���������<B> </B>�� �����:</P><B><FONT face=Wingdings size=2>
<P align=justify>l</FONT><FONT size=2> </FONT>"����������� ���������� ��������������� ������: ���� �������� � ����� �� �������������� � �������� ������ �����. ��������, ������������ ����������� ��������� �����. ��������� ������������ ����� � ��������. �������� ������ ������ �������������".</P><FONT face=Wingdings size=2>
<P align=justify>l</FONT><FONT size=2> </B></FONT>"<B>����������� �������� �� ����������, ���������� � ������������� ���������� ���. ��������� ������ � V.I.P. ���������",</P></B>
<P align=justify>������� ��������� � <B>����� (����� &#8211; �������)</B> <B>17&#8211;24 ������� 2006 �.</P></B>
<P align=justify>���������� �������� ����� ������������� ����������� �������� ������� ��������� �����, ���������� ������ � "���������" ���������� �������������� ���� � � ���������������� ���������� ������� � ���������� ������ (�� ������� ����� � ��������), ����������� � ���������-���������� ��������������� �� ������� ������.</P><B>
<P align=justify>���������� � ����������� ��������� ������ �������� ����������� �� ������� �� ������������� ���������, � ����� ���������� ����������� ������ ����� ���. ��� ����������� ��� ����� ��������� �����������.</P>
<P align=justify>����� ������ �� 1 ����� �� ������ ������.</P>
<P align=justify>�� �������� ������� ���������� � ����������:</P>
<P align=justify>������� ��������� ����������</P>
<P align=justify>���������� ���.: (495) 290-46-91, e-mail: kovshova_ev@arb.ru</P>
<P align=justify>�������� ������� ������������</P>
<P align=justify>���������� ���.: (495) 505-13-80, 8-909-150-82-44</P>
<P align=justify>(495) 765-36-15,</P>
<P align=justify>����: 158-96-83, e-mail: oprb.slv@mail.ru</P></B>


</body>
</html>

