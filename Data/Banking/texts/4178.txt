<!--
ID:38409740
TITLE:��������� � ���������� ��������� �����������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:21
ISSUE_DATE:2007-11-15
RECORD_DATE:2007-11-14


-->
<html>
<head>
<title>
38409740-��������� � ���������� ��������� �����������
</title>
</head>
<body >


<P align=justify>���������� ����������� (�����������) ������������ ������, �������� �������� �� ���������� ���������� ��������, ����������, ��� � ������������ � ��������� ������������ ����� �������� ���������� ��� ��������� ��������������� (���������) ����������� ����� � ��������� ���������������� (����������) �������� ���������� ��������� ��������� �����������:</P>
<P align=justify></P>
<TABLE cellSpacing=2 cellPadding=7 width=602 border=1>
<TBODY>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>&#8470;</P>
<P align=center>�/�</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>������������</P>
<P align=center>���������</P>
<P align=center>�����������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>��������-������� �����</FONT></P></TD>
<TD vAlign=top width="16%"><FONT size=3>
<P align=center>����</P>
<P align=center>������<BR>��������</FONT></P></TD>
<TD vAlign=top width="39%"><FONT size=3>
<P align=center>�����, �������, �.�.�. ����������� ������������ (�����������).</P>
<P align=center>���� ������������ ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>������������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>943</FONT></P></TD>
<TD vAlign=top width="16%"><FONT size=3>
<P>25.07.2007</FONT></P></TD>
<TD vAlign=top width="39%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>601650, ������������ ���., �����������, ��. �����������, 14;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>60 ����</FONT></P></TD></TR></TBODY></TABLE><I>
<P align=right>�����������</P></I>
<TABLE cellSpacing=2 cellPadding=7 width=602 border=1>
<TBODY>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>&#8470;</P>
<P align=center>�/�</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>������������</P>
<P align=center>���������</P>
<P align=center>�����������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>��������-������� �����</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>����</P>
<P align=center>������<BR>��������</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P align=center>�����, �������, �.�.�. ����������� ������������ (�����������).</P>
<P align=center>���� ������������ ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>������� ���</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>2329</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P>20.11.2000</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>109240, ������, �/� 48;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>����������-��� ����<BR>��������������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>1200</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P>15.08.2007</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>109052, ������, �/� 48;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>60 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>������� ������������ ����</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>2541</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P>15.06.2001</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>109052, ������, �/� 48;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>61 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>5</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>�������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>2906</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P>09.07.2007</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>150030, ���������, ����������� �����, 22;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>60 ����</FONT></P></TD></TR></TBODY></TABLE><I>
<P align=right>"������� ����� ������", 2007, &#8470; 57&#8211;59</P></I>


</body>
</html>

