<!--
ID:36451714
TITLE:�������� ����������� ���� ���������� ���������� ������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:10
ISSUE_DATE:2006-05-31
RECORD_DATE:2006-07-14


-->
<html>
<head>
<title>
36451714-�������� ����������� ���� ���������� ���������� ������
</title>
</head>
<body >


<B>
<P align=center>��������� ���������<BR>�� ���� 2006 �.</P></B>
<TABLE cellSpacing=1 cellPadding=7 width=618 align=center border=1>
<TBODY>
<TR>
<TD vAlign=center width="6%" bgColor=#ffffff height=42><B>
<P align=center>&#8470; �/�</B></P></TD>
<TD vAlign=center width="10%" bgColor=#ffffff height=42><B>
<P align=center>����</B></P></TD>
<TD vAlign=center width="39%" bgColor=#ffffff height=42><B>
<P align=center>���� ��������</B></P></TD>
<TD vAlign=center width="44%" bgColor=#ffffff height=42><B>
<P align=center>��������� ����������</B></P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=84>
<P align=center>1</P></TD>
<TD vAlign=top width="10%" height=84>
<P align=center>5&#8211;6 ����</P></TD>
<TD vAlign=top width="39%" height=84>
<P>�������������� � ���������� ������������ � ������������<BR>�����</P></TD>
<TD vAlign=top width="44%" height=84>
<P>���������� � ����������� ������� ����������� ������� � ������������, ����� �������� �����, � ����� ������������ �������������� ������</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=101>
<P align=center>2</P></TD>
<TD vAlign=top width="10%" height=101>
<P align=center>7&#8211;8 ����</P></TD>
<TD vAlign=top width="39%" height=101>
<P>����������� ������ �� ��������������� ����������� (���������) �������, ���������� ���������� �����, � �������������� ���������� � ��������� ������������</P></TD>
<TD vAlign=top width="44%" height=101>
<P>������������ ������������� ����� ������������ �����, ���������-��������, ����������� ��������</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=84>
<P align=center>3</P></TD>
<TD vAlign=top width="10%" height=84>
<P align=center>7&#8211;8 ����</P></TD>
<TD vAlign=top width="39%" height=84>
<P>�������-�������� �������� � ����� (�������-��������� � ����������� ����������� � ������������ �� ������ ���������� ����-����)</P></TD>
<TD vAlign=top width="44%" height=84>
<P>������������ ��������� ����, ������� ����������, �������� �������, ������������� ������</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=63>
<P align=center>4</P></TD>
<TD vAlign=top width="10%" height=63>
<P align=center>10&#8211;11 ����</P></TD>
<TD vAlign=top width="39%" height=63>
<P>������ ������������� ������������ � ������ ������������������ �����������-���������</P></TD>
<TD vAlign=top width="44%" height=63>
<P>������������ � ����������� ��������� � �������������� ������������� �����</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=42>
<P align=center>5</P></TD>
<TD vAlign=top width="10%" height=42>
<P align=center>10&#8211;11 ����</P></TD>
<TD vAlign=top width="39%" height=42>
<P>��������� &#8211; �������� ���������� � ���������</P></TD>
<TD vAlign=top width="44%" height=42>
<P>����������� ������, ����������� ������ ����������</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=106>
<P align=center>6</P></TD>
<TD vAlign=top width="10%" height=106>
<P align=center>10&#8211;12 ����</P></TD>
<TD vAlign=top width="39%" height=106>
<P>����������� ���������� ���������� ����� �����</P></TD>
<TD vAlign=top width="44%" height=106>
<P>���������� � ���������� ���������� �������������, ������� ����������, ������������� �������� ������������� �������, ���������� �� ����������� �������� � ����������� �����</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=63>
<P align=center>7</P></TD>
<TD vAlign=top width="10%" height=63>
<P align=center>10&#8211;12 ����</P></TD>
<TD vAlign=top width="39%" height=63>
<P>��������������� � ������������� ���� � �����</P></TD>
<TD vAlign=top width="44%" height=63>
<P>������� ����������, ������������ � ���������� ������, ������������ ����������������</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=84>
<P align=center>8</P></TD>
<TD vAlign=top width="10%" height=84>
<P align=center>12&#8211;14 ����</P></TD>
<TD vAlign=top width="39%" height=84>
<P>����������� ��������� ���������� ��������� � �����</P></TD>
<TD vAlign=top width="44%" height=84>
<P>������ � ������� �������������� ��������, ������������ � ����������� ������������� ������ �� ������� ��������� � �����</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=34><B>
<P align=center>&#8470; �/�</B></P></TD>
<TD vAlign=top width="10%" height=34><B>
<P align=center>����</B></P></TD>
<TD vAlign=top width="39%" height=34><B>
<P align=center>���� ��������</B></P></TD>
<TD vAlign=top width="44%" height=34><B>
<P align=center>��������� ����������</B></P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=140>
<P align=center>9</P></TD>
<TD vAlign=top width="10%" height=140>
<P align=center>12&#8211;14 ����</P></TD>
<TD vAlign=top width="39%" height=140>
<P>����������� ������������ ���������� ��� � �����</P></TD>
<TD vAlign=top width="44%" height=140>
<P>������������ � ����������� ���������� �������������, ������� ����������, ���������-������������ �� ������ � ����������� ������ � ������, �� ���������� � ������������������, ������������ �������������� ������ � ���������, �������������</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=63>
<P align=center>10</P></TD>
<TD vAlign=top width="10%" height=63>
<P align=center>13<BR>����</P></TD>
<TD vAlign=top width="39%" height=63>
<P>�������������� ������������ � ��������� �������������� �������������� ������������ �����</P></TD>
<TD vAlign=top width="44%" height=63>
<P>��������� � ������������ �������� ����� �������������� ������� ������ � �������������� ��������</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=63>
<P align=center>11</P></TD>
<TD vAlign=top width="10%" height=63>
<P align=center>13&#8211;14 ����</P></TD>
<TD vAlign=top width="39%" height=63>
<P>���������� ������� �������������� ����� � ��������� ������������</P></TD>
<TD vAlign=top width="44%" height=63>
<P>������������ � ���������� �����������, ������������� ����������� �������� � ������</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=42>
<P align=center>12</P></TD>
<TD vAlign=top width="10%" height=42>
<P align=center>13&#8211;15 ����</P></TD>
<TD vAlign=top width="39%" height=42>
<P>��������� ������ ����� �� ��������-��������� ������������</P></TD>
<TD vAlign=top width="44%" height=42>
<P>����������� ������������ ������������� �����</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=63>
<P align=center>13</P></TD>
<TD vAlign=top width="10%" height=63>
<P align=center>17&#8211;19 ����</P></TD>
<TD vAlign=top width="39%" height=63>
<P>����������� ��������� ����������</P></TD>
<TD vAlign=top width="44%" height=63>
<P>������������ � ���������� ��������� ������������� ������, ����� ��������� ���������</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=89>
<P align=center>14</P></TD>
<TD vAlign=top width="10%" height=89>
<P align=center>17&#8211;19 ����</P></TD>
<TD vAlign=top width="39%" height=89>
<P>���������� ������ ���������� ������������</P></TD>
<TD vAlign=top width="44%" height=89>
<P>������������ � ����������� ������������� �����, ����� �� ���������� ����������� �������, ����������� ��������, �������� � ����������</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=71>
<P align=center>15</P></TD>
<TD vAlign=top width="10%" height=71>
<P align=center>17&#8211;19 ����</P></TD>
<TD vAlign=top width="39%" height=71>
<P>������� � ��������: ����������������, ���� ��������� ������, �������������� �������</P></TD>
<TD vAlign=top width="44%" height=71>
<P>������������ � ���������� ������������� ������, ������������ ��������� � ��������������� �������������</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=47>
<P align=center>16</P></TD>
<TD vAlign=top width="10%" height=47>
<P align=center>17&#8211;18 ����</P></TD>
<TD vAlign=top width="39%" height=47>
<P>������ ����� � ������������ ���������</P></TD>
<TD vAlign=top width="44%" height=47>
<P>������������ � ���������� ������������� �� ������ � ������������ ���������</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=70>
<P align=center>17</P></TD>
<TD vAlign=top width="10%" height=70>
<P align=center>19&#8211;20 ����</P></TD>
<TD vAlign=top width="39%" height=70>
<P>������������� �������� ������: ���������������� ���������, �������� �������� � �������������</P></TD>
<TD vAlign=top width="44%" height=70>
<P>����������, ����������� ������������� ������������� �������� �����, ���������� ������� ����������������� ���������</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=70>
<P align=center>18</P></TD>
<TD vAlign=top width="10%" height=70>
<P align=center>20&#8211;21 ����</P></TD>
<TD vAlign=top width="39%" height=70>
<P>������������ ������� ��������������� ��������� �����������</P></TD>
<TD vAlign=top width="44%" height=70>
<P>������������ � ����������� ������������� ����������� �������� � ������, ����������� ���������� ������</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=35><B>
<P align=center>&#8470; �/�</B></P></TD>
<TD vAlign=top width="10%" height=35><B>
<P align=center>����</B></P></TD>
<TD vAlign=top width="39%" height=35><B>
<P align=center>���� ��������</B></P></TD>
<TD vAlign=top width="44%" height=35><B>
<P align=center>��������� ����������</B></P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=105>
<P align=center>19</P></TD>
<TD vAlign=top width="10%" height=105>
<P align=center>20&#8211;22 ����</P></TD>
<TD vAlign=top width="39%" height=105>
<P>���������� ������� ��������� ������������� � ��������� �������� � ����� (� ����������� ����������� � ���������� �� ������ ���������� ��������)</P></TD>
<TD vAlign=top width="44%" height=105>
<P>������������ � ����������� �������������, �������������� �������� ��������, ������������� ��������</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=137>
<P align=center>20</P></TD>
<TD vAlign=top width="10%" height=137>
<P align=center>21&#8211;22 ����</P></TD>
<TD vAlign=top width="39%" height=137>
<P>���������� ���������� ������������� ������� � ������������ �����</P></TD>
<TD vAlign=top width="44%" height=137>
<P>������������ ������, �������������� ���������� � ����� ������������ ������� ������������� ����-�����������, ������� ����������� � ������������ ����� ����������� ��������, ������, ������������� ������������ � ���������� �������</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=82>
<P align=center>21</P></TD>
<TD vAlign=top width="10%" height=82>
<P align=center>21&#8211;22 ����</P></TD>
<TD vAlign=top width="39%" height=82>
<P>���������� ������ ������������ ����������� ������ � �������� �������</P></TD>
<TD vAlign=top width="44%" height=82>
<P>������������ ��������� ������������� � ��������� ����������� ������, ����-���������, ����������� ������������� �� ���������� ���������� �������</P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=42>
<P align=center>22</P></TD>
<TD vAlign=top width="10%" height=42>
<P align=center>24&#8211;27 ����</P></TD>
<TD vAlign=top width="39%" height=42>
<P>�������� �������������</P></TD>
<TD vAlign=top width="44%" height=42>
<P>������������ � ���������� ����������� ����� �����</P></TD></TR></TBODY></TABLE><B><I>
<P align=justify>&#61480; (495) 365-01-07, 366-53-02, 366-23-51, 366-06-94<BR>���� (495) 365-11-07<BR>E-mail: info@ibdarb.ru<BR>http://www.ibdarb.ru</P></B></I>


</body>
</html>

