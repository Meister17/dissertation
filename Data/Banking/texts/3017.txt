<!--
ID:35308247
TITLE:������ ����������� ������������ ����������� ��������������� ������ �.�.���������� ��������� ����������� ��� �.�.��������� (�� 19.04.2005 � �-01/5-245)
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:11
ISSUE_DATE:2005-06-15
RECORD_DATE:2005-09-20


-->
<html>
<head>
<title>
35308247-������ ����������� ������������ ����������� ��������������� ������ �.�.���������� ��������� ����������� ��� �.�.��������� (�� 19.04.2005 � �-01/5-245)
</title>
</head>
<body >


<P align=justify><FONT>� ���������� ���������� ������ ��������� �������������� ��������� ��������� ����������� � ����� � ���������, ����������� ��-�� ������������ ���������������� �������� ����������� ��������������� ������ �������� ����� � ��������� ����������������� ��������� ���������, ����������� ������������� ������� � �������������� ������������� � ����������������� �������������� ��������� �������� ������� �� ������� � ���������� ������. </FONT></P>
<P align=justify><FONT>������������� ����������� ������ ��������� ��������������� ���������������� �������� ����������� ��������������� ������ ������� �� ��. 13 ������������ ������ �� 23.06.1999 &#8470; 117-�� "� ������ ����������� �� ����� ���������� �����" (����� &#8211; ����� &#8470; 117-��), �������� ������� �������� ���������� �����������, ������������ ��� ������������� ��������� �������� �� ���������� ���������������� �������, ������������ ����� ���������� ��������� ��������.</FONT></P>
<P align=justify><FONT>������ � ��� �������� ������ �������� �������������� ���������� ������������� �������� �������������� ������������� ����������� ��������� ������ � ������ � ���������������� ��������������� ������ ��. 13 ������ &#8470; 117-�� �� ������ ��������� (��.: ����������).</FONT></P>
<P align=justify><FONT>�������� �� �������� �������������, ��� ������ ��� ���������� ����������� ������� ����������� ��������������� ������ �� ���������� ��������.</FONT></P>
<P align=justify><FONT>���. �.�.��������</FONT></P>
<P align=justify><FONT>���. 291-50-53</FONT></P><B>
<P align=center><FONT>���������� ��������� ������������ ���<BR>�� ������� � �������� ����������� ���������� ����� ������ 13<BR>������������ ������ �� 23.06.1999 &#8470; 117-��<BR>"� ������ ����������� �� ����� ���������� �����"<BR>� ���������� �� �������������� ���������� �������������<BR>�������� �������������� ������������� �����������</FONT></P></B>
<P align=justify><FONT>� ������������ �� ��. 13 ������������ ������ �� 23.06.1999 &#8470; 117-�� "� ������ ����������� �� ����� ���������� �����" (����� &#8211; ����� &#8470; 117-��) �������� ���������� �����������, ������������ ��� ������������� ��������� �������� �� ���������� ���������������� �������, ������������ ����� ���������� ��������� ��������.</FONT></P>
<P align=justify><FONT>� ����� � ���� ������� ��������, ��� �������� ��. 96 �� �� �������, ���������� �� ��������� �����������, �� ������ ����������� ���������� ���������������� �������� ����������� �������������� �������� ������� �������������� ����������� ������� � �������������� �������, ��������������� ����� ������� ������������� ������ �����, ������������� �� ������� �������������� ��������� � ��.</FONT></P>
<P align=justify><FONT>� ���� ����� �������� ������������� ������������ ������������ ������ �������������� ����������� � �������� ������������� "����������� ���������� ����������� � ������������� ��������� �������� �� ���������� �������� �������", ��������� ������������ �������������� �� �� ���� �������������� ������� �������, � �� ���� ������� ��������� �����������.</FONT></P>
<P align=justify><FONT>� ���������������� ��������������� ������ ��. 13 ������ &#8470; 117-�� �� ��������� �� �������������� ���������� ������������� �������� �������������� ������������� ����������� ��������������� � ��, ���, ������ �������� ������, ����������, ��������������� � ������������� ��������� �������� �� ���������� �������� �������, ����� ���� �� �������� ����� � ����������� ���, ������������� ������������� ���������.</FONT></P>
<P align=justify><FONT>� ���������� ���������� ��������, ��� �������-����������� �������� ���������� ����� ��. 13 ������ &#8470; 117-�� ���� �� ���� ��������� ������������� ���������� ��������� �������� � ������� ������������ �� ���� ������� �������, � �� �� ���� ������� ���������� ����������� (������������� ��� ���������� ������ �� 04.03.2004 �� ���� &#8470; �09-701/04-��). ������ ����� ������������ �� ���, ��� � ������ ������ �������������� ������������ ��������� �������, � �� ������ &#8211; ����������� ������� ������� ���, �.�. �� ������������ ����������.</FONT></P>
<P align=justify><FONT>����� �������, ��������������, ��� � ������ �������������� ������ �������� ������������� ������������ ����������� �������� ������������� ���������� ���������� ��. 13 ������ &#8470; 117-��. �������������, � ��������� �������� ����������� �������� ��������� ���������������� ����������������, ���, � ���������, ��������� ������ � ������ � ���������������� ��������� ����������, ����������� ����� ������� � �������������� �������������.</FONT></P>
<P align=justify><FONT>��������� ���������</FONT></P>
<P align=justify><FONT>������������ ���&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B>�.�.��������</P></B></FONT>


</body>
</html>

