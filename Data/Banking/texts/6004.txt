<!--
ID:37505942
TITLE:�������� ������ ������ ���
SOURCE_ID:3126
SOURCE_NAME:������� ����������� ����
AUTHOR:
NUMBER:2
ISSUE_DATE:2007-02-28
RECORD_DATE:2007-04-06


-->
<html>
<head>
<title>
37505942-�������� ������ ������ ���
</title>
</head>
<body >


<B>
<DIR>
<DIR>
<P align=justify><FONT>�������� ������� ���������� ������� �� ������ �������������� �����. ������ ������� &#8220;�������� �������&#8221; ����� ������������� ��� ���� ������������ � ����������� ������ ��� �. �.</FONT></P></DIR></DIR></B>
<P align=justify><FONT>����� ��������� ������������� ������ �������� ������� ������ �� 27.11.2006 &#8470; 154�, 155�, 156�. ������ ������ �������� �� ���������� 17 ������. ������ &#8470; 154� ������ ����� �������� ��� 3/200 &#8220;���� ������� � ������������, ��������� ������� �������� � ����������� ������&#8221;, � ������� �������� ����� ������� ���������. � ������� &#8470;&#8470; 155� � 156� ������ ��������������� �������� � ��� ��������� �� �������������� �����, � ������� ����������� �������� �������. ��������� ����� ���������, ������� � ���������� �� 2007 ���.</FONT></P>
<P align=justify><FONT>������, ��� ��������� �������� ������� � ����� � ������� ���������, �� ��������� �� 1 ������ 2007 ����, ����������� � ����� ����������� � ������������ ������������� �� ��������������, ���������� � �������� � ������� ���������� �� ���� 84 &#8220;���������������� ������� (���������� ������)&#8221;.</FONT></P><B>
<P align=justify><FONT>������� �������� � �������� ������ ����������</FONT></B><FONT> </FONT></P>
<P align=justify><FONT>�� ������ ����� ���� ��������� ����������� �� ������ �������� ��������� �������������, ���������� � ��������. ���� ������������� ��������������� ������� � ������, �������� ����������� ��������� �������� ������� �� �������� ��� 3/2000. �� ���� ����������, ��������� ���������� �������� ������ ������ ��� ������������� �� ����� ����� ������ ��� ����� � ������, ���� �� �������� ����. ��� ���� �������� ����� ������������ ������ ������ �� ��.</FONT></P>
<P align=justify><FONT>���� ����������� ���� �������� ������� �� �������� ��������������, ��������� �������� �������, ��������� ������� ��������� � ����������� �� ��������� ������ ��� �������������. ����������� �������� �������� ������ ���� ����������� ��� 6/01 &#8220;���� �������� �������&#8221; � ������������� ���������� �� ����� �������� �������&#8221;, ��� 14/2000 &#8220;���� �������������� �������&#8221;, ��� 15/01 &#8220;���� ������ � �������� � ������ �� �� ������������&#8221;, ��� 5/01 &#8220;���� �����������-���������������� �������&#8221;, ��� 19/02 &#8220;���� ���������� ��������&#8221;, ��� 9/99 &#8220;������ �����������&#8221;, ��� 10/99 &#8220;������� �����������&#8221;.</FONT></P>
<P align=justify><FONT>������ ������� ����� ������������, ���������� � ��������, �������������. �� ������������� ���� ���������� ��������� ������ ���������� � �������� ��������. � ������� �������� ������� �������������� ���, ����� ��� ���� �������� ��� �������� ��������. ������� � ���� �������� ��������� � ���, ��� ������������� ������ ������� ��� ������������ ����� �� ������ �� ����� �� ��, �� � �� �����, ������� ���������� ��������� (�. �.). ��� � ������, �� ������ �������, ���������� ��������� ����� �������� �� ������ ������ ��� ������� ��������. ���������� ���������� ���� ������������� ������������ ����������, �� ������ � �������� �������. �������� ������� �� ��� ����� ��������� �� ����� 83 &#8220;���������� �������&#8221;.</FONT></P><B>
<P align=justify><FONT>��� �� ����� �������</FONT></B><FONT> </FONT></P>
<P align=justify><FONT>��������, ����� ��������� ���������� �� ��������� ������������� ������ � ����. ���� ������������� ��������� (���� 21) �� ������������� �� ��, ��� ������� �������������� � ������, �������� �� ���, � ������� ���������� ���� �������������. ��� �� �����, � ��������� ����� �������� ������� ������� ���� �� ������� ��������. � ��� ������, ��� ��������� �������� � �������������� ��������� ��������� ��� ������������ � ��������� ����������� ���������� �����. ������� � �������� �������� ������ ����� ���������� � ��� 18/02 &#8220;���� �������� �� ������ �� �������&#8221;.</FONT></P>
<P align=justify><FONT>����� ����, �� �������� ��������, ��� ������ ���������� �� �������� ���� ������������� �������� ������������� � ��������� ���������. ������� ������ ���������� ������. ���� ���� �� ��������������, �������� �� 1 ������ 2007 ����, �� ���������� ������ ����� ���� ����. ���������, ������� ����� ������������� (����������� � ������������ �������������), ����� �� ����������� ����� ����������� �� �� ��������� �� ��� ����. ��������� ������� ���� ������� �� ���� 84 &#8220;���������������� ������� (���������� ������)&#8221;. ���������� ��������� ������� �������� � ������ �� ���������� �������� (����� &#8470; 3 ������������� ����������) � ������� ���������� �� 2007 ��� ��� �������������, ��������� � ���������� ������� ��������. ��� ��� ����������� � � ������� ������.</FONT></P><I>
<P align=right><FONT>������� �������, �������<BR></FONT><FONT>������ &#8220;</FONT></I><FONT><I>����, ������, �����</I><I>&#8221;</P></I></FONT>


</body>
</html>

