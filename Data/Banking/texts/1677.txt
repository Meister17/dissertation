<!--
ID:36328305
TITLE:� �������, ������ ��������� � ����� ������������
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:������� ���������

NUMBER:4
ISSUE_DATE:2006-04-30
RECORD_DATE:2006-06-13


-->
<html>
<head>
<title>
36328305-� �������, ������ ��������� � ����� ������������
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgwhite>
<TD>
<P class=Main align=justify>������� &#8212; ��� ������� ���� ������� ����� ����� �������� ���� ����������. ����� ��� �������� � ������� ������, � ���� ������� �� �������. � ����� ��� ������������ ��������������� �����, � ��� ��� ��������� ���������� � ������ ���� ���������. ������ ��� ���� �� �������, ��� ������ ��������� �� �� ������� � ���� ������� ����� &#8212; ����� �� ���������. ������ ��� ������� � ��� ���� ������ ������ ���. 
<P class=Main align=justify>������, �� ����� ���������. �� ���� �������� &#8212; � �� &#171;�������&#187;, � �������� �����, ����� �� ��������� �� ������������ ������, ��� �� ������ �� ���������, �� �������� ����, ��� &#171;��� ������ � ������������&#187;. �� ������ �� ������ ���� ���������� �������, �� ��� � ���� �������, ���� �������� �� ������, � �����-��, ���� �����������. �� ���, �����, ������ � ���������� ���� ���������. � ��� �� �������, �� ������ ���� ���������, ��, ������, �������� ��� �������� �� ����. �� �������� ���������� ��-���������: ������ ��������� � ��� ���. 
<P class=Main align=justify>� ������ ����� ������ ������� ��� ��������, �������� ��������, � ��������, ����������� � ������ ��������. �������, ������, ��� ��� &#8212; ����� ���� ���������. ��������� &#171;������&#187; ��� ��������� ������������. 
<P class=Main align=justify>��������� ������ �� �������, �� ��������� ��������� ���������, ����� ������ �������� � ������� ������. �� �� ����, ������ �� ���� ������ � ����� ��������, �� ���� �����: �������������� �� ����������� �� ��������������� ������ ��������. �����, ������ ��� �������� ��� ���������. 
<P class=Main align=justify>������� �� ������������� ������������, � ����� ������� ��� &#8212; �� ������, ����� � ��������� &#8212; ������ �������� ������������� ����������. ��� ��� �����, ��, �������, ������, ��������� ���� &#171;����� ���������&#187; �������&#8230; �� ����� ��� ���, ������, ��������, ��� �������� �������������� �������� �� ��������� ������, &#8212; ������ � ������� ������ ��� �� ����. � ������������ ����� ������������� � ���� ����� �������, ������, � �� ������ ��������������� ����� ��� ��� � ��������������� ��� ���� � ���� ������. 
<P class=Main align=justify>� ������ �� �������� &#8212; ������� ��������, ����� �� �������������� �������� �������� � ���� ������ ������� �������� ������ ������� ���� ���������. 
<P class=Main align=justify>��� ���� ����������, ���, �������, ������ ����� ������ ����� ����������� � �������: ������������ � ����, ��� ��������������� �� ������������ ��������. � ��������� ����� � �������� �������� �������� �� ����, ������� �������� �������� &#8212; ������ ���� ������ �������� ���������� �������� ����� ���������-������������ ������. 
<P class=Main align=justify>����� ���, �������, ����� ������������� ��������. � ��� ������ � ���� �����������, ��� ����� ����������, ���, �������-��, ������ ����� ������� �������� ����� ������������: ����� ���, ��� ������� ��������, � ������ ���� ������ �� � ������� &#8212; �� ���� ������� �� �������� � ����� ���������. ������, �� ������ ����������� ��������, �������� ������� ������ ��������� ����������, � ��� ��� ����� ������, ��� ��� ������. �� ������� ������ �� ���� ����� &#8212; �������� ������� ������ ����� �������-�������, ����������� ����������. �� ���� �� ��� ���-���� ������ �� ��������. 
<P class=Main align=justify>�����������, � �������� &#171;����������&#187; � &#171;������� ������&#187; �������� ����� ����� ������ ����� � � ����������� &#8212; ����� ���������� �����, ��� � ���������������� ������ ��� ����������. �� ����, �������, ���� � �������� ���-�� ������. ������� ����� �� ������� ����� ����� �������� &#8212; ������� ������������� �������� � ��������� ���� ������ �������������� �������� � ��������� �������, ������� �� ������ &#171;����� �������� �����&#187;. � ����� �������������� ����� ��� �����, ���� ���� ������������ ����� �� ������� ��������� �� ������������, ������������ �� �������, � ��� � ������ &#8212; �� ���������. 
<P class=Main align=justify>�� ���� ����� ���������� �������� ������� �������� �� ����� �����. �� ��� ������������ �������, ��������� ��������� ����� ��������� �������. ������ ��� ��������, �� ����� ����� �������������� �� ��������� ������, ��������� � � ����������. 
<P class=Main align=justify>&#8230;������ ����� ����, ������, ������������� ����������. �� ������� ��� �� ����� �������� �� � ����� ��������. � �������� ��, �� ���� ���������, ������ ���� &#8212; � ������ ���������� � ������� ��������������. �����, �� ����, ��� ����������� ��������� ����������� ����������� �������� ������� ����� �� ����������: ������� ������ �����, ����� �������� � ������� �������, ����� ����� �������� ������, � ��� ���-�-���� &#8212; ���, �����. � ���� ������, ��� ������������ �������� ��������� �� ������� � ����������� ��������� (�� ����������� ������ ������������� ����� ��������� �������), ����� ������������, ��� � �������� ������� ������ ����� �� �����������. 
<P class=Main align=justify>� ����� ����� ������������ ������ &#8212; ���������� �� �������� ���� ������, � �� ��������� �� �����������. �� � ����, ����������, � ���� ���� �������. ��� ��� ��������� �� ������ ���� ��������� � �� �� ���������. � ����� ���������� ���� �������������� ����� � ��� ���������.</P></TD></TR></TBODY></TABLE>


</body>
</html>

