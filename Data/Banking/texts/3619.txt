<!--
ID:36772150
TITLE:����� ������������ �������� ���������� ��������� "�� ������ ������ �� ����������� ��������� ��� ���������� ������� ������� ����������� ��������"

SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:17
ISSUE_DATE:2006-09-15
RECORD_DATE:2006-10-06


-->
<html>
<head>
<title>
36772150-����� ������������ �������� ���������� ��������� "�� ������ ������ �� ����������� ��������� ��� ���������� ������� ������� ����������� ��������"

</title>
</head>
<body >


<P align=justify>� ����� � ��������������� ��������� ������ &#8211; ������ ���, ����������� �������� �� �������� ����������� ��������, ���������� ���������� ������ ���������� � ������ ������ � �������� ���������� ������� ��������� ������� �� ����������� ��������� (���) ��������� ������, ����������� �� ������������� �����.</P>
<P align=justify>� ���������� ������������ � ���������, ����������� � �������� �������������� ������� ������, ����� ������, ���������� ���������� ������ � ���� ���������������� ������ ���� ���������� ������������� �������, ����������� ������ ��������������� ��������� � ��������� ������ &#8211; ���������� ����� ����������� ��������. ������ ������� ���������� � ������ ������� ������ �� 04.08.2006 &#8470; 03-04-03/20 � ����� ���, � ������� ��������� ������� ���������� ��. 9 �. 3 ��. 149 �� �� ���������� �������������, ��������������� �������� �� ���������� ������� ����������� ��������.</P>
<P align=center>�����<BR>������������ �������� ���������� ���������<BR>�������� ���������� ������������ ���������<BR>� ���������-�������� �������� �.�. ���������<BR>(�� 04.08.2006 &#8470; 03-04-03/20)</P><B>
<P align=justify>�� ������ ������ �� �����������<BR>��������� ��� ���������� �������<BR>������� ����������� ��������</P></B>
<P align=justify>� ����� � ������������ ��������� � ������� ���������� ������� ������ �� ����������� ��������� ��� ������������� �������� �� ���������� ������� ����������� �������� ������ ������ �������� ���������.</P>
<P align=justify>�������� ��. 9 �. 3 ��. 149 ���������� ������� ���������� ��������� (����� &#8211; ������) �������� �� ���������� ����������� �������� � ������� ����������� ������ ���������� ��������� � ������� �� �������� ��������������� ������� �� ����������� ��������� ��� �������, ��� ��� ������ �������� � ����� �� ����������������� �������� (��������������� ��������� ���������, ��������� ������������ ����� ���������� ��������� ��� ���������� ������).</P>
<P align=justify>����� �������, ��������� ������ ������� �����������, ��� ������������ �� ��������������� �������� �� ���������� ������� ����������� �������� � ������� ����������� ��� �������, ��� ����������� ������� �������� � ���������� ������. �� ��������� ������ ����� � ������, ���� ��� ���������� ������-��������� ������ ��������� �� ��� ���������, ��� ������������� ������������� ���������� ������������ �� ��������������� �����-�������� ���������� ����� ���������, ��������������, ��� ������ ����� ���������� � ����������������� ��������� �����-����������. ��� ���� ��������� ������ �������� ��������������� ���������� �� ����������.</P>
<P align=justify>������������ ���������� ��������, ��� �������� ��. 9 �. 3 ��. 149 ������� �� �������� ��������������� ���������� ����������� �������� ������������������� (�� ����������� ��������� � ��. 6 �. 1 ��. 164 �������) ���������������� ����� ����������� �������� � ����������� ������ ���������� ���������, ������ ����������� �������� � ����������� ������ ��������� ���������� ���������, ������������ ����� ���������� ��������� � ������.</P>
<P align=justify>� ������������ �� ��. 143 ������� ������������������� ������ �� ����������� ��������� ���������� � ��� ����� �����������, � ����� ������� �������� �. 2 ��. 11 ������� ��������� �����.</P>
<P align=justify>������������ ������������ ��������� ������ �����������, ��� �������� � ������������ ��������� ����� ����� ��������� �����, ������� ����� �������� ����� ������ �� ������������� �������� � ������������ ���������, � ����� ���� ������, �������������� ������������ �� ��������� ������������ ������ �� 10 ���� 2002 �. &#8470; 86-�� "� ����������� ����� ���������� ��������� (����� ������)".</P>
<P align=justify>����� �������, �� ���� ������������� ���� ��. 9 �. 3 ��. 149 ������� �������, ��� ��� ���������� ������� ������� ����������� �������� ����������� �� ������ ������ �� ����������� ��������� ��������� ��� ������� ������� �� ��������, �� ����������� �������� �� ���������� ������� � �������� �������������� �������, ����� � ����������, � ������������ �������� �����, ������� ����� �������� ����� ������ �� ������������� �������� � ������������ ���������, � ����� ���� ������.</P>
<P align=justify>�������� �� �������� ����������� ����� ����������� ����������������, ���������� (�������� ����� �������������) ������� � ������ ������ ����������� �������� � ������� ������ ������ � ����� ������ ������������� �� ��������������� (�� �������� ���������������) � ������ ����������.</P>
<P align=justify>� ������������ � �. 1 ��. 11 ������� ���������, ������� � ������� ������������, ��������� � ������ �������� ���������������� ���������� ���������, ������������ � �������, ����������� � ��� ��������, � ����� ��� ������������ � ���� �������� ����������������, ���� ���� �� ������������� ��������.</P>
<P align=justify>���, �. 1 ��. 454 ������������ ������� ���������� ��������� �����������, ��� �� �������� �����-������� ���� ������� (��������) ��������� �������� ���� (�����) � ������������� ������ ������� (����������), � ���������� ��������� ������� ���� ����� � �������� �� ���� ������������ �������� ����� (����).</P>
<P align=justify>�������������, �����������, ��������������� �������������� ������������ �� ��������������� ������� �� ����������� ��������� �������� �����-�������� �� ���������� ������� ����������� �������� ������-�����������, �������� ���������:</P><FONT face=Wingdings size=2>
<P align=justify>l</FONT> �������, � ������������ � ������� ����-�������� ������������ ���������� ����������� �������� � �������<FONT face=Arial> </FONT>�����-����������, � ����� ������������ ����� ���������� ��������� � �������������;</P><FONT face=Wingdings size=2>
<P align=justify>l</FONT> ��� ������-�������� ����������� �������� � �������, ����������� ��������������� ��������������� �����-�������� � �����-����������;</P><FONT face=Wingdings size=2>
<P align=justify>l</FONT> ����� �������� ������������ ����� ���������� ��������� �� ���������� �������� � ������������ ���������, �������� �����-���������� (�� ����������� �������, ����� ����������� �������� ���� ������).</P>
<P align=justify>� ������ ���� � ��������� �� ���������� ������� ����������� �������� �� �������, ��� ����-���������� ��������� �� ��������� �������� ��������, �� �� ����� ��������� ����� ������������� �� ������ ����� ��������� � �����-����������, � �������� �� �� ���������� ������-��������� ������������� �� ��������������� ������� �� ����������� ���������.</P>
<P align=justify>� ������ ���� ���������� �������� ������������� ����������� �������� �������������� ������-����������� �� ��������� ���������� ���������, ����������������� ������� ����� ������������� ������ �����, �� ���������� ������� (�.�. ������ ����������� �������� ������� �� �������������� �������), ����� �������� �������� ��������������� ������� �� ����������� ��������� � ����������������� �������.</P>
<P align=justify>� ������ ���� � ������������ � ��������� �������� ����-�������� ��������� ����������� ������� � ������� �����-����������, � ��� ����� ����� ������, ������������ �� ������ �����, �� �� ���� ���������, ����� ����������� ������ (�.�. ������ ����������� �������� �� ������� �� �������������� �������), ������������ �� ��������������� ������� �� ����������� ��������� ����������� �� ��������� �. 1 ��. 996 ������������ ������� ���������� ���������, �������� �������� ����, ������������� ������������� �� ���� ���������, �������� �������������� ����������. �����������, ��������������� �������������� ������������ �� ��������������� ������� �� ����������� ��������� ��������� ��������, ��������:</P><FONT face=Wingdings size=2>
<P align=justify>l</FONT> �������, � ������������ � ������� ����-�������� ������������ ���������� ����������� �������� � ������� �����, ������������ � ��������� �����-���������;</P><FONT face=Wingdings size=2>
<P align=justify>l</FONT> ��� ������-�������� ����������� �������� � �������, ����������� ��������������� ��������������� ������;</P><FONT face=Wingdings size=2>
<P align=justify>l</FONT> ����� �������� ������������ ����� ���������� ��������� �� ���������� �������� � ������������ ���������, �������� �����-���������� (�� ����������� �������, ����� ����������� �������� ���� ������);</P><FONT face=Wingdings size=2>
<P align=justify>l</FONT> ����� �������� ������������ ����� ���������� ��������� �� ���������� �������� � ������������ ���������, �������� �����-��������� (�� ����������� �������, ����� ������-���������� �������� ���� ������).</P>
<P align=justify>�������������� � ������ ���� � ��������� �� ���������� ������� ����������� �������� �������, ��� ����-���������� ��������� �� ������ �����, �� �� ���� ���������, �� ����������� ������, � ��������� ������ �� ����������� � ����� �� ��������, ��������� � ��. 9 �. 3 ��. 149 �������, �� ����� �������� � �����-�������� �������� ��������������� ������� �� ����������� ��������� � ����������������� �������.</P>


</body>
</html>

