<!--
ID:38639693
TITLE:������� � �������.
SOURCE_ID:11517
SOURCE_NAME:���
AUTHOR:
NUMBER:1
ISSUE_DATE:2008-01-31
RECORD_DATE:2008-01-14


-->
<html>
<head>
<title>
38639693-������� � �������.
</title>
</head>
<body >


<P align=justify>�����������: ������������ �������� ����������� � ������, ��� ����;</P>
<P align=justify>��������, ����� ��� �� �������� ���������� ������ ��� ���������, ����� ����� ������� �� ������. �� ��� ��������� ���� � ���������� ������� � ������ ����������� �� ������ ����������� ������� ����� ��� ���������� ������� ��������. � �������� ������ 2007 ����, ����� ���� ���������� ����������� � ���� ���������� � ����������, �������� ����� ������ � ������ ������ �������� Renault, Ford, Nissan, Toyota, Volkswagen � ��. ���� ����� ���� ����� ������� ��-�� ������ �������������� �� �������� ���������� ������� - �������� 5%. ��� ���� ������������� ������ ������� �� ���� ������ 30% ���������� �������������.</P>
<P align=justify>����������� ���������� ��������, ���������� ����� ����������, ��� ��������� � ������ ������������� �������. �������� ��������� Ernst &amp; Young, � 2007 ���� � ����� ������ ����� ������� 450 ���. ��������, � � 2010-��, ����� ������ �� ��������� �������� ���������� �����������, - ����� 1 ��� �����. "������������� ����� ������ ������������� �������, - �������� �������� �� ������ � ��������� ������������� �������������� ��� ����������� � ��������� ������ Ernst &amp; Young ����� ����. - ������������� �� �� �������". � ���� � ������ ����� �������� � �������� ���������� ������������� ���������� ���� �� �� �����, �� ������� ����������� ��������� �������� �����������.</P>
<P align=justify>���� ���� �������� ���������� ������������� ��������������� ������� � ������� ������. ��� ���������� � ��������� � ����� ��������� ��������� Magna, � ������� �� ����������� ��������� � ���������� Stadco �������� "����������-����", ������ ���������� �������� ��������� Siemens. �������� ����������� ����������� ���, ��� ��� �������� � ����� ��������������. ��� ������������� ����-��� ������ � ������ ������� �� �������. ����������� �������� ������������ ����������� � ������ �� ��������� � �������� ����� ������� �������� � ������������� �������.</P>
<P align=justify>����� ��� ������� �������������� ��-�� ������ ��������� ��� ������, �������� �� �������� ���������� ������. ���������� ����������� � ��������� ���������: ����������� ������� �����, �� ������� ���������, ������������ ������ �� �������. "��� ����� ��� ���� �������� ������� � ���������������, - ������������ �������� �������� "����������-����" ����� ������. - ������� �� ����� � ������� ���� ��� ������� ����������� ������ �� 70%". � ��������� ���-��� ���� � ���������� ���������� ���� ��� ����� ������� ��� � �������� �����������. �������� � ������ ������� ��, ������� ����������� �������� Stadco ����� ������, ��� ������ �� �������� ����������� ������������. � �� ���������� �������� ������� ����������� �������������� ���������, � ����������� - ������� �������� � ������ ������ ���� �� �������������� � ���� ��� ������������ �����. ��������� ���� ����� ����� � �������� �� ������ ���������. ��� ���� �����������, �������, ����������� ������ � ����� ��������, ���������� �������� � �������� ����������������� �������.</P>
<P align=justify>***</P>
<P align=justify>"�� ����� ��������� ���������� ������� ���������� �������������", �������� �� �������� Renault � ������ ������ ���</P>
<P align=justify>"��� ����� �������� �������������� ����� � �����. � ������ �� ������, ��� ����������� �� ��, ��� ����� ��������� ���������� ������� ������������� � ������� ����������� ��� ���������� ������������ ��������. ����� ����, ������-��������� ����������� ������� � ���� ����������, ������� ������������� ����������� ����� � ��������� �����������. �� ������ �������� ������ "����������" ����������� ������������ ��� ���������� ������� 20%. � ����� 2007 ���� �� ������� ��� ���� �� 40%. � � 2009-��, ����� �������� �������� ������ ������ �� 160 ���. ����������� � ���, ������� ����������� ������������� ��������� 50%".</P>


</body>
</html>

