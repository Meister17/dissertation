<!--
ID:37479762
TITLE:SAS &#151; �� ���� � ����������������� �����������
SOURCE_ID:11528
SOURCE_NAME:���. ����� � ������� ���
AUTHOR:������ ������, �������� �� �������� ������� SAS ���, ���������� ������ 
NUMBER:1
ISSUE_DATE:2007-01-31
RECORD_DATE:2007-04-02


-->
<html>
<head>
<title>
37479762-SAS &#151; �� ���� � ����������������� �����������
</title>
</head>
<body >


<H1><STRONG><FONT size=3>�������� SAS �������� ������ ������������ ������ ��������� ������������ �����������, ���������������� ��� ������-�������. Ÿ ������� ���������� ����� ������ ����� �������� �� ����� ����, � ����� ������������ �� ��������, ��� ��� ��������� ������� ��, ��� ���������� &#171;���������������� ������������&#187;. � ��� �� �� �������� ��, ��� ������� SAS �������� � 13 �� ���������� ������� ���������� ������ &#8212; ������� �������� ������� ���������� ������� ����� �� �������� �������� � �������������. </FONT></STRONG></H1>
<P align=justify>��� �������� ����������� ����� ���������� �������, ��� ���� ��� ���������� ��������� ������, ����� ��� ����� ����������. �������� ��� �������� �� �� ������ ����� &#8212; ������� ����������� ���������� ����� �� ���� ���������� �������, �, �� ��������� �������, ���� �����, � ������� �� ������������ � 20%. 
<P align=justify>������� ����� ���������� � ��� ������������ ������ �������, ������� ����� &#171;������ � ����&#187;. �������, ����� &#8212; ������������ ����� �������, � �� ������ �����������. ������ ���� ������� ������ ��������, � ��� ���� ������ ����� ����� ������� ������������ ��������. 
<P align=justify>���� � ��� ���� �������������� ����������� ������������ ���������� ����� &#8212; �� ����� ������������� ������ �� �������. ������ � ����������� ������ ��� ������, ��� ��� ������ ����������� ������ �������� ������������ �������� &#171;���������&#187; ��� ������� ������� �������� ������������ &#8212; � ������, �������������, �������� � ��������������� ���������. 
<P align=justify>��� ������� ������ �������� ����� � �������� ������� ������ ����-�����������, ��� ������ &#8212; �������� CRM. ������-�� ������ ��� ��������� ������� �������������, �� ���� &#8212; �� ���������������. �� ���� �� ������� ��� ����������. ������ ���� � ���, ����� �� � ����� ��� �� ����������� ������ ��� ����� � ��������� ������, ������� ��������&shy;��� �������. ��, ������� � ���� �������� �������� Business Intelligence &#8212; ��� ���������� BI. 
<H3>����������� &#171;��-��������&#187; </H3>
<P align=justify>������������� �������� IDC, ����������� ������������ ����� �������������, � ���� ������ ��������, ���, � ������� �� ������ �����������, ������������ ������ &#171;�����������&#187; � �������� ���������� �����, SAS ���������� <STRONG>��������</STRONG> ���������� �����������, � ������� �������������� � ������������� ������������. � ���� ������� ��� ������������� � CRM, ����� ������ ����������� �� ��������� �� ���� �������� ��������. 
<P align=justify>����, �������� SAS, ����������� ��������� � ���������� ���������� ������� ��� �������������� CRM, ��� ������, ��������� ������� � ������� ������ &#8212; ���, �� ����� ����� ������� �������. ������ ���� (Corporate Performance Management) ���������������� ��� <STRONG>������� ���������� �������������� �������. </STRONG>
<P align=justify>����� ������� ���� ����� ������� ����� �������� � ���������� ������. ����� ��������, ���� ��� ���� ��� �����������, ���� � ����������� � ��������� ���������� ������-�����. �������������� ������� ���������� ���������� � �������� ����������. �������, ��� ��� ����� ������������ ���������� ������������ ����� ����-������� �������������� (��� ���� ������ ����������) ��������� ���� �� ������ �� ������-��������� ���������� �������� � �������� ������� �� ����� ��� �������. ��� ���, SAS ���������� ������ ���� &#171;���������&#187; &#8212; ��������������� ��������� � &#171;������&#187; &#8212; �������� ������-�������, �������������� ������� ����� �������� ��� ���������, ������������� � ����������� ������������. 
<P align=justify>����� ����������� ������������ ��������, ����������� ������ �������������, ������� ������������� ���������. 
<H3>�� ���������� �� ��������� </H3>
<P align=justify>����������� ���, �������, �� �����, ������ � ������ ���������� CPM-c������ ������ �������������� ����� ��������� ���������. ������ � ���� ������ ������������ SAS ������� � ������� ������-������� ������������� �������� �������� ������, � ����� ����� ���������� &#171;�������� �� ����� �� ��������&#187;. 
<P align=justify>����� ���������� ���������� ������� ����� ������ ������������� ������������� ���, � ���������� ��������. ������ ��� �������� SAS ���������� ��������� ������, ��� ��������� �������� ��� &#171;���������&#187;, � ���� ������� ������������ ����. 
<P align=justify>�� ������� ������ ��� ������������ ����� ����� ����������� �����������, ������� ���� �� ������ ������, �� � ���������������� ������������� �� ������������� ������ �����������. ��������� ����� ����������� � ���, ��� ������������� ������ ��������� ������� � ���������� ��-�������. ������ � ���������� � ����� ���������������� ��������� &#8212; ���� ���-������� �� �������������, � ���������� �� ���������� ������. 
<P align=justify>� ����� �������, ������ ������������ ��������� ����� �� ������������ � ������������: �� ������ � ����� � �� ����� ��������� �� ��� ���� �������������, ������������� ������ ������� ��� ���������� ������� �������. ��� ����, ����������, � ����� �������� �������, �� �����������������. 
<P align=justify>����� ��������� ������ �������� ���-�������� ����� &#8212; ������ �����, ��������� �������� ����� �������� � ������ ������� ��������� ��������� ���������. �������, ��� ����������� ��������, ������, ������� ������ �������� ������. ����� ������� ����������� �� ���������, ��������� ���������� ������ &#8212; ��� �������� ��������� ������������ �� ��������. ��������� �������� � ������ ��� �����������, ��� � ���������� ����������� �������������� ��� ����������� &#8212; ������������, ����������� ����� � �������� ����������� �������. ���� ����� ����� �������� �� ������ �� ���������, �� � �� ���������� ��������������. 
<H3>�� ����������, � ���������� </H3>
<P align=justify>������ ������ ��� ���, ��� ��������������� � ���-��������: �� ������� �� ��������� ���������� �� &#171;��������&#187;? ���� � ������ �������� ���� ��������������� ����� ������ I�-�������, � ������ �� ������ ����������� ������ � ������ �� ������, ���������, ������ ������������ ������, �������� ������������ �������� ������ ������� � ��������� �������� � ������ ������������. �������, ������, ��� ������ �������� � ���� ����������� � ������������ ������ �� ���� ������ ��� &#171;�������&#187; ������������� ��������������� ���������, ��� ����������� � ������� ������ �������� ��� ����. 
<P align=justify>��������� ���� ���������������� �������� ���������� ����� �� �������� ����������� ���������� ��� &#8212; ����������� � �������������� ������������ I�-�������� �������. ����� �������, � ������� ��� �����, ��-������, ������� ������� � ���� �������������� &#171;���������&#187;, � ��-������, ��������� ��� �������� � ����� ��� ����������� � ������ �������, ������� ���������� SAS. 
<H3>��� ����� ���-�������? </H3>
<P align=justify>���������� ��������� �������� ������� ��� � ����� ������� �� ��� ������� ��������������. 
<P align=justify><STRONG>SAS Strategic Performance Management</STRONG> &#8212; ������� �� ��������������� ���������� ���������. ��� ����� ������� �������� ������ ��������� �������. ��� ������� ��������: 
<UL>
<LI>������� ���������������� �����������; 
<LI>�������� ���������� ������������� (KPIs); 
<LI>����� ��������� � ���������; 
<LI>������� �������������� ���������� ��� �����������. </LI></UL>
<P align=justify><STRONG>SAS Financial Management Solutions</STRONG> &#8212; ������� ��� ����������� �����������. ��� ���� �� �������� ����������� ���-�������, �������������� ������ ���������� ������������ ��������. �������� ��� ��������������� ����������: 
<UL>
<LI>SAS Budgeting and Plan&shy;ning &#8212; ��� �������������� � ������������; 
<LI>SAS Consolidation and Reporting &#8212; ��� ������������, ������� � ���������� ���������� ����������; 
<LI>SAS Activity-Based Mana&shy;gement &#8212; ��� �������������-������������ ����������. </LI></UL>
<P align=justify><STRONG>SAS Risk Management &#8212; ������� �� ���������� �������</STRONG> ������������ ���������� �������, ������ ������ � ����������, ����������� ��� ���������� ���������, �������� �, ��� �������� ����� ��� ������ � ��������� ��������� ��������� ������ II, &#8212; ������������ ������. 
<P align=justify><STRONG>SAS Intelligence Platform</STRONG> ������ ���������� ��� ������� ��������, ���������� ���������� � �������� ������, ���������� �������, �������������� � BI-���������. ����� ����, ��� ������� ������������ ���������������, ��������� ������ �� ������������ ������������ ������� ����������� ��� �����������. 
<P align=justify><STRONG>SAS Analitic Intelligence</STRONG> &#8212; ������������� �������, ������� ���������� ������ � ���������� ���������, �������� � ����������� ��� ��������� �������� �������. 
<P align=justify><STRONG>SAS Business Intelligence</STRONG> &#8212; ������� �� �������� � ������������� ����������. ��� � ������� �� ��� ��������, ��� ���������� ������ ������� ���������� �� ����������� ������� �������� � � ������ �����. 
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<P class=vinos align=justify><STRONG><EM>�������, ������, ��� � ������� ���������� ��������� ��������� ���������� ������� � CRM &#8212; ��������, ������, ��� � ��������� ������� �� ������ ������, ������� � �����, ��, ���� ������, ����� ��������� �������. � ��� ������. ���� ����� ������� call-����� � ���� ������������, ������ ����, ��� ��������� CRM ��� ������ �����... </EM></STRONG></P></BLOCKQUOTE>
<P align=justify>����, ���� �������� ��������� ����������� ���-������� ���� ������������� � � �������� ������������� ���������. ������� ��������, ��� ������������ ����� ��������� ��������� � ������������� ������������� ����� �������, ������ �� ��������� � ���������� ������, ��� ��� ��� ����� ����� ���� � ������������� ������������, ������ �� ���������� ����������. 
<P align=justify><EM>� ���� ���������� �� ����������� ��� � ������ ������ ���������� � ���, ��� ����� ���-�������, ������� ���������� �������� ��������� �������� SAS. �������� ���������� ������ �� ��������� �������� ����������� ��������� ������� � ���������� ����������� ���������� ������ � �������� �� ���������� �����, � ������ &#8212; � ������� ���������� ������������ ������. � �������� � ��� ���, ��� ������ �������������� � ����� �������� � ���������� ������ ������������ ��������� � ������������. ��� ��� ����������� �������...</EM></P>


</body>
</html>

