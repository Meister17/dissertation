<!--
ID:36451706
TITLE:��������� � ���������� ��������� �����������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:10
ISSUE_DATE:2006-05-31
RECORD_DATE:2006-07-14


-->
<html>
<head>
<title>
36451706-��������� � ���������� ��������� �����������
</title>
</head>
<body >


<P align=justify>���������� ����������� (�����������) ������������ ������, �������� �������� �� ���������� ���������� ��������, ����������, ��� � ������������ � ��������� ������������ ����� �������� ���������� ��� ��������� ��������������� (���������) ����������� ����� � ��������� ���������������� (����������) �������� ���������� ��������� ��������� �����������:</P>
<TABLE cellSpacing=2 cellPadding=7 width=602 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="6%">
<P align=center>&#8470;</P>
<P align=center>�/�</P></TD>
<TD vAlign=top width="25%">
<P align=center>������������<BR>���������<BR>�����������</P></TD>
<TD vAlign=top width="14%">
<P align=center>��������-������� �����</P></TD>
<TD vAlign=top width="14%">
<P align=center>����<BR>������<BR>��������</P></TD>
<TD vAlign=top width="40%">
<P align=center>�����, �������, �.�.�. ����������� ������������ (�����������).</P>
<P align=center>���� ������������ ���������</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>1</P></TD>
<TD vAlign=top width="25%">
<P>����������-����</P></TD>
<TD vAlign=top width="14%">
<P align=center>2826</P></TD>
<TD vAlign=top width="14%">
<P>16.06.2005</P></TD>
<TD vAlign=top width="40%">
<P>109240, ������, ������� ��������� �����, 4.</P>
<P>���������� ���������� �� ������:</P>
<P>123022, ������, �/� 7;</P>
<P>���.: (495) 777-24-29, 589-40-88,<BR>514-74-78;</P>
<P>��������� �� ����������� �������;</P>
<P>61 ����</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>&#8470;</P>
<P align=center>�/�</P></TD>
<TD vAlign=top width="25%">
<P align=center>������������<BR>���������<BR>�����������</P></TD>
<TD vAlign=top width="14%">
<P align=center>��������-������� �����</P></TD>
<TD vAlign=top width="14%">
<P align=center>����<BR>������<BR>��������</P></TD>
<TD vAlign=top width="40%">
<P align=center>�����, �������, �.�.�. ����������� ������������ (�����������).</P>
<P align=center>���� ������������ ���������</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>2</P></TD>
<TD vAlign=top width="25%">
<P>���������-�������������</P></TD>
<TD vAlign=top width="14%">
<P align=center>1726</P></TD>
<TD vAlign=top width="14%">
<P>18.05.2001</P></TD>
<TD vAlign=top width="40%">
<P>109240, ������, ������� ��������� �����, 4.</P>
<P>���������� ���������� �� ������:</P>
<P>109240, ������, �/� 30;</P>
<P>���.: (495) 777-24-29, 589-40-88,<BR>514-74-78;</P>
<P>��������� �� ����������� �������</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>3</P></TD>
<TD vAlign=top width="25%">
<P>������-�����������</P></TD>
<TD vAlign=top width="14%">
<P align=center>1847</P></TD>
<TD vAlign=top width="14%">
<P>25.01.2006</P></TD>
<TD vAlign=top width="40%">
<P>109240, ������, ������� ��������� �����, 4.</P>
<P>���������� ���������� �� ������:</P>
<P>123022, ������, �/� 4;</P>
<P>���.: (495) 777-24-29, 589-40-88,<BR>514-74-78;</P>
<P>��������� �� ����������� �������;</P>
<P>60 ����</P></TD></TR></TBODY></TABLE><I>
<P>"������� ����� ������", 2006, &#8470; 26</P></I>


</body>
</html>

