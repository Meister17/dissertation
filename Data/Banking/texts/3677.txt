<!--
ID:36800265
TITLE:�������� ����������� ���� ���������� ���������� ������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:19
ISSUE_DATE:2006-10-15
RECORD_DATE:2006-10-13


-->
<html>
<head>
<title>
36800265-�������� ����������� ���� ���������� ���������� ������
</title>
</head>
<body >


<B>
<P align=center>��������� ���������<BR>�� ������ 2006 �.</P></B>
<TABLE cellSpacing=2 cellPadding=7 width=607 align=center border=1>
<TBODY>
<TR>
<TD vAlign=center width="7%" height=32><FONT size=3>
<P align=center>&#8470; �/�</FONT></P></TD>
<TD vAlign=center width="19%" height=32><FONT size=3>
<P align=center>����</FONT></P></TD>
<TD vAlign=center width="40%" height=32><FONT size=3>
<P align=center>���� ��������</FONT></P></TD>
<TD vAlign=center width="35%" height=32><FONT size=3>
<P align=center>��������� ����������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=31><FONT size=3>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="19%" height=31><FONT size=3>
<P align=center>3&#8211;4 ������</FONT></P></TD>
<TD vAlign=top width="40%" height=31><FONT size=3>
<P>����� ������� � ������ �� ��������������� ����������� (���������) �������, ���������� ���������� �����, � �������������� ���������� � ��������� ������������</FONT></P></TD>
<TD vAlign=top width="35%" height=31><FONT size=3>
<P>������������ ������������� ����������� �����������, ������ ������������ �����, ���������-��������, ����������� ��������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=79><FONT size=3>
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="19%" height=79><FONT size=3>
<P align=center>3&#8211;4 ������</FONT></P></TD>
<TD vAlign=top width="40%" height=79><FONT size=3>
<P>�������-�������� �������� � ����� (�������-���������)</FONT></P></TD>
<TD vAlign=top width="35%" height=79><FONT size=3>
<P>������������ ��������� ����, ������� ����������, �������� �������, ������������� ������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=48><FONT size=3>
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="19%" height=48><FONT size=3>
<P align=center>3&#8211;4 ������</FONT></P></TD>
<TD vAlign=top width="40%" height=48><FONT size=3>
<P>����������� �������� ����������� ����������� ����������: �������������� ������������</FONT></P></TD>
<TD vAlign=top width="35%" height=48><FONT size=3>
<P>������������ ������� � �������� �����, ������������ � ����������� ������������� ����� �����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=64><FONT size=3>
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="19%" height=64><FONT size=3>
<P align=center>7&#8211;9 ������ </FONT></P></TD>
<TD vAlign=top width="40%" height=64><FONT size=3>
<P>����������� ����������� ���������� � ������ �����: ����� � ������� �������� � ������ �� �������������</FONT></P></TD>
<TD vAlign=top width="35%" height=64><FONT size=3>
<P>�����������, ���������� � ���������� ����������� � ������� ��������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=48><FONT size=3>
<P align=center>5</FONT></P></TD>
<TD vAlign=top width="19%" height=48><FONT size=3>
<P align=center>8&#8211;10 ������</FONT></P></TD>
<TD vAlign=top width="40%" height=48><FONT size=3>
<P>�������������� ������������</FONT></P></TD>
<TD vAlign=top width="35%" height=48><FONT size=3>
<P>������������ � ���������� ��������� ������������� � ������������� ����������� �������� ������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=80><FONT size=3>
<P align=center>6</FONT></P></TD>
<TD vAlign=top width="19%" height=80><FONT size=3>
<P align=center>13&#8211;14 ������</FONT></P></TD>
<TD vAlign=top width="40%" height=80><FONT size=3>
<P>������: ������� � �������</FONT></P></TD>
<TD vAlign=top width="35%" height=80><FONT size=3>
<P>����������� � ������������ ������� � �������� �����, ���������� �� ������ � ����������� ������, ���������� ���-�����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=129><FONT size=3>
<P align=center>7</FONT></P></TD>
<TD vAlign=top width="19%" height=129><FONT size=3>
<P align=center>13&#8211;15 ������</FONT></P></TD>
<TD vAlign=top width="40%" height=129><FONT size=3>
<P>���������� ����� ������ � ������������� ���������� ����������. ������������� ���������� ����� � ����</FONT></P></TD>
<TD vAlign=top width="35%" height=129><FONT size=3>
<P>������������ � ���������� �����������, � ����� ������������� ����������� �������� � ����������� ������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=48><FONT size=3>
<P align=center>8</FONT></P></TD>
<TD vAlign=top width="19%" height=48><FONT size=3>
<P align=center>13&#8211;16 ������</FONT></P></TD>
<TD vAlign=top width="40%" height=48><FONT size=3>
<P>��������� � ������������ ���������� ������������</FONT></P></TD>
<TD vAlign=top width="35%" height=48><FONT size=3>
<P>������������ �����, ������������ � ����������� �������������� ���������� � ���������� �������� �����</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=32><FONT size=3>
<P align=center>&#8470; �/�</FONT></P></TD>
<TD vAlign=center width="19%" height=32><FONT size=3>
<P align=center>����</FONT></P></TD>
<TD vAlign=center width="40%" height=32><FONT size=3>
<P align=center>���� ��������</FONT></P></TD>
<TD vAlign=center width="35%" height=32><FONT size=3>
<P align=center>��������� ����������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=99><FONT size=3>
<P align=center>9</FONT></P></TD>
<TD vAlign=top width="19%" height=99><FONT size=3>
<P align=center>15&#8211;17 ������</FONT></P></TD>
<TD vAlign=top width="40%" height=99><FONT size=3>
<P>����������� ������������ ���������� ��� � �����</FONT></P></TD>
<TD vAlign=top width="35%" height=99><FONT size=3>
<P>������������ � ����������� ���������� �������������, ������� ����������, ���������-������������ �� ������ � ����������� ������ � ������, �� ���������� � ������������������, ������������ �������������� ������ � ���������, �������������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=41><FONT size=3>
<P align=center>10</FONT></P></TD>
<TD vAlign=top width="19%" height=41><FONT size=3>
<P align=center>16&#8211;17 ������</FONT></P></TD>
<TD vAlign=top width="40%" height=41><FONT size=3>
<P>����������� ������������ �������� � �������������� ������</FONT></P></TD>
<TD vAlign=top width="35%" height=41><FONT size=3>
<P>������������ �������� � �������������� ������, �������������� ������ ������, ���������� �� ������������ ���������� ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=128><FONT size=3>
<P align=center>11</FONT></P></TD>
<TD vAlign=top width="19%" height=128><FONT size=3>
<P align=center>17&#8211;18 ������</FONT></P></TD>
<TD vAlign=top width="40%" height=128><FONT size=3>
<P>���������� ������ ������������ ����������� ������ � �������� �������</FONT></P></TD>
<TD vAlign=top width="35%" height=128><FONT size=3>
<P>������������ ��������� ������������� � ��������� ����������� ������, ����-���������, ����������� ������������� �� ���������� ���������� �������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=64><FONT size=3>
<P align=center>12</FONT></P></TD>
<TD vAlign=top width="19%" height=64><FONT size=3>
<P align=center>17&#8211;18 ������</FONT></P></TD>
<TD vAlign=top width="40%" height=64><FONT size=3>
<P>����������� ������ � �������� ���������� ������ ����������� � ������������ �����</FONT></P></TD>
<TD vAlign=top width="35%" height=64><FONT size=3>
<P>������������ ������������� ����������� ������� � ������������, ������������, ����������� ��������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=54><FONT size=3>
<P align=center>13</FONT></P></TD>
<TD vAlign=top width="19%" height=54><FONT size=3>
<P align=center>20&#8211;21 ������</FONT></P></TD>
<TD vAlign=top width="40%" height=54><FONT size=3>
<P>������� � ���������� � 2006&#8211;2007 ��. ��� ������ � ����-<BR>�������</FONT></P></TD>
<TD vAlign=top width="35%" height=54><FONT size=3>
<P>������� ����������, � ����� ����������� ������������� ����� ������, ������������ ����������� ���������� ����� � ��������� � ����������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=48><FONT size=3>
<P align=center>14</FONT></P></TD>
<TD vAlign=top width="19%" height=48><FONT size=3>
<P align=center>20&#8211;22 ������</FONT></P></TD>
<TD vAlign=top width="40%" height=48><FONT size=3>
<P>���������� ������ ���������� ������������: ������ ����������� ��������� ������, �������� ������� ������� �������������� ������������ � ����������� ������� (��������), ���������� ����������� �������</FONT></P></TD>
<TD vAlign=top width="35%" height=48><FONT size=3>
<P>������������ � ����������� ������������� �����, ����� �� ���������� ����������� �������, ����������� ��������, �������� � ����������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=48><FONT size=3>
<P align=center>15</FONT></P></TD>
<TD vAlign=top width="19%" height=48><FONT size=3>
<P align=center>20&#8211;22 ������</FONT></P></TD>
<TD vAlign=top width="40%" height=48><FONT size=3>
<P>��������� �������� � �����</FONT></P></TD>
<TD vAlign=top width="35%" height=48><FONT size=3>
<P>������������ � ���������� ��������� ������������� ������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=64><FONT size=3>
<P align=center>16</FONT></P></TD>
<TD vAlign=top width="19%" height=64><FONT size=3>
<P align=center>20&#8211;23 ������</FONT></P></TD>
<TD vAlign=top width="40%" height=64><FONT size=3>
<P>���������� �������� � ����� � �����. ��������������� ��������� �����������</FONT></P></TD>
<TD vAlign=top width="35%" height=64><FONT size=3>
<P>������������ � ����������� ������������� ����������� �������� � ������, ����������� ���������� ������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=25><FONT size=3>
<P align=center>17</FONT></P></TD>
<TD vAlign=top width="19%" height=25><FONT size=3>
<P align=center>22&#8211;24 ������</FONT></P></TD>
<TD vAlign=top width="40%" height=25><FONT size=3>
<P>������� � ��������: ����������������, ���� ��������� ������, �������������� �������</FONT></P></TD>
<TD vAlign=top width="35%" height=25><FONT size=3>
<P>������������ � ���������� ������������� ������, ������������ ��������� � ��������������� �������������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=33><FONT size=3>
<P align=center>&#8470; �/�</FONT></P></TD>
<TD vAlign=center width="19%" height=33><FONT size=3>
<P align=center>����</FONT></P></TD>
<TD vAlign=center width="40%" height=33><FONT size=3>
<P align=center>���� ��������</FONT></P></TD>
<TD vAlign=center width="35%" height=33><FONT size=3>
<P align=center>��������� ����������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=96><FONT size=3>
<P align=center>18</FONT></P></TD>
<TD vAlign=top width="19%" height=96><FONT size=3>
<P align=center>23&#8211;24 ������</FONT></P></TD>
<TD vAlign=top width="40%" height=96><FONT size=3>
<P>����������������� ��������� � ������������� ������� ������</FONT></P></TD>
<TD vAlign=top width="35%" height=96><FONT size=3>
<P>������������ � ���������� ������� ��������� � ����������������� ��������� ������, � ����� �����������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=96><FONT size=3>
<P align=center>19</FONT></P></TD>
<TD vAlign=top width="19%" height=96><FONT size=3>
<P align=center>27&#8211;30 ������</FONT></P></TD>
<TD vAlign=top width="40%" height=96><FONT size=3>
<P>�������� ������������� ������������ �����</FONT></P></TD>
<TD vAlign=top width="35%" height=96><FONT size=3>
<P>������������ � ���������� ����������� ����� �����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=32><FONT size=3>
<P align=center>20</FONT></P></TD>
<TD vAlign=top width="19%" height=32><FONT size=3>
<P align=center>27&#8211;28 ������</FONT></P></TD>
<TD vAlign=top width="40%" height=32><FONT size=3>
<P>��������������, ���������� ������������ � �������������� ����</FONT></P></TD>
<TD vAlign=top width="35%" height=32><FONT size=3>
<P>���������� � ����������� ������� ����������� ������� � ������������, ����� �������� �����, � ����� ������������ �������������� ������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=64><FONT size=3>
<P align=center>21</FONT></P></TD>
<TD vAlign=top width="19%" height=64><FONT size=3>
<P align=center>1 �������</FONT></P></TD>
<TD vAlign=top width="40%" height=64><FONT size=3>
<P>������������ �������� �� ��������� ������ �� ������</FONT></P></TD>
<TD vAlign=top width="35%" height=64><FONT size=3>
<P>����������� ���������� � ��������������� ������������� �����, ������������� ����������� ��������, ����������� �����</FONT></P></TD></TR></TBODY></TABLE><B><I><FONT face=Wingdings>
<P align=justify>*</FONT> 105187, ������, ��. ������������, 38</P><FONT face=Wingdings>
<P align=justify>(</FONT> (495) 365-01-07</P>
<P align=justify>���� (495) 365-11-07</P>
<P align=justify>e-mail: registration@ibdarb.ru</P>
<P align=justify>www.ibdarb.ru</P></B></I>


</body>
</html>

