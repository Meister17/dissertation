<!--
ID:38245711
TITLE:��������� � ���������� ��������� �����������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:19
ISSUE_DATE:2007-10-15
RECORD_DATE:2007-10-08


-->
<html>
<head>
<title>
38245711-��������� � ���������� ��������� �����������
</title>
</head>
<body >


<P align=justify>���������� ����������� (�����������) ������������ ������, �������� �������� �� ���������� ���������� ��������, ����������, ��� � ������������ � ��������� ������������ ����� �������� ���������� ��� ��������� ��������������� (���������) ����������� ����� � ��������� ���������������� (����������) �������� ���������� ��������� ��������� �����������:</P>
<P align=justify></P>
<TABLE cellSpacing=2 cellPadding=7 width=610 border=1>
<TBODY>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>&#8470;</P>
<P align=center>�/�</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>������������</P>
<P align=center>���������</P>
<P align=center>�����������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>��������-������� �����</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P align=center>����</P>
<P align=center>������<BR>��������</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P align=center>�����, �������, �.�.�. ����������� ������������ (�����������).</P>
<P align=center>���� ������������ ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>���������� ��������� ������������ ����</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>3248</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P>23.05.2007</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>109052, ������, �/� 48;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>61 ����</FONT></P></TD></TR></TBODY></TABLE>
<P align=right></P><I>
<P align=right>"������� ����� ������", 2007, &#8470; 51</P></I>


</body>
</html>

