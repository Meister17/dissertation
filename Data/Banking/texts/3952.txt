<!--
ID:37637913
TITLE:����������� ����������� ��������� ����������� �� �������� ������������������� ��������� ��������* �� ��������� �� 01.04.2007 �.
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:9
ISSUE_DATE:2007-05-15
RECORD_DATE:2007-05-11


-->
<html>
<head>
<title>
37637913-����������� ����������� ��������� ����������� �� �������� ������������������� ��������� ��������* �� ��������� �� 01.04.2007 �.
</title>
</head>
<body >


<TABLE cellSpacing=2 cellPadding=4 width=614 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="26%">&nbsp;</TD>
<TD vAlign=top width="55%" colSpan=4><FONT face="Times New Roman CYR">
<P align=center>���������� ��</FONT></P></TD>
<TD vAlign=top width="12%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR">
<P align=center></P>
<P align=center>&#8470;</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR">
<P align=center>�������� ���������</P>
<P align=center>��������</FONT></P></TD>
<TD vAlign=top width="28%" colSpan=2><FONT face="Times New Roman CYR">
<P align=center>�� 01.01.2007</FONT></P></TD>
<TD vAlign=top width="28%" colSpan=2><FONT face="Times New Roman CYR">
<P align=center>�� 01.04.</FONT>2007</P></TD>
<TD vAlign=top width="12%"><FONT face="Times New Roman CYR">
<P align=center></P>
<P align=center>���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR">
<P align=center>�/�</FONT></P></TD>
<TD vAlign=top width="26%">&nbsp;</TD>
<TD vAlign=top width="14%"><FONT face="Times New Roman CYR">
<P align=center>����������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT face="Times New Roman CYR">
<P align=center>�������� ���</P>
<P align=center>� �����, %</FONT></P></TD>
<TD vAlign=top width="14%"><FONT face="Times New Roman CYR">
<P align=center>����������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT face="Times New Roman CYR">
<P align=center>�������� ���</P>
<P align=center>� �����, %</FONT></P></TD>
<TD vAlign=top width="12%"><FONT face="Times New Roman CYR">
<P align=center>(+/&#8211;)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR">
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR">
<P align=justify>�� 3 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%"><FONT face="Times New Roman CYR">
<P align=right>43</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right>3,6</P></TD>
<TD vAlign=top width="14%"><FONT face="Times New Roman CYR">
<P align=right>41</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right>3,5</P></TD>
<TD vAlign=top width="12%">
<P align=right>&#8211;2</P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR">
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR">
<P align=justify>�� 3 �� 10 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right>87</P></TD>
<TD vAlign=top width="14%">
<P align=right>7,3</P></TD>
<TD vAlign=top width="14%">
<P align=right>81</P></TD>
<TD vAlign=top width="14%">
<P align=right>6,9</P></TD>
<TD vAlign=top width="12%">
<P align=right>&#8211;6</P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR">
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR">
<P align=justify>�� 10 �� 30 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right>168</P></TD>
<TD vAlign=top width="14%">
<P align=right>14,1</P></TD>
<TD vAlign=top width="14%">
<P align=right>148</P></TD>
<TD vAlign=top width="14%">
<P align=right>12,6</P></TD>
<TD vAlign=top width="12%">
<P align=right>&#8211;20</P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR">
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR">
<P align=justify>�� 30 �� 60 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right>182</P></TD>
<TD vAlign=top width="14%">
<P align=right>15,3</P></TD>
<TD vAlign=top width="14%">
<P align=right>174</P></TD>
<TD vAlign=top width="14%">
<P align=right>14,8</P></TD>
<TD vAlign=top width="12%">
<P align=right>&#8211;8</P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR">
<P align=center>5</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR">
<P align=justify>�� 60 �� 150 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right>226</P></TD>
<TD vAlign=top width="14%">
<P align=right>19,0</P></TD>
<TD vAlign=top width="14%">
<P align=right>224</P></TD>
<TD vAlign=top width="14%">
<P align=right>19,0</P></TD>
<TD vAlign=top width="12%">
<P align=right>&#8211;2</P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR">
<P align=center>6</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR">
<P align=justify>�� 150 �� 300 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right>217</P></TD>
<TD vAlign=top width="14%">
<P align=right>18,3</P></TD>
<TD vAlign=top width="14%">
<P align=right>235</P></TD>
<TD vAlign=top width="14%">
<P align=right>19,9</P></TD>
<TD vAlign=top width="12%">
<P align=right>+18</P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR">
<P align=center>7</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR">
<P align=justify>�� 300 ��� ���. � ����</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=right>266</P></TD>
<TD vAlign=top width="14%">
<P align=right>22,4</P></TD>
<TD vAlign=top width="14%">
<P align=right>275</P></TD>
<TD vAlign=top width="14%">
<P align=right>23,3</P></TD>
<TD vAlign=top width="12%">
<P align=right>+9</P></TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="26%"><B><FONT face="Times New Roman CYR">
<P align=justify>����� �� ������</B></FONT></P></TD>
<TD vAlign=top width="14%"><B>
<P align=right>1189</B></P></TD>
<TD vAlign=top width="14%"><B><FONT face="Times New Roman CYR">
<P align=right>100</B></FONT></P></TD>
<TD vAlign=top width="14%"><B>
<P align=right>1178</B></P></TD>
<TD vAlign=top width="14%"><B><FONT face="Times New Roman CYR">
<P align=right>100</B></FONT></P></TD>
<TD vAlign=top width="12%"><B>
<P align=right>&#8211;11</B></P></TD></TR></TBODY></TABLE><FONT face="Times New Roman CYR"><SUP>
<P align=justify><FONT size=2><FONT face="Times New Roman CYR">*</FONT> </FONT></SUP><FONT size=2>�������� �������, �������� �������� �������� �����������, ������� � ����� ��������� ����������� � ������ � ����� ��������������� ����������� ��������� ����������� ����� ����������� ������ � �������������� �������������� ������.</FONT></P></FONT><I><FONT face="Times New Roman CYR">
<P align=right>"������� ����� ������", </FONT>2007<FONT face="Times New Roman CYR">, &#8470; 22</P></I></FONT>


</body>
</html>

