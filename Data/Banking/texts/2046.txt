<!--
ID:35712726
TITLE:&#171;���� ���������&#187; �� ���������� ����������
SOURCE_ID:3106
SOURCE_NAME:���������� ���������
AUTHOR:�. ������
NUMBER:12
ISSUE_DATE:2005-12-31
RECORD_DATE:2005-12-30


-->
<html>
<head>
<title>
35712726-&#171;���� ���������&#187; �� ���������� ����������
</title>
</head>
<body >


<P align=justify><STRONG>����������� ����������� ����� ������� ��������� ������������� ��������������</STRONG></P>
<P align=justify><EM>���������� �������������� �������� ����� ��� �������� ������� ��������� ��������. � ������ ������ � ��������� ����������� �� ���������� ����� ������ �������� �� �������������� ������, &#8212; ������ ������ ������, ����������� ��������� ������������ ������������ ��������������� �� ������� ����� �� ��������� ����������� �����������, ����������� �������� &#171;���������� ���������&#187; � ��� &#171;������.��&#187; 10 ������ 2005 ����. </EM></P>
<P align=justify>��������� �������� �������������� � ���������� �������, � ���� � ��� ����������� ��������������� �������� �������� ����� ��� �������� �������. �� ���� �� �������������� � ������ ��� �� ����������, � ������� ���� ������ ������� ����� (���������, ���������� ���� � �.�.), �������� ������� ��������� ��������. ������� ���������� ���� �� �������� � ���� ��������� �� �����, � ������, �� ����������, ��� �������� ����. <EM></EM></P>
<P class=pad align=justify>� ���� ���� �� ����� ��� ������������. � ������ ������� ��� ������ ����������� ���� �������� �� �� ��������, ������� �������������� � �������������� ���������� ��������������, ���������� ���������. � ������ ������������, �������, �� ������ ������, �������� ������� ����� �������, &#8212; ��� ���������� ��������� ��������, �������� ��������� �������� � �����. ����������� �� ���� �������� ���������, ���� ���������� �������� ������, � �� ���������������� �� ��������� ��������� �����, ���������� �� ���������� ��������, ����� ���� ����������� �������, ��� ������������ �������� �� ��������� ��������. ������� ����� �� ������ �������� ��� ���������� ��������������� � ���������� ��������� ��������� � ����� �� �������� �������� ��� ������������ ��������� �������� ��� ����� (�������� ������������ ���������, � ������� ����������������� ��������� �������� ������������ ��� �������) �������� ������ ���� ��������� &#8212; ������ ����������� �������� � ������������� �������������� �����.</P>
<P class=pad align=justify>� ����� �������� ���� ������������ �����������, �� ������ ������, ����������� ������������ ������, ������� �������� ���������� � ���� ���� ������� �� ������������ ������ �������. ����� ������� �� ����� ��� ����������� �����������, � ������� � �����, ���������� ������ �������, ������� �������� ������ ���������� � �����. ��� �������, ��� ���������� ������� �� ������ � ���������� ������-���������, ����������, ������� � ����� ��������� ��������, ������ ��� �������� ���� �������� � �����-���� ����� &#171;�����������&#187; ����� � ������ ���������-��������� ��������� ����� � ���� ��������� ������������ ������.</P>
<P class=pad align=justify>��� �������� ������������ ����������� ����� � ��� ����������� � ��������� �������, � ���� ������� ��� ������� ��� �������, ��� ������������ ����������� �� ������ ����������� ��������� � �����. ������������ �������� �������� ����������� ��������� �����. ������, ���� ����������� &#171;������&#187;, ������-�� ������������� ��������� �����, ��� ������ �����������, �� �������������� ������, ����������� �����������, ������ ����������� �������, ���-���, ���� ������, ���������� ���������� ����������� �� ������� ��������� ��������. � ���� �� ����� ����������� ��� ��� ������������, ������� ������ ������ �� ������������ ���� � ����������� ����� ������������� �������� � ������.</P>
<P class=pad align=justify>������ �������� �������� ������ ����������� ���������������, �������, �������� &#8212; ����������� ������ ��������������, ��������, ����������� ��������� � �������� ������� ��� ����������. ��������� ������ � �� ������� ����������� �������� ���-���������� �� ������, �� ���������� ������� ����� ����������. ���� ������� � ������������ ������� ����������� �� ������� ����������, ������� ������ ���� �������� �� ��������� �����.</P>
<P class=pad align=justify>� �������, ������ ������ &#8212; ��� �����������, ��������� �� �������� ����������, ����������� ��������� � ������. � �� ������� � ���� ����� ��� �������. ��������, ����������� ���������� ������� ���� ������� � ������������� ����� ������� ����� ������������ ������������ (����������� ���������, ����� � �������� �������� � ������ �������������) ����������� �������������������� �����. � ������ 90-� ����� ���� ����� ��������� ����������� ���������� �������. �� ������ ������� ���� ������, �� ������������ ������������. �� �������, ����� �� ������ ��������� �������, � ��� ������ ������ �������� ���� ���� ������������ ���������� ������������ �����������, �����������. ��� ������ �������������� ������, ����� ���� ������� ������ �� ���������� �����, �� ������������� ��� � �� ���� ���������. � ������ �������� ��������������� ������. � ����� ����� ������ ����� ���������� ������ �� �������������, ����� ���������� �������� � ������� ������ ����� &#8212; ������������ ����������� ����� ������������. ������ �������� ����� ��������� ��� ������������ ������� ������������ ��������, �������� �������� �����. ������� ����� ������ ��������������, ���������� �����, �� ��������� �������� �������� ���������� ������ ��� �������, ������, � ����������� ����, ��� ��������� �� ������ ������� �� ����� ��������, �� ����������. �� �� ������ ������������� ��������� �������� � ������������.</P>
<P class=pad align=justify>��� ����������� ������ � ������� ��������� �������� ������ ������. �� ��� ��������, ��� ���� �������� ��������������������, �� ������ �������� �����, ������� ������� ��� ������� �� ������� � ��������. ���������, ������������ � �����, ������ ���� ��������� �������, � ����� ����� ����� �������� � ���������� ��������� �� ��������� ��������� � ������, ������ ��� ����� ��� ��������� ����������������� � ����������� ���������� �� �������������� �������. � ���������, �������������� ��������� ������������ � ��������� ����������� ������, ����� � �������� ������ ��������� ������� ������. � ������ ��� ��������� ������, ��� ��� �������, ������ ����������� ��� &#171;�����-�����&#187; �� ��������� ����� �������.</P>
<P class=pad align=justify>��� �������� ������������� ������ ����������, � ��� ���� ������� ������������, ��� �������������� ������ ���� ����������� ������������������. �������� ��� ����������� ���������� � ��������������, ��� ����� ��������� � ������ � �������������� ������ �� ����������, ��� ������� �������� � ��������� ��������������, ��������� ���� � ���������� ����������� ����, ���� � ������������� ����, ��� � ���� ����� �������, ��� ������ ������� �� ��������������. ������� �� ������� ����������� ���������� ������ ����������, � ������� ������ ���� ����� �������� �������� �������������� �� ������� ������, ��������, ���������� ������������ �������.</P>
<P class=pad align=right><STRONG>�� ����������� �. ������ �� ������� �����<BR>&#171;����� � ��������� ��������: ����������� � ������&#187;<BR>10 ������ 2005 ����.</STRONG></P>


</body>
</html>

