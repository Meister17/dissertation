<!--
ID:35366973
TITLE:Central reservations
SOURCE_ID:3240
SOURCE_NAME:Banker
AUTHOR:
NUMBER:952
ISSUE_DATE:2005-06-30
RECORD_DATE:2005-10-04


RBR_ID:2043
RBR_NAME:������������� ���������� ���������
RBR_ID:3401
RBR_NAME:��������������� ����
RBR_ID:4935
RBR_NAME:������ � ����������� ������������ �� ��
RBR_ID:4937
RBR_NAME:�������-��������� ��������
-->
<html>
<head>
<title>
35366973-Central reservations
</title>
</head>
<body >


<P align=justify><FONT face="Times New Roman" size=3><B>During the Russian Economic Forum, in April, deputy head of the Central Bank of Russia, Alexei Ulyukayev, who is in charge of Russia's monetary policy, spoke to The Banker about the bank's strategy</B></FONT></P>
<P align=justify><STRONG>W<FONT face="Times New Roman" size=3>ill the GBR target inflation as well as the exchange rate this year?</FONT></STRONG></P>
<P align=justify><FONT face="Times New Roman" size=3>The CBR has ne</FONT><FONT face="Times New Roman" size=3>ver publicly said it would target the exchange rates. In the mid-term we can only influence the nominal exchange rates, but in fact the real rate depends on the functioning of the economy. Now we are making it a bit more concrete and clear that the only target is inflation. We include an estimate of possible rouble appreciation in our monetary policy documents, but it is only an opinion and not an obligation.</FONT></P>
<P align=justify><FONT face="Times New Roman" size=3><B>The rouble is appreciating</B></FONT> <STRONG>qui<FONT face="Times New Roman" size=3>te strongly and some say this is harming the domestic producers.</FONT></STRONG></P>
<P align=justify><FONT face="Times New Roman" size=3>We have no real evidence that is the case. Some companies have made statements but there is no firm evidence. We have different kinds of currency baskets. When we are talking about the real appreciation basket, we use the shares of the country in our foreign trade. When we are talking about our operations on the currency market then this consists of only two currencies: dollar and euro - now 80% dollars and 20% euros - but we are thinking about increasing the proportion of euros.</FONT></P>
<P align=justify><FONT face="Times New Roman" size=3><B>For the first time in several yea</B></FONT><FONT face="Times New Roman" size=3><B>rs, Russia's economy will almost certainly miss the government's 8.5% inflation target. What can the CBR do in the fight to hold inflation back?</B></FONT></P>
<P align=justify><FONT face="Times New Roman" size=3>We haven't officially changed our target rate of 8.5%, although we do understand that it is quite an optimistic target. The government has changed its target to between 8.5% and 10%. I think that we will do the same in the next few weeks, but we are still only talking about a general index. When we take into account core inflation then this year's 2.4% is the</FONT> <FONT face="Times New Roman" size=3>same as last year's. There is no acceleration of core inflation.</FONT></P>
<P align=justify><FONT face="Times New Roman" size=3><B>A key factor on monetary poliicy will be the cut-off price of oil: the price after which excess tax revenues from oil are transferred not to the budget, but to the stabilisation fund. The cut-off price has a big impact on liquidity in the financial sector and indirectly affects inflation. It will almost certainly be hiked from the current level of $20 and a price of $25 has been suggested. Is this the most suitable price?</B></FONT></P>
<P align=justify><FONT face="Times New Roman" size=3>I can't say that $25 is the best price for the cut-off, but it is better than $27 [which has also been suggested by some in the government] and $24 is better than $25. We agreed that we would introduce the stabilisation fund at $20 cut-off. Why $20? It was the average oil price for the previous 10 years in 2001. Now the oil price is higher and the ten-year average is up to $22.50. That would be reasonable [for the cut-off price]. The price is important because it affects our policy. If we have more than 125, it means that we have to sterilise money. If it is less than $25 then we have to give </FONT><FONT face="Times New Roman" size=3>credits to the banking system because of the lack of liquidity.</FONT></P>
<P align=justify><FONT face="Times New Roman" size=3><B>What is the GBR's position on using the huge hard currency reserves for the early repayment of Russia's sovereign debt, and the Paris Club debt in particular?</B></FONT></P>
<P align=justify><FONT face="Times New Roman" size=3>The Paris Club debt is a political debt: the rates are quite high and it is not tradable, so you cannot manage it. We are very dependant on the political will of other countries. For example, Germany securitised is our debt [the so-called Aries bonds], and they didn't even ask us about it. When I was working for the ministry of finance, I proposed we could change the structure of our debt and even increase the market for it, because our borrowing is a benchmark for corporations.</FONT></P>
<P align=justify><FONT face="Times New Roman" size=3><B>Russia enjoyed a net capital inflow for the first time ever in 2003, but last year capital flight returned due to the Yukos affair and the CBR's exchange rate policy, that slowed rouble appreciation. Are these tidal capital flows a problem?</B></FONT></P>
<P align=justify><FONT face="Times New Roman" size=3>The conditions are not so good now. We have the behaviour of domestic corporations and the reaction of the foreign investors to our exchange rate policy [to cope with]. Over the fourth quarter [of 2004] capital flows were $9bn, whereas over the first three quarters it was minus $18bn. It goes up and down depending on the exchange rate policy because it is possible to raise cheap money abroad, to invest in rouble assets or deposits at the central bank and to fix profits that way.</FONT></P>
<P align=justify><FONT face="Times New Roman" size=3>We are very worried about 'hot money' as it is influencing the liquidity and stability of the banking system. That is the reason we still have some currency regulations. However, the plan is to make the rouble fully convertible by January 1, 2007.</FONT></P>


</body>
</html>

