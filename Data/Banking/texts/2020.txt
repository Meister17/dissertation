<!--
ID:35578903
TITLE:����������� ���������
SOURCE_ID:3106
SOURCE_NAME:���������� ���������
AUTHOR:������ ��������� 
NUMBER:10
ISSUE_DATE:2005-10-31
RECORD_DATE:2005-11-25


RBR_ID:2252
RBR_NAME:���������� �������
-->
<html>
<head>
<title>
35578903-����������� ���������
</title>
</head>
<body >


<DIV class=subhead><STRONG>������������� ����� ������� ���������� ������� ������</STRONG></DIV>
<DIV class=subhead>&nbsp;</DIV>
<DIV class=line>
<DIV class=line2>������, ������� ���������� ����������� ������� ������� ��� ���������� � ����������, �� ����� �������� ��� ��� ������� ��������, �� ���� �� � ����� ��������� � �������� � ���������� ��������, ������������� �� ������� ������. ��� ����������� ������� � ����� ����� ������ �������� ����� � �������, ����� ���� � ����� �������. ������ ������������ �������������� ������ �������� ������ �� ������ ����� ������ ��������, �� � ��� ������� �������������� ���������. </DIV></DIV>
<DIV class=pad>
<H5><FONT size=3>����� �� �������</FONT></H5>�� ��������� ������ ������ �� ����. � ����� ���� �������������� �������� "�����" ������ ��� �������� ���� ���� �������� �������������� ������� �� ���������. ��� �����, �� ��������� ���������� ������ ������� �� "�����" ������ ������, ������ � ������ �����������, ���������� �� ���������� ������������� ������������� ������ ��������. 
<P align=justify>����� ���� �������, ��� � ��������, ������ ���������� ������ ���������, ���� ����� �� ��������� �� ����� �������� ���������. ������� ��� ������ ����� � ��������� ��� � "�������", ���� ����� ������ �� ������ ������� ��� �� ������, ���������� � ��������� ����������. ����� �� ��������� - ��� ��������� ��������� ������ 90-� ����� ����� ������ � ��������� ���������, ���, ��������, ������� �������. � �������, ���� �� ����� "������", ����-���������� ����� �� ��������� �� ��������� ������� ���� ����� �������� �� ������� ����, ��������� ��� �� ���������� ������ � ��������� ����� ����� ���������� �������. ���, ������� �. �����, ����� ������ �� ���� ����� ���� ��������. �� ���� ���-�� �������� ����� �������� ���� �������� �� ����� � � ����� - ��� ������ ��������. ������ ���� � ����� �������� ������ ��������. 
<P align=justify>�������� "������" ��� ���� �������� � ����������. ������������ ����� ���������� �� ����� ��������� �����, �� � ����� ������ �� ������ �������� ��� ���� ������� �������. ������ ������ � ��������� ������� ���������� � ����� �����, � ����� �����. ����� ���������� ����� � ������ ������������ ���������: ������, �����, ����������� ����� � �.�. ����� ����, ����� �������� ����������� ������ ������� ���������� � ��������� �� ������� �������, ���, ��������� "������ ������". ��� ��� ����� �� ����� ������������ � ������. 
<P align=justify>���� �� ������� ���������� ����� � �����������, ��������� � ������ "������", ���������� ������ ������ ������������ ������� � ����� �������������. ������������ ��������� ��������� ������ ������������ ����� ���� ����� ������ ������ ������. �������������� �������� � ��������� �������� ���������� ��������� � ���������� �������������� ��� ����� ���������� "���" - ����� ��������, ������ ���, �� ����� �������. � ���� ����� ������ �����, ��������� ������� ���������, ��� �������� - ��������� �������. ��� �� "������� 1", ��� ������� �����, �������� ����� ����� ������� ������������ � �������������� ������������� �������� ���������� �������! � ����� �����������, �������� �� ���� �����, �������������, ������� - �� ������, � �������. "������ ������ � ������� ��� ������� - �������, �������, - ������� ����� �����, - �� ��� ������ ������, ���� ���� ������ ���� � ������� � ������". 
<P align=justify>�����, �������� ���, ��� ������� "�����", ��������� �������� � ����� ����� 15-20 ���. ��������. ������� � ������� �������� ���� ����� ������ ����� ��������� ������� ��� � ���. � ��� ��������� ����� ���������� ����� ������ ������ ����������� �����������: �������� � ������������ ����� (������ �� ����������� ��������� ����� � ������ "���", ����� � �� ����������� ��������� "����������"), � ����� ������ �����, ������������� � ������������ - �������� ������, ����������� ������, ����� � �.�. 
<P align=justify>
<H5><FONT size=3>������� ������� ��������</FONT></H5>� ����������� "����-��������" � ���������� ���������� ������ ����� ����� ("�����") ������ ���: "��������, ������ � ��������� ���� ���������� � ��������� ��� ������ � ����� �� ����� ��� �������. �� � ���� ������� �� ������". � ��� ��������� ��������. ��������� ������ � ��������� � ������������ ������ ��� ������ ������� �� ������. �� ��� ������� ���������� ������������� ������������ ���������� ����� ����� � ����� ��������. � ������� �� �������� �������� ���������� ������� ���������� ���� ������. �������� ������, ��� ��������� � ����� ������ ����������� ����� � ����� ��������� �����������. ������� �������, �������, ������� � ���, ��� ��������� ��������� ������� ���������� ����� ������������� ������ �� ������������� ������� ��������. 
<P align=justify>� ��������� ��������� "������" ����� ��� � ��������� ������������� �������� ���������� ����� � ��������� � �������� ������� �����������. �� ���� ����� ����� ������ �����, ������������ �������� � ��������� ��� ���� ���������� ������� � ������ ����������� � ����� ���������� ��������� �� ������� � �������, ������������, �������� � �������� ���������� � ����������� �������� � �������. 
<P align=justify>���� � ��������-������������ ����������� ���������� �����������, ������������ ���������, � ������� - ��������������� ������, �� ��� ����� ������� ������� � ������� ������ ��� ������ ����� �� ������������ �������. �� �� ����� ������������ � ������������� ��� ���������� ������ ����� ����� ����� � � ������. 
<P align=justify>"������, �� ���� ����� "���������", �������� ������� �������� ������ ��� ��� ����������� �������, ��� � ��� ������������� �������, - ������� ��������� ������, �������-�������� �������� "��-�����". - �������� ���� - �������� �������������� ��������� "����-��������". � �� ���� � ���� ���������� ������, ������� ������ ����� ���������� ������ ��������". 
<P align=justify>�� ������ �. �������, ����� ������� ��� ����������������� �������� ����� ������������ � ���������... � ������� ������ ����� �� ������������� ������� �� ����� ��������, � �����, ����� ������ ����� �� ������, ���, ������, � ��������� ������ - ��� ������ �������. ������, ���� ������������������ ������������� �������� �������� �����, �� ��� ������ ����������� ����� �������� � ��������, ���������� � ����� �� �� ������, ����� � �������� ������������ ����� ������� ���� ����������. 
<P align=justify>��� �� "��-�����" ��������� ����������� ����������� �������� � ������� ����� ������� ��������. ����� ���������, ������� ����, ������� � ������������� ���� � ���� � ����������� ���� (��� ����������� � ��������� �������������) ���� ��������� ����� � ������� � ��������� ������ - � �� � ������, � � ������������� ��������. 
<P align=justify></P></DIV>


</body>
</html>

