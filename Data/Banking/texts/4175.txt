<!--
ID:38247957
TITLE:�������� &#8470; 3 ��������� ������ ���������� ���������� ������ �� 18 �������� 2007 �.
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:19
ISSUE_DATE:2007-10-15
RECORD_DATE:2007-10-09


-->
<html>
<head>
<title>
38247957-�������� &#8470; 3 ��������� ������ ���������� ���������� ������ �� 18 �������� 2007 �.
</title>
</head>
<body >


<B>
<P align=center>���������� ���������� ������</P><FONT size=4>
<P align=center>��������</P></FONT>
<P align=justify>18 �������� 2007 �. &#8470; 3</P>
<P align=center>������</P></B>
<P align=justify>��������� ������ ����������</P>
<P align=justify>���������� ������</P><I>
<P align=justify>������������ &#8211; �.�.�������</P>
<P align=justify>��������� &#8211; �.�.���������</P>
<P align=justify>��������������:</P>
<P align=justify>����� ������</I>: �.�.�������, �.�.�����, �.�.�������, �.�.�����, �.�.��������, �.�.��������, �.�.�������, �.�.�������, �.�.���������, �.�.�������, �.�.������, �.�.��������, �.�.������, �.�.��������, �.�.��������, �.�.�����, �.�.����������, �.�.�������, �.�.�������, �.�.��������, �.�.�������;</P><I>
<P align=justify>������������� ������ ������ �� ������������</I>: �.�.�������, �.�.��������, �.�.���������, �.�.������, �.�.������, �.�.�����, �.�.�����, �.�.��������, �.�.�������, �.�.����������;</P><I>
<P align=justify>���� ������������ �� ������������� �� �� ��������� ������</I>: �.�.��������, �.�.��������, �.�.�������, �.�.�����������, �.�.��������, �.�.����������, �.�.��-�����, �.�.���������, �.�.��������, �.�.����������, �.�.�����, �.�.��������, �.�.�������, �.�.�������, �.�.�����, �.�.��������, �.�.����������, �.�.����, �.�.�������, �.�.�������.</P>
<P align=justify>������ �������.</P><I>
<P align=justify>������������</I>: �.�.�������, ������������ ��������� ����� "������������"; �.�.�����������, ��������� "���������������"; �.�.�������, ����������� ������������ ��������� ��������� ������; �.�.��������, ����������� ������������ ��������� ����������� ����� ��������; �.�.�������, ������������ ������ ���������� "�����-<BR>������ ����"; �.�.��������, ����������� ��������� ������������ ����������� ������������� � ������� �� ��; �.�.��������, ����������� �������� ������������� ����������� �������; �.�.��������, ������������ ������� "������������ ���������� ������"; �.�.���������, �������� �� �������� "������������� ���� ��������� �������"; �������������� ����-���������� ���: �.�.����������, �.�.������, �.�.��������, �.�.������, �.�.�������, �.�.������.</P><B>
<P align=center>�������� ���:</P></B>
<P align=justify>1. � ���� ���������� ��������� ���������� ���������� ��������� � ������� XVIII ������ ��� �� �������� ���������� �������.</P><I>
<P align=justify>����: ��������� ��� �.�.�������, �������������� ����-��������� ��� �.�.�������.</P></I>
<P align=justify>2. �� ������ 1-�� ����������� ��������� � ���������� � ���������� ���������� ���� � ������ 12&#8211;14 ������� 2007 �.</P><I>
<P align=justify>����.: ��������� ��� �.�.�������, �������������� ����-��������� ��� �.�.������, ����������� �������� ��� �.�.��������, ������������ ������� "������������ ���������� ������" �.�.��������.</P></I>
<P align=justify>3. � ������� ��� �� ����������� ���������� ������������.</P><I>
<P align=justify>����.: ��������� ��� �.�.�������, �������������� ����-��������� ��� �.�.������, ������������ ��������� ����� "������������" �.�.�������.</P></I>
<P align=justify>4. ����������� ����� �������� ������� ��������� ��������� ����������� ����.</P><I>
<P align=justify>����.: �������������� ����-��������� ��� �.�.������.</P></I>
<P align=justify>5. � ���� ���������� �������� �� �������� �������������� ����������� �����.</P><I>
<P align=justify>����.: ����������� �������� ��� �.�.��������, �������� ���� �� �������� �.�.���������.</P></I>
<P align=justify>6. � ��������� ���������� ���������� ������.</P><I>
<P align=justify>����.: �������������� ����-��������� ��� �.�.��������.</P></I>
<P align=justify>7. �� ���������� � ������� ������ ���.</P><I>
<P align=justify>����.: ��������� ��� �.�.�������.</P></I>
<P align=justify>8. ����� � ���������� ������ ���.</P><I>
<P align=justify>����.: �������������� ����-��������� ��� �.�.����������.</P></I>
<P align=justify>9. ������.</P><B>
<P align=justify>I. �������</B>: � ���� ���������� ��������� ���������� ����������</P>
<P align=justify>��������� � ������� XVIII ������ ��� �� ��������</P>
<P align=justify>���������� �������.</P><B>
<P align=justify>���������:</B> �.�.�������, �.�.�������, �.�.�������, �.�.��������,</P>
<P align=justify>�.�.�������, �.�.��������, �.�.�������, �.�.��������,</P>
<P align=justify>�.�.����������, �.�.���������, �.�.�������, �.�.�����,</P>
<P align=justify>�.�.�����, �.�.������, �.�.��������.</P><B>
<P align=justify>�����������:</P></B>
<P align=justify>1. �������� ������������ ��� �� ���������� ��������� ���������� �� � ������� XVIII ������ ��� �� �������� ���������� �������.</P>
<P align=justify>���������� ���������� ������ ��� � ������������ ������� ������ � ���������� ����������� �� ���������� ��������� ���������� �� � ���������� ���������� ����� ���������.</P>
<P align=justify>������������� ������������� ������������ ���������� ����������� �������������� �������������� � �������� �������� ������ �� �������� ���������� ���������� ����� ���������, ��������� ��� ����� ���������, ����������� � ��� �� �������� ������.</P>
<P align=justify>2. ������� � �������� ���������� ������ ������ ��� � ������� ��������� �� ���������� ����� ������.</P>
<P align=justify>3. �������� ������������ ��� �� ���������� � ���������� VIII �������������� ����������� ������ � ������ ���������.</P>
<P align=justify>������� �������������� �������� IX ������������� ����� �� ��������� �������� ������������ ������ � ������� 2008 �.</P>
<P align=justify>4. �������� ������ ���������������� �������� ��� �� ���������� ���������� �������� ���������� ������������.</P>
<P align=justify>����������� � ������������ ���������������� �������� �� ��������� �������� ��������� ��������� "�������� �������� ���������� ������������. �������� ��������� � ����������".</P>
<P align=justify>������������� ���������������� �������� �������������� ������ �� ������������ ��������� ������������� ���������� � ��������� ������.</P>
<P align=justify>����������: �� &#8211; �����������.</P><B>
<P align=justify>II. �������:</B> �� ������ 1-�� ����������� ��������� � � ����������</P>
<P align=justify>� ���������� ���������� ���� � ������ 12&#8211;14 �������</P>
<P align=justify>2007 �.</P><B>
<P align=justify>���������:</B> �.�.�������, �.�.������, �.�.�������, �.�.��������,</P>
<P align=justify>�.�.��������.</P><B>
<P align=justify>�����������:</P></B>
<P align=justify>1. ������� � �������� ���������� �� ������ 1-�� ����������� ���������.</P>
<P align=justify>�������� �������������� ��������� ���������� ����������� ���������.</P>
<P align=justify>2. ������ ������ ��� ������� �������� ������������ ������� � ���������� � ���������� ���������� ���� � ������.</P>
<P align=justify>����������: �� &#8211; �����������.</P><B>
<P align=justify>III. �������:</B> � ������� ��� �� ����������� ���������� ������������.</P><B>
<P align=justify>���������:</B> �.�.�������, �.�.�������, �.�.������, �.�.�����, �.�.������,</P>
<P align=justify>�.�.�������.</P><B>
<P align=justify>�����������:</P></B>
<P align=justify>1. �������� �������������� ���������� ��� ����������� �� ������� ������� ������������ � ���������� �����.</P>
<P align=justify>2. ���������� ������ �� ��������� ��������� ����������� ������������ � ���������� ����� � ������ �������������� � ��������������� �����, ������� ��������� � ������������������� ��������.</P>
<P align=justify>����������: �� &#8211; �����������.</P><B>
<P align=justify>IV. �������:</B> ����������� ����� �������� �������</P>
<P align=justify>��������� ��������� ����������� ����.</P><B>
<P align=justify>���������:</B> �.�.������, �.�.��������, �.�.�������, �.�.���������,</P>
<P align=justify>�.�.�������.</P><B>
<P align=justify>�����������:</P></B>
<P align=justify>1. ���������� �������������� ������ ����� �������� ������� ��������� ��������� ����������� ���� � ������ ����������� ��������� � �����������.</P>
<P align=justify>2. �������� �������� ��������� ���������� ����� �������� ������� � ������������ ������������� ����������� ������� � ������ ���.</P>
<P align=justify>����������: �� &#8211; �����������.</P><B>
<P align=justify>V. �������:</B> � ���� ���������� �������� �� �������� ��������������</P>
<P align=justify>����������� �����.</P><B>
<P align=justify>���������:</B> �.�.���������, �.�.��������, �.�.�������.</P><B>
<P align=justify>�����������:</P></B>
<P align=justify>1. ������� � �������� ���������� � ������������ ������������� ����������� �������, ������������� ���� ��������� �������, ������������� ���������� ���������.</P>
<P align=justify>2. ������������� ������ ������ ��� �������� ��������� ����������� ��������� � �������������, ������������ ����������� ���������� ������.</P>
<P align=justify>3. ��������� ��� ���������� ������ �� �������� �������������� ����������� �����.</P>
<P align=justify>����������: �� &#8211; �����������.</P><B>
<P align=justify>VI.</B> <B>�������:</B> � ��������� ���������� ���������� ������.</P><B>
<P align=justify>���������:</B> �.�.��������, �.�.��������, �.�.������, �.�.�������,</P>
<P align=justify>�.�.�������.</P><B>
<P align=justify>�����������:</P></B>
<P align=justify>1.<B> </B>������� ������� ��� �� �������� ���������-���������� ��������� ��������� �������� � ������ �������������������.</P>
<P align=justify>2. ��������� ������������� �������� �������� �������� ����������, ����������� ������������ ��������� ����������� ����� ��������.</P>
<P align=justify>3. ��������� ������������� �������� ��� �� ������������� ������������ �������� ����� �����������, ������������ ������ ���������� "�����������-����".</P>
<P align=justify>���������� �� &#8211; �����������.</P><B>
<P align=justify>VII. �������</B>: �� ���������� � ������� ������ ���.</P><B>
<P align=justify>���������:</B> �.�.�������.</P><B>
<P align=justify>�����������:</P></B>
<P align=justify>1. ������� �� ������� ������ ��� �.�.���������, �.�.������������, �.�.�������, �.�.��������, �.�.�����������.</P>
<P align=justify>2. ������ �� ���������� � ������� ������ ��� ������� �� ����������� ������-���� ������ ���.</P>
<P align=justify>����������: �� &#8211; �����������.</P><B>
<P align=justify>VIII. �������</B>: ����� � ���������� ������ ���.</P><B>
<P align=justify>���������</B>: �.�.�������, �.�.����������, �.�.�������, �.�.���������,</P>
<P align=justify>�.�.�������, �.�.������.</P><B>
<P align=justify>�����������:</P></B>
<P align=justify>1. ������� � ���������� ���������� ������:</P>
<P align=justify>1. ���� "��������" (������);</P>
<P align=justify>2. �����-������-���� (������);</P>
<P align=justify>3. ��������������� (�����-�������, ���������� �����);</P>
<P align=justify>4. ���� ����� (������);</P>
<P align=justify>5. ������� (������);</P>
<P align=justify>6. ���� "��������" (������);</P>
<P align=justify>7. �������� (�����);</P>
<P align=justify>8. ��������� ������������ ���� (������, ���������� ���.);</P>
<P align=justify>9. ������ ������ ���� (������);</P>
<P align=justify>10. ��������� ������������� ���� (���������);</P>
<P align=justify>11. ���������� (������, ���������� ������);</P>
<P align=justify>12. �� ���� (������);</P>
<P align=justify>13. ������������ ���� ������������ ���������� (������);</P>
<P align=justify>14. ������������ ���� �������� (������);</P>
<P align=justify>15. ���� "������" (�����-���������);</P>
<P align=justify>16. ���-���� (��������);</P>
<P align=justify>17. ���� "�������" (������);</P>
<P align=justify>18. ����������� ���� ��������� � �������� (������);</P>
<P align=justify>19. � �� �� ���� (������);</P>
<P align=justify>20. ����������������� ����� "������ ����" (�������) (������);</P>
<P align=justify>21. ��� "��������������" (������);</P>
<P align=justify>22. ��� "�����-�����" (������);</P>
<P align=justify>23. ��� "��� &#8211; ������������� ���������" (������);</P>
<P align=justify>24. ������������� ��������������-�������� ����� �� ���������������;</P>
<P align=justify>25. ��� "������������� �������� � ������������ ������" (������);</P>
<P align=justify>26. ��� "����� ������ ������" (�����-���������);</P>
<P align=justify>27. ���������� �������� ��������� (������);</P>
<P align=justify>28. �� "����������� ����������� �������� ���������" (������);</P>
<P align=justify>29. ��� "�����" (������);</P>
<P align=justify>30. ��� "��.�" Legal Service, RUBURU (������);</P>
<P align=justify>31. ��������� ���������� ���������� ������� (��������, ���������� ���.);</P>
<P align=justify>32. ��� "����� �������" (������);</P>
<P align=justify>33. ����������������� �������� "������ �������� ���������� ����" (��������);</P>
<P align=justify>34. ��� "��������������� "��������� ����" (������);</P>
<P align=justify>35. ���������������� ����������� ���������� ���������� ��������� ������ (������).</P>
<P align=justify>36. ��� "������" (������).</P>
<P align=justify>2. ��������� �� ������ ��� � ����� � ������� ��� �������������� �������� �� ������������� ���������� ��������:</P>
<P align=justify>1. ������������ (�����������, ������������ ���.);</P>
<P align=justify>2. �����-��� ���� (���������);</P>
<P align=justify>3. �������-���� (������);</P>
<P align=justify>4. ���-���� (������);</P>
<P align=justify>5. ������������� (������);</P>
<P align=justify>6. ����-���� (������);</P>
<P align=justify>7.���� "�������" (���������).</P>
<P align=justify>3. ��������� �� ������ ��� � ����� � �������������� ��� �����������:</P>
<P align=justify>1. ���������� ����������������� ����� "��� ����� ��� �.�.�.";</P>
<P align=justify>2. ���������� ����������������� ������������ �������� ������ �� (SEB);</P>
<P align=justify>3. ����� ���������� ����� � ����������� (�������);</P>
<P align=justify>4. ��� "������������" (�. ������).</P>
<P align=justify>4. ��������� �� ������ ��� ��� "�����" (������) �� ������� ����������� �����������.</P>
<P align=justify>����������:</P>
<P align=justify>�� &#8211; 50</P>
<P align=justify>������ &#8211; 1</P>
<P align=justify>����������� &#8211; 1</P>
<P align=justify>�� ������ �������� ��� "������" ���� ��������� ���������� �� ��������:</P>
<P align=justify>1. � ���������� ��� � "������������ ������ �� ����������� ���������� �������� �������".</P>
<P align=justify>2. � �������� ������� �����������.</P>
<P align=justify>3. � ���������� �������� �� ���������� ���������� ���������� � ������� �������.</P>
<P align=justify>������������<B> �.�.�������</P></B>
<P align=justify>���������<B> �.�.���������</P></B>


</body>
</html>

