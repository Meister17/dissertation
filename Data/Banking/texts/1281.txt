<!--
ID:35410949
TITLE:����� ����������� ������� � ��� 2005 ����
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:
NUMBER:7
ISSUE_DATE:2005-07-31
RECORD_DATE:2005-10-14


RBR_ID:2039
RBR_NAME:���������� �������
-->
<html>
<head>
<title>
35410949-����� ����������� ������� � ��� 2005 ����
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgwhite>
<TD>
<P class=Main align=justify><EM>�� 1 ���� � ���������� ������� ����������� 662 ��������� �����������, � ��� ����� 647 � ������ � 15 &#8212; � ���������� ������� (�� 1 ��� &#8212; 664 �����: 649 � ������� � 15 � �������).</EM> 
<P class=Main align=justify>� ���<STRONG> ���������� ������</STRONG> ������ �������<SUP>1</SUP> ������� �� 2,8% (��� �� 116,4 ����. ������) � ��������� � ������ ���� 4 287,7 ����. ������. �� ���� ����� ����� ������� � ����� ���������� �������� ��� �������� ������� ��������� ����������� (� 64,4 �� 64,1%), ��� � �������� ������� � ������ �������� ��� � ����������� ������. ��������� ����� ��������� �������� ���� �������� � ������������ ������ (� 65 �� 64,9%) &#8212; ��������������� � ���������� ������������� ����� �������� �� ������ ����������� � ����������� � ����������� ������. 
<P class=Main align=justify>� ��������� <STRONG>��������</STRONG> ������� ���� ������������ � ����������� ���������� ����������� ���������� ������� &#8212; ��� ����������� �� ������������ �������� ���� �������� �� ��������� ������. 
<P class=Main align=justify><STRONG>����������� ������� </STRONG>������ � ��� ����������� �� 1,4%, �������� �� 1 ���� 676,7 ����. ������. <STRONG>����� ���������� ���������</STRONG> ������������ ������ (� ������ ������� ���) �������� �� 1,2% � �������� 138,1 ����. ������ &#8212; � ���������� ������������� ������� ������� ��� ����� ��������� ����������� �������. ��� ���� ���������� ��������� ��������� ���� ���������� � ��� �� 25,4%, ��������� 44,3 ����. ������. <STRONG>�������� ������� � �����</STRONG> ������ �� ��� ��������� ������� (�� 2%) � � ������ ���� ��������� 538,6 ����. ������. 
<P class=Main align=justify>����<STRONG> �������� �� ��������� ������</STRONG> (�� 2,1%, ��� �� 3,7 ����. ������ &#8212; �� 182,1 ����. ������) ��������� ������� ������� ���������� ���������� �������� �� ��������� ������ �� ������, �� ���� ������� ���������� 86% ���� ��������. 
<P class=Main align=justify>����� �������� <STRONG>������������ </STRONG>������ ������� �������� � ��� �� 3,1%, ��������� 3 428,8 ����. ������ (80% ��������). �������� ������� �� ���� ������������ ������� ���������� �������� �� ������ �������� ������. 
<P class=Main align=justify>����<STRONG> �������� ������� �� ��������� � ������� ������ ����������� </STRONG>����� ������������� ����������� �������� � ��� ����������� �������� ����� ���������� �������� (13,1 ������ 2,8%). ������ ��������� ����������� ��� �� ��������, ��� � �� �������� ������, ������ ���� ��������� ��� ����� ��������. ���������� �������� �������� ������� �������� �� ���������, �������, ��������� � ������ ������ � ������ ����������� �� 12,5%, �� 920,9 ����. ������, � �� ���������� � �������� ������ � 1 ���� �������� 21,5% (19,6% �� ��������� �� 1 ���). 
<P class=Main align=justify>����������� ����������� � ��� ����� ���������� ���������� ����. ����� ��������<STRONG> ���������</STRONG> ����������� �� 1,6%. � ���� ������ ������� ��������� ����� ������� � ���������� ������ ���������� ���, ��� ���������������� ������� � �������� �������� (������ ���������� �������). � ���������� ����� ������� ��������� � ��������� ������������ ����������� ������� � ����� �� ����� ����������� �� 1,1%, �� 555,3 ����. ������. ������� ��������, ��� �������� �������� ������� ����������� ��� �� ��������, ��� � �� �������� ������. � ���������� ����������� ��������� ���������� ��� � ������������ � ����������� ������ �� 1 ����, �� ��������� � ����������� �� ������ ���, �� ���������� (55 � 45% ��������������). ��������������� ��������� ����������������� �������� ����������� ���, ��� ������ ���������� ����� �� 4,3%, ��� �� 23,9 ����. ������ &#8212; �� 587 ����. ������. 
<P class=Main align=justify>� �������� ���������� ����������� ����������� ����� � ����������������� ���������� ������������� ��������� ������� �� �������� ����� ���������� <STRONG>����� ���</STRONG> ��� ������ ����������� ������� ���������. ��������, ����� ������������ �� ������������ ������������� �������� (� ������ �������� ����� ������) � ����� ���������� �� 1,5% � �������� 652,7 ����. ������, �� ���� � �������� ��������� � 15,9 �� 15,2%. �������� ��������� � ���� ������ ������� ��� �� �������� ��������, ������������ �� ���������� ������������� �����, ��� � �� ��������, ���������� �� ������������������ � ��������. 
<P class=Main align=justify><STRONG>������� ������� �� ��������� ������, </STRONG>�������� �������&shy;����������������, � ��� ������� �� 5,1% � � ������ ���� �������� 99,1 ����. ������. ���������� ��������� ������������� ���������� ����� ������� �� ����������������� ������ ���������� ������. 
<P class=Main align=justify>�������� � ��� ����������� ���������� ��������� ���������� �������, �������������� ������� ����� ������� ������ ����� (���������� �� 1,5%, �� 469,8 ����. ������). �� ���� � �������� �������� �������� ������� �� ���� 2005 ��� ������ &#8212; 11%. �� ����������� �������� ������������ ������� ������� �������� ���� ���������� �������� (�� 2,2%). 
<P class=Main align=justify>� ����� <STRONG>�������� </STRONG>�������� ������������ ��� ������ ������� ���� � ��� ������������ ������� ��� � ������������������, � ����� ���������� ������� �� ����� ������ �����. 
<P class=Main align=justify>� ������� �������� �������� ������ ����������<STRONG> ���� ��������������� � ������������� ��������, </STRONG>����������� ����� ���������� �������. ������, �������� �� ������ ������������� �������� ��������������, �������� ���������� ������� � ��� �������� �� ������ &#171;������� ����������� �����&#187;. ����� ��� �������<STRONG> ���������� �������� ���������� �������� </STRONG>������ (���� ������������ ������������� � ����� ������ ������� � ������������ � ��� ������������� ������� � 1,3 �� 1,4%), ������ ���� ������������ ������������� ��� ������� ��� �� ��������, �������� ���������� �����, ��� � �� ������, ��������������� ������������������ ������� ���������, � ����� ����������� �������. �������� ������� �� ���������� � ���� ��������, �������� ���������, ���������� � ������������� ������� �������� �����. 
<P class=Main align=justify>����� ����� ��������������� �������� �� ��������� � ���������� ������� � ��� ��������� ���������� (5,7%, �� 219,9 ����. ������), �� ���� � ������� ������ �� 1 ���� �������� 5,1%. ������������ ��������� �������������� ��� � ������, ��� � � ����������� ������. ������ � ��� �������� ����� ���������� ���������� �������� ����� ������� ������������� ���������� ��� (66,5%). 
<P class=Main align=justify>����� �����<STRONG> ������������ ����������� ��� </STRONG>��������� ���� ������ ���������� ������� (1,9 ������ 2,8%). ������ � ��� ���������� ������� ���������� ������� �� ������ ������ � �������� ��� ������ � ������������� ����� ������ ������� ���� ���������� �� ��������� �� ���������� �������� �������, ����������� � �������������, ������������ � ����������������� �������������. ����� �������� ������������ ��������� ������� ��������� ������� �� 2% � ��������� 1 804,3 ����. ������. ������ �� ���������� � ������� ������ �� ����� ��������� &#8212; � 42,4 �� 42,1%. 
<P class=Main align=justify>������� ������������� ��������� ����������� ������� �� ���������, ����������� �� <STRONG>������������� �����,</STRONG> � ��� ����������� �� 3,8% (��� �� 17,9 ����. ������) � �� ��������� 495,1 ����. ������. ���� ��������������� ��� � ������� ������ ����������� �� ����������, ��������� �� ������ 11,5%. ��������� �������������� � ������� ������� �� ���� ����������� �������� � �������&shy;������������� � ����������� ������ (���� �� 5,6%). ������� ������� ������������� �� ��������, �������� ���������� ������, ����������� �� ��� �� ������ ���� �� 1,1%. 
<P class=Main align=justify>����������� �������� ����������� � � ���������<STRONG> �������</STRONG> ��������� ����������� �������, <STRONG>����������� �� ���������</STRONG> � ������ ������. �� 1 ����, �� ��������� � 1 ���, ������� �� ������ &#171;������&#187; ������� �� 5,4%, ������ ���� ����� �������� ������� �� ������ � ����������� ������ (7,2%) ����������� �������� ����������� ���������� �� ���������, ����������� � ���������� ������������������ (0,8%). 
<P class=Main align=justify>����� ��������<STRONG> �� ���������� ����� </STRONG>������� � ������ ���� ���������� (�� 1,4%, �� 187,3 ����. ������), ��� ��������������� ������� � ����������� � ���� ��������� ����������� ��������� �������� �������� ����������� � ������. 
<P class=Main align=justify>�������� �� �������� � ��� ������������� ��������� �� �������� �����, ���������� ������ <STRONG>�� ����� ������ ����� </STRONG>���������� ������������ �������. �� ���� ��������������� (�� 0,8%) ����� ������� ��� ���������� �������� ������ � ����� (���������� �� 13,8%, �� 126,8 ����. ������), ������ � ��� � ������� �� ������ ���������� ������� � ��� ������������ ������ ������ ����������� � ����������� (���� ������������� ����� � �������� ����� �������� 60%). �� <STRONG>����� ���������������� �����</STRONG> ������ ����� ������������� ����������� �������� ����������� ��������� ���������� ���� �������� (�� 1,4%), ������ �� ���� � ���������� �������� ������ ����� ���������� ��������� (� 23,6 �� 22,5%). � ��������� ��������<STRONG> � �������� ������������� </STRONG>���������� ���� ���������� �� �������� �������������� ������������. � ����� �� ����� ����� �������� ������ ������� � ������ ������ ������� �� 6,% (�� 28,5 ����. ������), ��������� 503,1 ����. ������, ��� 11,7% �������. 
<P class=Main align=justify>�� ��������� � ���������� ������� � ��� ����������� <STRONG>��������� ������</STRONG> ������ �������. ����� ����� �������� ������� �� ��������� � ��������� � ����� ������, � ����� �������� ������� � ����������� �������� � ������ ����, �� ��������� � 1 ���, �������� �� 3,7% &#8212; �� 324,5 ����. ������. ������ ���� ������� ���������� � ������� ������ ����������� �� ����������. 
<P class=Main align=justify><SUP>1&nbsp;</SUP><EM>������ ���������� �� 658 ��������� ������������, ������� ����������� ���� ���������� ���������� � ���������� ��� ����� ������ �� 1.06.2005 � ��� ����� ����������� �� ��������� ������.</EM></P></TD></TR></TBODY></TABLE>
<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR>
<TD class=bgcolor1>
<P class=Rubrika align=center><STRONG>��������� �������� ������� ��������� ����������� ����������� �������, ���. ������</STRONG></P></TD></TR>
<TR>
<TD class=bgwhite>
<TABLE cellSpacing=0 cellPadding=0 width="100%" align=center border=1>
<TBODY>
<TR>
<TD width="48%">
<P align=center><STRONG>������</STRONG></P></TD>
<TD width="9%">
<P align=center><STRONG>01.04.2005</STRONG></P></TD>
<TD width="12%">
<P align=center><STRONG>01.05.2005</STRONG></P></TD>
<TD width="12%">
<P align=center><STRONG>��. ��� 01.04.05</STRONG></P></TD>
<TD width="9%">
<P align=center><STRONG>��. ��� 01.05.05</STRONG></P></TD>
<TD width="10%">
<P align=center><STRONG>��������<BR>�� ������ 2005 ���� </STRONG></P></TD></TR>
<TR>
<TD>
<P>������ �������-�����</P></TD>
<TD>
<P align=right>4 176 710,8</P></TD>
<TD>
<P align=right>4 171 253,4</P></TD>
<TD>
<P align=right>100,0</P></TD>
<TD>
<P align=right>100,0</P></TD>
<TD>
<P align=right>99,9</P></TD></TR>
<TR>
<TD>
<P><STRONG>����������� ��������:</STRONG></P></TD>
<TD>
<P align=right><STRONG>656 447,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>667 622,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>15,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>16,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>101,7</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�.: �������� ������� � ����� ������</P></TD>
<TD>
<P align=right>518 133,7</P></TD>
<TD>
<P align=right>527 867,7</P></TD>
<TD>
<P align=right>12,4</P></TD>
<TD>
<P align=right>12,7</P></TD>
<TD>
<P align=right>101,9</P></TD></TR>
<TR>
<TD>
<P>���������� ��������� ������������ (� ������ ������� ���)</P></TD>
<TD>
<P align=right>138 314,0</P></TD>
<TD>
<P align=right>139 754,9</P></TD>
<TD>
<P align=right>3,3</P></TD>
<TD>
<P align=right>3,4</P></TD>
<TD>
<P align=right>101,0</P></TD></TR>
<TR class=bgwhite>
<TD>
<P><STRONG>������� �� ��������� ������</STRONG></P></TD>
<TD>
<P align=right><STRONG>181 742,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>178 406,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>4,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>4,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>98,2</STRONG></P></TD></TR>
<TR class=bgwhite>
<TD>
<P>� �.�. : ������� ��� ��������� ������ �� ������</P></TD>
<TD>
<P align=right>156 775,2</P></TD>
<TD>
<P align=right>153 354,8</P></TD>
<TD>
<P align=right>3,8</P></TD>
<TD>
<P align=right>3,7</P></TD>
<TD>
<P align=right>97,8</P></TD></TR>
<TR class=bgwhite>
<TD>
<P>������� ��� ��������� ������ �� ������ �������</P></TD>
<TD>
<P align=right>5 712,5</P></TD>
<TD>
<P align=right>6 013,9</P></TD>
<TD>
<P align=right>0,1</P></TD>
<TD>
<P align=right>0,1</P></TD>
<TD>
<P align=right>105,3</P></TD></TR>
<TR class=bgwhite>
<TD>
<P>������ �������</P></TD>
<TD>
<P align=right>19 254,3</P></TD>
<TD>
<P align=right>19 037,8</P></TD>
<TD>
<P align=right>0,5</P></TD>
<TD>
<P align=right>0,5</P></TD>
<TD>
<P align=right>98,9</P></TD></TR>
<TR>
<TD>
<P><STRONG>�������������:</STRONG></P></TD>
<TD>
<P align=right><STRONG>3 338 521,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>3 325 224,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>79,9</STRONG></P></TD>
<TD>
<P align=right><STRONG>79,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>99,6</STRONG></P></TD></TR>
<TR>
<TD>
<P>�������� ��������</P></TD>
<TD>
<P align=right>888 549,6</P></TD>
<TD>
<P align=right>818 817,7</P></TD>
<TD>
<P align=right>21,3</P></TD>
<TD>
<P align=right>19,6</P></TD>
<TD>
<P align=right>92,2</P></TD></TR>
<TR>
<TD>
<P>� �.�. : �������� �� ����.� ���.������ �����������</P></TD>
<TD>
<P align=right>644 395,8</P></TD>
<TD>
<P align=right>603 896,1</P></TD>
<TD>
<P align=right>15,4</P></TD>
<TD>
<P align=right>14,5</P></TD>
<TD>
<P align=right>93,7</P></TD></TR>
<TR>
<TD>
<P>������ �����</P></TD>
<TD>
<P align=right>161 228,8</P></TD>
<TD>
<P align=right>132 151,5</P></TD>
<TD>
<P align=right>3,9</P></TD>
<TD>
<P align=right>3,2</P></TD>
<TD>
<P align=right>82,0</P></TD></TR>
<TR>
<TD>
<P><STRONG>�������� (����� ������)</STRONG></P></TD>
<TD>
<P align=right><STRONG>1 059 638,8</STRONG></P></TD>
<TD>
<P align=right><STRONG>1 124 513,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>25,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>27,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>106,1</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�.: ���������</P></TD>
<TD>
<P align=right>526 654,2</P></TD>
<TD>
<P align=right>561 518,2</P></TD>
<TD>
<P align=right>12,6</P></TD>
<TD>
<P align=right>13,5</P></TD>
<TD>
<P align=right>106,6</P></TD></TR>
<TR>
<TD>
<P>����������� ���</P></TD>
<TD>
<P align=right>532 984,6</P></TD>
<TD>
<P align=right>562 995,1</P></TD>
<TD>
<P align=right>12,8</P></TD>
<TD>
<P align=right>13,5</P></TD>
<TD>
<P align=right>105,6</P></TD></TR>
<TR>
<TD>
<P>� �.�.: �������� �����������</P></TD>
<TD>
<P align=right>307 880,7</P></TD>
<TD>
<P align=right>315 474,1</P></TD>
<TD>
<P align=right>7,4</P></TD>
<TD>
<P align=right>7,6</P></TD>
<TD>
<P align=right>102,5</P></TD></TR>
<TR>
<TD>
<P>������ ��������</P></TD>
<TD>
<P align=right>225 103,9</P></TD>
<TD>
<P align=right>247 521,0</P></TD>
<TD>
<P align=right>5,4</P></TD>
<TD>
<P align=right>5,9</P></TD>
<TD>
<P align=right>110,0</P></TD></TR>
<TR>
<TD>
<P><STRONG>����� ������ </STRONG></P></TD>
<TD>
<P align=right><STRONG>96 395,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>94 320,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>2,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>2,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>97,8</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�.: �������� ���������� ������</P></TD>
<TD>
<P align=right>59 240,3</P></TD>
<TD>
<P align=right>56 859,5</P></TD>
<TD>
<P align=right>1,4</P></TD>
<TD>
<P align=right>1,4</P></TD>
<TD>
<P align=right>96,0</P></TD></TR>
<TR>
<TD>
<P>�������� ������-������������</P></TD>
<TD>
<P align=right>12 528,6</P></TD>
<TD>
<P align=right>14 589,2</P></TD>
<TD>
<P align=right>0,3</P></TD>
<TD>
<P
 align=right>0,3</P></TD>
<TD>
<P align=right>116,4</P></TD></TR>
<TR>
<TD>
<P><STRONG>��� ����������, ������� ������������ �������������, ��� ������������ % (� ������ �������� ����� ������)</STRONG></P></TD>
<TD>
<P align=right><STRONG>660 506,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>663 013,8</STRONG></P></TD>
<TD>
<P align=right><STRONG>15,8</STRONG></P></TD>
<TD>
<P align=right><STRONG>15,9</STRONG></P></TD>
<TD>
<P align=right><STRONG>100,4</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�. ��� ������-������������</P></TD>
<TD>
<P align=right>455 530,7</P></TD>
<TD>
<P align=right>457 724,1</P></TD>
<TD>
<P align=right>10,9</P></TD>
<TD>
<P align=right>11,0</P></TD>
<TD>
<P align=right>100,5</P></TD></TR>
<TR>
<TD>
<P>������ ������ ����������</P></TD>
<TD>
<P align=right>479 663,5</P></TD>
<TD>
<P align=right>476 874,8</P></TD>
<TD>
<P align=right>11,5</P></TD>
<TD>
<P align=right>11,4</P></TD>
<TD>
<P align=right>99,4</P></TD></TR>
<TR>
<TD>
<P>������ �������</P></TD>
<TD>
<P align=right>153 767,5</P></TD>
<TD>
<P align=right>147 684,8</P></TD>
<TD>
<P align=right>3,7</P></TD>
<TD>
<P align=right>3,5</P></TD>
<TD>
<P align=right>96,0</P></TD></TR>
<TR class=bgwhite>
<TD>
<P align=center><STRONG>�����</STRONG></P></TD>
<TD>
<P align=right></P></TD>
<TD>
<P align=right></P></TD>
<TD>
<P align=right></P></TD>
<TD>
<P align=right></P></TD>
<TD>
<P align=right></P></TD></TR>
<TR>
<TD>
<P><STRONG>������ �������-�����</STRONG></P></TD>
<TD>
<P align=right><STRONG>4 176 710,8</STRONG></P></TD>
<TD>
<P align=right><STRONG>4 171 253,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>100,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>100,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>99,9</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>������� � ������������ � ��� ������������� (� ������ ��������) ����� ��� ������������ ���������</STRONG></P></TD>
<TD>
<P align=right><STRONG>2 838 954,9</STRONG></P></TD>
<TD>
<P align=right><STRONG>2 895 422,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>68,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>69,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>102,0</STRONG></P></TD></TR>
<TR>
<TD>
<P>����� �������� (����� ������)</P></TD>
<TD>
<P align=right>2 174 302,6</P></TD>
<TD>
<P align=right>2 227 944,0</P></TD>
<TD>
<P align=right>52,1</P></TD>
<TD>
<P align=right>53,4</P></TD>
<TD>
<P align=right>102,5</P></TD></TR>
<TR>
<TD>
<P>� �.�. ����� ���������</P></TD>
<TD>
<P align=right>197 030,2</P></TD>
<TD>
<P align=right>207 973,0</P></TD>
<TD>
<P align=right>4,7</P></TD>
<TD>
<P align=right>5,0</P></TD>
<TD>
<P align=right>105,6</P></TD></TR>
<TR>
<TD>
<P>����� ��. �����</P></TD>
<TD>
<P align=right>1 977 272,5</P></TD>
<TD>
<P align=right>2 019 971,0</P></TD>
<TD>
<P align=right>47,3</P></TD>
<TD>
<P align=right>48,4</P></TD>
<TD>
<P align=right>102,2</P></TD></TR>
<TR>
<TD>
<P>� �.�. ����� ������������</P></TD>
<TD>
<P align=right>1 725 481,7</P></TD>
<TD>
<P align=right>1 768 204,3</P></TD>
<TD>
<P align=right>41,3</P></TD>
<TD>
<P align=right>42,4</P></TD>
<TD>
<P align=right>102,5</P></TD></TR>
<TR>
<TD>
<P>������ ����� ��������</P></TD>
<TD>
<P align=right>251 790,7</P></TD>
<TD>
<P align=right>251 766,8</P></TD>
<TD>
<P align=right>6,0</P></TD>
<TD>
<P align=right>6,0</P></TD>
<TD>
<P align=right>100,0</P></TD></TR>
<TR>
<TD>
<P>������������ ������������� ���</P></TD>
<TD>
<P align=right>35 557,1</P></TD>
<TD>
<P align=right>34 315,8</P></TD>
<TD>
<P align=right>0,9</P></TD>
<TD>
<P align=right>0,8</P></TD>
<TD>
<P align=right>96,5</P></TD></TR>
<TR>
<TD>
<P>��� ��������</P></TD>
<TD>
<P align=right>484 271,8</P></TD>
<TD>
<P align=right>477 165,7</P></TD>
<TD>
<P align=right>11,6</P></TD>
<TD>
<P align=right>11,4</P></TD>
<TD>
<P align=right>98,5</P></TD></TR>
<TR>
<TD>
<P>� �.�. ��� ������-������������</P></TD>
<TD>
<P align=right>287 879,6</P></TD>
<TD>
<P align=right>283 839,5</P></TD>
<TD>
<P align=right>6,9</P></TD>
<TD>
<P align=right>6,8</P></TD>
<TD>
<P align=right>98,6</P></TD></TR>
<TR>
<TD>
<P>� �.�. ������������ �������������</P></TD>
<TD>
<P align=right>3 125,9</P></TD>
<TD>
<P align=right>3 125,5</P></TD>
<TD>
<P align=right>0,1</P></TD>
<TD>
<P align=right>0,1</P></TD>
<TD>
<P align=right>100,0</P></TD></TR>
<TR>
<TD>
<P>�������� �������</P></TD>
<TD>
<P align=right>180 038,4</P></TD>
<TD>
<P align=right>189 975,9</P></TD>
<TD>
<P align=right>4,3</P></TD>
<TD>
<P align=right>4,6</P></TD>
<TD>
<P align=right>105,5</P></TD></TR>
<TR>
<TD>
<P>� �.�. ������������</P></TD>
<TD>
<P align=right>936,2</P></TD>
<TD>
<P align=right>905,2</P></TD>
<TD>
<P align=right>0,0</P></TD>
<TD>
<P align=right>0,0</P></TD>
<TD>
<P align=right>96,7</P></TD></TR>
<TR>
<TD>
<P>������������ ������������� �����</P></TD>
<TD>
<P align=right>39 619,2</P></TD>
<TD>
<P align=right>38 346,5</P></TD>
<TD>
<P align=right>0,9</P></TD>
<TD>
<P align=right>0,9</P></TD>
<TD>
<P align=right>96,8</P></TD></TR>
<TR>
<TD>
<P><STRONG>������ � ������ ������������ ������ (������)</STRONG></P></TD>
<TD>
<P align=right><STRONG>342,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>337,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>98,5</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>������������ ��������</STRONG></P></TD>
<TD>
<P align=right><STRONG>2 259,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>2 322,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>102,8</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>�������� � ������ ������</STRONG></P></TD>
<TD>
<P align=right><STRONG>454 789,2</STRONG></P></TD>
<TD>
<P align=right><STRONG>474 633,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>10,9</STRONG></P></TD>
<TD>
<P align=right><STRONG>11,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>104,4</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�. � �������� ������������� ��</P></TD>
<TD>
<P align=right>117 391,2</P></TD>
<TD>
<P align=right>111 837,2</P></TD>
<TD>
<P align=right>2,8</P></TD>
<TD>
<P align=right>2,7</P></TD>
<TD>
<P align=right>95,3</P></TD></TR>
<TR>
<TD>
<P>�����</P></TD>
<TD>
<P align=right>107 059,0</P></TD>
<TD>
<P align=right>111 375,8</P></TD>
<TD>
<P align=right>2,6</P></TD>
<TD>
<P align=right>2,7</P></TD>
<TD>
<P align=right>104,0</P></TD></TR>
<TR>
<TD>
<P><STRONG>������������� �������</STRONG></P></TD>
<TD>
<P align=right><STRONG>25 997,9</STRONG></P></TD>
<TD>
<P align=right><STRONG>27 009,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>103,9</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>���</STRONG></P></TD>
<TD>
<P align=right><STRONG>62 474,8</STRONG></P></TD>
<TD>
<P align=right><STRONG>64 030,2</STRONG></P></TD>
<TD>
<P align=right><STRONG>1,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>1,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>102,5</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>��������, �������� �������� � ����������� �������</STRONG></P></TD>
<TD>
<P align=right><STRONG>552 209,9</STRONG></P></TD>
<TD>
<P align=right><STRONG>473 891,9</STRONG></P></TD>
<TD>
<P align=right><STRONG>13,2</STRONG></P></TD>
<TD>
<P align=right><STRONG>11,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>85,8</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�. : �������� � ������</P></TD>
<TD>
<P align=right>159 050,1</P></TD>
<TD>
<P align=right>160 924,2</P></TD>
<TD>
<P align=right>3,8</P></TD>
<TD>
<P align=right>3,9</P></TD>
<TD>
<P align=right>101,2</P></TD></TR>
<TR>
<TD>
<P>� �. �. �������� � ������-������������</P></TD>
<TD>
<P align=right>110 149,7</P></TD>
<TD>
<P align=right>114 008,0</P></TD>
<TD>
<P align=right>2,6</P></TD>
<TD>
<P align=right>2,7</P></TD>
<TD>
<P align=right>103,5</P></TD></TR>
<TR>
<TD>
<P><STRONG>������� � ��������� ��������� �������</STRONG></P></TD>
<TD>
<P align=right><STRONG>24 827,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>25 225,2</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>101,6</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>���������, ����������� ��������</STRONG></P></TD>
<TD>
<P align=right><STRONG>71 544,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>73 059,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>1,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>1,8</STRONG></P></TD>
<TD>
<P align=right><STRONG>102,1</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>������ ������</STRONG></P></TD>
<TD>
<P align=right><STRONG>143 653,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>135 658,8</STRONG></P></TD>
<TD>
<P align=right><STRONG>3,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>3,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>94,4</STRONG></P></TD></TR></TBODY></TABLE></TD></TR>
<TR>
<TD></TD></TR></TBODY></TABLE>


</body>
</html>

