<!--
ID:38637183
TITLE:����������� - ������ �������
SOURCE_ID:11602
SOURCE_NAME:����� ������
AUTHOR:
NUMBER:1
ISSUE_DATE:2008-01-31
RECORD_DATE:2008-01-14


-->
<html>
<head>
<title>
38637183-����������� - ������ �������
</title>
</head>
<body >


<P align=justify>�������� ������� �������� ��������</P>
<P align=justify>� ����������� ������ �� �� ������ ���� ������ � ������������ ��� ����� �� ����� ���������� � ����������� �������� ������ ������������ �������. ������������� �� ��� ��� ���������, ��� ����� ����������� ��������������? �� ������ �������� ���� ������, �������� ������� ���������� ��������� �������� �� ��������� �������:</P>
<P align=justify>1. ������ ��� ������ ������ ���� �������-����������?</P>
<P align=justify>2. ������� ����� ���� ������� � ������? �������� �������������� (����������� ��� ������� ��������)?</P>
<P align=justify>3. ������� ������� ������������ ��� ������ �� ����� ��������������? ��������� ��������� ���������� ���������� �� ��������?</P>
<P align=justify>4. � ��� ����������� ������ �����������?</P>
<P align=justify>5. ����������� �� ��������� �������������� � ���� ���������?</P>
<P align=justify>������ ��������, �����������, 27 ���, ����������� ������, �� ������������� - �������� ���������������� � �������������� ����� �����.</P>
<P align=justify>1. ���� �������������� � TeleTRADE � ����� 3 ���� ����� � �������� �������� ��������. ������ ������������ ������� �� ���� �������. � ����� �������� �������� � ������������ �� �������� ��������������� ����� ����� �� ����� �������, � � ��� ������� ������ ������, ������� ��� "��" � "������", ������ ����������� ���� ����. 31 ������ 2007 ���� ��� ������ ���� � ������������, � 1 ������ � �������������.</P>
<P align=justify>2-3. ������� �� �������� ����� � ������������ ���� ������� �� ��� �� � �����, �������� 180 ���. ������. ������� ���� ������� � �������� � ������� ���������, ��������, �������� ������� ������, ������������. ������ �� ���������� ������� � ��������� ����, ��� ���� ����� ������������ ����� �� �������� �� �������. � ������������� �� �������� � ������ ���������� �� ������������������ �����, ������� ���������� ��� ���������������� �������. ������ ����� � ������������ �������� � ���, ��� �������������� ���������� ������������ ������� �� ���������� �������� �����, ������� 6 �������.</P>
<P align=justify>4. � ������� �������� �� ������������ ��� ����� �������� ����� ����� ������������� �������� � ����������� � TeleTRADE. �� ������� ���������� ������������ ������� TeleTRADE � ������ ������� �� ����� ��������, ����� ���� ����������� ������������ ���� www.arkhangelsk.teletrade.ru, ������������� TeleTRADE, ����� ��������� ������� �� ������� �����, ���������� �������������� ���������� ����������, ������������� ����������� � ������� ������������� ������. ����� ����, ���� ���������� ������ ����� ����������� ������ ����������� ����� �� ���������� ������, �������� ��������, �������� ��� ��������� ������ ������������� �������� � ����������� ������ � ������� ����� ��������.</P>
<P align=justify>5. ��, �������. �� ������ ����� ��� ���������� ����������� �������, � 2008 �. ��������� �������� ������ � ������� � ���������. �� ��������� �� ��������� �� ������� ������ �������� ���� ���������� ���� TeleTRADE.</P>
<P align=justify>������� ��� �������� ���������� �� �����</P>
<P align=justify>15 ���</P>
<P align=justify>����� ��������� ��������� ���������� ����� ���������-���������</P>
<P align=justify>������ ��� ����, �������� ���������, �������������� ���������, �������� �������������.</P>
<P align=justify>����� ����������� ����������</P>
<P align=justify>� ����������� �� ������ ����������� ��������� � �������, 10000 $-40000 $.</P>
<P align=justify>���������� (��������������) �����</P>
<P align=justify>���</P>
<P align=justify>������ (������������� �������, ������ �����������)</P>
<P align=justify>���</P>
<P align=justify>��������� ���� �����������</P>
<P align=justify>������ ����������� �� 6 �� 12 �������</P>
<P align=justify>���������� � ���������</P>
<P align=justify>������� ��������� � ����������� �� ������ ����������� ����������� ������, ������������ � �������-������� ���������� ��� � �������� ������� ������, �������� �� ������������ �����������. �������������� ���������� ����� � "��������" ������ ����������� ������.</P>
<P align=justify>���������� � �������������� ���������</P>
<P align=justify>� �������������� �������. �������� �������� �������� �������������-����� ����� ������ ���������.</P>
<P align=justify>��������� �������, �. ���������</P>
<P align=justify>1. � ������� �������������� ���������������� �� ����� ������ ��� 7 ���. �������� ����� Westland � �� ���������� ������ ��� �������� ����� � ��������� � ����� ��������������� ���������. ����� ��������� ����������� ������� ��������� ������� �� ������������, � ���� ����� �� ������ ��������� � �� �������� �� ����.</P>
<P align=justify>2. � ������ ���� ������� ����� 800000 ������, ��������� ������� ���������� ������ �� ���� ����������� ��������, �. �. ������� �������� ��� ��������� ��� �������. ��� �������� ������ ��������� ������������ ������� �������� (���������� ������).</P>
<P align=justify>3. ��� ������ � ����� �������������� ������������ ����� 6 �������.</P>
<P align=justify>� ������ ��������� ������ ��� ����� ������. �� � ����� �� ����� 1 ����.</P>
<P align=justify>�������� ���������� ����� �����������, ��� ��������� ����������, �. �. ��, ��������, �����������, ��� ��������� ������� �� �������� �������� �� 1,5 ����, � ��� ������ �������� ���� �������� ������.</P>
<P align=justify>4. � ������ � ��������� Westland ������ ����������� ����� ��������, ������� � ������� � ���������� �������� �������� � ���������� ��������� ��������: ����������� ��������� ������������; �������������� ���� ����������� ��������; ���������� ������ �� ������������ ������������; ������ � ����������� ������ ���������; ���������� ������ �� ������������ ������������ � �������; ������������� ���������� ������ �� �������; �������������� �������� ��������; �������������� ���������� ���������.</P>
<P align=justify>5. � ��������� ����� � ���� ��� 4 ��������� ��������, � � 2008 ���� �������� ������� ��� ���.</P>
<P align=justify>������� ��� �������� ���������� �� �����</P>
<P align=justify>� 1994 ���� � � ��������� ����� ����� 20 ����������� ��������� � 85 ���������������.</P>
<P align=justify>����� ����������� ����������</P>
<P align=justify>��� �������� �������� 60 ��. � ������� �� �������� (������� ������, �������� ������������ � �������� �����) �������� �������� 1800 ���. ���.</P>
<P align=justify>���� �����������</P>
<P align=justify>�� 0,5 �� 2-� ���</P>
<P align=justify>���������� � ���������</P>
<P align=justify>Street-������, ��� �������� ������� �� ����� - ������ ���� ������ � ��������� ������ � ������, ���������� �� �����, � ����� �������� ������� ������.</P>
<P align=justify>������� � �������� ������ - �� 60 ��. � �� 1-� ��� 2-� ������ �������� ��������� ������.</P>
<P align=justify>���������� � �������������� ���������</P>
<P align=justify>������� ��������� ��������� ������� � ������ ���������� ���������������.</P>
<P align=justify>���������� ��������� ������������ ������ � ��������, ��������� ��������� �������� Westland ��� ��������������� ������� �������.</P>
<P align=justify>������� �����, 23 ����, ������, ����������� ������, �� ������������� - �� ����� �������������� � ����������� ��������� � ������</P>
<P align=justify>1. � ������� � ����� 5 ���. ������� �������� ��� �������� � ��������������� ����� �������� ������� �������, �� ���� ���� � ������ ������� ��� ������ "������� ����". ������? ������������� �� ���������� ������������ - ��� �������� ������� � "������� �����" ������� ����.</P>
<P align=justify>2. � �������� ������������ ������������ � ������� ����������� �������� � ������� 320000 ���., ������� ���� ������� ���� �� ������ 16-�� ��������� ����� � �. �����-�����.</P>
<P align=justify>3. ������������ � ������� � �������� �� ������ ����� - 1 ������ 2007 ���� � �������� ������ ���������� �����. ��������, � ���������� ����� �� ��� ������, ��� ��������, ���� ���� ����������� ������� (��������, ������ ������, � ������� ��������� ����), �� ��� �� ����� � ������ ������ ��������� �������� ��� �������.</P>
<P align=justify>4. ������� ��� �� ������! � ���� ��������, �� ����� ���������� �������-��������� �������� � �������� ������������ ��������� ��������� � ���������� ���� � ����������� �����. ��� "������� ����" �������� ������������ �������� �������. ����� ����� ����������� � ������� � ������������ �������� ����-������ ����, �� �� �������������� ������. ��������� ����� �. �����-����� � ��� ���� ���������� (��� ���� ��������� "������� �����"), � ����-������ �� �� ������������. ������ ����������� ������� ������, ��� ��� �������� ���-���� ����� ������������ ����-�������. ��� ������������� ����������� ���������, �� ��������� ��� ���������� - ������ �� ������, ����� ������� ���������� ��� ��������� �� ������� ��������.</P>
<P align=justify>5. ��. �� ��������� ��� � ���� �������� ������� ��� ���� ����. ��� ��� ������ ��� ���������.</P>
<P align=justify>������� ��� �������� ���������� �� �����</P>
<P align=justify>����� 5 ���</P>
<P align=justify>����� ����������� ����������</P>
<P align=justify>125000 ���.</P>
<P align=justify>���������� �����</P>
<P align=justify>12000 ���.</P>
<P align=justify>������</P>
<P align=justify>9000 ���.</P>
<P align=justify>���� �����������</P>
<P align=justify>�� 6 ������� �� 1 ����</P>
<P align=justify>���������� � ���������</P>
<P align=justify>����� ������� - 15-18 ��. �</P>
<P align=justify>�������� ����������� (������� � ����� �� ��������� �� ������� ����������������), 52 ����, �� ����������� �������.</P>
<P align=justify>1. ��-������, �������� �� ������ ��� �� �����. ��-������, �� ���� ����� �������, �������� �������� �����-�� �����. �-�������, �� ��� �������� � �������� � ��������� ���� ���������� � �������� ����������� ������������ ��������� ������� ������. �� �������� �������� � ������������� ������� � �������� �� �����. ����� � ������ ������ - ���� ������������, ����-�� �������� ����, ����-�� ������, �� ��������� �������� ���� �� ������, � ������. � ��� ����� ��� �������������� � �����������. ����, ������� �������� �����-������ ������ � ������� �������� ��������, � ������� ��� ������, ����� ������ � ������� ������, �������������. ��� �, ��� ��� ���� �������� �������������.</P>
<P align=justify>2. ���� ���� ��������, �� �������� �������� � �����. �� ������ ����� � ����, ��� ������ ��������� ������, ��� ���������, "�� ���������", ������ �� �����, �� ������ ��� ������� ������. ����������� ������ ���� ����� ���������. � �����, ��� � ����� ��������� ������, �������</P>
<P align=justify>���������, ������� � �. �. �������� � ����������� �����. �������, ���-�� ������, ��� ��� ����� ���� ���� ����� ��������, �� ����� ����� ������ ������.</P>
<P align=justify>3. � ��� ������ ��������� ����� ��� ��������� ������������. �������, �� � ������������ ��������� ������-����, �� � ������� ��� ��� ������������. � �����, �� ����, ������ ��� ��� ���, �� ��������� "�������" � ������� ����� 7-8. ������ ������� �������� ��� ���������� �����. � � ���� ���� ������� �� ��������� � ������� ����������� ���������.</P>
<P align=justify>5. ��� ��� ������ ����� � ������ ��� ���� ������ ����������� �� �������� ��� ������ �������� LTB. �� ����� �� ���� �������������� � ���������� ��������� ������� �� �������� �� �������� ������������� ������� ������. ������, ��� ��� ���� ����� ������������� �����������. �� ������ ������ � ����� ������ �������� ��� ���.</P>
<P align=justify>������� ��� �������� ���������� �� �����</P>
<P align=justify>����� 10 ���</P>
<P align=justify>����� ����������� ����������</P>
<P align=justify>�� ��������������</P>


</body>
</html>

