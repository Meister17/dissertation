<!--
ID:35709600
TITLE:�� &#171;����������&#187;
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:
NUMBER:11
ISSUE_DATE:2005-11-30
RECORD_DATE:2005-12-29


-->
<html>
<head>
<title>
35709600-�� &#171;����������&#187;
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgwhite>
<TD></TD></TR>
<TR class=bodycolor>
<TD>
<P class=Rubrika align=center>������������ ���� &#171;����������&#187;<BR>(�������� � ������������ ����������������)</P>
<P class=Main align=center>��������������� ����� 2192, ��� 044583337, 
<P class=Main align=center>�������� �����: 111024, �. ������, ��. ���������, 46�</P></TD></TR>
<TR class=bgwhite>
<TD align=middle>
<P class=Rubrika>������ �� 1 ������� 2005 ����</P>
<TABLE cellSpacing=0 cellPadding=0 width="98%" borderColorLight=#cccccc border=2>
<TBODY>
<TR>
<TD>
<P align=center><STRONG>����� �/�</STRONG></P></TD>
<TD>
<P align=center><STRONG>������������ ������</STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� �������� ����</STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� ��������������� �������� ���� �������� ����</STRONG></P></TD></TR>
<TR>
<TD width=63 height=0>
<P align=center><STRONG>1</STRONG></P></TD>
<TD width=322>
<P align=center><STRONG>2</STRONG></P></TD>
<TD width=92>
<P align=center><STRONG>3</STRONG></P></TD>
<TD width=110>
<P align=center><STRONG>4</STRONG></P></TD></TR>
<TR>
<TD colSpan=4 height=0>
<P align=center><B>������</B></P></TD></TR>
<TR>
<TD>
<P>1.</P></TD>
<TD>
<P>�������� ��������</P></TD>
<TD>
<P align=right>22 351</P></TD>
<TD>
<P align=right>14 386</P></TD></TR>
<TR>
<TD>
<P>2.</P></TD>
<TD>
<P>�������� ��������� ����������� � ����������� ����� ���������� ���������</P></TD>
<TD>
<P align=right>266 635</P></TD>
<TD>
<P align=right>175 049</P></TD></TR>
<TR>
<TD>
<P>2.1.</P></TD>
<TD>
<P>������������ �������</P></TD>
<TD>
<P align=right>7 452</P></TD>
<TD>
<P align=right>8 216</P></TD></TR>
<TR>
<TD>
<P>3.</P></TD>
<TD>
<P>�������� � ��������� ������������</P></TD>
<TD>
<P align=right>25 139</P></TD>
<TD>
<P align=right>35 864</P></TD></TR>
<TR>
<TD>
<P>4.</P></TD>
<TD>
<P>������ �������� � �������� ������ ������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>15 465</P></TD></TR>
<TR>
<TD>
<P>5.</P></TD>
<TD>
<P>������ ������� �������������</P></TD>
<TD>
<P align=right>115 862</P></TD>
<TD>
<P align=right>115 546</P></TD></TR>
<TR>
<TD>
<P>6.</P></TD>
<TD>
<P>������ �������� � �������������� ������ ������, ������������ �� ���������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>7.</P></TD>
<TD>
<P>������ �������� � ������ ������, ��������� � ������� ��� �������</P></TD>
<TD>
<P align=right>1 251</P></TD>
<TD>
<P align=right>1 184</P></TD></TR>
<TR>
<TD>
<P>8.</P></TD>
<TD>
<P>�������� ��������, �������������� ������ � ������������ ������</P></TD>
<TD>
<P align=right>7 529</P></TD>
<TD>
<P align=right>4 736</P></TD></TR>
<TR>
<TD>
<P>9.</P></TD>
<TD>
<P>���������� �� ��������� ���������</P></TD>
<TD>
<P align=right>1 328</P></TD>
<TD>
<P align=right>997</P></TD></TR>
<TR>
<TD>
<P>10.</P></TD>
<TD>
<P>������ ������</P></TD>
<TD>
<P align=right>3 273</P></TD>
<TD>
<P align=right>23 876</P></TD></TR>
<TR>
<TD>
<P>11.</P></TD>
<TD>
<P>����� �������</P></TD>
<TD>
<P align=right>443 368</P></TD>
<TD>
<P align=right>387 103</P></TD></TR>
<TR align=middle>
<TD colSpan=4>
<P><B>II. �������</B></P></TD></TR>
<TR>
<TD>
<P>12.</P></TD>
<TD>
<P>������� ������������ ����� ���������� ���������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>13.</P></TD>
<TD>
<P>�������� ��������� �����������</P></TD>
<TD>
<P align=right>6</P></TD>
<TD>
<P align=right>189</P></TD></TR>
<TR>
<TD>
<P>14.</P></TD>
<TD>
<P>�������� �������� (����������� �����������)</P></TD>
<TD>
<P align=right>332 118</P></TD>
<TD>
<P align=right>335 239</P></TD></TR>
<TR>
<TD>
<P>14.1.</P></TD>
<TD>
<P>������ ���������� ���</P></TD>
<TD>
<P align=right>35 292</P></TD>
<TD>
<P align=right>33 795</P></TD></TR>
<TR>
<TD>
<P>15.</P></TD>
<TD>
<P>���������� �������� �������������</P></TD>
<TD>
<P align=right>5 000</P></TD>
<TD>
<P align=right>6 987</P></TD></TR>
<TR>
<TD>
<P>16.</P></TD>
<TD>
<P>������������� �� ������ ���������</P></TD>
<TD>
<P align=right>501</P></TD>
<TD>
<P align=right>1 221</P></TD></TR>
<TR>
<TD>
<P>17.</P></TD>
<TD>
<P>������ �������������</P></TD>
<TD>
<P align=right>66 572</P></TD>
<TD>
<P align=right>870</P></TD></TR>
<TR>
<TD>
<P>18.</P></TD>
<TD>
<P>������� �� ��������� ������ �� �������� �������������� ���������� ���������, ������ ��������� ������� � �� ��������� � ����������� �������� ���</P></TD>
<TD>
<P align=right>225</P></TD>
<TD>
<P align=right>3 164</P></TD></TR>
<TR>
<TD>
<P>19.</P></TD>
<TD>
<P>����� ������������</P></TD>
<TD>
<P align=right>404 422</P></TD>
<TD>
<P align=right>347 670</P></TD></TR>
<TR align=middle>
<TD colSpan=4>
<P class=bold><STRONG>III. ��������� ����������� �������</STRONG></P></TD></TR>
<TR>
<TD>
<P>20.</P></TD>
<TD>
<P>�������� ���������� (����������)</P></TD>
<TD>
<P align=right>20 200</P></TD>
<TD>
<P align=right>20 200</P></TD></TR>
<TR>
<TD>
<P>20.1.</P></TD>
<TD>
<P>������������������ ������������ ����� � ����</P></TD>
<TD>
<P align=right>20 200</P></TD>
<TD>
<P align=right>20 200</P></TD></TR>
<TR>
<TD>
<P>20.2.</P></TD>
<TD>
<P>������������������ ����������������� �����</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>20.3.</P></TD>
<TD>
<P>�������������������� �������� ������� ������������� ��������� �����������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>21.</P></TD>
<TD>
<P>����������� �����, ����������� � ����������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>22.</P></TD>
<TD>
<P>����������� �����</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>23.</P></TD>
<TD>
<P>���������� �������� �������</P></TD>
<TD>
<P align=right>289</P></TD>
<TD>
<P align=right>289</P></TD></TR>
<TR>
<TD>
<P>24.</P></TD>
<TD>
<P>������� ������� �������� � ����������� �������, �������� �� ����������� �������� (�������)</P></TD>
<TD>
<P align=right>&#8212;473</P></TD>
<TD>
<P align=right>705</P></TD></TR>
<TR>
<TD>
<P>25.</P></TD>
<TD>
<P>����� � ���������������� ������� ������� ��� � ������������ ��������� ����������� (������������ ������ ������� ���)</P></TD>
<TD>
<P align=right>17 323</P></TD>
<TD>
<P align=right>18 346</P></TD></TR>
<TR>
<TD>
<P>26.</P></TD>
<TD height=0>
<P>������ (������) �� �������� ������</P></TD>
<TD>
<P align=right>661</P></TD>
<TD>
<P align=right>1 303</P></TD></TR>
<TR>
<TD>
<P>27.</P></TD>
<TD>
<P>����� ���������� ����������� �������</P></TD>
<TD>
<P align=right>38 946</P></TD>
<TD>
<P align=right>39 433</P></TD></TR>
<TR>
<TD>
<P>28.</P></TD>
<TD>
<P>����� ��������</P></TD>
<TD>
<P align=right>443 368</P></TD>
<TD>
<P align=right>387 103</P></TD></TR>
<TR align=middle>
<TD colSpan=4>
<P><B>IV. ������������� �������������</B></P></TD></TR>
<TR>
<TD>
<P>29.</P></TD>
<TD>
<P>����������� ������������� ��������� �����������</P></TD>
<TD>
<P align=right>9 186</P></TD>
<TD>
<P align=right>49 706</P></TD></TR>
<TR>
<TD>
<P>30.</P></TD>
<TD>
<P>��������, �������� ��������� ������������</P></TD>
<TD>
<P align=right>497</P></TD>
<TD>
<P align=right>5 098</P></TD></TR></TBODY></TABLE>
<P class=top align=justify>��������, ���������� ��������� �� ���� ������� ������� V &#171;����� �������������� ����������&#187;, �� �������������� 
<P class=Rubrika>����� � �������� � ������� �� I9 ������� 2005 ����.</P>
<TABLE cellSpacing=0 cellPadding=0 width="98%" borderColorLight=#cccccc border=2>
<TBODY>
<TR>
<TD>
<P align=center><STRONG>����� �/�</STRONG></P></TD>
<TD>
<P align=center><STRONG>������������ ������</STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� �������� ������ </STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� ��������������� ������ �������� ����</STRONG></P></TD></TR>
<TR>
<TD width=63 height=0>
<P align=center><STRONG>1</STRONG></P></TD>
<TD width=229>
<P align=center><STRONG>2</STRONG></P></TD>
<TD width=149>
<P align=center><STRONG>3</STRONG></P></TD>
<TD width=126>
<P align=center><STRONG>4</STRONG></P></TD></TR>
<TR>
<TD colSpan=4 height=0>
<P class=bold align=center><STRONG>�������� ���������� � ����������� ������ ��:</STRONG></P></TD></TR>
<TR>
<TD>
<P>1</P></TD>
<TD>
<P>���������� ������� � ��������� ������������</P></TD>
<TD>
<P align=right>132</P></TD>
<TD>
<P align=right>484</P></TD></TR>
<TR>
<TD>
<P>2</P></TD>
<TD>
<P>����, ��������������� �������� (����������� ������������)</P></TD>
<TD>
<P align=right>20 955</P></TD>
<TD>
<P align=right>13 735</P></TD></TR>
<TR>
<TD>
<P>3</P></TD>
<TD>
<P>�������� ����� �� ���������� ������ (�������)</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>4</P></TD>
<TD>
<P>������ ����� � ������������� �������</P></TD>
<TD>
<P align=right>581</P></TD>
<TD>
<P align=right>1 955</P></TD></TR>
<TR>
<TD>
<P>5</P></TD>
<TD>
<P>������ ����������</P></TD>
<TD>
<P align=right>576</P></TD>
<TD>
<P align=right>560</P></TD></TR>
<TR>
<TD>
<P>6</P></TD>
<TD>
<P>����� ��������� ���������� � ����������� �������</P></TD>
<TD>
<P align=right>22 244</P></TD>
<TD>
<P align=right>16 734</P></TD></TR>
<TR vAlign=top align=middle>
<TD colSpan=4>
<P><B>�������� ���������� � ����������� ������� ��:</B></P></TD></TR>
<TR>
<TD>
<P>7</P></TD>
<TD>
<P>������������ ��������� ��������� �����������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>8</P></TD>
<TD>
<P>������������ ��������� �������� (����������� �����������)</P></TD>
<TD>
<P align=right>3 675</P></TD>
<TD>
<P align=right>2 136</P></TD></TR>
<TR>
<TD>
<P>9</P></TD>
<TD>
<P>���������� �������� ��������������</P></TD>
<TD>
<P align=right>20</P></TD>
<TD>
<P align=right>224</P></TD></TR>
<TR>
<TD>
<P>10</P></TD>
<TD>
<P>����� ��������� ���������� � ����������� ��������</P></TD>
<TD>
<P align=right>3 695</P></TD>
<TD>
<P align=right>2 360</P></TD></TR>
<TR>
<TD>
<P>11</P></TD>
<TD>
<P>������ ���������� � ����������� ������</P></TD>
<TD>
<P align=right>18 549</P></TD>
<TD>
<P align=right>14 374</P></TD></TR>
<TR>
<TD>
<P>12</P></TD>
<TD>
<P>������ ������ �� �������� � ������� ��������</P></TD>
<TD>
<P align=right>5 107</P></TD>
<TD>
<P align=right>7 912</P></TD></TR>
<TR>
<TD>
<P>13</P></TD>
<TD>
<P>������ ������ �� �������� � ����������� �������</P></TD>
<TD>
<P align=right>318</P></TD>
<TD>
<P align=right>12</P></TD></TR>
<TR>
<TD>
<P>14</P></TD>
<TD>
<P>������ ������ �� �������� � ������������ ��������� � ������� ����������� �������������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>15</P></TD>
<TD>
<P>������ ������ �� ���������� ����������� ������</P></TD>
<TD>
<P align=right>&#8212;40</P></TD>
<TD>
<P align=right>15</P></TD></TR>
<TR>
<TD>
<P>16</P></TD>
<TD>
<P>������������ ������</P></TD>
<TD>
<P align=right>4 299</P></TD>
<TD>
<P align=right>4 556</P></TD></TR>
<TR>
<TD>
<P>17</P></TD>
<TD>
<P>������������ �������</P></TD>
<TD>
<P align=right>24</P></TD>
<TD>
<P align=right>12</P></TD></TR>
<TR>
<TD>
<P>18</P></TD>
<TD>
<P>������ ������ �� ������� ��������</P></TD>
<TD>
<P align=right>4</P></TD>
<TD>
<P align=right>554</P></TD></TR>
<TR>
<TD>
<P>19</P></TD>
<TD>
<P>������ ������ ������������ ������</P></TD>
<TD>
<P align=right>225</P></TD>
<TD>
<P align=right>59</P></TD></TR>
<TR>
<TD>
<P>20</P></TD>
<TD>
<P>���������������-�������������� �������</P></TD>
<TD>
<P align=right>24 723</P></TD>
<TD>
<P align=right>23 138</P></TD></TR>
<TR>
<TD>
<P>21</P></TD>
<TD>
<P>������� �� ��������� ������</P></TD>
<TD>
<P align=right>443</P></TD>
<TD>
<P align=right>&#8212;598</P></TD></TR>
<TR>
<TD>
<P>22</P></TD>
<TD>
<P>������� �� ���������������</P></TD>
<TD>
<P align=right>4 158</P></TD>
<TD>
<P align=right>3 734</P></TD></TR>
<TR>
<TD>
<P>23</P></TD>
<TD>
<P>����������� ������ (������� ����� �� �������)</P></TD>
<TD>
<P align=right>3 497</P></TD>
<TD>
<P align=right>2 431</P></TD></TR>
<TR>
<TD>
<P>24</P></TD>
<TD>
<P>������� (������) �� �������� ������</P></TD>
<TD>
<P align=right>661</P></TD>
<TD>
<P align=right>1 303</P></TD></TR></TBODY></TABLE>
<P class=Rubrika>���������� �� ������ ������������� ��������,<BR>�������� �������� �� �������� ������������ ����<BR>� ���� ������� �� 1 ������� 2005 ����</P>
<TABLE cellSpacing=0 cellPadding=0 width="95%" borderColorLight=#cccccc border=2>
<TBODY>
<TR vAlign=top align=middle>
<TD vAlign=center width=42 height=39>
<P align=center><B>&#8470; �/�</B></P></TD>
<TD vAlign=center width=0>
<P class=bold>������������ ����������</P></TD>
<TD width=88>
<P class=bold align=center><STRONG>������</STRONG> �� �������� ����</P></TD>
<TD width=88>
<P class=bold align=center><STRONG>������ �� ��������������� �������� ���� �������� ����</STRONG></P></TD></TR>
<TR>
<TD height=0>
<P align=center><STRONG>1</STRONG></P></TD>
<TD>
<P align=center><STRONG>2</STRONG></P></TD>
<TD>
<P align=center><STRONG>3</STRONG></P></TD>
<TD>
<DIV align=center>
<P><STRONG>4</STRONG></P></DIV></TD></TR>
<TR>
<TD>
<P align=center>1</P></TD>
<TD>
<P>����������� �������� (�������), ���. ���.</P></TD>
<TD>
<P align=right>56 943</P></TD>
<TD>
<P align=right>39 431</P></TD></TR>
<TR>
<TD>
<P align=center>2</P></TD>
<TD>
<P>����������� �������� ������������� ����������� ������� (��������), �������</P></TD>
<TD>
<P align=right>38,3</P></TD>
<TD>
<P align=right>19,5</P></TD></TR>
<TR>
<TD>
<P align=center>3</P></TD>
<TD>
<P>����������� �������� ������������� ����������� ������� (��������), �������</P></TD>
<TD>
<P align=right>11,0</P></TD>
<TD>
<P align=right>11,0</P></TD></TR>
<TR>
<TD>
<P align=center>4</P></TD>
<TD>
<P>��������� ������ �� ��������� ������ �� ������, ������� � ������������ � ��� �������������, ���. ���.</P></TD>
<TD>
<P align=right>37 132</P></TD>
<TD>
<P align=right>37 442</P></TD></TR>
<TR>
<TD>
<P align=center>5</P></TD>
<TD>
<P>���������� �������������� ������ �� ��������� ������ �� ������, ������� � ������������ � ��� �������������, ���. ���.</P></TD>
<TD>
<P align=right>37 132</P></TD>
<TD>
<P align=right>37 442</P></TD></TR>
<TR>
<TD>
<P align=center>6</P></TD>
<TD>
<P>��������� ������ �� ��������� ������, ���. ���.</P></TD>
<TD>
<P align=right>1 309</P></TD>
<TD>
<P align=right>3 925</P></TD></TR>
<TR>
<TD>
<P align=center>7</P></TD>
<TD>
<P>���������� �������������� ������ �� ��������� ������, ���. ���.</P></TD>
<TD>
<P align=right>1 309</P></TD>
<TD>
<P align=right>3 925</P></TD></TR></TBODY></TABLE>
<TABLE cellSpacing=0 cellPadding=0 width="95%" border=0>
<TBODY>
<TR>
<TD height=0>
<P class=top>���. ������������ ��������� ��������� �����������</P></TD>
<TD>
<P align=right><STRONG>������� �.�.</STRONG></P></TD></TR>
<TR>
<TD height=0>
<P class=top>������� ��������� ��������� �����������</P></TD>
<TD>
<P align=right><STRONG>��������� �.�.</STRONG></P></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>


</body>
</html>

