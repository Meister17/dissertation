<!--
ID:37106291
TITLE:�� ������ �������� �� ������������� ���������� ��������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:24
ISSUE_DATE:2006-12-31
RECORD_DATE:2006-12-26


-->
<html>
<head>
<title>
37106291-�� ������ �������� �� ������������� ���������� ��������
</title>
</head>
<body >


<P align=justify>� ����� � ������������� ���������� ����������� �������, ������������ ���������� ������������, � ����������� ����� ����� ������, �������� ������������� ���������� ��� � ������� �������, ���� ������ ������� ��� ����������� �������� �� ������������� ���������� �������� � ��������� ������ � ���:</P>
<TABLE cellSpacing=2 cellPadding=7 width=610 border=1>
<TBODY>
<TR>
<TD vAlign=top width="8%"><FONT size=3>
<P align=center>&#8470;<BR>�/�</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=3>
<P align=center>����, ���</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P align=center>�����</FONT></P></TD>
<TD vAlign=top width="21%"><FONT size=3>
<P align=center>����<BR>�����������</FONT></P></TD>
<TD vAlign=top width="21%"><FONT size=3>
<P align=center>������������-<BR>��� �����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="8%"><FONT size=3>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=3>
<P>����-����� ����</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P align=center>������</FONT></P></TD>
<TD vAlign=top width="21%"><FONT size=3>
<P align=center>11.09.1991</FONT></P></TD>
<TD vAlign=top width="21%"><FONT size=3>
<P align=center>1567</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="8%"><FONT size=3>
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=3>
<P>��������</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P align=center>������</FONT></P></TD>
<TD vAlign=top width="21%"><FONT size=3>
<P align=center>31.01.1990</FONT></P></TD>
<TD vAlign=top width="21%"><FONT size=3>
<P align=center>235</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="8%"><FONT size=3>
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=3>
<P>��-����� (���)</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P align=center>������</FONT></P></TD>
<TD vAlign=top width="21%"><FONT size=3>
<P align=center>07.02.2006</FONT></P></TD>
<TD vAlign=top width="21%"><FONT size=3>
<P align=center>3462-�</FONT></P></TD></TR></TBODY></TABLE><I>
<P align=right>�����-������ ������������<BR>������� � ������������ ������<BR>����� ������ �� 05, 14.12.2006</P></I>


</body>
</html>

