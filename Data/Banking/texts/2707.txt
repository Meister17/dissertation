<!--
ID:38501819
TITLE:$120 ��������� &#151; �� &#171;��������&#187;?
SOURCE_ID:11528
SOURCE_NAME:���. ����� � ������� ���
AUTHOR:������� ��������, &#171;������� ����� ������&#187; 
NUMBER:10
ISSUE_DATE:2007-10-31
RECORD_DATE:2007-12-04


-->
<html>
<head>
<title>
38501819-$120 ��������� &#151; �� &#171;��������&#187;?
</title>
</head>
<body >


<H1><FONT size=3>��� �������� �� ���������� �����������, ������� �������� ��������, ������������ ��������� ������� ������������, ����� ������, ��� ����� �� �������� ������ ������� �����, ��� �������. ����������� ����������� (� � ������� ����� ����� � ����) �������� �������� � ��������, ������-������ � �����������, ����������� ����� ������������������� � ������� ������������, �������� ������� ��������� �����������, ����������� � �������� ���������. </FONT></H1>
<P align=justify><FONT>� ����� �� ������� �� ������� ��������, ���� ��������������� ����������� &#8212; ���� �� ������, ������� ���������� �������� ������. ��� ������ �������� �, ��� ����������, ��� ��-�������. </FONT>
<H3><FONT size=3>� ������� � ��������</FONT></H3>
<P align=justify><FONT>������ �����, ����� ������� � ���� �������. ��� ��� �������, ��� �������� ��-�������, ������ �� ������ ���������, �� ����� � ������������. &#171;��� ���������� ������� ����� ������������ � ����������� �������� ����� &#8212; ������ ����� ��������� ���������?&#187; &#8212; ���������� ����. &#171;��� ����� ����������� ��������, ��������� �������������� ��������, � ������ ��������������� �������������� � ��� � ��� ���� ����&#187;, &#8212; ���������� ������. ��� �� �����, ����� ����� ����������� �����. </FONT>
<P align=justify><FONT>������ �� ������������ ������� ���������� �� ����������� �ommunication. ������� �� (������ ��� ����������� ��� �����) ����� ���� � ������, � ����������, � ���� &#171;��� ����&#187; &#8212; ������������. ��� ������ &#171;����������&#187; ��� &#171;�������&#187; &#8212; ����� ��������� � ��������� �����. �������� ����� ����� ���, ����� ��������� ���� ���� ������������� �������. &#171;� ����� �� ����&#187; &#8212; ��� � ������� ����� ����� ������������: �� �������� ������� ��� � ��������-�����������. </FONT>
<P align=justify><FONT>� �� ��� &#8212; ������������, ������ �� ������ �������������, �����������, ������ � �������������. �� �� ������ ������� ��������. ��� ����� ���������� ��������� �������� � ����� ����� ������� ����, ��� � ������� ����� �������� ������ � �������, ����������� � ����� ���� ������������: ��������� �� ������� � ����� �� �����������, ���� ������� ��������� ��� ���������� ���������� ���� � ����� �����. </FONT>
<P align=justify><FONT>�������, ������, � ������� �����������. ������� ����������������� ������, ��� ����� �� ������������ �������� ������������� ���������, � ����� ���� ������������ <EM>������� ���������� ����� ������������. </EM>� ������, ������� �������� �� ����� ������� &#8212; ��� ������, ��� �������� ��<STRONG> � ���-��.</STRONG> �, ����������, �������� ������� � ���-���������, � ��������� �������� ������, � ��������� ����������, ����������� � ���������. ����������, ����������� ��������� � ����������� ���� ����� �� ��������. ��� � ���������� ����, ��� �������� ������ �� ���� �������� ������� �� ���������� �� �������� ������� ������������ �������. </FONT>
<P align=justify><FONT>����� �������� ����������������� ��������, ������� ������� <STRONG>��������� ������������</STRONG> ���������������� ������� � ������� ����������� �����, � �������� � ������������ ��� ������������ ������ <STRONG>������������������� �������������.</STRONG> </FONT>
<P align=justify><FONT>�����, �������� ����� &#171;�����������&#187; ������� ���� &#8212; ��� ����������� ���� ��� ����� ����������� ���������� ��������� ������ � ����� �����. ����� �������, ������, �������� � ������� �������, ������� ��������������� <STRONG>��������������� �����������.</STRONG> </FONT>
<P align=justify><FONT></FONT>
<H3><FONT size=3>������������� ����� �������</FONT></H3>
<P align=justify><FONT>������� ������� � ���������� ������� ���������� ���������, �� ��������, ��� ������� ����������� �������� ����������� ����������� �������� ����� � ���-���������� �������� �������� ��� ����������� ������� �����. �������� ������������ ���������� ������ ��������� �������������� ��� ����� �����������, ����������� ������� �������� �������� ���� ������� ������: </FONT>
<UL>
<LI><FONT>������������������ ������; </FONT>
<LI><FONT>������ ��������� � ����� ��������; </FONT>
<LI><FONT>����������� ����������� ������ ������������ �������������; </FONT>
<LI><FONT>����������� �������������� � ���������; </FONT>
<LI><FONT>����������� �������������� � ������� �����. </FONT></LI></UL>
<P align=justify><FONT>��� &#8212; &#171;���������� � �����&#187;, ������� ������ ������������� ���������, ���������� ���������� ���������. ����� ��������������� ��, �� ������ ����� � ��������� ���������� �������� � ������: ����������������, ������������������� � &#8212; ������������������, �� �������, ����������, � ��������� ����������� ��������������� �����������. </FONT>
<P align=justify><FONT>�������� ��������: ��� �� ���� ������ �������������� ������������� ������� ����������� � �������. ����������� ������ �����������, �������� � ���������, ������� � ������������ � ������� &#8212; �� ��� ��������, ������ �����, �� ����������� ������� ���������. ����� ������� ����������� ��� ������ ������ � ������. ������ ��� ������������ ������� � ����� ��� ���������� �� �����. ��� ������� ������ ���������� ���, ������ ����������� ��������. </FONT>
<H3><FONT size=3>������ ������ � ������</FONT></H3>
<P align=justify><FONT>������������ ���������� ����������, ������ ���������������� � ������� ���������������� ��������� � ������������� ��������. ��� ���������� � ������ ����� �� ������� &#8212; ����� �������� �������. �� �� �� ��� ������&#8230; ������ ��� ������, ����� ����� �������, ������� � ������ ����������� ���������. � ��������������� ���� &#171;������� &#8212; �������&#187; ��������� ������������ ������� ������������� ������������. � � ����� �� ��������: ������ ����������, �� ������������ ��������, �������������, ���������� � �����������&#8230; ������ �� ��� �����: ����� ������� �����, �������� ���� �������� ����� ��� ��������������, ���� � ����������. ���������� �������� &#171;������������&#187;: �� ���������� �������� (�������, ��������, ����) &#8212; � �� ������. ��� ����� ��� �� ��������� ���� ����� ����� ��, � ������� ������������� �������, ����� � ���. � ���� ��� � ��� ��������� � ������� ���������, �� ������� ���� �������. ��� ��������� ������&#8230; </FONT>
<P align=justify><FONT>������� ������� � � ���������� ������������ �� ����� ��������. ������ ��������� �����������, ������ ��������� ��� �����-���� ����������, ���������� <STRONG>����������� � ����������.</STRONG> ����� ������������ ������� �� ����������� ���������� ��� �����������, � �������� &#8212; ��������. ������ ��� ����������� ������ ����� ����. ���� ������� ������������� � ���� ���������, ���������. ������, �������. </FONT>
<P align=justify><FONT>��, � ���� ��� ����������� ������� �������? ����� �� ������� ����, ��� ����������� ��������� ���� &#171;������� &#8212; �������&#187;? ������... </FONT>
<H3><FONT size=3>��� ����� ������� ���������� ���������?</FONT></H3>
<P align=justify><FONT>����, � �����������, ��������������� �������� �������� ������������ ����������� ������ ����������� ������� � �������, ���������, ������������, ������������. ����� ������� &#8212; ��������� � ������� �������. </FONT>
<P align=justify><FONT>�������, ������������, ��� ������ �������� ������� ���� ������������� ������������, �������� ������������ �������� ������������ ���������������� ���������, ����� ������� ���� ������� ��� ������������. ������, ���� ����� ��� ����������� ������������ �����������, � �� ������� � &#171;������� �������&#187;. ��, ��� ��������, ����� ������ ������� ��������. </FONT>
<P align=justify><FONT>����������� ������� �� �������� ��������������� �����������, ���� �� �������������������, ��������� ������������, ��������� ����������� ��� ������� ����������, ������� �� ���� ������, ������ ���������� ������������. � ����� �������, �������� � �������� ����������� �������, �����, ������� � �����. � ������ &#8212; ������������ ���� �����������, ����������� � ����� ������ ���������. ��� �������� ��������� ����������� &#8212; ������� ��������� � ������ ������� ����� ���������. ������� ����� �� ������� �������� ����� � ��������� ������ � �������� �����, ��� ����� ����� ����. </FONT>
<P align=justify><FONT>��, ��� ������ �������� &#171;��� ���� ����������&#187;, ������������ �������� ���� � ������ ��������. �, ���������� ����� ���� �� ���� � ����������� � �������, ���������� ��� �������� ���������. �� ��������� �� ������������������� ������������ ��������� ���������� ���� ������� � ������� ��������, ���� ������ �������. ���� ���� ��������� � ������ ��������������� ����� ����� � ��������������� ����� � �������, � ������ ������ &#8212; �������� ��� �� ��������, ������� ����� ����� ������ � ������ ��������� ������ � ���� �����������. </FONT>
<H3><FONT size=3>� ���� �� �� �����&#8230;</FONT></H3>
<P align=justify><FONT>����� ��� ����������: &#171;��� ������ � ��� �������, � ��� &#8212; ����� ���-��� ��� ����� ��������� � �������? ��� ��, ���, ��������, ������� �?&#187;. ���, ������, ������� �� ������� ����� �� ������ ���� ���������� ����� ��������� � ����������� ����������� �������? </FONT>
<P align=justify><FONT>���, �������� ����� �� �� ������� &#8212; �� ����� ��� ���, �� �� ��������� ���� ������������ ������� �� �����. ����� ����� �� ������� ����� � ������? </FONT>
<P align=justify><FONT>����� �������, � �� ������ ��� � ���� ����� ��������� ������-������. ����� �� ������� ������ � ���, ��������, ���� ������� �� ������ ������� ������ ������ ����������� ��� ��������� �����, ��� � ����� ��. ������ ����� ���������� �������� ��� ��� �������������� ��������� � ���������� ������, ����� �� ��������� ���� � �������. ���� �� �� ����, ��� ����� ������� ������� ���� ����� ����������� � ���������� ������, �� ���� �� ��������� �������� � ��������� �������� ������������ �� ��������, ��������� ����������� ����� &#8212; � ��� �����&#8230; � ����� �������, �� ���� ������ �� ������� �� ��� ���� ������ ��� �����, �� ���� ���������� ������� ��������. �� ��������� &#8212; � ����� �����. </FONT>
<H3><FONT size=3>� �����?</FONT></H3>
<P align=justify><FONT>��� ���� ���������� ������, ���� � ��� ��������, ������� �������, ��� ��, ��� ����� &#8212; �������� ��������� �� ����������� ����� �� �������. �� ���������� ������������, �� �������������� ��������� ��� �� ����������� ������. ������������ ������������ ���������� �������� �� ����������� ����� � ������� ������� �����, ���������� �� �����. ������� ��������� ����������, ��� �������� �������� ����� &#8212; ����� &#171;�������&#187;. �������, ����� �� ����� ���� &#8212; ���� ������ � ���, ��������� ��� �������� ����� ������� &#171;������� � ������������ �����&#187;. </FONT>
<P align=justify><FONT>�� ��������� ���� ������ � ������� ���������� ��������� � ������ �� ������� ������ ������� ��������, ��� ��� ����� ��������� � ��������� ��������������� ����������� �����������. � � �������� ������ �� �������� ������ &#171;�����?&#187; ������� ���� ������������� ������ �� ����������: &#171;������ � �����, ��� ������������� ������-�����������, ��������, �������� &#8212; �� ��� ����������. �������� � ��������� ����� ����� � �������. � ����� ������ �����, ��� ���� �������, ��� ���������� ����� ����������� ����� � ������ ���������� $100&#8211;120 ���������. ���� �� ���� ������ ������� 120 ��������� �������� &#8212; ������ �� ��������&#8230;&#187;. </FONT>
<P align=justify><FONT><EM>� ��������� ������� �� �������� ���������� �������� � �������� ��������������� �����������. ��� ��, ��� ��� ����� ������� ��������� �������, ����������� ���������� � &#171;������� ����� ������&#187;.</EM> </FONT></P>


</body>
</html>

