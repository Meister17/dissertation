<!--
ID:36110554
TITLE:�������������� ���������� ������ �� �������� � �������� ������ � ������� ������� SWIFTNet Accord
SOURCE_ID:11077
SOURCE_NAME:������������� ���������� ��������
AUTHOR:�.�. ��������� ��������
NUMBER:1
ISSUE_DATE:2006-02-28
RECORD_DATE:2006-04-17


-->
<html>
<head>
<title>
36110554-�������������� ���������� ������ �� �������� � �������� ������ � ������� ������� SWIFTNet Accord
</title>
</head>
<body >


<DIV class=line>� ��������� �� SWIFTNet &#8212; ����� ���������� SWIFT, ���������� �� ������������� ��������� IP, ��������������� ������� ����������� ���������� ������������ ��������� ������ ����������� ��������������, &#8212; � ������������� ��������� ����������� ������� �� ���� ������� SWIFT. ����� �������, ��� SWIFTNet FileAct, SWIFTNet Cash Reporting, SWIFTNet Exceptions and Investigations, SWIFTNet Accord, ��� ����������� ������������ ��������� �����������, ������������ �������������� SWIFT � ����� ������, ��� ����� ����� �� &#171;������&#187;, � ���������� �� ���� �������������. ����� �������������� ���������� ��������� �������������� SWIFT � �������� �����������, Road Show1, ���������� ���������� � ������������������ ��������. � ������������ ��������� ������ �������� ��������������� ������ SWIFTNet Accord, � ����� ������������ ������� ��� ����������. ��������, ������ ���������� ������� ���������� ������������� ������� ��� ������������ ����� �������.</DIV>
<DIV class=top30 style="MARGIN-LEFT: 17px">
<P align=justify>������� ������ ������� SWIFTNet Accord ���������: ����� ���������� ������ �� �������� ��� �������� ����� ��� ������ � ������������ ������� ������������ ������ ������, ��������� ��������������� ������������� �� ���� SWIFT. 
<P align=justify>���� �����-���� �� ������ ��� ��� ������� �������� �������������� ������� SWIFTNet Accord, ������ ��������� ���������� �������� SWIFT � ����������� ������ Accord. ���������� ���������� ���������� ���������� ������������� � ������ ��������� �������. 
<P align=justify>��������� ������������� ����� ������� ���������� � ������ ������� �������� ���������� ������ ������������ �� ����������� �������������. ������������ ����� ����� ����������� ���������� ���� ����������� ������� ��� �������������� ��� �������� ����� � ������������ ���������. �������� ����� ���� ����������� &#171;�������������� ��������&#187; � ��������� ������, ���� ������� ����������� ������� ����������. 
<P align=justify>����� �������� ������ ������� ��������� �������������� ��� �������� ������������� ������� SWIFTNet Accord ����� ������� ��������� ��������� �������������, ���������� ����������� ����������. 
<H4>������������ SWIFTNet Accord</H4>
<H5>���������� ������</H5>
<P align=justify>���������� ������������ ������ �������� �������� �������� ����������� ���-����� ��� ������������ �������� �� �������� � �������� ������ � ������ �����������. �������� �� ������ SWIFTNet Accord ���� ����������� ����������� ��������� ������������ �����, ��� ��� � ��� ��������� ������� ����� ��������� ����������� ������� ����������, ��������������� ����������� ���������� � ��������������� �������� ����������� ����������. 
<P align=justify>������ Accord ��������� ��� �������������, � ����� ������� ����������� ���������� � ������� 7 ����, ����� ���� ��� ���������� ���������� � ����� (�� ������ ���������� ����������� &#8212; ��� �� 10 ���). 
<P align=justify>������ ������ ��� ���������� ����� �������, ����� ������� ��������� �����- ������ ��������� ������������� ���� ��� ����� ������������ ������� �������. ��������� ���� ��� ����������� ������� �������� ����������� ����������� ��������, ��������� �������� ����������� �������������. 
<H5>���������� �������� </H5>
<P align=justify>�������� �� ������ SWIFTNet Accord �������� ������� ������� �� ����������� ���������������� ���������� ������� ����������, ��������� ���������� ��������� ������ ���������� � �������� ������ ���������, ������� � ������ ���������� ������� ���������� ��������� ���������. ��������� ������������� ����������� ������� ���������� �������� ��������� ������ ������� ������� &#8212; �� ���������� �������� ������������� � ����� �� ���������� ����� � ����. 
<H5>��������� �������������</H5>
<P align=justify>������ ���� ��� ���� �������� �� �������� ������ � �������� � ������������; ������� ����� ������������ SWIFTNet Accord ������������ ����� ����������� ��������� ������������, ���������� ������� ����������� �������, ������� ��������� ����������� ��������� ����� ���������� � �������� ������� �������������. 
<P align=justify>� ����� ������ ������ ������������� ������ ������� ���������� �������� ����� �������� �� ��������� � �������������� ���������� ������������ ������. SWIFTNet Accord ������������� ������ � ������� ���� ��� ���� �������� �� �������� ������ � �������� � ������������ ���������� ��: 
<P align=justify>&#8226; ������ �������� &#8212; ������� � �������������� ������������� ��������, ������� �������, ������� � ������ CLS1. 
<P align=justify>&#8226; ����������� ����������� &#8212; � ������� SWIFTNet Accord �������� ���������� ������������� �� ������� �� �������� � �������� ������ � �� ������� � ������������; 
<P align=justify>&#8226; ������������ &#8212; ��� ������������� SWIFTNet Accord �� ���������, ����� ���� ����������� ���� ����� ��������� �� ������ ������. ������ SWIFTNet 
<P align=justify>Accord ������������� � ������������ ������������� ����������� ����������� ��� ��������� ������, ������������� ������� ��������� �� �������, �������� �� ������� SWIFT, &#8212; �� �����, ����� ��� �������. 
<H5>�������������� ��������� �������� (STP)</H5>
<P align=justify>���������� ����������� ����� ����������� �������� ������� �������� ��������� ��������� ������������� ������������ ���������� API, ����������� �� ������� XML1, ��� �������� ����������� ���������� ��� ������ ��������������� ���������� �� ������� ����������. 
<H5>�������� ������� � ������� (����� GUI ��� API)</H5>
<P align=justify>��� ����������� � ������ SWIFTNet Accord ���������� ���������� ����������� ��������� ������������ (GUI) SWIFTNet Accord �� ����� ��� ���������� SWIFTAlliance Web-Station2. ������ GUI, ������� ������������� ���������� ����������� ������ �� ������� SWIFTNet Accord, ���������� ����������� ��������� &#8212; ����������� ��������� ���������� (API), � ������� �������� ������������ ���������� ����������� ��� �������� &#8212; ������������ ������������ ����������� ������ ������� ���������� ��� ��������� ������� ������� � ������ ��������� ������� � ������� SWIFTNet Accord. 
<H5>���������</H5>
<P align=justify>� ������� ��������� ������������� SWIFT ����� �������� ������������ ������������ 24 ���� � ����� 7 ���� � ������ �� ������ ������ ����, ������� �������. � ������ ������������������ ����� ��������� ���������� ����� �������� � ������-�������� SWIFT �������� Alliance Factors. 
<P align=justify>
<DIV align=center>* * *</DIV>
<P align=justify>������ ��������� ������������, ������������ � ���� SWIFTNet Accord, ���� Banque de France &#8212; ����������� ���� �������, �������� ������� Accord ��� ��-������� ���. 
<P align=justify>������� � ��������� ������� ������� ��������� ������������� ������ ������� ������������ ��������� MT 3983. ��������������� ������������ �������, ������������ ������ � ������ ��������� �������, � �������� ������������ ��������� �� ������� ������� ������ �������, Banque de France ���� ������ ������������� ������� SWIFTNet Accord. ������ 18 ������� � ����� �� ���� ������� ��������� �� ������ ����������� ��������. ������������������ ������� � �������� ����� ��� ����������� ���������. �� ������ ������ ��������, ������������ ���-������ Banque de France, &#171;SWIFTNet Accord ���������� ������� �������� ��� ��������� ������������� ������ ������������&#187;. 
<P align=justify>��� ������ � �������� SWIFTNet Accord ���� ������ ������� ������������ � �������������� ����� dial-up � �������� �������� � ��������� �������, ��� ������������ ����������� �������� ������ �������. 
<P align=justify>&#171;���������� ��� ����� ������ ���� ��������� SWIFTNet Accord �� ������� ����������� ��������� ���������� �� ������������� ������ ������� �������� ������������� �� �������� � �������� ������, ��� �� ����� �� ����� ��������� � ��� ��� ������ �������������&#187;, &#8212; ������� ������ �������. 
<P align=justify>������������� SWIFTNet Accord ������� � ���������� � ������ ���-�����. &#171;�������, ������ � ����� ������ �������� ����� ����������� ������ � ������ ��������� ������� � on-line ��������� � ����������� ������������, ������� ��������� ����������� ����������� �� ��������� � �������������� � ���, &#8212; �������� ����������� ���-������ Banque de France. &#8212; �� ����� ����� ������������ �������� � �����������, �������� �� ����������� �������� �� ���� �������&#187;. 
<P align=justify>�� ����� ��������� �� �������� �������� � ���������� �������� � ���� ����������� ������������� ������, �������� � ����� ������������ � ���������: &#171;�����, ��� ������������� ������ �������, ��������� ��������� ��� � �����, � ������� �� �����, ��� ��� ��������� �� ����-���� � ���&#187;. 
<P align=justify>��� ���������� ��������� � ���������� ���������� �� ������� ����� ��� ����������� ������ ��������, ����� ���� ���� � �������� ���������, ��� ���� ���������� ������ � ���� ������������� ���������. 
<P align=justify>������������� SWIFTNet Accord ����� ������������ ������������ � ����� ������ ������, ��� ��� ���� ��� �������� ������ ���������� �������� �������� ������ ���� ���������� � ������������ �� ���������� �������� ��������. 
<P align=justify>���� ����� ����������, ��� ���������� ������, ����������� Banque de France � �������������� SWIFTNet Accord �� �������� ���������. ��� ��������������� � ���, ��� ������������ ����������� ��� ��� ���� ����������� �� ������ �� ���������������� �������. 
<P align=justify>������ ������������ ���� ������������� � �������� �����������, ��� ����������� � ��������� ����������� ����� � ������, ��� ��������� �� � ������� ������������� � ���������. 
<P align=justify>����� �� ������ ��������� Banque de France ������� SWIFTNet Accord ��������� �������� ��� �������������, ������� ��������� ����� ����������� ������ ��������� � ������ ���. ��� ���������� � ��������� ��� ���������� visual check, �������������� �������� ����������. 
<P align=justify>���� ����� ������������� � ��������� �������� ������������� ����������� ��������� ����������� �� ���� �������, ������������ ��������� �������������� ������. ������ ��� ��������� SWIFTNet 
<P align=justify>Accord ������ ��������� � ������� (��������, �������� � ����������, ������������� ���������), ��� ����� ��������� ��� ���������� ������������ ������ � �������� ��������� ���������� �������� ����������. 
<P align=justify>� ���������� ������ ������� ���������: &#171;�����������, ��� ����� 10 ��� �� ��������� ����� � ������, ��� SWIFTNet Accord ��� ����������� � ���������� �����, ��������������� � ������ ��������� �������, � ������ ������ ��� �������� ������ �������� �������������� ��� ������&#187;. 
<H4>����������</H4>
<P align=justify>� ����� ������ ������ SWIFTNet Accord ���������� � ����� ������ ��� ���������� �����. 
<P align=justify>� ��� ����� SWIFTNet Accord ��� ���������� ����� 2005 ���� �, �� ������ ���������� ������ ���������� �������� �� �������� ������ � ������ ������ ����� ��� ����� ������ �����������, ����� ���� �������� ������������: �������������� ������� �������� �������� � ��������� ��������� ��������� ��������� �������� ������������ ���� &#8212; ��� ���������� ���� ������ � ��������������� ���������. 
<P align=justify>� ������������ � ������������, �������������� � ������������ �������� �����, SWIFTNet Accord ����� ������ �������������� ������, ��� ��������� ������������ ������� �������������� � ������������� �������. ���, � ������� ������� �������������� ���� ����������� ������ ��� ����� ������� ��������, ������� �� ����� ���� �������� �������������. ������������� ����� ����� �����������������, �� ������ �������� � � ������ ������������� ������������ �������� ������ �������� ���������. 
<P align=justify>����� ������ ����������� ������� ����� �������� ��������� �������� ��������� ����������, �������� �������, ���������� ��������� ����������������, �������� ���������� ��������������� � ������ �����������, ����������� ������-��������� �������� ��������. 
<P align=justify>� ����� ��� ���-����� ����� ������������� SWIFTNet Accord ������� � ������. �������� ��������� �������, ����������, ������� �� ����� ��������� � ��������������� � �������� ��������� ���� �� �������������.</P></DIV>


</body>
</html>

