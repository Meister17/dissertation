<!--
ID:35382700
TITLE:��� &#171;��������������� ����&#187;
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:
NUMBER:6
ISSUE_DATE:2005-06-30
RECORD_DATE:2005-10-07


-->
<html>
<head>
<title>
35382700-��� &#171;��������������� ����&#187;
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgcolor1>
<TD height=0>
<P class=Rubrika align=center>��� &#171;��������������� ����&#187;</P></TD></TR>
<TR class=bgwhite>
<TD></TD></TR>
<TR class=bodycolor>
<TD height=0>
<P class=Rubrika align=center>&#171;��������������� ����&#187; (�������� ����������� ��������) 
<P class=&#171;Main&#187; align=center>��������������� ����� 3277, ���-��� 044525442 
<P class=&#171;Main&#187; align=center>�������� �����: 107045, �/� 47, ������, ���� ���., 2</P></TD></TR>
<TR class=bgwhite>
<TD align=middle width=0>
<P class=Rubrika>������ �� 1 ������ 2005 ����</P>
<TABLE cellSpacing=0 cellPadding=0 width="98%" borderColorLight=#cccccc border=2>
<TBODY>
<TR>
<TD>
<P align=center><STRONG>����� �/�</STRONG></P></TD>
<TD>
<P align=center><STRONG>������������ ������</STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� �������� ����</STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� ������ ��������� ����</STRONG></P></TD></TR>
<TR>
<TD width=63 height=0>
<P align=center><STRONG>1</STRONG></P></TD>
<TD width=300>
<P align=center><STRONG>2</STRONG></P></TD>
<TD width=93>
<P align=center><STRONG>3</STRONG></P></TD>
<TD width=110>
<P align=center><STRONG>4</STRONG></P></TD></TR>
<TR>
<TD colSpan=4 height=0>
<P align=center><B>������</B></P></TD></TR>
<TR>
<TD>
<P>1.</P></TD>
<TD>
<P>�������� ��������</P></TD>
<TD>
<P align=right>23887</P></TD>
<TD>
<P align=right>60205</P></TD></TR>
<TR>
<TD>
<P>2.</P></TD>
<TD>
<P>�������� ��������� ����������� � ����������� ����� ���������� ���������</P></TD>
<TD>
<P align=right>95129</P></TD>
<TD>
<P align=right>107309</P></TD></TR>
<TR>
<TD>
<P>2.1.</P></TD>
<TD>
<P>������������ �������</P></TD>
<TD>
<P align=right>24325</P></TD>
<TD>
<P align=right>50051|</P></TD></TR>
<TR>
<TD>
<P>3.</P></TD>
<TD>
<P>�������� � ��������� ������������</P></TD>
<TD>
<P align=right>61673</P></TD>
<TD>
<P align=right>11522</P></TD></TR>
<TR>
<TD>
<P>4.</P></TD>
<TD>
<P>������ �������� � �������� ������ ������</P></TD>
<TD>
<P align=right>56230</P></TD>
<TD>
<P align=right>11544</P></TD></TR>
<TR>
<TD>
<P>5.</P></TD>
<TD>
<P>������ ������� �������������</P></TD>
<TD>
<P align=right>662414</P></TD>
<TD>
<P align=right>706833</P></TD></TR>
<TR>
<TD>
<P>6.</P></TD>
<TD>
<P>������ �������� � �������������� ������ ������, ������������ �� ���������</P></TD>
<TD>
<P align=right>62024</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>7.</P></TD>
<TD>
<P>������ �������� � ������ ������, ��������� � ������� ��� �������</P></TD>
<TD>
<P align=right>495</P></TD>
<TD>
<P align=right>500</P></TD></TR>
<TR>
<TD>
<P>8.</P></TD>
<TD>
<P>�������� ��������, �������������� ������ � ������������ ������</P></TD>
<TD>
<P align=right>2173</P></TD>
<TD>
<P align=right>2049</P></TD></TR>
<TR>
<TD>
<P>9.</P></TD>
<TD>
<P>���������� �� ��������� ���������</P></TD>
<TD>
<P align=right>169</P></TD>
<TD>
<P align=right>1886</P></TD></TR>
<TR>
<TD>
<P>10.</P></TD>
<TD>
<P>������ ������</P></TD>
<TD>
<P align=right>676</P></TD>
<TD>
<P align=right>179055</P></TD></TR>
<TR>
<TD>
<P>11.</P></TD>
<TD>
<P>����� �������</P></TD>
<TD>
<P align=right>964870</P></TD>
<TD>
<P align=right>1080903</P></TD></TR>
<TR align=middle>
<TD colSpan=4>
<P><B>II. �������</B></P></TD></TR>
<TR>
<TD>
<P>12.</P></TD>
<TD>
<P>������� ������������ ����� ���������� ���������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>13.</P></TD>
<TD>
<P>�������� ��������� �����������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>14.</P></TD>
<TD>
<P>�������� �������� (����������� �����������)</P></TD>
<TD>
<P align=right>600804</P></TD>
<TD>
<P align=right>624221</P></TD></TR>
<TR>
<TD>
<P>14.1.</P></TD>
<TD>
<P>������ ���������� ���</P></TD>
<TD>
<P align=right>276165</P></TD>
<TD>
<P align=right>313421</P></TD></TR>
<TR>
<TD>
<P>15.</P></TD>
<TD>
<P>���������� �������� �������������</P></TD>
<TD>
<P align=right>144562</P></TD>
<TD>
<P align=right>242164</P></TD></TR>
<TR>
<TD>
<P>16.</P></TD>
<TD>
<P>������������� �� ������ ���������</P></TD>
<TD>
<P align=right>7691</P></TD>
<TD>
<P align=right>9356</P></TD></TR>
<TR>
<TD>
<P>17.</P></TD>
<TD>
<P>������ �������������</P></TD>
<TD>
<P align=right>1225</P></TD>
<TD>
<P align=right>512</P></TD></TR>
<TR>
<TD>
<P>18.</P></TD>
<TD>
<P>������� �� ��������� ������ �� �������� �������������� ���������� ���������, ������ ��������� ������� � �� ��������� � ����������� �������� ���</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>19.</P></TD>
<TD>
<P>����� ������������</P></TD>
<TD>
<P align=right>754282</P></TD>
<TD>
<P align=right>876253</P></TD></TR>
<TR align=middle>
<TD colSpan=4>
<P class=bold><STRONG>III. ��������� ����������� �������</STRONG></P></TD></TR>
<TR>
<TD>
<P>20.</P></TD>
<TD>
<P>�������� ���������� (����������)</P></TD>
<TD>
<P align=right>145092</P></TD>
<TD>
<P align=right>145092</P></TD></TR>
<TR>
<TD>
<P>20.1.</P></TD>
<TD>
<P>������������������ ������������ ����� � ����</P></TD>
<TD>
<P align=right>145092</P></TD>
<TD>
<P align=right>145092</P></TD></TR>
<TR>
<TD>
<P>20.2.</P></TD>
<TD>
<P>������������������ ����������������� �����</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>20.3.</P></TD>
<TD>
<P>�������������������� �������� ������� ������������� ��������� �����������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>21.</P></TD>
<TD>
<P>����������� �����, ����������� � ����������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>22.</P></TD>
<TD>
<P>����������� �����</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>23.</P></TD>
<TD>
<P>���������� �������� �������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>24.</P></TD>
<TD>
<P>������� ������� �������� � ����������� �������, �������� �� ����������� �������� (�������)</P></TD>
<TD>
<P align=right>9476</P></TD>
<TD>
<P align=right>9263</P></TD></TR>
<TR>
<TD>
<P>25.</P></TD>
<TD>
<P>����� � ���������������� ������� ������� ��� � ������������ ��������� ����������� (������������ ������ ������� ���)</P></TD>
<TD>
<P align=right>65590</P></TD>
<TD>
<P align=right>65058</P></TD></TR>
<TR>
<TD>
<P>26.</P></TD>
<TD>
<P>������� � ������������� (������) �� �������� ������</P></TD>
<TD>
<P align=right>9382</P></TD>
<TD>
<P align=right>3763</P></TD></TR>
<TR>
<TD>
<P>27.</P></TD>
<TD>
<P>����� ���������� ����������� �������</P></TD>
<TD>
<P align=right>210588</P></TD>
<TD>
<P align=right>204650</P></TD></TR>
<TR>
<TD>
<P>28.</P></TD>
<TD>
<P>����� ��������</P></TD>
<TD>
<P align=right>964870</P></TD>
<TD>
<P align=right>1080903</P></TD></TR>
<TR align=middle>
<TD colSpan=4>
<P><B>IV. ������������� �������������</B></P></TD></TR>
<TR>
<TD>
<P>29.</P></TD>
<TD>
<P>����������� ������������� ��������� �����������</P></TD>
<TD>
<P align=right>52306</P></TD>
<TD>
<P align=right>57200</P></TD></TR>
<TR>
<TD>
<P>30.</P></TD>
<TD>
<P>��������, �������� ��������� ������������</P></TD>
<TD>
<P align=right>2080</P></TD>
<TD>
<P align=right>2222</P></TD></TR></TBODY></TABLE>
<P class=top align=justify>��������, ���������� ��������� �� ���� ������� ������� V &#171;����� �������������� ����������&#187;, �� �������������� 
<P>&nbsp;����� � �������� � ������� �� 1 ������ 2005 ����.</P>
<TABLE cellSpacing=0 cellPadding=0 width="98%" borderColorLight=#cccccc border=2>
<TBODY>
<TR>
<TD>
<P align=center><STRONG>����� �/�</STRONG></P></TD>
<TD>
<P align=center><STRONG>������������ ������</STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� �������� ������ </STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� ��������������� ������ �������� ����</STRONG></P></TD></TR>
<TR>
<TD width=63 height=0>
<P align=center><STRONG>1</STRONG></P></TD>
<TD width=229>
<P align=center><STRONG>2</STRONG></P></TD>
<TD width=149>
<P align=center><STRONG>3</STRONG></P></TD>
<TD width=126>
<P align=center><STRONG>4</STRONG></P></TD></TR>
<TR>
<TD colSpan=4 height=0>
<P class=bold align=center><STRONG>�������� ���������� � ����������� ������ ��:</STRONG></P></TD></TR>
<TR>
<TD>
<P>1</P></TD>
<TD>
<P>���������� ������� � ��������� ������������</P></TD>
<TD>
<P align=right>3926</P></TD>
<TD>
<P align=right>2819</P></TD></TR>
<TR>
<TD>
<P>2</P></TD>
<TD>
<P>����, ��������������� �������� (����������� ������������)</P></TD>
<TD>
<P align=right>90214</P></TD>
<TD>
<P align=right>76002</P></TD></TR>
<TR>
<TD>
<P>3</P></TD>
<TD>
<P>�������� ����� �� ���������� ������ (�������)</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>4</P></TD>
<TD>
<P>������ ����� � ������������� �������</P></TD>
<TD>
<P align=right>2748</P></TD>
<TD>
<P align=right>3831</P></TD></TR>
<TR>
<TD>
<P>5</P></TD>
<TD>
<P>������ ����������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>6</P></TD>
<TD>
<P>����� ��������� ���������� � ����������� �������</P></TD>
<TD>
<P align=right>96888</P></TD>
<TD>
<P align=right>82652</P></TD></TR>
<TR vAlign=top align=middle>
<TD colSpan=4>
<P><B>�������� ���������� � ����������� ������� ��:</B></P></TD></TR>
<TR>
<TD>
<P>7</P></TD>
<TD>
<P>������������ ��������� ��������� �����������</P></TD>
<TD>
<P align=right>446</P></TD>
<TD>
<P align=right>4547</P></TD></TR>
<TR>
<TD>
<P>8</P></TD>
<TD>
<P>������������ ��������� �������� (����������� �����������)</P></TD>
<TD>
<P align=right>35750</P></TD>
<TD>
<P align=right>30049</P></TD></TR>
<TR>
<TD>
<P>9</P></TD>
<TD>
<P>���������� �������� ��������������</P></TD>
<TD>
<P align=right>25195</P></TD>
<TD>
<P align=right>16894</P></TD></TR>
<TR>
<TD>
<P>10</P></TD>
<TD>
<P>����� ��������� ���������� � ����������� ��������</P></TD>
<TD>
<P align=right>61391</P></TD>
<TD>
<P align=right>51490</P></TD></TR>
<TR>
<TD>
<P>11</P></TD>
<TD>
<P>������ ���������� � ����������� ������</P></TD>
<TD>
<P align=right>35497</P></TD>
<TD>
<P align=right>31162</P></TD></TR>
<TR>
<TD>
<P>12</P></TD>
<TD>
<P>������ ������ �� �������� � ������� ��������</P></TD>
<TD>
<P align=right>16023</P></TD>
<TD>
<P align=right>18548</P></TD></TR>
<TR>
<TD>
<P>13</P></TD>
<TD>
<P>������ ������ �� �������� � ����������� �������</P></TD>
<TD>
<P align=right>5044</P></TD>
<TD>
<P align=right>4315</P></TD></TR>
<TR>
<TD>
<P>14</P></TD>
<TD>
<P>������ ������ �� �������� � ������������ ��������� � ������� ����������� �������������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>15</P></TD>
<TD>
<P>������ ������ �� ���������� ����������� ������</P></TD>
<TD>
<P align=right>1873</P></TD>
<TD>
<P align=right>-575</P></TD></TR>
<TR>
<TD>
<P>16</P></TD>
<TD>
<P>������������ ������</P></TD>
<TD>
<P align=right>8010</P></TD>
<TD>
<P align=right>5576</P></TD></TR>
<TR>
<TD>
<P>17</P></TD>
<TD>
<P>������������ �������</P></TD>
<TD>
<P align=right>752</P></TD>
<TD>
<P align=right>616</P></TD></TR>
<TR>
<TD>
<P>18</P></TD>
<TD>
<P>������ ������ �� ������� ��������</P></TD>
<TD>
<P align=right>121</P></TD>
<TD>
<P align=right>198</P></TD></TR>
<TR>
<TD>
<P>19</P></TD>
<TD>
<P>������ ������ ������������ ������</P></TD>
<TD>
<P align=right>-454</P></TD>
<TD>
<P align=right>3274</P></TD></TR>
<TR>
<TD>
<P>20</P></TD>
<TD>
<P>���������������-�������������� �������</P></TD>
<TD>
<P align=right>59805</P></TD>
<TD>
<P align=right>50238</P></TD></TR>
<TR>
<TD>
<P>21</P></TD>
<TD>
<P>������� �� ��������� ������</P></TD>
<TD>
<P align=right>5560</P></TD>
<TD>
<P align=right>-2777</P></TD></TR>
<TR>
<TD>
<P>22</P></TD>
<TD>
<P>������� �� ���������������</P></TD>
<TD>
<P align=right>11117</P></TD>
<TD>
<P align=right>8867</P></TD></TR>
<TR>
<TD>
<P>23</P></TD>
<TD>
<P>����������� ������ (������� ����� �� �������)</P></TD>
<TD>
<P align=right>2958</P></TD>
<TD>
<P align=right>5186</P></TD></TR>
<TR>
<TD>
<P>24</P></TD>
<TD>
<P>������� �� �������� ������</P></TD>
<TD>
<P align=right>8159</P></TD>
<TD>
<P align=right>3681</P></TD></TR></TBODY></TABLE>
<P class=Main>���������� �� ������ ������������� ��������,<BR>�������� �������� �� �������� ������������ ����<BR>� ���� ������� �� �� 1 ������ 2005 ����</P>
<TABLE cellSpacing=0 cellPadding=0 width="95%" borderColorLight=#cccccc border=2>
<TBODY>
<TR vAlign=top align=middle>
<TD vAlign=center width=42 height=39>
<P align=center><B>&#8470; �/�</B></P></TD>
<TD vAlign=center width=0>
<P class=bold>������������ ����������</P></TD>
<TD width=88>
<P class=bold align=center><STRONG>������</STRONG> �� �������� ����</P></TD>
<TD width=88>
<P class=bold align=center><STRONG>������ �� ������ ��������� ����</STRONG></P></TD></TR>
<TR>
<TD height=0>
<P align=center><STRONG>1</STRONG></P></TD>
<TD>
<P align=center><STRONG>2</STRONG></P></TD>
<TD>
<P align=center><STRONG>3</STRONG></P></TD>
<TD>
<DIV align=center>
<P><STRONG>4</STRONG></P></DIV></TD></TR>
<TR>
<TD>
<P align=center>1</P></TD>
<TD>
<P>����������� �������� (�������), ���. ���.</P></TD>
<TD>
<P align=right>210882.0</P></TD>
<TD>
<P align=right>211928.0</P></TD></TR>
<TR>
<TD>
<P align=center>2</P></TD>
<TD>
<P>����������� �������� ������������� ����������� ������� (��������), �������</P></TD>
<TD>
<P align=right>28.1</P></TD>
<TD>
<P align=right>23.2</P></TD></TR>
<TR>
<TD>
<P align=center>3</P></TD>
<TD>
<P>����������� �������� ������������� ����������� ������� (��������), �������</P></TD>
<TD>
<P align=right>10.0</P></TD>
<TD>
<P align=right>10.0</P></TD></TR>
<TR>
<TD>
<P align=center>4</P></TD>
<TD>
<P>��������� ������ �� ��������� ������ �� ������, ������� � ������������ � ��� �������������, ���. ���.</P></TD>
<TD>
<P align=right>3037.0</P></TD>
<TD>
<P align=right>8832.0</P></TD></TR>
<TR>
<TD>
<P align=center>5</P></TD>
<TD>
<P>���������� �������������� ������ �� ��������� ������ �� ������, ������� � ������������ � ��� �������������, ���. ���.</P></TD>
<TD>
<P align=right>3037.0</P></TD>
<TD>
<P align=right>8832.0</P></TD></TR>
<TR>
<TD>
<P align=center>6</P></TD>
<TD>
<P>��������� ������ �� ��������� ������, ���. ���.</P></TD>
<TD>
<P align=right>5.0</P></TD>
<TD>
<P align=right>0.0</P></TD></TR>
<TR>
<TD>
<P align=center>7</P></TD>
<TD>
<P>���������� �������������� ������ �� ��������� ������, ���. ���.</P></TD>
<TD>
<P align=right>5.0</P></TD>
<TD>
<P align=right>0.0</P></TD></TR></TBODY></TABLE>
<P class=Main align=left>�� ������ ������, ������� ���������� �������� ���������� �� ���� ������������ ���������� ���������� ��������� ��� &#171;��������������� ����&#187; �� ��������� �� 01.01.2005 � ���������� ���������-������������� ������������ �� ������ � 01.01.2004 �� 31.12.2004 ������������. 
<P class=Main>
<P class=Main>
<P class=Main align=left>&nbsp; 
<TABLE cellSpacing=0 cellPadding=0 width="95%" borderColorLight=#cccccc border=0>
<TBODY>
<TR>
<TD>
<P><STRONG>������ ����������� �����������</STRONG></P></TD>
<TD>
<P></P></TD></TR>
<TR>
<TD>
<P>����� ��������:</P></TD>
<TD>
<P>000547</P></TD></TR>
<TR>
<TD>
<P>��������:</P></TD>
<TD>
<P>��� &#171;��� ���������&#187;</P></TD></TR>
<TR>
<TD>
<P>����� ������� � ������ ��������:</P></TD>
<TD>
<P>123</P></TD></TR>
<TR>
<TD>
<P>���� ������ ��������������� ����������� �����������</P>
<P>�������� �� ������������� ����������� ������������:</P></TD>
<TD>
<P></P>
<P>25.06.02</P></TD></TR>
<TR>
<TD>
<P>���� ������ �������� ��������:</P></TD>
<TD>
<P>25.06.02</P></TD></TR>
<TR>
<TD>
<P>���� ��������� �������� ��������:</P></TD>
<TD>
<P>24.06.07</P></TD></TR>
<TR>
<TD>
<P>������� �����(1)/������������ ��������(2):</P></TD>
<TD>
<P>����������� ����� (1)</P></TD></TR>
<TR>
<TD>
<P>��������
���� ����������� �������� ��������:</P></TD>
<TD>
<P>������������ �������� ���������� ��������� (2)</P></TD></TR>
<TR>
<TD>
<P>��� �������� ��� ������� �����������:</P></TD>
<TD>
<P>1</P></TD></TR>
<TR>
<TD>
<P>��� ������������:</P></TD>
<TD>
<P>��������� ������ �������</P></TD></TR>
<TR>
<TD>
<P>��������(1)/�� �������� ���������������������� ������������ �����������(2):</P></TD>
<TD>
<P>1</P></TD></TR>
<TR>
<TD>
<P>����� ������������� � ��������������� �����������:</P></TD>
<TD>
<P>2057726019426</P></TD></TR>
<TR>
<TD>
<P>���� ������ ������������� � ��������������� �����������:</P></TD>
<TD>
<P>11.03.05</P></TD></TR>
<TR>
<TD>
<P><STRONG>������ ����, ������������ ����� (�������������� ��������)</STRONG></P></TD>
<TD>
<P></P></TD></TR>
<TR>
<TD>
<P>������� ��� ��������:</P></TD>
<TD>
<P>�������� ����� ��������</P></TD></TR>
<TR>
<TD>
<P>���������:</P></TD>
<TD>
<P>���.������������ ���������</P></TD></TR>
<TR>
<TD>
<P>����� ����������������� ���������:</P></TD>
<TD>
<P>� 021430</P></TD></TR>
<TR>
<TD>
<P>���� ������ ����������������� ���������:</P></TD>
<TD>
<P>23.12.04</P></TD></TR>
<TR>
<TD>
<P>���� ��������� �������� ����������������� ���������:</P></TD>
<TD>
<P>31.12.05</P></TD></TR>
<TR>
<TD>
<P>�������� �������������� ���������� ����,</P>
<P>������������ ����� (��������, �����, ����):</P></TD>
<TD>
<P>����������� ������������</P>
<P>&#8470; 91 �� 02.11.2004�.</P></TD></TR></TBODY></TABLE>
<P class=Main>
<TABLE cellSpacing=0 cellPadding=0 width="96%" border=0>
<TBODY>
<TR>
<TD height=0>
<P class=top>������������ ���������</P></TD>
<TD height=0>
<P align=right><STRONG>�������� �.�.</STRONG></P></TD></TR>
<TR>
<TD height=0>
<P class=top>������� ���������</P></TD>
<TD>
<P align=right><STRONG>������� �.�.</STRONG></P></TD></TR></TBODY></TABLE></P></TD></TR></TBODY></TABLE>


</body>
</html>

