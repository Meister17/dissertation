<!--
ID:36860840
TITLE:��� ��� � ������ ���������� �������
SOURCE_ID:3121
SOURCE_NAME:�������� ���������
AUTHOR:�������� �������
NUMBER:9
ISSUE_DATE:2006-09-30
RECORD_DATE:2006-10-27


-->
<html>
<head>
<title>
36860840-��� ��� � ������ ���������� �������
</title>
</head>
<body >


<P align=justify><FONT><STRONG>����� ���������� ������� (Moving Averages, ��) ������������ ����� ������ �������� �������� ���� ��� ���������� �� ������������ ������ �������.</STRONG></FONT></P>
<P align=justify><FONT>������ ���������� ������� (Moving Averages, MA) - ��� �������������� ������ ������� �������� ������������� ������ ����� �� ������������ ������. ������ &#171;����������&#187; ������������� (� ��� �� ����� ���� ���), ��� ������� ���������� ��� ��������. � ����������� �� ����, ����� ��� ������ ����� �� ��������� �����, �� ������� ���� �������� ����� ��� ����.</FONT></P>
<P align=justify><B><FONT>����� ����� ��</FONT></B></P>
<P align=justify><FONT>���������� ������� ������������ �� ����� �����: ������� (simple), ���������������� (exponential), ��������� ����� (time series), ���-���������� (triangular), ���������� (variable) � ���������� (weighted). ����� ���������� ������� �������� ��� ��� �������� (open), ������������ (high) � ����������� (low), ��� �������� (close), ������� ���� (median price), �������� ���� (typical price), � ����� ������ (volume), </FONT><FONT>�������� ������� (open interest) ��� �������� ����������� ����� �����������.</FONT></P>
<P align=justify><FONT>��������� ��������, ������������ �������� ��, �������� ��� (��������), ����������� ����� ��������� ��������� ���������� ����. ��������� �������� ����� ����� ����� ����� ������ �������� ����������, ��� �������� ��� ����� ��� ���������� � ������� �������, ����� ����� ��� ����� &#171;������&#187; ��� ����������. ���� ��� ������������ ����� &#171;�����������&#187; �������� ���������� �� ���� ��������� ��������.</FONT></P>
<P align=justify><FONT>���������� ���������� ���������� �� � ������-��������� ��. ���������� �� ������������� �������������� � ����������� �� ���������� �����. ���������� �� ����������� ���������������� � ��������� ������, ���� ������������� ����� ����������, �, ��������, ��������� ��� ����������������, ���� ������������� ���������. ����������� ������� ������������� ������-��������� ��, ������� �������� ������� ������� �� ����, ������ ��� ������ ���������� ������.</FONT></P>
<P align=justify><B><FONT>��������� ���������� ���</FONT></B></P>
<P align=justify><FONT>������� (��� ��������������) �� �������������� ��� ������� ���� ����������� ������ �� ��������� ������ �������. �� ������ ��������� �����, ��������������� ���� ������� ����. ��� ����������� ����� ������ �������������� ��������� �������� ������� ���������� �������. ������������������ ����� ������������ � �����, ������� ���������� ��� ������� ������� ��������.</FONT></P>
<P align=justify><FONT>���������������� �� ��������������� ������������ ���� (��������) �������� ���� � ����������� � ��������� ���� (����� ����) � ��� ����������� � ����������� � ������ ����� (������ ����). ��� ������������ ���������� ���������������� � ��������� ������� ���. ��� ������� �� ��� �������� ����� ���������� ���, ������� ��� ����������� � ������� ������� ��������� �� ��������� �������� ��� ����� ��� ����������.</FONT></P>
<P align=justify><FONT>�� �� ������ ��������� ����� �������������� �� ������ ������� �������� ���������.</FONT></P>
<P align=justify><FONT>������������� �� ������ �� ���������������� � ���������� ��, �� ����������� �������� � �������� �����������. ���������������� � ���������� �� ����������� ���������� ��� ����� ��������� ���������. � ������� ��, ��� ����������, ��� �������� ���� ����� ���������� ���. � � �����-�������� �� ���������� ��� ������������� ���������, ����������� � �������� ����.</FONT></P>
<P align=justify><FONT>���������� �� ������������ �� ���� ���������������� ��, ������� ������������� �������� ��������� ����������� � ������������ � �������������� ��� �� ��������� ������ �������. ��� ������ ������������ ������, ��� ������ �������� ������������ �����������, ������������� � ������� ��, � ��� ����� ���� ���, ������������� ����� ������� ������. ��� ������� ������������� ��� ��������� ������, ��������, �����������.</FONT></P>
<P align=justify><FONT>��� ������� �� ��������, ������-��������� �� ������� ������� ��� ������ � �� �������, ����� ������� ������� �����.</FONT></P>
<P align=justify><FONT>���������� �� ����� ����������� � ����� �������� �������� ���� ����� ������� ������ �, ��������������, �������� - ����� ������. ������� �� ������ �� - � ������� �������.</FONT></P>
<P align=justify><FONT>���� �������� ��������� ����� ������� � ���, ��� ��� ���������� �������� ������� �������� ������ ��������� �������� ���������� ��� �� ��� ��������� �� ������ ������. ������������� ������������ �� ������, ������� ���������� ������� ���� �������.</FONT></P>
<P align=justify><B><FONT>��������� �� &#171;����� ���������� �������&#187; ����� ������</FONT></B></P>
<P align=justify><FONT>�������� ���������� �������� ������������� �� �������� �������� ��������� � �� ���� �������� �����. ������ ������� ������������, ����� ������ ���� ���������� ���� ������� ��, � ��������, ������ ������� ���������� � ������, ����� ���� ����������� ���� ������� ��.</FONT></P>
<P align=justify><FONT>������ �������� � ������������� �� �������� ����� �������, ������������� ��� �� �������. ��� ��������������� ������� ������ ����� ����� ��������� ��, ������� ����� ��������. ������ ��������� � ���, ����� ����� ��������� ��, ������� ����� ��������� ���������� �������.</FONT></P>
<P align=justify><FONT>����� �� �������� � �������� �������, ��� �������� ���������, ���������� &#171;����� ���������� �������&#187;. ���� ��� ���������� ����� ������: ������������ �� ������� ��� ����������� ��������� ���������� �� � ���������� ���������, ���������� � ������������ �����. ��������, ����� ��������� ������� �� � �������� 20 � ����� � ����� +7 ��������� ��� ������ ������� ��. ��������� ����������� �� ������� 1. ������ ��� ��� ��� ��������� �������� &#171;�����&#187;, ������� ��������� ��������������� � ����������� �� ��������� �����.</FONT></P>
<P align=justify><FONT>��������� �� &#171;����� ���������� �������&#187; ����� ������. ����� ������� �������� �� ������ ���. ����� �� ��������������� �� ��� ������, �� ������������ � �������� �������. ����� ������� ��������� ���������� ������� ��������� �����. ����� �������� ������ ���� &#171;��������&#187;, � �� ��������� ��������� ������. ����� ����� ���������� �� ��������� ���� �� �����, ���������� �����. � ������, ����� �������� ���� ������� � ���� �������� �� �������, ����� �������� �������������, �� ��������� ��������� � ����� �����.</FONT></P>
<P align=justify><FONT>���������� �� ������ ������ 2001 �. (���. I). ����� �������� ������������� ���������� ����� -�� ������� � �����, ����� ������������. ����� ��������� ����� �� � ��������� ��������� ������ ������ ����������� ��������, ����� &#171;���������������&#187;. � ������ ����������� &#171;�����&#187; �������� ��������������� ������ �� ��������� ������. ��������� ������� ������� ������� �������. � ���������� �� ������ �������� �������������� ��� ��� ������� �� �������.</FONT></P>
<P align=center></P>
<P align=justify><FONT>����������� �������� ��������� � �������� �������� ������, �� ��� ��-�������� ������� � ����� ��������.</FONT></P>


</body>
</html>

