<!--
ID:35917710
TITLE:� ������� � ������������� ������
SOURCE_ID:3144
SOURCE_NAME:������ � ������
AUTHOR:�. �. �����, �������� ������� &#147;������� � ������&#148; �������������� ���������� ��� ��. �. �. ����������
NUMBER:1
ISSUE_DATE:2006-01-31
RECORD_DATE:2006-02-28


RBR_ID:4939
RBR_NAME:������������� ���������� ������������
-->
<html>
<head>
<title>
35917710-� ������� � ������������� ������
</title>
</head>
<body >


<P align=justify>���� ���������������� ������������� � �������������� ��������� ������� � ������������� ������ ������� � ����������� �������, �������������� �������� ����������� ������� ������.</P>
<P align=justify>������ ������������� ������������ ��������� ������� � ������������� � ���������� ������� ������ ��������: �������� ��������������������� ���������� �������; ������������� ������������������ ������; ����������� ������������� ����� ���������� �����; ��������� ������������������ �������� ������� � �������������������; ��������� ��������� ���������� ���������� � ��������; ����������� ����� ���������� ����� � ��.</P>
<P align=justify>�������� ���������������� �������������� ������� ������������ ������ ������ ����������������. �� ������ ������� ����� ������ ����� �������������, �� ��� ������, �������� ��������� ������:</P>
<P align=justify>������������� ���������������� (���, ��������� ������, ��������� �������);</P>
<P align=justify>����������������� ��������� (������, �������);</P>
<P align=justify>�������������� ����� (����� �����);</P>
<P align=justify>�������� ���� ����������� � �������� ���������� ������� (������, ��������);</P>
<P align=justify>��������� ����������������� ������ �� ���� &#8220;���������&#8221; ���������� ������ (���, ��������� ������, ������ ����).</P>
<P align=justify>���������� �������� ��������� ������� ��������� ������� &#8211; �� ���������������� �� ����� ��������. ����� ��������� �������, ������������ �������������� ���������� �������-������������� ��������������� ��������� ��������. � ����������� ���������� ���������������� �������� ����� �������� �������� �� ����� ������, ��� ��������� �������� ���� ����������� � ��� ������� � �������� ������ � ��������� �� ����������������� �� ���� &#8220;���������&#8221; ���������� ������. ������� ������������� ���������������� � ������� ������� � ���������� � ���������� �������� ��� ������������ ����� ����������; ������, ������� �������� �������� �� ��� ��������� � � ��������� ������� ���������. � ���������, ��������� ������������ �������� ����� ������������ ������� ���������� ���������, ����� ������ ��. 60, � ������������ � ������� ����� ��� ���������� ������������ ����, ��������� ������� � ��� �������������, ������� ��������� ��������� �� ���� ���������� ��������������� ������������ ���� (���������� ������). �������� ����� ���������� � � ����������� ������ �� 26.12.1995 �. (� �������� �� 29.12.2004 �.) &#8470; 208-�� &#8220;�� ����������� ���������&#8221;. ��� ���� ���������� ������������ �������������� ��������� �����������, � �������� (��������) ������ ����������� ��������� ������������. ��� �������� ��������� ��������� ��� ������������� ��������� ����������� � �� ������ ���������� �� ��������. ���� ���������� � ������� ���������������� ���������, ��������, ��� ����������� �������������� ����������� ����� � ����� ������� �������������� (������) � 2001 �. ����������� ���� ��������� ���� ����������.</P>
<P align=justify>��� ���� �����������, �� ������ ������, ��������� ����������� ���������������� ����������� � ���������: ���������� � ��� ������� &#8220;����������&#8221;, ���������� ����������� �������� &#8220;�������������&#8221;, �� ������������� ����������� ��������, � ������� ����������� ��� ��� ������ &#8220;mergers&amp;acquisitions&#8221; (&#8220;������� � ����������&#8221;). � ����� ���������� � ���, ������ ���������� ������ �� ������� ���������� ����� �, ��������, ������� ������� ���������� ������ � ������ ��������� �� ����������� ������ � ���������� ����������� ���������������� � ���� ������� � ������������ � �������������� �����������. � �� �� ����� � ����������� ������ ��� ��� ����������� �� ������� ��� ������������� ������, ������� ������� ������������ � ���, ��������, � ���� ���� ������ <B>&#8211; </B>�������� �� 1933 �., ������������ ������� ������������� � �������������� ������, ���� ���� ���� �� ��� ���������.</P>
<P align=justify>���������������� ��������� ���� �� ����� ���������� � ����������� ��������, ��� ��� � ������� �� ����������� ������������� �������� ��� �� ������ � ������ ��� ����� ����������� ���������� ��� ������ �������, ��� � ���������. ��� ���������� ������� ��������� ���������� ��������� ��������������� ������� ��������.</P>
<P align=justify>�������, �������� �������������� ����� ����������������� ������ �� ����� ���� ����������� ���������� � ������, �� � � ������� �������� �� �������� ������������. ������ �� ����������� ���������������� � �������� �������� � ������������ � ��������� �����������, ��������� � ���������� �������������, ���������� ������������ ��������� �������� � ����������� ��������� ����������� �������� � ������������ � ���������� ����� ������ �� 4.06.2003 �. &#8470; 230-� &#8220;� ������������� ��������� ����������� � ����� ������� � �������������&#8221;. �������, ������ ����� ����� �������������� � ���������� ������� �������, ������� ��������������� ����������� ������ �������� �� ������������ ���������� ������������� ��������.</P>
<P align=justify>����� �������������� ���������� ������� ����� �������� ���� ��������������� ������ (������, ��� ���� ����������� � �������� �������� ��������� 50%), � ����� ���� ����������� � ������� ������ � ������������ ���� ����������� � ����������� ������. �������� ���� ����� <B>&#8211;</B> ��� ������� ������������ ����� �����, ������������� ������������� � ���������� ���������� ���������, � ����� �������������� �� ���������� (���� ����� �����) � ��������. ������� � ���� ����� ��������� ������������, ��������������� �������� � ������������ � 2001 �. � ������� ������ ���-�����. ����� ������ &#8220;������������ ������&#8221; ���� ��� �������� �� ������ ������������, ��� ������� � ��������. ��� ����� ���������� ��������� 2.04.2002 �. ������������ ������������� &#8470; 454�, � ������������ � ������� ��� ������������� � ��������������� ��������� ����������� ������ ���� ����� �� �������� ��������� �����������, ������� ���� ������������. �� �������� ���������� ���� ������� ��� ���������, �������������, ����������� ����� ��������, �������������� ����� �������� �������� � �������������. ������ ������������, ������� ���� �����, ���������� �� ������� ������� ���������� � ����������� ����� ������������ ������� ������������� ����������� ����� ������. ������ ������������� ������� ������� ������� ������ ��� �� ������. ���� ���� ��������� ������: ��-������, � ����������� ����� ��� ������ �����, �� �������������� �������� ��� �������; ��-������, ��� ���� �� ����� ��������������� �����; �-�������, ���� �� ���� ����� ���� ����� ������� ������� ���� ������ � �������� ��� �������������� ������ ������ ��� ���������� ������ ����������� �� �������� ����������� ���� ��������� �����������.</P>
<P align=justify>�������� ��������������� ���������� ������, ��������, ������������ ��� ������� ������ ������������� <B>&#8211;</B> ������������ ����������� ��������������� <B>&#8211;</B> ����������� � ������ ����������� ����� ������-�����������. ������ ������ ������������ �� ����� �������. ����� ��������, ��� ��������� ������������ � ������ ������������ �� &#8220;�����&#8221; ������. ��� �� ����������� ��� � ������� ������ ������� �� �������� �������� � � ����� ������� � �������� � ���� ������ ��������������. ������������ ��������� ��������� ������ �� ���������� ����� ������� �������� ��������� �� ������, �� �������� ����� ���� ����, ��������� �������� � ������ �� ����. ������ &#8220;����������&#8221; ����� ������������� � ����� ����� ��� �����, ������� ��������������� �������� ��� �������� ���� ��� ����� ������� ���������, �� ������� &#8220;������&#8221; �������� � ��� ����������� ����� ���������� ������.</P>
<P align=justify>��� ���� �� ������� ����������� ����������� ������� <B>&#8211;</B> ������� ������� ������������ � ��������� ����� ���� ����, ����� ������� ����� ���� �� ��������� ��������� ������. ��-������, ��� �� ������ ������������ �������� ��������� �������� ��������� � ����� �������� ���������� ������; ��-������, ������� �������� ������� ����������� � ���������� �������� ��������� �������-������������� ������.</P>
<P align=justify>������ ���������������� �������� ������ � ��������������� �������� �������, �������� ������������ ���������� � ������� ���������� ���������. ��� ��������� ���� �����, ������� ����������� �������� ������������ � ����������������� ����������� ������� �������. ����� ������ � ������ ����� ������������� ��������� ��������; ��� ���������, ��������, ������������������, � ������������ ���� �������� ����� �� ����������� � �������� ��������� ������� � ������������� ������ � ��������, � � ������������ ���� <B>&#8211;</B> � ���������� ��.</P>
<P align=justify>������ ������ <B>&#8211;</B> ��� �����, ������� ������� � �������� ����������� ������������� ���������� ��������� (������� ������) � ����� ������. ��� �������� ������������� ���������������� � ������� �� ��������� ������������, ������ ����������� ���������� ������ ���������� ���������� �������. ������ ����� <B>&#8211;</B> ��� �������� ������ (����� 60% ����� ����������� ����� ������) � ������������ (99,9% ����� ����������� ������������� ������); ����� ��������� ����� � ��������������, ������ �� �� �������� ����������� ������������ ������, ������������ ���� ������������ �� ������������ ������� ������, ��������������� �������� � ���������� �������� �� ������� � ����������� ������������.</P>
<P align=justify>������ ������������� ����������, ��� ������������ ����������� � ���� ���������: ������ �� ��������������� ������������ ���������� ������� � ������������� ����� ������� ����� ������������� � ������ � ��������������� �������� ���� �������� ���������������� ������������������ ����������� ��������. ��� ��������� ����������� � ��������� ��������� ��� �������� ����� �� ����� �������� ���������� �� ���������� � ���� ���������� ������ ���������� �������: ���� ����������� ����-���� (86% �����), ����� ������ (������������ ���� ������) � ������� (�����������), ������� ������������ ���� (����), ������� ������������ ���� (�����), � ������ ������������� ��������� ��������� �����������-������������ ���� (���� ����������� 25% �����, �� � ������� ���� ��� ����, ��� ��������, �������� �� 75%+1 �����). ������������ �������� ������������ ����� ������� �� ���� ������������� ������ ���������� ������ � ���������� ����������� �����. ����������� �������� ����������� ����� � ������������ �������� ����� ������. ������ ��� ����� ������ ������� ����������� ���� ������������ ������. ���������� �������, ��� ���� ���� � ������������ ��� ���������� ����������� � ������������ � �������� ������, ��������, ��� �� �������� ���� ����������� � ���������� �������, ���� ��������������.</P>
<P align=justify>� ���������� ����� ��������� ������� ����������� �������� ���������: ���� ���������� ������, ������������� ������������ ������� ������.</P>
<P align=justify>������� ��������� � � ������ ������������� ����� ����� ��������� � ����� ���������� ������ � ���. ���� �� ��� �����, �� ��� ������, ����������; ������������� ��������� ����������� ����� � ����� �����������, �������� ���� ��������� � ����������� �������� ���������� �����, �������� ��������� ���������� ���. � �� �� ����� ����� ����� �� ����� ������ � ����������� ����� ������������ ����� ���������� ��������� � ������ ������������� ���������� ���������. ��� ��������� �� ��������������� ������� �� �������� ��������, � ������������� ����� �������������� �������� � ���������� �����.</P>
<P align=justify>����, � ����� �������, ����������� ������������� ����, � ������ �����������, �� ���������� ������ ������� � �������� ������. �������� ��� �������� ������, ������������� ������������� � ����������, �� �����, � ����� ������������ ������� ������. &#8220;������������ �����&#8221; ���������� �� ������ ����� � ����� ����������� ������� �� ��������. ������ ������������ ����� �������� ������ ��� &#8220;������� ����������� ����&#8221;, ������������� � �������� 17 ��������������� �����������, ����������� � ������� ����������. � 2003 �. ��� ������� ������������ ��������������� �������� ������������ �� (25,1% ����� &#8211; ����������� �����); � 2005 �. ���������� ���� ������������ ��������� ����� �� ������� ����������, ������ ���� ����. &#8220;����������&#8221;, ������, ��� � ���������������, ������ ���,</P>
<P align=justify>���� ��������� �������. � ������ �������, �� ���� ������������� ��������� ���������� ������� ������������������ ��������.</P>
<P align=justify>� ������������ ���������� ��� �� ������������� ��� ��������� ������������. ����� ������ � ��������������� �������� �����������, ��� ���� ������� ���������� �� ������������� � ���������� �������� ������ � �� ���������� ������� �������� �� ������� ������ � ���� �����������. ����� ��������� <B>&#8211;</B> ���������� ���������� ������ � ��������������� �������� � ������������� ����������� �������� ����������, ������������� ���������� ��������������� ������ <B>&#8211;</B> �������������� ������ ��������. �� ������ ������, ����� ����� ����� �������������, ��� ������� ���������� ����� ������, �� ���� ���������� � 2005 �. ���� ��������, ��� �� ��� ����� ���� ����� 3000 ��������� ��������������� ������.</P>
<P align=justify>������� �����, �� ��� ������, ������� � ����� ����� �������������� ��������� �� ���������������� ��������� ����������� � ��� �������� �������� � ���� ����������� �������� �� ����������� ��������� ��� (����). ��� �������� ������������� ��������� ����������� ����� ������� � ����������� ���������������� &#8211; ������� ����������� ���������. ���������� ���� ����� �� ������� ������� ������� � ����, ������ �������� ������������ �������, � � ����������� ���� ����, ��������, ����� ����� �����������. ����� ����������� ������� 1998 �. ���� ���� ��������� ������� ������, � � ���������, � ������ �������� ���������, ��� &#8220;���-����&#8221; � &#8220;���������� ������&#8221;. � ������ �� ��� �� ������� �������� &#8220;������������� ����������&#8221;, � �� ��� ������������ ����� ���������� �������������� ��������� ����������. ����� ������� ������� ����� ������� ��������� �����������. ������������ ��� ������������������, � � �������, ��� ����� ������ ��������� �����������, ��� ����� �� �������.</P>
<P align=right>� � � � � � �</P><B>
<P align=center>������� ���� ����� ������������������� ������*</P></B>
<TABLE cellSpacing=1 cellPadding=7 width="100%" align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="19%">
<P align=center>����</P></TD>
<TD vAlign=top width="11%">
<P align=center>����</P>
<P align=center>������</P></TD>
<TD vAlign=top width="14%">
<P align=center>������ ������ �����, %</P></TD>
<TD vAlign=top width="14%">
<P align=center>��������� ����,</P>
<P align=center>���. ���.</P></TD>
<TD vAlign=top width="14%">
<P align=center>���� �������,</P>
<P align=center>���. ���.</P></TD>
<TD vAlign=top width="26%">
<P align=center>���������� ������</P></TD></TR>
<TR>
<TD vAlign=top width="19%">
<P align=left>���������������</P></TD>
<TD vAlign=top width="11%">
<P align=justify>23.04.01</P></TD>
<TD vAlign=top width="14%">
<P>76,3</P></TD>
<TD vAlign=top width="14%">
<P align=right>57 200</P></TD>
<TD vAlign=top width="14%">
<P align=right>57 200</P></TD>
<TD vAlign=top width="26%">
<P align=justify>��������������</P></TD></TR>
<TR>
<TD vAlign=top width="19%">
<P align=left>�����������</P></TD>
<TD vAlign=top width="11%">
<P align=justify>27.04.01</P></TD>
<TD vAlign=top width="14%">
<P>12,95</P></TD>
<TD vAlign=top width="14%">
<P align=right>18 000</P></TD>
<TD vAlign=top width="14%">
<P align=right>18 000</P></TD>
<TD vAlign=top width="26%">
<P align=justify>�� ����������</P></TD></TR>
<TR>
<TD vAlign=top width="19%">
<P align=left>����������</P></TD>
<TD vAlign=top width="11%">
<P align=justify>28.05.01</P></TD>
<TD vAlign=top width="14%">
<P>85,15</P></TD>
<TD vAlign=top width="14%">
<P align=right>85 145</P></TD>
<TD vAlign=top width="14%">
<P align=right>85 145</P></TD>
<TD vAlign=top width="26%">
<P align=justify>��������</P></TD></TR>
<TR>
<TD vAlign=top width="19%">
<P align=left>�����������</P></TD>
<TD vAlign=top width="11%">
<P align=justify>30.05.01</P></TD>
<TD vAlign=top width="14%">
<P>75,154</P></TD>
<TD vAlign=top width="14%">
<P align=right>104 449</P></TD>
<TD vAlign=top width="14%">
<P align=right>120 000</P></TD>
<TD vAlign=top width="26%">
<P align=justify>�����������</P></TD></TR>
<TR>
<TD vAlign=top width="19%">
<P align=left>���� ������</P></TD>
<TD vAlign=top width="11%">
<P ali
gn=justify>01.06.01</P></TD>
<TD vAlign=top width="14%">
<P>96,6</P></TD>
<TD vAlign=top width="14%">
<P align=right>96 601</P></TD>
<TD vAlign=top width="14%">
<P align=right>96 605</P></TD>
<TD vAlign=top width="26%">
<P align=justify>����������</P></TD></TR>
<TR>
<TD vAlign=top width="19%">
<P align=left>������������</P></TD>
<TD vAlign=top width="11%">
<P align=justify>22.06.01</P></TD>
<TD vAlign=top width="14%">
<P>82,215</P></TD>
<TD vAlign=top width="14%">
<P align=right>284 750</P></TD>
<TD vAlign=top width="14%">
<P align=right>284 750</P></TD>
<TD vAlign=top width="26%">
<P align=justify>���� �������</P></TD></TR>
<TR>
<TD vAlign=top width="19%">
<P align=left>����</P></TD>
<TD vAlign=top width="11%">
<P align=justify>17.09.01</P></TD>
<TD vAlign=top width="14%">
<P>36,596</P></TD>
<TD vAlign=top width="14%">
<P align=right>99 556</P></TD>
<TD vAlign=top width="14%">
<P align=right>99 557</P></TD>
<TD vAlign=top width="26%">
<P>���������� ������������������ �������������� �����</P></TD></TR>
<TR>
<TD vAlign=top width="19%">
<P align=left>�������</P></TD>
<TD vAlign=top width="11%">
<P align=justify>23.11.01</P></TD>
<TD vAlign=top width="14%">
<P>75</P></TD>
<TD vAlign=top width="14%">
<P align=right>76 802</P></TD>
<TD vAlign=top width="14%">
<P align=right>76 802</P></TD>
<TD vAlign=top width="26%">
<P align=justify>���� �������</P></TD></TR>
<TR>
<TD vAlign=top width="19%">
<P align=left>�����������������</P></TD>
<TD vAlign=top width="11%">
<P align=justify>30.11.01</P></TD>
<TD vAlign=top width="14%">
<P>99,99</P></TD>
<TD vAlign=top width="14%">
<P align=right>41 000</P></TD>
<TD vAlign=top width="14%">
<P align=right>41 300</P></TD>
<TD vAlign=top width="26%">
<P align=justify>����� �</P></TD></TR>
<TR>
<TD vAlign=top width="19%">
<P align=left>���� &#8220;�������&#8221;</P></TD>
<TD vAlign=top width="11%">
<P align=justify>29.12.01</P></TD>
<TD vAlign=top width="14%">
<P>99</P></TD>
<TD vAlign=top width="14%">
<P align=right>1 000</P></TD>
<TD vAlign=top width="14%">
<P align=right>1 250</P></TD>
<TD vAlign=top width="26%">
<P align=justify>����� �����</P></TD></TR>
<TR>
<TD vAlign=top width="19%">
<P align=left>�����������</P></TD>
<TD vAlign=top width="11%">
<P align=justify>29.04.02</P></TD>
<TD vAlign=top width="14%">
<P>50</P></TD>
<TD vAlign=top width="14%">
<P align=right>75 400</P></TD>
<TD vAlign=top width="14%">
<P align=right>75 400</P></TD>
<TD vAlign=top width="26%">
<P align=justify>���� �������</P></TD></TR>
<TR>
<TD vAlign=top width="19%">
<P align=left>�����������</P></TD>
<TD vAlign=top width="11%">
<P align=justify>15.01.03</P></TD>
<TD vAlign=top width="14%">
<P>99,99</P></TD>
<TD vAlign=top width="14%">
<P align=right>150 000</P></TD>
<TD vAlign=top width="14%">
<P align=right>150 001</P></TD>
<TD vAlign=top width="26%">
<P align=justify>�����-�����</P></TD></TR></TBODY></TABLE>
<P align=center>* � � � � � � � �. ���� ���������� ��� �����. &#8211; ���������. &#8211; 16.01.2003.</P>
<P align=justify>������ �� ����������� ������, ������������ ���� � ������ ������� �������� ��������������� ������� ����� ������� ��� ������������� � ��������. ������� �������� ���������������� ������ ���� ���������� ������������ ������ � ������� ��������, ����������� �� �� ������������. ��� ���� ������� � ������ � �������� �������. ������� ���������� ��� �������� ������������ �������, ����� ��������������� ������ ����� � ������������. � ����������� ���� ��� ������ �������� � �������� ���������������� ������ � ��������� �� ����������� �������.</P>
<P align=justify>��������� � ����� ������������� ������� &#8211; ���� (Federal Deposit Insurance Corporation), ������� ����� ���� ��������� � � ������� ����������� ������������ �� ������� ������ � �� ����������� �������� ����������� ��������� ������������. � ������������ �������� ����-������� ����� ��������� ��� ������� ����������� � ��� �� ����� �����������, �� ��� ������ (���������������) ���������. ��������������� ������ � ������� ������� ��������� ����� ���������, � ����������� ������ ���������� ���������� ��������� � ������ ���� ��������� �������� ��� �������������� (RTC), ������� ����� �� ���� ����� ��� ��������� ���������� � ����������. ����� �������� �������� ��������������� � ����� ������������� ����� � ���������� ������ &#8220;��������� �����������&#8221;.</P>
<P align=justify>���������� ���������� �������� ��� ���� ����� �������� ��������� ����� ������������ �������� ��� (� ������ ���������� ����� ��� ���������� 5 ���� ���.), �������� ���������� ���������� ��� �������������� (RFS), ����������� ������ ��������� �������. � ������ ���������� ����������� �����, ��������� ���������� ����� ������������ ��������� ����������� ����� ��������� � ��� ���� ��������� ������ ���������� � ����������, ��� ���������� ������ � ������� ����� ��������� ���� ��� ���������������� �����.</P>
<P align=justify>����, ���������� ������� �������������� ����������� �����, ����� ��������� ���������� ������� � ��������, � ����� ����������, � �������� �������� ��������, �, �������, ������� ����� ��������� ����������� ������ ����������� �����, ������� ������� ������� ��� ���������� � �������� ������� ���� �� ������.</P>
<P align=justify>��� �� ������������ ������ ������� ���� � ������������� ��������� ���������� ���. ���������� ���������� ���� 1992 �. � �������������� FR Bank Corporation. � 1992 �. ���� ���������� ������ ���� ���������� ���������� ��� ������������ ������ �� ����� 1 ���� ���. ��� �������� �������� ���������� ��������. ����� 4 ������ ���� ����������� �������� ������� � ��������� � ����������� 41 �������� ���� ����������. ���� ������� ��� ������������� �����. ������ ��� ����������� �� ���������� ���� � ������� ���������� ����������� NCNB Corporation of Charlotte, ����� �������� ��������� 40 ������� � ������. ����� ��� ���������� ����� ���������� � ��������� � ������������� ������� ����� ����� � 1 ���� ���. ��� ������� ������� NCNB � ���� � ����������� 20 : 80. ��� ���� NCNB �������� ������ �� 5 ��� �� ������������ (�����) 80-���������� ���� ����. ������ ������������� ���� ��� ����� ��������� ������� ����� �������� ������ Citibank.</P>
<P align=justify>�������������������� ������ ������������ ����������� �������� �� ����������� ��������� ��� <B>&#8211;</B> ��� ��������� ������ ������������� � ���������� � ����������� �������� &#8220;��������&#8221;. ��� ���� ����������� �������������� � ����������������� ������� ��� �� ��������� �����, �������� �� ���� ������������� ����� (������) �� ������������ �����. ��� ����������� �������������� ������� � ������������� ������.</P>
<P align=justify>� �������� ���� ������� � ������ ������. ��� ��������� ����� ��������� ������ ����������� � ������������ � ������������ &#8220;������� � ��������� ��� ����������� � ���������� ����&#8221; 1987 �. ����� ��������� �����-����. ����������� ������� <B>&#8211;</B> ����-������� ������ ����� ������ ����� 500 ��� ���. �����-���� ������ ��������� ��� ��������� ��������:</P>
<P align=justify>���� ������� �� ��� ����������� � ������������ �� ��������� ������ �� ���������� ��������� ����� ��� ������;</P>
<P align=justify>���� ����������� �������� ��������������� ����� ���������� ��� ���� �������, ��� ���������� ����;</P>
<P align=justify>���� ����������� �������� ���������������� ����� ���������� ��� ���������� ����� ����� � ���������� ���������.</P>
<P align=justify>���� �������� �����-����� <B>&#8211;</B> �� ���� ���. �� ��� ����� ����, ������� ���������� ��������� ������ �����������, ����������� ��� ������������� ����������, ������������ ���� (�����) � ������� ��� ����������� ���������� ����������. �� ������ �����-���� ����� ���� �������� ��� �������� � �������� ����� ������������ ������������� ����� ��� ����� �����.</P>
<P align=justify>������ ���������� ���������� ������, ���� �������� ����� ������� ������ ��� �������� �����������, ������� ����������� �������.</P>
<P align=justify>�����, ���������� � ���� ��������� ����������, ��������������� �������������, �������� ��������� ������������, ������� ������ ����� ������� ���������������: ����� ���������� ���� ��������� � ������� 14 ���� ����� ������ � ����������� ��������� ������ ��� ����������� RTC ������ �� ���������� ������������ (���������� ������������ ��������������� ������� � ������� 3 <B>&#8211;</B> 4 �������).</P>
<P align=justify>������ ����� ������� ���������� ���� �������� ���� � 1982 �. �� ������ ����� &#8211; ���-������� � �������������� �����������.</P>
<P align=justify>� ����� �������� ��������� ��� ������� ����������������� ������ ������������ �������� ���������� ���������, ����� ������, ��������� �� ����������� ������� � ������������ ������, ����� ������� ������� ������ �������� ����������� ������������� ���������� ������. ������� ���������� ������� � �������������� ���������� ������������ ������� ������������ ������, ����������� � ����������� ��������, �� ������� ����� ������ ��� ���� ����������� �������. ����� ���� ���������� �� ������ � ���, �� �, ��������, � ����� �����, ������ �������. ��� ��� ����� ������������� � ����� �������� ������� ����������� �������, ����� ����������� ����������� �� ������������� ������� ������� ������� �������. � ���� ��������, ������, ���� �� �������������, ����� ������������� � ���� ������ �������� �� ������� �������� � ������� �������������� ����������� ������� ������ � ������� ���������� �����.</P>
<P align=justify>������ �������� ������ ���������������� ��������� ����������� &#8211; ��� ����������� � ������������ ��������� ����������� ���������� ���������� ���������� �� ����� ������ ������ � ����������� �������� �� �� ����� � ����������� ��� ��������. ����� �������� ���������� � ���. � ������ �����</P>
<P align=justify>����� �������� �� ������ ����� �������� ����� ������, ����������, ���������� � ���������� ����������� �����, ��� ������������ ���������� �������� ���������� ������� ��������������� ������ � ��������, �� ������ ����������� ������. ������������������ ������ ����������� � ������� �� �������� ��������: ��� �������� �� �� ������������ �������� �������� �� ������ ����� ����������� � ������-������������ ����� ��������, ����������� ����� ���� ����, ������ �� �����, � ��� ����� ��������� ������� � ��� ������������� ������� ��� ������� ����� ���� �� ��������.</P>
<P align=justify>����, ��� �������� ����������, �� ������ ������, �������� ������ �������������� ������� � ������������� ��������� ����������� <B>&#8211;</B> ��� �������� ���� ����������� � �������� ����������� ������� � &#8220;���������&#8221; ���������� ������ � ������. ��� ������ � ��� ��� ���� ����������� ����������� � ����� ���������������� �������������� � ������� ������ �����. ������ <B>&#8211;</B> ����� ������� ���� ����������� � ������� ������������ ������ � ������������ ��������������� ��������� ����������; ������ <B>&#8211;</B> ����� ������� ������. �� ��� ������� ����������� ����������������� � ������������ � ������ �������� ���������� ������.</P>


</body>
</html>

