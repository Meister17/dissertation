<!--
ID:38671957
TITLE:������� ��� ���������?
SOURCE_ID:11610
SOURCE_NAME:�����
AUTHOR:������ ������
NUMBER:4
ISSUE_DATE:2008-01-21
RECORD_DATE:2008-01-22


-->
<html>
<head>
<title>
38671957-������� ��� ���������?
</title>
</head>
<body >




<B>������� ��� ���������?</B>
<BR>
<I>������ ������</I>
<BR>
<P>��������������, �����������, � ��� ��� ��� � ����. ����� � ��������, ������������, ����������� ���������, � ������ �� ����, ���. ����� �������, ��� �������� � ��� ����� ������ ���-�� �����, � ���������� � ���� ���-�� � ������: "�� ����� �� ���� �����, �������� �������? ����� ������"! �����������, ������ ���� ��� ������. ��������, ������ ��. ��� ��� ����� ��� � ���� �����, � ��� ����� ������ �������������, ���� �����������. �� �� ���� ���� ������� - ��������, �� ���� ������, ��� ����������. ������, ������, ���������. � ���, ������� ���������, �� �����? �� �� ����, ��� ������ �� ���� ��������� ��������: "������ ��� ��� �����, � ��� ������?" � ����� ��������.</P>
<P>� ���� � ����, ��� � ����� a����� � ��� ������ �� ��� � �������. ��� ���� �� ��� �� ���� ����������, ������ �������� �����, � ������� ����� ����� ����� ����� ������ ������� �� ��������������������� � �������: "������� ��� ���������? �����. ������ �� ���������� ��������� �����, ��� �������. �� � ��� ������ ������ ����������". � � �������� ��� ��������� - ��� ����� ��� ������ ����� �������. ����� ����������, ��� ��������, ����� ��� ��������. ������ ���������� ��������� � ������ �������� �������� ���������: ���� �������, ������ ����������, ������ �������, ����� ���������... �����-����������� � ���� a����� ��������, ������ ������ ����� ����. ������, ���������, ����������, � ������� - ����� ���������� ��������� ����� ������ ����� �� ����������. ���������� ������ ��������������� �����, ���������� ��������, ������ ���������, ���� �� �� �������. ���, �������, ����������������� ���������� ������ ���, ������ ��� �� ���� ���� �������. ��� ��� ��, ����� � ���� ����, ��� ���������. � ����� � ���, ������ ��������, ���������� ��������� ������ � ������� ����� ����� �� ����������� ���������.</P>
<P>����������, ������� ������� ������. � 1937 ���� ��� a����� ������������ � ��������� ��������� �� ���� ���������� �������� � ����� ��������, �� � 90-� ������ �������. ����������� ������ (����� ��������, �� ������������ ������), �� ������ ��� �� ���� ��������. ������� ��� ���������, ������ ������, ��������������, ������ ������� ��������� �� ���������. ���� ����������� ����� �����-�� ����� (�������� ������������� �� ���������� �������������) ������ ��� �� ������, ��� �� ����� ������������� ������������ �������� ������� ��� ����. ������� ���� ���������� (�������), ����� ������������ ��������������� � �������� ������ ���� a����� � ������������� ����������� �� 2045 ����. � ������ ��������� ������� � ������ �����������, ����� �� �������. �� � �������, ������ �����������, ��� ���������, �������� ����������� �������, ����������� ��������� ����� ������, �������������������� ����������. ���������������������, ������, ��� a����� �� ��������, ��� ������ ����� ��� ���������. ������ ��������, ������� �������, �����������. �� ��� ����� ����� �� ��������� ����������. �� �����, �����. �� ���� ��� a����� ������� ����� ��������� � ������ �������, ������������, �� ��� ���� ������ ��������, ����������� ���������, ������ ���������? �������: �� ������, ��� ������, ���������. ��������, ��������, ������� � �� ��������� �������������� �����������.</P>
<P>�� ����� ������� ����� � ��� �� ������� ��� ��������. �������� �� ������ �������� ���������� ������. ���� �� ����� ������ �����? �����������? ������? ��� ��������? ��� ������ ��������� � ������ �������� ��������� �������� ������ ���� �����, ��������, ���� ������� ��������, �������� ����� ��� �������. � ���? ��� ���, ��� �������? ���� ��. � ��������� ���, �� �������� ������ �����. ���� ����� �����? ����������? �� ���, ���� �� ����� �� ���������, �������� � ������, � ������� (�� ������� ���� ��, ������ �����������) � ������� ��������. ���� ��������, ��� ��-������?</P>
<P>P. S. ����� ��� ������� ���� ��� ��������, ������ ���������: ������ ����-������� ������� �������� ��������, ��� ����������� ������� ���, ����� �� ��������� ������������ ���� ������. ��������?</P>


</body>
</html>

