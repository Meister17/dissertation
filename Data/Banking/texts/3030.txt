<!--
ID:35308264
TITLE:��������� � ���������� ��������� �����������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:11
ISSUE_DATE:2005-06-15
RECORD_DATE:2005-09-20


-->
<html>
<head>
<title>
35308264-��������� � ���������� ��������� �����������
</title>
</head>
<body >


<P><FONT>���������� ����������� (�����������) ������������ ������, �������� �������� �� ���������� ���������� ��������, ����������, ��� � ������������ � ��������� ������������ ����� �������� ���������� ��� ��������� ��������������� (���������) ����������� ����� � ��������� ���������������� (����������) �������� ���������� ��������� ��������� �����������:</FONT></P>
<TABLE cellSpacing=2 cellPadding=7 width=610 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="8%">
<P align=center><FONT>&#8470;<BR>�/�</FONT></P></TD>
<TD vAlign=top width="25%">
<P align=center><FONT>������������<BR>���������<BR>�����������</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>��������-������� �����</FONT></P></TD>
<TD vAlign=top width="18%">
<P align=center><FONT>����<BR>������<BR>��������</FONT></P></TD>
<TD vAlign=top width="35%">
<P><FONT>�����, �������, �.�.�. ����������� ������������<BR>(�����������).<BR>���� ������������ ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center><FONT>1</FONT></P></TD>
<TD vAlign=top width="25%">
<P align=justify><FONT>����</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>1998</FONT></P></TD>
<TD vAlign=top width="18%">
<P align=center><FONT>24.03.2005</FONT></P></TD>
<TD vAlign=top width="35%">
<P><FONT>358000, ���������� ��������, ������, ��. ������, 3;</FONT></P>
<P><FONT>������� ������ ��������;</FONT></P>
<P><FONT>���. (84722) 5-95-23;</FONT></P>
<P><FONT>1 �����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center><FONT>2</FONT></P></TD>
<TD vAlign=top width="25%">
<P align=justify><FONT>�����������</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>1625</FONT></P></TD>
<TD vAlign=top width="18%">
<P align=center><FONT>09.02.2005</FONT></P></TD>
<TD vAlign=top width="35%">
<P><FONT>123022, ������, �/� 39;</FONT></P>
<P><FONT>��������� �� ����������� �������;</FONT></P>
<P><FONT>���. (095) 959-47-97, 589-40-88;</FONT></P>
<P><FONT>60 ����</FONT></P></TD></TR></TBODY></TABLE><I>
<P align=right><FONT>"������� ����� ������", 2005, &#8470; 20</FONT></P></I>


</body>
</html>

