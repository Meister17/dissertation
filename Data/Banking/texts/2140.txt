<!--
ID:36194258
TITLE:����� ������
SOURCE_ID:3106
SOURCE_NAME:���������� ���������
AUTHOR:
NUMBER:4
ISSUE_DATE:2006-04-30
RECORD_DATE:2006-05-10


-->
<html>
<head>
<title>
36194258-����� ������
</title>
</head>
<body >


<H5><FONT size=3>����-�����������, ����������� ���������� ��������� � �������������� �������� &#171;������������� ��������� ������&#187; ��������� ������� ��������� </FONT></H5>
<P align=justify><FONT>&nbsp;������� ��������� �������� � ������. ����� ������ ������� ��������� ���� � ������ ������ &#8212; ������. </FONT>
<P align=justify><FONT>� ������ � 1991 �� 2001 ��� �������� ������ ������� � ������� ��������, ����������� � �����������. �������� � ����� ���������, ��� ����� �������������� ����������� &#171;�������� ������&#187; (����������-�����������), &#171;�����&#187; (�������� �� ��������), &#171;����������� ��������������&#187; (������� �����������), &#171;��� ����&#187; (��������� �������� ������), &#171;�������&#187; (�������� �� ���������). </FONT>
<P align=justify><FONT>� 2002 �� 2006 ��� �������� �������������� ���������� &#8212; ���������� ������������ ���������� ������������� ��������� � ��� &#171;�������������� �������� �����&#187;, � ������� ����������� ����� ������� �������� �� ������, ��������� � �����������, �������� � ������������ ����������������, �������������� � ������������� HR-���������.� 2005 ���� ����� ������ ���������� � ������� HR �� ����������� ���������� ���������� ������. </FONT>
<P align=justify><FONT>� ����� ��������� ������� ��������� ����� ���������� ������� ���������������� ��������, ����������� � ��������� ���������, ����������� ������� � ������ �������������� ������ ������ �������, ������������� ������� � �������� � ��������. </FONT>
<H5><FONT size=3>�������� �������� �������� �� ��������� ��������� ���������� ���������������� ���������� ��� &#171;�������&#187; � �����������</FONT></H5>
<P align=justify><FONT>�������� �������� ������� � 1970 ���� � �����������. � 1993 ���� � �������� ������� ���������� ��������������� ����������� ����������� ��. �.�. �������, � 1998 ���� &#8212; ������������ ��������������� ����������� �� ������������� &#171;������� � ������&#187;. � ��������� ����� ����������� �������� �� ��������� ��� �� ���� ���������������� ������������ &#8212; ������ ����� ���������. </FONT>
<P align=justify><FONT>� 1994 ���� ������� � ��������� ������ �� ������ ����������. � 1997 ���� &#8212; ������ ����������� ������������ ��������� ��������-���������� ����� ��������� ������. � 2004&#8212;2006 ����� �������� ����������� ������������ ��������� ��������-���������� ����� ��������� ������. </FONT>
<H5><FONT size=3>����������� ���������� �������� ��� &#171;��������&#187; ��������� ������� ���������</FONT></H5>
<P align=justify><FONT>������� ��������� �������� � �������� ��������������� ������������ �������� �� ������������� &#171;������������� ���� � �����&#187;. �������� � ���������� ����� � 1995 ����. ���� ������� ��� �������� � ���������������� ����������� ����� &#171;��������&#187;. � � ���� 2004 ���� ������ ��������� ���������� ��������. </FONT>
<P align=justify><FONT>�� ����� ������ � ����� &#171;��������&#187; ������ ���� �� ���������� �� ���������� ����������. </FONT>
<H5><FONT size=3>�������������� ���������� ������������ ������������� ��������� � �������������� ������� �����-����� ��������� ��������� ����������</FONT></H5>
<P align=justify><FONT>&nbsp;�������� ������������ ������ ��������� ���������� ������ �������� ����������� ���������� ������������ � ������� ��������� ������������� ���������� ���� &#171;��������� ��� ������&#187;. </FONT>
<P align=justify><FONT>� ����� ����� ����������� ������� �����, ���������� ���������� � �����, ��������� ������������� ������� ������ �����, ��������-�������, ������� ������������� ������������ ���������� � ���������� �������� ����� ����� � ��������, �������� ��� ��������� �����. </FONT>
<P align=justify><FONT>� ���� ����������� ����� ��� ���������� ���������� ������������ ������ ������, ������������� ���������� ����� � ���������� �������� ��� ������������� ��. �. ���������� ��������, &#171;��� ����� ������ ����� ���������� � �������� ��� ���������� ������������, ��������� �� ����� �������������� �� ������ ���������� ����, �� � ���� �������� ���������� ������ ����� �������� � �������� ���������. ��� �������� ��������, ������� ���������� ����� ��������� � �������� ������&#187;. </FONT>
<P align=justify><FONT>�. ���������� �������� � 1984 ���� ���������� ���������� �������� �� ������������� &#171;������� � ������&#187;. � 1988 ���� ����� ���������� ������������� ����. </FONT>
<P align=justify><FONT>� ���������� ����� &#8212; � 1993 ����. �������� �� ����������� ���������� � ������������ ������. ��������� ����� ������ &#8212; ����������� ��������� ����, ��� �. ���������� ���� ����������� ������ &#8212; &#171;���� ���������� ����������&#187;. </FONT>
<H5><FONT size=3>������� ������� �������� ������������� ��������� ������������������</FONT></H5>
<P align=justify><FONT>&nbsp;������� ������� ������� � 1975 ���� � ����������. ����� ���� ������� � 1997 ���� � ����������� �� &#171;�������������&#187;. � ������ 1998 ���� �������� � ��� &#171;���-����&#187; �� ��������� �������� ���������� ������ ������������� ������������ ������������ ������������� �������, � ���� 1998 ���� ��������� �� ��������� ���������� ������ ����������� ������ ��������� ���������, � � ���� 1999 ���� ������� ������������ ��������� ������������ ������������� �������. � ����� 2000 ���� ����������� � ����������� �� &#171;�������������&#187; (� 20.04.2001 &#8212; ���� &#171;��������������� �.�.�.&#187;) �� ��������� ����������� ������������ ���������, � ��� 2000 ���� ��������� �� ��������� ���������� ���������� �������� ��������; � ������ 2001 ���� &#8212; �������� ����� &#171;��������������� �.�.�.&#187;. � 2004 ���� ������ � ��������� �� ��������� ��������� ������������ ���������, � 2005 �� 2006 ��� &#8212; ����������� ������������ ��������� ����������. </FONT></P>


</body>
</html>

