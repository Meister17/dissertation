<!--
ID:36498305
TITLE:���������� ������������� ������ ����� �������  "������" (��������)
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:11
ISSUE_DATE:2006-06-15
RECORD_DATE:2006-08-02


-->
<html>
<head>
<title>
36498305-���������� ������������� ������ ����� �������  "������" (��������)
</title>
</head>
<body >


<B>
<P align=center><FONT>������ ��������� ��� ������������ ������������ ������<BR>� ����������� ������ ��������� ��������&#8211;������� 2006 �.</FONT></P></B>
<TABLE cellSpacing=2 cellPadding=7 width=603 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="6%" height=5>
<P align=center><FONT>&#8470; �/�</FONT></P></TD>
<TD vAlign=top width="35%" height=5><B>
<P align=center><FONT>�������� ��������</FONT></B></P></TD>
<TD vAlign=center width="15%" height=5><B>
<P align=center><FONT>��������</FONT></B></P></TD>
<TD vAlign=center width="15%" height=5><B>
<P align=center><FONT>�������</FONT></B></P></TD>
<TD vAlign=center width="15%" height=5><B>
<P align=center><FONT>������</FONT></B></P></TD>
<TD vAlign=center width="14%" height=5><B>
<P align=center><FONT>�������</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>1</FONT></P></TD>
<TD vAlign=top width="35%">
<P align=center><FONT>2</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>3</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>4</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>5</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>6</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>1</FONT></P></TD>
<TD vAlign=top width="35%">
<P><FONT>������������� �������� ������������ ������</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>&#8211;</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>9&#8211;18</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>&#8211;</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>18&#8211;26</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>2</FONT></P></TD>
<TD vAlign=top width="35%">
<P><FONT>������������� �������. �������� ������������� � ��������</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>&#8211;</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>16&#8211;18</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>20&#8211;22</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>&#8211;</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>3</FONT></P></TD>
<TD vAlign=top width="35%">
<P><FONT>����������� �������-�������� ��������.</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>&#8211;</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>16-18</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>13&#8211;15</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>&#8211;</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>4</FONT></P></TD>
<TD vAlign=top width="35%">
<P><FONT>������������� ����, ���������������, ���������� � �����</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>&#8211;</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>10&#8211;14</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>&#8211;</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>18&#8211;22</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>5</FONT></P></TD>
<TD vAlign=top width="35%">
<P><FONT>���������� ������������ �����. ���������� �������� � ���������</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>21&#8211;23</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>&#8211;</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>22&#8211;24</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>&#8211;</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>6</FONT></P></TD>
<TD vAlign=top width="35%">
<P><FONT>����������� ���������� ���������� ������������ VIP &#8211; ��������</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>25&#8211;27</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>&#8211;</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>23&#8211;25</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>&#8211;</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%" height=42>
<P align=center><FONT>7</FONT></P></TD>
<TD vAlign=top width="35%" height=42>
<P><FONT>��������� ���������� ������������ ������</FONT></P></TD>
<TD vAlign=top width="15%" height=42>
<P align=center><FONT>21&#8211;23</FONT></P></TD>
<TD vAlign=top width="15%" height=42>
<P align=center><FONT>&#8211;</FONT></P></TD>
<TD vAlign=top width="15%" height=42>
<P align=center><FONT>22&#8211;24</FONT></P></TD>
<TD vAlign=top width="14%" height=42>
<P align=center><FONT>&#8211;</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>8</FONT></P></TD>
<TD vAlign=top width="35%">
<P><FONT>���������� ��������� �����������, �����������, �����. ���������� ������ � ������������</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>&#8211;</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>17&#8211;21</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>&#8211;</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>4&#8211;8</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>9</FONT></P></TD>
<TD vAlign=top width="35%">
<P><FONT>������� ������ ��������� �� ��������</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>&#8211;</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>16&#8211;18</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>&#8211;</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>4&#8211;6</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>10</FONT></P></TD>
<TD vAlign=top width="35%">
<P><FONT>���������� ����������</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>&#8211;</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>27&#8211;28</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>&#8211;</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>20&#8211;21</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>11</FONT></P></TD>
<TD vAlign=top width="35%">
<P><FONT>����� ���������������� ����������� �� 240-������� ���������</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>�� ���� ������ �����</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>�� ���� ������ �����</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>�� ���� ������ �����</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>�� ���� ������ �����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center><FONT>12</FONT></P></TD>
<TD vAlign=top width="35%">
<P><FONT>�������������� ���������������� �����������</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>�� ���� ������ �����</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>�� ���� ������ �����</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>�� ���� ������ �����</FONT></P></TD>
<TD vAlign=top width="14%">
<P align=center><FONT>�� ���� ������ �����</FONT></P></TD></TR></TBODY></TABLE>
<P align=justify><FONT>����� ������������� ������� ��������� ����� "������" (��������) �������� ������� �������� ������������������ 2&#8211;3 ��� �� �������� ���������� ��������� ��������:</FONT></P>
<P align=justify><FONT>1.��������� ���� �������� � ������������ �����.</FONT></P>
<P align=justify><FONT>2.���������� ������ ������������� �������� �����.</FONT></P>
<P align=justify><FONT>3.���������� �����. ���������� �������.</FONT></P>
<P align=justify><FONT>4.���������� ������� ��������������� ������������ � ��������. ���������� ��������������� ���������.</FONT></P>
<P align=justify><FONT>5.������� ������� ����������� ��������. ��������� �������������� �����.</FONT></P>
<P align=justify><FONT>6.����������� ������ � ������������ ��������.</FONT></P>
<P align=justify><FONT>7.���� ������������ ��������� � �������������� ������� � ������������ �����.</FONT></P>
<P align=justify><FONT>8.����������� ����� ������ � ������ �������� �����.</FONT></P>
<P align=justify><FONT>9.������� ������������� ����� � ���������� �������������� ��������.</FONT></P>
<P align=justify><FONT>10.�������� ����������� � ���������������� ����� ���� ����������� �������.</FONT></P>
<P align=justify><FONT>11.����������� ���������� �� �� � ����������� ��������� ������. ���������� ������.</FONT></P>
<P align=justify><FONT>12.�������������-������������� ������ ������������� ����� � ����������� ��������.</FONT></P>
<P align=justify><FONT>13.���������� ����������. ����, ������.</FONT></P>
<P align=justify><FONT>14.����������� ����� ����������� � �������������� � �������������� ���������.</FONT></P>
<P align=justify><FONT>15.����������� ����� ����������� ������������ ��������. ������������� ���������� ������.</FONT></P>
<P align=justify><FONT>16.���������� �������� ��� ���������� ��� � ������ � �� �� ���������.</FONT></P>
<P align=justify><FONT>17.���������� �����. ���������� ����������� �������.</FONT></P>
<P align=justify><FONT>18.������������� ���� � ���������� � ����������� �������������������.</FONT></P>
<P align=justify><FONT>19.���������� ��������� �����������, �����������, �����. ���������� ������ � ���������� ������������.</FONT></P>
<P align=justify><FONT>20.�������������� ���� �� �����������.</FONT></P><U>
<P align=justify><FONT>����� ���������� �������:</FONT></U><FONT> ���������� �������� ��������������� ������ ��� ���������� �� (��-� �����������, 84). ������ 2 ��������� ������ (��. ������������, 14, ��. ����� "�������� �����������").</FONT></P>
<P align=justify><FONT>�� ���������� �������� �� ��������� �������� ������������ ����������� � ����� �� �������� ����������, ����������� �� �� � �������� ��������� � ���������� ���������� ������������ ��������������� ������������.</FONT></P><I><B>
<P align=justify><FONT>������ �� �������� ����������� ���������� ������ "������":</FONT></P><FONT face=Wingdings>
<P align=justify><FONT>((</FONT></FONT><FONT> (495) 231-79-66, 101-41-88</FONT></P>
<P align=justify><FONT>���� (095) 231-7966, 237-34-30</FONT></P>
<P align=justify><FONT>E-mail: info@mirbis.ru </FONT></P>
<P align=justify><FONT>http://www.mirbis ru</FONT></P></B></I>


</body>
</html>

