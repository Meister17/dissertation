<!--
ID:35326191
TITLE:�� &#171;����������&#187;
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:
NUMBER:4
ISSUE_DATE:2005-04-30
RECORD_DATE:2005-09-23


-->
<html>
<head>
<title>
35326191-�� &#171;����������&#187;
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgwhite>
<TD></TD></TR>
<TR class=bodycolor>
<TD height=0>
<P class=Rubrika>������������ ���� &#171;����������&#187; (�������� � ������������ ����������������) 
<P class=Main align=center>��������������� ����� 2192, ��� 044583337, 
<P class=Main align=center>�������� �����: 111024, �. ������, ��. ���������, 46� 
<P class=Main></P></TD></TR>
<TR class=bgwhite>
<TD align=middle>
<P class=Rubrika>������ �� 1 ������ 2005 ���� 
<P align=center>
<TABLE cellSpacing=0 cellPadding=0 width="95%" borderColorLight=#cccccc border=2>
<TBODY>
<TR vAlign=top align=middle>
<TD vAlign=center width=48>
<P><B>&#8470; �/�</B></P></TD>
<TD vAlign=center width=419>
<P><B>������������ ������</B></P></TD>
<TD width=84>
<P class=bold>�� ����� ���. �������</P></TD></TR>
<TR>
<TD height=0>
<P align=center><STRONG>1</STRONG></P></TD>
<TD>
<P align=center><STRONG>2</STRONG></P></TD>
<TD>
<P align=center><STRONG>3</STRONG></P></TD></TR>
<TR>
<TD colSpan=3 height=0>
<P align=center><B>������</B></P></TD></TR>
<TR>
<TD>
<P>1.</P></TD>
<TD>
<P>�������� ��������</P></TD>
<TD>
<P align=right>11043</P></TD></TR>
<TR>
<TD>
<P>2.</P></TD>
<TD>
<P>�������� ��������� ����������� � ����������� ����� ���������� ���������</P></TD>
<TD>
<P align=right>525815</P></TD></TR>
<TR>
<TD>
<P>2.1.</P></TD>
<TD>
<P>������������ �������</P></TD>
<TD>
<P align=right>8499</P></TD></TR>
<TR>
<TD>
<P>3.</P></TD>
<TD>
<P>�������� � ��������� ������������</P></TD>
<TD>
<P align=right>10955</P></TD></TR>
<TR>
<TD>
<P>4.</P></TD>
<TD>
<P>������ �������� � �������� ������ ������</P></TD>
<TD>
<P align=right>44192</P></TD></TR>
<TR>
<TD>
<P>5.</P></TD>
<TD>
<P>������ ������� �������������</P></TD>
<TD>
<P align=right>211216</P></TD></TR>
<TR>
<TD>
<P>6.</P></TD>
<TD>
<P>������ �������� � �������������� ������ ������, ������������ �� ���������</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>7.</P></TD>
<TD>
<P>������ �������� � ������ ������, ��������� � ������� ��� �������</P></TD>
<TD>
<P align=right>1251</P></TD></TR>
<TR>
<TD>
<P>8.</P></TD>
<TD>
<P>�������� ��������, �������������� ������ � ������������ ������</P></TD>
<TD>
<P align=right>4611</P></TD></TR>
<TR>
<TD>
<P>9.</P></TD>
<TD>
<P>���������� �� ��������� ���������</P></TD>
<TD>
<P align=right>1269</P></TD></TR>
<TR>
<TD>
<P>10.</P></TD>
<TD>
<P>������ ������</P></TD>
<TD>
<P align=right>11415</P></TD></TR>
<TR>
<TD>
<P>11.</P></TD>
<TD>
<P>����� �������</P></TD>
<TD>
<P align=right>821767</P></TD></TR>
<TR align=middle>
<TD colSpan=3>
<P><B>II. �������</B></P></TD></TR>
<TR>
<TD>
<P>12.</P></TD>
<TD>
<P>������� ������������ ����� ���������� ���������</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>13.</P></TD>
<TD>
<P>�������� ��������� �����������</P></TD>
<TD>
<P align=right>1797</P></TD></TR>
<TR>
<TD>
<P>14.</P></TD>
<TD>
<P>�������� �������� (����������� �����������)</P></TD>
<TD>
<P align=right>773647</P></TD></TR>
<TR>
<TD>
<P>14.1.</P></TD>
<TD>
<P>������ ���������� ���</P></TD>
<TD>
<P align=right>30624</P></TD></TR>
<TR>
<TD>
<P>15.</P></TD>
<TD>
<P>���������� �������� �������������</P></TD>
<TD>
<P align=right>4692</P></TD></TR>
<TR>
<TD>
<P>16.</P></TD>
<TD>
<P>������������� �� ������ ���������</P></TD>
<TD>
<P align=right>1659</P></TD></TR>
<TR>
<TD>
<P>17.</P></TD>
<TD>
<P>������ �������������</P></TD>
<TD>
<P align=right>1030</P></TD></TR>
<TR>
<TD>
<P>18.</P></TD>
<TD>
<P>������� �� ��������� ������ �� �������� �������������� ���������� ���������, ������ ��������� ������� � �� ��������� � ����������� �������� ���</P></TD>
<TD>
<P align=right>276</P></TD></TR>
<TR>
<TD>
<P>19.</P></TD>
<TD>
<P>����� ������������</P></TD>
<TD>
<P align=right>783101</P></TD></TR>
<TR align=middle>
<TD colSpan=3>
<P class=bold>III. ��������� ����������� �������</P></TD></TR>
<TR>
<TD>
<P>20.</P></TD>
<TD>
<P>�������� ���������� (����������)</P></TD>
<TD>
<P align=right>20200</P></TD></TR>
<TR>
<TD>
<P>20.1.</P></TD>
<TD>
<P>������������������ ������������ ����� � ����</P></TD>
<TD>
<P align=right>20200</P></TD></TR>
<TR>
<TD>
<P>20.2.</P></TD>
<TD>
<P>������������������ ����������������� �����</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>20.3.</P></TD>
<TD>
<P>�������������������� �������� ������� ������������� ��������� �����������</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>21.</P></TD>
<TD>
<P>����������� �����, ����������� � ����������</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>22.</P></TD>
<TD>
<P>����������� �����</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>23.</P></TD>
<TD>
<P>���������� �������� �������</P></TD>
<TD>
<P align=right>289</P></TD></TR>
<TR>
<TD>
<P>24.</P></TD>
<TD>
<P>������� ������� �������� � ����������� �������, �������� �� ����������� �������� (�������)</P></TD>
<TD>
<P align=right>807</P></TD></TR>
<TR>
<TD>
<P>25.</P></TD>
<TD>
<P>����� � ���������������� ������� ������� ��� � ������������ ��������� ����������� (������������ ������ ������� ���)</P></TD>
<TD>
<P align=right>17897</P></TD></TR>
<TR>
<TD>
<P>26.</P></TD>
<TD>
<P>������� � ������������� (������) �� �������� ������</P></TD>
<TD>
<P align=right>1087</P></TD></TR>
<TR>
<TD>
<P>27.</P></TD>
<TD>
<P>����� ���������� ����������� �������</P></TD>
<TD>
<P align=right>38666</P></TD></TR>
<TR>
<TD>
<P>28.</P></TD>
<TD>
<P>����� ��������</P></TD>
<TD>
<P align=right>821767</P></TD></TR>
<TR align=middle>
<TD colSpan=3>
<P><B>IV. ������������� �������������</B></P></TD></TR>
<TR>
<TD>
<P>29.</P></TD>
<TD>
<P>����������� ������������� ��������� �����������</P></TD>
<TD>
<P align=right>15043</P></TD></TR>
<TR>
<TD>
<P>30.</P></TD>
<TD>
<P>��������, �������� ��������� ������������</P></TD>
<TD>
<P align=right>1200</P></TD></TR></TBODY></TABLE>
<P class=Main align=center>��������, ���������� ��������� �� ���� ������� ������� V &#171;����� �������������� ����������&#187;, �� �������������� 
<P> 
<P class=Rubrika>����� � �������� � ������� �� 2004 ���. 
<P class=&#171;Main&#187;>
<P class=&#171;Main&#187;>
<P align=center>
<TABLE cellSpacing=0 cellPadding=0 width="95%" borderColorLight=#cccccc border=2>
<TBODY>
<TR vAlign=top align=middle>
<TD vAlign=center width=44>
<P><B>&#8470; �/�</B></P></TD>
<TD vAlign=center width=422>
<P><B>������������ ������</B></P></TD>
<TD width=85>
<P><B>�� ���. ������</B></P></TD></TR>
<TR>
<TD height=0>
<P align=center><STRONG>1</STRONG></P></TD>
<TD>
<P align=center><STRONG>2</STRONG></P></TD>
<TD>
<P align=center><STRONG>3</STRONG></P></TD></TR>
<TR>
<TD colSpan=3 height=0>
<P></P>
<P class=bold align=center>�������� ���������� � ����������� ������ ��:</P></TD></TR>
<TR>
<TD>
<P>1</P></TD>
<TD>
<P>���������� ������� � ��������� ������������</P></TD>
<TD>
<P align=right>538</P></TD></TR>
<TR>
<TD>
<P>2</P></TD>
<TD>
<P>����, ��������������� �������� (����������� ������������)</P></TD>
<TD>
<P align=right>21000</P></TD></TR>
<TR>
<TD>
<P>3</P></TD>
<TD>
<P>�������� ����� �� ���������� ������ (�������)</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>4</P></TD>
<TD>
<P>������ ����� � ������������� �������</P></TD>
<TD>
<P align=right>2434</P></TD></TR>
<TR>
<TD>
<P>5</P></TD>
<TD>
<P>������ ����������</P></TD>
<TD>
<P align=right>764</P></TD></TR>
<TR>
<TD>
<P>6</P></TD>
<TD>
<P>����� ��������� ���������� � ����������� �������</P></TD>
<TD>
<P align=right>24736</P></TD></TR>
<TR vAlign=top align=middle>
<TD colSpan=3>
<P><B>�������� ���������� � ����������� ������� ��:</B></P></TD></TR>
<TR>
<TD>
<P>7</P></TD>
<TD>
<P>������������ ��������� ��������� �����������</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>8</P></TD>
<TD>
<P>������������ ��������� �������� (����������� �����������)</P></TD>
<TD>
<P align=right>2430</P></TD></TR>
<TR>
<TD>
<P>9</P></TD>
<TD>
<P>���������� �������� ��������������</P></TD>
<TD>
<P align=right>266</P></TD></TR>
<TR>
<TD>
<P>10</P></TD>
<TD>
<P>����� ��������� ���������� � ����������� ��������</P></TD>
<TD>
<P align=right>2696</P></TD></TR>
<TR>
<TD>
<P>11</P></TD>
<TD>
<P>������ ���������� � ����������� ������</P></TD>
<TD>
<P align=right>22040</P></TD></TR>
<TR>
<TD>
<P>12</P></TD>
<TD>
<P>������ ������ �� �������� � ������� ��������</P></TD>
<TD>
<P align=right>6341</P></TD></TR>
<TR>
<TD>
<P>13</P></TD>
<TD>
<P>������ ������ �� �������� � ����������� �������</P></TD>
<TD>
<P align=right>75</P></TD></TR>
<TR>
<TD>
<P>14</P></TD>
<TD>
<P>������ ������ �� �������� � ������������ ��������� � ������� ����������� �������������</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>15</P></TD>
<TD>
<P>������ ������ �� ���������� ����������� ������</P></TD>
<TD>
<P align=right>-1</P></TD></TR>
<TR>
<TD>
<P>16</P></TD>
<TD>
<P>������������ ������</P></TD>
<TD>
<P align=right>6329</P></TD></TR>
<TR>
<TD>
<P>17</P></TD>
<TD>
<P>������������ �������</P></TD>
<TD>
<P align=right>16</P></TD></TR>
<TR>
<TD>
<P>18</P></TD>
<TD>
<P>������ ������ �� ������� ��������</P></TD>
<TD>
<P align=right>538</P></TD></TR>
<TR>
<TD>
<P>19</P></TD>
<TD>
<P>������ ������ ������������ ������</P></TD>
<TD>
<P align=right>325</P></TD></TR>
<TR>
<TD>
<P>20</P></TD>
<TD>
<P>���������������-�������������� �������</P></TD>
<TD>
<P align=right>32228</P></TD></TR>
<TR>
<TD>
<P>21</P></TD>
<TD>
<P>������� �� ��������� ������</P></TD>
<TD>
<P align=right>-403</P></TD></TR>
<TR>
<TD>
<P>22</P></TD>
<TD>
<P>������� �� ���������������</P></TD>
<TD>
<P align=right>3000</P></TD></TR>
<TR>
<TD>
<P>23</P></TD>
<TD>
<P>����������� ������ (������� ����� �� �������)</P></TD>
<TD>
<P align=right>2081</P></TD></TR>
<TR>
<TD>
<P>24</P></TD>
<TD>
<P>������� �� �������� ������</P></TD>
<TD>
<P align=right>919</P></TD></TR></TBODY></TABLE>
<P class=Main> 
<P class=Rubrika>���������� �� ������ ������������� ��������, 
<P class=Rubrika>�������� �������� �� �������� ������������ ���� � ���� ������� �� 
<P class=Rubrika>1 ������ 2005 ���� 
<P align=center>
<TABLE cellSpacing=0 cellPadding=0 width="95%" borderColorLight=#cccccc border=2>
<TBODY>
<TR vAlign=top align=middle>
<TD vAlign=center width=42 height=39>
<P><B>&#8470; �/�</B></P></TD>
<TD vAlign=center width=421>
<P class=bold>������������ ������</P></TD>
<TD width=88>
<P class=bold>�� ����� ���. �������</P></TD></TR>
<TR>
<TD height=0>
<P align=center><STRONG>1</STRONG></P></TD>
<TD>
<P align=center><STRONG>2</STRONG></P></TD>
<TD>
<P align=center><STRONG>3</STRONG></P></TD></TR>
<TR>
<TD>
<P>1</P></TD>
<TD>
<P>����������� �������� (�������), ���. ���.</P></TD>
<TD>
<P align=right>38664.0</P></TD></TR>
<TR>
<TD>
<P>2</P></TD>
<TD>
<P>����������� �������� ������������� ����������� ������� (��������), �������</P></TD>
<TD>
<P align=right>13.0</P></TD></TR>
<TR>
<TD>
<P>3</P></TD>
<TD>
<P>����������� �������� ������������� ����������� ������� (��������), �������</P></TD>
<TD>
<P align=right>11.0</P></TD></TR>
<TR>
<TD>
<P>4</P></TD>
<TD>
<P>��������� ������ �� ��������� ������ �� ������, ������� � ������������ � ��� �������������, ���. ���.</P></TD>
<TD>
<P align=right>39689.0</P></TD></TR>
<TR>
<TD>
<P>5</P></TD>
<TD>
<P>���������� �������������� ������ �� ��������� ������ �� ������, ������� � ������������ � ��� �������������, ���. ���.</P></TD>
<TD>
<P align=right>39689.0</P></TD></TR>
<TR>
<TD>
<P>6</P></TD>
<TD>
<P>��������� ������ �� ��������� ������, ���. ���.</P></TD>
<TD>
<P align=right>855.0</P></TD></TR>
<TR>
<TD>
<P>7</P></TD>
<TD>
<P>���������� �������������� ������ �� ��������� ������, ���. ���.</P></TD>
<TD>
<P align=right>855.0</P></TD></TR></TBODY></TABLE>
<P class=Main align=left> 
<P class=Main align=left>�� ������ ����������� ����� &#171;����� �����&#187;, ������������� ������, ����� � �������� � �������, ����� �� ������ ������������� ��������, �������� �������� �� �������� ������������ ���� � ���� ������� �������� ���������� �� ���� ������������ ���������� ���������� ��������� ������������� ����� &#171;����������&#187; �������� � ������������ ���������������� �� ��������� �� 01 ������ 2005 � 
<P class=&#171;Main&#187;>
<TABLE cellSpacing=0 cellPadding=0 width="100%" align=center border=1>
<TBODY>
<TR>
<TD width="60%">
<P><STRONG>������������ ����������� �����������: </STRONG></P></TD>
<TD width="40%">
<P><STRONG>��� &#171;����� �����&#187;</STRONG></P></TD></TR>
<TR>
<TD>
<P>����� ��������:</P></TD>
<TD>
<P>001621</P></TD></TR>
<TR>
<TD>
<P>����� ������� � ������ ��������:</P></TD>
<TD>
<P>200</P></TD></TR>
<TR>
<TD>
<P>���� ������ �������� �� ������������� ����������� ������������:</P></TD>
<TD>
<P>06.09.2002 �.</P></TD></TR>
<TR>
<TD>
<P>���� �������� ��������:</P></TD>
<TD>
<P>5 ���</P></TD></TR>
<TR>
<TD>
<P>������������ ������ ��������� ��������:</P></TD>
<TD>
<P>������������ �������� ���������� ��������</P></TD></TR>
<TR>
<TD>
<P>�������, ���, �������� ������������:</P></TD>
<TD>
<P>�������� ����� �������������</P></TD></TR>
<TR>
<TD>
<P>�������, ���, ��������, ��������� ����, ����������� ����������� ����������:</P></TD>
<TD>
<P>�������� ����� �������������, ����������� �������� ��� &#171;����� �����&#187; ����� �� 09.04.2001, ������� &#8470; 8 ��������� ��� &#171;����� �����&#187; �� 18.12.2001 �.</P></TD></TR></TBODY></TABLE>
<TABLE cellSpacing=0 cellPadding=0 width="95%" border=0>
<TBODY>
<TR>
<TD height=0>
<P class=top>������������ ��������� ��������� �����������</P></TD>
<TD>
<P><STRONG>��������� ����� ��������</STRONG></P></TD></TR>
<TR>
<TD height=0>
<P class=top>������� ��������� ��������� �����������</P></TD>
<TD>
<P><STRONG>��������� ����� ����������</STRONG></P></TD></TR></TBODY></TABLE></P></TD></TR></TBODY></TABLE>


</body>
</html>

