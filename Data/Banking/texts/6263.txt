<!--
ID:35374922
TITLE:� ��������� ������ � ������� ����������� �������
SOURCE_ID:3144
SOURCE_NAME:������ � ������
AUTHOR:
NUMBER:8
ISSUE_DATE:2005-08-31
RECORD_DATE:2005-10-06


-->
<html>
<head>
<title>
35374922-� ��������� ������ � ������� ����������� �������
</title>
</head>
<body >


<I>
<P align=center><FONT>����������� ������� � ������������ ������ ����� ������ ��������:</FONT></P>
<P align=center></I><B><A name=OLE_LINK2><FONT>27 ���� 2005 ���� ������� ����������� ������� ����� ������</FONT></B><FONT> � ������������ � ������ ��������� �������� ���������� �� ������ ������������ ��������� ���������� ������ �� �� ������������ ����������� � ������� � ������� ����������� �������. �� ��������� ���� ������� ������� � ��������� ������������� ���������� � ������������ ����������� � ������� � ������� ����������� ������� �� 3 ��������� ������������:</FONT>
<TABLE cellSpacing=1 cellPadding=7 width=633 align=center border=1>
<TBODY>
<TR>
<TD vAlign=center width="55%">
<P align=center><FONT>�������� ��������� �����������</FONT></P></TD>
<TD vAlign=center width="10%">
<P align=center><FONT>���. &#8470;</FONT></P></TD>
<TD vAlign=center width="34%">
<P align=center><FONT>���������������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="55%">
<P><FONT>�������� � ������������ ����������������<BR>������������ ���� &#8220;�������������&#8221;</FONT></P></TD>
<TD vAlign=center width="10%">
<P align=center><FONT>1222</FONT></P></TD>
<TD vAlign=center width="34%">
<DIR>
<P align=center><FONT>�. ����-���, ��������� ������� </FONT></P></DIR></TD></TR>
<TR>
<TD vAlign=center width="55%">
<P><FONT>�������� ����������� �������� ������������ ���� &#8220;������������� ���� ����� ������ ���������&#8221;</FONT></P></TD>
<TD vAlign=center width="10%">
<P align=center><FONT>1717</FONT></P></TD>
<TD vAlign=center width="34%">
<DIR>
<P align=center><FONT>�. ������</FONT></P></DIR></TD></TR>
<TR>
<TD vAlign=center width="55%" height=18>
<P><FONT>��� &#8220;����������&#8221; (���)</FONT></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center><FONT>67</FONT></P></TD>
<TD vAlign=center width="34%" height=18>
<DIR>
<P align=center><FONT>�. ������</FONT></P></DIR></TD></TR></TBODY></TABLE></P><B>
<P align=justify><FONT>3 ������� 2005 ���� ��������� ����������� ������� ����� ������</FONT></B><FONT> �� ��������� ������������ ���� ������� ������� � ��������� ������������� ���������� � ������������ ����������� � ������� � ������� ����������� ������� ��� �� &#8220;������&#8221; (pe�. &#8470; 3098, �.&nbsp;������), �� &#8220;�������������&#8221; (���) (pe�. &#8470; 2795, �.&nbsp;������), ��� ��� &#8220;����������&#8221; (pe�. &#8470; 1035, �. ������). �� ��������� ����� ������� ������� � �������������� �������� �� ����������� �� ������ �������� ������� ���������� ��� � ������ � ����������� ������ ������������� ����� &#8220;���&#8221; (�������� � ������������ ����������������) �� &#8220;���&#8221; (���) (pe�. &#8470; 3413, �.&nbsp;������, ����������� �������), ������� ��������� �� ���������� ����� �������� �������. � ������������ � ����������� ����������������� ��������� ���� ����� ���������� ���������� ������� ����������� �������.</FONT></P><B>
<P align=justify><FONT>10 ������� 2005 ���� ������� ����������� ������� ����� ������</FONT></B><FONT> � ������������ � ������ ��������� �������� ���������� �� ������ ������������ ��������� ���������� ������ �� �� ������������ ����������� � ������� � ������� ����������� �������. �� ��������� ���� ������� ������� � ��������� ������������� ���������� � ������������ ����������� � ������� � ������� ����������� ������� �� 8 ��������� ������������:</FONT>
<TABLE cellSpacing=1 width=633 align=center border=1>
<TBODY>
<TR>
<TD vAlign=center width="55%" height=18>
<P align=center><FONT>�������� ��������� �����������</FONT></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center><FONT>���. &#8470;</FONT></P></TD>
<TD vAlign=center width="34%" height=18>
<P align=center><FONT>���������������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="55%" height=18>
<P><FONT>��� &#8220;��������&#8221; (���)</FONT></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center><FONT>2650</FONT></P></TD>
<TD vAlign=center width="34%" height=18>
<P align=center><FONT>�. �����-�������, ���������� ����� </FONT></P></TD></TR>
<TR>
<TD vAlign=center width="55%" height=18>
<P><FONT>��� �� &#8220;���� ���&#8221;</FONT></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center><FONT>2273</FONT></P></TD>
<TD vAlign=center width="34%" height=18 rowSpan=7>
<P align=center><FONT>�. ������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="55%" height=18>
<P><FONT>��� ��� &#8220;�������&#8221;</FONT></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center><FONT>1967</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="55%" height=18>
<P><FONT>��� &#8220;�����������&#8221; (���)</FONT></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center><FONT>2468</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="55%" height=18>
<P><FONT>&#8220;����-����&#8221; (���)</FONT></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center><FONT>2913</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="55%" height=18>
<P><FONT>��� �� &#8220;���-����&#8221;</FONT></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center><FONT>2734</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="55%" height=18>
<P><FONT>��� &#8220;��������������� ����&#8221;</FONT></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center><FONT>3277</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="55%" height=18>
<P><FONT>��� &#8220;����������&#8221;</FONT></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center><FONT>2765</FONT></P></TD></TR></TBODY></TABLE></P><B>
<P align=justify><A name=OLE_LINK1><FONT>17 ������� 2005&nbsp;���� ������� ����������� ������� ����� ������</FONT></B><FONT> � ������������ � ������ ��������� �������� ���������� �� ������ ������������ ��������� ���������� ������ �� �� ������������ ����������� � ������� � ������� ����������� �������. �� ��������� ���� ������� ������� � ��������� ������������� ���������� � ������������ ����������� � ������� � ������� ����������� ������� �� 7 ��������� ������������:</FONT></P>
<P align=right>
<TABLE cellSpacing=1 cellPadding=7 width=633 align=center border=1>
<TBODY>
<TR>
<TD vAlign=center width="55%" height=18>
<P align=center><FONT>�������� ��������� �����������</FONT></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center><FONT>���. &#8470;</FONT></P></TD>
<TD vAlign=center width="34%" height=18>
<P align=center><FONT>���������������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="55%" height=18>
<P><FONT>��� ��� &#8220;�����&#8221;</FONT></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center><FONT>3233</FONT></P></TD>
<TD vAlign=center width="34%" height=18 rowSpan=5>
<P align=center><FONT>�. ������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="55%" height=18>
<P><FONT>�� &#8220;������ ������� ����&#8221;, ���</FONT></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center><FONT>2537</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="55%" height=18>
<P><FONT>��� �� &#8220;�����������&#8221;</FONT></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center><FONT>2767</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="55%" height=18>
<P><FONT>��� ��� &#8220;���������&#8221;</FONT></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center><FONT>2313</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="55%" height=18>
<P><FONT>��� ��� &#8220;���������������&#8221;</FONT></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center><FONT>346</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="55%" height=18>
<P><FONT>��� &#8220;�� &#8220;����������&#8221;</FONT></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center><FONT>2975</FONT></P></TD>
<TD vAlign=center width="34%" height=18>
<P align=center><FONT>�. ������������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="55%" height=18>
<P><FONT>��� &#8220;������������&#8221;</FONT></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center><FONT>385</FONT></P></TD>
<TD vAlign=center width="34%" height=18>
<P align=center><FONT>�. ������</FONT></P></TD></TR></TBODY></TABLE></P></A>


</body>
</html>

