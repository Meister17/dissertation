<!--
ID:38440110
TITLE:������� ������� � ������ �� ��������������� ���������� ����
SOURCE_ID:10030
SOURCE_NAME:�����: ������� ����. ����� ���
AUTHOR:����� ������ &#8211; ������ �. �.
NUMBER:5
ISSUE_DATE:2007-10-31
RECORD_DATE:2007-11-23


-->
<html>
<head>
<title>
38440110-������� ������� � ������ �� ��������������� ���������� ����
</title>
</head>
<body >


<P>�� ��������� ���� ������� ������������� ������������ ����� �������� ���������� ���������� ���������, ��� ��������� ���� ������ ����� �� ������� �������� ������ �� ����� ���������� ��������� �� ���� ���������.</P><B>
<P align=center>����� ��������� �����</P></B>
<P>����� ���������� ��������� ��������� ���������� � ������� �� ������ 2006 �. �������� 43785, ��� ���������� 11,8 ������������ ��������� �� ������ 10 ���. ������� � �������� ������ 16 ���. �� ����� ����������, ��� ���������� � �������� ������� � ���������� ������� � ������� �� 2006 �., ������ ����������� �� ������ ����� � ����. ��� ����� ���������� ������� � �������������� �������� �� ����� ���������� ��������� ������ �������� ������ ���, �������� � �����.</P>
<P>�� ��������� � ������� ��������, ����������� �������� ������������� ����� ���������� ���������, ������� ������������������ � 2006 �. �������� ���������� ������� ��������� ���������� ���� � ������ ��������� �� ����� ���������� ��������� �� ��� �������, �������, ������, ����������, ��������, ��� � �����.</P>
<P>�� ������ ����� �������� � ������� ��������� ��������� ���������� �� ���� �������������� ���� ���������� 23456, ��� 53,57%, �� ���� ������ &#8211; 15135 ���������,</P>
<P>��� 34,57%. ��� ���� ������������� ������� ���������� ��������� �������������� ���� �� 2006 �. �������� 4,5%, ������ &#8211; 3,8%, � ��������� ������������ &#8211; 2,4%.</P>
<P>���� ������� ��������, ��� ���� ������������ ������������ ��������� ���������� �����������, ������ �����, �� ���� �������� ���������, ���������� ������������� ��� �������������� ����� � ������� ������������� ���������� &#8211; ����������� � ���������� ����� � � ����������� ����������. ����� ����� ����� ����� �������� ���������, ���������� ������ �������������, �� ������ ����� �������, ���������� ����������. ���������� ��������� ���������: ���� �������������� ����� �� ��������� ���� � ���������� ������ ��������� ������ ����� ��������� � �������� �������� �������� ���������� ��������� (������ ����� � ������� � ���������), �� ����� �������� ������� � ����� �������� ��������� �� ������� � ������� ���������� ���������� ����.</P><B>
<P>�������������� ���������� ������������ ���� �����������</P></B>
<P>������� ����� �������� ��������� � ������ �� ��������� ���� ����������: � ������� �� ������ 10 ���. ���������������� �������� ���������� ����� 15,5 �������������� �������� �����. �� 2006 �. ������� ���������� ���������� �������� 4% � ��� � �������� ��������� �� ���� ��������� ����� ���������� ��������� ��������������� �������. ��� ���� ���������� ����������� ���������� ���� ���������� �������� ��������� � ������������� ����������, ��� ��������������� � ���, ��� ��������� ���������� ���������� �������������� �������� ������ ������ ��� ���������� � ������������ ���������, ��� ��� �������������� ������, ��������������� ��������.</P>
<P>���������� ����������� ��������� ����������, ������������� ���������� ������������ �� ������������ �������� (Terminales de punto de venta &#8211; TPV), ����� ���������� ����� ����������� �������. �� 2006 �. �� ����� ������� �� 16,5% � ��������� � ����� ��������� 1,3 ��� ����������. �������� ������������ ����� � ���� ������� ������ �����, ������������ ������� ���������� ������������� TPV �� 22,4%.</P>
<P>��������� ����� ����� ���������� � TPV, ������������� ��� ���������, ��� � ��������� �����, ����������� ���������� ������������� ���������� ������������ ����������� ����. ���� ���� � 2006 �. �������� 9%, � ���������� ���������� ������������� ���������� ������������ ����, ����������� � �������, ��������� � ������� 90 ���. ��������� ����� ��������� 57% �� ������ ���������� ����������� � ������� ����.</P>
<P>�� ����� ������������� ��������� ���� ��������� �������� �����, �� ���� ������� �������� 42% (� 2005 �. &#8211; 38%). ��� ���� �� ���� ��������� ������ ����������� 32% ����� ��������� ����, � �� ���� �������� ����������� ������ &#8211; 10%. � ���� �������, �������������� ����� ��������� ���� �� ����� ��������� ���� � 28% � 2005 �. �� 25% � 2006 �. ��� �������� ����� ��������� ����, �� ����� ������������ ��������� �� ���������: ������ ����� ��������� �� ����� ��������� (59%), �� ������ ����� &#8211; ����� (32%).</P><B>
<P>��� ���������� �������</P></B>
<P>������� ���������� �������� � ������� �� ���� ��������� ���������� ���������� � ����� �� 2006 �. �������� ����������� ���������� � ���������� ����� ���� ����������� �� ���� ���������. ��� ���� ���� ��������� ����������� � �������������� ����� ��������� ���� ���������� ����� ����������, �� ����� ������ ��� ������������� �������.</P>
<P>��� ���������� � �������� ������� ����� �������, ������ � ������ ����� ����������� ��������� ��������� ����������, � ����� ������������� � ����������� ����� �������� ���������, � 2006 �. ��������� �������� ���� ���������� �������, �������������� ���������� ��������. ���, ��������, �������������� �����, �������� ����� ����������� ��������� ���� �� 5%, ��������� ���� ���������� ������� �� 134%. ������� ����������� ������ ������ ��������� ���������� ����� ����������� ���������, �� ��� ��, ��� � ��������� ���������, ����� ��������� ����� ���������� �������. ��� �����������, ������� �������, �������� ����������� ����������, ����������� ������������ ��������� � ���������� ������������, � ������� ������� ������ ���������� �����, ��������� � ���������� � ������� ��������.</P>
<P>���������� ���������� ������ (������� ��� �������������� ������, ������� ��������� � �.�.), �������� � ������� ������������ ��� ����������� ��������, ������� � 2006 �. ����� ��� �� 23% � ��������� � ������� 2,3 ��� ������. ��������� �� ������������� ������������� ���������� �������� ������ ���������� ������� ����������� ��������� ����������, ����� ������� ������ ����� 1,6 ��� ����� ��������. ��������� ����� ������� ��� ������ ����� ���������� 650 ���. ������, � ��������� &#8211; �������������� 120 ���. ��� ������������ ���������� �� ���������� ����������� ������������� ������������� ���������� ������ � ��������� �������������� ����� ������������������ ���������� ������������� ������� ���������� � ������ ����� &#8211; �� 50% �� 2006 �.</P>
<P align=right>������� 1</P><B>
<P>���������� ��������� ��������� ��������� ����������</P></B>
<TABLE borderColor=#000000 cellSpacing=1 cellPadding=7 width=638 border=1>
<TBODY>
<TR>
<TD vAlign=top width="40%">&nbsp;</TD>
<TD vAlign=top width="16%"><B><FONT size=3>
<P align=center>2003</B></FONT></P></TD>
<TD vAlign=top width="15%"><B><FONT size=3>
<P align=center>2004</B></FONT></P></TD>
<TD vAlign=top width="15%"><B><FONT size=3>
<P align=center>2005</B></FONT></P></TD>
<TD vAlign=top width="14%"><B><FONT size=3>
<P align=center>2006</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><FONT size=3>
<P>�����</FONT></P></TD>
<TD vAlign=top width="16%"><FONT size=3>
<P align=right>14116</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P align=right>14199</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P align=right>14577</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=right>15135</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><FONT size=3>
<P>�������������� �����</FONT></P></TD>
<TD vAlign=top width="16%"><FONT size=3>
<P align=right>20894</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P align=right>21529</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P align=right>22443</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=right>23456</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><FONT size=3>
<P>��������� �����������</FONT></P></TD>
<TD vAlign=top width="16%"><FONT size=3>
<P align=right>460</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P align=right>4563</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P align=right>4657</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=right>4771</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><FONT size=3>
<P>���� ��������� ����������</FONT></P></TD>
<TD vAlign=top width="16%"><FONT size=3>
<P align=right>358</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P align=right>391</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P align=right>397</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=right>423</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%"><B><FONT size=3>
<P>�����</B></FONT></P></TD>
<TD vAlign=top width="16%"><B><FONT size=3>
<P align=right>35828</B></FONT></P></TD>
<TD vAlign=top width="15%"><B><FONT size=3>
<P align=right>40682</B></FONT></P></TD>
<TD vAlign=top width="15%"><B><FONT size=3>
<P align=right>42074</B></FONT></P></TD>
<TD vAlign=top width="14%"><B><FONT size=3>
<P align=right>43785</B></FONT></P></TD></TR></TBODY></TABLE>
<P align=right>������� 2</P><B>
<P>��������� ���������� ��������� ��������� ����������</P></B>
<TABLE borderColor=#000000 cellSpacing=1 cellPadding=7 width=642 border=1>
<TBODY>
<TR>
<TD vAlign=top width="40%" height=17>
<P></P></TD>
<TD vAlign=top width="16%" height=17><B><FONT size=3>
<P align=center>2003</B></FONT></P></TD>
<TD vAlign=top width="15%" height=17><B><FONT size=3>
<P align=center>2004</B></FONT></P></TD>
<TD vAlign=top width="17%" height=17><B><FONT size=3>
<P align=center>2005</B></FONT></P></TD>
<TD vAlign=top width="12%" height=17><B><FONT size=3>
<P align=center>2006</B></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%" height=17><FONT size=3>
<P>���������� ��������� ����������</FONT></P></TD>
<TD vAlign=top width="16%" height=17><FONT size=3>
<P align=center>344</FONT></P></TD>
<TD vAlign=top width="15%" height=17><FONT size=3>
<P align=center>344</FONT></P></TD>
<TD vAlign=top width="17%" height=17><FONT size=3>
<P align=center>345</FONT></P></TD>
<TD vAlign=top width="12%" height=17><FONT size=3>
<P align=center>350</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%" height=34><FONT size=3>
<P>����������� ��������� �� 10 ���. ������� ������ 16 ���</FONT></P></TD>
<TD vAlign=top width="16%" height=34><FONT size=3>
<P align=center>69,1</FONT></P></TD>
<TD vAlign=top width="15%" height=34><FONT size=3>
<P align=center>68,6</FONT></P></TD>
<TD vAlign=top width="17%" height=34><FONT size=3>
<P align=center>69,3</FONT></P></TD>
<TD vAlign=top width="12%" height=34><FONT size=3>
<P align=center>70,7</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%" height=34><FONT size=3>
<P>����������� ��������� �� ���� ���������</FONT></P></TD>
<TD vAlign=top width="16%" height=34><FONT size=3>
<P align=center>4,7</FONT></P></TD>
<TD vAlign=top width="15%" height=34><FONT size=3>
<P align=center>4,6</FONT></P></TD>
<TD vAlign=top width="17%" height=34><FONT size=3>
<P align=center>4,6</FONT></P></TD>
<TD vAlign=top width="12%" height=34><FONT size=3>
<P align=center>4,6</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%" height=35><FONT size=3>
<P>����������� ��������� �� 10 ���. ������� ������ 16 ���</FONT></P></TD>
<TD vAlign=top width="16%" height=35><FONT size=3>
<P align=center>11,2</FONT></P></TD>
<TD vAlign=top width="15%" height=35><FONT size=3>
<P align=center>11,3</FONT></P></TD>
<TD vAlign=top width="17%" height=35><FONT size=3>
<P align=center>11,5</FONT></P></TD>
<TD vAlign=top width="12%" height=35><FONT size=3>
<P align=center>11,8</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%" height=20><FONT size=3>
<P>���������� ����������</FONT></P></TD>
<TD vAlign=top width="16%" height=20><FONT size=3>
<P align=center>52037</FONT></P></TD>
<TD vAlign=top width="15%" height=20><FONT size=3>
<P align=center>53624</FONT></P></TD>
<TD vAlign=top width="17%" height=20><FONT size=3>
<P align=center>55610</FONT></P></TD>
<TD vAlign=top width="12%" height=20><FONT size=3>
<P align=center>57804</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%" height=34><FONT size=3>
<P>����� ���������� �� 10 ���. ������� ������ 16 ���</FONT></P></TD>
<TD vAlign=top width="16%" height=34><FONT size=3>
<P align=center>14,7</FONT></P></TD>
<TD vAlign=top width="15%" height=34><FONT size=3>
<P align=center>14,9</FONT></P></TD>
<TD vAlign=top width="17%" height=34><FONT size=3>
<P align=center>15,2</FONT></P></TD>
<TD vAlign=top width="12%" height=34><FONT size=3>
<P align=center>15,5</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%" height=19><FONT size=3>
<P>����������� ���������� TPV</FONT></P></TD>
<TD vAlign=top width="16%" height=19><FONT size=3>
<P align=center>985719</FONT></P></TD>
<TD vAlign=top width="15%" height=19><FONT size=3>
<P align=center>1066426</FONT></P></TD>
<TD vAlign=top width="17%" height=19><FONT size=3>
<P align=center>1124107</FONT></P></TD>
<TD vAlign=top width="12%" height=19><FONT size=3>
<P align=center>1312194</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%" height=35><FONT size=3>
<P>����������� ���������� TPV �� 10 ���. ������� ������ 16 ���</FONT></P></TD>
<TD vAlign=top width="16%" height=35><FONT size=3>
<P align=center>278,2</FONT></P></TD>
<TD vAlign=top width="15%" height=35><FONT size=3>
<P align=center>295,9</FONT></P></TD>
<TD vAlign=top width="17%" height=35><FONT size=3>
<P align=center>306,7</FONT></P></TD>
<TD vAlign=top width="12%" height=35><FONT size=3>
<P align=center>352,4</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%" height=19><FONT size=3>
<P>���������� ��������� ���� (���.)</FONT></P></TD>
<TD vAlign=top width="16%" height=19><FONT size=3>
<P align=center>71114</FONT></P></TD>
<TD vAlign=top width="15%" height=19><FONT size=3>
<P align=center>76402</FONT></P></TD>
<TD vAlign=top width="17%" height=19><FONT size=3>
<P align=center>82564</FONT></P></TD>
<TD vAlign=top width="12%" height=19><FONT size=3>
<P align=center>90090</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="40%" height=34><FONT size=3>
<P>���������� ��������� � ��������� ���� �� ������ ������ ������ 16 ���</FONT></P></TD>
<TD vAlign=top width="16%" height=34><FONT size=3>
<P align=center>2,0</FONT></P></TD>
<TD vAlign=top width="15%" height=34><FONT size=3>
<P align=center>2,1</FONT></P></TD>
<TD vAlign=top width="17%" height=34><FONT size=3>
<P align=center>2,3</FONT></P></TD>
<TD vAlign=top width="12%" height=34><FONT size=3>
<P align=center>2,4</FONT></P></TD></TR></TBODY></TABLE>
<P align=right>���������</P><B>
<P align=center>��������� ��������� ���������� ���� (���������� ��������� ��������� ���������� �� 1000 �������)</P></B>
<P></P><B>
<P align=center></P>
<P align=center>����������:</P></B><FONT face="Times New Roman">
<P align=justify>1. Memoria de la supervisi&#243;n bancaria en Espa&#241;a, 2006. / Banco de Espa&#241;a. &#8211; Mode o</FONT>f access: http://www.bde.es/informes/be/supervi/2006/ms2006.pdf .</P><FONT face="Times New Roman">
<P>2. Guti&#233;rrez de Rozas Ruiz L. La posici&#243;n relativa de la banca Espa&#241;ola en el contexto Europeo. / Direcci&#243;n General de Regulaci&#243;n del Banco de Espa&#241;a. </FONT>&#8211; Mode of access: http://www.bde.es/informes/be/estfin/numero10/estfin0410.pdf</P><FONT face="Times New Roman">
<P>3. Las cajas de ahorros espa&#241;olas: expansi&#243;n, resultados y contribuci&#243;n social en 2006 // Newsletter sobre cajas de ahorros. &#8211; Madrid, 2007. &#8211; &#8470;</FONT> 36. &#8211; Mode of access: http://www.funcas.es/indicadores/Analisis_Financiero.asp</P>
<P>.</P>


</body>
</html>

