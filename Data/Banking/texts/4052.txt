<!--
ID:38078071
TITLE:��������� � ���������� ��������� �����������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:14
ISSUE_DATE:2007-07-31
RECORD_DATE:2007-08-29


-->
<html>
<head>
<title>
38078071-��������� � ���������� ��������� �����������
</title>
</head>
<body >


<P align=justify>���������� ����������� (�����������) ������������ ������, �������� �������� �� ���������� ���������� ��������, ����������, ��� � ������������ � ��������� ������������ ����� �������� ���������� ��� ��������� ��������������� (���������) ����������� ����� � ��������� ���������������� (����������) �������� ���������� ��������� ��������� �����������:</P>
<TABLE cellSpacing=2 cellPadding=7 width=610 border=1>
<TBODY>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>&#8470;</P>
<P align=center>�/�</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>������������</P>
<P align=center>���������</P>
<P align=center>�����������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>��������-������� �����</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P align=center>����</P>
<P align=center>������<BR>��������</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P align=center>�����, �������, �.�.�. ����������� ������������ (�����������).</P>
<P align=center>���� ������������ ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>����</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>2456</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P>11.09.1998</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>367025, ���������� ��������, ���������, �������� ������ ���������, 39</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>���-����</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>3076</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P>20.04.2006</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>109052, ������, �/� 48;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>60 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>���������-����</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>603</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P>10.05.2007</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>109052, ������, �/� 48;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>60 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>&#8470;</P>
<P align=center>�/�</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P align=center>������������</P>
<P align=center>���������</P>
<P align=center>�����������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>��������-������� �����</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P align=center>����</P>
<P align=center>������<BR>��������</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P align=center>�����, �������, �.�.�. ����������� ������������ (�����������).</P>
<P align=center>���� ������������ ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT size=3>
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="25%"><FONT size=3>
<P>�����������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=3>
<P align=center>3210</FONT></P></TD>
<TD vAlign=top width="15%"><FONT size=3>
<P>25.04.2007</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P>109240, ������, ������� ��������� �����, 4;</P>
<P>���������� ���������� �� ������:</P>
<P>109052, ������, �/� 48;</P>
<P>���. 8-800-200-08-05;</P>
<P>��������� �� ����������� �������;</P>
<P>60 ����</FONT></P></TD></TR></TBODY></TABLE><I>
<P align=right></P>
<P align=right>"������� ����� ������", 2007, &#8470; 38, 39</P></I>


</body>
</html>

