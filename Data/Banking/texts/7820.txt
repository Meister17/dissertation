<!--
ID:37789495
TITLE:C���� �����������
SOURCE_ID:10908
SOURCE_NAME:������������ ���������� ������
AUTHOR:
NUMBER:6
ISSUE_DATE:2007-06-30
RECORD_DATE:2007-06-15


-->
<html>
<head>
<title>
37789495-C���� �����������
</title>
</head>
<body >


<DIV class=article>
<DIV class=header>
<H2><FONT size=3>������ ��� ���� ������������ � ����� ���������� ������</FONT></H2></DIV>
<DIV class=body>
<P align=justify>� ������ ������ �������������� ������� �� ���������� ������������ ������������ �������� ��� ���������� �� II ������������� ����������� &#171;�����, ���� � ��������������� �������� � ������&#187;. ��� ��������� ������� �������� ���������, ������, �������������� ������� �������, � ����� ������������ ����������� � �������.</P>
<P align=justify>� ������ ������ �������������� ������� �� ���������� ������������ ������������ �������� ��� ���������� �� II ������������� ����������� &#171;�����, ���� � ��������������� �������� � ������&#187;. ��� ��������� ������� �������� ���������, ������, �������������� ������� �������, � ����� ������������ ����������� � �������.</P>
<P align=justify>������� ���������� ����������� ������ �������� ������������� � ���������� �������, � ����� ��������� �������� ����������. �� ���� �������� ��������� ������ &#171;���������-������������� ������: ��������� � ���������� ���������� ��������&#187;. ������������� �������� ������� �������� ������ � �����, ��������� �������������� ���������, �������� ���������������, �����������, ������ ���������������������� �� ������ ���� ������ ���������, ������������ ������������� ������ ���������� ������� ������ ������ �����. �� ��� ������, ������ ��������� ��� ������� ������� � ����������� � ���������� �����.</P>
<P align=justify>�������� ������� ����� �������� ��������, � �������� ����� ������� ��������. ������������� ���������� ��������������� - �� ���� ����, �� � �� ���������� �������������������. �����, ����� ������� ���������� �� ��������� ����������� � ���� ����������� �������� ��������, �������� ��������� �����������.</P>
<P align=justify>������ � ���������� �������� ���������� ��������, ��� �������, ��� ������� ������� �� ��� �������� �����������. ������ ��� ������������ ������ ���������� ������������, � ����������� ������� ��������� ��� ��� �������.</P>
<P align=justify>���������� ���������� ���������� - �������� ����������. ������������, ������� ��������� ������� ������ ������� � ���� �����, �������� ������������ ���������� �����. �� ���� ������� ��������� ��������, ������� ������������� �� &#171;�������&#187;, ������� � 2004 ���� ���������� ������ �� �������� ���������������� ������������� ������� ���������� ���������� GRI. � �������� ���� �������� ����������� ����� �� ����� ������ ����������� GRI, � �������� PricewaterhouseCoopers ������� ����� � ����������� ��� ������������ ���������.</P>
<P align=justify>� ������� ��������� ��������� ������ ��������� ������ ����� ������� �� &#171;�������&#187;. �������������� �������� ���������� ���� ������� ��������� � &#171;���������� �����&#187; - �������� ����������, ��������������� �� ���������� �������� ������� ���������� �������� ��� �������������, ��� � ������������ ������. �� ��� ������, ���� ���������� ������, ���������� �� ������� �����, ��������� ���������� ���������� ���������� ����� ��� ������ �������� ������������ �������. ����� ��� ����������� � �������������.</P>
<P align=justify>���������� ������������ ���������������� ����� ���������� �� �����������. ��� ������� ����������� ����������� �������� ��� &#171;�������&amp;�������&#187; ����� ���������, ��� ����������������� ��������� ���� �������� � ������ ������������ � ������������� �����������, �������������� ����������� �������. ��� ������� ��������������� �������� ������� �� ������� � ������� ����� ������, ������� �������, ��� ���������� ������� ������ ��������������� ����� � ������������� �������������� ����������������� ��������.</P></DIV></DIV>


</body>
</html>

