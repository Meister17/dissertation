<!--
ID:36820180
TITLE:����� ����������� ������� � I ��������� 2006 ���� 
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:
NUMBER:9
ISSUE_DATE:2006-09-30
RECORD_DATE:2006-10-18


-->
<html>
<head>
<title>
36820180-����� ����������� ������� � I ��������� 2006 ���� 
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgcolor1>
<TD></TD></TR>
<TR class=bgwhite>
<TD>
<P align=justify><EM>�� ��������� �� 1.07.2006 � ���������� ������� ����������� 623 ��������� �����������, � ��� ����� 609 � ������ � 14 � ���������� ������� (�� 1.01.2006 &#8212; 646 ��������� �����������: 631 � ������ � 15 � ���������� �������). </EM>
<P class=Main align=justify>� ������ ���� ���������� ������ ��������� ����������� ����������� ������� ������� �� 18,4%, ��� �� 1 037,6 ����. ������, � ��������� �� 1.07.2006 &#8212; 6 672,6 ����. ������ <SUP>1</SUP>. �� ���� �������� ����� ������� ��� � ����� ��������� ���� ��������� ���� ������� ������� � �������� ��������� �����������: � 66% �� 66,9% � � 64,9 % �� 66,2% ��������������. ��������������� ��� ���� ������� � ������ ������ ������������ � ������ ���������� ���������&shy;��� ����������� � ����&shy;�������, �������� � ������ ������ � ���� ��&shy;�������� ����� &#8212; � ������� � ����������� ������� ��������� ����������� ��� � ������� ���������� ��� &#8212; � ���������� ����������� �������. 
<P class=Main align=justify><STRONG>� ��������� ��������</STRONG> �� 1.07.2006 ������� ���� ������������ � ��������� ����������� ���������� ����������� ���������� ������� ��� �������������� �������� ��������� ���� �������� �� ��������� ������. 
<P class=Main align=justify><STRONG>����������� �������</STRONG> ������ � �������� ������� ����������� �� 9,3% � ��������� 938,5 ����. ������. 
<P class=Main align=justify><STRONG>����� ���������� ���������</STRONG> ������������ ������ (� ������ ������� ���) �� ��������� �� 1.07.2006 ������ 189,9 ����. ������. ��� ���� ���������� ��������� ��������� ���� �������� 105,5 ����. ������. 
<P class=Main align=justify><STRONG>�������� ������� � �����</STRONG> ������ ������� �� 11,2% � ��������� �� 1.07.2006 748,7 ����. ������. 
<P class=Main align=justify>������� �� ��������� ������ ����������� �� 10,7%, ��� �� 23,5 ����. ������ � 
<P class=Main align=justify>��������� 244,1 ����. ������. ���� ��������� ���������� ���������� ��������� ���� �������� &#8212; �� ��������� ������ �� ������. 
<P class=Main align=justify>����� �������� ������������ ��������� ����������� ����������� �����&shy;�� �� ��������� ����������� �� 20,5% � �������� 5 489,9 ����. ������ (82,3% ���������� ��������). �������� ������� �� ���� ������������ ������� ���������� �������� ������� �� ��������� � ������� ������ ���������� ������������ ����������� � �����������, � ����� ���������� ���, � ������� ���� &#8212; ��������� ����������� ���-������������, ��������� ���������� ������� ��������� �� � ������� ����&shy;���� ��������������, � ����� ��������� ���������� ���. 
<P class=Main align=justify><STRONG>������� ������� �� ��������� � ������� ������ ���������� ������������ ����������� � ����������� </STRONG>��������� ��������� ���������� ���������� ��������� ����. ����� �� ����� ����������� ��������� ����� ���������� �����&shy;���(24,1% ������ 18,4%). ��� ��������� ����������� ��� � ������, ��� � � ����������� ������. �� ��� ���� �������� ������������ ����� �������� ���������� � ����������� ������, � ������� � ���������� ��������� &#8212; � ������. � ����� ������� �� ��������� � ������� ������ �������� ����������� �� 23,9%, �� 1 393,3 ����. ������. �� ���������� � ���������� �������� ���&shy;����� 20,9% (�� 1.01.2006 &#8212; 20%). 
<P class=Main align=justify><STRONG>����� ���������� ���������� ����</STRONG> ����� ����������� ��������� ����� ���������� ���������� �������� (24,2% ������ 18,4%). �������� ������ ��������� ��� ������ � ������������� ����������� ���: ������� �� �� ���������� ������ ������� �� 35,1% &#8212; �� 1 165,2 ����. ������. ������ ���������� ��� ����������� �� 12,6% � �� 1.07.2006 ��������� 904,8 ����. ������. �� ���� �������������� ��������� ���������� ����� ������������ ������ �����&shy;����� ���� ��������� ���� �������� ������� ��������� � ����� ������ ������� (� 59,8% �� 66,5%). 
<P class=Main align=justify>� �������� �������� ������ ���������� ����������� ���������� ����� ��� � ���������� �������� ������ ����������� ������� �� ��������� �� 1.07.2006 ����������� �� ���������� &#8212; 16,6%. ������ � ��� ������� ������������� �� ���������� ������������� �������� (� ������ �������� ����� ������) � ����� ������� �� 19% � ��������� 1 108,9 ����. ������ &#8212; ������� ������� � ���������� ���������� ������������ ������� �� ������-������������. �� ���� � ����� ����� ������������ ��� (� ��������� ����� ������) ������� �� 73,9% (�� 1.01.2006 &#8212; 70,8%). 
<P class=Main align=justify><STRONG>������� ������� �� ������ ������ (����)</STRONG> ��������� �� 10,5% � �� 1.07.2006 ��������� 87,1 ����. ������. ������� ������� &#8212; �� ���� ���������� �������� ������� �� ��������� ���������� ������. 
<P class=Main align=justify>��������, ���������������� ������� ���� �<STRONG>������ ������ �����,</STRONG> ����������� �� ������������� ������ �� 8,3% �� 600,4 ����. ������. ������ � ��� �� ���� � ���������� �������� ��������� � 9,8% �� 9%. ������������ ������� �� ����������� �������� ������ ���� ����� ���������� �������� (�� 7,3%, �� 478,1 ����. ������) �� ���� ������������� �������� ������ ���������� ���������� ������������ (�� 36,3%, �� 27,4 ����. ������) � ���������� � ���������� ��������� ���������� ���������� ���������. 
<P class=Main align=justify><STRONG>� ����� �������� �������� </STRONG>������������ ������ �������� ��� ��������� ����������� ����������� ������� � I ��������� ��������: ������������ ���������� ������������ ����������� � �����������, �������� � ������ ������, � ����� ������������ ���������� ��� � ���������� �������� ������� �� ����� ���. ����� ����, ����������� ������������ ���������� �������� ������� �� ���������� ������ � ����� ������. ��� ���� ������� �� ����������������� ������ � ����� ������, � ����� ����� �������� ������� � ������������ ����������&shy;�������� ��������. 
<P class=Main align=justify><STRONG>����� �������� �������� ���������</STRONG> �� ��������� ��������� 37,5%, � ���������� ������� ������������� �� ������, ��������������� ���������� �����, �� 1.07.2006 �������� 643,3 ����. ������. Ÿ ���� � ���������� ������� ������� �� 9,6% �� ��������� � 8,3% �� 1.01.2006. ������� ��������� ���������� ��������������� � ������, �� ���� � ����� ������������� ���������� ��� ���������� � 74,8 �� 75,6%. 
<P class=Main align=justify>�� ��������� ��������� ��������� ��������� �������� ���������� �������� ������: ���� ������������ ������������� � ����� ������ ������� ������������� ���������� � 1,25% �� 1,53%. 
<P class=Main align=justify><STRONG>����� �������� ������ ������������ ����������� ���</STRONG> ��������� ��������� ���� ������ ���������� ������� (16,4% ������ 18,4%). �������� ��������, ������������ ���� �� ������ ������, ���� ���������� ������������� ������������ ����������� � �����������, ����������� � ����������������� ������������� &#8212; �� 16,6%, ��� �� 345,5 ����. ������. � ����� �������� ������������� �� ��������, ��������������� ������������ ��������� ������� ���������, ����������� �� 15,4% � ��������� 2 550,7 ����. ������ (38,2% ���������� �������). 
<P class=Main align=justify>������� ������������� �� �������� ���������� ������������� ������� <STRONG>������������� �������� </STRONG>����������� �� ��������� �� 28,3% (��� �� 148,6 ����. ������) � �� 1.07.2006 ��������� 673,1 ����. ������. ���� � �������� ��� ���������� ����������� ������������� ��������, ����������� � ������-������������, � �� ����� ��� ����� �������� ������� ���������� � ��������� ���������� ������ ���� ����� ����������. 
<P class=Main align=justify><STRONG>������� ������� ��������� �����������, ����������� �� ��������� � ������ ������ (����� ������),</STRONG> ���������� � ������������� ������� �� 4,4%. ��� ���� ���������� ���������� �������� ������� �� ������ � ������-������������ (�� 11,1%) � ������������ &#8212; �������� ������������ ���������� �� ���������� ������-������������ (�� 13,6%). 
<P class=Main align=justify>����� ������� �������� � ������� ������ ����������� ������� ������� �� 12,6% (�� 189,8 ����. ������). ������������ ������� �� ��� ��������� ������� ���������� �������� ������. 
<P class=Main align=justify><STRONG>�� ����� ������ ����� </STRONG>���������� ������������ ���������� �������� (�� 26,1%, ��� �� 191 ����. ������), ������� �������, � �������� ������������� (�� 26,6%, ��� �� 141,4 ����. ������) ��� ����� ��������� ������ �������� �������� � �����. ����� �� �������� ����� � ���������� ���� �������������� ��������� �������� � �������� ������������� ������������, � �������� �������������, ������������� �� �������� � �������� �� �������� �������, � ����� � �������� ������������� �����������. � ����� ����� �������� ������ ����������� ������� � ������ ������ �� ��������� �� 1.07.2006 ��������� 921,9 ����. ������, ��� 13,8% ���������� ������� &#8212; �� ��������� � 13% �� 1.01.2006. 
<P class=Main align=justify><STRONG>�������� ��������� ������</STRONG> ��������� ����������� ����������� ������� (��������� ������� �������� ������� �� ����������������� � ���������� ������ � ����� ������, �������� �������� ������� � ������������) � ������������� ������� ������� &#8212; �� 1,5%, �� 496,7 ����. ������. ������ � ��� �� ���� � ���������� ������� ������ ��������� � 8,7% �� 1.01.2006 �� 7,4% �� 1.07.2006. 
<DIV align=center>
<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR>
<TD class=bgcolor1>
<P class=Rubrika><STRONG>��������� �������� ������� ��������� ����������� ����������� �������, ���. ������</STRONG></P></TD></TR>
<TR>
<TD class=bgwhite>
<TABLE cellSpacing=0 cellPadding=0 width="100%" align=center border=1>
<TBODY>
<TR class=bodycolor>
<TD width="39%">
<P align=center><STRONG>������</STRONG></P></TD>
<TD width="10%">
<P align=center><STRONG>01.01.2006</STRONG></P></TD>
<TD width="10%">
<P align=center><STRONG>01.07.2006</STRONG></P></TD>
<TD width="9%">
<P align=center><STRONG>��. ��� 01.01.06</STRONG></P></TD>
<TD width="9%">
<P align=center><STRONG>��. ��� 01.07.06</STRONG></P></TD>
<TD width="23%">
<P align=center><STRONG>�������� �� I ��������� 2006 ����</STRONG></P></TD></TR>
<TR>
<TD>
<P>������ �������-�����</P></TD>
<TD align=right>
<P>5 635 012,7</P></TD>
<TD align=right>
<P>6 672 568,7</P></TD>
<TD align=right>
<P>100,0</P></TD>
<TD align=right>
<P>100,0</P></TD>
<TD align=right>
<P>118,4</P></TD></TR>
<TR>
<TD>
<P><STRONG>����������� ��������</STRONG></P></TD>
<TD align=right>
<P><STRONG>858 736,6</STRONG></P></TD>
<TD align=right>
<P><STRONG>938 553,8</STRONG></P></TD>
<TD align=right>
<P><STRONG>15,2</STRONG></P></TD>
<TD align=right>
<P><STRONG>14,1</STRONG></P></TD>
<TD align=right>
<P><STRONG>109,3</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�. �������� ������� � ����� ������</P></TD>
<TD align=right>
<P>673 211,5</P></TD>
<TD align=right>
<P>748 693,8</P></TD>
<TD align=right>
<P>11,9</P></TD>
<TD align=right>
<P>11,2</P></TD>
<TD align=right>
<P>111,2</P></TD></TR>
<TR>
<TD>
<P>���������� ��������� ������������ (� ������ ������� ���)</P></TD>
<TD align=right>
<P>185 525,1</P></TD>
<TD align=right>
<P>189 860,0</P></TD>
<TD align=right>
<P>3,3</P></TD>
<TD align=right>
<P>2,8</P></TD>
<TD align=right>
<P>102,3</P></TD></TR>
<TR>
<TD>
<P><STRONG>������� �� ��������� ������</STRONG></P></TD>
<TD align=right>
<P><STRONG>220 554,0</STRONG></P></TD>
<TD align=right>
<P><STRONG>244 060,1</STRONG></P></TD>
<TD align=right>
<P><STRONG>3,9</STRONG></P></TD>
<TD align=right>
<P><STRONG>3,7</STRONG></P></TD>
<TD align=right>
<P><STRONG>110,7</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�. ������� �� ��������� ������ �� ������</P></TD>
<TD align=right>
<P>193 072,3</P></TD>
<TD align=right>
<P>217 095,3</P></TD>
<TD align=right>
<P>3,4</P></TD>
<TD align=right>
<P>3,3</P></TD>
<TD align=right>
<P>112,4</P></TD></TR>
<TR>
<TD>
<P>������� �� ��������� ������ �� ������ �������</P></TD>
<TD align=right>
<P>6 296,2</P></TD>
<TD align=right>
<P>3 760,2</P></TD>
<TD align=right>
<P>0,1</P></TD>
<TD align=right>
<P>0,1</P></TD>
<TD align=right>
<P>59,7</P></TD></TR>
<TR>
<TD>
<P>������ �������</P></TD>
<TD align=right>
<P>21 175,5</P></TD>
<TD align=right>
<P>23 204,6</P></TD>
<TD align=right>
<P>0,4</P></TD>
<TD align=right>
<P>0,3</P></TD>
<TD align=right>
<P>109,6</P></TD></TR>
<TR>
<TD>
<P><STRONG>�������������:</STRONG></P></TD>
<TD align=right>
<P><STRONG>4 555 732,1</STRONG></P></TD>
<TD align=right>
<P><STRONG>5 489 954,8</STRONG></P></TD>
<TD align=right>
<P><STRONG>80,8</STRONG></P></TD>
<TD align=right>
<P><STRONG>82,3</STRONG></P></TD>
<TD align=right>
<P><STRONG>120,5</STRONG></P></TD></TR>
<TR>
<TD>
<P>�������� �������� �� ������</P></TD>
<TD align=right>
<P>1 124 599,4</P></TD>
<TD align=right>
<P>1 393 312,1</P></TD>
<TD align=right>
<P>20,0</P></TD>
<TD align=right>
<P>20,9</P></TD>
<TD align=right>
<P>123,9</P></TD></TR>
<TR>
<TD>
<P>� �.�. �������� ������������ �����������-���������� �� ��������� � ������� ������</P></TD>
<TD align=right>
<P>848 454,2</P></TD>
<TD align=right>
<P>1 052 905,4</P></TD>
<TD align=right>
<P>15,1</P></TD>
<TD align=right>
<P>15,8</P></TD>
<TD align=right>
<P>124,1</P></TD></TR>
<TR>
<TD>
<P>�������� ������ �������� �� ������</P></TD>
<TD align=right>
<P>214 755,4</P></TD>
<TD align=right>
<P>247 220,3</P></TD>
<TD align=right>
<P>3,8</P></TD>
<TD align=right>
<P>3,7</P></TD>
<TD align=right>
<P>115,1</P></TD></TR>
<TR>
<TD>
<P>�������� (����� ������)</P></TD>
<TD align=right>
<P>1 666 272,0</P></TD>
<TD align=right>
<P>2 070 083,6</P></TD>
<TD align=right>
<P>29,6</P></TD>
<TD align=right>
<P>31,0</P></TD>
<TD align=right>
<P>124,2</P></TD></TR>
<TR>
<TD>
<P>� �.�. ������ ���������� ���</P></TD>
<TD align=right>
<P>803 511,9</P></TD>
<TD align=right>
<P>904 849,9</P></TD>
<TD align=right>
<P>14,3</P></TD>
<TD align=right>
<P>13,6</P></TD>
<TD align=right>
<P>112,6</P></TD></TR>
<TR>
<TD>
<P>�������� ����������� ���</P></TD>
<TD align=right>
<P>862 760,0</P></TD>
<TD align=right>
<P>1 165 233,7</P></TD>
<TD align=right>
<P>15,3</P></TD>
<TD align=right>
<P>17,5</P></TD>
<TD align=right>
<P>135,1</P></TD></TR>
<TR>
<TD>
<P>� �.�. �������� ������������ �����������-����������</P></TD>
<TD align=right>
<P>405 119,5</P></TD>
<TD align=right>
<P>481 908,1</P></TD>
<TD align=right>
<P>7,2</P></TD>
<TD align=right>
<P>7,2</P></TD>
<TD align=right>
<P>119,0</P></TD></TR>
<TR>
<TD>
<P>�������� ������ ��������</P></TD>
<TD align=right>
<P>457 640,5</P></TD>
<TD align=right>
<P>683 325,6</P></TD>
<TD align=right>
<P>8,1</P></TD>
<TD align=right>
<P>10,2</P></TD>
<TD align=right>
<P>149,3</P></TD></TR>
<TR>
<TD>
<P>����� ������</P></TD>
<TD align=right>
<P>97 222,0</P></TD>
<TD align=right>
<P>87 059,6</P></TD>
<TD align=right>
<P>1,7</P></TD>
<TD align=right>
<P>1,3</P></TD>
<TD align=right>
<P>89,5</P></TD></TR>
<TR>
<TD>
<P>� �. �. �������� ���������� ������</P></TD>
<TD align=right>
<P>48 932,3</P></TD>
<TD align=right>
<P>45 476,4</P></TD>
<TD align=right>
<P>0,9</P></TD>
<TD align=right>
<P>0,7</P></TD>
<TD align=right>
<P>92,9</P></TD></TR>
<TR>
<TD>
<P>�������� ������-������������</P></TD>
<TD align=right>
<P>13 529,3</P></TD>
<TD align=right>
<P>12 806,0</P></TD>
<TD align=right>
<P>0,2</P></TD>
<TD align=right>
<P>0,2</P></TD>
<TD align=right>
<P>94,7</P></TD></TR>
<TR>
<TD>
<P>��� ����������, ������� ������������ �������������, ��� ������������ %% (� ������ �������� ����� ������)</P></TD>
<TD align=right>
<P>931 932,8</P></TD>
<TD align=right>
<P>1 108 917,9</P></TD>
<TD align=right>
<P>16,5</P></TD>
<TD align=right>
<P>16,6</P></TD>
<TD align=right>
<P>119,0</P></TD></TR>
<TR>
<TD>
<P>� �.�. ��� ������-������������</P></TD>
<TD align=right>
<P>660 164,8</P></TD>
<TD align=right>
<P>819 848,0</P></TD>
<TD align=right>
<P>11,7</P></TD>
<TD align=right>
<P>12,3</P></TD>
<TD align=right>
<P>124,2</P></TD></TR>
<TR>
<TD>
<P>������ ������ ����������</P></TD>
<TD align=right>
<P>554 305,3</P></TD>
<TD align=right>
<P>600 374,2</P></TD>
<TD align=right>
<P>9,8</P></TD>
<TD align=right>
<P>9,0</P></TD>
<TD align=right>
<P>108,3</P></TD></TR>
<TR>
<TD>
<P>������ �������</P></TD
>
<TD align=right>
<P>181 400,6</P></TD>
<TD align=right>
<P>230 207,3</P></TD>
<TD align=right>
<P>3,2</P></TD>
<TD align=right>
<P>3,5</P></TD>
<TD align=right>
<P>126,9</P></TD></TR>
<TR class=bodycolor>
<TD>
<P align=center><STRONG>�����</STRONG></P></TD>
<TD align=right>
<P></P></TD>
<TD align=right>
<P></P></TD>
<TD align=right>
<P></P></TD>
<TD align=right>
<P></P></TD>
<TD align=right>
<P></P></TD></TR>
<TR>
<TD>
<P>������ �������-�����</P></TD>
<TD align=right>
<P>5 635 012,7</P></TD>
<TD align=right>
<P>6 672 568,7</P></TD>
<TD align=right>
<P>100,0</P></TD>
<TD align=right>
<P>100,0</P></TD>
<TD align=right>
<P>118,4</P></TD></TR>
<TR>
<TD>
<P><STRONG>������� � ������������ � ��� ������������� (� ������ ��������) ����� ��� ������������ ���������</STRONG></P></TD>
<TD align=right>
<P><STRONG>3 719 961,4</STRONG></P></TD>
<TD align=right>
<P><STRONG>4 483 684,5</STRONG></P></TD>
<TD align=right>
<P><STRONG>66,0</STRONG></P></TD>
<TD align=right>
<P><STRONG>67,2</STRONG></P></TD>
<TD align=right>
<P><STRONG>120,5</STRONG></P></TD></TR>
<TR>
<TD>
<P>����� �������� (����� ������)</P></TD>
<TD align=right>
<P>3 026 438,4</P></TD>
<TD align=right>
<P>3 620 480,0</P></TD>
<TD align=right>
<P>53,7</P></TD>
<TD align=right>
<P>54,3</P></TD>
<TD align=right>
<P>119,6</P></TD></TR>
<TR>
<TD>
<P>� �.�. ����� ���������� �����</P></TD>
<TD align=right>
<P>467 855,8</P></TD>
<TD align=right>
<P>643 323,4</P></TD>
<TD align=right>
<P>8,3</P></TD>
<TD align=right>
<P>9,6</P></TD>
<TD align=right>
<P>137,5</P></TD></TR>
<TR>
<TD>
<P>����� ����������� �����</P></TD>
<TD align=right>
<P>2 558 582,6</P></TD>
<TD align=right>
<P>2 977 156,6</P></TD>
<TD align=right>
<P>45,4</P></TD>
<TD align=right>
<P>44,6</P></TD>
<TD align=right>
<P>116,4</P></TD></TR>
<TR>
<TD>
<P>� �.�. ����� ������������ ������������-����������</P></TD>
<TD align=right>
<P>2 211 094,7</P></TD>
<TD align=right>
<P>2 550 667,3</P></TD>
<TD align=right>
<P>39,2</P></TD>
<TD align=right>
<P>38,2</P></TD>
<TD align=right>
<P>115,4</P></TD></TR>
<TR>
<TD>
<P>����� ������ ��������</P></TD>
<TD align=right>
<P>347 487,8</P></TD>
<TD align=right>
<P>426 489,3</P></TD>
<TD align=right>
<P>6,2</P></TD>
<TD align=right>
<P>6,4</P></TD>
<TD align=right>
<P>122,7</P></TD></TR>
<TR>
<TD>
<P>������������ ������������� �� ������ ��������</P></TD>
<TD align=right>
<P>45 674,5</P></TD>
<TD align=right>
<P>63 172,1</P></TD>
<TD align=right>
<P>0,8</P></TD>
<TD align=right>
<P>0,9</P></TD>
<TD align=right>
<P>138,3</P></TD></TR>
<TR>
<TD>
<P>��� ��������</P></TD>
<TD align=right>
<P>524 565,5</P></TD>
<TD align=right>
<P>673 127,5</P></TD>
<TD align=right>
<P>9,3</P></TD>
<TD align=right>
<P>10,1</P></TD>
<TD align=right>
<P>128,3</P></TD></TR>
<TR>
<TD>
<P>� �.�. ��� ������-������������</P></TD>
<TD align=right>
<P>283 718,2</P></TD>
<TD align=right>
<P>409 363,7</P></TD>
<TD align=right>
<P>5,0</P></TD>
<TD align=right>
<P>6,1</P></TD>
<TD align=right>
<P>144,3</P></TD></TR>
<TR>
<TD>
<P>������������ ������������� �� �������� ���</P></TD>
<TD align=right>
<P>43,4</P></TD>
<TD align=right>
<P>79,5</P></TD>
<TD align=right>
<P>0,0</P></TD>
<TD align=right>
<P>0,0</P></TD>
<TD align=right>
<P>183,1</P></TD></TR>
<TR>
<TD>
<P>������� �������</P></TD>
<TD align=right>
<P>168 623,3</P></TD>
<TD align=right>
<P>189 790,1</P></TD>
<TD align=right>
<P>3,0</P></TD>
<TD align=right>
<P>2,8</P></TD>
<TD align=right>
<P>112,6</P></TD></TR>
<TR>
<TD>
<P>� �.�. ������������</P></TD>
<TD align=right>
<P>834,0</P></TD>
<TD align=right>
<P>5 307,8</P></TD>
<TD align=right>
<P>0,0</P></TD>
<TD align=right>
<P>0,1</P></TD>
<TD align=right>
<P>636,4</P></TD></TR>
<TR>
<TD>
<P>����������� ������������� �����</P></TD>
<TD align=right>
<P>46 552,0</P></TD>
<TD align=right>
<P>68 559,3</P></TD>
<TD align=right>
<P>0,8</P></TD>
<TD align=right>
<P>1,0</P></TD>
<TD align=right>
<P>147,3</P></TD></TR>
<TR>
<TD>
<P>������ � ������ ������������ ������ (������)</P></TD>
<TD align=right>
<P>334,2</P></TD>
<TD align=right>
<P>286,9</P></TD>
<TD align=right>
<P>0,0</P></TD>
<TD align=right>
<P>0,0</P></TD>
<TD align=right>
<P>85,8</P></TD></TR>
<TR>
<TD>
<P><STRONG>������������ ��������</STRONG></P></TD>
<TD align=right>
<P><STRONG>530,1</STRONG></P></TD>
<TD align=right>
<P><STRONG>581,3</STRONG></P></TD>
<TD align=right>
<P><STRONG>0,0</STRONG></P></TD>
<TD align=right>
<P><STRONG>0,0</STRONG></P></TD>
<TD align=right>
<P><STRONG>109,7</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>�������� � ������ ������</STRONG></P></TD>
<TD align=right>
<P><STRONG>730 820,6</STRONG></P></TD>
<TD align=right>
<P><STRONG>921 864,9</STRONG></P></TD>
<TD align=right>
<P><STRONG>13,0</STRONG></P></TD>
<TD align=right>
<P><STRONG>13,8</STRONG></P></TD>
<TD align=right>
<P><STRONG>126,1</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�. � �������� ������������� ��</P></TD>
<TD align=right>
<P>154 868,7</P></TD>
<TD align=right>
<P>174 258,5</P></TD>
<TD align=right>
<P>2,7</P></TD>
<TD align=right>
<P>2,6</P></TD>
<TD align=right>
<P>112,5</P></TD></TR>
<TR>
<TD>
<P>� �����</P></TD>
<TD align=right>
<P>198 691,8</P></TD>
<TD align=right>
<P>248 285,3</P></TD>
<TD align=right>
<P>3,5</P></TD>
<TD align=right>
<P>3,7</P></TD>
<TD align=right>
<P>125,0</P></TD></TR>
<TR>
<TD>
<P><STRONG>����������� �������� �� ������� ��������� ����</STRONG></P></TD>
<TD align=right>
<P><STRONG>35 111,4</STRONG></P></TD>
<TD align=right>
<P><STRONG>31 371,4</STRONG></P></TD>
<TD align=right>
<P><STRONG>0,6</STRONG></P></TD>
<TD align=right>
<P><STRONG>0,5</STRONG></P></TD>
<TD align=right>
<P><STRONG>89,3</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>���</STRONG></P></TD>
<TD align=right>
<P><STRONG>80 563,9</STRONG></P></TD>
<TD align=right>
<P><STRONG>95 649,4</STRONG></P></TD>
<TD align=right>
<P><STRONG>1,4</STRONG></P></TD>
<TD align=right>
<P><STRONG>1,4</STRONG></P></TD>
<TD align=right>
<P><STRONG>118,7</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>��������, �������� �������� � ����������� �������</STRONG></P></TD>
<TD align=right>
<P><STRONG>720 816,3</STRONG></P></TD>
<TD align=right>
<P><STRONG>726 117,1</STRONG></P></TD>
<TD align=right>
<P><STRONG>12,8</STRONG></P></TD>
<TD align=right>
<P><STRONG>10,9</STRONG></P></TD>
<TD align=right>
<P><STRONG>100,7</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�. �������� � ������</P></TD>
<TD align=right>
<P>205 462,0</P></TD>
<TD align=right>
<P>214 441,1</P></TD>
<TD align=right>
<P>3,6</P></TD>
<TD align=right>
<P>3,2</P></TD>
<TD align=right>
<P>104,4</P></TD></TR>
<TR>
<TD>
<P>� �.�. �������� � ������-������������</P></TD>
<TD align=right>
<P>149 537,0</P></TD>
<TD align=right>
<P>166 124,9</P></TD>
<TD align=right>
<P>2,7</P></TD>
<TD align=right>
<P>2,5</P></TD>
<TD align=right>
<P>111,1</P></TD></TR>
<TR>
<TD>
<P><STRONG>������� � ��������� ��������� �������</STRONG></P></TD>
<TD align=right>
<P><STRONG>71 454,7</STRONG></P></TD>
<TD align=right>
<P><STRONG>85 917,8</STRONG></P></TD>
<TD align=right>
<P><STRONG>1,3</STRONG></P></TD>
<TD align=right>
<P><STRONG>1,3</STRONG></P></TD>
<TD align=right>
<P><STRONG>120,2</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>���������, ����������� ��������</STRONG></P></TD>
<TD align=right>
<P><STRONG>95 822,3</STRONG></P></TD>
<TD align=right>
<P><STRONG>112 448,3</STRONG></P></TD>
<TD align=right>
<P><STRONG>1,7</STRONG></P></TD>
<TD align=right>
<P><STRONG>1,7</STRONG></P></TD>
<TD align=right>
<P><STRONG>117,4</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>������ ������</STRONG></P></TD>
<TD align=right>
<P><STRONG>179 932,1</STRONG></P></TD>
<TD align=right>
<P><STRONG>214 934,0</STRONG></P></TD>
<TD align=right>
<P><STRONG>3,2</STRONG></P></TD>
<TD align=right>
<P><STRONG>3,2</STRONG></P></TD>
<TD align=right>
<P><STRONG>119,5</STRONG></P></TD></TR></TBODY></TABLE></TD></TR>
<TR>
<TD></TD></TR></TBODY></TABLE></DIV>
<P align=justify><STRONG>�������� ����������� </STRONG><STRONG>������������ </STRONG><STRONG>�������� �������������� </STRONG><STRONG>���������� </STRONG><STRONG>����������� ��� ����� ������ </STRONG>
<HR>

<P class=Ssilka align=justify><STRONG>1. </STRONG>������ ���������� �� 619 ��������� ������������, ������������� ���������� ���������� � ���������� ��� ����� ������ �� 1.07.2006, � ��� ����� ����������� �� ��������� ������</P></TD></TR></TBODY></TABLE>


</body>
</html>

