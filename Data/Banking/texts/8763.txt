<!--
ID:37318561
TITLE:��� ������
SOURCE_ID:11517
SOURCE_NAME:���
AUTHOR:
NUMBER:3
ISSUE_DATE:2007-03-31
RECORD_DATE:2007-02-22


-->
<html>
<head>
<title>
37318561-��� ������
</title>
</head>
<body >


<P class=MsoNormal style="MARGIN: 0cm 0cm 0pt; TEXT-ALIGN: justify; tab-stops: 297.0pt"><STRONG>CADILLAC ������������ ��������� SRX</STRONG></P>
<P class=MsoNormal style="MARGIN: 0cm 0cm 0pt; TEXT-ALIGN: justify; tab-stops: 297.0pt">������� ����������� �� ��������� ������ ��� � 2004 ���� ��������� Cadillac SRX ��������� � ���� ������ � ����� � ��������� ���������� ����������. �������� ������� � �������������� ����������� � ������� V6, � �������������� ������ � V-�������� "����������" � 6-����������� "���������". ��������� ����������� ����� ������������ ������������ � ������ �������� � ������ ���������, ���������� �������� ���������, 18-��������� ������ ������� � �������� ������. � ������ SRX 2007 ������� �������� ���������� �����������, �������� ����������� � �������������. �� ���������� ������� ������� ������������ ����� ������ ���������� ��� ������� ������������ ��� ������ �������. �������� ������� ������������ Bose 5.1 Cabin Surround, � ����� ������������� DVD-������� � ����������� ��������� �������� ����� ����������� DVD-�������. ���� �� ��������� ������������ ������� � ����������������� General Motors �� ������ ���������� ������ �������� �� �������.</P>
<P class=MsoNormal style="MARGIN: 0cm 0cm 0pt; TEXT-ALIGN: justify; tab-stops: 297.0pt"><STRONG>Great Wall Hover ������������</STRONG></P>
<P class=MsoNormal style="MARGIN: 0cm 0cm 0pt; TEXT-ALIGN: justify; tab-stops: 297.0pt">���������� �� ����� ������� ������� ������ ������������ �������, �������� "�����" ������� �� ����� �������������� ����������� Great Wall Hover. ������ ��������������� ���������, ���������� ������������, ����������� ������������� ������ � ������� ��� � ������ ������� ������� ��������� ��������� ������ Hover, � ����� ������������ ������������ � "���������". � ����� ������ GW 2,8 INTEC CRDi ��������� <?xml:namespace prefix = st1 ns = "urn:schemas-microsoft-com:office:smarttags" /><st1:metricconverter w:st="on" ProductID="98 �">98 �</st1:metricconverter>.�, ��������������� ������������� ����������� ����-3, ��������� ���������� Turbo Common Rail. �������� �� �������� �����������, ���� ��������� ���������� ���������� (<st1:metricconverter w:st="on" ProductID="7,1 �">7,1 �</st1:metricconverter> ������� �� <st1:metricconverter w:st="on" ProductID="100 ��">100 ��</st1:metricconverter>) � ������������ ��������������� ������ ��� ������� �� -35 &#176;�. � ���, �� ������� ��������� ��������� ��� ��� ���������, ����� ������� ��������������� ����� ������� ������.</P>
<P class=MsoNormal style="MARGIN: 0cm 0cm 0pt; TEXT-ALIGN: justify; tab-stops: 297.0pt"><STRONG>������ ���������� �� ���������</STRONG></P>
<P class=MsoNormal style="MARGIN: 0cm 0cm 0pt; TEXT-ALIGN: justify; tab-stops: 297.0pt">������ ���� � ������ ����� �������� ������� ���������� �������� �� ����������������� ��������, ��� ��������� ��������� ������������� �������� �� ������ �����, ������� ����������� ������������ ���������� ������������ �������-������������� ��������� � ��������������� ����� ��������. "��������� ��������, ������� ���� ���������� ����� ������� ����, ��-�� ��������� ������ ������ �� �������������� � ������ ������ � ������, ������� � �������� ������, ��������� �� ����������", - ������� �� � �������, ��� �� ��������� ��������� ����� � ���. �� ������ ���������� ���������, � ����� �������, ����� ���� - ��� ���� ��� ��������������, � ������ -����� ����� ���������� ��-�� ��� � ��������� ��������: �������� � �������, ���������� ������������ ��������� �� ������ ����� ����� ���������� ������, ������� � ���������� ����� �������� ��������� �������� �������.</P>
<P class=MsoNormal style="MARGIN: 0cm 0cm 0pt; TEXT-ALIGN: justify; tab-stops: 297.0pt"><STRONG>������ ���������� VW Touareg</STRONG></P>
<P class=MsoNormal style="MARGIN: 0cm 0cm 0pt; TEXT-ALIGN: justify; tab-stops: 297.0pt">� ����� ������� Volkswagen �� ��������� � ������ ������ ������� ��������� ������������ Touareg. ������� ���������� �� ������ ���������������, ��� ������������� ���������, �������� �������� �����, ����������� �������� ��������� � ������������ ������. ����� ����, ���� ����������� ������ ������ ����������� �������� �����, ���������� 8-����������� ���������� ���������� � ���������������� �������� ������� V8 FS! ��������� <st1:metricconverter w:st="on" ProductID="350 �">350 �</st1:metricconverter>.�. � ������� ���������� �� ������� ��� ��������� W12 ��������� 450 "�������". ��������� ������ ���� �� ������ ���������� ������ ������������� �������� �����������, �������� ������, ��� ���� ��������� �� ������� ������.</P>
<P class=MsoNormal style="MARGIN: 0cm 0cm 0pt; TEXT-ALIGN: justify; tab-stops: 297.0pt"><STRONG>BMW ������� ����� ������ �����</STRONG></P>
<P class=MsoNormal style="MARGIN: 0cm 0cm 0pt; TEXT-ALIGN: justify; tab-stops: 297.0pt">����� ������� �������� �� ��������� ���������� � �������� ��������� ������� ������ � ���� ������ ��������� ��������� ���������� �� ���� BMW ������� �����. ��� ���� ���� � ������� �������� ������� ����������� ����. ����� ����������� ����������� �� �������� �����, ��������� �� ���� ������, ������������� ������������ ��� ������� ����� �� 23 �������, � ������������ �� ������� �������. ���������� ����� "������" ������ ���������� � ������������� ���������, ������� �� ��������� �������������� 4- � 6-����������� ���������: ��������� ������� ������ ���������� �������� � ���� ������. � ������� ������������ ���������� �� ����� ���������� ���������� � ��������� ���������� ���������� 6-������-����� "���������", ������ � �������� ����� �������� ������������������� 6-��������-��� ���� � ������������ ������������ ������� �������� �� ������� ������. ���� �� ���� � BMW ������� �������� ����� � ������ ������.</P>


</body>
</html>

