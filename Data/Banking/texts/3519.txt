<!--
ID:36669108
TITLE:����� ����������� ������ �� ���������� ������ (���� ������) �������� ������������ ������������ �.�.����������� (�� 24.05.2006 � 06-��-04-1/7795)
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:12
ISSUE_DATE:2006-06-30
RECORD_DATE:2006-09-11


-->
<html>
<head>
<title>
36669108-����� ����������� ������ �� ���������� ������ (���� ������) �������� ������������ ������������ �.�.����������� (�� 24.05.2006 � 06-��-04-1/7795)
</title>
</head>
<body >


<B>
<P align=justify>� ���������� ���� ������<BR>� �� ������������� �����������<BR>�������� �����������</P></B>
<P align=justify>���������� ���� ��������� �� 02.05.2006 &#8470; �-01/1�-230 ������������ ���������� ���� ������ � �� ������������� ����������� �������� ����������� � ������������ � �������� ���������� �������� �����������, ������������� �������� � ������� �� �������� ��������� �� ����������� ������ �� ���������� ������, ������������ �������� ���� ������ �� 20.04.2005 &#8470; 05-16/��-� (����� &#8211; ������� ���������� ��������), ���� ������ �������� ���������.</P>
<P align=justify>� ������������ � �. 3.1 ������� ���������� �������� ������������ ������ ����������� ������������ ����������� ������������; ������������ ����������� ����� ����; ������������� ������� �� ������. ���������� �������� �������� ����������� �� ����� �� ���������� ����� �������������� ��� ������� ����������� � ������ �������, ��� � ���������� ������������ �������� �� ������������, ����������� ������������� ������ �����������. ����� �������� ��������� ��������, ����� �� ����������� �������� ��������� �� ����� ������� � ����������� �� ����� �� ���������� � ������������ �������� ����� ����������, ��������������� ������������.</P>
<P align=justify>�������� �. 3.7.4 ������� ���������� �������� ����������� ������� �� ����������� ���������� ���������� � ������������� �� ���� ������������ ������� � ���������� ����������, ����� ���������� (� ��� ����� � ����������� ����) � ������������� ������� ���� � ����������� ��������� ������������ ����������� ��� ������ � ���������� ����������� � ������ "������ ��� ������", ��� ���� ���������� ����� ���������� �� �������� ��������� ������ ���� �����, �������������, ��������� �������������� ����� � �������� ������� �����������. �������������� � ������������ � ��������� ������� ���������, ��� �������, �������������� ����� ��������� ����������� ������������ ���������� ���������������� ���������� ��������� � ����� �� ������������, ���������� ��� ���������� ����������� �������� � �������, ������������� �������� 4 ������� ���������� ��������, � �������� �. 4.11 ������� ���������� �������� ��� ���������� ��� ���������� �������� �������� ��������� ����������� � ��������� ���� �������� � �������� �������� � ���� ������ (�� ���� ������). � ������������ � �. 3.9 ������� ���������� �������� ��������������� �� ���������� ���������� ���������� (�� �����) ��� ����������� ��������������� ���������� � �������� ������������ ���������� ������������� ��� ��������������� ��������.</P>
<P align=justify>� ������������ � �. 6.1 ������� ���������� �������� �������� ��������� �������� � ��������� ��������� ������������ ���������������� ���������� ���������, � ��� ����� ����������� �������� �����, ����������� � ����� �� ������������, ����� ���� ��������� ��� ����������� ��������� � ���� ����������� ��������. ��������� ��������, ��� �������, ���������� � ���������� � ������� ���������� � ����������� ���, ����������� � ���� ������ (�� ���� ������). ������������ �������� ��������� �������������� � ������������ � ��������, ������������� ����������� ����������� ���� ������ (�� ���� ������) � ������� ������������ ���������� ���������, ����������� ����������� ��������������� ������������ ����������� � ������������� ����������, �������� � ����������, ����������� ��� ������������ ��������� �� ��������. ����� ������������ ������ �������������� ������ ���������� ������������ ������������ ������������ ��� ���������� � ������������� ���� ����������� � ������������� ���������� ��� �� ���������� ��������� ���������������� ���������� ��������� ���������� �. 7 ��. 44 ������������ ������ �� 22.04.1996 "� ����� ������ �����" � �. 5.3.7 � 6.1 ��������� � ����������� ������ �� ���������� ������, ������������ �������������� ������������� ���������� ��������� �� 30.06.2004 &#8470; 317.</P><I>
<P align=justify>� ������������� ������ ���� ������, ������������ ����������� ����������, ���������� ������ �� ������ �������, ������� �������� �������������� ��������� ����������� � ��������������� ������� ���� ������.</P>
<P align=justify>��� ������� �� ������ ������, �������� �������� ���������������� �������� ���� ������ ����������� �� ����� �� ���������� ����� ����������� �� ������ ���� ������� �����������, �� � ���������� ��������������� �� �� ������������. ����� �����������, ��� ������� � ������ ���� ������, ������������� � �. 3.1 ������� ���������� �������� �����������, ������������� �������� � ������� �� �������� ��������� �� ���� ������, ������������� �������� ���� ������ �� 20.04.2005 &#8470; 05-16/ ��-�.</P>
<P align=justify>����� ����� ��������, ��� ������ �� ������� ���� ������ ��������� ������������ ���������� ������������ �� ���������� ��������������� ��������������� ������� �� ������ ����������, �� � ����� ����������. ��� �������� ���� ������, ��� ������������ ������ ����� ���, ��� ��� ���������� �������� ���������� ��������� ����������� � ��������� ����, � ������� ����������� ����� ����������, ������� ������ ��������.</P>
<P align=justify>� ��������� ���������� ������� ��������, ������� ����� ���� ����������� ���������� �� ���������� ��������, ���� ������ ��������, ��� ������ ��������, ��� �������, ������������ ������ �� ���������� ��������� � ����� ���������� � ����������� ���. ����� ����, �������� �. 7 ��. 44 ������������ ������ �� 22.04.1996 "� ����� ������ �����" ���� ������ ����� ����� ��������� �� ��������� � ���������������� ���������� ����� ������ ����� ������������� ����� ����������, ����������� ��� ������� ��������, ����������� � ����������� ���� ������.</P>
<P align=justify>��������� � ���� ������ ������� ������ ����� ���, ��� �����, ������������ ������� �������������� ������� ���� ������ � ������������� �������������, ����� ����� �������������� � ������������ ��������, ��� �������� � �� �������������� ����������. � ����� � ���� ���������� ���������� ������ � ���������� ����� ������� ��������� ����������������� ������� �������������� ��������� ����������� � �������� ���� ������.</P></I>


</body>
</html>

