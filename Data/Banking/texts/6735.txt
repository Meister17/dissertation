<!--
ID:38658255
TITLE:���������-��������� ������������� ��������������: ���������, ��������, �����������
SOURCE_ID:3144
SOURCE_NAME:������ � ������
AUTHOR:�. �. ������-���������, ��������� �������� ���������� ����� ������ �� ����������� ����; �. �. ���������, ��������� ������ ���������� ����������� ����������� � ��������� �������� 
NUMBER:12
ISSUE_DATE:2007-12-31
RECORD_DATE:2008-01-18


RBR_ID:1093
RBR_NAME:���������� ����
RBR_ID:2039
RBR_NAME:���������� �������
RBR_ID:4931
RBR_NAME:���������� ��������, ���������� �����
RBR_ID:4935
RBR_NAME:������ � ����������� ������������ �� ��
-->
<html>
<head>
<title>
38658255-���������-��������� ������������� ��������������: ���������, ��������, �����������
</title>
</head>
<body >


<P align=justify>� 90-� ���� ������ � ����� ���������� � ������ ������������� � 10 ���� ���. ��� � �������� ����� ������ � 2001&nbsp;�. �� ������ 2004 �. ����� �������� ������� ��������� ����� � 20 ���� ���. ���. ��� ����� ���� �������� ��������� ������, ������������ ������� ���������� � 2003&nbsp;�. �&nbsp;2006&nbsp;�. ����� ���� ��������� ����� �������� �������� ��������� ������ ����� ��������, ����������� � ������. ������������ ������������ ������ � ����� � 2006 �. �������� 33,4 ���� ���. ���, � ����� ���������-��������� ������������ �������� &#8211; 6,98 ���� ���. ���<SUP><STRONG>1</STRONG></SUP>.</P>
<P align=justify>�� ������� ������������� ���� ����� � 2006 �. � ������ ���� ���������� ����� ������ &#8211; ���������� � 2010 �. ������������� ������������� �� ������ 60&#8211;80 ���� ���. ���<SUP><STRONG>2</STRONG></SUP>.</P>
<P align=justify>��� ������ ������������ ����� �������� ����� � ��� ��������� ��������������� ��������������. ���������� �������������� ������� ������������� �����- � ������������, ������������ �� �������������� � 2020 �. ������� �������� ����� � ���� ������ � ������ ���, ���������� ��������������� ����������� � ����������, ������������ ������ ������������ ������������� ������� �������������� � ���������� ������� ���������� �����������. ������� �������������� �������������� �������� ����� ���� ���������� �� ���� ������������ ����������� ��������� ���������� � ��������� ���������� ��������� � 2020 �. � ������� 12 ���� ���. ���<STRONG><SUP><STRONG>3</STRONG></SUP></STRONG>.</P>
<P align=justify>� 2005 �. ������������ ����� ����������� ������������� �������������� ����� &#8211; �������������� � �������� � �������� ������ ������������� ��� � ������. ��������� �� ���������� ���������� ������ ������������� ��� ��������������� ��������� �������� ��� �������������� ����������� ����������� ����� ���������� � ������.</P>
<P align=justify>��� ��� ���� ��������� ��������, ��� ������������� ����������� ������� ������� ������ ��� �������� �������������� �������������� ������ � �����.</P>
<P align=justify>� ��������� ����� �������� �� ������ ������������ ������� � ������������ ����������� ������� ������ � ����� ������� � ������������ �������� � �� ��������. � ��� ���������:</P>
<P align=justify>&#183; &nbsp;������� ������ �� ������������� ��������� �������������� ����� � ����������;</P>
<P align=justify>&#183; &nbsp;�������� ����� ���������� ������������ � ��������� � ����� � ����������� ��������� ���������� ���������� ������ ����� ��� ����������� ������������ � ������������ ������� ������ (�&nbsp;����� � ����� � ����������� � ���) ��� ����� ������ (� ������ � ���������� ��������������� � ������������) �����������;</P>
<P align=justify>&#183; &nbsp;����� �� ���������������������� ���������� ������������ � ������� � ������������� ������������, ��� ������������ �������� ��������� ���������� ����� ������������ � ������������� ����� ���������� ����������;</P>
<P align=justify>&#183; &nbsp;����������� ����� ����������� �� ����������� ������� � ���������� �������� ���������������� ��������������� ������;</P>
<P align=justify>&#183; &nbsp;���������� ������������� ������������ ������ ����� ������ �� ���������� � ������� ����� IPO.</P>
<P align=justify>���������� ���������� ������, ��� � ���������, � ��������� ����� ���������� ������ �������������, ������������, ������ � ������ �������������� ����������� ��������, �� ��������� �������� ����������� ��������, ���������������� ���������� � ����������� ������ � �����.</P>
<P align=justify>������� �������������� ���������� �������� (� ��� ����� � � ���������) �������� ����� ������� �� ��� ��������� ���� � ����������� ������� � �������������� ����������� ����������� �������. �������� ����������� ������� ����������� ������� ������ �� ������ 2003&#8211;2006&nbsp;��. ��������������� � ��������� ���������� ���������� ����������� ������� ������� �� ��������. ������� � 2004&nbsp;�. ����������� ������������� ���������� ������ ��������� ����������� ������. � �������� ��� ����������� �� ���� ��������� ����������� ������� ������ ���������� � ���� ���� � ������ �� ������������. �� ��������� �� 01.01.2007&nbsp;�. ������������� �� ��������, ���������� �� ������������ ����������� �������, ��������� 67,8 ���� ���. ���<STRONG><SUP><STRONG>4</STRONG></SUP></STRONG>.</P>
<P align=justify>������ � ��� �������� ��� ����� � ����������� ������� � �������������� ����������� ������� ������ �������� ��������������, ��� ����� ����������������� � ������ ������ �������������� �������������� ���� ����� �, ��� ���������, �� ���������� �������� (����. 1).</P>
<P align=right>� � � � � � � 1</P><B>
<P align=center>����������� ������ � ������������� ����������� ������� ������,<BR>������� �� 1 ������, ��� ���. ���*</P></B>
<TABLE cellSpacing=1 cellPadding=7 width="100%" align=center border=1>
<TBODY>
<TR>
<TD vAlign=center width="41%">
<P align=center>����������</P></TD>
<TD vAlign=center width="17%">
<P align=center>2004 �.</P></TD>
<TD vAlign=center width="15%">
<P align=center>2005 �.</P></TD>
<TD vAlign=center width="14%">
<P align=center>2006 �.</P></TD>
<TD vAlign=center width="13%">
<P align=center>2007 �.</P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P align=left>����������� ������</P></TD>
<TD vAlign=top width="17%">
<P align=right>13&nbsp;680,5</P></TD>
<TD vAlign=top width="15%">
<P align=right>15&nbsp;937,6</P></TD>
<TD vAlign=top width="14%">
<P align=right>22&nbsp;797,6</P></TD>
<TD vAlign=top width="13%">
<P align=right>40&nbsp;780,0</P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P align=left>� ��� ����� ���������� � �����</P></TD>
<TD vAlign=top width="17%">
<P align=right>12,7</P></TD>
<TD vAlign=top width="15%">
<P align=right>67,6</P></TD>
<TD vAlign=top width="14%">
<P align=right>44,5</P></TD>
<TD vAlign=top width="13%">
<P align=right>41,6</P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P align=left>����������� �������������</P></TD>
<TD vAlign=top width="17%">
<P align=right>16&nbsp;276,2</P></TD>
<TD vAlign=top width="15%">
<P align=right>19&nbsp;842,0</P></TD>
<TD vAlign=top width="14%">
<P align=right>29&nbsp;463,3</P></TD>
<TD vAlign=top width="13%">
<P align=right>58&nbsp;823,9</P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P align=left>� ��� ����� ������������� ����� ������</P></TD>
<TD vAlign=top width="17%">
<P align=right>23,5</P></TD>
<TD vAlign=top width="15%">
<P align=right>26,2</P></TD>
<TD vAlign=top width="14%">
<P align=right>39,1</P></TD>
<TD vAlign=top width="13%">
<P align=right>497,2</P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P align=left>������ ������</P></TD>
<TD vAlign=top width="17%">
<P align=right>-2&nbsp;595,7</P></TD>
<TD vAlign=top width="15%">
<P align=right>-3&nbsp;904,4</P></TD>
<TD vAlign=top width="14%">
<P align=right>-6&nbsp;665,7</P></TD>
<TD vAlign=top width="13%">
<P align=right>-18&nbsp;043,9</P></TD></TR>
<TR>
<TD vAlign=top width="41%">
<P align=left>� ��� ����� � ������</P></TD>
<TD vAlign=top width="17%">
<P align=right>-10,8</P></TD>
<TD vAlign=top width="15%">
<P align=right>41,4</P></TD>
<TD vAlign=top width="14%">
<P align=right>5,4</P></TD>
<TD vAlign=top width="13%">
<P align=right>-455,6</P></TD></TR></TBODY></TABLE>
<P align=justify>* ������� �� ������������� ��������� � ����� �������� � ���������, ����������������� ������, � ����� �������� ����������� ������ � ������ ��������� �����������.</P><I>
<P align=justify>��������</I>: ������� ����� ������. &#8211; 2004. &#8211; &#8470; 26&#8211;27 (750&#8211;751). &#8211; �. 185&#8211;186; 2005. &#8211; &#8470; 25&#8211;26 (823&#8211;824). &#8211; �. 215&#8211;216; 2006. &#8211; &#8470; 28&#8211;29 (898&#8211;899). &#8211; �. 165; 2007. &#8211; &#8470; 26&#8211;27 (970&#8211;971). &#8211; �. 183&#8211;184.</P>
<P align=justify>���� ����� � ����������� ������� � �������������� ����������� ����������� ������� � ��������������� ������� �� �������� � 1% ��� �������� ���� ����� � ���������� �������� � ������� ������� � 2006&nbsp;�. 5,2% � 9,4% ��������������. ��� ��������������� �� ���������� �������������� �������������� �� ������������ � ��� ����� �� ������������ � 2010&nbsp;�. ������ �������� �������-������������� ��������� ����� ������� � ������. ������ � ��� � 2006 �. ����� �������� ����������� ������������� ���������� ������ ����� ���������� �������. ��� ������� � ����������� ������� ������������ ����������� ������� ������� � ���� �������� �� ��������� ������.</P>
<P align=justify>��������� �����, ��� �������, ����� ��������� ��������� � ������������ ���������� ������. &#8220;������� ��������&#8221; ��������� ������, ��������������� &#8220;������������&#8221; ����� ����� (��������������� ���� �������� ����� (����) � ���������-��������� ���� �����) ������������� ������� � ��������� ��������� ����� ������ ������� ���������� ������, ������� ������� ������������� �������, ��� ������� ����������� ����� ��������������� ����������� ����������� ���������� �������� ����� &#8220;Sinosure&#8221;. ���������� ����� ������������ ��������� ���������� ��������� ��������� ������� �������������� �������� ��������: ������� ����������� ����������� ������� ����������� ����������� � ��������� �����������, � ����� �������������� �������������� ������������ ���������� ������������������� ��������� ��� ����������� �� �������� � �����. ��� ���������� �����&nbsp;&#8211;������������ (� ���� 2005&nbsp;�.) � ������������� (� ������� 2006 �.) ��������� ���������� � �������������� � ��������������� ����������� ����������� ���������� �������� &#8220;Sinosure&#8221;. ������������ � ����� 2007&nbsp;�. �������� ��� ������� ��������� ���������� � ���������-��������� ������ ����� � ����, ������� ��������������� �������� ����������� ����� ��������� ����� �� ����� 500 ��� ���. ��� ������ ��� �������������� �������� ��������� ������� ����������� ����������� � ������ � ���������� ������� � ��� �������������� ��� ��������� �������� ��������������� ����������� ����������� ���������� �������� &#8220;Sinosure&#8221;. �������������� ��������� ���������� � �������������� (�������������� ��������� �����) � � ����� �� &#8220;������� ��������&#8221; ������������� ������� �����: ������ �����, �������-������������ ������ ����� � �������������������� ������ �����, � ����� � ������� ����������� ������ &#8211; ����� ��������� ������<STRONG><SUP><STRONG>5</STRONG></SUP></STRONG>.</P>
<P align=justify>� ������ 2005 �. �������������� (� ��������� ����� &#8211; ��������������� ���������� &#8220;���� �������� � ������������������� ������������ (��������������)&#8221;) �������� � ���� ��������� ���������� �� ����� 300 ��� ���. ���. ���������� ���������������� �������� ���� ���������� �� �������������� �������������� ���������� �������������� ������������������� ���������, ��������������� ��� ��������� ���������. � ������ 2006&nbsp;�. �������������� � ���� ��������� ���������� � ��������� �����, ����������������� �������������� ���������� ���������-��������� �������� �� ����� ����� 500 ��� ���. ���. ��� ���������� ��������������� ��������� ����� �������������� ���������� ��������. � ���������, �������������� ��������������� ������������ � ����������������, � ����� ������������� ����������� �������� � ���������� ������������� ��������<STRONG><SUP><STRONG>6</STRONG></SUP></STRONG>.</P><I>
<P align=justify>������ ����� � ������������� �������������� �������� ������������ �������� �� �������� � ������������ ������� � �������������� ������������ ����� ������ � �����. </I>�������������� �������� � ������������������� �������� �������� �������� ��������� ����������� �������� ��������������� ������������� �������������� ����� ������� � ������ �� ������������ ������. ������� ��� �������� �������������� ����� ����� �������� ������� ������������ �������������� ������ ������� ��������� &#8220;���������-������������� �������� �������� ������� � ���������� �� 2013&nbsp;����&#8221; � ���������� ����� &#8220;���� ����������� ������-������� �����&#8221;, �������� ����� ������� �������� ��������� �������������� ������ �������� ��������.</P>
<P align=justify>���� ����� �� ������� �������� ���������� �������������� ������������ �������� ������� ����������� ����, ��� � ����� �� ������. � 2006&nbsp;�., �� ������ ���������������� ����������� ����������, ��� ��������� 30,8%. ���������� �������� ����� ���������� �����: ��������� ���������� ������� &#8211; 95,9%, �������� ������� &#8211; 84,9%, ����������� ���� &#8211; 47,0% � ���������� ����&nbsp;&#8211; 39,9%. �� ������ ������������� � ������ ���������� ����������� � ���������� ���� &#8211; 2,03 � 1,64 ���� ���. ��� ��������������<STRONG><SUP><STRONG>7</STRONG></SUP></STRONG>.</P>
<P align=justify>� ������ 2000 �. � ������ �������� �� ���������� ���������� ������ ���� ������������ ������ � ����� ����� �������� �����, �������������� �������������� ����� ����� �������� � ���������� ����� &#8211; ����������� �� �������������� ��������������. ��������� ����������� ���������� �� ���� ������ ���� � ��� ���������� � ������ ������ (����. 2).</P>
<P align=justify>�������� � ��������������� ������ ��������� ����������� ����������� �� �������������� �������������� �������������� ������������� ������������� ���������� � ������� ������� ��� �������� ������������� �������������� ��������������.</P>
<P align=justify>� ������� 2002&nbsp;�. ����� ����������� ������ ���������� ��������� � �������� ������ ����� ���� ��������� ���������� � ������������� �������� � �������� � ������������ ������� � ������������ ������� ������ � �����. � 1 ������ 2003&nbsp;�. ������� �� ������������ �������� � ������������ ������� ������ ������������ ������������ ����� (����������� ������� ������) �������� ������� � ������� ��������� ������ � �. �����. � 1 ������ 2005&nbsp;�. �������� ���������� ���� �������������� �� ��� ������������ ������ ������ (���������� �����, ���������� ����, ����������� ����, �������� �������, ��������� �������, ��������� ���������� �������) � ����� (��������� ����������, ���������� ����� ���������� ��������, ��������-��������� ���������� ����� � ��������� �������).</P>
<P align=justify>��� ������������� �������� � ������������ ������� ����������� ������� (���������), �������������� � ������������ �������, ����������� ������-����� � ����� � ��������� ������ � ����-����� � ���������� ������ �������� ��������� ������, ������������� �� ������������ � ������� ������� �����. � ��������� ����� �� ������� 21 � 20 ��������������. �� ������� ����� ���������� ���������� � ������������ ����������������� ��������� �������� ������� ��������������������� ����� ����� � �������-������������� ����� �����.</P>
<P align=right>� � � � � � � 2</P><B>
<P align=center>����������� ��������� ����������� �� �������������� �������������� ������ � �����</P></B>
<TABLE cellSpacing=1 cellPadding=7 width="100%" align=center border=1>
<TBODY>
<TR>
<TD vAlign=center width="24%">
<P align=center>���� � ����� ���������� ���������</P></TD>
<TD vAlign=center width="76%">
<P align=center>�������� �������� ����������� ������� �������������� ��������������</P></TD></TR>
<TR>
<TD vAlign=top width="24%">
<P align=left>25&#8211;26.10.2000 �.,<BR>�. �����</P></TD>
<TD vAlign=center width="76%">
<P>����������� � ��������� �������� �������������� ��������������.</P></TD></TR>
<TR>
<TD vAlign=top width="24%" height=4>
<P align=left>26&#8211;28.06.2001 �.,<BR>�. ������</P></TD>
<TD vAlign=top width="76%" height=4>
<P align=justify>�������� ����������� �������������� ��������������, ����������������� ��������� ������������ ��������.</P></TD></TR>
<TR>
<TD vAlign=top width="24%">
<P align=left>27&#8211;28.05.2002 �.,<BR>�. ������</P></TD>
<TD vAlign=top width="76%">
<P align=justify>� ���������� <I>���������� </I>����� ������������ ������� ���� �����,<I> �� �������������</I> � �������� ������������ <I>������������ ����� ���� ����� � ������������� �������� �� ������������ �������� � ������������</I> <I>�������</I> �������� ������� �&nbsp;�. �����.</P></TD></TR>
<TR>
<TD vAlign=top width="24%">
<P align=left>30.06&#8211;03.07.2003 �.,<BR>�. �����-���������</P></TD>
<TD vAlign=top width="76%">
<P align=justify>����������� �������� ��������������� �������������� ���������� �����������, ������������ ���� ��������� � �����.</P>
<P align=justify>� ������������ ������� �������� � ���� ��������, ���������� �� ����������������� ������ ������.</P></TD></TR>
<TR>
<TD vAlign=top width="24%" height=33>

<P align=left>22.06.2004 �.,<BR>�. �����</P></TD>
<TD vAlign=top width="76%" height=33>
<P align=justify>��������� ����� �������� ���������� � ��������������� ��� ������� �� ������������ ������ ������ � �����.</P></TD></TR>
<TR>
<TD vAlign=top width="24%">
<P align=left>20&#8211;21.07.2005 �.,<BR>�. �����������</P></TD>
<TD vAlign=top width="76%">
<P align=justify>��������������� ������� ���������� �� ������� � ������� ��� �������� ������������� �����.</P>
<P align=justify>������������� ��������� ���������� ���� � ���� ������������ ����������� �������� � ������ � �����.</P>
<P align=justify>��������� ��������� ����� (������) �������� ������ � ����� ������� ���� �����.</P>
<P align=justify>����� ����������� ����� ������������ ������� ���� ����� � ����� ����� ���������� ��������.</P></TD></TR>
<TR>
<TD vAlign=top width="24%">
<P align=left>04&#8211;06.09.2006 �.,<BR>�. ������</P></TD>
<TD vAlign=top width="76%">
<P align=justify>���������� ���� ��������, ��������� � �������� �������� ������������ � �������������������, ������������ �������� � ���������� ����.</P>
<P align=justify>�������� ����������������, �������� ������ � �������� �� ���������� ������ � �����.</P>
<P align=justify>���������� ������������ ������ �� �������������� �������� � ������� �������������� �������� � �������������� ��������������� ��������.</P>
<P align=justify>������������ ������� ������� ����������� ��� ������������ ��������������� ������ ����� ������������ ������� ���� �����.</P></TD></TR>
<TR>
<TD vAlign=top width="24%">
<P align=left>04&#8211;05.09.2007 �.<BR>�. ������</P></TD>
<TD vAlign=top width="76%">
<P align=justify>����������������� �������������� � ����� ��������� � ���������� �������������� � � ������� ������������� �������� � �������������� ���������� ����.</P>
<P align=justify>����������� �������������� �� ������������� ����� ��������.</P>
<P align=justify>��������������� ���������� �� ������������� ������.</P>
<P align=justify>����� ����������� � ������.</P></TD></TR></TBODY></TABLE>
<P align=justify>���������� ���������� � ������������� �������� ������� ����� ��������� ��� �������� �������������� �������������� �� ������������� �������� � ������������ ��������. �� ������ �������� ���������� ����� �������������� ��������� ����� �������� �� ������������ �������� � ���������� ������, ��� ����� ������������� � �������� ������ �� ��������� ����� � ����������� � ��� ������� ����������������.</P>
<P align=justify>��������, ������ ���������� ������ ����������� ���� ��������������� � ������������ ����� �������� �� ������ ���� � ������ �������� ��������� ������. ���, � IV �������� 2006 �. �� ��� ���� ��������� ����� ������� �� ������� � ���������� ������, ������� �� ���� ������ �������� ���������� �� ���������� ����������� ���� (� 1 ������ 2005&nbsp;�.). �� II �������� 2007&nbsp;�. �� ��������� � ����������� �������� �������� ���� ����� ����������� ������� ���������� ����� ��� � 3 ����.</P>
<P align=justify>������ � ��� ������ � �������������� ����������� � �������� �������������� �������������� � ������ ����������, �� ������ ������, ��� ����������� �������� ���������, �������� �� ��������������, ����������� ��� ���������� �������� �������������� �������������� �� ���������� ���������� �����������. ��� ������� ��� ��������� �������, �������������� ����������� ���������� ����������.</P><I>
<P align=justify>�������� �������� ��������</I> ������� � ������������ ����������� ����������.</P>
<P align=justify>����� �� �������� ����� ������ ������ �� ���������� �������� ���� �� ������������� � �������� �� ������������ �������� ������� ������, � ������ ������� ���. ������ ��� ������������� ����������� �������� �����&#8211;���� �� ������������ �������� ��-�������� � �������� ��������� ��� ������������ ����� ������������ ������ ���. ��� <I>���� �� �������� ��������� ������� �������</I> � ������������� �������������� �� ������������ ������������ �������� ������ � �����.</P>
<P align=justify>�������� ������ ���������, �� ������ ������, ��������, ������ �����, ���������� � ����� ����������� ������ ��������� ���� �� ��������� � ����������� ����� ��� ����� ����� � ������������� ��������, ��������� ������������� ������������� �������� �� ������ � ����� �� �������� ������ ����� � ������ ��������������, � ����� ���������� �������������� ����� � ������������ �������.</P>
<P align=justify>� ����� � ���� ������� � ���������� ��������� ������, ��������� ����������������� ����� � ������, �� ����� ����������� ���������� ������������ ������� �� ������ � ��������� ������: ���������, �� ��������� ������������� ��� ��� ������������ ����������� �������� �� ������ � ���������� ������ � ������� ���. ��� ���������, ����� ����-������ � ������, �������� ����������� ������� �������� ��������� ������, ������������� � ������������ �������, ����� ���������� �������� ����������� ������������ �����.</P>
<P align=justify>����� ����, ����������� ������������� ������� ������������� � ������ � ����� ���������� ��������������� � ����������, ��������������� � �������� ���, ���� � ������ �������� �������������� ������. ������� � ���������� ������ � ��������� ����� ��� ������ � ����� �������������� ������ ���� ��������� ������������� ���������� �����������, ��� ���� ������� � ��������� ���������� ������������ �������� ������ ����� �����. �� ��������� ���� �������� ���������������� ������ � ����� ���������� ������������ ��������� � ������� ��� �������������. ���������� ����������� �� ������������� ����������� ����� ��� ���������� ������������� �������� ����������� �� ��������, � �� �������� ��� ����� ������ � ����������� ������������ ��������� ������� ������������ ��� ��������.</P><I>
<P align=justify>������ ���������</I> �������� ������� ����� �������� �������� �������, ������������ � ������������ ������� ���� �����.</P>
<P align=justify>������ ����������, ���������� �� ���������� ������ � �������������� �������� ������ �����, ����������, ��� ������ �� ��������, ��������������� ��������� ��������, �������������� ��������� ������ �������������� ������ �� �������� �� ��������������� ������. ������ ����������� ����������� ������ �������� �������, ������ ����������� ������� �������� ������� �� ��������� ���������� ��� ���������� ����� ��������� ������������, ��� ��� ������������ ������� �� ������� � ������������, ������� � ��������� ��������� ��������� ���������� �� ����������, &#8220;�����&#8221; ������.</P>
<P align=justify>� ��������� ���������� �������� � ��������� ��������� ���������� ��������������� ������ ��������� ����������� ������ ������������� ����������� ������ � �������� ��� �� ���������� ��������� � �����. � 2006 �. ����� ������������ � ��� ������ �������� 2,5 ���� ���. ���, ������� ����� ������ �������� &#8211; ����� 20 ��� ���. ���<STRONG><SUP><STRONG>8</STRONG></SUP></STRONG>.</P>
<P align=justify>�� ������������ ������ ����� ����� ����������. � ������&#8211;������� 2006 �. ����������� ������&nbsp;&#8211; ���������� ����� ����� ����� ����������� ���� ���������� � ��� � ������ ����������� � ���������� ��� ����� 365 ��� ���. ���. � ����������� ������� �������� ����� ������������ �������� ������� � �������� ��� ���� �������� ����� �� ������� �� �������� �����, � ������������ ������� �������� ����������� ���� ���, ������������������ � ������������ �������.</P>
<P align=justify>�������� ������ ��������� ������� ����� ������� ���������� ��������� � ������� �� ����������������� ������, �������� � ������������ �������, ����� ������� ����������� ����� � ���, ��� �������� �������� �������� ��-�������� �������� �������� ��������� ������� � ������������ �������.</P>
<P align=justify>�������� ��������� ������� ����� ��������� � ������������� ����������� ��� ��������������� � ����� ������� ��������� � ���������� �������� �������� �������. �� ������ ���������������� ����������� ����������, �� 2006 �. ����������� ������ �������� � ��� 99,7 ��� ���. ��� � 1015,5 ��� ���. � ������� �� ��� 115,6 ��� ���. ��� � 7537,9 ��� ������<STRONG><SUP><STRONG>9</STRONG></SUP></STRONG>.</P>
<P align=justify>����� �� ������ ���������� ��������� ������� �������� ���� ������������� �����. �� ������ ������������ �������������� �������������� � ������� ������������� ����������� ����, �� 2006&nbsp;�. ���� �������� 74,5 ���. ��������� ��������. �� �������� � ��� �� ��������� ������ �������� 768,7 ���. ���������� �������.</P><I>
<P align=justify>���������� �������� �������� �������, ���������� � ���������� �������� ������������� �����, � ����������� ���������� ������ </I>�� �������� � ������ ���������� ��������� �� ����������� ��������� ������� �� ����������������� ������ ������, �������� � ������������ �������, � ��������� ������ �������� �������� ������� � ������������ �������. ������ �������� �� ����� ��������� �� ����������� ������� ��������� ����������� �������������� �������������� ������� �������� ���������� �� ������� � ������� ��� �������� ������������� ����� � ������������ ������� �� ���������� �� �������� ��������� �������� ��� �� ���������. � ������������ ������� ����� ���������� ������������� &#8220;������&#8221; ����� �� �������� ������. ������� ������ ����� �� ��������� � ������� ������������ ������ � ������ �� ����� �������� ������ ����� ������� ���������� ��������� � ����� ���������� ����� ����������������� ������ � ���������� ������, �������� � ���������� ������.</P>
<P align=justify>�������, ���������� �� �������� ������������� ����� �� ���������� ����� � �������� ������, �� ������� �� ����� � ����� ������������ ������� �����, ������� ����������������� ����� � ������ � ���������� ������. ��� �������� (� ��� ����� � ����������) ����������� ������ �� ���������� ���������� ���������, �������������� � ������� ��� � ����������� ������� �� ����� ����������� ���. ����� �������, ���������� ���� ���������� ����� �� ���� ������� ������ �� �������������� �������� �������� �������.</P>
<P align=justify>������� ������� ����� �� �������� ������������ � ������������ �������� ����� �������������� <I>���������� �������� � ����������� ������� ���������� � ��������� ���������.</P></I>
<P align=justify>� ������ 2005&nbsp;�. ����� �������������� ��������� China Union Pay � �������������� ���� ��������� ���������� �� ������������� �������� �� ���������� ������ ������ ��������� ������� � ������, � ����� � ����������� ������� ���������� ���� � ������ ��������� � ������.</P>
<P align=justify>���������� ������������ ����� ���������� ��������� ��������� � �������� ��������������������� ����� ����� � ���������, ������ � �����������, ����� ������� ������������� ���������� ����� ���������� ��������� ������� &#8220;������� ������&#8221;. ������ ������ �������� �������� ������� ���������� ������ � ��������� ����� � �������� ���, ��� ����������� ������������ ����� ��������. �� ���� �� ����������� ���� ���������� ����������� ��� ������ �� ������������� ������������ ����� ��� ������������� �������� �� ���������� ������.</P>
<P align=justify>������� ��������, ��� ���������� ������� ������� � ������ �������� �� ���������� ������� �������� �������� ������� � ������������ ������� ��������� � � ����� ������ ���������� ������ �� ��������������� ����������� �������, ���������� ���������� �����, � �������������� ����������. � ����� � 1 ������ 2007&nbsp;�. ����� ����������� ����� � ������ � ���������� �����. �� ��������� ������ ���� � ���� 2007 �. ��� ���� ������� � ������������ ����� ������ �����������.</P>
<P align=justify>��� ��� ��������������, ����������� �������������� �������������� ������� �� ������ �� ���������� ��� �����, �� � �� ������ �� ��� ����������, ���� ����� �������������� <I>����������������� ��������������� ���� �������������� </I>������ ���� �����<I>.</P></I>
<P align=justify>��-������ &#8211; ��� ��������� ��������� �������� ����������� ������� ����������������� ������ � ����� � ��������� ������ (��������). � ��������� ����� ��������� �������� �������� ����������������� ������ ��� ���������� �������� ������� ����������� ������� (��� �������� ������ ���������� ������ � ��������� ������� �� �������� � �����). ��� ������� � ���, ��� ��� �������� ��������� � ������������ ��������� ������, ������������� � ������������ ������� �����, ����������������� ������ � ����� ���������� ������ ������� ��������� �������� ��������� ����. � ���� ������� ��� �������� ������ � ���������� ������ �������� � ����������� ��������� ������ ���������� ����� ������� �� ��� ������������� ���������� � ��������������� ����������� �������� ������, ��������� ���������� ����� � �������, �������� �������� ������ ������������ ������� ������������, � ��. � ����������� ������� ������� � ���������� ��������� ������ ��������� �� ��, ��� ��������� ���� ��������� ������������� ��� ����������� ����������� � �� ������������� ����� ����������. ���������� ������ ���������� ������ ��������� ����������� ��������� ���������� (��������, �� �������������� ����������� ����������� Bankers Almanac) ��� ��������� ����������� �������� � �������������� ��������� ������.</P>
<P align=justify>��-������ &#8211; ��� ��������� ������ ���������� � ����� ��������� ��������� �������� ����������������, �������� � �������� ������ �� ���������� �����. �������� �� ��������������, ����������� �� ������� ��������� �����������, � ���������������� ��� ����������� ��������� �������� ������ ���������.</P>
<P align=justify>��� �������� ��������, ��� �������� ���������������� ���������� ������ � ����� ��������� ������������ ���������. ��� ����������� ����������� ������� �������� ��������� �������� �����������������, ������� �������������� ����������, � ����� ���������� � ����������� ����������. � ��������� ����� � ����� ����� ����������������� ������ ������� ���������� ����� (��������������, ���, �������, �������������, �����������). ������� ������������ ������ ����������� ���� ������� ����������������� � ������������ ������� ����� �� ���������� �������. ���, ������� ������� �� �������� ������� � ��������, �� ��������������� ���������� ������� � �������� ������������ ���������. �������������� ��� ������� �������� ������ ������������ ������������ ������ � ����������� � ��� ����������� ��������� �� ������������� ����� ���������� �����.</P>
<P align=justify>����������������� ����������� ������� ���������� � ����������� ������ � ��� �������� �������� �� ���������� ����� (������ ����������������� �� ���������� ����� �� ����� 3 ���, �������� ������������������� �������� � ������ ������� ��������� ����� �� ����� 20 ���� ���. ��� � ��.). ��� ����������, �� ���������� ��������� �������, �������� ���������� ������ �� ���������� ����� ����� ������ �������������, ���������� �������� � ����������� ���������� ������ �� ����� � �������� �������� ����������� �������. �� ���������� ������ ������ ��� � 2006 �. ������� ���������� �� �������� ������� � �. ������ (��� &#8211; ������ ������ ����������� ����� � ����� � 1917 �.).</P>
<P align=justify>� ������ � 1993 �. ��������������� � �������� ����������� ������������ ���� &#8220;���� ����� (����)&#8221;, �. ������ &#8211; �������� ���� &#8220;Bank of China&#8221;, ������� ����������� �������� �� ������������� ���������� �������� �� ���������� ������. � 2006&nbsp;�. ���� ������� ������� �� �������� ��������� ����� �������-������������� ����� ����� � �. ������. � 2007 �. ������� ����������������� ���������-���������� ����� ����� � �. �����-����������.</P>
<P align=justify>����� �������, � ��������� ����� �������� ���������������� � �������� ����������� �������������� �������� ������� �� ���������� ���� ����� � �������� � ���������� ������� ����� (�. ����� � �. ������) � ������ (�. ������ � �. �����-���������). �� ������ ������, ���������� �������������� �������������� ����� ����� �������������� �� ���������� ��������� ����������� � ������������ ������� ������ ������������ ������. �������� ������� ����� ������� �������� ���������� ������ � ������������ ������� �����, ��� ������ �� ������ ������������ �� ����������� ���� �������� �� �������������� � ����������� ����� ���������� ������� ���������� ���������.</P>
<P align=justify>�, �������, ����������� �������� �������������� �������������� ������� �� ������� ����������� <I>������� ��� ������ ����������� ����� ������ ������ � �������� ������ �����</I>, � ��� ����� �<I> </I>�� ������������ ������ ����� ���������������� ������������ ����� ������ � ����������� ��������� ����� �����. ���������� ���������� � ���������� ������ �� ��������� � �������� ���������������� �����, ���������������� � ������� ����������� ������
������� � �������, � ����� � ����� ������ �� ������������� ����� �� �������������� ���������� � ����� ����� ���������� �������� �� ���������� (� ��� ����� � �� ������������ ������) ���������� ����������� ������� ����������� �������� �� �� ������.</P>
<P align=justify>��������������� � ������������ ���������� ������� ���������� ������� ����� �������������� ��������� ������������� ������������ ���������� ���������� � ����������� �������� ���������-���������� �������������� ��������������. � ���� �����, ������ �� ������������ ������ �������� �������������� �������������� � ������ ����������, ���������, �� ������ ������, ����������� ������ �� ������ ������������ ������� ������ � �����.</P>
<P align=justify>����������� �������� ���������-���������� �������������� �������������� � ������������ �������, ��� ��� ��������������, �������:</P>
<P align=justify>&#183; &nbsp;� ����������� ����� �������� ���������� �� ������� � ������� �� ������������� �������;</P>
<P align=justify>&#183; &nbsp;� �������������� ��������� ���� � ��������� � ������������ ������� ����� ����� ��� ���������� �������� � ��������;</P>
<P align=justify>&#183; &nbsp;� ����������� ����������� ������������ ������ ����� ����� � ������������ �������;</P>
<P align=justify>&#183; &nbsp;� ��������� �������� � ���� �������� ��� ���������� �������� �������, ������������ ���������� ����������� � �������� ������ ��������;</P>
<P align=justify>&#183; &nbsp;� ������������ ������� ������ �� ���������� ������ ������������ ��������������� ���������� ����� ������ � ������������ ��������� ��������� ����� ����� � �������� ������ (��������) ����� ����� � ����� ��������� �������������� ����������� �� �������� �������������� �������������� ��� ����������� �� ���������� �� ���������� �����������.</P>
<P align=justify>�, �������, ���������� ��������� ������������� ������������� �������� � ���������� ������ �� ���� ��������� ������� ������ �� ��������, �� ������ ������, ������� �� ��� ����� ����� � �������� ����������� ����� � ������ ����������������. ��� ��������� ��������� ���������� �������� �� ������ ������� ��� ������������� � ������ ����������� ������� ����� ������ �� ����������� ������ �������������� �� ���������� ��������� � ����������� ��������-��������� ��������������.</P>
<P align=justify>������ � ��� ����������� ���������� �������� ������ ��� ��������� ������������� ������������ ���������� ���������� ������ ���� ���������� ���������� �� ������� ����������������� ��������, ��������������� ������� �����, �������� ��������������� � ����������� ����, � ��������� ����� �����.</P><SUP>
<P><STRONG>1</STRONG> </SUP>������������ ��������� ��� http://www.crc.mofcom.Gov.com.</P><SUP>
<P align=justify><STRONG>2 </STRONG></SUP>���������� ���������� ���������� ��������� � ��������� �������� ���������� // �������� �������� �������. &#8211;2006. &#8211; &#8470; 3. &#8211; �. 7&#8211;13.</P><SUP>
<P><STRONG>3</STRONG> </SUP>��� ��.</P><SUP>
<P><STRONG>4 </STRONG></SUP>������� ����� ������. &#8211; 2007. &#8211; &#8470; 26&#8211;27 (970&#8211;971). &#8211; �. 14.</P><SUP>
<P><STRONG>5</STRONG> </SUP>http://www.vtb.ru.</P><SUP>
<P><STRONG>6</STRONG> </SUP>http://www.veb.ru.</P><SUP>
<P><STRONG>7 </STRONG></SUP>��������������� ���������� ���������� http://www.dvtu.vladivostok.ru/dinamica2005/2006/xls.</P><SUP>
<P><STRONG>8</STRONG> </SUP>������� ����� ������. &#8211; 2007. &#8211; &#8470; 26&#8211;27 (970&#8211;971).</P><SUP>
<P><STRONG>9</STRONG> </SUP>��������������� ���������� ���������� http://www.dvtu.vladivostok.ru/dinamica2005/2006/xls.</P>


</body>
</html>

