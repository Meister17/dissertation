<!--
ID:36286104
TITLE:����������� ���������� ��������� ����������� ����������� ������������ ���� �������� � ������������ ���������������� ������������ ���
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:8
ISSUE_DATE:2006-04-30
RECORD_DATE:2006-06-01


-->
<html>
<head>
<title>
36286104-����������� ���������� ��������� ����������� ����������� ������������ ���� �������� � ������������ ���������������� ������������ ���
</title>
</head>
<body >


<B>
<P align=center>������<BR>�� 1 ������ 2006 �.<BR>��������� �����������<BR>����������� ������������ ����<BR>�������� � ������������ ����������������<BR>������������ ���</P><I>
<P>��� ���������� �� ����� 45286560000<BR>��� ��������� ����������� �� ���� 40019305<BR>�������� ��������������� ��������������� ����� 1027739019527<BR>��������������� ����� 2983 ���-��� 044579213<BR>�����: 115054, �.������ , 3-� �������������� ���., �.11, ���.1</P></B></I>
<P align=right>���. ���.</P>
<TABLE cellSpacing=2 width=604 align=center border=1>
<TBODY>
<TR>
<TD vAlign=center width="8%" height=51><B>
<P align=center>&#8470;<BR>�/�</B></P></TD>
<TD vAlign=center width="66%" height=51><B>
<P align=center>������������ ������</B></P></TD>
<TD vAlign=center width="13%" height=51><B>
<P align=center>������ ��<BR>�������� ����</B></P></TD>
<TD vAlign=top width="14%" height=51><B>
<P align=center>������ �� ��������������� �������� ���� �������� ����</B></P></TD></TR>
<TR>
<TD vAlign=bottom width="8%" height=10><B>
<P align=center>1</B></P></TD>
<TD vAlign=bottom width="66%" height=10><B>
<P align=center>2</B></P></TD>
<TD vAlign=bottom width="13%" height=10><B>
<P align=center>3</B></P></TD>
<TD vAlign=top width="14%" height=10><B>
<P align=center>4</B></P></TD></TR>
<TR>
<TD vAlign=center width="8%" height=17><B>
<P align=center>I</B></P></TD>
<TD vAlign=center width="66%" height=17><B>
<P align=center>������</B></P></TD>
<TD vAlign=center width="13%" height=17>
<P align=right></P></TD>
<TD vAlign=top width="14%" height=17>
<P></P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>1</P></TD>
<TD vAlign=top width="66%">
<P>�������� ��������</P></TD>
<TD vAlign=top width="13%">
<P align=right>48833</P></TD>
<TD vAlign=top width="14%">
<P align=right>3301</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>2</P></TD>
<TD vAlign=top width="66%">
<P>�������� ��������� ����������� � ����������� ����� ���������� ���������</P></TD>
<TD vAlign=top width="13%">
<P align=right>298933</P></TD>
<TD vAlign=top width="14%">
<P align=right>356387</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>2.1</P></TD>
<TD vAlign=top width="66%">
<P>������������ �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>83314</P></TD>
<TD vAlign=top width="14%">
<P align=right>75406</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=26>
<P align=center>3</P></TD>
<TD vAlign=top width="66%" height=26>
<P>�������� � ��������� ������������</P></TD>
<TD vAlign=top width="13%" height=26>
<P align=right>7090</P></TD>
<TD vAlign=top width="14%" height=26>
<P align=right>1123</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=26>
<P align=center>4</P></TD>
<TD vAlign=top width="66%" height=26>
<P>������ �������� � �������� ������ ������</P></TD>
<TD vAlign=top width="13%" height=26>
<P align=right>0</P></TD>
<TD vAlign=top width="14%" height=26>
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>5</P></TD>
<TD vAlign=top width="66%">
<P>������ ������� �������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>5565583</P></TD>
<TD vAlign=top width="14%">
<P align=right>3422311</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=22>
<P align=center>6</P></TD>
<TD vAlign=top width="66%" height=22>
<P>������ �������� � �������������� ������ ������, ������������ �� ���������</P></TD>
<TD vAlign=top width="13%" height=22>
<P align=right>0</P></TD>
<TD vAlign=top width="14%" height=22>
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>7</P></TD>
<TD vAlign=top width="66%">
<P>������ �������� � ������ ������, ��������� � ������� ��� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="14%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=21>
<P align=center>8</P></TD>
<TD vAlign=top width="66%" height=21>
<P>�������� ��������, �������������� ������ � ������������ ������</P></TD>
<TD vAlign=top width="13%" height=21>
<P align=right>908753</P></TD>
<TD vAlign=top width="14%" height=21>
<P align=right>15726</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=27>
<P align=center>9</P></TD>
<TD vAlign=top width="66%" height=27>
<P>���������� �� ��������� ���������</P></TD>
<TD vAlign=top width="13%" height=27>
<P align=right>262</P></TD>
<TD vAlign=top width="14%" height=27>
<P align=right>191</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=28>
<P align=center>10</P></TD>
<TD vAlign=top width="66%" height=28>
<P>������ ������</P></TD>
<TD vAlign=top width="13%" height=28>
<P align=right>14751</P></TD>
<TD vAlign=top width="14%" height=28>
<P align=right>3799</P></TD></TR>
<TR>
<TD vAlign=top width="8%"><B>
<P align=center>11</B></P></TD>
<TD vAlign=top width="66%"><B>
<P>����� �������</B></P></TD>
<TD vAlign=top width="13%"><B>
<P align=right>6844205</B></P></TD>
<TD vAlign=top width="14%"><B>
<P align=right>3802838</B></P></TD></TR>
<TR>
<TD vAlign=top width="8%"><B>
<P align=center>II</B></P></TD>
<TD vAlign=top width="66%"><B>
<P align=center>�������</B></P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="14%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>12</P></TD>
<TD vAlign=top width="66%">
<P>������� ������������ ����� ���������� ���������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="14%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=30>
<P align=center>13</P></TD>
<TD vAlign=top width="66%" height=30>
<P>�������� ��������� �����������</P></TD>
<TD vAlign=top width="13%" height=30>
<P align=right>684336</P></TD>
<TD vAlign=top width="14%" height=30>
<P align=right>90000</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>14</P></TD>
<TD vAlign=top width="66%">
<P>�������� �������� (����������� �����������)</P></TD>
<TD vAlign=top width="13%">
<P align=right>744943</P></TD>
<TD vAlign=top width="14%">
<P align=right>2057718</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>14.1</P></TD>
<TD vAlign=top width="66%">
<P>������ ���������� ���</P></TD>
<TD vAlign=top width="13%">
<P align=right>54836</P></TD>
<TD vAlign=top width="14%">
<P align=right>9615</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>15</P></TD>
<TD vAlign=top width="66%">
<P>���������� �������� �������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>1641997</P></TD>
<TD vAlign=top width="14%">
<P align=right>717226</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>16</P></TD>
<TD vAlign=top width="66%">
<P>������������� �� ������ ���������</P></TD>
<TD vAlign=top width="13%">
<P align=right>2585</P></TD>
<TD vAlign=top width="14%">
<P align=right>818</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>17</P></TD>
<TD vAlign=top width="66%">
<P>������ �������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>12774</P></TD>
<TD vAlign=top width="14%">
<P align=right>6401</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>18</P></TD>
<TD vAlign=top width="66%">
<P>������� �� ��������� ������ �� �������� �������������� ���������� ���������, ������ ��������� ������� � �� ��������� � ����������� �������� ���</P></TD>
<TD vAlign=top width="13%">
<P align=right>7149</P>
<P align=right></P></TD>
<TD vAlign=top width="14%">
<P align=right>43</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=26>
<P align=center>19</P></TD>
<TD vAlign=top width="66%" height=26>
<P>����� ������������</P></TD>
<TD vAlign=top width="13%" height=26>
<P align=right>3093784</P></TD>
<TD vAlign=top width="14%" height=26>
<P align=right>2872206</P></TD></TR></TBODY></TABLE><I>
<P align=right>�����������</P></I>
<TABLE cellSpacing=2 cellPadding=7 width=611 align=center border=1>
<TBODY>
<TR>
<TD vAlign=bottom width="8%" height=13><B>
<P align=center>1</B></P></TD>
<TD vAlign=bottom width="66%" height=13><B>
<P align=center>2</B></P></TD>
<TD vAlign=bottom width="13%" height=13><B>
<P align=center>3</B></P></TD>
<TD vAlign=top width="13%" height=13><B>
<P align=center>4</B></P></TD></TR>
<TR>
<TD vAlign=center width="8%" height=13><B>
<P align=center>III</B></P></TD>
<TD vAlign=center width="66%" height=13><B>
<P align=center>��������� ����������� �������</B></P></TD>
<TD vAlign=center width="13%" height=13>
<P></P></TD>
<TD vAlign=top width="13%" height=13>
<P></P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>20</P></TD>
<TD vAlign=top width="66%">
<P>�������� ���������� (����������)</P></TD>
<TD vAlign=top width="13%">
<P align=right>3495141</P></TD>
<TD vAlign=top width="13%">
<P align=right>895588</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=25>
<P align=center>20.1</P></TD>
<TD vAlign=top width="66%" height=25>
<P>������������������ ������������ ����� � ����</P></TD>
<TD vAlign=top width="13%" height=25>
<P align=right>895588</P></TD>
<TD vAlign=top width="13%" height=25>
<P align=right>895588</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=27>
<P align=center>20.2</P></TD>
<TD vAlign=top width="66%" height=27>
<P>������������������ ����������������� �����</P></TD>
<TD vAlign=top width="13%" height=27>
<P align=right>0</P></TD>
<TD vAlign=top width="13%" height=27>
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=28>
<P align=center>20.3</P></TD>
<TD vAlign=top width="66%" height=28>
<P>�������������������� �������� ������� ������������� ��������� �����������</P></TD>
<TD vAlign=top width="13%" height=28>
<P align=right>2599553</P></TD>
<TD vAlign=top width="13%" height=28>
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>21</P></TD>
<TD vAlign=top width="66%">
<P>����������� �����, ����������� � ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=21>
<P align=center>22</P></TD>
<TD vAlign=top width="66%" height=21>
<P>����������� �����</P></TD>
<TD vAlign=top width="13%" height=21>
<P align=right>178822</P></TD>
<TD vAlign=top width="13%" height=21>
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=28>
<P align=center>23</P></TD>
<TD vAlign=top width="66%" height=28>
<P>���������� �������� �������</P></TD>
<TD vAlign=top width="13%" height=28>
<P align=right>67</P></TD>
<TD vAlign=top width="13%" height=28>
<P align=right>67</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>24</P></TD>
<TD vAlign=top width="66%">
<P>������� ������� �������� � ����������� �������, �������� �� ����������� �������� (�������)</P></TD>
<TD vAlign=top width="13%">
<P align=right>30344</P></TD>
<TD vAlign=top width="13%">
<P align=right>4398</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>25</P></TD>
<TD vAlign=top width="66%">
<P>����� � ���������������� ������� ������� ��� � ������������ ��������� ����������� (������������ ������ ������� ���)</P></TD>
<TD vAlign=top width="13%">
<P align=right>39374</P></TD>
<TD vAlign=top width="13%">
<P align=right>5855</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>26</P></TD>
<TD vAlign=top width="66%">
<P>������� (������) �� �������� ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>67361</P></TD>
<TD vAlign=top width="13%">
<P align=right>33520</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>27</P></TD>
<TD vAlign=top width="66%">
<P>����� ���������� ����������� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>1150868</P></TD>
<TD vAlign=top width="13%">
<P align=right>930632</P></TD></TR>
<TR>
<TD vAlign=top width="8%"><B>
<P align=center>28</B></P></TD>
<TD vAlign=top width="66%"><B>
<P>����� ��������</B></P></TD>
<TD vAlign=top width="13%"><B>
<P align=right>6844205</B></P></TD>
<TD vAlign=top width="13%"><B>
<P align=right>3802838</B></P></TD></TR>
<TR>
<TD vAlign=top width="8%"><B>
<P align=center>IV</B></P></TD>
<TD vAlign=top width="66%"><B>
<P align=center>������������� �������������</B></P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>29</P></TD>
<TD vAlign=top width="66%">
<P>����������� ������������� ��������� �����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>199589</P></TD>
<TD vAlign=top width="13%">
<P align=right>4200</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=13>
<P align=center>30</P></TD>
<TD vAlign=top width="66%" height=13>
<P>��������, �������� ��������� ������������</P></TD>
<TD vAlign=top width="13%" height=13>
<P align=right>212387</P></TD>
<TD vAlign=top width="13%" height=13>
<P align=right>30</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=17><B>
<P align=center>V</B></P></TD>
<TD vAlign=top width="66%" height=17><B>
<P align=center>����� �������������� ����������</B></P></TD>
<TD vAlign=top width="13%" height=17>
<P></P></TD>
<TD vAlign=top width="13%" height=17>
<P></P></TD></TR>
<TR>
<TD vAlign=top width="8%">&nbsp;</TD>
<TD vAlign=top width="66%">
<P>�������� �����</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="8%" height=18>
<P align=center>1</P></TD>
<TD vAlign=top width="66%" height=18>
<P>�����</P></TD>
<TD vAlign=top width="13%" height=18>
<P align=right>0</P></TD>
<TD vAlign=top width="13%" height=18>
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=16>
<P align=center>2</P></TD>
<TD vAlign=top width="66%" height=16>
<P>������ ������ � ����������</P></TD>
<TD vAlign=top width="13%" height=16>
<P align=right>0</P></TD>
<TD vAlign=top width="13%" height=16>
<P align=right>277490</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>3</P></TD>
<TD vAlign=top width="66%">
<P>����������� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>4</P></TD>
<TD vAlign=top width="66%">
<P>������� ���������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>5</P></TD>
<TD vAlign=top width="66%">
<P>��������, �������������� �� ������ ����</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>6</P></TD>
<TD vAlign=top width="66%">
<P>������� �� �������������� ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>277487</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>7</P></TD>
<TD vAlign=top width="66%">
<P>���������� ����������� ���������� (��������) ����� �� ���������� (��������) �������� ��������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=15>
<P align=center>8</P></TD>
<TD vAlign=top width="66%" height=15>
<P>������� �����</P></TD>
<TD vAlign=top width="13%" height=15>
<P align=right>0</P></TD>
<TD vAlign=top width="13%" height=15>
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>9</P></TD>
<TD vAlign=top width="66%">
<P>������� �� �������������� ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="8%">
<P align=center>10</P></TD>
<TD vAlign=top width="66%">
<P>������ �� �������������� ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=9>
<P></P></TD>
<TD vAlign=top width="66%" height=9>
<P>��������� �����</P></TD>
<TD vAlign=top width="13%" height=9>
<P></P></TD>
<TD vAlign=top width="13%" height=9>
<P></P></TD></TR>

<TR>
<TD vAlign=top width="8%" height=12>
<P align=center>11</P></TD>
<TD vAlign=top width="66%" height=12>
<P>������� � ����������</P></TD>
<TD vAlign=top width="13%" height=12>
<P align=right>0</P></TD>
<TD vAlign=top width="13%" height=12>
<P align=right>277487</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=15>
<P align=center>12</P></TD>
<TD vAlign=top width="66%" height=15>
<P>������� �� �������������� ����������</P></TD>
<TD vAlign=top width="13%" height=15>
<P align=right>0</P></TD>
<TD vAlign=top width="13%" height=15>
<P align=right>277490</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=21>
<P align=center>13</P></TD>
<TD vAlign=top width="66%" height=21>
<P>���������� ����������� ���������� (��������) ����� �� ���������� (��������) �������� ��������������</P></TD>
<TD vAlign=top width="13%" height=21>
<P align=right>0</P></TD>
<TD vAlign=top width="13%" height=21>
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=21>
<P align=center>14</P></TD>
<TD vAlign=top width="66%" height=21>
<P>������ �� �������������� ����������</P></TD>
<TD vAlign=top width="13%" height=21>
<P align=right>0</P></TD>
<TD vAlign=top width="13%" height=21>
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="8%" height=21>
<P align=center>15</P></TD>
<TD vAlign=top width="66%" height=21>
<P>������� �� �������������� ����������</P></TD>
<TD vAlign=top width="13%" height=21>
<P align=right>0</P></TD>
<TD vAlign=top width="13%" height=21>
<P align=right>0</P></TD></TR></TBODY></TABLE><I>
<P align=justify>������������</I><B>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; �.�. ����������</P></B><I>
<P align=justify>������� ���������</I>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <B>�.�. �������</P>
<P align=center>����� � �������� � �������<BR>�� 2005 �.<BR>��������� �����������<BR>����������� ������������ ����<BR>�������� � ������������ ����������������<BR>������������ ���</P><I>
<P>��� ���������� �� ����� 45286560000<BR>��� ��������� ����������� �� ���� 40019305<BR>�������� ��������������� ��������������� ����� 1027739019527<BR>��������������� ����� 2983 ���-��� 044579213<BR>�����: 115054, �.������ , 3-� �������������� ���., �.11, ���.1</P></B></I>
<P align=right>���. ���.</P>
<TABLE cellSpacing=2 width=604 align=center border=1>
<TBODY>
<TR>
<TD vAlign=center width="6%" height=46><B>
<P align=center>&#8470;<BR>�/�</B></P></TD>
<TD vAlign=center width="69%" height=46><B>
<P align=center>������������ ������</B></P></TD>
<TD vAlign=center width="13%" height=46><B>
<P align=center>������ �� �������� ������</B></P></TD>
<TD vAlign=top width="13%" height=46><B>
<P align=center>������ �� ��������������� ������ �������� ����</B></P></TD></TR>
<TR>
<TD vAlign=bottom width="6%" height=7><B>
<P align=center>1</B></P></TD>
<TD vAlign=bottom width="69%" height=7><B>
<P align=center>2</B></P></TD>
<TD vAlign=bottom width="13%" height=7><B>
<P align=center>3</B></P></TD>
<TD vAlign=top width="13%" height=7><B>
<P align=center>4</B></P></TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="69%"><B>
<P align=center>�������� ���������� � ����������� ������ ��</B>:</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>1</P></TD>
<TD vAlign=top width="69%">
<P>���������� ������� � ��������� ������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>3175</P></TD>
<TD vAlign=top width="13%">
<P align=right>579</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>2</P></TD>
<TD vAlign=top width="69%">
<P>����, ��������������� �������� (����������� ������������)</P></TD>
<TD vAlign=top width="13%">
<P align=right>641439</P></TD>
<TD vAlign=top width="13%">
<P align=right>201921</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>3</P></TD>
<TD vAlign=top width="69%">
<P>�������� ����� �� ���������� ������ (�������)</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>4</P></TD>
<TD vAlign=top width="69%">
<P>������ ����� � ������������� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>5</P></TD>
<TD vAlign=top width="69%">
<P>������ ����������</P></TD>
<TD vAlign=top width="13%">
<P align=right>1265</P></TD>
<TD vAlign=top width="13%">
<P align=right>391</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>6</P></TD>
<TD vAlign=top width="69%">
<P>����� ��������� ���������� � ����������� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>645879</P></TD>
<TD vAlign=top width="13%">
<P align=right>202891</P></TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="69%">
<P>�������� ���������� � ����������� ������� ��:</P></TD>
<TD vAlign=top width="13%">&nbsp;</TD>
<TD vAlign=top width="13%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%" height=15>
<P align=center>7</P></TD>
<TD vAlign=top width="69%" height=15>
<P>������������ ��������� ��������� �����������</P></TD>
<TD vAlign=top width="13%" height=15>
<P align=right>35704</P></TD>
<TD vAlign=top width="13%" height=15>
<P align=right>8675</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>8</P></TD>
<TD vAlign=top width="69%">
<P>������������ ��������� �������� (����������� �����������)</P></TD>
<TD vAlign=top width="13%">
<P align=right>75742</P></TD>
<TD vAlign=top width="13%">
<P align=right>10297</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>9</P></TD>
<TD vAlign=top width="69%">
<P>���������� �������� ��������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>33697</P></TD>
<TD vAlign=top width="13%">
<P align=right>11107</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>10</P></TD>
<TD vAlign=top width="69%">
<P>����� ��������� ���������� � ����������� ��������</P></TD>
<TD vAlign=top width="13%">
<P align=right>145143</P></TD>
<TD vAlign=top width="13%">
<P align=right>30079</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>11</P></TD>
<TD vAlign=top width="69%">
<P>������ ���������� � ����������� ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>500736</P></TD>
<TD vAlign=top width="13%">
<P align=right>172812</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>12</P></TD>
<TD vAlign=top width="69%">
<P>������ ������ �� �������� � ������� ��������</P></TD>
<TD vAlign=top width="13%">
<P align=right>9386</P></TD>
<TD vAlign=top width="13%">
<P align=right>22722</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>13</P></TD>
<TD vAlign=top width="69%">
<P>������ ������ �� �������� � ����������� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>2498</P></TD>
<TD vAlign=top width="13%">
<P align=right>1283</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>14</P></TD>
<TD vAlign=top width="69%">
<P>������ ������ �� �������� � ������������ ��������� � ������� ����������� �������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD>
<TD vAlign=top width="13%">
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>15</P></TD>
<TD vAlign=top width="69%">
<P>������ ������ �� ���������� ����������� ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>875</P></TD>
<TD vAlign=top width="13%">
<P align=right>48</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>16</P></TD>
<TD vAlign=top width="69%">
<P>������������ ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>4382</P></TD>
<TD vAlign=top width="13%">
<P align=right>6458</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>17</P></TD>
<TD vAlign=top width="69%">
<P>������������ �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>1514</P></TD>
<TD vAlign=top width="13%">
<P align=right>37</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>18</P></TD>
<TD vAlign=top width="69%">
<P>������ ������ �� ������� ��������</P></TD>
<TD vAlign=top width="13%">
<P align=right>&#8211;434</P></TD>
<TD vAlign=top width="13%">
<P align=right>&#8211;44</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>19</P></TD>
<TD vAlign=top width="69%">
<P>������ ������ ������������ ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>&#8211;3435</P></TD>
<TD vAlign=top width="13%">
<P align=right>&#8211;2762</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>20</P></TD>
<TD vAlign=top width="69%">
<P>���������������-�������������� �������</P></TD>
<TD vAlign=top width="13%">
<P align=right>196615</P></TD>
<TD vAlign=top width="13%">
<P align=right>55536</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>21</P></TD>
<TD vAlign=top width="69%">
<P>������� �� ��������� ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>&#8211;215978</P></TD>
<TD vAlign=top width="13%">
<P align=right>&#8211;98861</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>22</P></TD>
<TD vAlign=top width="69%">
<P>������� �� ���������������</P></TD>
<TD vAlign=top width="13%">
<P align=right>99901</P></TD>
<TD vAlign=top width="13%">
<P align=right>46083</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>23</P></TD>
<TD vAlign=top width="69%">
<P>����������� ������ (������� ����� �� �������)</P></TD>
<TD vAlign=top width="13%">
<P align=right>32540</P></TD>
<TD vAlign=top width="13%">
<P align=right>12563</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>24</P></TD>
<TD vAlign=top width="69%">
<P>������� (������) �� �������� ������</P></TD>
<TD vAlign=top width="13%">
<P align=right>67361</P></TD>
<TD vAlign=top width="13%">
<P align=right>33520</P></TD></TR></TBODY></TABLE><B>
<P align=center>����� �� ������ ������������� ��������,<BR>�������� �������� �� ��������<BR>������������ ���� � ���� �������<BR>�� 1 ������ 2006 �.<BR>��������� �����������<BR>����������� ������������ ����<BR>�������� � ������������ ����������������<BR>������������ ���</P><I>
<P>��� ���������� �� ����� 45286560000<BR>��� ��������� ����������� �� ���� 40019305<BR>�������� ��������������� ��������������� ����� 1027739019527<BR>��������������� ����� 2983 ���-��� 044579213<BR>�����: 115054, �.������ , 3-� �������������� ���., �.11, ���.1</P></B></I>
<TABLE cellSpacing=2 width=604 align=center border=1>
<TBODY>
<TR>
<TD vAlign=center width="6%" height=45><B>
<P align=center>&#8470;<BR>�/�</B></P></TD>
<TD vAlign=center width="70%" height=45><B>
<P align=center>������������ ����������</B></P></TD>
<TD vAlign=center width="12%" height=45><B>
<P align=center>������ ��<BR>��������<BR>����</B></P></TD>
<TD vAlign=top width="12%" height=45><B>
<P align=center>������ �� ��������������� �������� ���� �������� ����</B></P></TD></TR>
<TR>
<TD vAlign=bottom width="6%" height=17><B>
<P align=center>1</B></P></TD>
<TD vAlign=bottom width="70%" height=17><B>
<P align=center>2</B></P></TD>
<TD vAlign=bottom width="12%" height=17><B>
<P align=center>3</B></P></TD>
<TD vAlign=top width="12%" height=17><B>
<P align=center>4</B></P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>1</P></TD>
<TD vAlign=top width="70%">
<P>����������� �������� (�������), ���. ���.</P></TD>
<TD vAlign=top width="12%">
<P align=right>995001</P></TD>
<TD vAlign=top width="12%">
<P align=right>933909</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>2</P></TD>
<TD vAlign=top width="70%">
<P>����������� �������� ������������� ����������� ������� (��������), �������</P></TD>
<TD vAlign=top width="12%">
<P align=right>14,7</P></TD>
<TD vAlign=top width="12%">
<P align=right>27,0</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>3</P></TD>
<TD vAlign=top width="70%">
<P>����������� �������� ������������� ����������� ������� (��������), �������</P></TD>
<TD vAlign=top width="12%">
<P align=right>10</P></TD>
<TD vAlign=top width="12%">
<P align=right>10</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>4</P></TD>
<TD vAlign=top width="70%">
<P>��������� ������ �� ��������� ������ �� ������, ������� � ������������ � ��� �������������, ���. ���.</P></TD>
<TD vAlign=top width="12%">
<P align=right>312469</P></TD>
<TD vAlign=top width="12%">
<P align=right>103744</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>5</P></TD>
<TD vAlign=top width="70%">
<P>���������� �������������� ������ �� ��������� ������ �� ������, ������� � ������������ � ��� �������������, ���. ���.</P></TD>
<TD vAlign=top width="12%">
<P align=right>312469</P></TD>
<TD vAlign=top width="12%">
<P align=right>103744</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>6</P></TD>
<TD vAlign=top width="70%">
<P>��������� ������ �� ��������� ������, ���. ���.</P></TD>
<TD vAlign=top width="12%">
<P align=right>7296</P></TD>
<TD vAlign=top width="12%">
<P align=right>43</P></TD></TR>
<TR>
<TD vAlign=top width="6%">
<P align=center>7</P></TD>
<TD vAlign=top width="70%">
<P>���������� �������������� ������ �� ��������� ������, ���. ���.</P></TD>
<TD vAlign=top width="12%">
<P align=right>7296</P></TD>
<TD vAlign=top width="12%">
<P align=right>43</P></TD></TR></TBODY></TABLE><I>
<P align=justify>������������</I><B>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;�.�. ����������</P></B><I>
<P align=justify>������� ���������</I>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp
;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B>�.�. �������</P></B>
<P align=justify><STRONG><EM>�� ������ ������, ���������� (�������������) ���������� ������������� ��� �������� ���������� �� ���� ������������ ���������� ���������� ��������� �� 1 ������ 2006 �. � ���������� ���������-������������� ������������ �� ������ � 1 ������ �� 31 ������� 2005 �. ������������, � ������������ � ������������ ���������������� ���������� ���������, � ����� ���������� ���������� (�������������) ����������, ������������ ����������� ����� ������ � �������� ������� ���������. </EM></STRONG></P><B><I>
<P align=justify>������������ ����������� ����������� &#8211; ��� "���������� � ������������� ������������", �������� &#8470; � 000001, ���� ������ �������� &#8211; 10 ������ 2002 �., ���� �������� �������� &#8211; 5 ���, ������������ ������, ��������� ��������, &#8211; ������������ �������� ���������� ���������, ������������ &#8211; ��������� ������ ����������. ������������ ����������� ��������, ������� ��� "���������� � ������������� ������������" ����� ������ �����������, ������������ &#8470; 1 �� 24.10.2005 �. ������������ ����������� � ���������������� ����� ���������� ���������� ��� "���", ���������������� �������� &#8470; � 012130, ���� �������� �� ���������.</P></B></I>


</body>
</html>

