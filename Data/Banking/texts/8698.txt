<!--
ID:37140162
TITLE:�������-2007
SOURCE_ID:11517
SOURCE_NAME:���
AUTHOR:
NUMBER:1
ISSUE_DATE:2007-01-31
RECORD_DATE:2007-01-12


-->
<html>
<head>
<title>
37140162-�������-2007
</title>
</head>
<body >


<P align=justify>Honda ����������� Civic Type-R.</P>
<P align=justify>������ 2007 ���� �������� ������� Honda Motor ��������� � ��������� ������������ �������� ��������� ������������ ������� Civic Type-R. �������� ���������� �������� ��� ���������������, ������ �� ����� ����������� �������� ��������� ����������. 2-�������� ������������ DOHC i-VTEC ������ ��� �� �� 201 �.�., ������ ������������������� ������� ������� ������� ��� ����� �������������� � ��������� ��������. ��� ���������, �� ���������� � ������������ �������������� ����. �� ����� Type-R "������������" �� 6,6 ���., � ������������ �������� ��� ���������� 235 ��/�. ��� ���� 6-����������� "��������" � ���� ����� �������� ������������� ������� ������������ �������� � ����. ���������� ��� �������� ��������� Type-R ������ ����������� ����������� ������������� ����� ��������� 18 ������. � ������ ���� ��������� ������ � ������ � ����� ���� ������ ����������.</P>
<P align=justify>Zoom-zoom �� Mazda.</P>
<P align=justify>�� ��������� �����-��� ������ 2006 ���� �������� Mazda Motor ����������� ����������� ������ ������ ���������� Mazda CX-7, ������� �������� 6-����������� ������������ ������� ������������ ������� � ��� ��������� �� ������� ����. ���� ���������� ������������� ������� ������� ���������������� ������ ����� � 18-�������� �������� �����. ������������ ��-7 ������������ ������������� ���������� ������� MZR 2.3L DISI, �� �������� ������ 258 "�������". � 2007 ���� ������� ����� ������� �������������� �� ���������� � ������ ��� ��� ���� ����� ������� Mazda MX-5 �������� ���������. � 2000 ���� �� ������� � ����� �������� �������� ��� �������� ����������� ����������, ������� ����� ������� ������������ �����. ��� ������� �� Mazda �������� � ������ ����� � �������� 2007 ����, � �� �� ����� � ������������� ����� ��������� �������������.</P>
<P align=justify>"�������" ��� �����.</P>
<P align=justify>����� ����� �������� �� ��������� �������� BMW ������ ������ ��������� ������������ �5 (� ��� ������ "���" ����������� � N12), ����� ������ �������� ������� �� ����� �����. ������ ���� ����� �������� � �������� ����� ����-��������� �� ���� ������� �����, �������� �������� ��������� 7 ������ 2007 ���� � ������ �������� ���������� North American International Auto Show � ��������. ��������� ��������� ��������� ������������ ���� � ������� �������������� ������� ������ ����������� ����������� �� �������� ����� ������������� �� ������������ "��������" �������������. �� ����� ������� - ���� ��������� �������. ������� ����������� 335i ���������� 3-�������� ������� � ������� ������������� ��������� 306 �.�. ���� �� ����-��������� ���� �� ������������.</P>
<P align=justify>Audi �� ��-����.</P>
<P align=justify>����������� ���������� ����� ��-���� Audi R8 ��������� � ����� ��������� Audi. ����� ��� ���� ����� ������ ��������������� Audi Le Mans quattro ��� �������� ������ � ����������� ������������� ���������, ���������� �������� Audi R8, ��������� ����� �������� �� ������������� ���������� � ������ ������ 2006 ����. ���� �������� � ������ ������������ ������� � ������������� � ������ ������� ������ ��������� Audi. ������� ��� ���������� �������� �������� 420-������� ��������� V8 FSI, ������� � ������� �������� �������� ������������� ����������, ������� ����������� ������� ������� quattro � ����������� ����� Audi Space Frame. ���������� �������� ������� �������� �� ������ 2007 ����. ��������������� ���� Audi R8 - 170 ���. ��������.</P>
<P align=justify>�������� Volkswagen.</P>
<P align=justify>����� ��������� ������ �������� ���������� ������������ Volkswagen Touareg, �������� �������� ���������� �� ��������� ���������� ������, �������� � ����� ������ � ����� ������ 2007-��. ������ ����� ������ �� ��� ������ �������� ��� �� ������ ����. �� ������ ��������������� ������ ���������� ���������� �������� �������� �����, ����������� �������� ���������, ������ � ������ ������. ����� ����, ����� ��������� � ��������� ����� ����������� �������. ����������� ������ ������ ������� �������� �����, ���������� 8-����������� ���������� ���������� � ���������������� �������� ������� V8 FSI ��������� 350 �.�. � ������� ���������� ���� ������� ��� ��������� W12 - 450 "�������". ���� �� ������� �� ������ ���������� ������ ������������� �������� �����������. ������������� ����������� ��������� Touareg � ���������� ������� ����������� �� ���� �� 48,7 �� 93,9 ���. ��������.</P>
<P align=justify>����������� Mitsubishi.</P>
<P align=justify>��������������� "���������" Mitsubishi Outlander ������� � ������ � ������. �� �������� ������������ ������ Outlander ������� ����������� �����, ��������������� ����������� ������ �������� � ������������� ����������� Multi-Select 4WD, ����������� ������������� ������ ����� ������� ����������. ������� ����������� ����� �������� � ������������ ���������� - � 3-�������� V-�������� "���������" ��������� 200 �.�. � ���� � 6-����������� "���������" INVECS ������� ���������. �����, ������, � ���������� ����������� �������� ������������ Outlander � ���������� ������� ������� 2,4 � � 6-����������� ���������� � ������ ������� ���� ������������ 5-����������� ��������. ������������ ����� ���������� ������� ���������� ������������� ������� ������������ ����, � ����� �������������� ����������� 18-�������� ������������� ������. ���������� � ����� �� ������� ������������ �������� Mitsubishi � ������ "����� �������" �� ������ ���������� ������ �� �������.</P>
<P align=justify>�������� ������.</P>
<P align=justify>���� �� ����� ��������� � ����� ������ ������� - ������������ �����-���� Volvo C30. ���������� ��� ������, ����� ����� �� ����� ��� ���� �����. ����� ����, � �� ����� ������ ������� 65 ���. ����� ����������� � ���, 75% ������� �������� �� ������. ����� ���������� � ������������� ���� � ��������� ���� Volvo ��� �������� ����� ������ � ��������� ����������. ������������� ������� �������� ��� 4- � 5-������������ ����������� ����������� ������� �� 1,6 �� 2,5 � (100-220 �.�.). � ����� 2007 ���� ������ ���� ��������� ����������� � �������� ���������� ��������� ����� �������� ������� ��� ����������� iPod ��� �������� USB Flash. � ������� ������ �30 ������������� ���������������� ������� �������������� �������� IDIS, ��������� �������������� ������� �������� ������, ������� ������� ������ ����, ������� ����������� �������� � ������. ���� �� ���������� Volvo ���������� � �������.</P>
<P align=justify>����-��� �� �������.</P>
<P align=justify>�� ��������� ��� �������� � 2007 ���� � ������� ����-�����. ����������� ������� Renault ���������� ������ ��������� ������ Scenic. ������ � �������� ��������� ����������� ����� ��������, �������� ��������� ���������� � ���� ��������� ����������: 5-������� Scenic � 7-������� Grand Scenic, ����� �������� �� 23 �� �������. ����� ���-��� ����-���� - ����� �������� ������, ����� ���������� ������ (�� ������������ �����), ���������� ������������� ����� �����, � ����� ����������� ������� ������ � ���� ������������ ��������. �� ����� ����������� - ��� ���������� ������ - ������� 1,6 � 2 �, ����������� �������� � 115 � 130 �.�. ��������������. ������ �� ����� ����� �������������� �� ������ � 4-����������� "���������", �� � � 5- � 6-����������� "���������". ���� �� ����-��� �������� "����������", �������������� �������� Renault � ������, ������� ����� � ������ ������.</P>


</body>
</html>

