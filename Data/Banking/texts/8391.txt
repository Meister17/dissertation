<!--
ID:35810282
TITLE:�������� ������������� ���������� ����������
SOURCE_ID:10906
SOURCE_NAME:��������� ���� � ��������� �����������
AUTHOR:�.�. ����������, ������������ ������ &#171;���-�����&#187; 
NUMBER:5
ISSUE_DATE:2005-10-31
RECORD_DATE:2006-01-31


-->
<html>
<head>
<title>
35810282-�������� ������������� ���������� ����������
</title>
</head>
<body >


<P align=justify><B><I>��� ����������� ���������� ���������� ���������� ���������� ������������� ���� ������ ���������� ������������ ������������ ������������ � ������� ����������� � ������������� ���������� ������������� ���������� ���������� � ������������ � �������������� ����������� ���������� ���������� (����� </I>&#8212;<I>����), �������������� ��������� �� ������������� ���������� ���������� ����������.</I></B></P>
<P align=justify>��� ����������� ���������� ���������� � ������������ � ���� ��������� ������������ ������������ ��������� ����������� ���������� ����������. ��������� �������� ������� ����������, ����������� ���������� ���������� � ������������ � ����, ������� ��������� ��������������� � ������������� ������ �������������� ������� � ������ � �������� � �������. ����� �����, ��������� �������� ������������������� ���������������� ��������, � ����� ������� �������� ��������� ������������ � ������� ������, ������������� ��� ���������� ���������� � ������������ � ����������������� ���������� ���������, � ��� ����� ������������ ������ ����� ������.</P>
<P align=justify>���������������� �������� &#8212; ��� ���������������� ������ ������������� ��� ��������� �����������, �������������� �� ���� ����������� ������������� ��������� ���������� � ���������� ��������� � ������� ��������� ����������� � ������������ � ������������ ����.</P>
<P align=justify>��������� ���������������:</P>
<P align=justify>&#8212; ������������� ��� ��������� �����������, ������� ������������ ��������� ����������� ������������� ����� ����������� ���������������� �������� �� ������� �� ����������� ������������ ��������� ����������� � ������������ ���������� �� ���� ���������������� ��������� ������������� � ���������� ������������� ����������;</P>
<P align=justify>&#8212;&nbsp;����������� ��� �� ����� ������������� ��������� �����������, ������� ������� ��������� �������� ������������� �������������� ���������������� �������� � ����������� �� ������ ���� �������� ������������� � ���������� ������������� ����������;</P>
<P align=justify>&#8212;&nbsp;������������� ��������� �����������, ������������� �� ��������� ���� ������������� � ���������� ������������� ����������, ���������� �� ��������������� ������������� ��������� �����������, � ����� ����������� ��� ��������� �����������, �������������� �������� ������������ ��������� ���� ������������� � ���������� ������������� ���������� ��� ����� ���������� ���������� ���������� � ������������ � ����;</P>
<P align=justify>&#8212;&nbsp;������� ����������, �����������, �����������, ����������, �������� ������������, ������� ��������� ���������������, ���������������� �������� � ���������� �� ���� ���������������� ��������� ������������� � ���������� ������������� ����������, � ����� ������ �������������, ���������� � ���������� ���������� ��������� ����������� � ������������ � ����;</P>
<P align=justify>&#8212;&nbsp;���������� ���������� ���������� ��������� ����������� ������������� � ������� �����������, ������� � ������������ � ����������������� ���������� ���������, � ��� ����� ������������ ������ ����� ������, ����� ��������������� �� ������������� ���������� ����������.</P>
<P align=justify>����������� ����������������� ���������� ������������ ����� ��������, �������������� ����� ������������� ���������� ���������� � ������ ����.</P>
<P align=justify>��� ��������� ����������� ���������� ����� 0409101 &#171;��������� ��������� �� ������ �������������� ����� ��������� �����������&#187;, ������� ����� �������������� � ������������ � ���������� ������������������ ����������.</P>
<P align=justify>��� ������������� ��� ������������� ���������� ������������� ���������� ����������� � ����������� �� ���� �������, ������� ��� ��������� �� ����������:</P>
<P align=justify>&#8212;&nbsp;���������� �������� ��� ���������, ���� � ��������� ���������� ����������� ������ ������ ������������ ������ ����������� ���������� ���������� � ���������� ����;</P>
<P align=justify>&#8212;&nbsp;���������� �������� ������, ���� ������ ���������� ���������� ���������� ������������� ��������� ����������� ���������� ����;</P>
<P align=justify>&#8212;&nbsp;������� ���������� ����������� �����������, ���� ���������� ����������� ���������� ���������� ������������� ���� ���������� � ���������� ���������� �� ����.</P>
<P align=justify>������������ �� �� &#8470; 181-� �������� �� ������ ����������, �����������, ��� ����� ����� ���������� �� �������������� ��� ������������� ���������� � ������ ����, �� � �������� ������������� ��� ���������� �������������. ��� ����� ������� ������ ���������� ��������� ���������� ���������� �� ����. ����� ����, ������������ �������� ������ �� ����, �������� ������� ����������� 31 ������� 2004 �., � ������, ��� ����������� ���������� � 2005 ���� ������ ������� ���������.</P>
<P align=justify>�������� ����� ������������� ����� �������������� ��������� �������:</P>
<P align=justify>&#8212;&nbsp;������ ������� �������� � �� ���������� � ������������� �����;</P>
<P align=justify>&#8212;&nbsp;������ ����������� ��������� ���������, �������������� �� ���� ��, ����������� ��������� ���������� ��� ���������� � ������������ ������ �������������;</P>
<P align=justify>&#8212;&nbsp;���� ����������, ����������� ��� ���������� �������������, ���������� � ���������� ������� ����������;</P>
<P align=justify>&#8212;&nbsp;������������� ��������� ���������, �� ���� �������� ������������� � �������� � �����������;</P>
<P align=justify>&#8212;&nbsp;���������� ������������ ������ ���������� ���������� �� ���� �� � ����.</P>
<P align=justify></P>
<P align=justify>��� ���������� ��������, ����������� ���������� ��������� ����������� ����� ��� ������������ �������� � ����, ��� � ��������������. ������ ������ ��� ������ � ����� ����������, ��������� ��������� ��� ����� �������������� ����� �� ���� ��. ��� ���������� ����� �������� ���������� �������� ������ ���� �� � ����.</P>
<P align=justify>������������� � �������� �������� ���� 31 ������� 2004 ���� ��������, ��� ��������� ������������� ������ ���� ������� �� ����� ������ ���� � ������� ��������� ����.</P>
<P align=justify>����� ������� ������������� ��� �������������, ��������� ������, ��������� �� ���� ��, ���������� ������������ � ������� ������� �� ���� ����������� �������� �� 31 ������� 2004 ����.</P>
<P align=justify>��� ��������� ������������ �������� ��� � �������� �������������, ��������� ��������� ������ ���� � ��������� ��������� �������������, ����������� �����.</P>
<P align=justify>��� ����������������� ������������� ������������� ��������� �������. ����� ������� �� �������������� � ���� ������� ������� ������������� �������� ���������� ������. ������� ������ ������ �����������, ���� ����� ��������� ��� ��������� �������. ��� �� ���������� �� ��� ���, ���� �� ���������� ������ � ����� �� ����� ������ � ������� �����������. ����� ����� � ��������� �������� �� ��������� ������� ��������� �������� ��� �� ������, ��������� �� ����, ��� � �� ������ ����.</P>
<P align=justify>��� ��� ��� ����������������� ������������� ������������� ��������� �������, �� ���� �������� �� �������� ����� ������� �������� ������. ��� ������ � �������������, �� ����������� �������� �������, �� �������� ��������� ������������� ����� ������������� � �������� ��������.</P>
<P align=justify>�� ���� ����������� ��������� ������ ������ �� ����� ������������� ����������� ������. ���������������� �������� �� ����� 100 �.�. ������ �� ����� ������������� ���������� ������������� � ����� 24 �.�., ������ ������ ������������� �� 76 �.�. �� ������� ������ �� �������. ��������� ����� ����� �������� �������� �� �� ������ ������, � ����� ������ ������.</P>
<P align=justify>������ ���������� ������������� ����� �� ����������� ������ �� ������� ������������� ���������� ������������� �������� ��� &#171;����������&#187; �����. ��� ����� ������������� ��� ����� �� ��� ����������������� �������������, �������� �� ������ ������� � ��������.</P>
<P align=justify>��� � ��� ����������������� �������������, ��� ���������� ������ �� �������� ����� ������������ � ��� �� ������, ��� � ��������������� �� �������������. �� ����� ������������� ��� &#171;��������� ��������� �������&#187;.</P>
<P align=justify>�������� �������� ������������� �������� �������������.</P>
<P align=justify>������������� �������� �� ������ � ������� � �������������� �����������. ��� �������� � ��������� ��������������� � ������������� ������ �������������� ������� � ������ � �������� � �������, � ����� �������������� �������������������� ����������������� ����������.</P>
<P align=justify>����� ����� ��� ���������� � ��������-��������� ��������� � ������� ����, ������� ������ ������� ��� ���������� ���������� ����������.</P>
<P align=justify>�������, ������� ������ ������������� �� ���������� ���������� ����������, �������� ���������� ������ ��� ������ ������� ���� ������ ��������� �����������.</P>
<P align=justify>������������� ���������� ����� ������������� ���������� ���������:</P>
<P align=justify>&#8212;&nbsp;������ ��������������, ������� ������������ ���������������� ������� ����������� ����� � ���� ���� (� �������� ������� ����� �������� �������� �������� �������� ���� 37, ����� ����������� ��������� ����������� ������ ���������� � ���, ��� ��������� ������� �� ���� �� ������������� ����������� ����);</P>
<P align=justify>&#8212;&nbsp;�������������� �� ������ �������� ����� ������ � ������� ����, ������� ���������� � ���������� ��������������� ������ ����������� ����� �� ��������� ������ ���� (��������, ��� ������������� �������������� ������� � ������� ���� ��������� ������������ ������������� �� ������ �� ������� �������� �� ������ ������� � ������ � ������. � �� �� ����� ���������� ������ � ������ � ���� ������� � ����� &#171;������������� �� ������ �������&#187; �������� ���� (IAS) 1);</P>
<P align=justify>&#8212;&nbsp;�������������� �� ������ �������� ����� ������ ��� ������������� � ���� � ��� �������, ����� �� ���� ����������� ������������ �����-���� �� ���������� ������ ��� ������������� ������������� � ���� � � ��������� ��������-��������� ��������� �� ���� ������ ������� ������� ������� (��������, ��� ����� ����������� �������� ������� �������� ���� (IAS) 36 ��������� ��������� ������� � ������� ��������, ��� ��� �� ���� �� ����������� �� ����������).</P>
<P align=justify>� ���������� ������������ ������� �������������� ������ �������������� ����� ����������� ���� ���� �������� ������������ ������, � ������� ��������� ���������:</P>
<P align=justify>&#8212;&nbsp;���� ��������� ������������� ������� �� ���������� �������� ����, ��� �� ����;</P>
<P align=justify>&#8212;&nbsp;�������� �������� � ��������� ������� ���������� �������� ������� &#8212; ���������� ��������� � 2004 �. � ������ ���� �������� � ���������� �� ���� �� ���� ���, � �� ���� �� ���������� ���������� ������ � ��������� ����;</P>
<P align=justify>&#8212;&nbsp;�������� �������� � ��������� �������� �������� �������;</P>
<P align=justify>&#8212;&nbsp;�������� �������� � ������� � ������ ������� ����� ���������, ��� ����������� �������.</P>
<P align=justify>������� ��� ��������� ���� �������� ��������������� ���������� ��.</P>
<P align=justify><STRONG>��������� � ������ �������� ������� (���� ������������)</STRONG></P>
<P align=justify>�������� ���������� �������� �������������� ����� �� ����������� ������� ��������� � ������� ������, ��������� � ���������� ����� ������������ ��� ������ �����������, ����� ������, ����� ��������� ��������� (���������) ������������� �������� ����������� ����������� ���������������� ������� �������� ������� � ���������� ����������� ���������, ��������������, ������������� ��� ������������ ������������ ���������������� ���� ��������� ������������� �� ����� �������.</P>
<P align=justify>� ������������ � ���� (IAS) 16 (��. ��������� 51, 61) ��������� ������� ������, ��������� � ���������� ����� ������������ ��� ������ �����������, �����������.</P>
<P align=justify>� ���� 2004 ���� ��������� ����������� ���������������� ���������� ���� ������������ ������������� ������������ � ������� ������� ��������� ���� ���� � 5 �� 4 ���. ������� ��� ��������� ����� ��������� ����� ������������ �� ���� ���������� ��������� �������� ����������� � ������������ � ���� �����������.</P>
<P align=justify>������������� ������������ � ������� 1 � ����������� � ���������� ����������� ��������������� ���������� �� 120 �.�.</P>
<P align=justify></P>
<P align=justify><STRONG>��������� ������� ���������� �������� �������</STRONG></P>
<P align=justify>���������� �������� ������� � ������������ � ��������� ���� ��, ����������� �� ����� ��������� ����, ���������� � ������ ���������� ����������� ����. �������������, ���������� ���������� � ��������� ����, � �� � ��� ����, � ������� ��� ���� ����������� �� ����� ����.</P>
<P align=justify>���������� �������� ������� � ������������ � ��������� ���� ���������� � ��� �� �������� �������, � ������� ��� ���������.</P>
<P align=justify>� ������� 2004 ���� ��������� ����������� ��������� ���������� ������ ��������� (����������� ������ � ��������� ���������, ���������� ��, � ����� ������������������� ������������).</P>
<P align=justify>� ����� ��������� ���������� ������������ ����� ���������� �����: �������� ������� �� ����� 10 000 �.�., � ����������� �� ����� 3 600 �.�.</P>
<P align=justify>� ������������ � ������������ ���� �������� ����� �������� � ������������� ����� �� 2005 ���, � �� ���� ��� ������ ���� �������� � 2004 ����. ����������� ������������� ������������ � ������� 2.</P>
<P align=justify></P>
<P align=justify><STRONG>��������������� �������� ������� � �������������� �������������</STRONG></P>
<P align=justify>��� ������������ ������ �� ������ ��������� ������������� ����� 1 ���� �������� ���� �� ���������� ��������� ���������� ���������� �� �� ������������� ��������.</P>
<P align=justify>�������� ���� �������������� ������������� ������������ ���������� ���������, ����������� � ��������� ��� ���������� �� �������� ���������� ������, � ����� ��������� �������� �������� ��� ��������� �������� ���� ������������ � ����, � �������.</P>
<P align=justify>� �������� ���� ������� ������, ������� � 1 ������ 2004 ���� � ����� � �������������� ������� ���� ����� � ������ ������ �����.</P>
<P align=justify>��������� ������ ���������� 100 000 �.�.</P>
<P align=justify>����������� ����������� �� ��������� �� 31 ������� 2004 �. &#8212; 10 000 �.�. ����������� �������� ������������ � ������� 3.</P>
<P align=justify><B>���������� �������������� �������������</B></P>
<P align=justify>�������� ���� �� ���������� ���� �������� ������� ���������� ����� ������������� ����� ����������� ��������.</P>
<P align=justify>�������� ���� ���������� �������������� ������������� ���������� � ������ � �������� � ������� (���� (IAS) 40, �������� 35).</P>
<P align=justify>��������� ����������� ��������� ���������� ����� ��������� � ������������ ������������ �������� � 2004 ����. ������� ��������� ��������� �� ���������� ��� ������� �� ���������� ������� &#8212; 3 400 � ������� � ������������� ������� �� ���� �� ��������� �� 31.12.2004 �.</P>
<P align=justify>��� ���������� ���������� � ������������ ����������� ���� ���������� �������� ��������� ������������� ����� ����������, �������������� � ������� 4.</P>
<P align=justify><B>����������� �������� �������</B></P>
<P align=justify>�������� ���� �� ����������� �������� ������� �� ����������.</P>
<P align=justify>�������� ���� ���������� ����������� ������������ ��������� �������� ������� ��� ������������ ������ (CGU), ���� ��� ���� ��������������� ���������� ���� (IAS) 36.</P>
<P align=justify>� ��������� ����������� ������� ������, ������ ��������� ��������� �������� ���� ��� ���������� ��������� �� 2 900 �.�. ������ ��������� ������� ������������ ����� ������� �������� ����. �������������, ��������� ������ ������ ���� �������� �� ������ ��������� ���������, ������������� ������������ � ������� 5.</P>
<P align=justify></P>
<P align=justify><SUP>1</SUP> &#171;����������� �������� �������&#187; ���������� ��������� ������� � ������ � �������� � ������� � ��������� �� ��������� �������� �������� ���� (IAS) 1.����� �� ������� ���������� ������ ���� ������ ����� 0409101 &#171;��������� <I>��������� </I>�� ������ �������������� ����� ��������� �����������&#187; � ������������ ���������������� ��������.</P>
<P align=justify><B>���������� ������������� ���������� ��������
�� � ������������ � ����������� ��������� �����������</B></P>
<P align=justify>����� ������������ �������� ������������� ���������� ���������� ��������� ����������� ���������� ������� � ���������� ������������� �������� � ������������ � ������������ �� ��.</P>
<P align=justify>������������ �� �� &#8470; 181-� �������� ���������� �� ���������� ���������� ����������� ���������� ����������, ������� �������� � ���� ����������� ��������� ��������������� � ������������� ������ �������������� ������� � ������ � �������� � �������.</P>
<P align=justify>� �������� ������� ���������� ������ ��� ���� <EM>&#171;</EM>N<EM>&#187;:</EM></P>
<P align=justify>&#8212;&nbsp;��������� ��������������� � ������������� ������ �������������� ������� � ������ � �������� � ������� ������������ � ������� 6;</P>
<P align=justify>&#8212;&nbsp;������������ ������ ���������� �� ���� �� � ���� ��������� � �������� 7,8.</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify>���� ������ �������� ��������������� � ���, ��� ������ ���� ���������� ��������� ������ �� ��� ��������, ������� ������� ������ ���.</P>
<P align=justify>� ������� �� ������������ ������������� �������, ������� ������������ � ����� ��������� Ecxel � � ����� ������� ������� �������� � ������ �������������.</P>
<P align=justify></P>
<P align=justify>������� 7 � 8 ������������ ����� ������������ ������ �������������� ������� � ������ � �������� � �������, ��� ��������� ������������� ����������� ������ �� �� �� 28.02.2005 &#8470; 35-� &#171;� ������������ ������������� �� ������� ���������� ����������, ������������ ���������� ������������� � ������������ � ����&#187;.</P>
<P align=justify>������� ��������, ��� ��������� ���������� � ���������� ���������� �� ���� ������ ��������� � ���������� ���������� � �������������� ���������� ����������, �������������� � ������������ � ����, ������� ���� � ���������� 1 � ������������� �� �� &#8470;181 -�.</P>
<P align=justify>������ ������ �������� ������ ���������� ����������, ������������ ���������� ������������� � ������������ � ������������ ����.</P>


</body>
</html>

