<!--
ID:35367508
TITLE:�������� ����������� ���� ���������� ���������� ������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:13
ISSUE_DATE:2005-07-15
RECORD_DATE:2005-10-04


-->
<html>
<head>
<title>
35367508-�������� ����������� ���� ���������� ���������� ������
</title>
</head>
<body >


<P align=center><FONT><STRONG>���� ������� �����������<BR>�� ���� 2005 �.</STRONG></FONT></P>
<P align=center>
<TABLE cellSpacing=1 cellPadding=7 width=598 border=1>
<TBODY>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>����<BR>����-<BR>����-<BR>���</FONT></P></TD>
<TD vAlign=top width="48%">
<P align=center><BR><FONT>���� �������</FONT></P></TD>
<TD vAlign=top width="40%">
<P align=center><BR><FONT>���������<BR>����������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>6 &#8211; 7</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>������ ������������� ������������ � ������ ������������������ �����������-���������</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������� ���������� � ��������������� �������������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>6 &#8211; 8</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>����������� ����������� ���������� � ������ �����: ����� � ������� �������� � ������ �� �������������</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������� ������������� ��<BR>������ � ������� ��������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>7 &#8211; 8</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>�������������� � ���������� ������������ � �����</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������� ������������� ����������� ������� � ������������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>7 &#8211; 9</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>��������� ������ ����� �� ��������-��������� ������������</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������ ������������ �������������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>8</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>������������ ��������������� � ������ �������������� ��������� ������������� �����</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������ �������-������������� ������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>8 &#8211; 9</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>����� ������� � ����������� ��������������� ����������� (���������) �������, ���������� ���������� �����, � �������������� ���������� � ��������� ������������</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������� �����, ������������� �� ����������� ��������������� ����������� �������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>9</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>�������� ������������ ������ �����</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������� ������������� �� ���������� �������, ����������� �������� � ������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>11 &#8211; 12</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>����������������� ��������� � ������������� ������� ������</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������� ������������� ��������� � ����������������� ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>11 &#8211; 13</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>������������� � �������������� ���������� ���������*</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������ ������ ����������� ��������, ���������� �������������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>13 &#8211; 15</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>����������� ��������� ����������</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������� ���������� � ��������������� �������������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>14</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>���������� ����������� ������� ��� ������ �����������</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������� ������������� �� ���������� �������, ������������� ����� � ������������� ������������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>14 &#8211; 16</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>���������� ������� ��������� ������������� � ��������� �������� � �����</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������� ������������� ��������� ��������, ������������� ��������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>15 &#8211; 16</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>���������� ������� ������ ��������� ���� ��������� �����������</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������ ��������� ����, ���-�����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" height=59>
<P align=center><FONT>����<BR>����-<BR>����-<BR>���</FONT></P></TD>
<TD vAlign=top width="48%" height=59>
<P align=center><BR><FONT>���� �������</FONT></P></TD>
<TD vAlign=top width="40%" height=59>
<P align=center><BR><FONT>���������<BR>����������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>18 &#8211; 20</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>���������� ����� ������ � ������������� ���������� ����������. ������������� ���������� ����� � ����</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>�������� ����������, ������������� ������������� ����������� ��������, ����������� � �������� ������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>20 &#8211; 21</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>������������� ����� &#8211; ����� ����������� ��� ������������� �����</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������� ������������� �� ������ � ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" height=18>
<P align=center><FONT>20 &#8211; 22</FONT></P></TD>
<TD vAlign=top width="48%" height=18>
<P><FONT>���������� ������ ������������ �����*</FONT></P></TD>
<TD vAlign=top width="40%" height=18>
<P><FONT>������������� ������������� ����������� ������� � ������������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" height=24>
<P align=center><FONT>20 &#8211; 22</FONT></P></TD>
<TD vAlign=top width="48%" height=24>
<P><FONT>��������������� � ������������� ����<BR>� �����</FONT></P></TD>
<TD vAlign=top width="40%" height=24>
<P><FONT>�������� ����������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" height=30>
<P align=center><FONT>21</FONT></P></TD>
<TD vAlign=top width="48%" height=30>
<P><FONT>��������� ������ � �����</FONT></P></TD>
<TD vAlign=top width="40%" height=30>
<P><FONT>������������ �� ���������� � ��������� ������, ��������������, �����������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" height=27>
<P align=center><FONT>22 &#8211; 23</FONT></P></TD>
<TD vAlign=top width="48%" height=27>
<P><FONT>����������� ������� ������������ ����������� � �����</FONT></P></TD>
<TD vAlign=top width="40%" height=27>
<P><FONT>������������� ������������� ����������� ������� � ������������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" height=27>
<P align=center><FONT>22 &#8211; 23</FONT></P></TD>
<TD vAlign=top width="48%" height=27>
<P><FONT>�������-�������� �������� � �����<BR>(�������-���������)</FONT></P></TD>
<TD vAlign=top width="40%" height=27>
<P><FONT>�������� �������� � ��������������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" height=27>
<P align=center><FONT>25 &#8211; 28</FONT></P></TD>
<TD vAlign=top width="48%" height=27>
<P><FONT>�������� ������������� ������������ �����*</FONT></P></TD>
<TD vAlign=top width="40%" height=27>
<P><FONT>������������ ����������� ������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" height=27>
<P align=center><FONT>25 &#8211; 28</FONT></P></TD>
<TD vAlign=top width="48%" height=27>
<P><FONT>���������� �������� � ����� � �����. ��������������� ��������� �����������</FONT></P></TD>
<TD vAlign=top width="40%" height=27>
<P><FONT>������������� ������������� ����������� �������� � ������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" height=27>
<P><FONT>30 ���� &#8211;<BR>5 �������</FONT></P></TD>
<TD vAlign=top width="48%" height=27>
<P><FONT>���������� ���� � ������������ �����</FONT></P></TD>
<TD vAlign=top width="40%" height=27>
<P><FONT>������������ �����, ��� ������������, �������� ����������</FONT></P></TD></TR></TBODY></TABLE></P><B>
<P align=center><FONT>���� ������� ����������� �� �������� 2005 �.</FONT></P></B>
<P align=right>
<TABLE cellSpacing=1 cellPadding=7 width=598 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>����<BR>����-<BR>����-<BR>���</FONT></P></TD>
<TD vAlign=top width="48%">
<P align=center><BR><FONT>���� �������</FONT></P></TD>
<TD vAlign=top width="40%">
<P align=center><BR><FONT>���������<BR>����������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>8 &#8211; 9</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>����������������� ��������� � ������������� ������� ������</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������� ������������� ��������� � ����������������� ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>9 &#8211; 10</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>����� ������� � ����������� ��������������� ����������� (���������) �������, ���������� ���������� �����, � �������������� ���������� � ��������� ������������</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������ �����, �������������� �� ����������� ��������������� ����������� �������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>9 &#8211; 10</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>�������-�������� �������� � �����<BR>(�������-���������)</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>�������� �������� � ��������������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>10</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>�������� ������������ ������ �����</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������� ������������� �� ���������� �������, ����������� �������� � ������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>12 &#8211; 14</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>����������� ��������� ����������</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������� ���������� � ��������������� �������������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>13 &#8211; 14</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>������ ������������� ������������ � ������ ������������������ �����������-���������</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������� ���������� � ��������������� �������������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>����<BR>����-<BR>����-<BR>���</FONT></P></TD>
<TD vAlign=top width="48%">
<P align=center><BR><FONT>���� �������</FONT></P></TD>
<TD vAlign=top width="40%">
<P align=center><BR><FONT>���������<BR>����������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>14 &#8211; 16</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>��������������� � ������������� ���� � �����</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>�������� ����������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>15</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>������������ �������� �� ��������� ������ �� ������</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������� ���������� � ��������������� �������������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>15 &#8211; 17</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>���������� ������� ��������� ������������� � ��������� �������� � �����</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������� ������������� ��������� ��������, ������������� ��������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>19 &#8211; 20</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>����� � ����������� ��������</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������ �� ���������� � ��������� ������, ��������������, �����������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>19 &#8211; 21</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>���������� ����� ������ � ������������� ���������� ����������. ������������� ���������� ����� � ����</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>�������� ����������, ������������� ������������� ����������� ��������, ����������� � �������� ������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>19 &#8211; 22</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>���������� �������� � ����� � �����. ��������������� ��������� �����������</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������� ������������� ����������� �������� � ������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>21 &#8211; 22</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>����������� ���������� ���������� � ����� (�������)</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������� ������� ����� � ������ ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>21 &#8211; 23</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>���������� ������ ������������ �����*</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������� ������������� ����������� ������� � ������������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%">
<P align=center><FONT>21 &#8211; 23</FONT></P></TD>
<TD vAlign=top width="48%">
<P><FONT>��������� ����������: ����������� ����������� � ����� �����������</FONT></P></TD>
<TD vAlign=top width="40%">
<P><FONT>������������ ���������� �������������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" height=18>
<P align=center><FONT>22 &#8211; 23</FONT></P></TD>
<TD vAlign=top width="48%" height=18>
<P><FONT>����������� ������������ �������� � �������������� ������</FONT></P></TD>
<TD vAlign=top width="40%" height=18>
<P><FONT>������������� �������, ��������������� �����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" height=24>
<P align=center><FONT>22 &#8211; 23</FONT></P></TD>
<TD vAlign=top width="48%" height=24>
<P><FONT>����������� ��������� ���������� ��������� � �����</FONT></P></TD>
<TD vAlign=top width="40%" height=24>
<P><FONT>������� � �������� ��������������� ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" height=30>
<P align=center><FONT>27</FONT></P></TD>
<TD vAlign=top width="48%" height=30>
<P><FONT>���������� ����������� ������� ��� ������ �����������</FONT></P></TD>
<TD vAlign=top width="40%" height=30>
<P><FONT>������������� ������������� �� ���������� �������, ������������� ����� � ������������� ������������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" height=27>
<P align=center><FONT>27 &#8211; 30</FONT></P></TD>
<TD vAlign=top width="48%" height=27>
<P><FONT>�������� ������������� ������������ �����*</FONT></P></TD>
<TD vAlign=top width="40%" height=27>
<P><FONT>������������ ����������� ������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="12%" height=27>
<P><FONT>30 �������� &#8211; 5 �������</FONT></P></TD>
<TD vAlign=top width="48%" height=27>
<P><FONT>���������� ���� � ������������ ����� (�����, �����)</FONT></P></TD>
<TD vAlign=top width="40%" height=27>
<P><FONT>������������� ������, �� ������������, ������� �����������</FONT></P></TD></TR></TBODY></TABLE></P>
<P align=justify><FONT>����������. ��������, ���������� "����������" (*), �������� ����� � ������� �����. ���������� �� ����� ��� ���: </FONT><A href="http://www.ibdarb.ru/"><FONT color=#0f0f0f>www.ibdarb.ru</FONT></A></P><B><I>
<P align=justify><FONT>105187, �. ������, ��. ������������, 38<BR></FONT><FONT>( (095) 365-3105, 366-5302, 366-2351<BR></FONT><FONT>���� (095) 365-0107, 365-1107<BR></FONT><FONT>E-mail: </FONT></B></I><A href="mailto:info@ibdarb.ru"><B><I><FONT color=#0f0f0f>info@ibdarb.ru</FONT></B><
/I></A></P>


</body>
</html>

