<!--
ID:35530679
TITLE:� ���������� ��������� ������� �� �������� ���������� ������������
SOURCE_ID:3126
SOURCE_NAME:������� ����������� ����
AUTHOR:
NUMBER:8
ISSUE_DATE:2005-08-31
RECORD_DATE:2005-11-15


-->
<html>
<head>
<title>
35530679-� ���������� ��������� ������� �� �������� ���������� ������������
</title>
</head>
<body >


<TABLE cellSpacing=1 cellPadding=7 width=662 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="32%"><B>
<P align=justify>������������</B></P></TD>
<TD vAlign=top width="68%">
<P align=justify>������� �.�., ��� &#8220;���� ���������&#8221;, ������</P></TD></TR>
<TR>
<TD vAlign=top width="32%" rowSpan=6><B>
<P align=justify>����� ��������:</B></P></TD>
<TD vAlign=top width="68%">
<P align=justify>������� �.�., ������� <I>(�� ������������)</I></P></TD></TR>
<TR>
<TD vAlign=top width="68%">
<P align=justify>������� �.�., ��� �� &#8220;����������&#8221;, ���������� �������</P></TD></TR>
<TR>
<TD vAlign=top width="68%">
<P align=justify>������� �.�., ��� &#8220;�������������&#8221;, �. ������������</P></TD></TR>
<TR>
<TD vAlign=top width="68%">
<P align=justify>��������� �.�., ��� �� &#8220;������������&#8221;, ������</P></TD></TR>
<TR>
<TD vAlign=top width="68%">
<P align=justify>������ �.�., ��� &#8220;����&#8221;, ������ <I>(�� ������������)</I></P></TD></TR>
<TR>
<TD vAlign=top width="68%">
<P align=justify>��������� �.�, ��� ��� �������� &#8220;�����������&#8221;, �. �����������</P></TD></TR>
<TR>
<TD vAlign=top width="32%"><B>
<P align=justify>������������� �� ��������� ���������� &#8220;������&#8221;</B></P></TD>
<TD vAlign=top width="68%">
<P align=justify>�������� �.�., ������ ����-��������� ����������</P></TD></TR></TBODY></TABLE><B>
<P align=center>�������� ����������� ������������ �������� ����������<BR>�� �������� ���������� ������������</P>
<P align=justify>I. ���������� ����������� �� �������� � ����������������� ����������� ���������������� � ����������� ���� �� �� � ������� ���������� ������������</P>
<UL></B>
<P align=justify>
<LI>������ ������� ������ �������, �������� � 2004 �., �� �������� �������� �� ���������� ��������� ������������;</LI>
<P align=justify>
<LI>���������� �������� ������� ����������� ������ �� �������;</LI>
<P align=justify>
<LI>����������� ���� ����� ������ � ����������� �� �����������������.</LI></UL><I>
<P align=justify>����� ����������:</P>
<UL></I>
<P align=justify>
<LI>��������, �� ���� �������������, � ������ �������� ������� ����� �� ����� ������������ ������ ��� ���������� ��������������� ����������;</LI>
<P align=justify>
<LI>���������� ���������� �������������� ���������� �� ���������� �������� � ����������� �� ��� ���������� � ��������������� ����� � ����� ���������� &#8220;������&#8221;, � ����� � ������� ������� �� ��������� ������������ � ���������� ������ � ������ ���������� �������� ������������ ��������, � ��������������� ������������ �� ��, ������������� ��, ������������ � ��������� ��;</LI>
<P align=justify>
<LI>�������������� � ������������� ������� ���������� �� ���������� ������������� ���������� �� ��������� �������.</LI></UL><B>
<P align=justify>II. ������������ ������ �� ����������� �������������� ������������ ������������� � ������������ ������ � ���� ���������� ������������ �������� ��������� ��������</P></B><I>
<P align=justify>����� ����������:</P>
<UL></I>
<P align=justify>
<LI>�������� ���� ������ �� ������ &#8212; ������ ���������� &#8220;������&#8221;, ����������� �������� ��������� ���������;</LI>
<P align=justify>
<LI>���������� �������� ���������-��������� � �������� ������ �� ���������� ��������� ������� � �������� ������������� � ������������ ������ � ������������� ��������� ��;</LI>
<P align=justify>
<LI>���������� ����������� �������� ���������� ���������� ��������� �������� � �������� ��.</LI></UL><B>
<P align=justify>III. ���������� ��������� �������� �� ���������� ��������� ������������, ���������� �������� ����������� � ������������� ��������������� ������������� ������ � ������ �������������� ������ ���������� &#8220;������&#8221; � ����������� ���������� ����������</P></B><I>
<P align=justify>����� ����������:</P>
<UL></I>
<P align=justify>
<LI>���������� ��������� �������� ������� �������� � ����������;</LI>
<P align=justify>
<LI>���������� � ���������� ������ ������ �������������� ������ ���������� ����� ��������� � ����������.</LI></UL><B>
<P align=justify>IV. ���������� ��������� � �������� ��������������, ����� ������ ������ � �������������� � ����������� ����������� &#8212; ����������������� ����������� ���������� ����� (�� ���������� �����).</P>
<P align=center>���� ������ �������� �� �������� ���������� ������������</P>
<P align=justify>1. �������� ��������� ��������</P>
<DIR>
<DIR></B>
<P align=justify>1.1. ������� ����������� �������� � ����� ���������� ������������;</P>
<DIR>
<DIR>
<P align=justify>1.2. ������� ������ �������, �������� � 2004 �., �� �������� �������� �� ���������� ������������;</P></DIR></DIR>
<P align=justify>1.3. ���������� �������� ������� ����������� ������ �� �������;</P>
<P align=justify>1.4. �������������� �������� �� ������������ ����� � ���������� �����; ����������� ����� &#8470; 214-�� &#8220;�� ������� � ������� �������������&#8221;; �������������� ������, ������������ � ��������� ��� ���������� �������� ��������;</P>
<P align=justify>1.5. �������� ���������� � ����������� ���������;</P>
<P align=justify>1.6. ������� ����������� ��������� �������� � ������ � ��������;</P>
<P align=justify>1.7. �������������� ������������ ������������� � ������������ ������ ��� ������������� ������������ �������� ��������� ��������;</P>
<P align=justify>1.8. ������� ������� ��������� ������ �����;</P>
<P align=justify>1.9. �������������� ������, ��������� �������� � ������������;</P>
<P align=justify>1.10. ����������� ��������� ������;</P>
<P align=justify>1.11. ��������� ����;</P>
<P align=justify>1.12. ����������� ���� ���������� ������������ � ����������� �� �����������������;</P>
<P align=justify>1.13. ����������� ������������ �������������. ����������� ���� �� �� � ����������� �� �����������������;</P>
<P align=justify>1.14. ����������� �������� ������������������� ����;</P>
<P align=justify>1.15. ���������� ���� �� �����. ����������� ������������ ��������������� �������������.</P></DIR></DIR><B>
<P align=justify>2. ������� � ����������� � ���������� ����������� � ������� ������</P>
<DIR>
<DIR></B>
<P align=justify>2.1. ����������� ����������� �������� �� &#8220;������������&#8221; �� ������ � ���������������� ��������� �������� � ������ ��������� ���������������� ������ ���������� &#8220;������&#8221; <I>(17 ������� 2005 �., �. ������ ��������)</I>;</P>
<P align=justify>2.2. ���������� � ���������� &#8220;�������� �����&#8221; &#8220;�������� �������� ���������� ������������ � ������&#8221; � ������ III �������������� ����������� ������ &#8220;����� ������ &#8212; XXI ���&#8221; <I>(2 �������� 2005 �., �. ����).</P></DIR></DIR></I><B>
<P align=justify>3. ������ � ������� ������� ��� ������������� ��, �������, �� �� �� ����������������� ���������������� � ����� ������� � ������������</P>
<P align=justify>4. �������� ��������� � �������������� � ���, ���, ����� � ��. ����������������� ����������� ���������� �����</P>
<P align=justify>5. �������� ��������� � ������������ � ������������ ����������� �������������.</P></B>


</body>
</html>

