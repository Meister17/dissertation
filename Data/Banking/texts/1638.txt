<!--
ID:36328225
TITLE:�� ����� ����&#133; ����������� ����� � ������
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:������� �������, ������� ������� "���������� ���� � ������"

NUMBER:4
ISSUE_DATE:2006-04-30
RECORD_DATE:2006-06-13


RBR_ID:2039
RBR_NAME:���������� �������
-->
<html>
<head>
<title>
36328225-�� ����� ����&#133; ����������� ����� � ������
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgwhite>
<TD></TD></TR>
<TR class=bodycolor>
<TD>
<P class=Main align=justify><EM>� 2005 ���� ����������� ��������� ���� ������� � �������� ��������� ��������� ����������� �� 26001 ���. ������ &#8212; ������, ��� �� ��� ���������� ����, ������ ������. � ���������� ���� ������������ � ���������� �������� �������� ������������� ���������� �������, ������� � ��������� ���� ��������� ����������� �� ������ 5-7%, �� �������� ��� ��������� � �������� 11,15%. ���������� ���������� � �������� �������� ������ � ������� ���� ��������� 37 908 ���. ���.</EM></P></TD></TR>
<TR class=bgwhite>
<TD height=0>
<P class=Main align=justify>
<P class=Main align=justify>�� ������ ��������� ���� � ������ ����������� 136 ������ � �������� ������������, �� ��� 81 ����� ����������� ��������, � ����������� ��� �������� ������ ���������� ������� ������. ����������� ����������� ���������������� � �������� (88 � ������ � 8 &#8212; � �����-����������). 
<P class=Main align=justify>� ���������� ������ ���� ����������� ���������� �� ��������� 20%, ��� � ������������ ���������� �� 20 �� 50%, � � ������������ �� ��� ���������� 14,3% ������������ �������� � ���������� ������� ������. ��� � 11 ������ ������������ ����������� ����� �������� ��������� �������� (� ����� 3,1% ������������ ��������). 
<P class=Main align=justify>�������� ����� ����������� ���������� (40953,1 ���. ������, ��� 82,6% �� ������ ������) ���������������� � ������ ������ �� ������������� �������� ������������. �� ��� �� ����������� ������� � 33 �� 41 �����, � ���������� �������� ������� &#8212; � 2,6 ����. ������ ��� � ��������� ���� ������������� ���������� ����. 
<P class=Main align=justify>���������� ����� � ��������� ������������� �������<STRONG> �������������� �������</STRONG>, ����������� ������� �� 5,4 ��������� ������, <STRONG>���-���� </STRONG>&#8212; �� 4 ���������,<STRONG> ������������� ���������� ����</STRONG> &#8212; �� 3 ���������, <STRONG>��� ������ </STRONG>&#8212; �� 1,25 ���������, � ����� ��������� � �������� ���� <STRONG>������ ������ ����</STRONG> &#8212; 1,3 ��������� � <STRONG>������� ��������������</STRONG> &#8212; 1,53 ��������� ������.</P></TD></TR></TBODY></TABLE>
<P></P>
<TABLE cellSpacing=0 cellPadding=0 width="100%" align=center border=1>
<TBODY>
<TR class=bodycolor>
<TD width="9%" rowSpan=3>
<P align=center><STRONG><FONT size=2>����</FONT></STRONG></P></TD>
<TD width="6%" rowSpan=3>
<P align=center><STRONG><FONT size=2>���������� �� ����������� ��</FONT></STRONG></P></TD>
<TD width="8%" rowSpan=3>
<P align=center><STRONG><FONT size=2>����� ����� ������������</FONT></STRONG></P></TD>
<TD width="8%" rowSpan=3>
<P align=center><STRONG><FONT size=2>���� ������������ � �� ��</FONT></STRONG></P></TD>
<TD width="8%" rowSpan=3>
<P align=center><STRONG><FONT size=2>����� �� � �������� ������������</FONT></STRONG></P></TD>
<TD colSpan=10>
<P align=center><STRONG><FONT size=2>����</FONT></STRONG></P></TD></TR>
<TR>
<TD class=bodycolor colSpan=2>
<P align=center><STRONG><FONT size=2>100%</FONT></STRONG></P></TD>
<TD class=bodycolor colSpan=2>
<P align=center><STRONG><FONT size=2>�� 50 �� 100%</FONT></STRONG></P></TD>
<TD class=bodycolor colSpan=2>
<P align=center><STRONG><FONT size=2>�� 20 �� 50%</FONT></STRONG></P></TD>
<TD class=bodycolor colSpan=2>
<P align=center><STRONG><FONT size=2>�� 20 �� 100%</FONT></STRONG></P></TD>
<TD class=bodycolor colSpan=2>
<P align=center><STRONG><FONT size=2>20% � �����</FONT></STRONG></P></TD></TR>
<TR>
<TD width="5%">
<P align=center><STRONG><FONT size=2>���-�� ��</FONT></STRONG></P></TD>
<TD width="6%">
<P align=center><STRONG><FONT size=2>�����</FONT></STRONG></P></TD>
<TD width="5%">
<P align=center><STRONG><FONT size=2>���-�� ��</FONT></STRONG></P></TD>
<TD width="5%">
<P align=center><STRONG><FONT size=2>�����</FONT></STRONG></P></TD>
<TD width="5%">
<P align=center><STRONG><FONT size=2>���-�� ��</FONT></STRONG></P></TD>
<TD width="4%">
<P align=center><STRONG><FONT size=2>�����</FONT></STRONG></P></TD>
<TD width="6%">
<P align=center><STRONG><FONT size=2>���-�� ��</FONT></STRONG></P></TD>
<TD width="6%">
<P align=center><STRONG><FONT size=2>�����</FONT></STRONG></P></TD>
<TD width="7%">
<P align=center><STRONG><FONT size=2>���-�� ��</FONT></STRONG></P></TD>
<TD width="12%">
<P align=center><STRONG><FONT size=2>�����</FONT></STRONG></P></TD></TR>
<TR>
<TD>
<P><FONT size=2>1 ������, 1999</FONT></P></TD>
<TD>
<P align=right><FONT size=2>52 510,00</FONT></P></TD>
<TD>
<P align=right><FONT size=2>3 336,50</FONT></P></TD>
<TD>
<P align=right><FONT size=2>6,35%</FONT></P></TD>
<TD>
<P align=right><FONT size=2>142</FONT></P></TD>
<TD>
<P align=right><FONT size=2>18</FONT></P></TD>
<TD>
<P align=right><FONT size=2>423,7</FONT></P></TD>
<TD><FONT size=2>&nbsp;</FONT></TD>
<TD><FONT size=2>&nbsp;</FONT></TD>
<TD><FONT size=2>&nbsp;</FONT></TD>
<TD><FONT size=2>&nbsp;</FONT></TD>
<TD>
<P align=right><FONT size=2>47</FONT></P></TD>
<TD>
<P align=left><FONT size=2>1101,0</FONT></P></TD>
<TD>
<P align=right><FONT size=2>77</FONT></P></TD>
<TD>
<P align=left><FONT size=2>1811,7</FONT></P></TD></TR>
<TR>
<TD>
<P><FONT size=2>1 ������, 2000</FONT></P></TD>
<TD>
<P align=right><FONT size=2>111130,00</FONT></P></TD>
<TD>
<P align=right><FONT size=2>11 898,50</FONT></P></TD>
<TD>
<P align=right><FONT size=2>10,71%</FONT></P></TD>
<TD>
<P align=right><FONT size=2>133</FONT></P></TD>
<TD>
<P align=right><FONT size=2>20</FONT></P></TD>
<TD>
<P align=left><FONT size=2>7359,0</FONT></P></TD>
<TD><FONT size=2>&nbsp;</FONT></TD>
<TD><FONT size=2>&nbsp;</FONT></TD>
<TD><FONT size=2>&nbsp;</FONT></TD>
<TD><FONT size=2>&nbsp;</FONT></TD>
<TD>
<P align=right><FONT size=2>38</FONT></P></TD>
<TD>
<P align=left><FONT size=2>4342,4</FONT></P></TD>
<TD>
<P align=right><FONT size=2>75</FONT></P></TD>
<TD>
<P align=right><FONT size=2>197,1</FONT></P></TD></TR>
<TR>
<TD>
<P><FONT size=2>1 ������, 2001</FONT></P></TD>
<TD>
<P align=right><FONT size=2>207402,00</FONT></P></TD>
<TD>
<P align=right><FONT size=2>15 576,00</FONT></P></TD>
<TD>
<P align=right><FONT size=2>7,51%</FONT></P></TD>
<TD>
<P align=right><FONT size=2>130</FONT></P></TD>
<TD>
<P align=right><FONT size=2>23</FONT></P></TD>
<TD>
<P align=left><FONT size=2>9475,7</FONT></P></TD>
<TD>
<P align=right><FONT size=2>10</FONT></P></TD>
<TD>
<P align=left><FONT size=2>5090,3</FONT></P></TD>
<TD>
<P align=right><FONT size=2>23</FONT></P></TD>
<TD>
<P align=right><FONT size=2>714,2</FONT></P></TD>
<TD>
<P align=right><FONT size=2>33</FONT></P></TD>
<TD>
<P align=left><FONT size=2>5804,5</FONT></P></TD>
<TD>
<P align=right><FONT size=2>74</FONT></P></TD>
<TD>
<P align=right><FONT size=2>295,8</FONT></P></TD></TR>
<TR>
<TD>
<P><FONT size=2>1 ������, 2002</FONT></P></TD>
<TD>
<P align=right><FONT size=2>260989,00</FONT></P></TD>
<TD>
<P align=right><FONT size=2>13 843,60</FONT></P></TD>
<TD>
<P align=right><FONT size=2>5,30%</FONT></P></TD>
<TD>
<P align=right><FONT size=2>126</FONT></P></TD>
<TD>
<P align=right><FONT size=2>23</FONT></P></TD>
<TD>
<P align=left><FONT size=2>8003,0</FONT></P></TD>
<TD>
<P align=right><FONT size=2>12</FONT></P></TD>
<TD>
<P align=left><FONT size=2>4530,2</FONT></P></TD>
<TD>
<P align=right><FONT size=2>18</FONT></P></TD>
<TD>
<P align=right><FONT size=2>745,5</FONT></P></TD>
<TD>
<P align=right><FONT size=2>30</FONT></P></TD>
<TD>
<P align=left><FONT size=2>5275,7</FONT></P></TD>
<TD>
<P align=right><FONT size=2>73</FONT></P></TD>
<TD>
<P align=right><FONT size=2>564,9</FONT></P></TD></TR>
<TR>
<TD>
<P><FONT size=2>1 ������, 2003</FONT></P></TD>
<TD>
<P align=right><FONT size=2>300391,00</FONT></P></TD>
<TD>
<P align=right><FONT size=2>15 886,80</FONT></P></TD>
<TD>
<P align=right><FONT size=2>5,29%</FONT></P></TD>
<TD>
<P align=right><FONT size=2>123</FONT></P></TD>
<TD>
<P align=right><FONT size=2>27</FONT></P></TD>
<TD>
<P align=left><FONT size=2>10114,3</FONT></P></TD>
<TD>
<P align=right><FONT size=2>10</FONT></P></TD>
<TD>
<P align=left><FONT size=2>4081,5</FONT></P></TD>
<TD>
<P align=right><FONT size=2>15</FONT></P></TD>
<TD>
<P align=right><FONT size=2>923,0</FONT></P></TD>
<TD>
<P align=right><FONT size=2>25</FONT></P></TD>
<TD>
<P align=left><FONT size=2>5004,5</FONT></P></TD>
<TD>
<P align=right><FONT size=2>71</FONT></P></TD>
<TD>
<P align=right><FONT size=2>768,0</FONT></P></TD></TR>
<TR>
<TD>
<P><FONT size=2>1 ������, 2004</FONT></P></TD>
<TD>
<P align=right><FONT size=2>362010,00</FONT></P></TD>
<TD>
<P align=right><FONT size=2>18 902,60</FONT></P></TD>
<TD>
<P align=right><FONT size=2>5,22%</FONT></P></TD>
<TD>
<P align=right><FONT size=2>128</FONT></P></TD>
<TD>
<P align=right><FONT size=2>32</FONT></P></TD>
<TD>
<P align=left><FONT size=2>13035,8</FONT></P></TD>
<TD>
<P align=right><FONT size=2>9</FONT></P></TD>
<TD>
<P align=left><FONT size=2>3422,1</FONT></P></TD>
<TD>
<P align=right><FONT size=2>15</FONT></P></TD>
<TD>
<P align=left><FONT size=2>1148,3</FONT></P></TD>
<TD>
<P align=right><FONT size=2>24</FONT></P></TD>
<TD>
<P align=left><FONT size=2>4570,4</FONT></P></TD>
<TD>
<P align=right><FONT size=2>72</FONT></P></TD>
<TD>
<P align=right><FONT size=2>1296,4</FONT></P></TD></TR>
<TR>
<TD>
<P><FONT size=2>1 ������, 2005</FONT></P></TD>
<TD>
<P align=right><FONT size=2>380468,00</FONT></P></TD>
<TD>
<P align=right><FONT size=2>23 553,00</FONT></P></TD>
<TD>
<P align=right><FONT size=2>6,19%</FONT></P></TD>
<TD>
<P align=right><FONT size=2>131</FONT></P></TD>
<TD>
<P align=right><FONT size=2>33</FONT></P></TD>
<TD>
<P align=left><FONT size=2>15775,7</FONT></P></TD>
<TD>
<P align=right><FONT size=2>10</FONT></P></TD>
<TD>
<P align=left><FONT size=2>3318,3</FONT></P></TD>
<TD>
<P align=right><FONT size=2>15</FONT></P></TD>
<TD>
<P align=left><FONT size=2>2890,9</FONT></P></TD>
<TD>
<P align=right><FONT size=2>25</FONT></P></TD>
<TD>
<P align=left><FONT size=2>6209,2</FONT></P></TD>
<TD>
<P align=right><FONT size=2>73</FONT></P></TD>
<TD>
<P align=right><FONT size=2>1568,1</FONT></P></TD></TR>
<TR>
<TD>
<P><FONT size=2>1 ������, 2006</FONT></P></TD>
<TD>
<P align=right><FONT size=2>444377,00</FONT></P></TD>
<TD>
<P align=right><FONT size=2>49 554,50</FONT></P></TD>
<TD>
<P align=right><FONT size=2>11,15%</FONT></P></TD>
<TD>
<P align=right><FONT size=2>136</FONT></P></TD>
<TD>
<P align=right><FONT size=2>41</FONT></P></TD>
<TD>
<P align=left><FONT size=2>40953,1</FONT></P></TD>
<TD>
<P align=right><FONT size=2>11</FONT></P></TD>
<TD>
<P align=left><FONT size=2>1558,2</FONT></P></TD>
<TD>
<P align=right><FONT size=2>14</FONT></P></TD>
<TD>
<P align=left><FONT size=2>5589,4</FONT></P></TD>
<TD>
<P align=right><FONT size=2>25</FONT></P></TD>
<TD>
<P align=left><FONT size=2>7147,6</FONT></P></TD>
<TD>
<P align=right><FONT size=2>70</FONT></P></TD>
<TD>
<P align=right><FONT size=2>1453,8</FONT></P></TD></TR></TBODY></TABLE>
<P class=Main align=justify>
<P class=Subheader align=center>�������� ����������� �� ����������� �� � ������ ���������� ������������, ���. ���.</P>
<P class=Subheader align=center></P>
<P class=Main align=justify>
<P class=Subheader align=center>���� ������������ � ���������� �� ����������� �� 
<P class=Subheader align=center></P>
<P class=Subheader align=center>����������� �� �� ������� ������������ � �������� �������� (����������) 
<P class=Subheader align=center></P>
<P class=Subheader align=center>����������� �� �� ����� ����� (�����) ������������ � �������� �������� (���. ���.) 
<P class=Subheader align=center></P>
<P></P>
<P class=Rubrika align=center>� ������ �������� ����������� ������, � �� ������ 
<P class=Subheader align=justify>�������� �����<BR><STRONG>��������� �� </STRONG></P>
<P class=top align=justify>������������ �������� ����������� ������ �� ���������� ������ ������ ���� ����������, � �� ���� &#8212; ���������. 
<P class=top align=right><STRONG><EM>�� ����������� �� ��������� � ������������<BR></EM></STRONG><EM><STRONG>�����-����, 14.12.2005 </STRONG></EM></P>
<P class=Subheader align=justify>����� ��������<BR><STRONG>������ ����������� ������������ ����������� �������� ������� </STRONG>
<P class=top align=justify>��� ��������� ���������� �������. ������������ �������� ����������� ������ ��������� ����������� � ���������������. �� �� ����� ���� �� �������. 
<P class=top align=right><STRONG><EM>����-����, 15.12.2005 </EM></STRONG>
<P class=Subheader align=justify>��������� �������<BR><STRONG>��������� ���������� ������������ ������ ������ </STRONG>
<P class=top align=justify>���������� ���������� ������� ���� �� ������ � ����������� � ������������ ������� � ������ ������ ��-�� ������� � ������� ��������� �����, �������������� � ������������� �������������. 
<P class=top align=right><STRONG><EM>���������, 26.01.2006 </EM></STRONG>
<P class=Subheader align=justify>������ ��������<BR><STRONG>������������ ����� ������ </STRONG>
<P class=top align=justify>���� ������������ �������� � ���������� ���������� ������� ��������� �������������, �� �������� ���������� ������&#8230; ������ ��� ������� ������ � ������ �����������. 
<P class=top align=right><STRONG>�� ����������� �� &#171;����������������� ����&#187;<BR></STRONG><STRONG>� ��������������� ����, ����-����, 22.03.2006 </STRONG>
<P class=Subheader align=justify>�������� ��������<BR>����������� ������������ ����� ������ 
<P class=top align=justify>����������� ���� ������� �������� ������� ������� ������������ � �������� ��������� ���������� ������, �������� ��� ���� ������ �� �������� �������� ����������� ��������� ����������� � ������. 
<P class=top align=right><EM><STRONG>�� ����������� �� �����-����������� � �������������,<BR></STRONG></EM><STRONG><EM>���������, 27.01.2006 </EM></STRONG></P>
<P class=Subheader align=justify>���� ������<BR><STRONG>������������ ���� </STRONG>
<P class=top align=justify>������������ ������� ���������� ��� ����������� ������ �� ��������� ������� � ������ &#171;����� ���������� ��������&#187;, ��������� ����������� ������� � ���������� ������ ����� ������ � ������ �����. 
<P class=top align=right><STRONG><EM>�����-����, 28.03.2006 </EM></STRONG></P>


</body>
</html>

