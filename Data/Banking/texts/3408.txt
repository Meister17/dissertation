<!--
ID:36228843
TITLE:����� ������������ ����� ���������� ��������� �������� ������������ ��������� ������������ ����������� ����������� � ��������� �������� �.�.��������� (�� 09.03.2006 � 12-1-5/541)
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:7
ISSUE_DATE:2006-04-15
RECORD_DATE:2006-05-18


-->
<html>
<head>
<title>
36228843-����� ������������ ����� ���������� ��������� �������� ������������ ��������� ������������ ����������� ����������� � ��������� �������� �.�.��������� (�� 09.03.2006 � 12-1-5/541)
</title>
</head>
<body >


<B>
<P align=justify>� ���������� ���������<BR>����� ������ �� 01.06.2004 &#8470; 258-�</P></B>
<P align=justify>����������� ����������� ����������� � ��������� �������� ���������� ������ ���������� ���������� ������ �� 16.02.2006 &#8470; �-02/5-113 � �������� ���������.</P>
<P align=justify>�������� �. 2.4 ��������� ����� ������ �� 01.06.2004 &#8470; 258-� "� ������� ������������� ����������� �������������� ������ �������������� ���������� � ����������, ��������� � ����������� �������� �������� � ������������� �� �������������� �������, � ������������� ��������������� ������� �������� �� ����������� �������� ��������" (����� &#8211; ��������� &#8470; 258-�) �������� ������ ����������� � �������������� ���� �������������� ��������� � ����, ������������� � �������������� ������, �� �� ������� 15 ����������� ���� ����� ��������� ������, � ������� �������� �� ��������� ��� ����������� ����� ������� � ���������� ���������� ���������� ��������� ��� ��������� ���������, �������������� ���������� �����, �������� �����, �������� ���������� � ����������� ���������������� ������������, � ��� ����� �������������� ���� �� ���.</P>
<P align=justify>�������� 9 �. 1 ��. 11 ����������� ������� ���������� ��������� (����� &#8211; ���������� ������) ���������� ����� ������� � ���������� ���������� ���������� ��������� ��� ������ ���������� ���������� ��� ���������� ��������� � ������ ������ ����� ��������� ��������, ��������������� ������������ �� ����� �������, � ����� ��� ����������� ��������������� ���������� �������� �������� � �������� �� ������������ ����������� ��� ���������� �������.</P>
<P align=justify>�������� �. 4 ��. 129 ����������� ������� ���������� ���������� �� ������, ��������� � ���������� ���������� ���������� ���������, �������� �� �� ������ � ���������� ���������� ���������� ���������.</P>
<P align=justify>��� ���� ���������� ������ (�. 4 ��. 124) ��������������� ���� ����������� ������������� ���������� ���������� � �������� ��������������� ���������, � �� ���������� �� � �������� ������������� ���������, ��������������� ���� ������ ������� � ���������� ���������� ���������� ���������.</P>
<P align=justify>������ � ��� � ������������ � �. 1 ��. 132 ����������� ������� ���� ������ ���������� ���������� � ������������� ����������� ���������� ����������� � ���� �� ��������� ���������� �������. �� ������� ����, ��������� ���������� ����������, ���������� ����� ��������������� ������ ���������� ������������� (� ��� ����� � ����� ������������ ���������) � ��������� ���������� ���������� � ������������� ����������� ����������.</P>
<P align=justify>� ������� �������� ���������� ���������� ���������� ����������, ����������������� � ������, ������� ����������� �������� (�. 3 ��. 132 ����������� �������).</P>
<P align=justify>������� ��������, ��� ����� 4 ��. 23 ������������ ������ "� �������� ������������� � �������� ��������" �� �������� ������������ ���������� � ����������, ���������� ��������������� ��� ������ ������� � ���������� ���������� ���������� ���������.</P>
<P align=justify>��������� &#8470; 258-� ����� �� �������� ����, ��������������� ������������ ���������� � �������������� ����������, �������������� ���������� � ���� ��.</P>
<P align=justify>�������� ����������, �� ������ ������������, ��� ����� ���������� ��������� &#8470; 258-� � �������� ���������, ��������������� ����� ������� � ���������� ���������� ���������� ���������, ����� �������������� ����� ���������, ���������� ����������� �������� � ������ ������� � ���������� ���������� ���������� ���������, � ��� ����� ����������� �������� ���������� ���������� (����� &#8211; ���) � ������� �� �������� ���������� �������.</P>
<P align=justify>��� ������� � ��������� ����������, �������������� ���� ������ ������� � ���������� ���������� ���������� ���������, ���������� � ������������� ������� �� �������, � ������� ������������ ������� ���������� ����� �������� ����� ��������������� ������� ���������� ��������� (����� &#8211; ����������� �������), � ����� ��� ������� � ��� ������� ����������� ������� � ���� ������������ ������ ������� �������� ������ ��������� � ����� 1 ������� � �������������� ���������� ���� ������������ ������ ������� �� ��������� ���������� ���� ������������� ����������� �������.</P>
<P align=justify>��� ���������� � ��� ��������� ������� � ���� ������������ ������ ������� ���� ������������� �������� �� ����������� ������ ��������� �������������, � ������ ������������ ��������� ���������� ��������, � �������� ���� ������ ������� � ���������� ���������� ���������� ��������� � ����� 1 ������� � �������������� ����������, �������������� � ���� ��, ����� ���� ������� ����, ������������� � ������ "������ ��������" (����� "�" ���).</P>
<P align=justify>������������� ��������, ��� ������ � ������������ ������������ ���������������� ���������� ��������� �. 2.4 ��������� &#8470; 258-� � ����� ����� ������������� ���������� � ���� �� ����������, �������������� ����� ������� � ���������� ���������� ���������� ���������, ��� ��������� ������������ ���������� ���� ���������� ��������� (������� ���������� ���� ���������� ��������� �� 20.12.2005<BR>&#8470; ����05-1149), ������� �������, ��� ������������ ����� ��������� &#8470; 258-� ����������� ������������ ���������������� ���������� ��������� �� ������������ � ��������� ��� ��������� ��� � ��������� ����� ������������� �� �������.</P>


</body>
</html>

