<!--
ID:37843408
TITLE:�� ������ �������� �� ������������� ���������� ��������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:12
ISSUE_DATE:2007-06-30
RECORD_DATE:2007-06-27


-->
<html>
<head>
<title>
37843408-�� ������ �������� �� ������������� ���������� ��������
</title>
</head>
<body >


<P align=justify><FONT>� ����� � ������������� ���������� ����������� �������, ������������ ���������� ������������, � ����������� ����� ����� ������, �������� ������������� ���������� ��� � ������� �������, ���� ������ ������� ��� ����������� �������� �� ������������� ���������� �������� � ��������� ������ � ���:</FONT></P>
<TABLE cellSpacing=2 cellPadding=7 width=610 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="7%">
<P align=center><FONT>&#8470;</FONT></P>
<P align=center><FONT>�/�</FONT></P></TD>
<TD vAlign=center width="34%">
<P align=center><FONT>����, ���</FONT></P></TD>
<TD vAlign=center width="22%">
<P align=center><FONT>�����</FONT></P></TD>
<TD vAlign=top width="17%">
<P align=center><FONT>����</FONT></P>
<P align=center><FONT>�����������</FONT></P></TD>
<TD vAlign=top width="20%">
<P align=center><FONT>������������-��� �����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center><FONT>1</FONT></P></TD>
<TD vAlign=top width="34%">
<P><FONT>�������������</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>������</FONT></P></TD>
<TD vAlign=top width="17%">
<P align=center><FONT>13.11.1990</FONT></P></TD>
<TD vAlign=top width="20%">
<P align=center><FONT>741</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center><FONT>2</FONT></P></TD>
<TD vAlign=top width="34%">
<P><FONT>�����</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>������</FONT></P></TD>
<TD vAlign=top width="17%">
<P align=center><FONT>15.02.1995</FONT></P></TD>
<TD vAlign=top width="20%">
<P align=center><FONT>3218</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center><FONT>3</FONT></P></TD>
<TD vAlign=top width="34%">
<P><FONT>���������� ��������� ������������ ����</FONT></P></TD>
<TD vAlign=top width="22%">
<P align=center><FONT>������</FONT></P></TD>
<TD vAlign=top width="17%">
<P align=center><FONT>18.04.1995</FONT></P></TD>
<TD vAlign=top width="20%">
<P align=center><FONT>3248</FONT></P></TD></TR></TBODY></TABLE><I>
<P align=right><FONT>�����-������ ������������<BR></FONT><FONT>������� � ������������ ������<BR></FONT><FONT>����� ������ �� 24.05, 06.06.2007</FONT></P></I>


</body>
</html>

