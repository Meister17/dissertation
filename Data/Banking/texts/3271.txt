<!--
ID:35789897
TITLE:�������� ����������� ���� ���������� ���������� ������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:24
ISSUE_DATE:2005-12-31
RECORD_DATE:2006-01-26


-->
<html>
<head>
<title>
35789897-�������� ����������� ���� ���������� ���������� ������
</title>
</head>
<body >


<P align=center><FONT><STRONG>���� ��������� �� ������ 2006 �.</STRONG></FONT></P>
<TABLE cellSpacing=1 cellPadding=7 width="100%" align=center border=1>
<TBODY>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center><FONT>&#8470;<BR>�/�</FONT></P></TD>
<TD vAlign=center width="15%" height=28>
<P align=center><FONT>����</FONT></P></TD>
<TD vAlign=center width="41%" height=28>
<P align=center><FONT>���� ��������</FONT></P></TD>
<TD vAlign=top width="39%" height=28>
<P align=center><FONT>��������� ����������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center><FONT>1</FONT></P></TD>
<TD vAlign=center width="15%" height=28>
<P><FONT>13.01&#8211;14.01</FONT></P></TD>
<TD vAlign=center width="41%" height=28>
<P><FONT>�������-�������� �������� � ����� (�������-���������)</FONT></P></TD>
<TD vAlign=top width="39%" height=28>
<P><FONT>�������� ������� � ������������� ������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=43>
<P align=center><FONT>2</FONT></P></TD>
<TD vAlign=center width="15%" height=43>
<P><FONT>13.01&#8211;14.01</FONT></P></TD>
<TD vAlign=center width="41%" height=43>
<P><FONT>����������� ������ �� ��������������� ����������� (���������) �������, ���������� ���������� ����� � �������������� ���������� � ��������� ������������</FONT></P></TD>
<TD vAlign=top width="39%" height=43>
<P><FONT>������������ ������������� ������ ������������ �����, ��������� &#8211; ��������, ����������� ��������.</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=17>
<P align=center><FONT>3</FONT></P></TD>
<TD vAlign=center width="15%" height=17>
<P><FONT>16.01&#8211;18.01</FONT></P></TD>
<TD vAlign=center width="41%" height=17>
<P><FONT>����������� ��������� ����������</FONT></P></TD>
<TD vAlign=top width="39%" height=17>
<P><FONT>������������ � ���������� ��������� ������������� ������, ����� ��������� ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=17>
<P align=center><FONT>4</FONT></P></TD>
<TD vAlign=center width="15%" height=17>
<P><FONT>16.01&#8211;18.01</FONT></P></TD>
<TD vAlign=center width="41%" height=17>
<P><FONT>��������������� � ������������� ���� � �����</FONT></P></TD>
<TD vAlign=top width="39%" height=17>
<P><FONT>������� ����������, ������������ � ���������� ������, ������������ ����������������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center><FONT>5</FONT></P></TD>
<TD vAlign=center width="15%" height=28>
<P><FONT>18.01&#8211;20.01</FONT></P></TD>
<TD vAlign=center width="41%" height=28>
<P><FONT>���������� ����� ������ � ������������� ���������� ����������. ������������� ���������� ����� � ����</FONT></P></TD>
<TD vAlign=top width="39%" height=28>
<P><FONT>������������ � ���������� ����������� � ������������� ����������� �������� � ����������� ������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=17>
<P align=center><FONT>6</FONT></P></TD>
<TD vAlign=center width="15%" height=17>
<P><FONT>18.01&#8211;20.01</FONT></P></TD>
<TD vAlign=center width="41%" height=17>
<P><FONT>���������� ������ ���������� ������������</FONT></P></TD>
<TD vAlign=top width="39%" height=17>
<P><FONT>������������ � ����������� ������������� �����, ����� �� ���������� ����������� �������, ����������� ��������, �������� � ����������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center><FONT>7</FONT></P></TD>
<TD vAlign=center width="15%" height=28>
<P><FONT>19.01&#8211;20.01</FONT></P></TD>
<TD vAlign=center width="41%" height=28>
<P><FONT>������������� �������� ������: ����������������� ���������, �������� �������� � �������������</FONT></P></TD>
<TD vAlign=top width="39%" height=28>
<P><FONT>����������, ����������� ������������� ������������� �������� �����, ���������� ������� ����������������� ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=29>
<P align=center><FONT>8</FONT></P></TD>
<TD vAlign=center width="15%" height=29>
<P><FONT>19.01&#8211;21.01</FONT></P></TD>
<TD vAlign=center width="41%" height=29>
<P><FONT>��������� ������ � ������ ��������-��������� ������������</FONT></P></TD>
<TD vAlign=top width="39%" height=29>
<P><FONT>����������� ������������ ������������� </FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center><FONT>9</FONT></P></TD>
<TD vAlign=center width="15%" height=28>
<P><FONT>23.01&#8211;24.01</FONT></P></TD>
<TD vAlign=center width="41%" height=28>
<P><FONT>������� � ��������: ����������������, ���� ��������� ������ � ��������� ������������</FONT></P></TD>
<TD vAlign=top width="39%" height=28>
<P><FONT>������������ � ���������� ������������� ������, ������������ ��������� �������������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center><FONT>10</FONT></P></TD>
<TD vAlign=center width="15%" height=28>
<P><FONT>23.01&#8211;24.01</FONT></P></TD>
<TD vAlign=center width="41%" height=28>
<P><FONT>���������� ������������ � �������� ��������� �����</FONT></P></TD>
<TD vAlign=top width="39%" height=28>
<P><FONT>���� &#8211; ���������, ������������ ������������� � �������������� ������������� �����</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center><FONT>11</FONT></P></TD>
<TD vAlign=center width="15%" height=28>
<P><FONT>23.01&#8211;26.01</FONT></P></TD>
<TD vAlign=center width="41%" height=28>
<P><FONT>���������� �������� � ����� � �����. ��������������� ��������� �����������</FONT></P></TD>
<TD vAlign=top width="39%" height=28>
<P><FONT>������������ � ����������� ������������� ����������� �������� � ������, ����������� ���������� ������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=17>
<P align=center><FONT>12</FONT></P></TD>
<TD vAlign=center width="15%" height=17>
<P><FONT>24.01&#8211;27.01</FONT></P></TD>
<TD vAlign=center width="41%" height=17>
<P><FONT>�������� ������������� ������������ �����</FONT></P></TD>
<TD vAlign=top width="39%" height=17>
<P><FONT>������������ � ���������� ����������� ����� �����</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center><FONT>&#8470;<BR>�/�</FONT></P></TD>
<TD vAlign=center width="15%" height=28>
<P align=center><FONT>����</FONT></P></TD>
<TD vAlign=center width="41%" height=28>
<P align=center><FONT>���� ��������</FONT></P></TD>
<TD vAlign=top width="39%" height=28>
<P align=center><FONT>��������� ����������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center><FONT>13</FONT></P></TD>
<TD vAlign=center width="15%" height=28>
<P><FONT>24.01&#8211;25.01</FONT></P></TD>
<TD vAlign=center width="41%" height=28>
<P><FONT>������ ������������� ������������ � ������ ������������������ �����������-���������</FONT></P></TD>
<TD vAlign=top width="39%" height=28>
<P><FONT>������������ � ����������� ��������� � �������������� ������������� �����</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center><FONT>14</FONT></P></TD>
<TD vAlign=center width="15%" height=28>
<P><FONT>25.01&#8211;26.01</FONT></P></TD>
<TD vAlign=center width="41%" height=28>
<P><FONT>������� � ���������� � 2006 �.: �������������, ���������, �������� �������</FONT></P></TD>
<TD vAlign=top width="39%" height=28>
<P><FONT>������� ����������, � ����� ����������� ������������� ����� ������, ������������ ����������� ���������� ����� � ��������� � ����������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=17>
<P align=center><FONT>15</FONT></P></TD>
<TD vAlign=center width="15%" height=17>
<P><FONT>25.01&#8211;27.01</FONT></P></TD>
<TD vAlign=center width="41%" height=17>
<P><FONT>����������� ��������� ���������� ��������� � �����</FONT></P></TD>
<TD vAlign=top width="39%" height=17>
<P><FONT>������ � ������� �������������� ��������, ������������ � ����������� ������������� ������ �� ������� ��������� � �����</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center><FONT>16</FONT></P></TD>
<TD vAlign=center width="15%" height=28>
<P><FONT>26.01&#8211;28.01</FONT></P></TD>
<TD vAlign=center width="41%" height=28>
<P><FONT>���������� ������� ��������� ������������� � ��������� ����-����</FONT></P></TD>
<TD vAlign=top width="39%" height=28>
<P><FONT>������������ � ����������� �������������, �������������� �������� ��������, ������������� ��������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=28>
<P align=center><FONT>17</FONT></P></TD>
<TD vAlign=center width="15%" height=28>
<P><FONT>27.01&#8211;28.01</FONT></P></TD>
<TD vAlign=center width="41%" height=28>
<P><FONT>���������� ���������� ������������� ������� � ������������<BR>�����</FONT></P></TD>
<TD vAlign=top width="39%" height=28>
<P><FONT>������������ ������, �������������� ���������� ������� ������������� ���� &#8211; ����������� � ����� ������������, ������� ����������� � ������������ ����� ����������� ��������, ������, ������������� ������������ � ���������� ������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=18>
<P align=center><FONT>18</FONT></P></TD>
<TD vAlign=center width="15%" height=18>
<P><FONT>30.01</FONT></P></TD>
<TD vAlign=center width="41%" height=18>
<P><FONT>������������ �������� �� ��������� ������ �� ������</FONT></P></TD>
<TD vAlign=top width="39%" height=18>
<P><FONT>��������� ���������� � ��������������� ������������� �����, ������������� ����������� ��������, ����������� �����</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=17>
<P align=center><FONT>19</FONT></P></TD>
<TD vAlign=center width="15%" height=17>
<P><FONT>30.01&#8211;31.01</FONT></P></TD>
<TD vAlign=center width="41%" height=17>
<P><FONT>������ ����� � ������������ ���������</FONT></P></TD>
<TD vAlign=top width="39%" height=17>
<P><FONT>������������ � ���������� ������������� �� ������ � ������������ ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=17>
<P align=center><FONT>20</FONT></P></TD>
<TD vAlign=center width="15%" height=17>
<P><FONT>30.01&#8211;01.02</FONT></P></TD>
<TD vAlign=center width="41%" height=17>
<P><FONT>����������� ���������� ���������� ����� �����</FONT></P></TD>
<TD vAlign=top width="39%" height=17>
<P><FONT>���������� � ���������� ���������� �������������, ������� ����������, ������������� �������� ������������� �������, ���������� �� ����������� �������� � ����������� �����</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="6%" height=29>
<P align=center><FONT>21</FONT></P></TD>
<TD vAlign=center width="15%" height=29>
<P><FONT>30.01&#8211;01.02</FONT></P></TD>
<TD vAlign=center width="41%" height=29>
<P><FONT>����������� ����������� ���������� � ������ �����: ����� � ������� ��������, ������ �� ��������-�����</FONT></P></TD>
<TD vAlign=top width="39%" height=29>
<P><FONT>�����������, ���������� � ���������� ����������� � ������� ��������</FONT></P></TD></TR></TBODY></TABLE>
<P align=justify><FONT>��������, ���������� "����������" (*), �������� ����� � ������� �����. ���������� �� ����� ��� ���: www.ibdarb.ru</FONT></P><B><I>
<P align=justify>105187, ������, ��. ������������, 38<BR>( (095) 365-3105, 366-5302, 366-2351<BR>���� (095) 365-0107, 365-1107<BR>E-mail: </B></I><A href="mailto:info@ibdarb.ru"><B><I><A href="mailto:info@ibdarb.ruhttp"><FONT color=#000000>info@ibdarb.ru</FONT></B></I></A><BR><B><I><FONT color=#000000>http</FONT><FONT color=#000000>:</FONT> //www.ibdarb.ru</P></B></I>


</body>
</html>

