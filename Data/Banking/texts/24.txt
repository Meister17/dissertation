<!--
ID:36056101
TITLE:ALFA BANK - Commercial giant
SOURCE_ID:3240
SOURCE_NAME:Banker
AUTHOR:
NUMBER:960
ISSUE_DATE:2006-02-28
RECORD_DATE:2006-04-04


-->
<html>
<head>
<title>
36056101-ALFA BANK - Commercial giant
</title>
</head>
<body >


<P align=justify><FONT face="Times New Roman" size=3><B>AFTER SETTING UP </B>his own</FONT> <FONT face="Times New Roman" size=3>sales finance company in the Czech Republic and then travelling the world for eight years working for GE Capital, Petr Smida took up his post as Alfa Bank's CEO two years ago. Here he gives his first extensive interview to The <B>Banker.</B></FONT></P>
<P align=justify>- <FONT face="Times New Roman" size=3>How does your Czech background and international experience help with running one of Russia's biggest commercial banks?</FONT></P>
<P align=justify>-<FONT face="Times New Roman" size=3>&nbsp;My global experience definitely helped me to settle in Russia and distinguish what is culturally and what is generically true of the Russian market. If you travel to several countries and see the business models, after a while you can separate what is specific and what is general.</FONT></P>
<P align=justify>A<FONT face="Times New Roman" size=3>&nbsp;good engine is a good engine if it is made in Russia, Japan or the US. Whether the car is good depends on the platform you put it into. It is these platforms that support the business model.</FONT></P>
<P align=justify>-<FONT face="Times New Roman" size=3>&nbsp;Competition is running white hot in Russia's banking sector. Do the increasingly active foreign banks present a threat to the leading domestic banks?</FONT></P>
<P align=justify>-<FONT face="Times New Roman" size=3>&nbsp;All the foreign investors coming into Russia, all the initial public offerings that are coming out of Russia, the development of the debt capital market - all of this activity is creating a benchmark for the</FONT> <FONT face="Times New Roman" size=3>benchmarks. It is defining what is right and wrong and the Russian banks have to react to that.</FONT></P>
<P><FONT face="Times New Roman" size=3>Every year of Russian growth is equal to five or 10 years of evolution in developed markets. Sales finances took 70 years to develop in Europe and here it took two years; it is the same with credit cards.</FONT></P>
<P align=justify><FONT face="Times New Roman" size=3>The foreign competition is good for Russian banks. Russky Standart [the leading consumer finance bank] bought in international know-how, as did Home Credit [the number two consumer finance bank].</FONT></P>
<P align=justify><FONT face="Times New Roman" size=3>Russians have to react very quickly to catch up. Those that do not will be left behind. The frenetic pace puts a lot of strain on society and on our organisation. Our people are asking: why are we changing all the time?</FONT></P>
<P align=justify><FONT face="Times New Roman" size=3>But Russia is a vast territory and foreign banks' presence in the regions is basically zero.</FONT></P>
<P align=justify><FONT face="Times New Roman" size=3>Our competitive advantages are: being Russian, being able to read Russian balance sheets and being wired to the society. There are not many formal information resources, so being wired is very important for corporate business.</FONT></P>
<P align=justify>-<FONT face="Times New Roman" size=3>&nbsp;Retail banking has been the hot topic for the past two years but many banks are returning the focus to corporate banking. Why?</FONT></P>
<P align=justify>-<FONT face="Times New Roman" size=3>&nbsp;Retail banking is still a new area for most banks. Branch retail banking is not new, but debt finance and mortgage - which we started very recently - is new, and is doubling monthly.</FONT></P>
<P align=justify><FONT face="Times New Roman" size=3>Since we rolled out Alfa Express, we are now setting up branches in the regions. In terms of growth and</FONT> <FONT face="Times New Roman" size=3>profitability, the regional branches are already comparable with most branches in Moscow. In consumer finance, we now have more business in the regions than in Moscow. At the moment, we have people focused on branch banking and the individual products, but we have been building platforms so that they can be integrated seamlessly later on when it is possible.</FONT></P>
<P align=justify><FONT face="Times New Roman" size=3>However, the different products - such as consumer finance versus mortgages - are at very different stages of maturity. There is so much that banks can add to these products and they can afford to build up their risk data as they can take risks. Later there will be a squeeze and those without this experience will suffer.</FONT></P>


</body>
</html>

