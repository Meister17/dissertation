<!--
ID:37843407
TITLE:��������� � ���������� ��������� �����������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:12
ISSUE_DATE:2007-06-30
RECORD_DATE:2007-06-27


-->
<html>
<head>
<title>
37843407-��������� � ���������� ��������� �����������
</title>
</head>
<body >


<P align=justify><FONT>���������� ����������� (�����������) ������������ ������, �������� �������� �� ���������� ���������� ��������, ����������, ��� � ������������ � ��������� ������������ ����� �������� ���������� ��� ��������� ��������������� (���������) ����������� ����� � ��������� ���������������� (����������) �������� ���������� ��������� ��������� �����������:</FONT></P>
<TABLE cellSpacing=2 cellPadding=7 width=100% align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="7%">
<P align=center><FONT>&#8470;</FONT></P>
<P align=center><FONT>�/�</FONT></P></TD>
<TD vAlign=top width="23%">
<P><FONT>������������</FONT></P>
<P><FONT>���������</FONT></P>
<P><FONT>�����������</FONT></P></TD>
<TD vAlign=top width="13%">
<P align=center><FONT>��������-������� �����</FONT></P></TD>
<TD vAlign=top width="16%">
<P align=center><FONT>����</FONT></P>
<P align=center><FONT>������ ��������</FONT></P></TD>
<TD vAlign=top width="42%">
<P><FONT>�����, �������, �.�.�. ����������� ������������ (�����������).</FONT></P>
<P><FONT>���� ������������ ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center><FONT>1</FONT></P></TD>
<TD vAlign=top width="23%">
<P><FONT>�������</FONT></P></TD>
<TD vAlign=top width="13%">
<P align=center><FONT>734</FONT></P></TD>
<TD vAlign=top width="16%">
<P align=center><FONT>05.04.2007</FONT></P></TD>
<TD vAlign=top width="42%">
<P><FONT>109240, ������, ������� ��������� �����, 4;</FONT></P>
<P><FONT>���������� ���������� �� ������:</FONT></P>
<P><FONT>367025, ���������� ��������, ���������, �������� ������ ���������, 39;</FONT></P>
<P><FONT>���. 8-800-200-08-05;</FONT></P>
<P><FONT>��������� �� ����������� �������;</FONT></P>
<P><FONT>60 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center><FONT>2</FONT></P></TD>
<TD vAlign=top width="23%">
<P><FONT>���</FONT></P></TD>
<TD vAlign=top width="13%">
<P align=center><FONT>2770</FONT></P></TD>
<TD vAlign=top width="16%">
<P align=center><FONT>28.03.2007</FONT></P></TD>
<TD vAlign=top width="42%">
<P><FONT>109240, ������, ������� ��������� �����, 4;</FONT></P>
<P><FONT>���������� ���������� �� ������:</FONT></P>
<P><FONT>109052, ������, �/� 48;</FONT></P>
<P><FONT>���. 8-800-200-08-05;</FONT></P>
<P><FONT>��������� �� ����������� �������;</FONT></P>
<P><FONT>60 ����</FONT></P></TD></TR></TBODY></TABLE><I>
<P align=right><FONT>�����������</FONT></P></I>
<TABLE cellSpacing=2 cellPadding=7 width=100% align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="7%">
<P align=center><FONT>&#8470;</FONT></P>
<P align=center><FONT>�/�</FONT></P></TD>
<TD vAlign=top width="23%">
<P><FONT>������������</FONT></P>
<P><FONT>���������</FONT></P>
<P><FONT>�����������</FONT></P></TD>
<TD vAlign=top width="13%">
<P align=center><FONT>��������-������� �����</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>����</FONT></P>
<P align=center><FONT>������ ��������</FONT></P></TD>
<TD vAlign=top width="42%">
<P><FONT>�����, �������, �.�.�. ����������� ������������ (�����������).</FONT></P>
<P><FONT>���� ������������ ���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center><FONT>3</FONT></P></TD>
<TD vAlign=top width="23%">
<P><FONT>����</FONT></P></TD>
<TD vAlign=top width="13%">
<P align=center><FONT>3281</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>19.03.2007</FONT></P></TD>
<TD vAlign=top width="42%">
<P><FONT>109240, ������, ������� ��������� �����, 4;</FONT></P>
<P><FONT>���������� ���������� �� ������:</FONT></P>
<P><FONT>109052, ������, �/� 48;</FONT></P>
<P><FONT>���. 8-800-200-08-05;</FONT></P>
<P><FONT>��������� �� ����������� �������;</FONT></P>
<P><FONT>60 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center><FONT>4</FONT></P></TD>
<TD vAlign=top width="23%">
<P><FONT>��������-������</FONT></P></TD>
<TD vAlign=top width="13%">
<P align=center><FONT>1952</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>02.11.2006</FONT></P></TD>
<TD vAlign=top width="42%">
<P><FONT>109240, ������, ������� ��������� �����, 4;</FONT></P>
<P><FONT>���������� ���������� �� ������:</FONT></P>
<P><FONT>109052, ������, �/� 48;</FONT></P>
<P><FONT>���. 8-800-200-08-05;</FONT></P>
<P><FONT>��������� �� ����������� �������;</FONT></P>
<P><FONT>60 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center><FONT>5</FONT></P></TD>
<TD vAlign=top width="23%">
<P><FONT>��������� ���� ��������</FONT></P></TD>
<TD vAlign=top width="13%">
<P align=center><FONT>2501</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>05.04.2007</FONT></P></TD>
<TD vAlign=top width="42%">
<P><FONT>109240, ������, ������� ��������� �����, 4;</FONT></P>
<P><FONT>���������� ���������� �� ������:</FONT></P>
<P><FONT>109052, ������, �/� 48;</FONT></P>
<P><FONT>���. 8-800-200-08-05;</FONT></P>
<P><FONT>��������� �� ����������� �������;</FONT></P>
<P><FONT>60 ����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center><FONT>6</FONT></P></TD>
<TD vAlign=top width="23%">
<P><FONT>��������</FONT></P></TD>
<TD vAlign=top width="13%">
<P align=center><FONT>2833</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>18.05.2007</FONT></P></TD>
<TD vAlign=top width="42%">
<P><FONT>107014, ������, ��. ���������, 13�/12, ���. 1;</FONT></P>
<P><FONT>���. 739-22-49, 268-32-12;</FONT></P>
<P><FONT>������� ��������� ����������;</FONT></P>
<P><FONT>2 ������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center><FONT>7</FONT></P></TD>
<TD vAlign=top width="23%">
<P><FONT>����-����</FONT></P></TD>
<TD vAlign=top width="13%">
<P align=center><FONT>3410</FONT></P></TD>
<TD vAlign=top width="15%">
<P align=center><FONT>11.05.2007</FONT></P></TD>
<TD vAlign=top width="42%">
<P><FONT>127018, ������, ��. ��������� ���, 16, ���. 4;</FONT></P>
<P><FONT>���. (499) 781-29-76;</FONT></P>
<P><FONT>������ ������ ����������;</FONT></P>
<P><FONT>2 ������</FONT></P></TD></TR></TBODY></TABLE><I>
<P align=right><FONT>"������� ����� ������", 2007, &#8470; 33</FONT></P></I>


</body>
</html>

