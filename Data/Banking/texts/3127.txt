<!--
ID:35451165
TITLE:��������� � �������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:16
ISSUE_DATE:2005-08-31
RECORD_DATE:2005-10-25


-->
<html>
<head>
<title>
35451165-��������� � �������
</title>
</head>
<body >


<P align=justify><I><FONT><STRONG>��� ��� ����������, �� ���������� ���������� ��� �.�.�������� ���������� ��������� � ��� �������� � ������� "�������� �����" � �������������� ������ � ���������� ����������� ��������������� � ��������. ���� ���� ��������� &#8211; ���������� ����� ���������� ���������� ������ �� ������������ ��������� �������������, ������������ � ��������� �������� ���������� ������� ������, � ������ ������� �� ������������ ������. ����� ��������� ��������� � 31 �������.</STRONG></FONT></P>
<P align=justify><FONT><STRONG>�� ���������� � �������� ���� ������������� (������������) � ������������� ��������������� ���������� ����� ������ ����������� ���������� �������� ������������ ������ � �������� �� ������� � ���������� �������, � ��� ����� ��������, ������� ������� �������� �� ����������� ������ � ������� ���.</STRONG></FONT></P></I>
<P align=justify><FONT></FONT><FONT>21 ���� 2005 �. ���������� ��������� ������ � �������� ������ ���������� ���� (������) �� ���� <B><I>"� ��������� ��������������������� ���������� ������ �������� � �������� �� ���� � �������� ���������".</P></B></FONT></I>
<P align=justify><FONT>�� ��������� � �������� "� ����� �� ���������� ��������� ��������� ��������� ��������������������� ������������ ���������� ������� ������" �������� ��������� ���������� ���������� ������ �.�.�������.</FONT></P>
<P align=justify><FONT>� ��������� ������� �������: �.�.��������, ������������ ������������� ����� ���������� ���� (������); �.�.��������, ����������� ��������� ������������ �������������� ������������ � ����������� ������������ ��������� ����������� ����� ������; �.�.����������, �������������� ����-��������� ���; �.�.������, �������� �� �������� ����; ������������ ������ � �������� ������ ���������� ���� (������); ������������� ������������ ���, ����� &#8211; ����� 30 ���.</FONT></P>
<P align=justify><FONT>��������� ��������� �������� ���������� �������� �������� ����������� ������� � �������, � ����� ���������� ����������� � ����, ������������ �� ��������� ��������������������� ������ ���������� ���� (������).</FONT></P>
<P align=justify><FONT>� ������������ ������������� ������ � �������� ����������, ��� � �������� ��������� ����������� �� ������� ������� ������������� ������ � ������ � ����������� �������� � �������� ��������. ������ ������� � ����� ������������ �����, ������������ ����� �������������, � ������ ����� �������� ���� ����. ��� � � ���� ������ ��������, ����������, ��� ���������� ������������ �� ������������ ������ ���������� ��� ����� ��������� &#8211; ������������� ������� ������� � ����� ������ �� ��������� ����, ���������������� ������������ ����������� ��� �������� ���������� � ���, ������ ����������� ���� ���������� �������� ��������� ���������� ����� ������ � ������������� ����������, �������������� ��������� ������ ������� ��������� �����������, ���������� ���� ���������� ���������� � �.�.</FONT></P>
<P align=justify><FONT>������� ������� ���������� ��������� ������� ����������� �.�.���������, ����������� ��������� ������������ �������������� ������������ � ����������� ������������ ��������� ����������� ����� ������. � ���������, �� �������� ����������� �� ��������� ����������� ������������ ������ � �������� �������������� ������� � �������� ���������� �������. �� ����� �������, ��� ������������ ����� ��������� ����� ��������������� � ��������� � ������� ����������� �������.</FONT></P>
<P align=justify><FONT>���������� ��������� ���� ������������ ����������� ������������� ���� ��������� �������, ���������� �� ���������� � ��� ������� ���������� ���������� ������. � ���������� �������� �.�.������, �������� �� �������� ����.</FONT></P>
<P align=justify><FONT>������������ ������ � �������� ���������� ���� (������) ������� ������� � ������������� ����������� ���������� ������� � ������������ ���������� � ������������ ������ ���������� ����������� �� �������� � ���������� ���������� �������.</FONT></P>
<P align=justify><FONT>�� ������ ��������� ������� ��������� (����������� ����).</FONT></P>
<P align=justify><FONT>��� ��������� ������ ��������� ������������� ���. ��������� ��� �.�.������� � �������� �� �������� ���� �.�.������ ���� �������� �������� �����������.</FONT></P>
<P align=justify><FONT>�� ����� ��������� �.������� ���������� ������� ���������� ��� �.�.�������� � ������ ������������ ������������ ��������� ��� "������� �� ����������� �������� � ����������� ������ ���������� ���� (������)" �.�.��������. �� ��������� ������� ��������� ���������� ���������� ��������� �������� ����� ����������� �������� � ����������� ������ ���������� ���� (������). ����� ��� ��� ���� ������������ ������� �� ���� ���� � ���������� "�������� ������".</FONT></P><B>
<P align=center><FONT>���������</FONT></P>
<P align=center><FONT>��������� ������������� ������<BR></FONT><FONT>� �������� ������ </FONT><FONT>���������� ���� (������)<BR></FONT><I><FONT>"� ��������� ��������������������� ���������� ������ ��������<BR></FONT><FONT>� �������� �� ���� � �������� ���������"</FONT></P></I>
<P align=justify><FONT>21 ���� 2005 �.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;�. ������</FONT></P></B>
<P align=justify><FONT>��������� ��������� �������� ��� ���������� ��������, ���������� ��������� ��������������������� ���������� ������ ��������, ���������� �� ����������� ��������.</FONT></P>
<P align=justify><FONT>��������� ��������� ��������, ��� ������ � �����������, ������������ � "��������� ��������� ��������������������� ������������ ���������� ������� ���������� ���������", �������� �� XVI ������ ���, ������������ ����� ������� ������ ��� ���������� ����������� �� ����������������� ���������� ������������ � ��������.</FONT></P><B>
<P align=justify><FONT>I. ��������� ��������� ������� �����������:</FONT></P></B>
<P align=justify><FONT>1. ����������� ���������� ���������� ���� (������) �� ������ ������� ����, ���������� � ���������, ����������� ���������� ����������� � ���� �� �� ����������, ������������ ��:</FONT></P>
<P align=justify>-<FONT>&nbsp;��������� ��������������������� ������ ������� � ������ ������� ���������-������������� ������������, ������� ���������� ��������� ��������� �������, ������������� �������� �������������, ���������-���������� ��������� ���������;</FONT></P>
<P align=justify>-<FONT>&nbsp;���������� �������� ���������� ������� �������, ���� � ���� ���������� �� ���� � ��������� ����������� ������������ � �������.</FONT></P>
<P align=justify><B><I><FONT>2. ���������� ���������� ������ ����� ������ ������������� � ���������� ���� (������) �������� ������� ��������� �������������� ���������� ���������� ������ � ����������� ���������� ���������� ���� (������) � ���� ���������� ��������� ��������� � �������.</FONT></P></B></I>
<P align=justify><FONT>3. ������� ������������ ���� ���������� ���� (������) ������� ���������� ��� ��������� ������������� ���� ��������� ������� � �������� ������������� ���� ��������� �������.</FONT></P>
<P align=justify><B><FONT>II. �������</FONT></B><FONT> <B>�������� ����������� ��������, </B>��������� ��������� ��������� ������������ ������� ���������� ����������� ��������� �������������� ����������� ������, �������� ��� �� ������������ �������� � ������ ���������� ������������ ������ ������������� �������, ������������� 26 ��� 2005 �. � �. ����������, ������������ ������� ������������ ������ "� �������� ��������� � ����������� ������ "� ������ � ���������� ������������", "� ����������������� (�����������) ��������� �����������".</FONT></P>
<P align=justify><FONT>� ������������ � ����� ����������� �������������� ���������� � ��������� ������������ ��� �� �������������� ����������� ��� �������� ��������� ������������� �������� �1 ���� 12% � ����� � ��������� ����������� �������� ��� �������� ������������ �������� ���������� ��������� ���� 10%.</FONT></P>
<P align=justify><B><FONT>III. ������� ������ �������� ������ ��������� �6.1, </FONT></B><FONT>��������� ���������<B> </B>�������<B>, </B>��� ���������� �������������� ������ ���� ���������� �� ��������� ������������ "��������� ���������" ����� �������� � ������ ������������ ����������� � ������ �������� ���������� ���������� ��������� ������ �������� � ���������� ��������������� � ������ �� ������������ �����.</FONT></P>
<P align=justify><B><I><FONT>��������� ��������� ������� ���������������� �������� ����������� ����� ������ �� ����������� ���������� �� �������������� �������� "��������� ���������".</FONT></I></B></P>
<P align=justify><B><I></B></I><B><FONT>��������� ��������� ������������ ����������� ���������� ���������� ������ �� ���������� �1 � �6.1, ��������� � ����������� ���� ��.</FONT></P>
<P align=justify><FONT>IV. ������� ��������, ��������� � ����������� ������������ ������ �� 22.05.2003 &#8470; 54-�� "� ���������� ����������-�������� ������� ��� ������������� �������� �������� �������� � �������������� ��������� ����", </FONT></B><FONT>��������� ��������� �������� �������� ���������� ���������� ������, ������������ �� ���������� ��������� ����������� �� ����� �������� ����� ������.</FONT></P>


</body>
</html>

