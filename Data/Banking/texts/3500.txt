<!--
ID:36498294
TITLE: ����� ������������ ����� ���������� ��������� �������� ������������ ��������� ������������ ����������� ������������� � ������� �.�.���������� (�� 12.05.2006 � 15-1-2-13/1484)
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:11
ISSUE_DATE:2006-06-15
RECORD_DATE:2006-08-02


-->
<html>
<head>
<title>
36498294- ����� ������������ ����� ���������� ��������� �������� ������������ ��������� ������������ ����������� ������������� � ������� �.�.���������� (�� 12.05.2006 � 15-1-2-13/1484)
</title>
</head>
<body >


<B>
<P align=justify><FONT>� ���������� ���������� &#8470; 124-�</FONT></P></B>
<P align=justify><FONT>����������� ����������� ������������� � ������� ���������� ������ ��� �� 26.01.2006 &#8470; �-02/2-53 � ������� ������� �������� ����������� ���������� ����� ������ �� 15.07.2005 &#8470; 124-� "�� ������������ �������� (�������) �������� �������� �������, �������� �� ������� � ������������ ������������� ������� �� �� ����������� ���������� �������������" (����� &#8211; ���������� &#8470; 124-�) � �������� ���������.</FONT></P>
<P align=justify><FONT>�� �������� ���-�����.</FONT></P>
<P align=justify><FONT>1. � ������������� ����� ������ ���������� �������� ������� �������������� �����-��������, ������������� ������������� ��������� �������� ������� ������������. ����� �������, ������������ (��������) ������ ���������� � �������������� ����� ������ �������� ��������� �����-�������� �� ���������� ������������ ������� (������������� �����������) �� ����������� ����� ������ �� ���������� ��������. � ����� � ���� � �����-�������� � ������� �������� ��� ���������, ��� � ����������� ����������� ����� ���������� ����������� ���������� ������������ �� ����������� �� ���� ���� � �������������� �������� ����, ������������� �������������� ������� �������� �������, � ������ ��������� ������������� (�������������� ��� ����������� ������) ������� �����������.</FONT></P>
<P align=justify><FONT>������ �� ����� �������������� �������������� �������� ����������� (��� ��������, ��� � ����������) � ������ ��� ���������� �������� ���������, �.�. �� ������ "&#8211;", � �������, ��������������� ������� ����� ������ �� 22.02.2006 &#8470; 28-�<BR>"� ������� ���������� �. 1.9.2 ���������� ����� ������ �� 15.07.2005 &#8470; 124-� "�� ������������ �������� (�������) �������� �������� �������, �������� �� ������� � ������������ ������������� ������� �� �� ����������� ���������� �������������" ("������� ����� ������" �� 1 ����� 2006 �. &#8470; 14), � ������ � ������� ������������� ����������������� �������� ��������� �����������-�������� ����������� � ����������� �������� ����������� ������������ ������������ ����� ������������. ��������������� ��������� �������������� ������ � ���������� &#8470; 124-�.</FONT></P>
<P align=justify><FONT>2. ����� ������������ ������� �������� �������� ������� �������� ����������� ���������� �������������� ������� ���������� �������� �� ���������� � ������������� ������, �������������� � ����������� �������, �� ���������� ��������� ��������� �����������. ���������� � ������������ ������� �� �������������� ��������� ����������� � ����������� ������ ������������ � ������������������ ����� ��������� ����������� � ����� ������ � ������������ ���������� (�� 1-� ����� ��������� ������) � ������ ��������������������� �������� ������������� ������������, ����������� �� �������� ������, �� �������������� ������ ������ ����� ����������� ����� �� ��������� � ����� �� ���� �������������. ��� ���� ������������� �������� ������ �������, ��������� � ���������� ����� ����������� ������ �� ��������� � �����, �� ���������� �� ������ ������� ��� �������� ��������� ����������� (�� ��������������� �������� � ���������� �������, � �����������/����������� ��������������� �� ������� �����, �������� � ����� ������) � � ���� ����� �� ��������� �� ��������������, �� �������������� ������� �� ���������� ��������� ��������� �����������.</FONT></P>
<P align=justify><FONT>� ����� � ���������� ������������ ������� ��������� �����������, ���������� �� ����� &#8470; 30204 "������������� ��������� ����������� �� ������ � ����������� ������, ������������� � ���� ������", �� ������� �������� � ������ ���.</FONT></P>
<P align=justify><FONT>�� �������� ��� "������������� ����������� ����".</FONT></P>
<P align=justify><FONT>1. ������� ������ �� ������� "����������" ������ ����� (��� ������� �������� ���� ������ � ��������), � ������������ � �������� �������������� ������ ������ ����� � ������ �� ����� ����� ������, �������������� �� ����, �������������� ���� ������ ������ �����, ������ ���������� � ������ ��� ��� �������� �������, �������� ������� ������� �� ��������� ����� ����������� ������. ��� ���� ������ ������ ������ ����������� � ������� ��� � ���� �� ���������� � �� ������� ������������ (�.�. ����������� ��������) �������� ������� � ������ ������ �����.</FONT></P>
<P align=justify><FONT>2. �� ������ ������������ �������������� ����� � ���������� ������� ������, ���������� �� ������������� ������, ���� ������������� ������� �� ���������, �� ����� ���� ������� � �������������� �����. �������������� ������ �������� �������� ������� � ��������� ����� ������� ������ ������ �������������� � ����� �������. ��� ����������� ������ �� ������ ���� �������� ���������� � ������������� �� ������� ������� ����������� � ������������� ������ � ���������� �� ���������� ������ 47407, 47408 "������� �� ������������� ��������� � ������� �������". �������, ���������� �� ��������� ������, ���������� � ������ ���.</FONT></P>
<P align=justify><FONT>3. ������� �� ��������� ������ �������� � ������ ��� � ���, ����� �������� ��������� ����� ������������ ������ �� �������� ��������� �����������, �������� �������� ����. �.�. � ��������� ������������, ����������� �� ������������ ���������, ������ ��� ������������� ����������<FONT face=Arial> </FONT>� ������ (�� �������) �������������� �������� �� ��������� ������.</FONT></P>


</body>
</html>

