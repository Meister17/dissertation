<!--
ID:37755331
TITLE:���������� ��������� ����������� � �����
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:11
ISSUE_DATE:2007-06-15
RECORD_DATE:2007-06-06


-->
<html>
<head>
<title>
37755331-���������� ��������� ����������� � �����
</title>
</head>
<body >


<B>
<P align=center><FONT>���������� �������� ����</FONT></P>
<P align=center><FONT>�������� ����������� � �����</FONT></P>
<P align=center><FONT>RUSSIAN ACADEMY OF SCIENCES</FONT></P>
<P align=center><FONT>INSTITUTE OF STATE AND LAW</FONT></P></B>
<P><FONT>119841 ������, ������, ��. ��������, 10 119841 Znamenka str. 10, Moscow, Russia<BR>���.: (495) 291 8816, 291 3381 tel.: 7 (495) 291 8816, 291 3381<BR>����: (495) 291 8574 fax: 7 (495) 291 8574<BR>E-mail: isl_ran@rinet.ru<BR>" 21 " ��� 2007 �.</FONT></P><B>
<P align=center><FONT>����������</FONT></P></B><I>
<P align=justify><FONT>� ����������� ������ ��������� �������� �� �������������� ���������� � ���� ��������� �������, � ����� �������� �������� ��������� ������� �� ��������� ���������� ������, ����������� ��� ��������� �������, � ����� � �������� � ����������� ������ "� ������������ ������" ����� �������� ������������ ������ �� ����� �������� �� ��������� ��� ������������ ������.</FONT></P></I>
<P align=justify><FONT>� ������������ � �. 1 ��. 3 ������������ ������ "� ������������ ������" ������������ ������ &#8211; ����� ����������, ����������� � ������������� ��� ������������� �� ��������� ����� ���������� ����������� ���� (�������� ������������ ������), � ��� ����� ��� �������, ���, ��������, ���, �����, ���� � ����� ��������, �����, ��������, ����������, ������������� ���������, �����������, ���������, ������, ������ ����������.</FONT></P>
<P align=justify><FONT>� ������������ �� ��. 4 ������������ ������ "� ��������� ��������" � ������ ��������� ������� ������ ��������� ������������ ������ �������� (�������� ��������� �������):</FONT></P>
<P align=justify><FONT>l �������, ���, ��������, ���� � ����� ��������;</FONT></P>
<P align=justify><FONT>l ������ �������� ��� ����� ���������, ��������������� �������� (�����, �����, ���� � ����� ������, ������������ � ��� ������, ��������� ������� ��� ���� ��������, �������������� ��������);</FONT></P>
<P align=justify><FONT>l ����������������� ����� �����������������;</FONT></P>
<P align=justify><FONT>l ��������� ����� ��������������� �������� �����, ��������� � ��������� ������������� ������������� ����������� �����������;</FONT></P>
<P align=justify><FONT>l �������� ����� ����������� � ������������ ����� ����������.</FONT></P>
<P align=justify><FONT>� ������������ � �. 1 �. 2 ��. 6 ������������ ������ "� ������������ ������" �������� �������� ������������ ������ �� ��������� � �������, ����� ��������� ������������ ������ �������������� �� ��������� ������������ ������, ���������������� �� ����, ������� ��������� ������������ ������ � ���� ���������, ������������ ������ ������� �������� ���������, � ����� ������������� ���������� ���������.</FONT></P>
<P align=justify><FONT>��������� ������������ ������, �������� � ������ ��������� �������, �������������� �� ��������� ������������ ������ "� ��������� ��������", ������� ������������� �� ����, ������� ��������� ������������ ������ � ���� ���������, ������������ ������ ������� �������� ���������, � ����� ���������� ���������� ���� ��������� �������, ������������ �������� ��������� �������, ���������� ������������ ��������� ������� � ������������� ��������� �������.</FONT></P>
<P align=justify><FONT>� ������������ �� ��. 2 ������������ ������ "� ��������� ��������" ��������� ����� ������������� ������� �������������� � ��������� ������ ���������� ������������ ������������� ��������� �������, � �� ������-���� ������� ��������, � ��� ����� ����������� ������� "� ������������ ������".</FONT></P>
<P align=justify><FONT>����� �������, ��������� ������������ ������ "� ������������ ������", ���������� ������������� ��������� �������� �������� ������������ ������ �� ��������� ��� ������������ ������ � �������������� ����������� ������ ���������� ��������, �� ����� ����������� � ����������, ����������� ����� ���������� � �������������, ������������ � ����������� ������, � ��� ����� � ��������������� �����������������, � (���) ������������ ������ �������� ����� (�������), ������� ������������ ������������� ����������� ������� "� ��������� ��������".</FONT></P>
<P align=justify><FONT>����� ����, ����� �������� �������� �� �������������� ���������� � ���� ��������� �������, � ����� ����� �������� �������� ��������� ������� �� ��������� ���������� �� ��������� ����������:</FONT></P>
<P align=justify><FONT>1. � ������������ � �. 1 ��. 6 ������������ ������ "� ������������ ������" ��������� ������������ ������ ����� �������������� ���������� � �������� ��������� ������������ ������, �� ����������� �������, ��������������� �. 2 ��������� ������. �������� �. 2 ��. 3 ���������� ������ ��� ���������� ���������� ��������������� �����, ������������� �����, ����������� ��� ���������� ����, ������������ � (���) �������������� ��������� ������������ ������, � ����� <I>������������ ���� � ���������� ��������� ������������ ������</I> (����. &#8211; ���.).</FONT></P>
<P align=justify><FONT>� ������������ � ����������� ������������ ������ "� ��������� ��������" �������� � ��������� ����������, �������� � ������ ��������� �������, ������:</FONT></P>
<P align=justify><FONT>l ��������� &#8211; ��������� ������������ ��������� ������� (��. 5);</FONT></P>
<P align=justify><FONT>l ��������� ��������� ������� &#8211; ������������ ��������� ������� (��. 6).</FONT></P>
<P align=justify><FONT>� ������������ � ����������� ������� "� ��������� ��������" �� �������� ������������ ��������� �������, �� ������������ ��������� ������� �� ������ ���������� �� ����, �� ���������� ������������, ���������, �������� � ��������� ����������, ��������������� ��������������� ���������� ���������� ����� ������������ �� ��������� ����� (�������). ���� � ���������� ��������� ������������ ���������� ��������������� ����������� ������� "� ��������� ��������".</FONT></P>
<P align=justify><FONT>����� �������, �� �������� ������������ ��������� �������, �� ������������ ��������� ������� �� �������� ����������� � ������ ��������� ������������ ������ "� ������������ ������", � ����� � ��� ��� �� ������ �������� �������� �������� �� ��������� ��� ������������ ������.</FONT></P>
<P align=justify><FONT>2. ������ ����������� �������������� �������� ������� � � ������� �������� �������� �������� ������������ ������ � �������� �������� (�������� ��������� �������). ���, � ������������ � �. 4 ��. 9 ������������ ������ "� ������������ ������" ���������� �������� �������� ������������ ������ �� ��������� ����� ������������ ������ ������ ���������� ������������ ������ ������ �������� � ����:</FONT></P>
<P align=justify><FONT>l ���� ��������� ������������ ������;</FONT></P>
<P align=justify><FONT>l �������� ������������ ������, �� ��������� ������� ������ �������� �������� ������������ ������;</FONT></P>
<P align=justify><FONT>l �������� �������� � ������������� �������, �� ���������� ������� ������ ��������, ����� �������� ������������ ���������� �������� ��������� ������������ ������;</FONT></P>
<P align=justify><FONT>l ����, � ������� �������� ��������� ��������, � ����� ������� ��� ������.</FONT></P>
<P align=justify><FONT>� ������� �� ����� ���������� ������������ ������ "� ��������� ��������" � �������� �������� �� �������������� � ���� ��������� ������� ����������, ���������� ��������� ������� ������� ��������, ������������ � ��. 5 ���������� ������, ����������� � ���, ��� ����� �������� ������ ���� ������������� �������������, ����� ���� �������� � �����, ����������� ���������� ���������� ��� ���������. ��� ���� ����������� ������ �������� �������� ����������� ������� "� ��������� ��������" �� �������������.</FONT></P>
<P align=justify><FONT>������������� ����������� ������� "� ��������� ��������" �������� ����� �������� �������� ��������� ������� �� ��������� ������������� ��������� ������� �������� ����� ��������� ������� ������� �������� ����� �� ��������������� ����������� ������ �������� �������� ���������. ���, �������� �. 9 ��. 6 ������������ ������ "� ��������� ��������" �������� ����� ��������� ������� ������������ ������������ ��������� ������� � ����������� ��� ���� �������� ������������� ���������������� �������� �������� ��������� �������. � ���� �������� ������ ���� ������� ������������ ������������ ��������� ������� � ���� ��� ����������. � ������������ � �. 10 ��������� ������ �������� �������� ��������� �������, ���������� ������������� ��������� �������, ��������� � ������� ������ ������ �� ��� ��� ����������. ��� ���� � ������������ � �. 11 ��������� ������ �������� �������� ��������� �������, ���������� ��������� ������������, ��������� ���� � ������� ����� ����� �������� �������� ����� (�������), ������������ � ��������� ��������� ��������� ������� � ������� �����, �������������� �. 10 ��������� ������. �� ��������� ����� �������� �������� �������� ��������� �������, �������������� �. 10 � 11 ��������� ������, ���� ��������� ������� �� ������ ���������� �������� ����� ��������� ������� ������������ ��������� �������, ����������� ��� ��������.</FONT></P>
<P align=justify><FONT>����� �������, � ������� �� �������� ������������ ������ ������� (������� ��������� �������) ����� ����������� ����������: ���� ��������� ������; �� ��������, �������� �������� � �������, ������� �� ���������, � ����� ����, � ������� �������� ��������� ��������.</FONT></P>
<P align=justify><FONT>3. ����� �������� ��������� ������� �� ����� ������ �������� �� ������������� � � ��. 8 ������������ ������ "� ��������� ��������", ���������� ������������� �������� ���� �������� ��������� ������� � ��������� ����� ��������� �������.</FONT></P>
<P align=justify><FONT>����� �������, ��������� ������������ ������, �������� � ������ ��������� �������, �������������� ������������� �� ��������� ������������ ������ "� ��������� ��������", ���������������� �� ����, ������� ��������� ������������ ������, ���� ���������, ������������ ������ ������� �������� ���������, � ����� ������������� ���������� ���� ��������� �������, ���������� ������������ ��������� �������, ������������� ��������� ������� � ������������ �������� ��������� �������.</FONT></P>
<P align=justify><FONT>� ����� � ���� � ������ ���������, ������������ ����������� ������� "� ��������� ��������", ��������� �������� �������� ������������ ������ (������������� � �������� �������� �� �������������� ���������� � ���� ��������� ������� � � �������� �������� ��������� ������� �� ��������� ������������� ��������� ������� �������� ����� ��������� �������), ����������� ������ �������� ������������� ����������� ������� "� ������������ ������", �� ���������.</FONT></P>
<P align=justify><FONT>��� ������� � ���������� ������ ������������ ������ "� ��������� ��������" � ������������ ������ "� ������������ ������". ���, ����� ������������ ������ "� ������������ ������" �������� ����������� ������ ���� �� ������������������ ������� �����, ������ � �������� ����� (��. 2). � ������� �� ����� ��������� ������ ������������ ������ "� ��������� ��������" �������� ��������� ������������ ���������� � ��������� �� ���� ������ �������� ��������� ������, � ����� ��������� ������������� ������ ��������� ����������� (�. 2 ��. 2).</FONT></P><B>
<P align=justify><FONT>������� ������� ���������<BR>������ ����������� � ����������� �����<BR>��������� ����������� � ����� ���,<BR>������ ����������� ���� �.�.�������</FONT></P></B>


</body>
</html>

