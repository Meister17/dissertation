<!--
ID:35601285
TITLE:��������� � ��������������� ����������� ��������� ����������� � ����� � �� �����������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:21
ISSUE_DATE:2005-11-15
RECORD_DATE:2005-12-01


-->
<html>
<head>
<title>
35601285-��������� � ��������������� ����������� ��������� ����������� � ����� � �� �����������
</title>
</head>
<body >


<P align=justify>�� ��������� ��������� ������������ ���������� ��� ������ ��������� ����� ������ � ����� ��������������� ����������� ��������� ����������� ������� ������:</P><FONT face=Wingdings size=2>
<P align=justify>l</FONT> � ���������� ��������� ��������� �����������:</P>
<TABLE cellSpacing=2 cellPadding=7 width=606 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="9%"><FONT size=3>
<P align=justify>&#8470;<BR>�/�</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P align=justify>������������<BR>��������� �����������</FONT></P></TD>
<TD vAlign=top width="28%"><FONT size=3>
<P align=justify>������</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P align=justify>��������������� �����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="9%"><FONT size=3>
<P align=justify>1</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P align=justify>�����</FONT></P></TD>
<TD vAlign=top width="28%"><FONT size=3>
<P align=justify>������</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P align=justify>2101</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="9%"><FONT size=3>
<P align=justify>2</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P align=justify>��������� ������</FONT></P></TD>
<TD vAlign=top width="28%"><FONT size=3>
<P align=justify>���������� ����</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P align=justify>1831</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="9%"><FONT size=3>
<P align=justify>3</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P align=justify>��������������</FONT></P></TD>
<TD vAlign=top width="28%"><FONT size=3>
<P align=justify>������</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P align=justify>1863</FONT></P></TD></TR></TBODY></TABLE><FONT face=Wingdings size=2>
<P align=justify>l</FONT> � ����������� ������������ ��������� ��������� �����������:</P>
<TABLE cellSpacing=2 cellPadding=7 width=606 align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="9%"><FONT size=3>
<P align=justify>&#8470;</P>
<P align=justify>�/�</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P align=justify>������������<BR>��������� �����������</FONT></P></TD>
<TD vAlign=top width="28%"><FONT size=3>
<P align=justify>������</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P align=justify>��������������� �����</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="9%"><FONT size=3>
<P align=justify>1</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P align=justify>��������-������</FONT></P></TD>
<TD vAlign=top width="28%"><FONT size=3>
<P align=justify>������</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P align=justify>30</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="9%"><FONT size=3>
<P align=justify>2</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P align=justify>�������� �������� ����</FONT></P></TD>
<TD vAlign=top width="28%"><FONT size=3>
<P align=justify>������</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P align=justify>2784</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="9%"><FONT size=3>
<P align=justify>3</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P align=justify>����������������</FONT></P></TD>
<TD vAlign=top width="28%"><FONT size=3>
<P align=justify>��������</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P align=justify>1898</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="9%"><FONT size=3>
<P align=justify>4</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=3>
<P align=justify>������</FONT></P></TD>
<TD vAlign=top width="28%"><FONT size=3>
<P align=justify>������</FONT></P></TD>
<TD vAlign=top width="23%"><FONT size=3>
<P align=justify>409</FONT></P></TD></TR></TBODY></TABLE>
<DIR><I>
<P align=right>"������� ����� ������", 2005, &#8470; 53</P></DIR></I>


</body>
</html>

