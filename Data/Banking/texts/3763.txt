<!--
ID:37036856
TITLE:��������� ���� DeltaCredit ������������� ����� �������� �� ����� ���������� ������������: ��� ������� ����� ����������� � ��������� &#150; � ���� ������!
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:23
ISSUE_DATE:2006-12-15
RECORD_DATE:2006-12-08


-->
<html>
<head>
<title>
37036856-��������� ���� DeltaCredit ������������� ����� �������� �� ����� ���������� ������������: ��� ������� ����� ����������� � ��������� &#150; � ���� ������!
</title>
</head>
<body >


<B>
<P align=justify>������������������ ���� DeltaCredit �������� ����� ��� ���������� ����� ������-�������: ���� �������� �������� ��������� ������� (������) �������� ����� ����� ���������� �������� �����-������� ��������, � �� ����� ��������������� ����������� ������, ��� ��� ������� �� �����. ����� �������� �������� ����������� �������� ������ �������� ���������� ������������ � ������.</P></B>
<P align=justify>����������� �������� �������� �� ����� ���������� ������������ ������ ��������������� ������� ����� ����������� � ��������� �������� ������ ����� ��������������� ����������� &#8211; �������, ������� ����� �������� �� 2 �� 6 ������. ��� ������� ������ ����� � ������� �������������� ������ ��������� ������ �� ������� ��������������� ����������� &#8211; ����� ����������� ���� � ����� � ���������� ������������ ����������� ���������������� (������� ������ ���� ���������).</P>
<P align=justify>�������� � ��������� ������� ��������� ��������� ����������� ��� ����� ����������������� � ����������� ������� &#8211; �������� ������������ �������� ������������� ����������� �� ��� ����������� ���� ��� "��������� �����������"; "��������� �����������" ���������� ����� �������������� �������� ��� ����������� �� ����������� ��� ��� ������� ������������; ����������� �������� ��������� ����������� ������� "�������", ����� ��������� "��������� �����������" ���������� �������� �������� (������ ����������� ����� ���������� ������ � ������� �������� �� ����� ������� � ��������� ������ ������������ ������); ����� ����� ��������� ������� �� ������������� � ���������� �������������, � ����� �������������� �������� �� �������� &#8211; ��� ��� ������� � ������������ ������ ������� ����� ��������������� �������� ���������� ����������� ����� � ������.</P>
<P align=justify>������, �� ������ ����� ������, ������������ ��������� ����� DeltaCredit, �� ������ �������� ����� ������� �� ������� �����-������� ������������ � ������� ������� ����������� ����� &#8211; �������� �������� �� ���������� ������ � ���� ������, � �� ����� 2&#8211;6 ������. ��� ����������� ���, ��� � ���, � �������, ����� � ������������� ������� �� ����� �������� ��������� �� ������� ��������������� �����������, � ����������� �������� ������������ �������� ���� &#8211; �� ������. ���� DeltaCredit ���� �� ����� ��� &#8211; ������� ����� ��������� � ���� ������ &#8211; ����� ���������� ��������� ����� ������� � ������ � �������. "�� ��������, ��� ���������� ������ ����� ����� ����� ������ ������ ���� ����� ������������ �������� � � ������", &#8211; ������� ����� �����.</P>
<P align=justify>���� �������, ����-��������� ����� DeltaCredit, �� �����-����������� �������� ��� ���� ������� ������������: "��������� ������ ���� ��� �� ����� ������������ ��� �������������� �����. ��� ����������� ����� ���������� ������ � 2-���������� ����������� ��������, �������� ������ �������� ����� ������� ����� ������, ��� �� ����� ����� �������: �� ������� �����". ���� ���� ����������� ������ �����, �� ��� ����� �������� ���������, ������� �� ������ �������� � "���������� ������������", ��� ��� �� ���������� ����� ����� �� �������� � ������� ����������� �������.</P>
<P align=justify>����� �������������� ����� �����������-��������� ��� ���������� �������� ��������� ������ �������, ���� DeltaCredit ������ ������������ ����������� ����������, � ����� ��������� � ���������� ������ �������� ����� �������� ��������� &#8211; ��������������� ����������� ���������, � �������� ���� �������� ����������. �������, ��� ���� ����� ����� �������� �������� ���� ����������� �� ���������� ������ � ��� �������� �������������� ������ ���������, ���������� ������ �������� � ����������������, ������� ��������������� �� ������������� ��������� ��������, ���������� ����������� ������, �������� �����-���������.</P>
<P align=justify>��� ������� ���������� �����, �������� ������������ ������������ � �������������� ������ ����� DeltaCredit, "���� ��������, ��� ���������� �������� ������ ����� ����� ����� ������ ������ �������, � ��� ������ ���� ��������� � ����������������; � ����� �������� �������� ���� ����� ����� ���������� ������ �� ��� ������� ����������� � ���������� ��������� ������, ���������� �������� �� ����� ������������ ������ ��� ������ ������������� ���������� � ����� ������� �������� ���������� ��� ���������� ����������� �����".</P><B>
<P align=justify>���������� � ������������������ ��������� ����� DeltaCredit (www.deltacredit.ru)</P></B>
<P align=justify>DeltaCredit �������� ����� �� ������� ��������� ������ � ������. ���� ���������� ������� ������� ��������� ��������, ���������� �� ��������� �������� ������� �������. DeltaCredit �������� ������ ������� ����������� �������. � ��������� �����, �� ������� �������������� ������������ ��������� Moody's Investors Service, ������� ����� DeltaCredit �� ������������ ����� ������ ������������� ������ � �������������� ������������ ��������� ������, ������� ����� ��������� �� ������ ���2. � 2006 �., �������� ������� ���� ����������� �������� "���������� ����", DeltaCredit ���� ����������� � ��������� "�������� ��������� ������������� ���� �� ����� ���������� ������������".</P><I>
<P align=justify>������, 16 ������ 2006 �.</P></I>


</body>
</html>

