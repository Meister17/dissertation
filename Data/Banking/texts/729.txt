<!--
ID:36423739
TITLE:���������� ���������� ���
SOURCE_ID:10030
SOURCE_NAME:�����: ������� ����. ����� ���
AUTHOR:�.�.��������, ����. ����. ����, ����� ���
NUMBER:3
ISSUE_DATE:2006-06-30
RECORD_DATE:2006-07-06


RBR_ID:1093
RBR_NAME:���������� ����
-->
<html>
<head>
<title>
36423739-���������� ���������� ���
</title>
</head>
<body >


<B>
<P align=center>��������: </B>International Financial Statistics.<B> </B>&#8211; Wash.: IMF, 2006. &#8211; Vol. 59, N 2. &#8211; P. 888-893.</P>
<TABLE cellSpacing=0 cellPadding=5 width=785 border=0>
<TBODY>
<TR>
<TD vAlign=top width="47%">&nbsp;</TD>
<TD vAlign=top width="11%"><B>
<P align=center>2000</B></P></TD>
<TD vAlign=top width="11%"><B>
<P align=center>2002</B></P></TD>
<TD vAlign=top width="11%"><B>
<P align=center>2003</B></P></TD>
<TD vAlign=top width="11%"><B>
<P align=center>2004</B></P></TD>
<TD vAlign=top width="11%"><B>
<P align=center>2005<BR>(������)</B></P></TD></TR>
<TR>
<TD vAlign=top width="47%" bgColor=#ffffff><B>
<P>�������� �����</P>
<P>Exchange Rates</B></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>�������� ���� ����� � ������� ���</P>
<P>(� ������� �� ������)</P>
<P>Rand per US Dollar, Principal Rate</P></TD>
<TD vAlign=top width="11%">
<P>6,93983</P></TD>
<TD vAlign=top width="11%">
<P align=right>10,54075</P></TD>
<TD vAlign=top width="11%">
<P align=right>7,56475</P></TD>
<TD vAlign=top width="11%">
<P align=right>6,45969</P></TD>
<TD vAlign=top width="11%">
<P align=right>6,67645</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������ ������������ ������������ ��������� �����. (������, ������������ �� ������ ����� ���� �����) (� ������� �� ������) (2000=100)</P>
<P>Nominal Effective Exchange Rate (Index Numbers)</P></TD>
<TD vAlign=top width="11%">
<P align=right>100,0</P></TD>
<TD vAlign=top width="11%">
<P align=right>67,5</P></TD>
<TD vAlign=top width="11%">
<P align=right>83,8</P></TD>
<TD vAlign=top width="11%">
<P align=right>91,8</P></TD>
<TD vAlign=top width="11%">
<P align=right>90,9 <FONT size=2>(������� 2005 �.)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%" bgColor=#ffffff><B>
<P>�������-��������� ������ (</B>����������������� ����� ������������ ����� � ������ ����������, ����������� ������� ���������� �������� (�������� �������, �������� ������������� �������� � ������� �������� � ���). (���. ������, �� ����� �������)</P><B>
<P>Monetary Authorities</B></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>����������� ������</P>
<P>Foreign Assets</P></TD>
<TD vAlign=top width="11%">
<P align=right>57094</P></TD>
<TD vAlign=top width="11%">
<P align=right>66072</P></TD>
<TD vAlign=top width="11%">
<P align=right>52991</P></TD>
<TD vAlign=top width="11%">
<P align=right>82903</P></TD>
<TD vAlign=top width="11%">
<P align=right>129232</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������������� ������������ �������������</P>
<P>Claims on Central Government</P></TD>
<TD vAlign=top width="11%">
<P align=right>10378</P></TD>
<TD vAlign=top width="11%">
<P align=right>16995</P></TD>
<TD vAlign=top width="11%">
<P align=right>18635</P></TD>
<TD vAlign=top width="11%">
<P align=right>16945</P></TD>
<TD vAlign=top width="11%">
<P align=right>14122</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������������� �������� �������</P>
<P>Claims on Private Sector</P></TD>
<TD vAlign=top width="11%">
<P align=right>598</P></TD>
<TD vAlign=top width="11%">
<P align=right>12</P></TD>
<TD vAlign=top width="11%">
<P align=right>14</P></TD>
<TD vAlign=top width="11%">
<P align=right>14</P></TD>
<TD vAlign=top width="11%">
<P align=right>14</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������������� ���������� ����������</P>
<P>Claims on Banking Institutions</P></TD>
<TD vAlign=top width="11%">
<P align=right>10356</P></TD>
<TD vAlign=top width="11%">
<P align=right>12415</P></TD>
<TD vAlign=top width="11%">
<P align=right>13146</P></TD>
<TD vAlign=top width="11%">
<P align=right>13047</P></TD>
<TD vAlign=top width="11%">
<P align=right>12712</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>��������� ������</P>
<P>Reserve Money</P></TD>
<TD vAlign=top width="11%">
<P align=right>43254</P></TD>
<TD vAlign=top width="11%">
<P align=right>56154</P></TD>
<TD vAlign=top width="11%">
<P align=right>62387</P></TD>
<TD vAlign=top width="11%">
<P align=right>76721</P></TD>
<TD vAlign=top width="11%">
<P align=right>95640</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<DIR><I>
<P>� ��� �����</I>: ������ ��� ������</P><I>
<P>of which</I>: Currency Outside Banks</P></DIR></TD>
<TD vAlign=top width="11%">
<P align=right>23724</P></TD>
<TD vAlign=top width="11%">
<P align=right>29219</P></TD>
<TD vAlign=top width="11%">
<P align=right>33718</P></TD>
<TD vAlign=top width="11%">
<P align=right>39084</P></TD>
<TD vAlign=top width="11%">
<P align=right>43222</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������ ������������� ����� ����������� ������������</P>
<P>Other Liabs. to Banking Insts.</P></TD>
<TD vAlign=top width="11%">
<P align=right>18629</P></TD>
<TD vAlign=top width="11%">
<P align=right>45530</P></TD>
<TD vAlign=top width="11%">
<P align=right>7442</P></TD>
<TD vAlign=top width="11%">
<P align=right>7701</P></TD>
<TD vAlign=top width="11%">
<P align=right>2922</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>����������� �������������</P>
<P>Foreign Liabilities</P></TD>
<TD vAlign=top width="11%">
<P align=right>20307</P></TD>
<TD vAlign=top width="11%">
<P align=right>22873</P></TD>
<TD vAlign=top width="11%">
<P align=right>20821</P></TD>
<TD vAlign=top width="11%">
<P align=right>21495</P></TD>
<TD vAlign=top width="11%">
<P align=right>24323</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>�������� ������������ �������������</P>
<P>Central Government Deposits</P></TD>
<TD vAlign=top width="11%">
<P align=right>4009</P></TD>
<TD vAlign=top width="11%">
<P align=right>2884</P></TD>
<TD vAlign=top width="11%">
<P align=right>8238</P></TD>
<TD vAlign=top width="11%">
<P align=right>8671</P></TD>
<TD vAlign=top width="11%">
<P align=right>31041</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>����� ����������� �������</P>
<P>Capital Accounts</P></TD>
<TD vAlign=top width="11%">
<P align=right>2476</P></TD>
<TD vAlign=top width="11%">
<P align=right>2917</P></TD>
<TD vAlign=top width="11%">
<P align=right>2524</P></TD>
<TD vAlign=top width="11%">
<P align=right>2285</P></TD>
<TD vAlign=top width="11%">
<P align=right>2392</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������ ������ (�����)</P>
<P>Other Items (Net)</P></TD>
<TD vAlign=top width="11%">
<P align=right>-10249</P></TD>
<TD vAlign=top width="11%">
<P align=right>-34864</P></TD>
<TD vAlign=top width="11%">
<P align=right>-16625</P></TD>
<TD vAlign=top width="11%">
<P align=right>-3964</P></TD>
<TD vAlign=top width="11%">
<P align=right>-239</P></TD></TR>
<TR>
<TD vAlign=top width="47%" bgColor=#ffffff><B>
<P>���������� ����������</P></B>
<P>(���. ������, �� ����� �������)</P><B>
<P>Banking Institutions</B></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>�������</P>
<P>Reserves</P></TD>
<TD vAlign=top width="11%">
<P align=right>19761</P></TD>
<TD vAlign=top width="11%">
<P align=right>26304</P></TD>
<TD vAlign=top width="11%">
<P align=right>28507</P></TD>
<TD vAlign=top width="11%">
<P align=right>32018</P></TD>
<TD vAlign=top width="11%">
<P align=right>38028</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>����������� ������</P>
<P>Foreign Assets</P></TD>
<TD vAlign=top width="11%">
<P align=right>41115</P></TD>
<TD vAlign=top width="11%">
<P align=right>88709</P></TD>
<TD vAlign=top width="11%">
<P align=right>142327</P></TD>
<TD vAlign=top width="11%">
<P align=right>138938</P></TD>
<TD vAlign=top width="11%">
<P align=right>148431</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������������� ������������ �������������</P>
<P>Claims on Central Government</P></TD>
<TD vAlign=top width="11%">
<P align=right>53546</P></TD>
<TD vAlign=top width="11%">
<P align=right>68369</P></TD>
<TD vAlign=top width="11%">
<P align=right>85979</P></TD>
<TD vAlign=top width="11%">
<P align=right>99519</P></TD>
<TD vAlign=top width="11%">
<P align=right>91102</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������������� �������� �������</P>
<P>Claims on Private Sector</P></TD>
<TD vAlign=top width="11%">
<P align=right>637212</P></TD>
<TD vAlign=top width="11%">
<P align=right>819137</P></TD>
<TD vAlign=top width="11%">
<P align=right>1035926</P></TD>
<TD vAlign=top width="11%">
<P align=right>1146153</P></TD>
<TD vAlign=top width="11%">
<P align=right>1266471</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>�������� �� �������������</P>
<P>Demand Deposits</P></TD>
<TD vAlign=top width="11%">
<P align=right>242244</P></TD>
<TD vAlign=top width="11%">
<P align=right>328047</P></TD>
<TD vAlign=top width="11%">
<P align=right>352627</P></TD>
<TD vAlign=top width="11%">
<P align=right>379842</P></TD>
<TD vAlign=top width="11%">
<P align=right>450426</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������� � �������������� ��������</P>
<P>Time and Savings Deposits</P></TD>
<TD vAlign=top width="11%">
<P align=right>232628</P></TD>
<TD vAlign=top width="11%">
<P align=right>351692</P></TD>
<TD vAlign=top width="11%">
<P align=right>417158</P></TD>
<TD vAlign=top width="11%">
<P align=right>489341</P></TD>
<TD vAlign=top width="11%">
<P align=right>570261</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>����������� ��������� �����</P>
<P>Money Market Instruments</P></TD>
<TD vAlign=top width="11%">
<P align=right>5869</P></TD>
<TD vAlign=top width="11%">
<P align=right>16652</P></TD>
<TD vAlign=top width="11%">
<P align=right>15467</P></TD>
<TD vAlign=top width="11%">
<P align=right>13500</P></TD>
<TD vAlign=top width="11%">
<P align=right>18813</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>����������� �������������</P>
<P>Foreign Liabilities</P></TD>
<TD vAlign=top width="11%">
<P align=right>67243</P></TD>
<TD vAlign=top width="11%">
<P align=right>57810</P></TD>
<TD vAlign=top width="11%">
<P align=right>66945</P></TD>
<TD vAlign=top width="11%">
<P align=right>66618</P></TD>
<TD vAlign=top width="11%">
<P align=right>78585</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>�������� ������������ �������������</P>
<P>Central Government Deposits</P></TD>
<TD vAlign=top width="11%">
<P align=right>30158</P></TD>
<TD vAlign=top width="11%">
<P align=right>22807</P></TD>
<TD vAlign=top width="11%">
<P align=right>49188</P></TD>
<TD vAlign=top width="11%">
<P align=right>62017</P></TD>
<TD vAlign=top width="11%">
<P align=right>64550</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>����� ����������� �������</P>
<P>Capital Accounts</P></TD>
<TD vAlign=top width="11%">
<P align=right>75581</P></TD>
<TD vAlign=top width="11%">
<P align=right>104453</P></TD>
<TD vAlign=top width="11%">
<P align=right>112846</P></TD>
<TD vAlign=top width="11%">
<P align=right>125535</P></TD>
<TD vAlign=top width="11%">
<P align=right>134552</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������ ������ (�����)</P>
<P>Other Items (Net)</P></TD>
<TD vAlign=top width="11%">
<P align=right>97911</P></TD>
<TD vAlign=top width="11%">
<P align=right>121059</P></TD>
<TD vAlign=top width="11%">
<P align=right>278509</P></TD>
<TD vAlign=top width="11%">
<P align=right>279775</P></TD>
<TD vAlign=top width="11%">
<P align=right>226844</P></TD></TR>
<TR>
<TD vAlign=top width="47%" bgColor=#ffffff><B>
<P>���������� ������� (</B>����������������� ���������� �� �������-��������� ������� � ���������� �����������). (��� ������, �� ����� �������)</P><B>
<P>Banking Survey</B></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>����������� ������ (�����)</P>
<P>Foreign Assets (Net)</P></TD>
<TD vAlign=top width="11%">
<P align=right>10658</P></TD>
<TD vAlign=top width="11%">
<P align=right>74099</P></TD>
<TD vAlign=top width="11%">
<P align=right>107553</P></TD>
<TD vAlign=top width="11%">
<P align=right>133728</P></TD>
<TD vAlign=top width="11%">
<P align=right>174755</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>���������� ������</P>
<P>Domestic Credit</P></TD>
<TD vAlign=top width="11%">
<P align=right>667567</P></TD>
<TD vAlign=top width="11%">
<P align=right>878822</P></TD>
<TD vAlign=top width="11%">
<P align=right>1083128</P></TD>
<TD vAlign=top width="11%">
<P align=right>1191943</P></TD>
<TD vAlign=top width="11%">
<P align=right>1276117</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<DIR>
<P>������������� ������������ ������������� (�����)</P>
<P>Claims on Central Government (Net)</P></DIR></TD>
<TD vAlign=top width="11%">
<P align=right>29757</P></TD>
<TD vAlign=top width="11%">
<P align=right>59673</P></TD>
<TD vAlign=top width="11%">
<P align=right>47188</P></TD>
<TD vAlign=top width="11%">
<P align=right>45776</P></TD>
<TD vAlign=top width="11%">
<P align=right>9633</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<DIR>
<P>������������� �������� �������</P>
<P>Claims on Private Sector</P></DIR></TD>
<TD vAlign=top width="11%">
<P align=right>637810</P></TD>
<TD vAlign=top width="11%">
<P align=right>819149</P></TD>
<TD vAlign=top width="11%">
<P align=right>1035940</P></TD>
<TD vAlign=top width="11%">
<P align=right>1146167</P></TD>
<TD vAlign=top width="11%">
<P align=right>1266485</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>�������� ��������</P>
<P>Money</P></TD>
<TD vAlign=top width="11%">
<P align=right>266184</P></TD>
<TD vAlign=top width="11%">
<P align=right>357470</P></TD>
<TD vAlign=top width="11%">
<P align=right>386681</P></TD>
<TD vAlign=top width="11%">
<P align=right>424288</P></TD>
<TD vAlign=top width="11%">
<P align=right>508119</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>�����������</P>
<P>Quasi-Money</P></TD>
<TD vAlign=top width="11%">
<P align=right>232628</P></TD>
<TD vAlign=top width="11%">
<P align=right>351692</P></TD>
<TD vAlign=top width="11%">
<P align=right>417158</P></TD>
<TD vAlign=top width="11%">
<P align=right>489341</P></TD>
<TD vAlign=top width="11%">
<P align=right>570261</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>����������� ��������� �����</P>
<P>Money Market Instruments</P></TD>
<TD vAlign=top width="11%">
<P align=right>5869</P></TD>
<TD vAlign=top width="11%">
<P align=right>16652</P></TD>
<TD vAlign=top width="11%">
<P align=right>15467</P></TD>
<TD vAlign=top width="11%">
<P align=right>13500</P></TD>
<TD vAlign=top width="11%">
<P align=right>18813</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>����� ����������� �������</P>
<P>Capital Accounts</P></TD>
<TD vAlign=top width="11%">
<P align=right>78057</P></TD>
<TD vAlign=top width="11%">
<P align=right>107370</P></TD>
<TD vAlign=top width="11%">
<P align=right>115370</P></TD>
<TD vAlign=top width="11%">
<P align=right>127820</P></TD>
<TD vAlign=top width="11%">
<P align=right>136945</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������ ������ (�����)</P>
<P>Other Items (Net)</P></TD>
<TD vAlign=top width="11%">
<P align=right>95487</P></TD>
<TD vAlign=top width="11%">
<P align=right>119737</P></TD>
<TD vAlign=top width="11%">
<P align=right>256005</P></TD>
<TD vAlign=top width="11%">
<P align=right>2
70722</P></TD>
<TD vAlign=top width="11%">
<P align=right>216735</P></TD></TR>
<TR>
<TD vAlign=top width="47%" bgColor=#ffffff><B>
<P>������������ ���������� ����������</P></B>
<P>(���. ������, �� ����� �������)</P><B>
<P>Nonbank Financial Institutions</B></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>�������� ������</P>
<P>Cash</P></TD>
<TD vAlign=top width="11%">
<P align=right>92369</P></TD>
<TD vAlign=top width="11%">
<P align=right>116047</P></TD>
<TD vAlign=top width="11%">
<P align=right>126595</P></TD>
<TD vAlign=top width="11%">
<P align=right>157696</P></TD>
<TD vAlign=top width="11%">
<P align=right>150895 <FONT size=2>(���� 2005)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������������� ������������ �������������</P>
<P>Claims on Central Government</P></TD>
<TD vAlign=top width="11%">
<P align=right>122381</P></TD>
<TD vAlign=top width="11%">
<P align=right>140650</P></TD>
<TD vAlign=top width="11%">
<P align=right>140751</P></TD>
<TD vAlign=top width="11%">
<P align=right>158337</P></TD>
<TD vAlign=top width="11%">
<P align=right>166702<SUP> </SUP><FONT size=2>(���� 2005)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������������� ��������������� �����������</P>
<P>Claims on Official Entities</P></TD>
<TD vAlign=top width="11%">
<P align=right>20541</P></TD>
<TD vAlign=top width="11%">
<P align=right>24751</P></TD>
<TD vAlign=top width="11%">
<P align=right>31694</P></TD>
<TD vAlign=top width="11%">
<P align=right>22980</P></TD>
<TD vAlign=top width="11%">
<P align=right>22997<SUP> </SUP><FONT size=2>(���� 2005)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������������� �������� �������</P>
<P>Claims on Private Sector</P></TD>
<TD vAlign=top width="11%">
<P align=right>595366</P></TD>
<TD vAlign=top width="11%">
<P align=right>627435</P></TD>
<TD vAlign=top width="11%">
<P align=right>682876</P></TD>
<TD vAlign=top width="11%">
<P align=right>796006</P></TD>
<TD vAlign=top width="11%">
<P align=right>854030 <FONT size=2>(���� 2005)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������������</P>
<P>Real Estate</P></TD>
<TD vAlign=top width="11%">
<P align=right>57507</P></TD>
<TD vAlign=top width="11%">
<P align=right>50131</P></TD>
<TD vAlign=top width="11%">
<P align=right>51936</P></TD>
<TD vAlign=top width="11%">
<P align=right>49265</P></TD>
<TD vAlign=top width="11%">
<P align=right>55034<SUP> </SUP><FONT size=2>(���� 2005)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������� ����� ������� (�� ������)</P>
<P>Incr. In Total Assets (Within Per.)</P></TD>
<TD vAlign=top width="11%">
<P align=right>61030</P></TD>
<TD vAlign=top width="11%">
<P align=right>-26727</P></TD>
<TD vAlign=top width="11%">
<P align=right>74838</P></TD>
<TD vAlign=top width="11%">
<P align=right>150432</P></TD>
<TD vAlign=top width="11%">
<P align=right>. . .</P></TD></TR>
<TR>
<TD vAlign=top width="47%" bgColor=#ffffff><B>
<P>���������� �������</B></P>
<P>(���. ������, �� ����� �������)</P><B>
<P>Financial Survey</B></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>����������� ������ (�����)</P>
<P>Foreign Assets (Net)</P></TD>
<TD vAlign=top width="11%">
<P align=right>10658</P></TD>
<TD vAlign=top width="11%">
<P align=right>74099</P></TD>
<TD vAlign=top width="11%">
<P align=right>107553</P></TD>
<TD vAlign=top width="11%">
<P align=right>133728</P></TD>
<TD vAlign=top width="11%">
<P align=right>155794<SUP> </SUP><FONT size=2>(���� 2005)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>���������� ������</P>
<P>Domestic Credit</P></TD>
<TD vAlign=top width="11%">
<P align=right>1405855</P></TD>
<TD vAlign=top width="11%">
<P align=right>1671658</P></TD>
<TD vAlign=top width="11%">
<P align=right>1938449</P></TD>
<TD vAlign=top width="11%">
<P align=right>2169266</P></TD>
<TD vAlign=top width="11%">
<P align=right>2266401 (<FONT size=2>���� 2005)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<DIR>
<P>������������� ������������ ������������� (�����)</P>
<P>Claims on Central Government (Net)</P></DIR></TD>
<TD vAlign=top width="11%">
<P align=right>152138</P></TD>
<TD vAlign=top width="11%">
<P align=right>200323</P></TD>
<TD vAlign=top width="11%">
<P align=right>187939</P></TD>
<TD vAlign=top width="11%">
<P align=right>204113</P></TD>
<TD vAlign=top width="11%">
<P align=right>192982 <FONT size=2>(���� 2005)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<DIR>
<P>������������� ��������������� �����������</P>
<P>Claims on Official Entities</P></DIR></TD>
<TD vAlign=top width="11%">
<P align=right>20541</P></TD>
<TD vAlign=top width="11%">
<P align=right>24751</P></TD>
<TD vAlign=top width="11%">
<P align=right>31694</P></TD>
<TD vAlign=top width="11%">
<P align=right>22980</P></TD>
<TD vAlign=top width="11%">
<P align=right>22997 <FONT size=2>(���� 2005)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<DIR>
<P>������������� �������� �������</P>
<P>Claims on Private Sector</P></DIR></TD>
<TD vAlign=top width="11%">
<P align=right>1233176</P></TD>
<TD vAlign=top width="11%">
<P align=right>1446584</P></TD>
<TD vAlign=top width="11%">
<P align=right>1718816</P></TD>
<TD vAlign=top width="11%">
<P align=right>1942173</P></TD>
<TD vAlign=top width="11%">
<P align=right>2050423 <FONT size=2>(���� 2005)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>��������� �������������</P>
<P>Liquid Liabilities</P></TD>
<TD vAlign=top width="11%">
<P align=right>406416</P></TD>
<TD vAlign=top width="11%">
<P align=right>593115</P></TD>
<TD vAlign=top width="11%">
<P align=right>677244</P></TD>
<TD vAlign=top width="11%">
<P align=right>755933</P></TD>
<TD vAlign=top width="11%">
<P align=right>856488 (<FONT size=2>���� 2005)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>����������� ��������� �����</P>
<P>Money Market Instruments</P></TD>
<TD vAlign=top width="11%">
<P align=right>5869</P></TD>
<TD vAlign=top width="11%">
<P align=right>16652</P></TD>
<TD vAlign=top width="11%">
<P align=right>15467</P></TD>
<TD vAlign=top width="11%">
<P align=right>13500</P></TD>
<TD vAlign=top width="11%">
<P align=right>10416<SUP> </SUP><FONT size=2>(���� 2005)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������ ������ (�����)</P>
<P>Other Items (Net)</P></TD>
<TD vAlign=top width="11%">
<P align=right>1004229</P></TD>
<TD vAlign=top width="11%">
<P align=right>1135990</P></TD>
<TD vAlign=top width="11%">
<P align=right>1353291</P></TD>
<TD vAlign=top width="11%">
<P align=right>1533561</P></TD>
<TD vAlign=top width="11%">
<P align=right>1555292 <FONT size=2>(���� 2005)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="47%" bgColor=#ffffff><B>
<P>���������� ������ </B>(������� ��������)</P><B>
<P>Interest Rates</B></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������� ������ (�� ����� �������)</P>
<P>Discount Rate</P></TD>
<TD vAlign=top width="11%">
<P align=right>12,00</P></TD>
<TD vAlign=top width="11%">
<P align=right>13,50</P></TD>
<TD vAlign=top width="11%">
<P align=right>8,00</P></TD>
<TD vAlign=top width="11%">
<P align=right>7,50</P></TD>
<TD vAlign=top width="11%">
<P align=right>7,00</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������ ��������� �����</P>
<P>Money Market Rate</P></TD>
<TD vAlign=top width="11%">
<P align=right>9,54</P></TD>
<TD vAlign=top width="11%">
<P align=right>11,11</P></TD>
<TD vAlign=top width="11%">
<P align=right>10,93</P></TD>
<TD vAlign=top width="11%">
<P align=right>7,15</P></TD>
<TD vAlign=top width="11%">
<P align=right>6,53</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������ �� ���������</P>
<P>Deposit Rate</P></TD>
<TD vAlign=top width="11%">
<P align=right>9,20</P></TD>
<TD vAlign=top width="11%">
<P align=right>10,77</P></TD>
<TD vAlign=top width="11%">
<P align=right>9,76</P></TD>
<TD vAlign=top width="11%">
<P align=right>6,55</P></TD>
<TD vAlign=top width="11%">
<P align=right>6,02</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������ �������� ��������</P>
<P>Lending Rate</P></TD>
<TD vAlign=top width="11%">
<P align=right>14,50</P></TD>
<TD vAlign=top width="11%">
<P align=right>15,75</P></TD>
<TD vAlign=top width="11%">
<P align=right>14,96</P></TD>
<TD vAlign=top width="11%">
<P align=right>11,29</P></TD>
<TD vAlign=top width="11%">
<P align=right>10,50</P></TD></TR>
<TR>
<TD vAlign=top width="47%" bgColor=#ffffff><B>
<P>������� ���������� ������� (���) </B>(���. ������)</P><B>
<P>Gross Domestic Product (GDP)</B></P></TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD>
<TD vAlign=top width="11%" bgColor=#ffffff>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>����� ���</P>
<P>Gross Domestic Product (GDP)</P></TD>
<TD vAlign=top width="11%">
<P align=right>922148</P></TD>
<TD vAlign=top width="11%">
<P align=right>1168778</P></TD>
<TD vAlign=top width="11%">
<P align=right>1257026</P></TD>
<TD vAlign=top width="11%">
<P align=right>1386658</P></TD>
<TD vAlign=top width="11%">
<P align=right>&#8230;</P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>����� ��� � ����� 2000 �.</P>
<P>GDP Volume 2000 Prices</P></TD>
<TD vAlign=top width="11%">
<P align=right>922148</P></TD>
<TD vAlign=top width="11%">
<P align=right>982327</P></TD>
<TD vAlign=top width="11%">
<P align=right>1011556</P></TD>
<TD vAlign=top width="11%">
<P align=right>1056771</P></TD>
<TD vAlign=top width="11%"><SUP>
<P align=right>&#8230;</SUP></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>������ ������ ��� (2000=100)</P>
<P>GDP Volume (2000=100)</P></TD>
<TD vAlign=top width="11%">
<P align=right>100,0</P></TD>
<TD vAlign=top width="11%">
<P align=right>106,5</P></TD>
<TD vAlign=top width="11%">
<P align=right>109,7</P></TD>
<TD vAlign=top width="11%">
<P align=right>114,6</P></TD>
<TD vAlign=top width="11%"><SUP>
<P align=right>&#8230;</SUP></P></TD></TR>
<TR>
<TD vAlign=top width="47%">
<P>�������� ��� (2000=100)</P>
<P>GDP Deflator (2000=100)</P></TD>
<TD vAlign=top width="11%">
<P align=right>100,0</P></TD>
<TD vAlign=top width="11%">
<P align=right>119,0</P></TD>
<TD vAlign=top width="11%">
<P align=right>124,3</P></TD>
<TD vAlign=top width="11%">
<P align=right>131,2</P></TD>
<TD vAlign=top width="11%"><SUP>
<P align=right>&#8230;</SUP></P></TD></TR></TBODY></TABLE>


</body>
</html>

