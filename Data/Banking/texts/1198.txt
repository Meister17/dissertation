<!--
ID:35352241
TITLE:����� ����������� ������� � ������ �������� 2005 ����
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:
NUMBER:5
ISSUE_DATE:2005-05-31
RECORD_DATE:2005-09-30


RBR_ID:2039
RBR_NAME:���������� �������
-->
<html>
<head>
<title>
35352241-����� ����������� ������� � ������ �������� 2005 ����
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgwhite>
<TD>
<P class=Main align=justify><EM>�� 1 ������ � ������� ����������� 664 ��������� �����������, � ��� ����� 649 � ������ � 15 &#8212; � ������� (�� 1 ������ &#8212; 671 ����, �� ������� 656 � ������� � 15 � ���������� �������).</EM> 
<P class=Main align=justify>� I �������� ���������� ������ ������ ����������� �������<SUP>1</SUP> ������� �� 4,8% � ��������� 4 176,7 ����. ������. �������� ����������� ������� � ������� �������� ���� ������������. ���� � ������&shy;������� ����� ����� �� ��������� 1%, �� � ����� �������� ����������� ��������� ����� �� 3%. 
<P class=Main align=justify>� �������� ����������������� ���������, �������������� �� �������� �����, ���� �������� ������� � �������� ��������� ����������� � ������� �������� ����������. ���, ���������� �������� ������������ ������� ��������� � ������, ����� ���������� �� ����. ����������� ���������� �� �������� �������� � ������ � �������, � � ����� ��������� ����������. ������ ���������� ������������ ������ � ������� ��� � ����� �� ������� �� ������� � ����� ��������� ���� �������� ������� � �������� (�� ��������� � ������� ���� �� ���� ��������� � 67,3 �� 65% � � 65,8 �� 65% ��������������). �������� ����� �������� ������� � ������� ��������� � ��������� � ����� ������. ����������� ����� ������������ ���������� ���, �������� ������&shy;������������ � ����������� ������. � �������� � �������� ���������� ����� ������� �� ������ �������� � ������ � ���� ��������� ����������� ��� � ������. 
<P class=Main align=justify>� ��������� <STRONG>��������</STRONG> ��������� ���� ���������� ����������� ������� &#8212; ��� ����� ��������� ���� ������������. � ����� ����������� ������� ��������� 15,7% ��������, ������������� &#8212; 79,9%. ���� �������� �� ��������� ������ �� ��������� � ������� ���� �� ���������� (4,4%). 
<P class=Main align=justify><STRONG>����������� �������</STRONG> ������ �� ������� ������� �� 3,7%, �������� 656,4 ����. ������. �� ���������� ����������� ���� ������ ����� �������� ��������� <STRONG>������ ����������� ����������</STRONG> ������������ ������ (� ������ ������� ���), ��������� � ������ ���� �� 17,2% � ���������� � 1 ������ 138,3 ����. ������. ��������������� � ������ �������� ����� ������� �������� <STRONG>�������</STRONG> � 27 ����. ������ �<STRONG> ������</STRONG> � ������� 1 ����. ������. <STRONG>���������� �������� ������� � �����</STRONG> ������ ����������� �������������, �� 0,6%, � ��������� 518,1 ����. ������. 
<P class=Main align=justify><STRONG>������� �� ��������� ������</STRONG> ������� �� 3,8%, �� 181,7 ����. ������ &#8212; � �������� �� ���� ���������� �������� �� ��������� ������ �� ������. 
<P class=Main align=justify><STRONG>�������������</STRONG> �������� �� 5,1% � �������� 3 338,5 ����. ������. � �� ��������� �������� ������� ����������� ���� ���������� ���� &#8212; ��� ���������� ���������� ��� ������ �������, ���������� �� ������ ��������� �����������, ������� �������� � ���������� ������ ����� ��� ��������� ����������� ��������. 
<P class=Main align=justify>����� ����� <STRONG>���������� ����</STRONG> ����������� ��������� ����� ���������� �������� (12,9% &#8212; �� 1 059,6 ����. ������). �������� ������� �������� �� �������� ����������� ���, � ���������� ���� �� ���� � ����� ����� ��������� ��������� 50%. ������� �� <STRONG>���������� ������ ����������� ��� </STRONG>����������� �� 15,2% (��� �� 70,2 ����. ������), �� 533 ����. ������. ��������� �������������� � ������� ������� ���������� ����� ������ ���������, � �������� ����������� ���&shy;������������ � ���������� ������� ��������� ��. 
<P class=Main align=justify>�������� ����������� ��������� ������� ��������� ����������� �� 11,6% &#8212; �� 307,9 ����. ������. �������� ������������ ����� ��������� ������������ ������� ����������� � 48,8 �� 43%. 
<P class=Main align=justify>���� ������� ��������� � ���������� ������� ������������ �������� �� ������ ���������� <STRONG>��������� ���������� ���</STRONG> � ������ �������. � ����� ���� ������� ���������� � ������ �� ���� �������� 10,7%, �� 526,7 ����. ������. � ������� �� ����������� ��������� �������� ������������ �������� ��������� &#8212; �� ���� ������� � 52,4 �� 54,5%. ������������ ��������� ���������� ��� � ������� ������ ���������� ���������� ���������� �������. ���, 12 ��������� ����������� � ��������� �� ���������� ������ ���������� ��� ����� 10 ����. ������ ������������ ����� 60% ���� �������, ������������ ������� �������. ������ ��� �� ��� &#8212; ����� �� 100&shy;���������� �������� ������������. 
<P class=Main align=justify><STRONG>�������� �� ������ ��������</STRONG> (����� ������) ������� ���� �� 2% � �� 1 ������ ��������� 888,6 ����. ������. �������� �������� ������ ������ ����� ����� ������������ �������� �������� ������� �� ������ �������� � �������. ��� ���� <STRONG>������� �� ��������� ������ ����������� </STRONG>����������� �� 9,5% &#8212; �� 644,4 ����. ������. 
<P class=Main align=justify>����� ������������� <STRONG>�� ������������� �����</STRONG> � ������ �������� ����������� �� ���������. ��������������, �� 0,1%, ���� ������ ������������� � ����������� �������, ������������ �� ������&shy;����������. ����� ����� ��� (� ������ �������� ����� ������) �� 1 ������ �������� 660,5 ����. ������. ���� ������������� � ������&shy;������������ ��������� ��������� &#8212; �� 69%. 
<P class=Main align=justify><STRONG>������� ������� �� ������ ������</STRONG> ������� �� 3,6% � �������� � ������ ������ 96,4 ����. ������. ��� � � ��������� �������, ������������ �� ������������� �����, ����� ����������� ���������� �������� �� ������ ������&shy;������������. 
<P class=Main align=justify>� ������ �������� ����������� �������� ���������� � �������� <STRONG>���������� ������ �����:</STRONG> �� ���� ����������� � 12,2 �� 11,5%. ��������, ������������ ������� � ������� ������� �����������, ����������� �� 1,4%, �� 479,7 ����. ������. �������� ��������, ���������� �� �������� ����������, ����� ���������� ������� ���������� ������������ � ���� ������ �������. � ��������� ���������� ������ ����� ������� ���� �������� (� 75 �� 79%) � ��������� (� 5,8 �� 6,5%) ��� ���������� ��������� ���� ���������� ������������ (� 18,3 �� 13,5%). 
<P class=Main align=justify>������������� ������� ����������� �������� ��� ������ ������� ����� ������������ � ���������� ������� �� ����� ������ �����. ��������� ������ ���������, ���� ������� �� ����������� �� ��������� � ������ ��������� �������� ���� ��������� ������������ �������. 
<P class=Main align=justify>� ������� <STRONG>���������� ��������</STRONG> ����������� ������ ������������� �� ���� ������� ���������. �������� ������� ����� ����� ��������� �� ������, �������� ������. �������� �������� ���� ����������� �� ���������� &#8212; ���� ������������ ���� ��������� 1,4% �� ������ ������. ����� ������� ������� �� ���������� � ���� ������������� ��������� �� �������� ���������� ����� &#8212; 3,7%. 
<P class=Main align=justify>���������� <STRONG>���������������� ������������</STRONG> ��� ������ ������� ��������� ����� (���� ���� ��������� � ������� ����������� � 4,5 �� 4,7%). ��������� ������������ ������� � ������������ ������, �� ���� � ����� ������ ��������, �������� ������ ��������� ���������, � 1 ������ �������� 67%. �� ��������������� ������������ ���������������� 51 ���� (���� ���� ��������� ��������� 20% �� �������). �������� ������� ������������� �� ��������������� �������� �������� � ������ �������� �� 6 ������, ��� �� ������� &#8212; �� 100&shy;���������� �������� ������������. � ����� ����� ��������, �������� ���������, �� ��������� � ������� ���� ������� �� 9,7%, �� 197 ����. ������. 
<P class=Main align=justify>����� ����� <STRONG>������������ ����������� </STRONG>��������� ���� ������ ���������� ������� � ��������� 4,6%, ������������ �� 1 725,5 ����. ������. �������� ��� � ������� ����, ��������������� ������������, ����������� �� ���������, ��������� �� ������ 41,3%. ���� �������� ���� ��������� ��������� &#8212; �� 66,8%. 
<P class=Main align=justify>� ������ �������� ����������� ���������� ���� �������������� ����� ��� ��������� ����������� �������, ��������������� � ������� �������� � �������&shy;�������������. �������� ��� � ���������� �������, �������� ���, ���������� � 8,3 �� 11,6%. �������� ������� ������������� ���������� �� �������� � ������, �������� ����������� ������������ � ������&shy;�������. � ����� ���������� ������������� �� ��������������� ��� ��������� 46,9%, ��������� 484,3 ����. ������. 
<P class=Main align=justify>�������� �� <STRONG>���������� �����</STRONG> ����� ���� ��������������� ������� � ���������� ��������. ���� �������� �������� ������ � ����� ����� �������� �������� ������� � 50 �� 55,7% ��� �������� ��������� ���� �������� ����������� � �����������. ����� ����� �������� �������� ����������� �� 11,5%, �� 180 ����. ������. 
<P class=Main align=justify>����� <STRONG>�������, ����������� ������� �� ����������������� ������ � ������ ������,</STRONG> �������� �� 11,2% � �������� 159,1 ����. ������. ���������� �� ���� � ������� ��������� ����� ������������ (� 4,5 �� 3,8%) �� ��������� � ������ ���������� ��������������� ������� �� ����� ���. 
<P class=Main align=justify>������������� ���������, ������������� � ������� �������� �� ����� <STRONG>������ �����,</STRONG> �������������� ������������ ������ �� ������ �������� ����������� �����. ���� �������� � ������ ������ �������� 8,5%, �� ���� � ������� ����������� � 10,5 �� 10,9%. �������, ��� ������������ ��������� �������� ���������� ��� ������ ��������������� ������ ����� �� ��������� �� ���������������, ���� ����� �������������� ��������, ���������� �������� ��. ���� ������� ���� �������� ������������ � �������� ������ ����� ������� � 22,9 �� 25,8%. �� ���� ����� ������� ��� � ��������� �������� ������ ����� ��� � �������� ��� ����� (� 22,5 �� 23,5%). 
<P class=Main align=justify>� ������ �������� ����������� ��������� ������ ������ ������� &#8212; ����� ����� �������� �� ��������� ��������� �����������, ��������� � ���� ����������� ������� � ����� ������, � ����� �������� ������� � ����������� ��������, ��������� �� 24,9% &#8212; �� 393,2 ����. ������. 
<P class=Main align=justify><SUP>1</SUP><EM> ������ ���������� �� 661 ������, ������������� ���������� ���������� � ���������� ��� ����� ������ �� 1 ������ 2005 �. � ��� ����� ����������� �� ��������� ������</EM> 
<P class=Main align=right><STRONG>�������� �����������<BR>������������ ��������<BR>�������������� ����������<BR>����������� ��� ����� ������</STRONG></P>
<P class=Main align=justify>
<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR>
<TD class=bgcolor1>
<P class=Rubrika align=center><STRONG>��������� �������� ������� ��������� ����������� ����������� �������, ���. ������</STRONG></P></TD></TR>
<TR>
<TD class=bgwhite>
<TABLE cellSpacing=0 cellPadding=0 width="100%" align=center border=1>
<TBODY>
<TR>
<TD width="48%">
<P align=center><STRONG>������</STRONG></P></TD>
<TD width="9%">
<P align=center><STRONG>01.01.2005</STRONG></P></TD>
<TD width="12%">
<P align=center><STRONG>01.04.2005</STRONG></P></TD>
<TD width="12%">
<P align=center><STRONG>��. ��� 01.01.05</STRONG></P></TD>
<TD width="9%">
<P align=center><STRONG>��. ��� 01.04.05</STRONG></P></TD>
<TD width="10%">
<P align=center><STRONG>��������<BR>�� I ������� 2005 ���� </STRONG></P></TD></TR>
<TR>
<TD>
<P>������ �������-�����</P></TD>
<TD>
<P align=right>3 984 295,5</P></TD>
<TD>
<P align=right>4 176 710,8</P></TD>
<TD>
<P align=right>100,0</P></TD>
<TD>
<P align=right>100,0</P></TD>
<TD>
<P align=right>104,8</P></TD></TR>
<TR>
<TD>
<P><STRONG>����������� ��������:</STRONG></P></TD>
<TD>
<P align=right><STRONG>633 124,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>656 447,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>15,9</STRONG></P></TD>
<TD>
<P align=right><STRONG>15,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>103,7</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�.: �������� ������� � ����� ������</P></TD>
<TD>
<P align=right>515 062,9</P></TD>
<TD>
<P align=right>518 133,7</P></TD>
<TD>
<P align=right>12,9</P></TD>
<TD>
<P align=right>12,4</P></TD>
<TD>
<P align=right>100,6</P></TD></TR>
<TR>
<TD>
<P>���������� ��������� ������������ (� ������ ������� ���)</P></TD>
<TD>
<P align=right>118 061,3</P></TD>
<TD>
<P align=right>138 314,0</P></TD>
<TD>
<P align=right>3,0</P></TD>
<TD>
<P align=right>3,3</P></TD>
<TD>
<P align=right>117,2</P></TD></TR>
<TR class=bgwhite>
<TD>
<P><STRONG>������� �� ��������� ������</STRONG></P></TD>
<TD>
<P align=right><STRONG>175 087,8</STRONG></P></TD>
<TD>
<P align=right><STRONG>181 742,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>4,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>4,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>103,8</STRONG></P></TD></TR>
<TR class=bgwhite>
<TD>
<P>� �.�. : ������� ��� ��������� ������ �� ������</P></TD>
<TD>
<P align=right>151 465,0</P></TD>
<TD>
<P align=right>156 775,2</P></TD>
<TD>
<P align=right>3,8</P></TD>
<TD>
<P align=right>3,8</P></TD>
<TD>
<P align=right>103,5</P></TD></TR>
<TR class=bgwhite>
<TD>
<P>������� ��� ��������� ������ �� ������ �������</P></TD>
<TD>
<P align=right>5 785,3</P></TD>
<TD>
<P align=right>5 712,5</P></TD>
<TD>
<P align=right>0,1</P></TD>
<TD>
<P align=right>0,1</P></TD>
<TD>
<P align=right>98,7</P></TD></TR>
<TR class=bgwhite>
<TD>
<P>������ �������</P></TD>
<TD>
<P align=right>17 837,6</P></TD>
<TD>
<P align=right>19 254,3</P></TD>
<TD>
<P align=right>0,4</P></TD>
<TD>
<P align=right>0,5</P></TD>
<TD>
<P align=right>107,9</P></TD></TR>
<TR>
<TD>
<P><STRONG>�������������:</STRONG></P></TD>
<TD>
<P align=right><STRONG>3 176 083,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>3 338 521,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>79,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>79,9</STRONG></P></TD>
<TD>
<P align=right><STRONG>105,1</STRONG></P></TD></TR>
<TR>
<TD>
<P>�������� ��������</P></TD>
<TD>
<P align=right>871 205,2</P></TD>
<TD>
<P align=right>888 549,6</P></TD>
<TD>
<P align=right>21,9</P></TD>
<TD>
<P align=right>21,3</P></TD>
<TD>
<P align=right>102,0</P></TD></TR>
<TR>
<TD>
<P>� �.�. : �������� �� ����.� ���.������ �����������</P></TD>
<TD>
<P align=right>588 345,0</P></TD>
<TD>
<P align=right>644 395,8</P></TD>
<TD>
<P align=right>14,8</P></TD>
<TD>
<P align=right>15,4</P></TD>
<TD>
<P align=right>109,5</P></TD></TR>
<TR>
<TD>
<P>������ �����</P></TD>
<TD>
<P align=right>165 762,8</P></TD>
<TD>
<P align=right>161 228,8</P></TD>
<TD>
<P align=right>4,2</P></TD>
<TD>
<P align=right>3,9</P></TD>
<TD>
<P align=right>97,3</P></TD></TR>
<TR>
<TD>
<P><STRONG>�������� (����� ������)</STRONG></P></TD>
<TD>
<P align=right><STRONG>938 305,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>1 059 638,8</STRONG></P></TD>
<TD>
<P align=right><STRONG>23,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>25,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>112,9</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�.: ���������</P></TD>
<TD>
<P align=right>475 546,9</P></TD>
<TD>
<P align=right>526 654,2</P></TD>
<TD>
<P align=right>11,9</P></TD>
<TD>
<P align=right>12,6</P></TD>
<TD>
<P align=right>110,7</P></TD></TR>
<TR>
<TD>
<P>����������� ���</P></TD>
<TD>
<P align=right>462 758,5</P></TD>
<TD>
<P align=right>532 984,6</P></TD>
<TD>
<P align=right>11,6</P></TD>
<TD>
<P align=right>12,8</P></TD>
<TD>
<P align=right>115,2</P></TD></TR>
<TR>
<TD>
<P>� �.�.: �������� �����������</P></TD>
<TD>
<P align=right>275 987,2</P></TD>
<TD>
<P align=right>307 880,7</P></TD>
<TD>
<P align=right>6,9</P></TD>
<TD>
<P align=right>7,4</P></TD>
<TD>
<P align=right>111,6</P></TD></TR>
<TR>
<TD>
<P>������ ��������</P></TD>
<TD>
<P align=right>186 771,4</P></TD>
<TD>
<P align=right>225 103,9</P></TD>
<TD>
<P align=right>4,7</P></TD>
<TD>
<P align=right>5,4</P></TD>
<TD>
<P align=right>120,5</P></TD></TR>
<TR>
<TD>
<P><STRONG>����� ������ </STRONG></P></TD>
<TD>
<P align=right><STRONG>93 085,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>96 395,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>2,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>2,3</STRONG></P></TD>
<TD>
<P align=right><STRO
NG>103,6</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�.: �������� ���������� ������</P></TD>
<TD>
<P align=right>49 235,6</P></TD>
<TD>
<P align=right>59 240,3</P></TD>
<TD>
<P align=right>1,2</P></TD>
<TD>
<P align=right>1,4</P></TD>
<TD>
<P align=right>120,3</P></TD></TR>
<TR>
<TD>
<P>�������� ������-������������</P></TD>
<TD>
<P align=right>17 217,9</P></TD>
<TD>
<P align=right>12 528,6</P></TD>
<TD>
<P align=right>0,4</P></TD>
<TD>
<P align=right>0,3</P></TD>
<TD>
<P align=right>72,8</P></TD></TR>
<TR>
<TD>
<P><STRONG>��� ����������, ������� ������������ �������������, ��� ������������ % (� ������ �������� ����� ������)</STRONG></P></TD>
<TD>
<P align=right><STRONG>659 968,2</STRONG></P></TD>
<TD>
<P align=right><STRONG>660 506,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>16,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>15,8</STRONG></P></TD>
<TD>
<P align=right><STRONG>100,1</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�. ��� ������-������������</P></TD>
<TD>
<P align=right>457 441,8</P></TD>
<TD>
<P align=right>455 530,7</P></TD>
<TD>
<P align=right>11,5</P></TD>
<TD>
<P align=right>10,9</P></TD>
<TD>
<P align=right>99,6</P></TD></TR>
<TR>
<TD>
<P>������ ������ ����������</P></TD>
<TD>
<P align=right>486 687,9</P></TD>
<TD>
<P align=right>479 663,5</P></TD>
<TD>
<P align=right>12,2</P></TD>
<TD>
<P align=right>11,5</P></TD>
<TD>
<P align=right>98,6</P></TD></TR>
<TR>
<TD>
<P>������ �������</P></TD>
<TD>
<P align=right>126 831,2</P></TD>
<TD>
<P align=right>153 767,5</P></TD>
<TD>
<P align=right>3,2</P></TD>
<TD>
<P align=right>3,7</P></TD>
<TD>
<P align=right>121,2</P></TD></TR>
<TR class=bgwhite>
<TD>
<P align=center><STRONG>�����</STRONG></P></TD>
<TD>
<P align=right></P></TD>
<TD>
<P align=right></P></TD>
<TD>
<P align=right></P></TD>
<TD>
<P align=right></P></TD>
<TD>
<P align=right></P></TD></TR>
<TR>
<TD>
<P><STRONG>������ �������-�����</STRONG></P></TD>
<TD>
<P align=right><STRONG>3 984 295,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>4 176 710,8</STRONG></P></TD>
<TD>
<P align=right><STRONG>100,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>100,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>104,8</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>������� � ������������ � ��� ������������� (� ������ ��������) ����� ��� ������������ ���������</STRONG></P></TD>
<TD>
<P align=right><STRONG>2 562 734,2</STRONG></P></TD>
<TD>
<P align=right><STRONG>2 838 954,9</STRONG></P></TD>
<TD>
<P align=right><STRONG>64,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>68,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>110,8</STRONG></P></TD></TR>
<TR>
<TD>
<P>����� �������� (����� ������)</P></TD>
<TD>
<P align=right>2 071 230,6</P></TD>
<TD>
<P align=right>2 174 302,6</P></TD>
<TD>
<P align=right>52,0</P></TD>
<TD>
<P align=right>52,1</P></TD>
<TD>
<P align=right>105,0</P></TD></TR>
<TR>
<TD>
<P>� �.�. ����� ���������</P></TD>
<TD>
<P align=right>179 687,7</P></TD>
<TD>
<P align=right>197 030,2</P></TD>
<TD>
<P align=right>4,5</P></TD>
<TD>
<P align=right>4,7</P></TD>
<TD>
<P align=right>109,7</P></TD></TR>
<TR>
<TD>
<P>����� ��. �����</P></TD>
<TD>
<P align=right>1 891 542,9</P></TD>
<TD>
<P align=right>1 977 272,5</P></TD>
<TD>
<P align=right>47,5</P></TD>
<TD>
<P align=right>47,3</P></TD>
<TD>
<P align=right>104,5</P></TD></TR>
<TR>
<TD>
<P>� �.�. ����� ������������</P></TD>
<TD>
<P align=right>1 650 150,2</P></TD>
<TD>
<P align=right>1 725 481,7</P></TD>
<TD>
<P align=right>41,4</P></TD>
<TD>
<P align=right>41,3</P></TD>
<TD>
<P align=right>104,6</P></TD></TR>
<TR>
<TD>
<P>������ ����� ��������</P></TD>
<TD>
<P align=right>241 392,7</P></TD>
<TD>
<P align=right>251 790,7</P></TD>
<TD>
<P align=right>6,1</P></TD>
<TD>
<P align=right>6,0</P></TD>
<TD>
<P align=right>104,3</P></TD></TR>
<TR>
<TD>
<P>������������ ������������� ���</P></TD>
<TD>
<P align=right>31 081,4</P></TD>
<TD>
<P align=right>35 557,1</P></TD>
<TD>
<P align=right>0,8</P></TD>
<TD>
<P align=right>0,9</P></TD>
<TD>
<P align=right>114,4</P></TD></TR>
<TR>
<TD>
<P>��� ��������</P></TD>
<TD>
<P align=right>329 672,7</P></TD>
<TD>
<P align=right>484 271,8</P></TD>
<TD>
<P align=right>8,3</P></TD>
<TD>
<P align=right>11,6</P></TD>
<TD>
<P align=right>146,9</P></TD></TR>
<TR>
<TD>
<P>� �.�. ��� ������-������������</P></TD>
<TD>
<P align=right>148 762,7</P></TD>
<TD>
<P align=right>287 879,6</P></TD>
<TD>
<P align=right>3,7</P></TD>
<TD>
<P align=right>6,9</P></TD>
<TD>
<P align=right>193,5</P></TD></TR>
<TR>
<TD>
<P>� �.�. ������������ �������������</P></TD>
<TD>
<P align=right>3 138,3</P></TD>
<TD>
<P align=right>3 125,9</P></TD>
<TD>
<P align=right>0,1</P></TD>
<TD>
<P align=right>0,1</P></TD>
<TD>
<P align=right>99,6</P></TD></TR>
<TR>
<TD>
<P>�������� �������</P></TD>
<TD>
<P align=right>161 470,5</P></TD>
<TD>
<P align=right>180 038,4</P></TD>
<TD>
<P align=right>4,1</P></TD>
<TD>
<P align=right>4,3</P></TD>
<TD>
<P align=right>111,5</P></TD></TR>
<TR>
<TD>
<P>� �.�. ������������</P></TD>
<TD>
<P align=right>846,1</P></TD>
<TD>
<P align=right>936,2</P></TD>
<TD>
<P align=right>0,0</P></TD>
<TD>
<P align=right>0,0</P></TD>
<TD>
<P align=right>110,6</P></TD></TR>
<TR>
<TD>
<P>������������ ������������� �����</P></TD>
<TD>
<P align=right>35 065,9</P></TD>
<TD>
<P align=right>39 619,2</P></TD>
<TD>
<P align=right>0,9</P></TD>
<TD>
<P align=right>0,9</P></TD>
<TD>
<P align=right>113,0</P></TD></TR>
<TR>
<TD>
<P><STRONG>������ � ������ ������������ ������ (������)</STRONG></P></TD>
<TD>
<P align=right><STRONG>360,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>342,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>94,9</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>������������ ��������</STRONG></P></TD>
<TD>
<P align=right><STRONG>2 821,2</STRONG></P></TD>
<TD>
<P align=right><STRONG>2 259,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>80,1</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>�������� � ������ ������</STRONG></P></TD>
<TD>
<P align=right><STRONG>419 185,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>454 789,2</STRONG></P></TD>
<TD>
<P align=right><STRONG>10,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>10,9</STRONG></P></TD>
<TD>
<P align=right><STRONG>108,5</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�. � ���������������</P></TD>
<TD>
<P align=right>95 935,5</P></TD>
<TD>
<P align=right>117 391,2</P></TD>
<TD>
<P align=right>2,4</P></TD>
<TD>
<P align=right>2,8</P></TD>
<TD>
<P align=right>122,4</P></TD></TR>
<TR>
<TD>
<P>�����</P></TD>
<TD>
<P align=right>94 443,1</P></TD>
<TD>
<P align=right>107 059,0</P></TD>
<TD>
<P align=right>2,4</P></TD>
<TD>
<P align=right>2,6</P></TD>
<TD>
<P align=right>113,4</P></TD></TR>
<TR>
<TD>
<P><STRONG>������������� �������</STRONG></P></TD>
<TD>
<P align=right><STRONG>19 957,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>25 997,9</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>130,3</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>���</STRONG></P></TD>
<TD>
<P align=right><STRONG>57 421,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>62 474,8</STRONG></P></TD>
<TD>
<P align=right><STRONG>1,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>1,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>108,8</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>��������, �������� �������� � ����������� �������</STRONG></P></TD>
<TD>
<P align=right><STRONG>702 879,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>552 209,9</STRONG></P></TD>
<TD>
<P align=right><STRONG>17,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>13,2</STRONG></P></TD>
<TD>
<P align=right><STRONG>78,6</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�. : �������� � ������</P></TD>
<TD>
<P align=right>179 105,2</P></TD>
<TD>
<P align=right>159 050,1</P></TD>
<TD>
<P align=right>4,5</P></TD>
<TD>
<P align=right>3,8</P></TD>
<TD>
<P align=right>88,8</P></TD></TR>
<TR>
<TD>
<P>� �. �. �������� � ������-������������</P></TD>
<TD>
<P align=right>126 733,8</P></TD>
<TD>
<P align=right>110 149,7</P></TD>
<TD>
<P align=right>3,2</P></TD>
<TD>
<P align=right>2,6</P></TD>
<TD>
<P align=right>86,9</P></TD></TR>
<TR>
<TD>
<P><STRONG>������� � ��������� ��������� �������</STRONG></P></TD>
<TD>
<P align=right><STRONG>23 033,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>24 827,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>107,8</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>���������, ����������� ��������</STRONG></P></TD>
<TD>
<P align=right><STRONG>65 703,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>71 544,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>1,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>1,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>108,9</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>������ ������</STRONG></P></TD>
<TD>
<P align=right><STRONG>130 559,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>143 653,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>3,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>3,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>110,0</STRONG></P></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></P></TD></TR></TBODY></TABLE>


</body>
</html>

