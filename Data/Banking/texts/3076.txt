<!--
ID:35367510
TITLE:������������� ���������� ���������-���������� �����
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:13
ISSUE_DATE:2005-07-15
RECORD_DATE:2005-10-04


-->
<html>
<head>
<title>
35367510-������������� ���������� ���������-���������� �����
</title>
</head>
<body >


<P align=center><FONT><STRONG>������ ���������<BR>�� ���� &#8211; ���� 2005 �.</STRONG></FONT></P>
<P align=center>
<TABLE cellSpacing=1 width=591 border=1>
<TBODY>
<TR>
<TD vAlign=center width="7%" height=56>
<P align=center>&#8470;<BR>�/�</P></TD>
<TD vAlign=center width="56%" height=56>
<P align=center>�������� ��������</P></TD>
<TD vAlign=center width="9%" height=56>
<P align=center>���� (���.<BR>���.)</P></TD>
<TD vAlign=center width="10%" height=56>
<P align=center>������������ ���� (���.<BR>���.)</P></TD>
<TD vAlign=center width="9%" height=56>
<P align=center>�����</P></TD>
<TD vAlign=center width="8%" height=56>
<P align=center>�����</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=33><B>
<P align=center>I</B></P></TD>
<TD vAlign=center width="56%" height=33><B>
<P>���������� ������</B></P></TD>
<TD vAlign=center width="9%" height=33>
<P></P></TD>
<TD vAlign=center width="10%" height=33>
<P></P></TD>
<TD vAlign=center width="9%" height=33>
<P></P></TD>
<TD vAlign=center width="8%" height=33>
<P></P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=42>
<P align=center>1</P></TD>
<TD vAlign=center width="56%" height=42>
<P><A href="file:///F:/COMMERCE/Ip_2005/bank%20standart/�����%20����%20�%20����������.doc"><FONT color=#0f0f0f>�������������� ���� � �������������� � ����� &#8211; ������������ ������</FONT></A></P></TD>
<TD vAlign=center width="9%" height=42>
<P align=center>10,0</P></TD>
<TD vAlign=center width="10%" height=42>
<P align=center>9,0</P></TD>
<TD vAlign=center width="9%" height=42>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=42>
<P align=center>28</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=36>
<P align=center>2</P></TD>
<TD vAlign=center width="56%" height=36>
<P><A href="file:///F:/COMMERCE/Ip_2005/bank%20standart/����������%20����.doc"><FONT color=#0f0f0f>���������� ���������� ����� �����</FONT></A></P></TD>
<TD vAlign=center width="9%" height=36>
<P align=center>15,0</P></TD>
<TD vAlign=center width="10%" height=36>
<P align=center>13,5</P></TD>
<TD vAlign=center width="9%" height=36>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=36>
<P align=center>14&#8211;16</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=34>
<P align=center>3</P></TD>
<TD vAlign=center width="56%" height=34>
<P><A href="file:///F:/COMMERCE/Ip_2005/bank%20standart/����������������%20�������.doc"><FONT color=#0f0f0f>���������������� ������� �����</FONT></A></P></TD>
<TD vAlign=center width="9%" height=34>
<P align=center>9,0</P></TD>
<TD vAlign=center width="10%" height=34>
<P align=center>7,9</P></TD>
<TD vAlign=center width="9%" height=34>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=34>
<P align=center>27</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=18><B>
<P align=center>II</B></P></TD>
<TD vAlign=center width="56%" height=18><B>
<P>������������ �������� � �������������<BR>����������</B></P></TD>
<TD vAlign=center width="9%" height=18>
<P align=center></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center></P></TD>
<TD vAlign=center width="9%" height=18>
<P></P></TD>
<TD vAlign=center width="8%" height=18>
<P align=center></P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=16 rowSpan=2>
<P align=center>1</P></TD>
<TD vAlign=center width="56%" height=16 rowSpan=2>
<P><A href="file:///F:/COMMERCE/Ip_2005/bank%20standart/����%20��%20�������.doc"><FONT color=#0f0f0f>���� ��������� "��������� ��������� ������"</FONT></A></P></TD>
<TD vAlign=center width="9%" height=16 rowSpan=2>
<P></P></TD>
<TD vAlign=center width="10%" height=16 rowSpan=2>
<P></P></TD>
<TD vAlign=center width="9%" height=16>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=16 rowSpan=2>
<P align=center>27&#8211;5</P></TD></TR>
<TR>
<TD vAlign=center width="9%" height=17>
<P align=center>����</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=37>
<P></P></TD>
<TD vAlign=center width="56%" height=37>
<P>1.1. ���������� �������������� ��������� ����� ��� ��������� ����������� ����� �������� � ����������� �� ������������</P></TD>
<TD vAlign=center width="9%" height=37>
<P align=center>5,9</P></TD>
<TD vAlign=center width="10%" height=37>
<P align=center>5,3</P></TD>
<TD vAlign=center width="9%" height=37>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=37>
<P align=center>27</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=28>
<P></P></TD>
<TD vAlign=center width="56%" height=28>
<P>1.2. ������������ ������� ����������� ���������� ������� � ������������ �����</P></TD>
<TD vAlign=center width="9%" height=28>
<P align=center>12,0</P></TD>
<TD vAlign=center width="10%" height=28>
<P align=center>10,4</P></TD>
<TD vAlign=center width="9%" height=28>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=28>
<P align=center>28&#8211;29</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=37>
<P></P></TD>
<TD vAlign=center width="56%" height=37>
<P>1.3. ������ �������� � �������� ���������� ���� ������������� �����</P></TD>
<TD vAlign=center width="9%" height=37>
<P align=center>5,9</P></TD>
<TD vAlign=center width="10%" height=37>
<P align=center>5,3</P></TD>
<TD vAlign=center width="9%" height=37>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=37>
<P align=center>30</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=36>
<P></P></TD>
<TD vAlign=center width="56%" height=36>
<P>1.4. ������� �������� ������ � ��������� �����: ��������, ��������� ��������� � ��������� ��������</P></TD>
<TD vAlign=center width="9%" height=36>
<P align=center>7,5</P></TD>
<TD vAlign=center width="10%" height=36>
<P align=center>6,8</P></TD>
<TD vAlign=center width="9%" height=36>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=36>
<P align=center>1</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=37>
<P></P></TD>
<TD vAlign=center width="56%" height=37>
<P>1. 5. ����������� ������������ ����������� ������ ������� � �������������� ����������������</P></TD>
<TD vAlign=center width="9%" height=37>
<P align=center>7,5</P></TD>
<TD vAlign=center width="10%" height=37>
<P align=center>6,8</P></TD>
<TD vAlign=center width="9%" height=37>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=37>
<P align=center>2</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=36>
<P></P></TD>
<TD vAlign=center width="56%" height=36>
<P>1.6. ������ ��������� � ������������ ������ �����</P></TD>
<TD vAlign=center width="9%" height=36>
<P align=center>7,5</P></TD>
<TD vAlign=center width="10%" height=36>
<P align=center>6,8</P></TD>
<TD vAlign=center width="9%" height=36>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=36>
<P align=center>4</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=38>
<P></P></TD>
<TD vAlign=center width="56%" height=38>
<P>1.7. ���������� �������� ����������: ���������� � �������� ������������ ���������� ����</P></TD>
<TD vAlign=center width="9%" height=38>
<P align=center>7,5</P></TD>
<TD vAlign=center width="10%" height=38>
<P align=center>6,8</P></TD>
<TD vAlign=center width="9%" height=38>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=38>
<P align=center>5</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=18><B>
<P align=center>III</B></P></TD>
<TD vAlign=center width="56%" height=18><B>
<P>��������� ��������</B></P></TD>
<TD vAlign=center width="9%" height=18>
<P align=center></P></TD>
<TD vAlign=center width="10%" height=18>
<P align=center></P></TD>
<TD vAlign=center width="9%" height=18>
<P></P></TD>
<TD vAlign=center width="8%" height=18>
<P align=center></P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=37>
<P align=center>1</P></TD>
<TD vAlign=center width="56%" height=37>
<P>���������� ������ ������������� ������������ � ������ ������������������ ��������</P></TD>
<TD vAlign=center width="9%" height=37>
<P align=center>16,0</P></TD>
<TD vAlign=center width="10%" height=37>
<P align=center>14,5</P></TD>
<TD vAlign=center width="9%" height=37>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=37>
<P align=center>20&#8211;22</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=20>
<P align=center>2</P></TD>
<TD vAlign=center width="56%" height=20>
<P>���������� ������ (������)</P></TD>
<TD vAlign=center width="9%" height=20>
<P align=center>5,9</P></TD>
<TD vAlign=center width="10%" height=20>
<P align=center>5,3</P></TD>
<TD vAlign=center width="9%" height=20>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=20>
<P align=center>15</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=34>
<P align=center>3</P></TD>
<TD vAlign=center width="56%" height=34>
<P>�������������� ��� ������� ��������� ���������� (���������)</P></TD>
<TD vAlign=center width="9%" height=34>
<P align=center>5,9</P></TD>
<TD vAlign=center width="10%" height=34>
<P align=center>5,3</P></TD>
<TD vAlign=center width="9%" height=34>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=34>
<P align=center>12</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=34>
<P align=center>4</P></TD>
<TD vAlign=center width="56%" height=34>
<P>���������� ���������� �������� ��� ������� ������� ���������� �����</P></TD>
<TD vAlign=center width="9%" height=34>
<P align=center>17,0</P></TD>
<TD vAlign=center width="10%" height=34>
<P align=center>15,0</P></TD>
<TD vAlign=center width="9%" height=34>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=34>
<P align=center>23&#8211;24</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=35>
<P align=center>&#8470;<BR>�/�</P></TD>
<TD vAlign=center width="56%" height=35>
<P align=center>�������� ��������</P></TD>
<TD vAlign=center width="9%" height=35>
<P align=center>���� (���.<BR>���.)</P></TD>
<TD vAlign=center width="10%" height=35>
<P align=center>������������ ���� (���.<BR>���.)</P></TD>
<TD vAlign=center width="9%" height=35>
<P align=center>�����</P></TD>
<TD vAlign=center width="8%" height=35>
<P align=center>�����</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=35><B>
<P align=center>IV</B></P></TD>
<TD vAlign=center width="56%" height=35><B>
<P>�������� � ����������� �������. ������������� �������</B></P></TD>
<TD vAlign=center width="9%" height=35>
<P align=center></P></TD>
<TD vAlign=center width="10%" height=35>
<P align=center></P></TD>
<TD vAlign=center width="9%" height=35>
<P></P></TD>
<TD vAlign=center width="8%" height=35>
<P align=center></P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=40>
<P align=center>1</P></TD>
<TD vAlign=center width="56%" height=40>
<P>������� ���� �� �������� ��������� ������������� � ��������� ��������</P></TD>
<TD vAlign=center width="9%" height=40>
<P align=center>12,0</P></TD>
<TD vAlign=center width="10%" height=40>
<P></P></TD>
<TD vAlign=center width="9%" height=40>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=40>
<P align=center>7</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=18><B>
<P align=center>V</B></P></TD>
<TD vAlign=center width="56%" height=18><B>
<P>����, ����������, �����</B></P></TD>
<TD vAlign=center width="9%" height=18>
<P></P></TD>
<TD vAlign=center width="10%" height=18>
<P></P></TD>
<TD vAlign=center width="9%" height=18>
<P></P></TD>
<TD vAlign=center width="8%" height=18>
<P></P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=37>
<P align=center>1</P></TD>
<TD vAlign=center width="56%" height=37>
<P>������������� ��������� ���������� ����������</P></TD>
<TD vAlign=center width="9%" height=37>
<P align=center>26,0</P></TD>
<TD vAlign=center width="10%" height=37>
<P align=center>23,5</P></TD>
<TD vAlign=center width="9%" height=37>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=37>
<P align=center>14&#8211;18</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=46>
<P align=center>2</P></TD>
<TD vAlign=center width="56%" height=46>
<P>������������� ���������� ������������� ����� �� ���� � ���� (���������)</P></TD>
<TD vAlign=center width="9%" height=46>
<P align=center>13,5</P></TD>
<TD vAlign=center width="10%" height=46>
<P align=center>12,0</P></TD>
<TD vAlign=center width="9%" height=46>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=46>
<P align=center>12&#8211;13</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=36>
<P align=center>3</P></TD>
<TD vAlign=center width="56%" height=36>
<P>����������� ����� �������� �� ����</P></TD>
<TD vAlign=center width="9%" height=36>
<P align=center>5,9</P></TD>
<TD vAlign=center width="10%" height=36>
<P align=center>5,3</P></TD>
<TD vAlign=center width="9%" height=36>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=36>
<P align=center>21</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=28>
<P align=center>4</P></TD>
<TD vAlign=center width="56%" height=28>
<P>���� ������ ����� �� ����</P></TD>
<TD vAlign=center width="9%" height=28>
<P align=center>7,4</P></TD>
<TD vAlign=center width="10%" height=28>
<P align=center>6,7</P></TD>
<TD vAlign=center width="9%" height=28>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=28>
<P align=center>8</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=27>
<P align=center>5</P></TD>
<TD vAlign=center width="56%" height=27>
<P>���������� ���������� ���������� �������� �� ����</P></TD>
<TD vAlign=center width="9%" height=27>
<P align=center>14,0</P></TD>
<TD vAlign=center width="10%" height=27>
<P align=center>12,6</P></TD>
<TD vAlign=center width="9%" height=27>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=27>
<P align=center>16&#8211;17</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=28>
<P align=center>6</P></TD>
<TD vAlign=center width="56%" height=28>
<P>����� ���������� ��������� ����������� �� ����</P></TD>
<TD vAlign=center width="9%" height=28>
<P align=center>12,0</P></TD>
<TD vAlign=center width="10%" height=28>
<P align=center>10,8</P></TD>
<TD vAlign=center width="9%" height=28>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=28>
<P align=center>4</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=26>
<P align=center>7</P></TD>
<TD vAlign=center width="56%" height=26>
<P>����� � ����</P></TD>
<TD vAlign=center width="9%" height=26>
<P align=center>10,0</P></TD>
<TD vAlign=center width="10%" height=26>
<P align=center>9,0</P></TD>
<TD vAlign=center width="9%" height=26>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=26>
<P align=center>11</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=38>
<P align=center>8</P></TD>
<TD vAlign=center width="56%" height=38>
<P>����������������� ���������� � ������������ ���������� �� ����. ���� ������� � ����������, ����������� ������������� �����</P></TD>
<TD vAlign=center width="9%" height=38>
<P align=center>18,0</P></TD>
<TD vAlign=center width="10%" height=38>
<P align=center>16,2</P></TD>
<TD vAlign=center width="9%" height=38>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=38>
<P align=center>14&#8211;15</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=19 rowSpan=2>
<P align=center>9</P></TD>
<TD vAlign=center width="56%" height=19 rowSpan=2>
<P>������� �� ������������� ���������� ��������� �����������</P></TD>
<TD vAlign=center width="9%" height=19 rowSpan=2>
<P align=center>32,0</P></TD>
<TD vAlign=center width="10%" height=19 rowSpan=2>
<P></P></TD>
<TD vAlign=center width="9%" height=19>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=19 rowSpan=2>
<P align=center>14-02</P></TD></TR>
<TR>
<TD vAlign=center width="9%" height=9>
<P align=center>����</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=22><B>
<P align=center>VI</B></P></TD>
<TD vAlign=center width="56%" height=22><B>
<P>�������� </B></P></TD>
<TD vAlign=center width="9%" height=22><I>
<P align=center></I></P></TD>
<TD vAlign=center width="10%" height=22><I>
<P align=center></I></P></TD>
<TD vAlign=center width="9%" height=22>

<P></P></TD>
<TD vAlign=center width="8%" height=22><I>
<P align=center></I></P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=26>
<P align=center>1</P></TD>
<TD vAlign=center width="56%" height=26>
<P>����������� ��������� �� ����� ���������� ��������. �������������� ������������</P></TD>
<TD vAlign=center width="9%" height=26>
<P align=center>5,9</P></TD>
<TD vAlign=center width="10%" height=26>
<P align=center>5,3</P></TD>
<TD vAlign=center width="9%" height=26>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=26>
<P align=center>25</P></TD></TR>
<TR>
<TD vAlign=center width="7%" height=24>
<P align=center>2</P></TD>
<TD vAlign=center width="56%" height=24>
<P>���������� ����������� (�������-�������)</P></TD>
<TD vAlign=center width="9%" height=24>
<P align=center>10,5</P></TD>
<TD vAlign=center width="10%" height=24>
<P align=center>9,5</P></TD>
<TD vAlign=center width="9%" height=24>
<P align=center>����</P></TD>
<TD vAlign=center width="8%" height=24>
<P align=center>27&#8211;29</P></TD></TR></TBODY></TABLE></P><B><I>
<P align=justify>( (095) 943-98-35 (�����������)</P>
<P align=justify>(095) 943-93-54 (������� �����)</P>
<P align=justify>���� (095) 943-93-62 (������������ ������)</P>
<P align=justify>E-mail: info@ifbsm.ru, </B></I><A href="mailto:commerce@ifbsm.ru;"><B><I><FONT color=#0f0f0f>commerce@ifbsm.ru</FONT></B></I></A></P><I>
<P align=justify></I><A href="http://www.ifbsm.ru/"><B><I><FONT color=#0f0f0f>www.ifbsm.ru</FONT></B></I></A></P>


</body>
</html>

