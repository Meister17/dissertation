<!--
ID:36678497
TITLE:���������� � ������� &#171;��������&#187;
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:������� �������� ����-��������� ��������� ������ ������, �.�.�.

NUMBER:7
ISSUE_DATE:2006-07-31
RECORD_DATE:2006-09-13


-->
<html>
<head>
<title>
36678497-���������� � ������� &#171;��������&#187;
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgwhite>
<TD></TD></TR>
<TR class=bodycolor>
<TD>
<P align=justify><I>��� �������, � �������������, ����������� �������������� ��������� � ���������� �������� ���, �� �������� ������������� � ������� ������������ �������� ������������� ������. ������ ���������� ������������ ���������� ������ ���������� �� ������ �� ������ ������������ ��������, ��� ����� ������� ����� ���������� ��������.</I></P></TD></TR>
<TR class=bgwhite>
<TD>
<P class=Main align=justify>��������� ����������� ������ ����� � ������������ � ������������ ���������� � ������������ ������������ �������� ������� ��������� ������������� ���������. � ����� ������ ����� &#171;������������&#187; ��������� �������������� �������� ������� ��������� � ���������� � ������� �������� ������������� ��������, �������������� ��������������� ������, ����� �������� ����/������ ������, ������� �������� ��������, ����� ���������� ��������, ������� ���������� ��������� � ��� �����. ��� ��� ������ ������������� ������� ������������� ����������. ��, ��� �� �� ��� ������������� �������� ������ � ������� ���������� ���������� ������ ���, �� �� ������� ������ � ���� �������������� ���������� �, ������ �����, &#8212; ���������� ����������, ��������� ��� �������. �� ����, ������������ �������� ������, ���������� ������������ ����� ������ �����������, &#8212; ����������� ���������� �� ����/������ ������ � ������������� �������� ��� ���������� ������ (603 � 501 �����) �� 2005 ���.</P>
<P class=Subheader align=center>������ 1. ������� ���������� ���������� (������ �� ������� 2005 �.) 
<CENTER><A href="buzdalin_b.gif" target=_blank></A> </CENTER>
<P class=Main align=justify>����� ������� �������������� ��������� ��������� ���������������� ��� ������? 
<P class=Main align=justify>��-������, �������� ������� �������� ����������������� ��������� ������������ ������ ����� �������������� ��������� ������� � ��������� ����� ���������. ���� �������� ����/������ ������ �� ���������� ������������ ������������ �����������, �� ������ �������� �������� ������ ������� �� ���������� ������������ ��������. ��� ������ ������������� �������� �� ���������� ��������� ���������� ������� ���� �� �����, ��� ���� � ��� ��������� ������������� � �������������� ��������, ��� �������� ����� ����� � ��������� �������� ��������� ����������������� ���������. 
<P class=Main align=justify>��-������, ��� �������� �����, ������� ��������� ��������, ���������� ������� ����������� ����������������� ��������� � ��� ����������� ����������� ������������ �� ����������� ��� �� ������ ������ �����, ��� � ������� ������� ������������ ���������� ������� ������������ �� ������������ ��������� � ������ ���������� ���������� �������, ��� &#8212; ��� ����������� �� ������� ��������������� ������������ ����������� �������� &#8212; ����������� ����������������� ������. 
<P class=Main align=justify>�-�������, ����� ��������� ��������, ���������� ������� ����� ������� ����/������ ������, � ����� ���������� ������������� �������� �������� �� ������ ������� ��������, �� � ������ ����������� � ���������� �������, ��������� �������� ���������������� ������ ���������� ������������ ���������� ������. ��������, ��� ���� � ������ ���������� ����� ������ ������������� ������� ������-������������ �� ����� ���, ��� ������� ��� ������� � �� ���������� � ������������ ���������� ������ � �����. 
<P class=Main align=justify>�-���������, ��������� � ������ ������� ����� ���������� ������ ������, �������� ���������� ��������� ������� ���������� ���������� ������ ����� ��� ��� ���������� ���������. 
<P class=Main align=justify>���������� ������ �� �������� &#171;�������&#187; ����������� ������������� ������ ��������� ����������� ������ � ��������� �� ��� &#8212; ����� ����������, ���������� � �������. ���������� ������� ���� ����� ���� ��������� � �������� (���������� �������, ����������������, ��������������� � �.�.) ������ ������ �����������. ���, � ���� �������, &#8212; �������� ������������ �������� � ������ ���������� � ��������� ������������ ��������. ������, ���� ���������� ������� ������� ������� ����������� ��������� ������ �� ���������� �������� � ���������� ������ ��������� (836 ������ 335 � ���������� �������������� �� ������� ���������� &#8212; ����������), �� ���������� � ��������� �������� � ������� �� ����� ���������� ��� �� ���������� ������ (136 � 355 ��������������, ������ 80 � ������� � 39 &#8212; � ������������). 
<P class=Main align=justify>����� �������, �������� ������� � ������� ��� ����������� ����������� ������� �������������� �������� � ����������� ��������� �������, ���������� � ����������. ������ �������� � ������� ������ ����� ��� ������������� ����. 
<P class=Subheader align=center>������ 2. ����������� ���������� ������ ��� �� ���������� ���������� ������� (������ �� ������� 2005 �.) 
<CENTER><A href="buzdalin2_b.gif" target=_blank></A> </CENTER>
<P class=Main align=justify>������� ���������� ���������� ���������� �������� �������� �������� ���������, �� �� �� ���������. ���� �� �� ����������� ����������� ������� � ������������� (���������� ������������� �������� �� ����������� ������������ ���������� ������), ���������� ����������� ����������� ������� ���������� � ������� ���������� ���������� ������. 
<P class=Main align=justify>������ ���������� ����������� � ������� ����������� ������ �� ������ ������������� �������� � ������������� �������������� ������������� ��������. ������ ��������� ����� �������� ����������� ������� � ������� ��� ��������� � ���������� ����������� ��� ������ ������������� �� �����, � ������� ������������������ ����� ��������� ����������� � ���������� �������� � ������� ���. ��������� �� ����� �������� �������� � ���������� ������ �������� ���������� ���������������� ����������� ������� &#8212; ��� ��� ����, ��� ����� ��������� � �����, � ���������� ������� ������. 
<P class=Main align=justify>�������� ����, ��� ������ ����������, � �� ��������� ������ �������� ��������, ������� ��� ������ ���������� ������������ ���������� ������ ����������� ������������ ������� �������� �� &#171;����������&#187; ����������� ������-������������ (������ ������� � �������), ����� ��� ���������� ����� �� ������� ����� �� ��� ��������. 
<P class=Main align=justify>���������� ������������� ����������, �� �����, ��� �������� ������ ���������� ��� ����� ����������� ������� � ������� ����������, ����������, ������������ � ��������, � ������� �������� �� ������� �������. ������, ���� �������� �������� ���������� ���������� ������ � ������� ���������� ��������� �� ����������, �� ��� ���������� ���� ������� ������� �� ������������� ������������ ��������� ������� (�������� �������� ������������ ���������). ����������� ���������� ������� �������� � ���� ����� ������������� �������, � ������� ���������� � ���������� ����������� ���, ��� � ������ ���������� ����� � ��������������� ��������. 
<P class=Main align=justify>���������� ������� ������� (�����, ��� � ����������) �������� ������� ����� ����������� ��� ��������� �����������, ������ �� ������� � ���� ����� ���� ������� � ���������� ������� �� ��������������� ��� � �������� ��������. ������ ���� ������������� ���������� ������� �� ������ ��������� ����� �������� ����������� ������� �������� � ���������� ������, � �������� ����������� ����������������� ���������, ��� �������� ������������ ������� �� ������ �� ����������� � ���������� ������ ������. �� ���� ������ ���������� ������������ ���������� ������ ����� ������� ������, ������� ����������������� ��������� � �����������, ������� ����� ������� � ���� ������ � ���������, ���������, ��������, �������, �������������, ������������ � ����������. � ��� ����� ���������� ��������� ��������� ��� ����� �������� ����������� ���������� ������. 
<P class=Main align=justify>������ �������� ������ (��������� ����� ��� &#171;��������&#187; � ���������� ���������� �������) � ����� ������ �� �� �������. �� ������� ����������� ���������� ����������� ������� �� ������ ��� ������������� �������� �������� ���������, �������, ����������. ���������� ��������� ���� ��������, ����������� � ������ �������. ���� �� ������� � ������� ������������� ����������� (� ���������, ����������� ��������� ���������� ���������� ������ ������� ��� � ����������� ������������ ���������� ������, ��� ���������� �� &#171;�����������������&#187; �� ���������� ���������� ������), ���������� ������� �������� ���������� ������� ��������. �������� �� ����������� �� ���������� ������� ������ ����� ����� ���������� � ����������, ���� ���������� ������ � ������� ������ ����������� &#8212; ������ � �������������, ������������, ���������, ����������, �������������, ������� � ��������. 
<P class=Main align=justify>&#171;�������&#187; ���������� ������ ����������� ���, ��� �������� �� ������� ������� ���������� �������� � �������, �� ������ ������� �� ���� ����� ����������� ��������� ����������� �������. � ������ �������, ���������� ����� �� ������������� ���� ��������� ��� ���������� ��������, � ������, � �� ����� ������������ �� �������� ��������� ������� ����������� �������, �� ��������� ���� ������������� �����-���������� �������. 
<P class=Main align=justify>����� ������ � ����� �������� ���������� ������ � ������� ��� ���� ��� ���������� �������. ��������� ���������� ������ �� ����� ��� ����� ������� ������ � ��� � ������� 2005-�� ��������� 992, ��� ����������� � ����������� ������� ����������� �������������� �����. ������ ������� ���� ������������ ������������� ����� � ��� ������ ���������� �� ������, ���������� �������� � ���������� �������� ��������� ����������. ���������� � ���������� �������� ��� ���� ��� �� ������ ���������� ��������� ����������� � ���������� ����� � ��� �� ������������ �������� � ������ �������. ��� �� �����, ������� ��� ��������� ���������� ���������� ��� �������, ��������� &#8212; ���� �������.</P>
<P class=Main align=justify>
<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR>
<TD class=bgcolor1>
<P class=Rubrika align=center>������������� �������� ����� ����������� ������� � ������� ��� (������ �� ������� 2005 �.)</P></TD></TR>
<TR>
<TD class=bgwhite>
<TABLE cellSpacing=0 cellPadding=0 width="100%" align=center border=1>
<TBODY>
<TR class=bgcolor1>
<TD>
<P align=center><STRONG>�����������</STRONG></P></TD>
<TD>
<P align=center><STRONG>����� ������</STRONG></P></TD>
<TD>
<P align=center><STRONG>���-�� ���. ������, �������� ����������� �������</STRONG></P></TD>
<TD>
<P align=center><STRONG>���-�� �������� ��� ����������� �������</STRONG></P></TD>
<TD>
<P align=center><STRONG>���-�� ���������� ��� ����������� �������</STRONG></P></TD>
<TD>
<P align=center><STRONG>���-�� �������� ��� ����������� ������� / ����� ������</STRONG></P></TD>
<TD>
<P align=center><STRONG>���-�� ���������� ��� ����������� ������� / ����� ������</STRONG></P></TD>
<TD>
<P align=center><STRONG>���-�� ���. ������, �������� ����������� ������� / ����� ������</STRONG></P></TD>
<TD>
<P align=center><STRONG>���-�� ���������� ��� ����������� ������� / ���-�� ���. ������, �������� ����������� �������</STRONG></P></TD></TR>
<TR>
<TD>
<P>�����������</P></TD>
<TD align=right>
<P>44</P></TD>
<TD align=right>
<P>188</P></TD>
<TD align=right>
<P>6</P></TD>
<TD align=right>
<P>23</P></TD>
<TD align=right>
<P>0,14</P></TD>
<TD align=right>
<P>0,52</P></TD>
<TD align=right>
<P>4,27</P></TD>
<TD align=right>
<P>0,12</P></TD></TR>
<TR>
<TD>
<P>�������</P></TD>
<TD align=right>
<P>21</P></TD>
<TD align=right>
<P>189</P></TD>
<TD align=right>
<P>2</P></TD>
<TD align=right>
<P>2</P></TD>
<TD align=right>
<P>0,10</P></TD>
<TD align=right>
<P>0,10</P></TD>
<TD align=right>
<P>9,00</P></TD>
<TD align=right>
<P>0,01</P></TD></TR>
<TR>
<TD>
<P>��������</P></TD>
<TD align=right>
<P>30</P></TD>
<TD align=right>
<P>355</P></TD>
<TD align=right>
<P>136</P></TD>
<TD align=right>
<P>94</P></TD>
<TD align=right>
<P>4,53</P></TD>
<TD align=right>
<P>3,13</P></TD>
<TD align=right>
<P>11,83</P></TD>
<TD align=right>
<P>0,26</P></TD></TR>
<TR>
<TD>
<P>������</P></TD>
<TD align=right>
<P>19</P></TD>
<TD align=right>
<P>164</P></TD>
<TD align=right>
<P>15</P></TD>
<TD align=right>
<P>8</P></TD>
<TD align=right>
<P>0,79</P></TD>
<TD align=right>
<P>0,42</P></TD>
<TD align=right>
<P>8,63</P></TD>
<TD align=right>
<P>0,05</P></TD></TR>
<TR>
<TD>
<P>���������</P></TD>
<TD align=right>
<P>34</P></TD>
<TD align=right>
<P>289</P></TD>
<TD align=right>
<P>212</P></TD>
<TD align=right>
<P>128</P></TD>
<TD align=right>
<P>6,24</P></TD>
<TD align=right>
<P>3,76</P></TD>
<TD align=right>
<P>8,50</P></TD>
<TD align=right>
<P>0,44</P></TD></TR>
<TR>
<TD>
<P>��������</P></TD>
<TD align=right>
<P>19</P></TD>
<TD align=right>
<P>239</P></TD>
<TD align=right>
<P>6</P></TD>
<TD align=right>
<P>12</P></TD>
<TD align=right>
<P>0,32</P></TD>
<TD align=right>
<P>0,63</P></TD>
<TD align=right>
<P>12,58</P></TD>
<TD align=right>
<P>0,05</P></TD></TR>
<TR>
<TD>
<P>�������</P></TD>
<TD align=right>
<P>16</P></TD>
<TD align=right>
<P>278</P></TD>
<TD align=right>
<P>17</P></TD>
<TD align=right>
<P>70</P></TD>
<TD align=right>
<P>1,06</P></TD>
<TD align=right>
<P>4,38</P></TD>
<TD align=right>
<P>17,38</P></TD>
<TD align=right>
<P>0,25</P></TD></TR>
<TR>
<TD>
<P>�����������</P></TD>
<TD align=right>
<P>11</P></TD>
<TD align=right>
<P>147</P></TD>
<TD align=right>
<P>39</P></TD>
<TD align=right>
<P>9</P></TD>
<TD align=right>
<P>3,55</P></TD>
<TD align=right>
<P>0,82</P></TD>
<TD align=right>
<P>13,36</P></TD>
<TD align=right>
<P>0,06</P></TD></TR>
<TR>
<TD>
<P>���������</P></TD>
<TD align=right>
<P>13</P></TD>
<TD align=right>
<P>15</P></TD>
<TD align=right>
<P>0</P></TD>
<TD align=right>
<P>1</P></TD>
<TD align=right>
<P>0,00</P></TD>
<TD align=right>
<P>0,08</P></TD>
<TD align=right>
<P>1,15</P></TD>
<TD align=right>
<P>0,07</P></TD></TR>
<TR>
<TD>
<P>����������</P></TD>
<TD align=right>
<P>29</P></TD>
<TD align=right>
<P>98</P></TD>
<TD align=right>
<P>9</P></TD>
<TD align=right>
<P>12</P></TD>
<TD align=right>
<P>0,31</P></TD>
<TD align=right>
<P>0,41</P></TD>
<TD align=right>
<P>3,38</P></TD>
<TD align=right>
<P>0,12</P></TD></TR>
<TR>
<TD>
<P>�������</P></TD>
<TD align=right>
<P>163</P></TD>
<TD align=right>
<P>836</P></TD>
<TD align=right>
<P>80</P></TD>
<TD align=right>
<P>111</P></TD>
<TD align=right>
<P>0,49</P></TD>
<TD align=right>
<P>0,68</P></TD>
<TD align=right>
<P>5,13</P></TD>
<TD align=right>
<P>0,13</P></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></P></TD></TR></TBODY></TABLE>


</body>
</html>

