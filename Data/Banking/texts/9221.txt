<!--
ID:35681313
TITLE:����� ������� ��������� - ���������� �������� �������������� ����������
SOURCE_ID:3204
SOURCE_NAME:����� ������ �����
AUTHOR:����� �����, �������� �� ��������������� ��������;  ����� �����������, ��������� ������ ���������� ��� &#171;�������&#187;
NUMBER:22
ISSUE_DATE:2005-11-30
RECORD_DATE:2005-12-21


RBR_ID:2038
RBR_NAME:���������� �����
RBR_ID:3398
RBR_NAME:����������
-->
<html>
<head>
<title>
35681313-����� ������� ��������� - ���������� �������� �������������� ����������
</title>
</head>
<body >


<P align=justify>�� ��� ��������, ��� ����� ������������ ���������� ������ ����� �� ������ ��������� �����. �������� ���������� ���������� �������� �������, � ��������� ����� ������ �������� ������������� ���������� ������������ ��� ��������� ����� ��������������.</P>
<P align=justify>����� ��� ���������� � �������� ���������� �� ����� ������������ ����������. ������ ��� ���� ���� ��������, ��� �� ������� ���������� �� ���� ����� ����������, � ��� �������� ������ �������, � �������� ������������ ����� ��������� ������ �������� �� ������ ���������������� ��������� - ���� ������� � ��������, �� � �� ����� ���������� � ����������� ����, ��� ���������� ������� �������� � ��� ������� �����. �� ���� ����������� ������� �� ����� ������ ���� ������������� �� ��������� ������������.</P>
<P align=justify>������ ���� � �������� �������� ������� �������������� ���������������� ���������� ����� ����� � � �������� ������������, � ��������� ����������� �������� � �����������, ������ �������������� ������������, ����������� ������ �������� ����������� �������� � ������������� ��������� �������� ���������� � ���������, ���������� � ������������� ����������.</P>
<P align=justify>� ��� &#171;�������&#187; ���� ������ �������� �� ���� ������ <I>&#171;����� ������� ���������&#187;, </I>������� ��������� �������� ����������� ������������ �������������� � �������������� ������������, ����������� �������� �� ������������� �� � ����� ���������� ���������� ���������, ���������������� ��������� � ���������������� �������������-������������� ������������� �������������� ��������� ���������.</P>
<P align=justify>� �������� ��������� ����� ��������� ��� ��������� �������� � ���, ��� � ����� ������ ����������������� ��� ������� ��������, �������������� ���������� ������ �������� ��������������, � ����� � ������������ ������������� ����������� - ����������� ��������.</P>
<P align=justify>���������� ����� (��������) ������� ����������, ����� �������� �������� ��� ��������� �������� ������ � ������������� ���������� ���������� ��, ��������� ����� ��������� ������������� ������������ ��� ������� ��������� ������:</P>
<P align=justify>-&nbsp;�������������� � ���-������� �� � ����� ��������� �� ��� ����������� ��� ��������� ���������� �� �������, �������� � ������������� ����������;</P>
<P align=justify>-&nbsp;�������� ������������ ����������� ���������� �� � ������������� ����������, ������������ � ����������;</P>
<P align=justify>-&nbsp;�������� ������ �� ������� ������ �� � ������ ����� (������) ��� ������ ������� ���������� ��������; ��������� ������������� �������������� ������ ������� ������ �� � ������������� ������������ ��������, ��������� �������� ������� ����� � ��� �� �� ���� ���������� ���������� ���������� � ��������� ����������� �������� �����������;</P>
<P align=justify>-&nbsp;������ ������������� ���������� ���������� �� ������ ��;</P>
<P align=justify>-&nbsp;�������� �� ����������� �� �������������� ���������� � ���� ��������������� �� ���������� ��������,</P>
<P align=justify>&#171;����� ������� ���������&#187; ��������� � � ������, ����� ����������� �������� �� ������������ ����������, � ����� ����� ���������� �������� �� ������������ ��������� � �����������, �� � ������ �������� �� ������������� ��������� ����.</P>
<P align=justify>������ �������� ����������� � ������������� ���� �������� ��������� ��� �������� ������� � ������������� ���������� �������� ������������� ��������� �������� ����������, ��������� ��� ������� � ����� �������� ���������� ������� � ��������� �������, ����������� � ������������� ����������. ���������� ������������� � ��������� � ������� ���������� ���������� ������� ���������� �������� � ��������� ����������, ������� ���������� ��� ������� � �������� ���������� ���������� �� ����� � ������� ���������� �� �������, ����� ����� ��� ���������� ��������� ��������, ��� ������ �� ����������.</P>
<P align=justify>��� �������������� � ����������� ������������ � ���������� ������� ������������� ��������� � ��������� ���������� �������� ����� �������� ��������� ���� ���������������� ������-������������� �������������, ��������� ���������� � ����� ���������� �����. ��� ���� ������ ����� �� (���� �� ������ ����� �� ������������ ��� ����������� �����) ����� �������� ��������� �������� �������� ���������� � ����������� ���������� ���������.</P>
<P align=justify>��� &#171;�������&#187; ����������� ������� ����� �������, ���������� � ������������� ���������� �� ��������� �������������� ����������� �������������� � �������������� ������������ � ������������ ������������ �������� �� ������������� ����������� �������� � ����� ���������� ���������� ���������. ��� ���� �������� �������� ��������� �����������:</P>
<P align=justify>1.&nbsp;����� &#171;������ ����&#187; - �������������� � ����� ������� ���������� ��� ����������� �� ����� ������������ �� ���������� ��������.</P>
<P align=justify>2.&nbsp;������� ������ �������� � � ������� 5 ��� ������������ �� �������� ������������� �������, ����������� �������� � ������������ ����� �������������� ���������� ��� ��������� (��� ���������� ������������ �� ����� ������������ ����������).</P>
<P align=justify>3.&nbsp;������������� ���������, ��������� � ������� ����������.</P>
<P align=justify>4.&nbsp;����������� ������� �������������� ����� ������ ����� �� ��������� �����������.</P>
<P align=justify>5.&nbsp;����������� �������� � ������� �� ����������� � ����������� ������-������������� ��������� � ��������� ������� ���������.</P>
<P align=justify>6.&nbsp;�������� ������������ ��������� �� ������������� ����������� ��������, ���������� � ���������-����������.</P>
<P align=justify>7. �������������� �������������� ������� � ��������� ���������� ��������.</P>
<P align=justify>8.&nbsp;������������� � �������� ������� ��� ����������������, � ������������ ����� �������������� ����������.</P>
<P align=justify>9.&nbsp;����������� ��������������� �� �������� ���������������� ������������ ��� ������ � �������������� ����������� �������������� ���������.</P>
<P align=justify>10.&nbsp;���� �������������� ������������ ������� �������� ���������.</P>
<P align=justify>� �����, ������ ������������������� ����������� �������� �� ����������� ����������� �������� � �������������� ��������� ����������� ������������ ��� ������������� �����������: ��������� ���������� ��� � ��������� ������ ��, ��� � � ������� ������ � ������������ �����������. ��� ���� ����������� ��������� ���������� �������������� ������������ ���������� ������������� ����������� � ���������� ���������� ������������� �� �� ��� ���������� ���������� ���������.</P>
<P align=justify>������ ������� ����� ����������� (������� ����) ������������� ������� ��������� ��� ������������� ����� ������� ���������. � ���� ������ �������� �������� ������������ ����� ���������� ��� �������� � ���������� ������������� �� � ��������� ������� � ������������� ����������, ��� �������� �������������� ������� � ��������� ���� ������� ���������. ��������, ��������� ������ �����, �������� ���������������� ��������� ����� �������������� ��������� ����������� �����������-����������-�������������. �� ������������ �������� �� ���� ������-������������� ��������, ������������ ��������������� � ��������������� ����� ������ �����, � � ����������� ��� ����������� ������ � ��������������� ��������, ���������� �� ����������� ����������������, ����������� �� ����� ������ �����.</P>
<P align=justify>����� ����, ������������� &#171;����� ������� ���������&#187; ������������ ����� ������� ������������� ������������� �����: ���������������� ��������� ������-������������� ��������� ��������� � ���������������� ��������� � ���������� � ������������� ������-������������� ��������� ���������.</P>
<P align=justify>��� ���� ����� ����� ���� ����� ��������� ���������� ��������� ������� ��� ���������, � ������� ������ ��������� � ����������� �������� ���� ����� ������ ��������.</P>
<P align=justify>����������, ��� ������, � ������� �� ������ �������, ����� ������ ��������, ��������� ����� ������������ ���������� ���������� ������ �������� ��������� ������������� �������������� ���������������� ���������� � ����������. ���� ���� �� �������� ����� ����������� ��������������, ������������ ���������� ���������� �������, ��� ��� ��� ������ ���������� ����������� ��������� ��������� ������������ ������������ �������������� � ������� ���������.</P>
<P align=justify>&#171;����� ������� ���������&#187; ��������� ������� ���� �������� �������������� ���������� �� ����������� ����� �������, ������ ��������� (��� ������������, ��� � ����������� ����) �������� ������������� �������������� � ������������ ���������� � ����� �������������� ���������� ��������.</P>
<P align=justify>����� �������, ������������ ��� ���� ����������� ���������� ����� ����������� ����� ������������ ���������� � ��������� ����� � ����� ������� �����������, ������� ����� ����������������� ����������� ����� � �����������.</P>


</body>
</html>

