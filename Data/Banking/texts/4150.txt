<!--
ID:38237391
TITLE:����������� ����������� ��������� ����������� �� �������� ������������������� ��������� ��������* �� ��������� �� 01.08.2007 �.
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:18
ISSUE_DATE:2007-09-30
RECORD_DATE:2007-10-05


-->
<html>
<head>
<title>
38237391-����������� ����������� ��������� ����������� �� �������� ������������������� ��������� ��������* �� ��������� �� 01.08.2007 �.
</title>
</head>
<body >


<TABLE cellSpacing=2 cellPadding=4 width=614 border=1>
<TBODY>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="26%">&nbsp;</TD>
<TD vAlign=top width="55%" colSpan=4><FONT face="Times New Roman CYR" size=2>
<P align=center>���������� ��</FONT></P></TD>
<TD vAlign=top width="12%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR" size=2>
<P align=center></P>
<P align=center>&#8470;</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR" size=2>
<P align=center>�������� ���������</P>
<P align=center>��������</FONT></P></TD>
<TD vAlign=top width="28%" colSpan=2><FONT face="Times New Roman CYR" size=2>
<P align=center>�� 01.01.2007</FONT></P></TD>
<TD vAlign=top width="28%" colSpan=2><FONT face="Times New Roman CYR" size=2>
<P align=center>�� 01.08.</FONT><FONT size=2>2007</FONT></P></TD>
<TD vAlign=top width="12%"><FONT face="Times New Roman CYR" size=2>
<P align=center></P>
<P align=center>���������</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR" size=2>
<P align=center>�/�</FONT></P></TD>
<TD vAlign=top width="26%">&nbsp;</TD>
<TD vAlign=top width="14%"><FONT face="Times New Roman CYR" size=2>
<P align=center>����������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT face="Times New Roman CYR" size=2>
<P align=center>�������� ���</P>
<P align=center>� �����, %</FONT></P></TD>
<TD vAlign=top width="14%"><FONT face="Times New Roman CYR" size=2>
<P align=center>����������</FONT></P></TD>
<TD vAlign=top width="14%"><FONT face="Times New Roman CYR" size=2>
<P align=center>�������� ���</P>
<P align=center>� �����, %</FONT></P></TD>
<TD vAlign=top width="12%"><FONT face="Times New Roman CYR" size=2>
<P align=center>(+/&#8211;)</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR" size=2>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR" size=2>
<P align=justify>�� 3 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%"><FONT face="Times New Roman CYR" size=2>
<P align=right>43</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>3,6</FONT></P></TD>
<TD vAlign=top width="14%"><FONT face="Times New Roman CYR" size=2>
<P align=right>40</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>3,4</FONT></P></TD>
<TD vAlign=top width="12%"><FONT size=2>
<P align=right>&#8211;3</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR" size=2>
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR" size=2>
<P align=justify>�� 3 �� 10 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>87</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>7,3</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>67</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>5,8</FONT></P></TD>
<TD vAlign=top width="12%"><FONT size=2>
<P align=right>&#8211;20</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR" size=2>
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR" size=2>
<P align=justify>�� 10 �� 30 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>168</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>14,1</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>139</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>12</FONT></P></TD>
<TD vAlign=top width="12%"><FONT size=2>
<P align=right>&#8211;29</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR" size=2>
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR" size=2>
<P align=justify>�� 30 �� 60 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>182</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>15,3</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>172</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>14,8</FONT></P></TD>
<TD vAlign=top width="12%"><FONT size=2>
<P align=right>&#8211;10</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR" size=2>
<P align=center>5</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR" size=2>
<P align=justify>�� 60 �� 150 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>226</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>19,0</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>216</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>18,6</FONT></P></TD>
<TD vAlign=top width="12%"><FONT size=2>
<P align=right>&#8211;10</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR" size=2>
<P align=center>6</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR" size=2>
<P align=justify>�� 150 �� 300 ��� ���.</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>217</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>18,3</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>246</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>21,2</FONT></P></TD>
<TD vAlign=top width="12%"><FONT size=2>
<P align=right>+29</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%"><FONT face="Times New Roman CYR" size=2>
<P align=center>7</FONT></P></TD>
<TD vAlign=top width="26%"><FONT face="Times New Roman CYR" size=2>
<P align=justify>�� 300 ��� ���. � ����</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>266</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>22,4</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>283</FONT></P></TD>
<TD vAlign=top width="14%"><FONT size=2>
<P align=right>24,3</FONT></P></TD>
<TD vAlign=top width="12%"><FONT size=2>
<P align=right>+17</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="6%">&nbsp;</TD>
<TD vAlign=top width="26%"><B><FONT face="Times New Roman CYR" size=2>
<P align=justify>����� �� ������</B></FONT></P></TD>
<TD vAlign=top width="14%"><B><FONT size=2>
<P align=right>1189</B></FONT></P></TD>
<TD vAlign=top width="14%"><B><FONT face="Times New Roman CYR" size=2>
<P align=right>100</B></FONT></P></TD>
<TD vAlign=top width="14%"><B><FONT size=2>
<P align=right>1163</B></FONT></P></TD>
<TD vAlign=top width="14%"><B><FONT face="Times New Roman CYR" size=2>
<P align=right>100</B></FONT></P></TD>
<TD vAlign=top width="12%"><B><FONT size=2>
<P align=right>&#8211;26</B></FONT></P></TD></TR></TBODY></TABLE><FONT face="Times New Roman CYR" size=2><SUP>
<P align=justify><FONT face="Times New Roman CYR">*</FONT> </SUP>�������� �������, �������� �������� �������� �����������, ������� � ����� ��������� ����������� � ������ � ����� ��������������� ����������� ��������� ����������� ����� ����������� ������ � �������������� �������������� ������.</P></FONT><I><FONT face="Times New Roman CYR">
<P align=right>"������� ����� ������", </FONT>2007<FONT face="Times New Roman CYR">, &#8470; 47</P></I></FONT>


</body>
</html>

