<!--
ID:35709591
TITLE:&#171;��������� ����&#187; (���)
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:
NUMBER:11
ISSUE_DATE:2005-11-30
RECORD_DATE:2005-12-29


-->
<html>
<head>
<title>
35709591-&#171;��������� ����&#187; (���)
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgwhite>
<TD></TD></TR>
<TR class=bodycolor>
<TD>
<P class=Rubrika align=center>��������� ����������� ������������ ���� &#171;��������� ����&#187;<BR>(�������� ����������� ��������)</P>
<P class=Main align=center>��������������� ����� 3423, ���-��� 044579555, 
<P class=Main align=center>�������� �����: 105066, �. ������, ��. ���������������, �. 31/7, ������ 13</P></TD></TR>
<TR class=bgwhite>
<TD align=middle height=0>
<P class=Rubrika>������ �� 1 ������� 2005 ����</P>
<TABLE cellSpacing=0 cellPadding=0 width="98%" borderColorLight=#cccccc border=2>
<TBODY>
<TR>
<TD>
<P align=center><STRONG>����� �/�</STRONG></P></TD>
<TD>
<P align=center><STRONG>������������ ������</STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� �������� ����</STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� ��������������� �������� ���� �������� ����</STRONG></P></TD></TR>
<TR>
<TD width=63 height=0>
<P align=center><STRONG>1</STRONG></P></TD>
<TD width=300>
<P align=center><STRONG>2</STRONG></P></TD>
<TD width=93>
<P align=center><STRONG>3</STRONG></P></TD>
<TD width=110>
<P align=center><STRONG>4</STRONG></P></TD></TR>
<TR>
<TD colSpan=4 height=0>
<P align=center><B>������</B></P></TD></TR>
<TR>
<TD>
<P>1.</P></TD>
<TD>
<P>�������� ��������</P></TD>
<TD>
<P align=right>5 942</P></TD>
<TD>
<P align=right>1 396</P></TD></TR>
<TR>
<TD>
<P>2.</P></TD>
<TD>
<P>�������� ��������� ����������� � ����������� ����� ���������� ���������</P></TD>
<TD>
<P align=right>85 785</P></TD>
<TD>
<P align=right>47 469</P></TD></TR>
<TR>
<TD>
<P>2.1.</P></TD>
<TD>
<P>������������ �������</P></TD>
<TD>
<P align=right>14 277</P></TD>
<TD>
<P align=right>3 723</P></TD></TR>
<TR>
<TD>
<P>3.</P></TD>
<TD>
<P>�������� � ��������� ������������</P></TD>
<TD>
<P align=right>2 925</P></TD>
<TD>
<P align=right>9 470</P></TD></TR>
<TR>
<TD>
<P>4.</P></TD>
<TD>
<P>������ �������� � �������� ������ ������</P></TD>
<TD>
<P align=right>44 974</P></TD>
<TD>
<P align=right>9 578</P></TD></TR>
<TR>
<TD>
<P>5.</P></TD>
<TD>
<P>������ ������� �������������</P></TD>
<TD>
<P align=right>515 973</P></TD>
<TD>
<P align=right>244 294</P></TD></TR>
<TR>
<TD>
<P>6.</P></TD>
<TD>
<P>������ �������� � �������������� ������ ������, ������������ �� ���������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>7.</P></TD>
<TD>
<P>������ �������� � ������ ������, ��������� � ������� ��� �������</P></TD>
<TD>
<P align=right>8</P></TD>
<TD>
<P align=right>2</P></TD></TR>
<TR>
<TD>
<P>8.</P></TD>
<TD>
<P>�������� ��������, �������������� ������ � ������������ ������</P></TD>
<TD>
<P align=right>10 469</P></TD>
<TD>
<P align=right>7 789</P></TD></TR>
<TR>
<TD>
<P>9.</P></TD>
<TD>
<P>���������� �� ��������� ���������</P></TD>
<TD>
<P align=right>127</P></TD>
<TD>
<P align=right>63</P></TD></TR>
<TR>
<TD>
<P>10.</P></TD>
<TD>
<P>������ ������</P></TD>
<TD>
<P align=right>58 626</P></TD>
<TD>
<P align=right>370</P></TD></TR>
<TR>
<TD>
<P>11.</P></TD>
<TD>
<P>����� �������</P></TD>
<TD>
<P align=right>724 829</P></TD>
<TD>
<P align=right>320 431</P></TD></TR>
<TR align=middle>
<TD colSpan=4>
<P><B>II. �������</B></P></TD></TR>
<TR>
<TD>
<P>12.</P></TD>
<TD>
<P>������� ������������ ����� ���������� ���������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>13.</P></TD>
<TD>
<P>�������� ��������� �����������</P></TD>
<TD>
<P align=right>41 196</P></TD>
<TD>
<P align=right>30 000</P></TD></TR>
<TR>
<TD>
<P>14.</P></TD>
<TD>
<P>�������� �������� (����������� �����������)</P></TD>
<TD>
<P align=right>433 195</P></TD>
<TD>
<P align=right>68 390</P></TD></TR>
<TR>
<TD>
<P>14.1.</P></TD>
<TD>
<P>������ ���������� ���</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>15.</P></TD>
<TD>
<P>���������� �������� �������������</P></TD>
<TD>
<P align=right>27 306</P></TD>
<TD>
<P align=right>72 550</P></TD></TR>
<TR>
<TD>
<P>16.</P></TD>
<TD>
<P>������������� �� ������ ���������</P></TD>
<TD>
<P align=right>12 196</P></TD>
<TD>
<P align=right>2 334</P></TD></TR>
<TR>
<TD>
<P>17.</P></TD>
<TD>
<P>������ �������������</P></TD>
<TD>
<P align=right>56 915</P></TD>
<TD>
<P align=right>444</P></TD></TR>
<TR>
<TD>
<P>18.</P></TD>
<TD>
<P>������� �� ��������� ������ �� �������� �������������� ���������� ���������, ������ ��������� ������� � �� ��������� � ����������� �������� ���</P></TD>
<TD>
<P align=right>453</P></TD>
<TD>
<P align=right>46</P></TD></TR>
<TR>
<TD>
<P>19.</P></TD>
<TD>
<P>����� ������������</P></TD>
<TD>
<P align=right>571 261</P></TD>
<TD>
<P align=right>173 764</P></TD></TR>
<TR align=middle>
<TD colSpan=4>
<P class=bold><STRONG>III. ��������� ����������� �������</STRONG></P></TD></TR>
<TR>
<TD>
<P>20.</P></TD>
<TD>
<P>�������� ���������� (����������)</P></TD>
<TD>
<P align=right>145 000</P></TD>
<TD>
<P align=right>145 000</P></TD></TR>
<TR>
<TD>
<P>20.1.</P></TD>
<TD>
<P>������������������ ������������ ����� � ����</P></TD>
<TD>
<P align=right>145 000</P></TD>
<TD>
<P align=right>145 000</P></TD></TR>
<TR>
<TD>
<P>20.2.</P></TD>
<TD>
<P>������������������ ����������������� �����</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>20.3.</P></TD>
<TD>
<P>�������������������� �������� ������� ������������� ��������� �����������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>21.</P></TD>
<TD>
<P>����������� �����, ����������� � ����������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>22.</P></TD>
<TD>
<P>����������� �����</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>23.</P></TD>
<TD>
<P>���������� �������� �������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>24.</P></TD>
<TD>
<P>������� ������� �������� � ����������� �������, �������� �� ����������� �������� (�������)</P></TD>
<TD>
<P align=right>12 569</P></TD>
<TD>
<P align=right>2 349</P></TD></TR>
<TR>
<TD>
<P>25.</P></TD>
<TD>
<P>����� � ���������������� ������� ������� ��� � ������������ ��������� ����������� (������������ ������ ������� ���)</P></TD>
<TD>
<P align=right>6 516</P></TD>
<TD>
<P align=right>570</P></TD></TR>
<TR>
<TD>
<P>26.</P></TD>
<TD height=0>
<P>������ (������) �� �������� ������</P></TD>
<TD>
<P align=right>14 621</P></TD>
<TD>
<P align=right>3 446</P></TD></TR>
<TR>
<TD>
<P>27.</P></TD>
<TD>
<P>����� ���������� ����������� �������</P></TD>
<TD>
<P align=right>153 568</P></TD>
<TD>
<P align=right>146 667</P></TD></TR>
<TR>
<TD>
<P>28.</P></TD>
<TD>
<P>����� ��������</P></TD>
<TD>
<P align=right>724 829</P></TD>
<TD>
<P align=right>320 431</P></TD></TR>
<TR align=middle>
<TD colSpan=4>
<P><B>IV. ������������� �������������</B></P></TD></TR>
<TR>
<TD>
<P>29.</P></TD>
<TD>
<P>����������� ������������� ��������� �����������</P></TD>
<TD>
<P align=right>21 377</P></TD>
<TD>
<P align=right>2 635</P></TD></TR>
<TR>
<TD>
<P>30.</P></TD>
<TD>
<P>��������, �������� ��������� ������������</P></TD>
<TD>
<P align=right>45 734</P></TD>
<TD>
<P align=right>2 552</P></TD></TR></TBODY></TABLE>
<P class=top align=justify>��������, ���������� ��������� �� ���� ������� ������� V &#171;����� �������������� ����������&#187;, �� �������������� 
<P class=Rubrika>����� � �������� � ������� �� 9 ������� 2005 ����.</P>
<TABLE cellSpacing=0 cellPadding=0 width="98%" borderColorLight=#cccccc border=2>
<TBODY>
<TR>
<TD>
<P align=center><STRONG>����� �/�</STRONG></P></TD>
<TD>
<P align=center><STRONG>������������ ������</STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� �������� ������ </STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� ��������������� ������ �������� ����</STRONG></P></TD></TR>
<TR>
<TD width=64 height=0>
<P align=center><STRONG>1</STRONG></P></TD>
<TD width=267>
<P align=center><STRONG>2</STRONG></P></TD>
<TD width=78>
<P align=center><STRONG>3</STRONG></P></TD>
<TD width=178>
<P align=center><STRONG>4</STRONG></P></TD></TR>
<TR>
<TD colSpan=4 height=0>
<P class=bold align=center><STRONG>�������� ���������� � ����������� ������ ��:</STRONG></P></TD></TR>
<TR>
<TD>
<P>1</P></TD>
<TD>
<P>���������� ������� � ��������� ������������</P></TD>
<TD>
<P align=right>5 511</P></TD>
<TD>
<P align=right>4 087</P></TD></TR>
<TR>
<TD>
<P>2</P></TD>
<TD>
<P>����, ��������������� �������� (����������� ������������)</P></TD>
<TD>
<P align=right>24 102</P></TD>
<TD>
<P align=right>3 007</P></TD></TR>
<TR>
<TD>
<P>3</P></TD>
<TD>
<P>�������� ����� �� ���������� ������ (�������)</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>4</P></TD>
<TD>
<P>������ ����� � ������������� �������</P></TD>
<TD>
<P align=right>2 581</P></TD>
<TD>
<P align=right>378</P></TD></TR>
<TR>
<TD>
<P>5</P></TD>
<TD>
<P>������ ����������</P></TD>
<TD>
<P align=right>103</P></TD>
<TD>
<P align=right>26</P></TD></TR>
<TR>
<TD>
<P>6</P></TD>
<TD>
<P>����� ��������� ���������� � ����������� �������</P></TD>
<TD>
<P align=right>32 297</P></TD>
<TD>
<P align=right>7 498</P></TD></TR>
<TR vAlign=top align=middle>
<TD colSpan=4>
<P><B>�������� ���������� � ����������� ������� ��:</B></P></TD></TR>
<TR>
<TD>
<P>7</P></TD>
<TD>
<P>������������ ��������� ��������� �����������</P></TD>
<TD>
<P align=right>1 220</P></TD>
<TD>
<P align=right>1 848</P></TD></TR>
<TR>
<TD>
<P>8</P></TD>
<TD>
<P>������������ ��������� �������� (����������� �����������)</P></TD>
<TD>
<P align=right>3 004</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>9</P></TD>
<TD>
<P>���������� �������� ��������������</P></TD>
<TD>
<P align=right>5 735</P></TD>
<TD>
<P align=right>523</P></TD></TR>
<TR>
<TD>
<P>10</P></TD>
<TD>
<P>����� ��������� ���������� � ����������� ��������</P></TD>
<TD>
<P align=right>9 959</P></TD>
<TD>
<P align=right>2 371</P></TD></TR>
<TR>
<TD>
<P>11</P></TD>
<TD>
<P>������ ���������� � ����������� ������</P></TD>
<TD>
<P align=right>22 338</P></TD>
<TD>
<P align=right>5 127</P></TD></TR>
<TR>
<TD>
<P>12</P></TD>
<TD>
<P>������ ������ �� �������� � ������� ��������</P></TD>
<TD>
<P align=right>13 224</P></TD>
<TD>
<P align=right>10 645</P></TD></TR>
<TR>
<TD>
<P>13</P></TD>
<TD>
<P>������ ������ �� �������� � ����������� �������</P></TD>
<TD>
<P align=right>9 547</P></TD>
<TD>
<P align=right>2 048</P></TD></TR>
<TR>
<TD>
<P>14</P></TD>
<TD>
<P>������ ������ �� �������� � ������������ ��������� � ������� ����������� �������������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>15</P></TD>
<TD>
<P>������ ������ �� ���������� ����������� ������</P></TD>
<TD>
<P align=right>-137</P></TD>
<TD>
<P align=right>94</P></TD></TR>
<TR>
<TD>
<P>16</P></TD>
<TD>
<P>������������ ������</P></TD>
<TD>
<P align=right>14 515</P></TD>
<TD>
<P align=right>5 892</P></TD></TR>
<TR>
<TD>
<P>17</P></TD>
<TD>
<P>������������ �������</P></TD>
<TD>
<P align=right>182</P></TD>
<TD>
<P align=right>19</P></TD></TR>
<TR>
<TD>
<P>18</P></TD>
<TD>
<P>������ ������ �� ������� ��������</P></TD>
<TD>
<P align=right>189</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>19</P></TD>
<TD>
<P>������ ������ ������������ ������</P></TD>
<TD>
<P align=right>-1 774</P></TD>
<TD>
<P align=right>-815</P></TD></TR>
<TR>
<TD>
<P>20</P></TD>
<TD>
<P>���������������-�������������� �������</P></TD>
<TD>
<P align=right>29 632</P></TD>
<TD>
<P align=right>14 245</P></TD></TR>
<TR>
<TD>
<P>21</P></TD>
<TD>
<P>������� �� ��������� ������</P></TD>
<TD>
<P align=right>-6 579</P></TD>
<TD>
<P align=right>-1 666</P></TD></TR>
<TR>
<TD>
<P>22</P></TD>
<TD>
<P>������� �� ���������������</P></TD>
<TD>
<P align=right>21 509</P></TD>
<TD>
<P align=right>7 061</P></TD></TR>
<TR>
<TD>
<P>23</P></TD>
<TD>
<P>����������� ������ (������� ����� �� �������)</P></TD>
<TD>
<P align=right>6 888</P></TD>
<TD>
<P align=right>3 615</P></TD></TR>
<TR>
<TD>
<P>24</P></TD>
<TD>
<P>������� (������) �� �������� ������</P></TD>
<TD>
<P align=right>14 621</P></TD>
<TD>
<P align=right>3 446</P></TD></TR></TBODY></TABLE>
<P class=Rubrika>���������� �� ������ ������������� ��������,<BR>�������� �������� �� �������� ������������ ����<BR>� ���� ������� �� 1 ������� 2005 ����</P>
<TABLE cellSpacing=0 cellPadding=0 width="95%" borderColorLight=#cccccc border=2>
<TBODY>
<TR vAlign=top align=middle>
<TD vAlign=center width=42 height=39>
<P align=center><B>&#8470; �/�</B></P></TD>
<TD vAlign=center width=0>
<P class=bold>������������ ����������</P></TD>
<TD width=88>
<P class=bold align=center><STRONG>������</STRONG> �� �������� ����</P></TD>
<TD width=88>
<P class=bold align=center><STRONG>������ �� ��������������� �������� ���� �������� ����</STRONG></P></TD></TR>
<TR>
<TD height=0>
<P align=center><STRONG>1</STRONG></P></TD>
<TD>
<P align=center><STRONG>2</STRONG></P></TD>
<TD>
<P align=center><STRONG>3</STRONG></P></TD>
<TD>
<DIV align=center>
<P><STRONG>4</STRONG></P></DIV></TD></TR>
<TR>
<TD>
<P align=center>1</P></TD>
<TD>
<P>����������� �������� (�������), ���. ���.</P></TD>
<TD>
<P align=right>153 565</P></TD>
<TD>
<P align=right>146 667</P></TD></TR>
<TR>
<TD>
<P align=center>2</P></TD>
<TD>
<P>����������� �������� ������������� ����������� ������� (��������), �������</P></TD>
<TD>
<P align=right>24,8</P></TD>
<TD>
<P align=right>58,8</P></TD></TR>
<TR>
<TD>
<P align=center>3</P></TD>
<TD>
<P>����������� �������� ������������� ����������� ������� (��������), �������</P></TD>
<TD>
<P align=right>11,0</P></TD>
<TD>
<P align=right>11,0</P></TD></TR>
<TR>
<TD>
<P align=center>4</P></TD>
<TD>
<P>��������� ������ �� ��������� ������ �� ������, ������� � ������������ � ��� �������������, ���. ���.</P></TD>
<TD>
<P align=right>9 022</P></TD>
<TD>
<P align=right>1 805</P></TD></TR>
<TR>
<TD>
<P align=center>5</P></TD>
<TD>
<P>���������� �������������� ������ �� ��������� ������ �� ������, ������� � ������������ � ��� �������������, ���. ���.</P></TD>
<TD>
<P align=right>9 022</P></TD>
<TD>
<P align=right>1 805</P></TD></TR>
<TR>
<TD>
<P align=center>6</P></TD>
<TD>
<P>��������� ������ �� ��������� ������, ���. ���.</P></TD>
<TD>
<P align=right>740</P></TD>
<TD>
<P align=right>267</P></TD></TR>
<TR>
<TD>
<P align=center>7</P></TD>
<TD>
<P>���������� �������������� ������ �� ��������� ������, ���. ���.</P></TD>
<TD>
<P align=right>740</P></TD>
<TD>
<P align=right>267</P></TD></TR></TBODY></TABLE>
<TABLE cellSpacing=0 cellPadding=0 width="95%" border=0>
<TBODY>
<TR>
<TD height=0>
<P class=Main>������������ ���������</P></TD>
<TD>
<P class=&#171;Main&#187; align=right><STRONG>�������� �.�.</STRONG></P></TD></TR>
<TR>
<TD>
<P class=Main>������� ���������</P></TD>
<TD>
<P align=right><STRONG>������������ �.�.</STRONG></P></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>


</body>
</html>

