<!--
ID:35451178
TITLE:����� "������" �������� �� ��������&#8211;������� 2005 �.
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:16
ISSUE_DATE:2005-08-31
RECORD_DATE:2005-10-25


-->
<html>
<head>
<title>
35451178-����� "������" �������� �� ��������&#8211;������� 2005 �.
</title>
</head>
<body >


<TABLE cellSpacing=1 cellPadding=7 width=610 align=center border=1>
<TBODY>
<TR>
<TD vAlign=center width="7%"><B>
<P align=center>&#8470; �/�</B></P></TD>
<TD vAlign=center width="66%"><B>
<P align=center>���� ��������</B></P></TD>
<TD vAlign=center width="28%"><B>
<P align=center>���� ����������</B></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>1</P></TD>
<TD vAlign=center width="66%">
<P>���������� ���������� � ����������� �������� ������ � �����, ����������� �� ����������� � ������������������. ��������� �� ����������� ������ ���� � �������� �������</P></TD>
<TD vAlign=center width="28%">
<P>8&#8211;9 ��������</P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>2</P></TD>
<TD vAlign=center width="66%">
<P>���������� ����������, ���������� ������������ � �����������, �������� (� ��� ����� �����������) � ��������� ������������� �����. ��������������� ���������� �����������</P></TD>
<TD vAlign=center width="28%">
<P>13&#8211;15 ��������</P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>3</P></TD>
<TD vAlign=center width="66%">
<P>����� ������� ��������������� ������������ � ���������� ��������������</P></TD>
<TD vAlign=center width="28%">
<P>15&#8211;16 ��������</P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>4</P></TD>
<TD vAlign=center width="66%">
<P>������� � ������������� �������</P></TD>
<TD vAlign=center width="28%">
<P>19 ��������</P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>5</P></TD>
<TD vAlign=center width="66%">
<P>����� � ������� ��������������� � ��������� ������������. ������������ ������������</P></TD>
<TD vAlign=center width="28%">
<P>19&#8211;21 ��������</P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>6</P></TD>
<TD vAlign=center width="66%">
<P>�������� ������������� � �������� �������� � ������. ����� ��������� � ����������� ����������������</P></TD>
<TD vAlign=center width="28%">
<P>22&#8211;23 ��������<B> </B></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>7</P></TD>
<TD vAlign=center width="66%">
<P>����� � ������������� ����������� �������� � ������������� ��������� ���� � ���������� ���������</P></TD>
<TD vAlign=center width="28%">
<P>28 ��������</P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>8</P></TD>
<TD vAlign=center width="66%">
<P>������� � ����������� ���������� ����� ������ �� �������� ����������� ������������� � �������. ���������� ������� � ������������ ������������</P></TD>
<TD vAlign=center width="28%">
<P>29&#8211;30 ��������</P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>9</P></TD>
<TD vAlign=center width="66%">
<P>������������� ������ �������� � ��������� �����. ������� �� �������� ������������� ������ � ���������� ���������<BR>��������� �����, �� �����������. ������������ ���������</P></TD>
<TD vAlign=center width="28%">
<P>4&#8211;6 �������</P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>10</P></TD>
<TD vAlign=center width="66%">
<P>����� ������� � ����������� ��������������� �����������<BR>(���������) �������, ���������� ���������� �����,<BR>� �������������� ����������</P></TD>
<TD vAlign=center width="28%">
<P>6&#8211;7 �������</P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>11</P></TD>
<TD vAlign=center width="66%">
<P>���������� ��������� � ��������� ���������� ������������� �����. �������� �� �� �������������. ���������� ��������</P></TD>
<TD vAlign=center width="28%">
<P>11&#8211;13 �������</P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>12</P></TD>
<TD vAlign=center width="66%">
<P>����� ������ ������� � ������ ���������-�������������<BR>������������ �����������-��������</P></TD>
<TD vAlign=center width="28%">
<P>13&#8211;14 �������</P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>13</P></TD>
<TD vAlign=center width="66%">
<P>����� � �������� ������� �� � �������� ����������������.<BR>������������ ������� ��������</P></TD>
<TD vAlign=center width="28%">
<P>19&#8211;20 �������</P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>14</P></TD>
<TD vAlign=center width="66%">
<P>���������� ���������� � ����������� �������� ������ � �����, ����������� �� ����������� � ������������������. ��������� �� ����������� ������ ���� � �������� �������</P></TD>
<TD vAlign=center width="28%">
<P>26&#8211;27 �������</P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>15</P></TD>
<TD vAlign=center width="66%">
<P>���������� ����������� ������� ��������� (���������� ���������) �� ������������ � � ������������</P></TD>
<TD vAlign=center width="28%">
<P>27&#8211;28 �������</P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=center>16</P></TD>
<TD vAlign=center width="66%">
<P>������ � �������������. ����������� ������������� ��������� ���������. ����� �������� ������������ ������</P></TD>
<TD vAlign=center width="28%">
<P>31 ������� &#8211;</P>
<P>3 ������</P></TD></TR></TBODY></TABLE><B><I>
<P align=justify>( (095) 436-05-34; 210-49-11</P></B></I>


</body>
</html>

