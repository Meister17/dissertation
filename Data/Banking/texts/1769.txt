<!--
ID:36820192
TITLE:����������� ������� ����������� ������� 
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:������ ���, ���������� �������������� ������ &#171;��&�&#187; 
NUMBER:9
ISSUE_DATE:2006-09-30
RECORD_DATE:2006-10-18


-->
<html>
<head>
<title>
36820192-����������� ������� ����������� ������� 
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgwhite>
<TD></TD></TR>
<TR class=bodycolor>
<TD>
<P align=justify><I>� ������ ������� ������������� �����-���������� �������� ��������� ������ �� �������� �������� �� ����� &#8212; ���� �� ����� � �������� ������� ������ ��� �������� �������, � ��������� ������ ����� �������� ���������� ����� ����� �� �����. </I></P></TD></TR>
<TR class=bgwhite>
<TD>
<P class=Main align=justify>����, ������� �� ����� ����� ������������ ����� �� ������� ������. �������, ���� � �������� � ������ �� ������ �� �����, �� �������� � ����-�������� �������� ���� ������� ������ � ��������� �������. ����� �������������� ���������� �������� ��������� ����� ����� � � ������ ������� ������� ������. ���� �� ���� ���� ��� � ��������� �������, �� ��������� ������������ �� ������ �������� �������� ���������. ������ �� ������ ������������� � ������ � ������������� ������ �� ������� ������� �������. ������������ ������������� � ���������������� �����, � ������� �������� � �� �������� ������� ������, � ������������ ������ ������-�������� �������� ������, ���, �������, ���� �� ��������� ���������. 
<P class=Subheader align=justify>������������� ����� ����� � 2005 ���� 
<TABLE cellSpacing=0 cellPadding=0 width="90%" align=center border=1>
<TBODY>
<TR class=bodycolor>
<TD rowSpan=2>
<P align=justify><STRONG>������������</STRONG></P>
<P align=justify></P></TD>
<TD colSpan=3>
<P align=justify><STRONG>�������</STRONG></P></TD></TR>
<TR class=bgcolor1>
<TD>
<P align=justify>���. �2 ����� �������</P></TD>
<TD>
<P align=justify>� ��� ����� � �������</P></TD>
<TD>
<P align=justify>� % � 2004 ����</P></TD></TR>
<TR>
<TD>
<P align=justify>���������� ���������</P></TD>
<TD align=right>
<P align=justify>43 609,2</P></TD>
<TD align=right>
<P align=justify>13 974,5</P></TD>
<TD align=right>
<P align=justify>106,3</P></TD></TR>
<TR>
<TD>
<P align=justify>����������� ����������� �����</P></TD>
<TD align=right>
<P align=justify>15 176,1</P></TD>
<TD align=right>
<P align=justify>4 282,6</P></TD>
<TD align=right>
<P align=justify>99,2</P></TD></TR>
<TR>
<TD>
<P align=justify>������-�������� ����������� �����</P></TD>
<TD align=right>
<P align=justify>3 946,1</P></TD>
<TD align=right>
<P align=justify>1 295,4</P></TD>
<TD align=right>
<P align=justify>106,2</P></TD></TR>
<TR>
<TD>
<P align=justify>����� ����������� �����</P></TD>
<TD align=right>
<P align=justify>6 021,8</P></TD>
<TD align=right>
<P align=justify>1 234,3</P></TD>
<TD align=right>
<P align=justify>103,4</P></TD></TR>
<TR>
<TD>
<P align=justify>����������� ����������� �����</P></TD>
<TD align=right>
<P align=justify>8 964,4</P></TD>
<TD align=right>
<P align=justify>2 987,4</P></TD>
<TD align=right>
<P align=justify>108,5</P></TD></TR>
<TR>
<TD>
<P align=justify>��������� ����������� �����</P></TD>
<TD align=right>
<P align=justify>3 385,5</P></TD>
<TD align=right>
<P align=justify>1 274,4</P></TD>
<TD align=right>
<P align=justify>106,8</P></TD></TR>
<TR>
<TD>
<P align=justify>��������� ����������� �����</P></TD>
<TD align=right>
<P align=justify>4 079,8</P></TD>
<TD align=right>
<P align=justify>1 665,3</P></TD>
<TD align=right>
<P align=justify>104,8</P></TD></TR>
<TR>
<TD>
<P align=justify>��������������� ����������� �����</P></TD>
<TD align=right>
<P align=justify>886,9</P></TD>
<TD align=right>
<P align=justify>265,2</P></TD>
<TD align=right>
<P align=justify>106,8</P></TD></TR></TBODY></TABLE>
<P class=Main align=justify>
<P class=Main align=justify>����� �� ������� ������ �������� ���������� ����� ����� ������ �������, ������� �� ����������� �������� ������������, ���������� ������ ������� ��� �� ����� ������������. �� ����, ����������� ��������, � ������� ����� ������������� ��������� ��������: ������������ �������� ������� � ������� �������� �������� �� ������� � ������������� ����� ������������, ������� � ������������ �������� ����� ���, �������, � ���� �������, ����� ������ ���� ����� ������������. 
<P class=Main align=justify>���������, �� ���������� ������� ����� � ������, �� ������������ ������� ���������� ������, �� ������ �������������� �� ����� �������� ����������. ������ ���������� ������������, �������� �� �� ���, ������ �������, ������������ ����� ������������� ��������. �� ������ ����� ������, ������ �� 2005 ��� ��������� ������������ (� ������ ��� ����) ������� �� �����, ��� ��������������, � ����� � ��� ����: � 17,8 �� 52,8 ����. ������. � ���� ����� �������� �������� �� 2006 ��� ������������ ���������� �� ���������� &#8212; �� ������ ������, �� I ������� 25-���������� ����, ����������� ��� �����, ��� ���������. �� � �� 
<P class=Main align=justify>2007-� ���������� �� ���� ����� �������, ������������ � �������� ������������� �������� ���������� ��������. 
<P class=Main align=justify>����� ��� ����������� �� ����� ����� ����� ������� �������� �������. ��� ����� �� �������, � ����������� ����������� ������ � 2005 ���� �������� ������������� �������� �� ����� � ������������ ����� ����� ��������. � ������, ������� ��������� � ��������� ������� ���� �� �� �������, ��, �� ������ ������, �� ���������� � ��������, ���� ����� ����� ���������� ����������� �������. ��������� ����� ������������� �������� � ���������, �� � ����� ���� &#8212; ����� 8,5%. ��� ���, ��� �������� � ����� ������������ &#8212; �������������� ����� ������ �� ������� ���������� ������������, � ����� �� ����� ����� ���� ��������������, ������������� ����� � ���������� � 2005 ���� ������� ����� �� 6,3%. � ������������� ������� (������ ����� �� �������� �������) ��� � ����� ��������� ����� �� 30%. 
<P class=Main align=justify>� ����� ��, � 25 �������� ������ � 2005 ���� ����� ��������� ������������� �������� �� ��������� � 2004-�. �� ������� �������� ��������� ���� ���������� ��� ����: �� ������ ����� ���������� ����� ����� �� ��������� � ����������� �������� �������� ���� ����� ��� �� 5%. �����, �������, ��������� �� ��������� ������, ����� ����������� �������������� ���������� � �������, &#8212; ������ �� ����� ����� ��������� �������� ������ ����� �� ��������, � ����� &#8212; ������� ��������������� ������������ ����������� &#8212; ����� �������� �������. 
<P class=Main align=justify>�������, ��� ������ ����� �� ��� ��� ����������� ����� �� ��������� � ������������ ������� ����� � ���������� ����� &#8212; � ��������� ������ &#8470;214 ��� ����� ����� ������������� (�� ������ ������ &#8212; ����������). ������� ���� ������� � ����������� ������� �������� ��������� &#171;��������&#187; �� ������� � ������ �������� ���������� �����. ���� �� ������� ������ ������� �� ��������� ����� ����� �����, ����� ���� ������������, �� ������� ��� �������� ����� ��������������� �����. �����������, ����� ������� ������� ����� �� ������������� ����� �����������, ���� � ������ ��������������� ��������� �������������. 
<P class=Main align=justify>�� ������������� �������� �������� �������, � ���� ����� ��������� �� ����� �������� &#171;�������&#187; �����, ������� ����������� � ��������� ���������� ������� � �������������, �� ���� ����� �� ����������. ������, ���������� ���� ������������� ��� �������� ��������������, ���� ������� ����� �� ����� ���������������� ���������. ���� �� ������� ������������� ���������� ����������� �������. 
<P class=Subheader align=justify>������� ���� AK&amp;M. ����������� ��������� �������� 
<TABLE cellSpacing=0 cellPadding=0 width="90%" align=center border=1>
<TBODY>
<TR class=bgcolor1>
<TD>
<P align=justify><STRONG>������������</STRONG></P></TD>
<TD>
<P align=justify><STRONG>�������</STRONG></P></TD>
<TD>
<P align=justify><STRONG>�������</STRONG></P></TD>
<TD>
<P align=justify><STRONG>���� </STRONG></P></TD></TR>
<TR class=bodycolor>
<TD colSpan=4>
<P align=justify><STRONG>�����������</STRONG></P></TD></TR>
<TR>
<TD>
<P align=justify>��� &#171;��������&#187;</P></TD>
<TD align=middle>
<P align=justify>�+</P></TD>
<TD align=middle>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>������� 2005 ����</P></TD></TR>
<TR>
<TD>
<P align=justify>��� &#171;������������&#187;*</P></TD>
<TD align=middle>
<P align=justify>�+</P></TD>
<TD align=middle>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>������ 2005 ����</P></TD></TR>
<TR>
<TD>
<P align=justify>��� &#171;����� ��Ш�����&#187;</P></TD>
<TD align=middle>
<P align=justify>�</P></TD>
<TD align=middle>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>���� 2006 ����</P></TD></TR>
<TR>
<TD>
<P align=justify>��� ��� &#171;��������� �������&#187;</P></TD>
<TD align=middle>
<P align=justify></P></TD>
<TD align=middle>
<P align=justify></P></TD>
<TD>
<P align=justify>���� �������� �������� ����</P></TD></TR>
<TR>
<TD>
<P align=justify>��� &#171;�������������&#187;</P></TD>
<TD align=middle>
<P align=justify>�</P></TD>
<TD align=middle>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>��� 2006 ����</P></TD></TR>
<TR>
<TD>
<P align=justify>��� &#171;���������&#187;</P></TD>
<TD align=middle>
<P align=justify>�</P></TD>
<TD align=middle>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>������ 2005 ����</P></TD></TR>
<TR>
<TD>
<P align=justify>��� &#171;����������&#187;</P></TD>
<TD align=middle>
<P align=justify>�</P></TD>
<TD align=middle>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>��� 2006 ����</P></TD></TR>
<TR>
<TD>
<P align=justify>��� &#171;��������&#187;</P></TD>
<TD align=middle>
<P align=justify>�+</P></TD>
<TD align=middle>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>���� 2006 ����</P></TD></TR>
<TR>
<TD>
<P align=justify>��� &#171;�������&#187;*</P></TD>
<TD align=middle>
<P align=justify></P></TD>
<TD align=middle>
<P align=justify></P></TD>
<TD>
<P align=justify>���� �������� �������� ����</P></TD></TR>
<TR>
<TD>
<P align=justify>��� &#171;������&#187;</P></TD>
<TD align=middle>
<P align=justify>B++</P></TD>
<TD align=middle>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>���� 2005 ����</P></TD></TR>
<TR class=bodycolor>
<TD colSpan=4>
<P align=justify><STRONG>�������������� ��������</STRONG></P></TD></TR>
<TR>
<TD>
<P align=justify>��� &#171;�������������� �������� &#171;��������&#187;</P></TD>
<TD align=middle>
<P align=justify>�</P></TD>
<TD align=middle>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>������� 2005 ����</P></TD></TR>
<TR>
<TD>
<P align=justify>��� &#171;��� ������&#187;</P></TD>
<TD align=middle>
<P align=justify>�</P></TD>
<TD align=middle>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>��� 2006 ����</P></TD></TR>
<TR class=bodycolor>
<TD colSpan=4>
<P align=justify><STRONG>���������� &#171;������� �� &#8212; ��&amp;M&#187; �������</STRONG></P></TD></TR>
<TR>
<TD>
<P align=justify>�. ������</P></TD>
<TD align=middle>
<P align=justify>�++</P></TD>
<TD align=middle>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>��� 2006 ����</P></TD></TR>
<TR>
<TD>
<P align=justify>���������� �������</P></TD>
<TD align=middle>
<P align=justify>�+</P></TD>
<TD align=middle>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>������� 2005 ����</P></TD></TR>
<TR>
<TD>
<P align=justify>�������� �������</P></TD>
<TD align=middle>
<P align=justify>�+</P></TD>
<TD align=middle>
<P align=justify></P></TD>
<TD>
<P align=justify>���� �������� �������� ����</P></TD></TR>
<TR>
<TD>
<P align=justify>��������� �������</P></TD>
<TD align=middle>
<P align=justify>�+</P></TD>
<TD align=middle>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>��� 2006 ����</P></TD></TR>
<TR>
<TD>
<P align=justify>���������� ����</P></TD>
<TD align=middle>
<P align=justify>�+</P></TD>
<TD align=middle>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>������� 2005 ����</P></TD></TR>
<TR>
<TD>
<P align=justify>������������ ����</P></TD>
<TD align=middle>
<P align=justify>�</P></TD>
<TD align=middle>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>������ 2005 ����</P></TD></TR>
<TR>
<TD>
<P align=justify>����������� �������</P></TD>
<TD align=middle>
<P align=justify>�</P></TD>
<TD align=middle>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>���� 2005 ����</P></TD></TR>
<TR>
<TD>
<P align=justify>���������� �������</P></TD>
<TD align=middle>
<P align=justify>�++</P></TD>
<TD align=middle>
<P align=justify>����������</P></TD>
<TD>
<P align=justify>��� 2006 ����</P></TD></TR></TBODY></TABLE>
<P class=Main align=justify></P></TD></TR></TBODY></TABLE>


</body>
</html>

