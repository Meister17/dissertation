<!--
ID:37284032
TITLE:Building domestic asian bond markets
SOURCE_ID:3240
SOURCE_NAME:Banker
AUTHOR:Joseph Yam 
NUMBER:971
ISSUE_DATE:2007-01-31
RECORD_DATE:2007-02-14


-->
<html>
<head>
<title>
37284032-Building domestic asian bond markets
</title>
</head>
<body >


<P align=justify><FONT face="Times New Roman"><STRONG>Regional co-operation through the Asian Bond Fund initiatives will make intermediation between savings and investment increasingly effective in the region, says Joseph Yam</STRONG></FONT></P>
<P align=justify><FONT face="Times New Roman">A lesson that we learnt from the 1997-1998 Asian Financial Crisis is the importance of developing a deep and liquid domestic bond market, which can help reduce the corporate sector's reliance on financing through short-term bank borrowing.</FONT></P>
<P align=justify><FONT face="Times New Roman">At the same time, a significant part of the high savings in Asia, which are invested in assets of developed markets, have returned to the region in the form of short-term capital inflows, particularly in the form of bank lending and portfolio inflows. These sorts of inflows tend to be volatile. Thus, to improve the efficiency of financial intermediation in Asia and to develop a more stable source of funding for Asian borrowers, the region needs to develop deep and liquid domestic bond markets.</FONT></P>
<P align=justify><FONT face="Times New Roman">Therefore the question is, how can the goal of developing domestic bond markets be achieved? We believe that both regional co-operation and appropriate market development policies by local authorities are equally important.</FONT></P>
<P align=justify><FONT face="Times New Roman">As far as regional co-operation is concerned, the Hong Kong Monetary Authority (HKMA) proposed at the Executives Meeting of East Asia-Pacific Central Banks (EMEAP) Working Group on Financial Markets in 2002 to establish an Asian Bond Fund (ABF) to invest part of EMEAP economies' foreign reserves in Asian bonds. The HKMA chaired the EMEAP Working Group.</FONT></P>
<P align=justify><FONT><FONT face="Times New Roman">In June 2003, EMEAP launched ABF1, which invests in a basket of dollar-denominated bonds issued by sovereign and quasi-sovereign issuers in eight EMEAP markets: China, Hong Kong, Indonesia, South Korea, </FONT><FONT face="Times New Roman">Malaysia, the Philippines, Singapore and Thailand. It was a small but significant step forward &#8212; 11 highly varied economies of different sizes, with various economic structures and stages of economic and social development, cooperating for the first time to set up a bond fund and lay the foundation for promoting the development of regional and domestic bond markets in Asia.</FONT></FONT></P>
<P align=justify><FONT face="Times New Roman">The next, bigger step was ABF2, which extended the concept to bonds denominated in Asian currencies. There are two parts to this: the Pan-Asia Bond Index Fund (PAIF) and eight single-market funds. The PAIF is a single bond fund investing in sovereign and quasi-sovereign local currency-denominated bonds issued in the eight EMEAP markets, while the single-market funds invest in similar bonds issued in the individual markets.</FONT></P>
<P align=justify><FONT face="Times New Roman">So far, seven ABF2 funds have been successfully offered to the public, raising some $550m (to end-September 2006) from non-EMEAP investors. The PAIF was launched as a listed open-ended fund, while six single-market funds were either listed as bond exchange-traded funds (ETFs) or launched in the form of unlisted open-ended funds. The remaining two single-market funds were targeted for public offering around the end of 2006.</FONT></P>
<P align=justify><FONT face="Times New Roman"><B>Bond precursor</B></FONT></P>
<P align=justify><FONT><FONT face="Times New Roman">ABF2 has paved the way for broader investor participation in the Asian bond markets. The asset size of the launched ABF2 funds recorded growth of between 26% and 138% (to end-September 2006), despite rising interest rates in the US and uncertain interest rate outlook. The 33% growth of PAIF is satisfactory and compares favourably </FONT><FONT face="Times New Roman">with that of other bond funds in the region.</FONT></FONT></P>
<P align=justify><FONT face="Times New Roman">While the ABF initiatives have helped broaden investor participation in bond markets in the region, there continues to be room for local authorities to improve domestic market infrastructure and promote market development, with the initiatives at both regional and local levels complementing each other. In this regard, it is worth sharing the experience of Hong Kong. In particular, the HKMA has adopted a two-pronged approach to develop the local debt market, namely to set up a market for high quality Exchange Fund paper, and to build an efficient financial infrastructure.</FONT></P>
<P align=justify><FONT face="Times New Roman"><B>Hong Kong example</B></FONT></P>
<P align=justify><FONT face="Times New Roman">Before the 1990s, the debt market in Hong Kong was virtually non-existent, largely because of the absence of the need for the government to borrow amid large cumulative fiscal surpluses. In order to facilitate the development of a Hong Kong dollar debt market, the HKMA rolled out the exchange fund bills and notes programme in 1990. This initiative has been very successful in creating a yield curve for the Hong Kong dollar.</FONT></P>
<P align=justify><FONT><FONT face="Times New Roman">As of today, the yield curve has been extended up to 10 years and serves as a benchmark for debt instruments issued by other entities in the local market. In order to maintain a liquid market for </FONT><FONT face="Times New Roman">the Exchange Fund paper, the HKMA has also put in place a market-making system under which market makers are obliged to quote two-way prices. Currently, 26 banks in Hong Kong have been appointed as market makers, and tenders of exchange fund bills and notes of maturities between three months and 10 years are held regularly.</FONT></FONT></P>
<P align=justify><FONT face="Times New Roman">These arrangements have been effective to help promote the liquidity of the exchange fund paper and enhance the credibility of the Hong Kong dollar yield curve. Indeed, overseas borrowers have remained the largest issuers in our market, with outstanding debts reaching an equivalent of $42bn at the end of September 2006, from just $1.3bn at the end of 1997.</FONT></P>
<P align=justify><FONT face="Times New Roman">Another important area of our work is the building of an efficient financial infrastructure for the bond market. Our objective is to build a multi-currencies and multi-product payment and settlement platform to make Hong Kong the region's payment and settlement hub. We believe that a safe and efficient payment and settlement system is crucial to the development of the debt market. Our system will serve not only local investors but also investors elsewhere in the region to facilitate fund transfers and delivery versus payments when they buy or sell debt instruments.</FONT></P>
<P align=justify><FONT><FONT face="Times New Roman">The HKMA has therefore been working actively on linkages between the Central Moneymarkets Unit </FONT><FONT face="Times New Roman">(CMU), which is our key custodian system for debt instruments in Hong Kong, and other custodian systems in the region and international custodian systems such as the Euroclear and Clearstream. These linkages have been instrumental in fostering cross-border bond trading and investment.</FONT></FONT></P>
<P align=justify><FONT face="Times New Roman"><B>Efficient settlement</B></FONT></P>
<P align=justify><FONT face="Times New Roman">With the seamless interfaces between the CMU and the Real Time Gross Settlement (RTGS) systems in Hong Kong for the Hong Kong dollar, US dollar and euro, all transactions in debt instruments denominated in any of these three currencies and under the custodianship of the CMU can be settled in Hong Kong on a delivery versus payment basis in an efficient way.</FONT></P>
<P align=justify><FONT face="Times New Roman">The ABF initiatives provide a cost-effective way for investors to gain exposure to debt securities issued in Asia, and our market development strategy helps remove market frictions and unnecessary restrictions, and keep up with the best practices adopted in other well-developed markets.</FONT></P>
<P align=justify><FONT face="Times New Roman">We believe regional co-operation through the ABF initiatives and efforts made by local authorities, such as those in Hong Kong, will make intermediation between savings and investment increasingly effective in the region.</FONT></P>
<P align=justify><FONT face="Times New Roman"><I>Joseph Yam is head of the Hong Kong Monetary Authority</I></FONT></P>


</body>
</html>

