<!--
ID:38436937
TITLE:�������� �������� ������� &#150; ��������� �������
SOURCE_ID:10030
SOURCE_NAME:�����: ������� ����. ����� ���
AUTHOR:����� ������ &#150; �.�. ������, ����. ����. ����, ����� ���
NUMBER:2
ISSUE_DATE:2007-04-30
RECORD_DATE:2007-11-20


-->
<html>
<head>
<title>
38436937-�������� �������� ������� &#150; ��������� �������
</title>
</head>
<body >


<P align=justify>�������� �������������� (�������������� ������������� � ������� �����, housing finance), � ������������� ��������� ������������, &#8211; ���� �� �������� ���� ����������� �������, ������� � ��������� ����� ��������� � ��������� ������������ ����� ��� � ����������� �������� ������� (� ������ 1990-� �����), ��� � � ������� � ������������� ������ (� ��������� �����������).</P><B>
<P align=justify>�������� ��������� �������� �������� ����� �������</P></B>
<P align=justify>� ����������� ����������� �������� ����� ������������� �� ������� ��������� 40% ���, � � ��������� ������� (���������, �������������� � ���) &#8211; 70%. � ����������� ���� ���������� ���������� ����� 100% ���. � ������� � �������� ������� �������������� ����� ��������� ������� ���� ������ ���������� ������������ �, ��������������, �������������. � ������� � ������� ������� �������������� ����� (�������� � ������), ��������, ��� ���������� ����� �� ����������. ���� � ������� � ������������� ������ ���������� ������ ������������� �� ������� ������������ �������, � ��������� ������� ���� ������ �� �������� 10% ��� (�����, ������� � �������) � ���� 20% ��� (����, ����� �����, �������� � ���).</P>
<P align=justify>��������� ����� �������� ����� ��������� �������������� ����� ������� ����� �������� ���������, ���������� �������������, ���������� ����������� �, ��� �������� �����, �������� � ������������ ��������. ����������� �������� �������� �������, ��� ������������� ���������� ������ (������� ������������ ��������������� �������� ������) ���� ������ � 1980-� ����. � 1990-� ����, ����� ������������� ������� � �� ���������� ������ � ������ ����� ���������, ��� �������� �� ����� ���������� ���������� �����, ����� �������� ����� ��� �������� &#8211; � 17% � 1990 �. �� 42% ��� � 2004 �.</P>
<P align=justify>����� ��� �� ������� ���� ���������� ����� ���������� � ������������� ������� � �������� ������� ����� � ������� ����������� �������� (�����, �����, ����� ����� � ��������). ������, ����� ����, �������� � ����� ������ ��������������, ��� ��������� ��������� ������������ � ������������ �����. ���� ������ ��� ���������������, ������� �������, ��� ��������������� �����, �� ������ ���������� ����������� ��� ������� �������� ����������� ������, ������� ����� ������������ � �������� ������ ��� ����������, ��� ������� ��������� ��������������� ������� � �����. ��������������� ������ ������������ ����� ����� � �������������� ������������ � ������� ���������������, � ���������, �������������� ������������� �����, ������� ���� � ��� ��������� (�� ����� � ����), ������ �� ���������������� ���� ��������� �� ���������� ������� (�����, ���������� � ���) � ����������������� ������ ��������������� ������� �� ������� (��������� � ���).</P>
<P align=justify>����� �� ����������� ����� �������������� ��������� ������������� ����� ��������� ��� �� �����, ��������������� �������� ������� ����� ���������, ���������������� ���������� (����������� � ������������) � ������������ ������ ���������� � ��������� ����������� ��������.</P>
<P align=justify>�� ������ ���������� ������������� ������������ ������������� ������ ����������� ����� ��� ��������� ������������� � ������� �����, ��������, � �������������� &#8211; ������������ �������� (building societies), �� �������, ������� � �� ������ ������������� ������� &#8211; �������� ����� (housing banks). ��� ������ ������ ����������� ������� ���������� �������� � ��������� ������������, ��������������� �����������.</P>
<P align=justify>� ���� ���������� ���������� ������������� 1980-� ����� � �������� ������� ���� ��������� ����� ����� ��������� ��� �����������, � �� ���� ������ ������������ ���������� ������������� ������ ��� ����������� ������� ��� ��������� ��������������. ������ � ����������� ����������� �������� ����� ����� 70% ��������� �������� ������������ ������������ � �������������� �����. � �������� � ��� ������� ���� ������ ������������ �� �������� ������������ ������������������� ���������� ������� � ����������.</P>
<P align=justify>������������� ������ �� ��������� ����� �������� ���������� ���������� ��������, � ��� ����� ����������� ���������� ������������ ��������� �������� � �����������, ��������� � �������� � ����������������� �����.</P>
<P align=justify>����� ������������ � �� ��������� ������ ������ ������������� �����, ���� ����� ����� ������ ���� ������ � ������������������ ��������� (���, ��������, �������� �������������� ����� � �����, �������� Sofols � �������). � ���� ������������� ����� (��������, ����� ����� � �������) ������� ���������� �������������� ����� �������� ������ ������, ������ ��� �����, ����������� �� ���� ������� �� ��������, ������� ������������� ��� �������������� ��������� ������������� � ������� ��������. ������ ����� ������ ���� ������ ������ �������������������, � ������ � ���� ������� ����������� �������������� ��������� ������������� �� ���� ������ �������.</P>
<P align=justify>�� ������ ������� ���� ������ ������� ������������� ���������� �������� ����� ��������� ���������� (�������). �� ��������� ������� ������ �� ��� ���������� ����� 40% ����� ��������. ����� ���������� ������ ������������ ���� ����� � ������� � ���������� ���������� (��������, � ����� � ������ �� ���� � ����� ����� �������� ��������� 50%).</P>
<P align=justify>��������� �������� ���������� ��� ����� ��������, � ����� ���������, ������� ��������� � ������ &#8211; ��� ����� ������� � ��������������. ���, � �������������� ������������� ��������� ��������� ����� ��������� ���������. � ���, ��� �� ��������� ������� ������������ 30-������ ������� � �������������� ��������� (level payment mortgage), �� ������� ������������� ������� (��������� �� ��������� � ������ � ���� ��������� �������� ����� �����) �������� ������� �� ���� ���������� ����� �������������, � ��������� ����� ����������� ������� ���� ��������� ��������� � ������������ ���������� ������� � ������ ������ ���������. ������� � ������������ ������� (adjustable rate mortgage &#8211; ARM), ������� ��������� ��������� �������� ���������� ����� ������� � ���� ��������� ��������� �����, ����� ��������� ����� ����������� &#8211; � 2004 � 2005 �. �� ��� ����������� 20% ���� ����� ����� ��������� ��������.</P>
<P align=justify>� ��������� � �������������� ������� ��������������� ��������� ������ ��������� �������, ��������������� ����������� ���������� ��������, ��������� ������������� ����������� �������� ������� � �������� ����� � �������, �� �������� ������� ������� ������ ���������� �������� � ���������� (shared appreciation loan). � ��������� ������� � ������ ������� ��������� ������� � ����� ��������������� ������ ��������� (����� 50 ���).</P>
<P align=justify>��������� �� ���� ��������� ���� ����������� ��� ����, ����� ���������� ���������� �������� � ����� � �������� ����� ��� �� ����. ������ ���� ����������� ����������� � ��������� ������� ���� &#8211; ����������� ���� �������� ������� � ��� � ��������������. � �������, ��� ���� ������� �������� ���������, �������� ��������� (reverse mortgages), �� ���� ������ ����� ��� ������ ������������ � ��������� ���������� ������ �� ����������, ��������� ������������� ����� �������� ������� �����, �� �������� ���� ������������� � �� ��������� �� ������ ����� ����������.</P>
<P align=justify>�������� ����� ����� ��������������� �� ��������� (��������) ��� ������� (����� ��������) ����������. ������ ��������� � �������-�������������� ���������� ������������ ����� �����������, ��� ��������� (��������) ��������� (covered bonds &#8211; ��), ������ ������, ������������ ��������� �������� ��� ����� ��������� �������� (mortgage-backed securities &#8211; MBS), � ���������, ����������� �������� ����������� ������������������� ������� (liquidity facility bonds). � ������ 2/3 ���������� �������������� ���������� �� �������� ��� ����������� �������������� ���������, 20% &#8211; �� �� � ����� 2% &#8211; �� MBS. ���� � ����� ����������� �� � MBS ������ �����, �������� ����� � ���� ������ �������� ����������������� ��������� (���������� ����� � ��������� ��������).</P>
<P align=justify>� ������� �� ������ � ��� �������� ����� �������������� ������� ��������� � ����� ���������. ����� 54% ������������� �� �������� ������� ����������������� � ��� 4% ������������� �� ���� ������� �������� ������ ����� ����� ������������ ����������� &#8211; Fannie Mae � Freddie Mac<SUP>1</SUP>. �������� �������� ����������� ������ ��������� ������� (Federal Home Loan Banks), ������� ������������� ������� � ���������� ������������ ���������� �����������, ����������� �������� ������ ������, ����������, ��� � ����� ����� 2/3 �������������� ������� ������������ ��������� �����. ������������ ����� MBS � ������ ����� ��������� ����������� �������� �������� �����, ������������� �� ���� ���������.</P>
<P align=justify>���������� ��������� ������ ������������� ����� ��������� � �������� �������������� � ���. ������ ����� ��� ������� � �������� ��������� � ���������� ����� ��������� ��� ������� ������ �� ������������������ ������ � ������������� �������. ����������� �� ���� ����� ������������ ������������ ����������� ������������ ����������� �������� �������� � ����������������� MBS. ������ ������ &#8211; ������������ ������� ������������� ������������ ���������������� ������� � ������������ ���������� ��������� ��� �������� �������. ����� ���, ������������ ������, ��� ����� ���������� ����� ��������� � �������������� ������� � ������������ ����������� ��������� �����������, &#8211; ��� �����.</P>
<P align=justify>���� � ������� � ������������� ������ �������������� ������� �������������� � �������� ����������� ������� � ������������������� �����������, �� ������ �� ��� ������ ����������� ������� � ����� ���������. ���, � ��������� �������, ��������, � ����� � �������, ������������ ���� �������������� ���������� �� ��, � � ���� ��� ��������� 70% ����� �������������� �������. ���������� � ���� MBS � ������� � ������������� ������ (� ���������, � ����� �����, ������� � ���). �� ���� ���������� ��������� ����������� (liquidity facilities) ������������� ����� 20% ��������� ������ � �������� � ��������.</P>
<P align=justify>��� �������� ���������� �������� ���������� ��������������, �� ��� ������������ ����� ��������� &#8211; �������������� � �����������, ������� ����� ������������� ���� � ������, ��������� ��� ���������� ������������ ���� ��������� ��������������� ���������.</P>
<P align=justify>� �������� ������������� ���������� �������������� ���������� ��� ���������. ��-������, ��� ������������ � ������������ �������������, ��������������� � ���������� ������-2. �������� ����� ���������� ����� ������� ������� ������� �� ����������� ���������� ������� ��� ������� ���������� ������������� ���������� ������ ��������, ������� ����� ������ ������������� ��� ����������� ��������� ��������. � ���������, ����������� ����������� �� ������ ��� ������� ������ � 50% � ��������� ����� �� 35% ��� ������������������� ������� (standardised approach) � ������ ���������� ����� � �� 20% � ����� ��� ������� ������, ������������ ������ �� ������ ���������� ��������� (internal ratings-based approach &#8211; IRB). �������� ��������� ���������� ������-2 ����� �������������� ���������� �������� ��������� �������� � �������� ������������ ����������� ������� ������, ����������� ��������� ���� � ������� IRB, � ����� ���������� ��������������.</P>
<P align=justify>��-������, ������� ������������� ����������� � ����������� ���������� ������ ������������ � ��������� ����������. ��� ����������� ���, ��� � �������� ��� ����� ��������� ������������� ������� ��������� ��������� � �������� ������������ ��������� � ������ ������� ������������������ (sub-prime lending) ������ �� ������ ����������� �������� �������� � ������ ���� ��������� � ���� �����.</P>
<P align=justify>���������������� �� �������� ������� � �������������� ��� ������ ����������������� �� ������ ����������� ���������������� �������� ��� ����������� �����������. � ��� �� ������ ������ ��� ������ ��������� ��������� ��������� ����� ������� ����������������, ���������������� ��������� ������������ ��������� � ������ ������� ������������������ (sub-prime lenders). ��������������� ������������� ����������� �������� ���������� � ����������� � ��������� ����������� ������������ ������������ ���������.</P>
<P align=justify>����� ���������� ������������ ��������� ����� ������� ������������� ��������� ��������� (����� ��� �������� � ���������� �����). ������������� ������������ �������� ����� � �������� ���������� ������ � ������ ������� ����������, � ����� ��������� ����������� �� ��������������� �����.</P><B>
<P align=justify>���������� ���������� ����� ��</P></B>
<P align=justify>� ����� 2006 �. ��� ���������� ������ ��� ����������� �������� &#8211; �� �������������� ������� (Mortgage Funding Expert Group &#8211; MFEG) � �� ������� ����� ���������� ����������� � ������������� (Mortgage Industry and Consumers Dialogue Group &#8211; MICDG) &#8211; ������� ����� ����� ������. �������� ���� ����� ����� ����������� ������� ������������, ����������� ����������� ��������� ����� ���������� � ���� 2005 �. &#8220;������� �����&#8221; &#8211; ������������ ���������� ������������, ��������������� ����������� �������� ��������� ����������. � ���� &#8220;������� �����&#8221; ������� ���� �������� ������ ��������� ������������ �� ������� ������� � �������� �������� ����������� ������� ������������.</P>
<P align=justify>�������� ������ ���� ����� ��������������� �������� �� �������� ���������� ��������� ������ � ������ ����������� ����������� � �������� ������� ����������� ����� � ��������� ������������� �������� (Lisbon Agenda)<SUP>2</SUP>, � ����� ���� �����, ��� ����������� ��������� ����� �������� ���������� ����������� � ������������. �� ���� ���� �������� ������ ���� ������ ��� ����������� ���������� �������� �� ��������� ���������.</P>
<P align=justify>������ � ��� �������� ������ ������ � �������� ������������ ������������ ����� � ��������� ���������� ������ ������� �������������� ������������� ������� ��� ���������� ��������� ������ �����-������ ���� �����������. ��������� ����� ������������������ �������� �������� ������������ ����� ��������� ��� ������ �������, ������� ������ �� ���� �������������� �����. ����� ����� ���������� ����� �� ��������� ����� 9,4% � ��� (� 2005 �. &#8211; 11%), � ���������� ����� ������������ �������� ����� ��� �������� �� �������� �� ��������� �����������, ��������� � 2005 �. 5100 ���� ����.</P>
<P align=justify>����������� ����� � �������������� ����� ����� ����� �������� ���������� ����������� ��������, ������������ �� ��������������� � �������� �����������, ����� ��� &#8220;���������� � ����� ���������� ����� �� 2005&#8211;2010 ��.&#8221; (Financial Services Priorities 2005&#8211;2010).</P>
<P align=justify>������������ ���� ������� ���������� �������� ������������ ���������� ���������� ����� �, � ���������, ����� ��. � ������� ���� ��������� ��� �� ������������ � �������� ������ ������� ����� ������� ��������� ��; � ����� 2005 �. � ������� ���������� �� �� ����� ����� 1800 ���� ����.</P>
<P align=justify>� ��������� ����� ���������������� �� �� ������� � 19 ������� ��, � � �������������� � ����������� ��������������� ������ �������������� ������� � ��������� �������. � ����� �� �������� ����� ���� ������ ����� � 2004 �. ����������� ��������� ��������� (European Mortgage Federation &#8211; EMF) ������� ����������� ����� �� �� (European Covered Bond Council), ������� ��������� ����� 80 ���������� ����� �� �� 16 ����������� ����� � ������� ������ ������� ���������.</P>
<P align=justify>������ ������ �������� �� ����������� ��������� ����� ����� �������� ��������� � ������������� �������� (Capital Requirements Directive), ������� �������� � ���� � 1 ������ 2007 �. � ������� ������������� ���������� � ��������� ���������� �������� �� ������ �������������� ������. ��������� ����������� ������� � ������� ����� ������ ������ ������; �� �������� ����� � ������������� �������� ��������� ���������� �� ��������� ��������, ������� �� ����� ������� ��������������� ������ ������� �����.</P>
<P align=justify>���������� ����������� ��������� ������ ������������ ��-�� ������������ ����������� �������������� ������������ ����������. ����� ����, ���������� ������� ���� ��� �������� ����������� ������ ��� ����������, ���������� � �������� sub-prime. ������ �������� � ���� ������� ������� �� ������������ �������� � ��������, � ����� �� ����
����� ���������� � ����� ���� ������������.</P><B>
<P align=justify>�������� ��������� ������ � ������� ��������� � ���-��������� ������</P></B>
<P align=justify>� ��������� ���� ������ ����������� ����������� ���������� ������������ � ������� ��������� (�����, �������, ������, ��������, �������, ������, �����) � ���-��������� (�������, ��������, ��������, ��������, �������, ������ ) ������ (�������������� �� � ���). ������ ����� ������������ ������������������ �������� ������������ ���� �����, ������������ �� ������ � ��������� � �� ������������ ������� ����������������� � �������� ����� ��� �������, � ����� �� ������ ���������������� ����� ��������� ������� ����� �������������� �����. ����� �������� � ��������������� ������� ��������� �� ����������� ����, �� ����������� ������ (17,7%) � ������ (10,9%). ��� ������� ����� ����� ��� � 6% ������ ������ �� 2&#8211;3 ���������� ������ ��������� ������ �������� ������. ������������� ����� ��������� ��������� ���� ������� �������� �������� ���� ����� �� ������� �������� �������� ������ ���������� �����. �� ������ � 1998 �. ������� ����� ��������� ������� ��������� ����� �� 40%. ����� ����� �� ������������ ��� ���������, ��������� ��������� ��������� �������.</P>
<P align=right>������� 1</P><B>
<P align=center>�������� ������������� ���������� ����� ��������� � ���-��������� ������</P></B>
<TABLE cellSpacing=1 cellPadding=7 width=636 border=1>
<TBODY>
<TR>
<TD vAlign=top width="21%" height=9 rowSpan=2>
<P></P></TD>
<TD vAlign=top width="42%" colSpan=2 height=9>
<P align=center>���� ���, � ����.</P></TD>
<TD vAlign=top width="38%" colSpan=2 height=9>
<P align=center>��������, � ����.</P></TD></TR>
<TR>
<TD vAlign=top width="21%" height=9>
<P align=center>2005 �.</P></TD>
<TD vAlign=top width="21%" height=9>
<P align=center>2006 �.*</P></TD>
<TD vAlign=top width="19%" height=9>
<P align=center>2005 �.</P></TD>
<TD vAlign=top width="19%" height=9>
<P align=center>2006 �.*</P></TD></TR>
<TR>
<TD vAlign=top width="21%">
<P align=justify>������ ��</P></TD>
<TD vAlign=top width="21%">
<P align=center>6,73</P></TD>
<TD vAlign=top width="21%">
<P align=center>6,64</P></TD>
<TD vAlign=top width="19%">
<P align=center>3,4</P></TD>
<TD vAlign=top width="19%">
<P align=center>3,71</P></TD></TR>
<TR>
<TD vAlign=top width="21%">
<P align=justify>������ ���</P></TD>
<TD vAlign=top width="21%">
<P align=center>4,93</P></TD>
<TD vAlign=top width="21%">
<P align=center>5,05</P></TD>
<TD vAlign=top width="19%">
<P align=center>6,85</P></TD>
<TD vAlign=top width="19%">
<P align=center>5,78</P></TD></TR>
<TR>
<TD vAlign=top width="21%">
<P align=justify>������</P></TD>
<TD vAlign=top width="21%">
<P align=center>6,4</P></TD>
<TD vAlign=top width="21%">
<P align=center>6,3</P></TD>
<TD vAlign=top width="19%">
<P align=center>10,9</P></TD>
<TD vAlign=top width="19%">
<P align=center>10,2</P></TD></TR></TBODY></TABLE><FONT size=2>
<P align=justify>* &#8211; ��������������� ������</P></FONT>
<P align=justify>���� ��������� ����� ������ ����������� �� ���� ������� �� � ���, ����� ����� �������� ������ �����������. ��������� ���������� ��� ������ �����: 1) ������ &#8211; ����� ����� �� � ��������; 2) ������ ����������� ����������� � ������.</P>
<P align=justify>���� �� ������ �������� ����� ����� �������� ����� ������� � ���, ��� ��� � ������ ����� ������ �������� �������: ������ �� &#8211; � ������ 1990-� �����, � ������ ��� &#8211; ������� ������ �����. �������������� ������ ������ ����� ������� �� ������ �������� �������� � ����������������� ��������, ����� ������������ ����� ������ ������� ����� ����������� �����, ������� ��������� ������������ ������� �� �������������� � ���������� �������.</P>
<P align=justify>������ � ��������� �� �������� �� ���������� ����� �� � ��� ��������� ���� �� ��������� ������ ��������: � ����� �� ������� ������������� �� ������� ���������� ����� 7,2% �� ��� ������� ������ 47,5% � ������� �� ��-25. ������ ����� ����� ��� �� ����� ����������� �����, ��� ������ ������ ����� ��� ������� ������ ���������� ���������, � ��� ����� ��� ���������� ������������. ���� � ��-25 98% �������� �������� ����� ���������� ����, �� � ������ � ������� &#8211; ����� 70%, � � �������� � ������� &#8211; 33 � 35% ��������������.</P>
<P align=justify>������ ��������, �������������� �������� ���������� ����� � ��������������� �������, �������� ������������ ����������� ����� �� ����. �������� ���������� ������ �� �������� ����� ����� ����� � ����� ������ �� �����, ��� � �������� ��������� ����������� ����� �������� � ��������� ��� �� ����. ���, � ������ ����� ��������� �������� ����� � 2005 �. �� 244%, ��, �������� �� ����� ������������ ����������, ������� ����� ��������� �� ����� ���� ��������� ������� �����: �������� �������� � 55 ��. � ����� �������� 69 ���. ����, � ������������� �������� � ������ ����� 3816 ����. � �������� ����� ��������� �������� ����� � 1,4% ��� � 2004 �. �� 2,1% ��� � 2005 �. ������������ ���� �� ����� ����������� �������������� �� 47,6 � 36,5%.</P>
<P align=justify>������� �������� ����� ���������� ����� ����� �� � ��� �������� ����������� �����, � ��������� ����������� (����� ��� Erste Bank � Raiffeisen Group) � ����������� (UniCredit � Banca Intesa). ��� ������� � ������� ������ ���� �������� � �������� ��������, ��������������� ��������� �������. ����� ������������������ ����������� ����������� &#8211; �������� � ����������� �������-������������ �������������� ����� (Bausparkassen), ���������� ���� �������� �������� � �����, ��������, �������, ������� � ��������. �� �������� ������������ ������������������ ������ �������� ����� � ������� �������������� ��������, ������� ����� �������� �����������.</P>
<P align=justify>�������� ������ ���������������� � ������� ��������� ������� &#8211; ��������� ������ �� ������� ��� ������������� �����. ���������� ������ �� ����� ������� ������ �������������� ��� �����������. ������� ����� ������������ � ����������� ������ (������ � ���� ��� ��������) ��� � ������� ������, �� � ����������� �� ����� ���� (����� �������� ���������� ��� ���������� �����).</P>
<P align=right>������� 2</P><B>
<P align=center>������� ���������� ������������ (�� ��������� �� 2005 �.)</P></B>
<P align=center>
<CENTER>
<TABLE cellSpacing=1 cellPadding=7 width=624 border=1>
<TBODY>
<TR>
<TD vAlign=center width="25%">&nbsp;</TD>
<TD vAlign=center width="15%">
<P align=center>�����</P></TD>
<TD vAlign=center width="13%">
<P align=center>������</P></TD>
<TD vAlign=center width="13%">
<P align=center>��������</P></TD>
<TD vAlign=center width="18%">
<P align=center>������</P></TD>
<TD vAlign=center width="15%">
<P align=center>��</P></TD></TR>
<TR>
<TD vAlign=top width="25%">
<P align=justify>������� ���������� ������, � ����.</P></TD>
<TD vAlign=center width="15%">
<P align=center>3,3</P></TD>
<TD vAlign=center width="13%">
<P align=center>6</P></TD>
<TD vAlign=center width="13%">
<P align=center>5-5,71</P></TD>
<TD vAlign=center width="18%">
<P align=center>10-15</P>
<P align=center>(� �������� � ����)</P></TD>
<TD vAlign=center width="15%">
<P align=center>3,91</P></TD></TR>
<TR>
<TD vAlign=top width="25%">
<P>���� �������,</P>
<P>� �����</P></TD>
<TD vAlign=center width="15%">
<P align=center>15-25</P></TD>
<TD vAlign=center width="13%">
<P align=center>5-30</P></TD>
<TD vAlign=center width="13%">
<P align=center>15-20</P></TD>
<TD vAlign=center width="18%">
<P align=center>5-15</P></TD>
<TD vAlign=center width="15%">
<P align=center>25-35</P></TD></TR>
<TR>
<TD vAlign=top width="25%">
<P>������� ����� �������, ���. ����</P></TD>
<TD vAlign=center width="15%">
<P align=center>20</P></TD>
<TD vAlign=center width="13%">
<P align=center>16</P></TD>
<TD vAlign=center width="13%">
<P align=center>20-30</P></TD>
<TD vAlign=center width="18%">
<P align=center>40-50</P></TD>
<TD vAlign=center width="15%">
<P align=center>��� ������</P></TD></TR></TBODY></TABLE></CENTER>
<P align=justify>� ����� ��-�� ����������� ����������� �������� ������� ���������� ������ �� ��������� �������� ����, ��� � ������� �� ��. � ������ ��������� ��������� ������������ ������ ���� � ������� ��������� ���������, �����, ��������, ��� �������� ������� (bullet mortgages)<SUP>3</SUP> � ��., ���������� &#8220;������/��������� �����������&#8221; (loan-to-value &#8211; LTV) � ��� ����� ��������� 100%. � �������� �����, ��� �������, ����������� ���� ��������� �������� �� ����� � �������� ��������.</P>
<P align=justify>��������� ������ ������� �� �������������� ��������� ������� �����������, ��� ������� � ���������� � ������� �������� ������. ���������� ��� �������� ���������������� ��������� ��������.</P>
<P align=justify>1. ���������������� � ������� ���������� ��������, ��� ���������� ��� ��������� ���������� ������, �������, ������ � �����������, ��������� � ��������; �������������� �������� &#8211; ��������� ����� ������������� ���������� ����������� ��� ��������� ����� ����������� ��������.</P>
<P align=justify>2. ���������������� �� ����� ���������. ���� ������ ������������ � ��������, �������� � ������� ������, � ���������, �� ���� ������� ��������� ��������� &#8211; ��. � ������� �������� ����� ��� ���������� ����� ��������� ��������� ���� ������� � 2006 �., ������ ��-�������� ������������ ���� � ���������������� ������� ������ �������� � ��������� ����� ������������� ���������� �����������. � ������ ��������������� ����� �� ��������� ���������� � ��������� ������ ������ �������� ������� ��.</P>
<P align=justify>3. ������� � ���������������� �� ������ �������� �� ��������� ��������� ����� ����������� � �����, �������� � �������. ������ ������ �� ����� ������� �������� ��������� ����� � 1997 �., ������ ��� �������� ���� �������� ���������� �������� 1998 �. � ������� � ����� �� ������ �������� ������ ���� ��� �������� ���������������� (��������������� ���������� � ��� ����, ��� � ������� �� ��). ���, � 2005 �. ����� ��, ����������� � ���������, ��������� � ����� 83,1% ���, ������� &#8211; 98%, ������ &#8211; 3%, ������ &#8211; 1,6%, ����� &#8211; 0,6% ������ 17,9% ��� � ������� �� ��.</P>
<P align=justify>� ����������� ����� ������� �������������� ��������� ��������������, �� ������� ����, �������������� ������. ������ ����������� ������� ������� ��������������� �������, � ��� �������� ���������� ��������������� �� �������.</P><B>
<P align=center>����������:</P></B>
<P>1. Lambert A. The road to integration // Banker. &#8211; L., 2007. &#8211; 8 January. &#8211; P. 10.</P>
<P>2. Lea M. Global trends // Banker. &#8211; L., 2007. &#8211; 8 January. &#8211; P. 6-8.</P>
<P>3. Roy F. European potential // Banker. &#8211; L., 2007. &#8211; 8 January. &#8211; P. 20.</P><SUP>
<P align=justify>1 </SUP><FONT size=2>Fannie Mae (&#8220;����� ���&#8221;) &#8211; ������������ �������� ����������� ������������ ��������� ���������� ��� (US Federal National Mortgage Association); Freddie Mac (&#8220;������ ���&#8221;) &#8211;<B> </B>������������ �������� ����������� �������� ��������� ���������� ��� (US Federal Home Loan Mortgage Corp.). &#8211; ����. ���.</P><SUP><FONT size=2>
<P>2 </SUP>������������ ������� (��) &#8211; �������� ������������� � ���������� �������� �����, ������������ � &#8220;������������ ���������&#8221;, ����������� ������� ���������� � ������������ �� ������� �� 24 ����� 2000&nbsp;�. � ��������� (����������). ��������������� ���� �� &#8211; � 2010 �. ���������� �� � ����� �������� ������������� ������� ����. ������� ������������ �� ����� �������� ������� ����� ����� (��������������� ����� �����, ������� ��������� ����������������, ������ �����, ���������� ���������� ������ � ���� ���������� � �.�.). ����� ����, ������ �� ������������� ��������� ������������, ������������ �� ������������� ����� ����� (������������ � ��������������� ������������ ����� � ���������� ���������� � ��.). &#8211; ����. ���.</P><SUP>
<P>3 </SUP>�������, ��������� ������� ������������ �������������, �.�. ��� ������������ ����������� ��� ���������� ���������. &#8211; ����. ���.</P></FONT></FONT>


</body>
</html>

