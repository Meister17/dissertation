<!--
ID:37258510
TITLE:��������� � ��������������� ����������� ��������� ����������� � ����� � �� �����������
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:3
ISSUE_DATE:2007-02-15
RECORD_DATE:2007-02-08


-->
<html>
<head>
<title>
37258510-��������� � ��������������� ����������� ��������� ����������� � ����� � �� �����������
</title>
</head>
<body >


<P align=justify>�� ��������� ��������� ������������ ���������� ��� ������ ��������� ����� ������ � ����� ��������������� ����������� ��������� ����������� ������� ������:</P>
<P align=justify>&#61548; � ���������� ��������� ��������� �����������:</P>
<TABLE cellSpacing=2 cellPadding=7 width=100% align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="11%">
<P align=center>&#8470; �/�</P></TD>
<TD vAlign=top width="39%">
<P align=center>������������ ��������� �����������</P></TD>
<TD vAlign=top width="24%">
<P align=center>������</P></TD>
<TD vAlign=top width="26%">
<P align=center>��������������� �����</P></TD></TR>
<TR>
<TD vAlign=top width="11%">
<P align=center>1</P></TD>
<TD vAlign=top width="39%">
<P>������������ ����</P></TD>
<TD vAlign=top width="24%">
<P align=center>������</P></TD>
<TD vAlign=top width="26%">
<P align=center>1703</P></TD></TR>
<TR>
<TD vAlign=top width="11%">
<P align=center>2</P></TD>
<TD vAlign=top width="39%">
<P>������� ������������ �������������� ����</P></TD>
<TD vAlign=top width="24%">
<P align=center>������</P></TD>
<TD vAlign=top width="26%">
<P align=center>3253</P></TD></TR></TBODY></TABLE>
<P align=justify>&#61548; � ����������� ������������ ��������� ��������� �����������:</P>
<TABLE cellSpacing=2 cellPadding=7 width=100% align=center border=1>
<TBODY>
<TR>
<TD vAlign=top width="11%">
<P align=center>&#8470;</P>
<P align=center>�/�</P></TD>
<TD vAlign=top width="39%">
<P align=center>������������ ��������� �����������</P></TD>
<TD vAlign=top width="24%">
<P align=center>������</P></TD>
<TD vAlign=top width="26%">
<P align=center>��������������� �����</P></TD></TR>
<TR>
<TD vAlign=top width="11%">
<P align=center>1</P></TD>
<TD vAlign=top width="39%">
<P>��������������� ���</P></TD>
<TD vAlign=top width="24%">
<P align=center>����������� ����</P></TD>
<TD vAlign=top width="26%">
<P align=center>719</P></TD></TR>
<TR>
<TD vAlign=top width="11%">
<P align=center>2</P></TD>
<TD vAlign=top width="39%">
<P>������ ���</P></TD>
<TD vAlign=top width="24%">
<P align=center>������</P></TD>
<TD vAlign=top width="26%">
<P align=center>605</P></TD></TR>
<TR>
<TD vAlign=top width="11%">
<P align=center>3</P></TD>
<TD vAlign=top width="39%">
<P>��������� ���</P></TD>
<TD vAlign=top width="24%">
<P align=center>���������� �������</P></TD>
<TD vAlign=top width="26%">
<P align=center>1295</P></TD></TR>
<TR>
<TD vAlign=top width="11%">
<P align=center>4</P></TD>
<TD vAlign=top width="39%">
<P>����������������</P></TD>
<TD vAlign=top width="24%">
<P align=center>������������� ���.</P></TD>
<TD vAlign=top width="26%">
<P align=center>1522</P></TD></TR></TBODY></TABLE><I>
<P align=right>"������� ����� ������", 2007, &#8470; 1, 2, 4</P></I>


</body>
</html>

