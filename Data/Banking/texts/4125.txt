<!--
ID:38139831
TITLE:��������������� ���� "������������� ���������� ���������-���������� �����" (�������� ��������������� ����������������� �����������)
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:17
ISSUE_DATE:2007-09-15
RECORD_DATE:2007-09-12


-->
<html>
<head>
<title>
38139831-��������������� ���� "������������� ���������� ���������-���������� �����" (�������� ��������������� ����������������� �����������)
</title>
</head>
<body >


<B>
<P align=center>������ ��������� �� ������� 2007 �.</P></B>
<TABLE cellSpacing=1 cellPadding=7 width=100% border=1>
<TBODY>
<TR>
<TD vAlign=center width="5%" rowSpan=2><FONT size=2>
<P>&#8470;<BR>�/�</FONT></P></TD>
<TD vAlign=center width="9%" rowSpan=2><FONT size=2>
<P align=center>����</FONT></P></TD>
<TD vAlign=center width="26%" rowSpan=2><FONT size=2>
<P align=center>���� ��������</FONT></P></TD>
<TD vAlign=center width="40%" rowSpan=2><FONT size=2>
<P align=center>��������� ����������</FONT></P></TD>
<TD vAlign=top width="20%" colSpan=2><FONT size=2>
<P align=center>���������,<BR>���.</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>���<BR>������ � ��</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>���<BR>��������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="5%"><FONT size=2>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="9%"><FONT size=2>
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=2>
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=2>
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>5</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>6</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%" height=92><FONT size=2>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="9%" height=92><FONT size=2>
<P align=center>9&#8211;11</FONT></P></TD>
<TD vAlign=top width="26%" height=92><FONT size=2>
<P>������������� �������������� ���������� ����� ����������� ���������� ����������� �����������</FONT></P></TD>
<TD vAlign=center width="40%" height=92><FONT size=2>
<P>������ ����������� � ����� ��������� �����;</P>
<P>������������ �������� � �� �����������;</P>
<P>������������ �������� ������-������-�������;</P>
<P>������������ � ��������� ������������� ��������������� � ����������� ������������;</P>
<P>������������ ����� ��������� � ����� ����������� ��������;</P>
<P>������������ � ��������� ������������� �������������;</P>
<P>������������ � ���������� ����-�����������;</P>
<P>������������ � ���������� ������������� ������������� �����</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>20 200</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>18 200</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%"><FONT size=2>
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="9%"><FONT size=2>
<P align=center>22&#8211;1</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=2>
<P>����������� ����������� �������� � ������������ �����</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=2>
<P>������������ ����� �����������<BR>��������;</P>
<P>����������� ����� �����������<BR>��������;</P>
<P>���������� � ����������</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>34 000</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>30 600</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%"><FONT size=2>
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="9%"><FONT size=2>
<P align=center>15&#8211;17</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=2>
<P>���� ��������� "������������ �������� � ������������� ����������"</FONT></P></TD>
<TD vAlign=top width="40%">&nbsp;</TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>20 200</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>18 200</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%"><FONT size=2>
<P align=center>3.1</FONT></P></TD>
<TD vAlign=top width="9%"><FONT size=2>
<P align=center>15</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=2>
<P>������������ ������� ���������� ���������� �����: ����������� ���������� ����, �������� ���� ������������, ����������� ������������ � ���������</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=2>
<P>���������� �������������, ��������� � ���������� ������������� ������, ������������� ��� ������� ��������, ��� � �������������� ������ � �������� �������;</P>
<P>������� ���������</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>8 200</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>7 400</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%"><FONT size=2>
<P align=center>3.2</FONT></P></TD>
<TD vAlign=top width="9%"><FONT size=2>
<P align=center>16</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=2>
<P>�������� ������������ ��������: ������ ������, �������� � ���������� ������������, ����������� �����������������</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=2>
<P>����������� �������������, �������� � ���������� �������������;</P>
<P>������������� ��������� ������, ������������� ��� ������� ��������, ��� � �������������� ������ � �������� �������</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>8 200</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>7 400</FONT></P></TD></TR></TBODY></TABLE><I>
<P align=right>�����������</P></I>
<TABLE cellSpacing=1 cellPadding=7 width=100% border=1>
<TBODY>
<TR>
<TD vAlign=center width="5%"><FONT size=2>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="9%"><FONT size=2>
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=2>
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=2>
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>5</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>6</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%"><FONT size=2>
<P align=center>3.3</FONT></P></TD>
<TD vAlign=top width="9%"><FONT size=2>
<P align=center>17</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=2>
<P>���������� ������������� ��������� ��� �������������� � ��������� (������������)</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=2>
<P>������������ � �����������, ���������� � ���������, ��������� �� ��������</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>8 200</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>7 400</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%"><FONT size=2>
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="9%"><FONT size=2>
<P>19&#8211;20</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=2>
<P>�������� ������������ VIP-�������� (private banking)</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=2>
<P>������������ �����;</P>
<P>����������� � ����������� ������������� �� ��������������� �������� �����;</P>
<P>����������� � ����������� ������������� ���������� ����� �����;</P>
<P>����������� � ����������� ������������� �� ������������ VIP-���-�����/private banking</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>14 800</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>13 000</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%"><FONT size=2>
<P align=center>5</FONT></P></TD>
<TD vAlign=top width="9%"><FONT size=2>
<P>22&#8211;26</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=2>
<P>����������� ������ ���������������� ����-����������� � ���������� �������</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=2>
<P>������������ � ����������� ������������� ����-�����������;</P>
<P>������������ � ����������� ������ ����������� ��������;</P>
<P>������������ � ����������� ������-������������� � ����������� �������������;</P>
<P>������������ ��������������� ������������� �������� �����;</P>
<P>������������ � ����������� ������������;</P>
<P>������������ � ����������� ������������� �� ������ � ������� ��������;</P>
<P>��������� ������: ������������ � ����������� ��������� �������������;</P>
<P>������������ �������, ������ ����������� � ����� ���������</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>27 800</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>25 000</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%"><FONT size=2>
<P align=center>6</FONT></P></TD>
<TD vAlign=top width="9%"><FONT size=2>
<P>19&#8211;20</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=2>
<P>���������� ������������� ������� � ������������ �����</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=2>
<P>������������, �������������� ���������� � ����� ������������ ������ ������������� ����-�����������;</P>
<P>������������ � ������� ����������� ����� ����������� ��������, ������, ������������� ������������ � ���������� �������</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>11 200</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>10 000</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%"><FONT size=2>
<P align=center>7</FONT></P></TD>
<TD vAlign=top width="9%"><FONT size=2>
<P>9&#8211;13</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=2>
<P>���� ��������� "������������� ��������� ���������� ����������"</FONT></P></TD>
<TD vAlign=top width="40%">&nbsp;</TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>28 000</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>25 200</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%"><FONT size=2>
<P align=center>8</FONT></P></TD>
<TD vAlign=top width="9%"><FONT size=2>
<P>9&#8211;11</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=2>
<P>������������� ��������� ���������� ����������</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=2>
<P>������� ���������� � �� �����������;</P>
<P>��������� �����������;</P>
<P>�����������, ������������ ������������ ����������;</P>
<P>���������� ����� ��������������� � �������������</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>20 200</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>18 200</FONT></P></TD></TR></TBODY></TABLE><FONT size=2>
<P align=center></P></FONT><I>
<P align=right>�����������</P></I>
<TABLE cellSpacing=1 cellPadding=7 width=100% border=1>
<TBODY>
<TR>
<TD vAlign=center width="5%"><FONT size=2>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="9%"><FONT size=2>
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=2>
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=2>
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>5</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>6</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%"><FONT size=2>
<P align=center>9</FONT></P></TD>
<TD vAlign=top width="9%"><FONT size=2>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=2>
<P>������������� ���������� ������������� ����� �� ���� � ���� (�������-���������)</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=2>
<P>������� ���������� � �� �����������;</P>
<P>��������� �����������;</P>
<P>�����������, ������������ ������������ ����������;</P>
<P>���������� ����� ��������������� � �������������.</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>14 800</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>13 300</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%"><FONT size=2>
<P align=center>10</FONT></P></TD>
<TD vAlign=top width="9%"><FONT size=2>
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=2>
<P>������������ ���������� ������������ ������ "� ��������������� ����������� (���������) �������, ���������� ���������� �����, � �������������� ����������"</FONT></P></TD>
<TD vAlign=top width="40%">&nbsp;</TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>8 200</FONT></P></TD>
<TD vAlign=top width="10%">&nbsp;</TD></TR></TBODY></TABLE><B>
<P align=center>������ ��������� �� ������ 2007 �.</P></B>
<TABLE cellSpacing=1 cellPadding=7 width=100% border=1>
<TBODY>
<TR>
<TD vAlign=center width="5%" rowSpan=2><FONT size=2>
<P>&#8470;<BR>�/�</FONT></P></TD>
<TD vAlign=center width="9%" rowSpan=2><FONT size=2>
<P align=center>����</FONT></P></TD>
<TD vAlign=center width="26%" rowSpan=2><FONT size=2>
<P align=center>���� ��������</FONT></P></TD>
<TD vAlign=center width="40%" rowSpan=2><FONT size=2>
<P align=center>��������� ����������</FONT></P></TD>
<TD vAlign=top width="20%" colSpan=2><FONT size=2>
<P align=center>���������,<BR>���.</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>���<BR>������ � ��</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>���<BR>��������</FONT></P></TD></TR>
<TR>
<TD vAlign=center width="5%"><FONT size=2>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="9%"><FONT size=2>
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="26%"><FONT size=2>
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="40%"><FONT size=2>
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>5</FONT></P></TD>
<TD vAlign=top width="10%"><FONT size=2>
<P align=center>6</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%" height=92><FONT size=2>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="9%" height=92><FONT size=2>
<P align=center>20&#8211;22</FONT></P></TD>
<TD vAlign=top width="26%" height=92><FONT size=2>
<P>���������� ���������� ����� �����</FONT></P></TD>
<TD vAlign=top width="40%" height=92><FONT size=2>
<P>������������ ��������, �� ����������� � ������������ ����������� ������������� �������;</P>
<P>������������ ������������� ��������� �����, ���������� �� ���������� ���� �����</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>20 200</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>18 200</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%" height=92><FONT size=2>
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="9%" height=92><FONT size=2>
<P align=center>14&#8211;16</FONT></P></TD>
<TD vAlign=top width="26%" height=92><FONT size=2>
<P>���������� ������ � ���������� ������������ ������ (�������-�������)</FONT></P></TD>
<TD vAlign=top width="40%" height=92><FONT size=2>
<P>������ � ������� �������������� �������� �����;</P>
<P>���������� ���������� ���������� � ����� �����;</P>
<P>������� ������������ ����� (�������� ������), ������� ������ �� �������� ��������� ������������� � ���������� ����������� � �����</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>22 900</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>20 600</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%" height=92><FONT size=2>
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="9%" height=92><FONT size=2>
<P align=center>26&#8211;6</FONT></P></TD>
<TD vAlign=top width="26%" height=92><FONT size=2>
<P>����������� ���������� ����</FONT></P></TD>
<TD vAlign=top width="40%" height=92><FONT size=2>
<P>������������ �������� ����� �������� � ��������������� ������������� �����;</P>
<P>���������� �������� � ��������������� ������������� �����;</P>
<P>��������� ������-������������ ������������� �����</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>34 000</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>30 600</FONT></P></TD></TR></TBODY></TABLE><I>
<P align=right>�����������</P></I>
<TABLE cellSpacing=1 cellPadding=7 width=100% border=1>
<TBODY>
<TR>
<TD vAlign=top width="5%" height=22><FONT size=2>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="9%" height=22><FONT size=2>
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="26%" height=22><FONT size=2>
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="40%" height=22><FONT size=2>
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="10%" height=22><FONT size=2>
<P align=center>5</FONT></P></TD>
<TD vAlign=top width="10%" height=22><FONT size=2>
<P align=center>6</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%" height=92><FONT size=2>
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="9%" height=92><FONT size=2>
<P align=center>26&#8211;28</FONT></P>
</TD>
<TD vAlign=top width="26%" height=92><FONT size=2>
<P>������ ����������� ������������ �������� �����. ����������� ��������� � ����� �����. ������ ������������ � ���������� ���������� �����</FONT></P></TD>
<TD vAlign=top width="40%" height=92><FONT size=2>
<P>������������ �������� � ������-�������������;</P>
<P>������������ � ���������� ������������� �������������;</P>
<P>������������ � ���������� PR-���-����������;</P>
<P>������������ � ���������� ������������� �������������;</P>
<P>������������ � ���������� ��������� ������������� �����;</P>
<P>������������ ������-������������� � �������������, ���������� �� ���������� � �������� ���������� ��������� � �����</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>20 200</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>18 200</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%" height=92><FONT size=2>
<P align=center>5</FONT></P></TD>
<TD vAlign=top width="9%" height=92><FONT size=2>
<P align=center>19&#8211;22</FONT></P></TD>
<TD vAlign=top width="26%" height=92><FONT size=2>
<P>���� ��������� "������ ������������������ ��������"</FONT></P></TD>
<TD vAlign=top width="40%" height=92>
<P></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>24 400</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>22 000</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%" height=92><FONT size=2>
<P align=center>5.1</FONT></P></TD>
<TD vAlign=top width="9%" height=92><FONT size=2>
<P align=center>19&#8211;21</FONT></P></TD>
<TD vAlign=top width="26%" height=92><FONT size=2>
<P></FONT><FONT size=2>������ ������������������ �������������� �������� </FONT></P></TD>
<TD vAlign=top width="40%" height=92><FONT size=2>
<P>������������ � ����������� ��������� ������������� �����;</P>
<P>���������� ������������� �����<BR>�����</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>20 200</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>18 200</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%" height=92><FONT size=2>
<P align=center>5.2</FONT></P></TD>
<TD vAlign=top width="9%" height=92><FONT size=2>
<P align=center>22</FONT></P></TD>
<TD vAlign=top width="26%" height=92><FONT size=2>
<P></FONT><FONT size=2>����������� ��������� �� ����� ���������� ��������. �������������� ������������ (������������)</FONT></P></TD>
<TD vAlign=top width="40%" height=92><FONT size=2>
<P>��������� ��������, ����������� ���������� ������ �����</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>8 200</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>7 400</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%" height=92><FONT size=2>
<P align=center>6</FONT></P></TD>
<TD vAlign=top width="9%" height=92><FONT size=2>
<P align=center>8&#8211;9</FONT></P></TD>
<TD vAlign=top width="26%" height=92><FONT size=2>
<P>��������� � ������ &#8211; ���������� � �������� ���������</FONT></P></TD>
<TD vAlign=top width="40%" height=92>
<P></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>14 800</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>13 300</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%" height=92><FONT size=2>
<P align=center>7</FONT></P></TD>
<TD vAlign=top width="9%" height=92><FONT size=2>
<P align=center>10</FONT></P></TD>
<TD vAlign=top width="26%" height=92><FONT size=2>
<P>������� ���������� ������ ����������� � ������������ �����</FONT></P></TD>
<TD vAlign=top width="40%" height=92><FONT size=2>
<P>������������, �������������� ���������� � ����� ������������ �������������� ������ ���������� ������������;</P>
<P>������� ����������� � ������������ �����������, ����-�������������, ����� ����������� ��������, �������������� ����������;</P>
<P>���������� ���������</P>
<P></P>
<P></FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>8 200</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>7 400</FONT></P></TD></TR></TBODY></TABLE><I>
<P align=right>�����������</P></I>
<TABLE cellSpacing=1 cellPadding=7 width=100% border=1>
<TBODY>
<TR>
<TD vAlign=top width="5%" height=30><FONT size=2>
<P align=center>1</FONT></P></TD>
<TD vAlign=top width="9%" height=30><FONT size=2>
<P align=center>2</FONT></P></TD>
<TD vAlign=top width="26%" height=30><FONT size=2>
<P align=center>3</FONT></P></TD>
<TD vAlign=top width="40%" height=30><FONT size=2>
<P align=center>4</FONT></P></TD>
<TD vAlign=top width="10%" height=30><FONT size=2>
<P align=center>5</FONT></P></TD>
<TD vAlign=top width="10%" height=30><FONT size=2>
<P align=center>6</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%" height=92><FONT size=2>
<P align=center>8</FONT></P></TD>
<TD vAlign=top width="9%" height=92><FONT size=2>
<P align=center>16</FONT></P></TD>
<TD vAlign=top width="26%" height=92><FONT size=2>
<P>���� ������ ����� �� ����</FONT></P></TD>
<TD vAlign=top width="40%" height=92><FONT size=2>
<P>������� ���������� � �� �����������;</P>
<P>�����������, ������������ ��������� ����� ������ �����;</P>
<P>��������</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>8 200</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>7 400</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%" height=92><FONT size=2>
<P align=center>9</FONT></P></TD>
<TD vAlign=top width="9%" height=92><FONT size=2>
<P align=center>17</FONT></P></TD>
<TD vAlign=top width="26%" height=92><FONT size=2>
<P>����������� ����� �������� �� ����</FONT></P></TD>
<TD vAlign=top width="40%" height=92><FONT size=2>
<P>������� ���������� � �� �����������;</P>
<P>�����������, ������������ ��������� ����� ��������; ��������</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>8 200</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>7 400</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="5%" height=92><FONT size=2>
<P align=center>10</FONT></P></TD>
<TD vAlign=top width="9%" height=92><FONT size=2>
<P align=center>14&#8211;15</FONT></P></TD>
<TD vAlign=top width="26%" height=92><FONT size=2>
<P>����� ���������� � ������ ��������� � ������������� ����� ��������� ����������� </FONT></P></TD>
<TD vAlign=top width="40%" height=92>
<DIR><FONT size=2>
<P>������� ���������� � �� �����������;</P>
<P>��������</P></DIR></FONT></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>14 800</FONT></P></TD>
<TD vAlign=top width="10%" height=92><FONT size=2>
<P align=center>13 300</FONT></P></TD></TR></TBODY></TABLE><B><I><FONT face=Wingdings>
<P align=justify>*</FONT> 125468, ������, ������������� �-��, �. 51</P>
<P align=justify><FONT face=Wingdings>(</FONT> 8(499) 943-9835/ -9354/ -9841/ -9411;</P>
<P align=justify>���� 8(499) 943-9362/ -9472</P>
<P align=justify>E-mail:</B></I><B><I>seminar@ifbsm.ru</B></I></P><FONT size=2>
<P align=justify></FONT><B><I>www.ifbsm.ru</B></I></P>


</body>
</html>

