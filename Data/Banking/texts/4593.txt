<!--
ID:35512468
TITLE:� ���
SOURCE_ID:3126
SOURCE_NAME:������� ����������� ����
AUTHOR:
NUMBER:7
ISSUE_DATE:2005-07-31
RECORD_DATE:2005-11-10


-->
<html>
<head>
<title>
35512468-� ���
</title>
</head>
<body >


<P align=justify><FONT face=Wingdings>&#252;</FONT> <B>�� &#8220;���&#8221; �������� �� ����������� ����������� � ��������������� ������� ������������ �������� ���������� ��������� ����� &#8220;������ ������������. ������� ����������� �������&#8221;.</B> ������ ���� ������������ ��� �������������� ���������� �� ������� ������ � ������� ����������� �������. ��� ����������� ���������� ����� ��� ����� � ��������� �����, �� ������� ������������ ����, �������������� ������� � ����������, �� ������ � ���� ��������, ������� ��������� ����������� ������, ������ ��������� ���������� ��� �������� ������. ��������� ��� ����� ������� ����� �������� ������������ � ������� �������������� ������� ���������� �� �������� ����������� �������. � ��� ������ ����������� ���������� ����������, ������������ �� ��������� ������������� �������������� �������� ������ � �������� �� ���������� �������. � ���������, ��� ��� ��������������� ����� ��������� ��� ������������� ������� &#8212; ����������� ������� ����������� ������� ��� ���������� ��� ��������� ��������.</P>
<P align=justify><FONT face=Wingdings>&#252;</FONT> <B>15 ���� � ������ ������ &#8212; ���������� ������� ������������� ����������� ������� (���) ������ ��������</B> &#8220;������-������&#8221; � &#8220;������������&#8221;, <B>11 ���� </B>&#8212; ��� &#8220;���� ��������� ��������������&#8221; � ��� ��� &#8220;������������ ��������� ����.</P>
<P align=justify><FONT face=Wingdings>&#252;</FONT> <B>�����-���� ��������� ���������� �� ���������� ����� � ��������� ����� ���� �� ������</B>. ��� ������ ����������� �������� ��������� ��������� ��������, &#8220;� ������� ����������� ������� �������� ������ ���������-���������� �����, ������� � ������� ����� ���� �� ��������� ������� �� ����. � ���������, �������� ����� �������� � ������������� ��������, �������� ���� ��������� ������&#8221;. 2006 ��� ��������� �������� ���������� �������� �� ����� ���������� ������. �� &#8220;���&#8221; � ������ ����� ���� ��������� ������� ����������� ������������. � ��������� ����� &#8220;� ������&#8221; ���������� ��������� 35 ������, � �.�. ����� ��� � 20 &#8212; ��� �������� ���������� �����������, � 3 &#8212; ��������� ������� ��������������� �����������. � ����� ��������� �����, ��������� ��� ����� 10 ������ (��� ����� � ������ ��� ���������� ��������� ��� �� ����� &#8220;������&#8221; ���������), � ��������� � ��, ��� ��������� ���������� � ������������ ��������� ��������� �� ����������� ������� ������ ����������� ������������ &#8212; ����������� ����. ������ � ����������� ���������� ���������� �� ������ ������� ����������� � ����������� ������������, �. �������� ������, ��� ������������������ � �������� ���������� ������� ��������� ��� �������� � ����������, � ������������� ��������, �� ���� �� ������ ��������� ������������.</P>
<P align=justify><FONT face=Wingdings>&#252;</FONT> <B>�� &#8220;���&#8221; �������� �� ������������ ��� ��������� ������ ��� ��� &#8220;�����-����&#8221; � ��� &#8220;����&#8221; (���).</B> ��������� ��������� ����������� �������� ����� ����������� � ��������� �� ������ ������-�������, ����� ������� ��� ����� ������������ ����� �� ���������� ��������� � ������� ���������� �� �������, � ����� ����������� ���������� ��������� ����������. ����� ������-������� ������������ � ������������ � �. 12 ��. 12 ������������ ������ &#8220;� ����������� ������� ���������� ��� � ������ ���������� ���������&#8221; � �������� ����������� ������ ������-�������, ������������ �������� ������ ���������� ��� �� 17.09.2004.</P>


</body>
</html>

