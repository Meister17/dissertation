<!--
ID:35410950
TITLE:�������� ������ � ������ ����������� �������*
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:������� �������, ������� ������� ���������� �� ������ � ��������������� ����������� ��� ����� ������
NUMBER:7
ISSUE_DATE:2005-07-31
RECORD_DATE:2005-10-14


RBR_ID:4928
RBR_NAME:��������� � ������� ��������
RBR_ID:4935
RBR_NAME:������ � ����������� ������������ �� ��
-->
<html>
<head>
<title>
35410950-�������� ������ � ������ ����������� �������*
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgwhite>
<TD>*<EM>&nbsp;�� 1 ���� 2005 ����. </EM></TD></TR>
<TR class=bgwhite>
<TD height=0>
<P class=Main align=justify>������ ������ �� �������� �������, ������������ ����� ������� ����������� ������� � ������ ���� 2005 ����, ���������� ��������� �� ��������. ������� ������ �� ������������ ���������, ����������� �� ������ ����������� 145 ������ ����������� �������, �� ������ � ��� ������ ��������� � 8,03 �� 7,93%, �� ����������� &#8212; � 9,36 �� 9,25%, �� ������� &#8212; � 10,52 �� 10,38% � �� ������� �� ���� ��� ���� &#8212; �������� ��� ���������. � ������� 1 ��������� �������, ����������� � ������������ ������ �� ������� � ����������� �� �� ��������� �� 1.07.2005 �. 
<P class=Subheader align=center>����������� ������ �� ������� � ����������� �� ���������</P>
<P class=Subheader align=justify>
<TABLE cellSpacing=0 cellPadding=0 width="100%" align=center border=1>
<TBODY>
<TR class=bgcolor1>
<TD>
<P align=justify><STRONG>���� ������</STRONG></P></TD>
<TD>
<P align=justify><STRONG>������� </STRONG></P></TD>
<TD>
<P align=justify><STRONG>����������� </STRONG></P></TD>
<TD>
<P align=justify><STRONG>������������ </STRONG></P></TD></TR>
<TR>
<TD>
<P align=justify>3 ������</P></TD>
<TD>
<P align=justify>7,93</P></TD>
<TD>
<P align=justify>1,00</P></TD>
<TD>
<P align=justify>13,0</P></TD></TR>
<TR>
<TD>
<P align=justify>6 �������</P></TD>
<TD>
<P align=justify>9,25</P></TD>
<TD>
<P align=justify>1,70</P></TD>
<TD>
<P align=justify>13,50</P></TD></TR>
<TR>
<TD>
<P align=justify>12 �������</P></TD>
<TD>
<P align=justify>10,38</P></TD>
<TD>
<P align=justify>2,50</P></TD>
<TD>
<P align=justify>14,05</P></TD></TR>
<TR>
<TD>
<P align=justify>24 ������</P></TD>
<TD>
<P align=justify>11,02</P></TD>
<TD>
<P align=justify>6,75</P></TD>
<TD>
<P align=justify>14,50</P></TD></TR></TBODY></TABLE></P>
<P class=Main align=justify>&nbsp;<STRONG>����� �������������� ������� </STRONG>
<P class=Main align=justify>� �������������� ������ ���������� ���������� � 3044 �������� �������, ������������ 145 ������� ����������� �������. ��� ����� ����� ������� ���������� ����� (������� �������� ��), ������� ������ �� ��������� ����������� ������� � ������ ����� ���������� � ������. ���������� ����� �� ����� ���������, �� ������������ ��������� ����� &#8212; ��� �� ��������, ��� � �� ���������� �� ����� �������. 105 ������ �������� ����������� C������ ����������� �������. 
<P class=Main align=justify>���������� �������, ������������ ����� ���������� �������������, ������ �����������. ��������, ���� &#171;�����&#187; ����������� 94 ���������� (������������ ������� � ����������� ����), ������������ &#8212; 68, ���������� ��������� ���� &#8212; 76, ��������� &#8212; 50 ����������. � �� �� ����� ������� ����������� ����� ����������, ������� &#8212; ��������, BSVG &#8212; �����, � ��������� ����� &#8212; ����� �����. � ������� 2 ������������ ����������� ������� �� ������ � �������� �����������. 
<P class=Subheader align=center>����������� ������� �� ������ � �������� �����������</P>
<P class=Subheader align=justify>
<TABLE cellSpacing=0 cellPadding=0 width="100%" align=center border=1>
<TBODY>
<TR class=bodycolor>
<TD width="67%">
<P align=justify><STRONG>�� ������ �����������:</STRONG></P></TD>
<TD width="33%">
<P align=justify></P></TD></TR>
<TR>
<TD>
<P align=justify>�� 3 ������</P></TD>
<TD>
<P align=justify>800</P></TD></TR>
<TR>
<TD>
<P align=justify>�� 6 �������</P></TD>
<TD>
<P align=justify>941</P></TD></TR>
<TR>
<TD>
<P align=justify>�� 12 �������</P></TD>
<TD>
<P align=justify>1033</P></TD></TR>
<TR>
<TD>
<P align=justify>�� 24 ������</P></TD>
<TD>
<P align=justify>270</P></TD></TR>
<TR>
<TD>
<P align=justify>�� �������������:</P></TD>
<TD>
<P align=justify></P></TD></TR>
<TR>
<TD>
<P align=justify>��� �������������</P></TD>
<TD>
<P align=justify>2630</P></TD></TR>
<TR>
<TD>
<P align=justify>C ��������������</P></TD>
<TD>
<P align=justify>414</P></TD></TR>
<TR class=bodycolor>
<TD colSpan=2>
<P align=justify><STRONG>�� ����������� ����������:</STRONG></P></TD></TR>
<TR>
<TD>
<P align=justify>�������������</P></TD>
<TD>
<P align=justify>1506</P></TD></TR>
<TR>
<TD>
<P align=justify>�����������</P></TD>
<TD>
<P align=justify>1538</P></TD></TR>
<TR class=bodycolor>
<TD colSpan=2>
<P align=justify><STRONG>�� ������������� ���������� %</STRONG></P></TD></TR>
<TR>
<TD>
<P align=justify>����������/�������������</P></TD>
<TD>
<P align=justify>1431</P></TD></TR>
<TR>
<TD>
<P align=justify>� ����� ����� ������</P></TD>
<TD>
<P align=justify>1613</P></TD></TR></TBODY></TABLE></P>
<P class=Main align=justify>��������� ��� � �� �������� � ��� 40 ��������� ����������� �� 145��, ��� 27% �������������� ����� ������, �� ������ � ������� ����������� �������. ������������ ������ �� ������� ��� ���������������� ��� � ������, � ��� �� �����������, ����������, ��� � �� ������������� ���������� ���������� ��� ����� ��������, ��� � �������. �� ����� ������� ������ 70% ������� ��������������� ��������, �������� � ������� (���������� &#8212; ����� ��� �� 20%). �� � ���������� 30���������� �������� �������� ����: �����&shy;��������� ������� ����������� ��������� �� ���������� ������� 13% �������, �� ���������� &#8212; 17%, � �� ����� ��� ���������������� ����� ��������� �� ���������� ������� 26% �������, � �� ���������� &#8212; 4%. 
<P class=Main align=justify>�� ������ ����������� ������������ ��������� ��� ������ ����������, 98% ������� ������� �� ��������� ��� ����� ������ ��� �� ��������� �����������. ������� ������ ������ �������� &#8212; 54 ����� ������, ����������� &#8211; 48 �����, � � ����������� �������������� �������� &#8212; 2025 ����� ������ (&#171;������������������ ���������&#187;, 07.06.2005). 
<P class=Main align=justify>� ������ ���� ���������� �������� ������� ���� ������� �� ���� ��������� � ����� ������ ����������� �����: 
<P class=Main align=justify>������ &#8212; �� 1 �� 29 999 ���. 
<P class=Main align=justify>������� &#8212; �� 30 000 �� 99 999 ���. 
<P class=Main align=justify>������� &#8212; �� 100 000 �� 299 999 ���. 
<P class=Main align=justify>������������&#8212; �� 300 000 �� 2 999 999 ���. 
<P class=Main align=justify>������ &#8212; 3 000 000 � ����� ���. 
<P class=Main align=justify>������, �������������� � ������� 3, �������������, ��� &#171;����������������&#187; ����� ��������� ����� ���� ������� �� ������� &#171;���� �����&#187;, ��� �� �������� ������� �� ����� �� 300 ����� ������, �� � 3040% ������� ��� ����������� �� ���������� ������� � ������� &#8212; �� ������� ���� �����. 
<P class=Main align=justify>����� ������������, ��� �� �������� ����� ������� &#171;����������������&#187; ������ ������� � ���, �����, �� �������� ����� ������ ��� ������ � ������� ����������� (���� �������, ����������, � ������������ ������� �����������), ���� � ������ ������� ���������, ������������� � ������� ������� ��� ���������� �������� ��� ������������� �������� �� �����: ���� �� �� ������, ������� ���� �������� ��� ����� �� �������� ��� �������� �� ���������, �� ����� ������. 
<P class=Subheader align=justify>����������� ������-���������� ��� � �� �������� � ��� 
<TABLE cellSpacing=0 cellPadding=0 width="100%" align=center border=1>
<TBODY>
<TR class=bgcolor1>
<TD>
<P align=justify><STRONG>��������� �������</STRONG></P></TD>
<TD>
<P align=justify><STRONG>������ �� ���������� ������</STRONG></P></TD>
<TD>
<P align=justify><STRONG>������ �� ���������� ������</STRONG></P></TD>
<TD>
<P align=justify><STRONG>������ �� ���������� ������</STRONG></P></TD></TR>
<TR class=bodycolor>
<TD colSpan=4>
<P align=justify>�����, �� �������� � ���</P></TD></TR>
<TR>
<TD>
<P align=justify>������ ������</P></TD>
<TD>
<P align=justify>13%</P></TD>
<TD>
<P align=justify>78%</P></TD>
<TD>
<P align=justify>9%</P></TD></TR>
<TR>
<TD>
<P align=justify>������� ������</P></TD>
<TD>
<P align=justify>27%</P></TD>
<TD>
<P align=justify>71%</P></TD>
<TD>
<P align=justify>3%</P></TD></TR>
<TR>
<TD>
<P align=justify>������� ������</P></TD>
<TD>
<P align=justify>24%</P></TD>
<TD>
<P align=justify>72%</P></TD>
<TD>
<P align=justify>4%</P></TD></TR>
<TR>
<TD>
<P align=justify>������������ ������</P></TD>
<TD>
<P align=justify>39%</P></TD>
<TD>
<P align=justify>61%</P></TD>
<TD>
<P align=justify>0%</P></TD></TR>
<TR>
<TD>
<P align=justify>������ ������</P></TD>
<TD>
<P align=justify>36%</P></TD>
<TD>
<P align=justify>64%</P></TD>
<TD>
<P align=justify>0%</P></TD></TR>
<TR>
<TD>
<P align=justify>����� �� ������</P></TD>
<TD>
<P align=justify>26%</P></TD>
<TD>
<P align=justify>70%</P></TD>
<TD>
<P align=justify>4%</P></TD></TR>
<TR class=bodycolor>
<TD colSpan=4>
<P align=justify>����� - ��������� ���</P></TD></TR>
<TR>
<TD>
<P align=justify>������ ������</P></TD>
<TD>
<P align=justify>7%</P></TD>
<TD>
<P align=justify>71%</P></TD>
<TD>
<P align=justify>22%</P></TD></TR>
<TR>
<TD>
<P align=justify>������� ������</P></TD>
<TD>
<P align=justify>8%</P></TD>
<TD>
<P align=justify>74%</P></TD>
<TD>
<P align=justify>18%</P></TD></TR>
<TR>
<TD>
<P align=justify>������� ������</P></TD>
<TD>
<P align=justify>10%</P></TD>
<TD>
<P align=justify>76%</P></TD>
<TD>
<P align=justify>14%</P></TD></TR>
<TR>
<TD>
<P align=justify>������������ ������</P></TD>
<TD>
<P align=justify>20%</P></TD>
<TD>
<P align=justify>67%</P></TD>
<TD>
<P align=justify>13%</P></TD></TR>
<TR>
<TD>
<P align=justify>������ ������</P></TD>
<TD>
<P align=justify>20%</P></TD>
<TD>
<P align=justify>59%</P></TD>
<TD>
<P align=justify>21%</P></TD></TR>
<TR>
<TD>
<P align=justify>����� �� ������</P></TD>
<TD>
<P align=justify>12%</P></TD>
<TD>
<P align=justify>70%</P></TD>
<TD>
<P align=justify>17%</P></TD></TR>
<TR>
<TD>
<P align=justify>����� �� ������</P></TD>
<TD>
<P align=justify>16%</P></TD>
<TD>
<P align=justify>70%</P></TD>
<TD>
<P align=justify>14%</P></TD></TR></TBODY></TABLE>
<P class=Main align=justify>� ������-���������� ��� ������� ����: � ������� ��� ����� ������� ���� �� ���������� �������, ���������� �������������� ������� � ����� ���������� � ���������� ������ � ����������� ���� ������� � ���������� ����������� �� ���� ����� ����������� ���� ������. 
<P class=Rubrika align=justify><STRONG>������� ������� ������ �� ������ </STRONG>
<P class=Main align=justify>������������, ��� ���������� ���������� ����� �������������� ������ ��� ����������� ���������� �������� � ��������� � ������� ����� ����� � ��� �����, �������� �������������� �������� �� ����������� ��������, ������������� ���������� ���������� ������ ��� ������� �������. 
<P class=Main align=justify>&#171;��� ������ ������������ �� ���������� ���������� ������������ �������, �������� ���������� � �����������, ��� ���� ����� ������&#187;, &#8212; ������������ ������� ����� &#171;����&#187; �. ��������. ������������ ���������� �� ������ � ��������� �������� �. ��������� �������, ��� &#171;������ � ����������� �������� ��������� ������ ����� �������, ��� �������� � �������� � ����� �����&#187;<SUP>1</SUP>. 
<P class=Main align=justify>��� ��� ������� �������� � �������� ���������? ������ ������� ���� �������� &#8212; ����������� ����� ������ � ������� ������ (����������� ����������, ������������� � ������������� ���������� ���������) �� ���������� ��������� �������, ��� ��� ������� �� ������������. 
<P class=Main align=justify><STRONG>����������� ����� ����������� ������ ������ � ����������� �� ���� </STRONG>������������ ��������� �������. ��� ����������� ������ ���� ��������������� ���������������: �� ������, ������, �������� � �������� ����������� ��������. ����� ��� ������� ������������������ �������� �������������� ������� ����� ����������� (��� ������ ����) � ������������ (��� ������� ����) ��������. ��� ����������� ���������� ������ ����������� ������� �� ���� �� ���� � �������� ����� &#171;������&#187; (������� 4). 
<P class=Subheader align=center>������ �� ������� � �������� �� &#171;������&#187;</P>
<P class=Subheader align=justify>
<TABLE cellSpacing=0 cellPadding=0 width="100%" align=center border=1>
<TBODY>
<TR class=bgcolor1>
<TD width="55%">
<P align=justify><STRONG>������� ������</STRONG></P></TD>
<TD width="14%">
<P align=justify><STRONG>�������� ������</STRONG></P></TD>
<TD width="18%">
<P align=justify><STRONG>��������-��� �����</STRONG></P></TD>
<TD width="13%">
<P align=justify><STRONG>������, %</STRONG></P></TD></TR>
<TR class=bodycolor>
<TD colSpan=4>
<P align=justify><STRONG>3 ������</STRONG></P></TD></TR>
<TR>
<TD rowSpan=4>
<P align=justify>�������������, � ����� �����, ��� �������������</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD rowSpan=4>
<P align=justify>����� &#8470;1</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD>
<P align=justify>10 000</P></TD>
<TD>
<P align=justify>5,00</P></TD></TR>
<TR>
<TD>
<P align=justify>100 000</P></TD>
<TD>
<P align=justify>5,25</P></TD></TR>
<TR>
<TD>
<P align=justify>1 000 000</P></TD>
<TD>
<P align=justify>5,50</P></TD></TR>
<TR>
<TD>
<P align=justify>10 000 000</P></TD>
<TD>
<P align=justify>6,00</P></TD></TR>
<TR>
<TD rowSpan=4>
<P align=justify>�������������, ����������, � ��������������</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD rowSpan=4>
<P align=justify>����� &#8470;2</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD>
<P align=justify>10 000</P></TD>
<TD>
<P align=justify>3,00</P></TD></TR>
<TR>
<TD>
<P align=justify>100 000</P></TD>
<TD>
<P align=justify>3,40</P></TD></TR>
<TR>
<TD>
<P align=justify>1 000 000</P></TD>
<TD>
<P align=justify>3,50</P></TD></TR>
<TR>
<TD>
<P align=justify>10 000 000</P></TD>
<TD>
<P align=justify>3,70</P></TD></TR>
<TR>
<TD rowSpan=9>
<P align=justify>�����������, � ����� �����, ��� �������������</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD rowSpan=5>
<P align=justify>����� &#8470;3</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD>
<P align=justify>50 000</P></TD>
<TD>
<P align=justify>2,75</P></TD></TR>
<TR>
<TD>
<P align=justify>100 000</P></TD>
<TD>
<P align=justify>3,00</P></TD></TR>
<TR>
<TD>
<P align=justify>1 500 000</P></TD>
<TD>
<P align=justify>3,25</P></TD></TR>
<TR>
<TD>
<P align=justify>3 000 000</P></TD>
<TD>
<P align=justify>3,50</P></TD></TR>
<TR>
<TD>
<P align=justify>6 000 000</P></TD>
<TD>
<P align=justify>3,65</P></TD></TR>
<TR>
<TD rowSpan=4>
<P align=justify>����� &#8470;4</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD>
<P align=justify>10 000</P></TD>
<TD>
<P align=justify>3,60</P></TD></TR>
<TR>
<TD>
<P align=justify>100 000</P></TD>
<TD>
<P align=justify>3,80</P></TD></TR>
<TR>
<TD>
<P align=justify>1 000 000</P></TD>
<TD>
<P align=justify>3,90</P></TD></TR>
<TR>
<TD>
<P align=justify>10 000 000</P></TD>
<TD>
<P align=justify>4,00</P></TD></TR>
<TR class=bodycolor>
<TD colSpan=4 height=0>
<P align=justify>6 �������</P></TD></TR>
<TR>
<TD rowSpan=4>
<P align=justify>�������������, � ����� �����, ��� �������������</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD rowSpan=4>
<P align=justify>����� &#8470;1</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD>
<P align=justify>10 000</P></TD>
<TD>
<P align=justify>7,50</P></TD></TR>
<TR>
<TD>
<P align=justify>100 000</P></TD>
<TD>
<P align=justify>7,75</P></TD></TR>
<TR>
<TD>
<P align=justify>1 000 000</P></TD>
<TD>
<P align=justify>8,00</P></TD></TR>
<TR>
<TD>
<P align=justify>10 000 000</P></TD>
<TD>
<P align=justify>8,50</P></TD></TR>
<TR>
<TD rowSpan=4>
<P align=justify>�������������, ���������� / �������������, � ��������������</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD rowSpan=4>
<P align=justify>����� &#8470;2</P>
<P align=justify></P>
<P align=justify></P>
<P align=
justify></P></TD>
<TD>
<P align=justify>10 000</P></TD>
<TD>
<P align=justify>7,25</P></TD></TR>
<TR>
<TD>
<P align=justify>100 000</P></TD>
<TD>
<P align=justify>7,50</P></TD></TR>
<TR>
<TD>
<P align=justify>1 000 000</P></TD>
<TD>
<P align=justify>7,75</P></TD></TR>
<TR>
<TD>
<P align=justify>10 000 000</P></TD>
<TD>
<P align=justify>8,25</P></TD></TR>
<TR>
<TD rowSpan=9>
<P align=justify>�����������, � ����� �����, ��� �������������</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD rowSpan=5>
<P align=justify>����� &#8470;3</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD>
<P align=justify>50 000</P></TD>
<TD>
<P align=justify>7,00</P></TD></TR>
<TR>
<TD>
<P align=justify>100 000</P></TD>
<TD>
<P align=justify>7,25</P></TD></TR>
<TR>
<TD>
<P align=justify>1 500 000</P></TD>
<TD>
<P align=justify>7,50</P></TD></TR>
<TR>
<TD>
<P align=justify>3 000 000</P></TD>
<TD>
<P align=justify>7,75</P></TD></TR>
<TR>
<TD>
<P align=justify>6 000 000</P></TD>
<TD>
<P align=justify>8,00</P></TD></TR>
<TR>
<TD rowSpan=4>
<P align=justify>����� &#8470;4</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD>
<P align=justify>10 000</P></TD>
<TD>
<P align=justify>7,50</P></TD></TR>
<TR>
<TD>
<P align=justify>100 000</P></TD>
<TD>
<P align=justify>7,75</P></TD></TR>
<TR>
<TD>
<P align=justify>1 000 000</P></TD>
<TD>
<P align=justify>8,00</P></TD></TR>
<TR>
<TD>
<P align=justify>10 000 000</P></TD>
<TD>
<P align=justify>8,50</P></TD></TR>
<TR class=bodycolor>
<TD colSpan=4 height=0>
<P align=justify>12 �������</P></TD></TR>
<TR>
<TD rowSpan=4>
<P align=justify>�������������. � ����� �����, ��� �������������</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD rowSpan=4>
<P align=justify>����� &#8470;1</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD>
<P align=justify>10 000</P></TD>
<TD>
<P align=justify>8,75</P></TD></TR>
<TR>
<TD>
<P align=justify>100 000</P></TD>
<TD>
<P align=justify>8,00</P></TD></TR>
<TR>
<TD>
<P align=justify>1 000 000</P></TD>
<TD>
<P align=justify>9,25</P></TD></TR>
<TR>
<TD>
<P align=justify>10 000 000</P></TD>
<TD>
<P align=justify>10,00</P></TD></TR>
<TR>
<TD rowSpan=4>
<P align=justify>�������������, ���������� / �������������, � ��������������</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD rowSpan=4>
<P align=justify>����� &#8470;2</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD>
<P align=justify>10 000</P></TD>
<TD>
<P align=justify>8,50</P></TD></TR>
<TR>
<TD>
<P align=justify>100 000</P></TD>
<TD>
<P align=justify>8,75</P></TD></TR>
<TR>
<TD>
<P align=justify>1 000 000</P></TD>
<TD>
<P align=justify>9,00</P></TD></TR>
<TR>
<TD>
<P align=justify>10 000 000</P></TD>
<TD>
<P align=justify>9,75</P></TD></TR>
<TR>
<TD rowSpan=9>
<P align=justify>�����������, � ����� �����, ��� �������������</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD rowSpan=5>
<P align=justify>����� &#8470;3</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD>
<P align=justify>50 000</P></TD>
<TD>
<P align=justify>8,25</P></TD></TR>
<TR>
<TD>
<P align=justify>100 000</P></TD>
<TD>
<P align=justify>8,50</P></TD></TR>
<TR>
<TD>
<P align=justify>1 500 000</P></TD>
<TD>
<P align=justify>8,75</P></TD></TR>
<TR>
<TD>
<P align=justify>3 000 000</P></TD>
<TD>
<P align=justify>9,00</P></TD></TR>
<TR>
<TD>
<P align=justify>6 000 000</P></TD>
<TD>
<P align=justify>9,25</P></TD></TR>
<TR>
<TD rowSpan=4>
<P align=justify>����� &#8470;4</P>
<P align=justify></P>
<P align=justify></P>
<P align=justify></P></TD>
<TD>
<P align=justify>10 000</P></TD>
<TD>
<P align=justify>9,00</P></TD></TR>
<TR>
<TD>
<P align=justify>100 000</P></TD>
<TD>
<P align=justify>9,25</P></TD></TR>
<TR>
<TD>
<P align=justify>1 000 000</P></TD>
<TD>
<P align=justify>9,50</P></TD></TR>
<TR>
<TD>
<P align=justify>10 000 000</P></TD>
<TD>
<P align=justify>10,00</P></TD></TR></TBODY></TABLE></P>
<P class=Main align=justify>� ���������� ������� �����������, ��� ���������� ����� ����������� ������ ������ � ��������� ���������� ������ �� ���� ���������� �������� � �������� ������� (53%). �� ������ �������� ������� ������� ����������� ������ ������, ��� ����������� �� ������������ �����. 
<P class=Main align=justify><STRONG>������� ������� ������� ������ �� ����������</STRONG> ������������ ����������� �������: ������������ �� ���� ����������, ����� ������������, � �������� ������� ����������� � ������������ ������. ���� �������� &#8212; ����������� ��������� ����������� �����, ������������� ��������� ��������, ������� ���������� ������ � ��. &#8212; � ������ ��������� �� ���������������. 
<P class=Main align=justify>���������� ������ ����������, ��� ������� ������ � ������� ������� ������ �� ����������, ������ ����������� �����. ���, ����������� ����������� ������ � �������������� ���������� ��������� (��� ��������� ������ �������) �������� � 23,3% �������, � ������������ ���������� &#8212; � 13,9% �������, � �������������� ��������� &#8212; ���� � 9,3% �������. 
<P class=Main align=justify>������������� ������� �� ��������� ��������� ������� �� ������ �� �������. � ������ ����, ��� �� ����������� ���������� � ������������� ���������� ��������� ������������� �������� ������� ����������� ������� (�� ����������� �������������, ������� ����������� ����), ��� ������� ���������� �������� &#171;������������ �������&#187; ��� �������� ��������� � �� ��������� ������� �� ���������� ��������. 
<P class=Main align=justify><STRONG>��� �������������� ������ �������� �� �������� ����������. ����� �������� ��������������� ���������� ������� <A href="javascript:if(confirm('http://www.banki.ru/  \n\nThis file was not retrieved by Teleport Pro, because it is addressed on a domain or path outside the boundaries set for its Starting Address.  \n\nDo you want to open it from the server?'))window.location='http://www.banki.ru/'" target=_blank tppabs="http://www.banki.ru/"><FONT color=#0f0f0f>www.banki.ru</FONT></A> �� ���������� �� �����</STRONG> 
<P class=Main align=justify><SUP>1</SUP><EM>&nbsp;&#171;������&#187; &#8470;19 (109) 23-29 ��� 2005.</EM></P></TD></TR></TBODY></TABLE>


</body>
</html>

