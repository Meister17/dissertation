<!--
ID:35789899
TITLE:���������� � ����������� � �������������� ��������� ����������� �� 01.11.2005 �.*
SOURCE_ID:3125
SOURCE_NAME:������� ���
AUTHOR:
NUMBER:24
ISSUE_DATE:2005-12-31
RECORD_DATE:2006-01-26


-->
<html>
<head>
<title>
35789899-���������� � ����������� � �������������� ��������� ����������� �� 01.11.2005 �.*
</title>
</head>
<body >


<TABLE cellSpacing=2 cellPadding=9 width="100%" align=center border=1>
<TBODY>
<TR>
<TD vAlign=top colSpan=5><B>
<P align=center>����������� ��������� �����������</B></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=center><A name=DDE_LINK1></A></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center>�� 01.01.2004</P></TD>
<TD vAlign=top width="15%">
<P align=center>�� 01.11.2005</P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><B>
<P align=justify>1. ���������������� ��*<SUP>*</SUP> ������ ������ ���� �� ��������� ��� ������� �������������� �������������� �������, �����<SUP>1</SUP></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>1 666</B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=right>1 420</B></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>-&nbsp;������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 612</P></TD>
<TD vAlign=top width="15%">
<P align=right>1 367</P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>-&nbsp;������������ ��</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>54</P></TD>
<TD vAlign=top width="15%">
<P align=right>53</P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<DIR>
<P align=justify>1.1. ���������������� �� �� 100%-��� ����������� �������� � ��������</P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>32</P></TD>
<TD vAlign=top width="15%">
<P align=right>40</P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<DIR>
<P align=justify>1.2. ��, ������������������ ������ ������, �� ��� �� ���������� �������� ������� � �� ���������� �������� (� ������ �������������� �������������� �����)</P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>
<DIR>
<P align=right>4</P></DIR></TD>
<TD vAlign=top width="15%">
<DIR>
<P align=right>1</P></DIR></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>-&nbsp;�����</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<DIR>
<DIR>
<P align=right>3</P></DIR></DIR></TD>
<TD vAlign=top width="15%">
<DIR>
<DIR>
<P align=right>1</P></DIR></DIR></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>-&nbsp;������������ ��</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<DIR>
<DIR>
<P align=right>1</P></DIR></DIR></TD>
<TD vAlign=top width="15%">
<DIR>
<DIR>
<P align=right>0</P></DIR></DIR></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=left><STRONG>2. ������������ ��, ������������������ �� 01.07.2002 ������� ��������</STRONG></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><STRONG>2</STRONG></P></TD>
<TD vAlign=top width="15%">
<P align=right><STRONG>0</STRONG></P></TD></TR>
<TR>
<TD vAlign=top colSpan=5><B>
<P align=center>����������� ��������� �����������</B></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2><B>
<P align=justify>3. ��, ������� ����� �� ������������� ���������� ��������, �����<SUP>2</SUP></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>1 329</B></P></TD>
<TD vAlign=top width="15%"><B>
<P align=right>1 260</B></P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%">&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>-&nbsp;�����</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 277</P></TD>
<TD vAlign=top width="15%">
<P align=right>1 212</P></TD></TR>
<TR>
<TD vAlign=top width="71%" colSpan=2>
<P align=justify>-&nbsp;������������ ��</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>52</P></TD>
<TD vAlign=top width="15%">
<P align=right>48</P></TD></TR>
<TR>
<TD vAlign=top colSpan=5><I>
<P align=right>�����������</I></P></TD></TR>
<TR>
<TD vAlign=top width="69%">&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center>�� 01.01.2004</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center>�� 01.11.2005</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<P align=justify>3.1. ��, ������� �������� (����������), ��������������� ����� ��:</P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>-&nbsp;����������� ������� ���������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 190</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 073</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>-&nbsp;������������� �������� � ����������� ������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>845</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>824</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>-&nbsp;����������� ��������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>310</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>306</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>-&nbsp;���������� �������� � �������������</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>����������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>5</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>4</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>��������<SUP>3</SUP></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>176</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>179</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<P align=justify>3.2. �� � ����������� �������� � �������� ��������, �����</P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>128</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>131</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>�� ���:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>�� 100%-���</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>32</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>40</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>����� 50%</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>9</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>9</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<P align=justify>3.3. ��, ���������� � ������ ����� &#8211; ���������� ������� ������������ �����������</P></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>923</P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify>4. ������������������ �������� ������� ����������� �� (��� ���.)</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>362 010</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>432 144</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify>5. ������� ����������� �� �� ���������� ��, �����</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>3 219</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>3284</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>�� ���:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>��������� ������<SUP>4</SUP></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 045</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1 009</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>������ �� 100%-��� ����������� �������� � �������� ��������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>15</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>24</P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify>6. ������� ����������� �� �� �������, �����<SUP>5</SUP></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>4</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>3</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify>7. ������� ������-������������ �� ���������� ���������� ���������</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>0</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>0</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%"><B>
<P align=justify>8. ����������������� ����������� ���������� ��, �����<SUP>6</SUP></B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>219</B></P></TD>
<TD vAlign=top width="15%" colSpan=2><B>
<P align=right>446</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><B>-</B>&nbsp;�� ���������� ���������� ���������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>176</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>402</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><B>-</B>&nbsp;� ������� ���������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>30</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>31</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><B>-</B>&nbsp;� ������� ���������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>13</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>13</P></TD></TR>
<TR>
<TD vAlign=top colSpan=5><B>
<P align=center>����� �������� � ���������� ����������� ���</B></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=left><STRONG>9. ��, � ������� �������� (������������) �������� �� ������������� ���������� �������� � ������� �� ��������� �� ����� ��������������� ����������� ��������� �����������<SUP>7</SUP></STRONG></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>335</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>159</P></TD></TR>
<TR>
<TD vAlign=top colSpan=5><I>
<P align=right>�����������</I></P></TD></TR>
<TR>
<TD vAlign=top width="69%">&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center>�� 01.01.2004</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=center>�� 01.11.2005</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><STRONG>10. ������� ������ � ����� ��������������� ����������� ��������� ����������� � ���������� �� ��� ������������ ����, �����<SUP>8</SUP></STRONG></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><STRONG>1 416</STRONG></P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right><STRONG>1 675</STRONG></P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=left><B>-</B>&nbsp;� ����� � ������� (��������������) ��������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1047</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1296</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><B>-</B>&nbsp;� ����� � ��������������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>364</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>378</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>�� ���: � ����� �������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>0</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>0</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>� ����� �������������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>364</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>378</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>� ��� �����:</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>����� �������������� � ������� ������ ������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>326</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>334</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify>������������ � ������ ������ (��� ����������� �������)</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>38</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>44</P></TD></TR>
<TR>
<TD vAlign=top width="69%">
<P align=justify><B>-</B>&nbsp;� ����� � ������������ �������� ���������� � ����������</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>4</P></TD>
<TD vAlign=top width="15%" colSpan=2>&nbsp;</TD></TR>
<TR>
<TD vAlign=top width="69%">
<DIR>
<DIR>
<P align=left><B>-</B>&nbsp;� ����� � ���������� ���������������� � ����� ������ ��������� ��������</P></DIR></DIR></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1</P></TD>
<TD vAlign=top width="15%" colSpan=2>
<P align=right>1</P></TD></TR></TBODY></TABLE><SUP>
<P align=justify>*</SUP> ���������� ������������ � ��� ����� � �� ��������� ��������, ����������� �� ��������������� ��������������� ������ �� �������� ����.</P><B>
<P align=justify>��������� � �������.</P></B><SUP>
<P align=justify>*</SUP>* �� &#8211; ��������� �����������. ������� "��������� �����������" � ��������� ���������� �������� ���� �� ��������� �������:</P>
<DIR>
<DIR>
<P align=justify>&#61548; ����������� ����, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� ������� � ������� ����� �� ������������� ���������� ��������;</P>
<P align=justify>&#61548; ����������� ����, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� �������, �������, �� ���������� ����� �� ������������� ���������� ��������;</P>
<P align=justify>&#61548; ����������� ����, ������������������ ������� �������� (�� ���������� � ���� ������������ ������ "� ������ � ���������� ������������") � ������� �������� ����� ������ �� ������������� ���������� ��������.</P></DIR></DIR><SUP>
<P align=justify>1 </SUP>����������� ��, ������� ������ ������������ ���� �� �������� ����, � ��� ����� ��, ���������� ����� �� ������������� ���������� ��������, �� ��� �� ��������������� ��� ����������� ����.</P><SUP>
<P align=justify>2 </SUP>����������� ��, ������������������ ������ ������ (�� 01.07.2002) ��� �������������� �������������� ������� � ������� ����� �� ������������� ���������� ��������, � ����� ������������ ��, ������������������ ������� �������� � ���������� �������� ����� ������ �� ������������� ���������� ��������.</P><SUP>
<P align=justify>3 </SUP>�������� � ������� 1996 �. � ������������ � ������� ����� ������ �� 03.12.1996 &#8470; 367.</P><SUP>
<P align=justify>4 </SUP>����������� ������� ��������� ������, ��������� � ����� ��������������� ����������� ��������� ����������� � ���������� ���������� ������. �� 01.01.1998 �. � ����������� ���������� � ��������� ������������ �� ������ ������ ����������� ����� ���������� ���������� ��������� ������ &#8211; 34 426.</P><SUP>
<P align=justify>5 </SUP>����������� �������, �������� ����������� �� �� �������.</P><SUP>
<P align=justify>6 </SUP>� ����� ���������������� ���������� �� �� ������� �������� �����������������, �� ������� ��������� � ���� ������ ����������� �� �������� �� �� �������.</P><SUP>
<P align=justify>7 </SUP>����� ���������� ��, � ������� ������ ������ ���� �������� (������������) �������� �� ������������� ���������� �������� (������� ��, �� ������� � ����� ��������������� ����������� ��������� ����������� ��
����� ������ �� �� ����������), &#8211; 1465.</P><SUP>
<P align=justify>8 </SUP>����� 01.07.2002 � ����� ��������������� ����������� ��������� ����������� ������ � ���������� ��������� ����������� ��� ������������ ���� �������� ������ ����� ��������������� ����������� ��������� ����������� � ����� � �� ����������� �������������� �������������� �������.</P>


</body>
</html>

