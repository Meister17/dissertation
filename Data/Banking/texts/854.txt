<!--
ID:35284950
TITLE:�������-�������� ������ ���������� ������������ ����� ���������� ���������, ������������ ������ ������� � �������� ����������� ������
SOURCE_ID:3099
SOURCE_NAME:������ �����-����������
AUTHOR:
NUMBER:2
ISSUE_DATE:2005-06-30
RECORD_DATE:2005-09-14


RBR_ID:2039
RBR_NAME:���������� �������
RBR_ID:4928
RBR_NAME:��������� � ������� ��������
-->
<html>
<head>
<title>
35284950-�������-�������� ������ ���������� ������������ ����� ���������� ���������, ������������ ������ ������� � �������� ����������� ������
</title>
</head>
<body >


<CENTER>
<TABLE cellSpacing=1 cellPadding=3 width=565 border=1>
<TBODY>
<TR>
<TD vAlign=top width="7%" height=37>
<P align=center><FONT>&#8470; </FONT></P></TD>
<TD vAlign=top width="75%" height=37>
<P align=center><FONT>����������</FONT></P></TD>
<TD vAlign=top width="18%" height=37>
<P align=center><FONT>�������� �� </FONT></P>
<P align=center><FONT>I ��.2005 �.</FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=right><FONT>1.</FONT></P></TD>
<TD vAlign=top width="75%">
<P><FONT>�������-�������� ������, ����� (� % � ���������������� ������� ��������������� ����)</FONT></P></TD>
<TD vAlign=top width="18%"><B>
<P align=right><FONT></FONT></P>
<P align=right><FONT>126</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=right><FONT>2.</FONT></P></TD>
<TD vAlign=top width="75%">
<P><FONT>�����������, ����� (� % � ���������������� ������� ��������������� ����) (������� �������� ������)</FONT></P></TD>
<TD vAlign=top width="18%"><B>
<P align=right><FONT></FONT></P>
<P align=right><FONT>125</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=8>
<P><FONT></FONT></P></TD>
<TD vAlign=top width="75%" height=8>
<P><FONT>� ��� ����� (� ��������� � ����� ����� �����������): </FONT></P></TD>
<TD vAlign=top width="18%" height=8>
<P><FONT></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=right><FONT>2.1</FONT></P></TD>
<TD vAlign=top width="75%">
<P><FONT>����������� �������� �������</FONT></P></TD>
<TD vAlign=top width="18%"><B>
<P align=right><FONT>46</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=right><FONT>2.2</FONT></P></TD>
<TD vAlign=top width="75%">
<P><FONT>����������� �� ����� �� ������� �������</FONT></P></TD>
<TD vAlign=top width="18%"><B>
<P align=right><FONT>12</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=right><FONT>2.3</FONT></P></TD>
<TD vAlign=top width="75%">
<P><FONT>����������� ������� �� ���������� ������� ����� ���������</FONT></P></TD>
<TD vAlign=top width="18%"><B>
<P align=right><FONT>15</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=right><FONT>2.4</FONT></P></TD>
<TD vAlign=top width="75%">
<P><FONT>����������� �� ������� ����������� ������ ���������</FONT></P></TD>
<TD vAlign=top width="18%"><B>
<P align=right><FONT>11</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="7%"><B>
<P align=right><FONT>3</FONT></B><FONT>.</FONT></P></TD>
<TD vAlign=top width="75%">
<P><FONT>�������, ����� (� % � ���������������� ������� ��������������� ���� ) (������� �������� ������)</FONT></P></TD>
<TD vAlign=top width="18%"><B>
<P align=right><FONT></FONT></P>
<P align=right><FONT>126</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=3>
<P><FONT></FONT></P></TD>
<TD vAlign=top width="75%" height=3>
<P><FONT>� ��� ����� (� ��������� � ����� ����� ������): </FONT></P></TD>
<TD vAlign=top width="18%" height=3>
<P><FONT></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=right><FONT>3.1</FONT></P></TD>
<TD vAlign=top width="75%">
<P><FONT>������ �� ������ ����� � ������� ����������� ���������</FONT></P></TD>
<TD vAlign=top width="18%"><B>
<P align=right><FONT>11</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=right><FONT>3.2</FONT></P></TD>
<TD vAlign=top width="75%">
<P><FONT>������ �� ������� ����������� ������</FONT></P></TD>
<TD vAlign=top width="18%"><B>
<P align=right><FONT>10</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=21>
<P align=right><FONT>3.3</FONT></P></TD>
<TD vAlign=top width="75%" height=21>
<P><FONT>������ �� ������ �� ������� �������</FONT></P></TD>
<TD vAlign=top width="18%" height=21><B>
<P align=right><FONT>61</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=right><FONT>3.4</FONT></P></TD>
<TD vAlign=top width="75%">
<P><FONT>������ �� ������� �������������������� ��������� � ���������</FONT></P></TD>
<TD vAlign=top width="18%"><B>
<P align=right><FONT>3</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=22>
<P align=right><FONT>3.5</FONT></P></TD>
<TD vAlign=top width="75%" height=22>
<P><FONT>������ �� ������� ������ � �������</FONT></P></TD>
<TD vAlign=top width="18%" height=22><B>
<P align=right><FONT>0</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="7%">
<P align=right><FONT>3.6</FONT></P></TD>
<TD vAlign=top width="75%">
<P><FONT>������ �� ������ ����</FONT></P></TD>
<TD vAlign=top width="18%"><B>
<P align=right><FONT>5</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=39><B>
<P align=right><FONT>4</FONT></B><FONT>.</FONT></P></TD>
<TD vAlign=top width="75%" height=39>
<P><FONT>�������� ��� � ������ �������-��������� ������� ��������� ����������� ������� (��� ��� ) � %:</FONT></P></TD>
<TD vAlign=top width="18%" height=39>
<P><FONT></FONT></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=21>
<P align=right><FONT>4.1.</FONT></P></TD>
<TD vAlign=top width="75%" height=21>
<P><FONT>������ �����-����������</FONT></P></TD>
<TD vAlign=top width="18%" height=21><B>
<P align=right><FONT>41,3</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=21>
<P align=right><FONT>4.2.</FONT></P></TD>
<TD vAlign=top width="75%" height=21>
<P><FONT>�������� ������ �. ������ </FONT></P></TD>
<TD vAlign=top width="18%" height=21><B>
<P align=right><FONT>54,3</FONT></B></P></TD></TR>
<TR>
<TD vAlign=top width="7%" height=22>
<P align=right><FONT>4.3.</FONT></P></TD>
<TD vAlign=top width="75%" height=22>
<P><FONT>�������� ������ ����������� ������</FONT></P></TD>
<TD vAlign=top width="18%" height=22><B>
<P align=right><FONT>4,0</FONT></B></P></TD></TR></TBODY></TABLE></CENTER>
<P align=justify><FONT size=+0>��������� ������� �������� �����, ���������� ����� ����� ��������� ����������� �������, ����� ���� ������������ ���������� �����������: </FONT></P>
<P align=center></P><I>
<P align=right><FONT>���������� ������������<BR></FONT><FONT>����������� ����������-�������� ��������<BR></FONT><FONT>�������� ���������� ����� ������ �� �����-����������</FONT></P></I>


</body>
</html>

