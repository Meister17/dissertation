<!--
ID:37639271
TITLE:������������ ��������
SOURCE_ID:3106
SOURCE_NAME:���������� ���������
AUTHOR:
NUMBER:5
ISSUE_DATE:2007-05-31
RECORD_DATE:2007-05-11


-->
<html>
<head>
<title>
37639271-������������ ��������
</title>
</head>
<body >


<H1><B><FONT size=3>�������� ������� �������, ��� ��������� � ������� ���� ���������� �� ��������� ��������� � ����������</FONT></B></H1>
<H3><FONT size=3>��������� ���������� &#171;������&#187; ������� �������� ������� ����� ������������ ������� � ���������������� ������� ������, ��������� � �������� ������, ������� �������� ����������� ����������� �� ������������� ����������� �������. ������� �� ���� �� ���������� ����������� � ���-�����, �������� ������� ������ � ����� ������������� � ������� �������� ����������� �������. ���������� ��� &#171;��&#187; �. ������� �������, � ��� ����������� ������������ ��� �������.</FONT></H3>
<DIV class=article_text>
<TABLE style="MARGIN-RIGHT: 10px" cellSpacing=0 width=250 align=left border=0 cellpading="0">
<TBODY>
<TR>
<TD><FONT>&nbsp;</FONT></TD></TR>
<TR>
<TD width=250 bgColor=#d6d6d6>
<DIV style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 10px; PADDING-TOP: 5px"><FONT size=3><FONT><SMALL>�������� �������: ������ �� ��������� ���� �� ��, ����� ��� ��������� ��������� ��������� �� ������� ���������.</SMALL> </FONT></FONT></DIV></TD></TR></TBODY></TABLE>
<DIV class=quest><EM>&#8212; �������� �����������, ����������, ��� �� ������ ���������? </EM></DIV><BR>
<DIV class=otvet>&#8212; ��� �������� ������������ ������ �������� � ������� ������ � �������, ��� ���������� �������������� � �������� ���������������� ������� ������, � ������� ����� ������� ������������� ������������ �������� &#8212; �� ���� ���� � ������ ���������, � ����� ������������� � ������������ �����. ���� ������� ������ &#8212; ��� ��� ��� ���������������� � ���������� ����������� �� ����������������� ����������� ������� &#8212; ���������������, ����������� �, ����� ����, ���������������. </DIV>
<DIV class=quest><EM>&#8212; � ��� ����������� ������������ ����� �������? </EM></DIV><BR>
<DIV class=otvet>&#8212; � �������, ��� ����� ���������������� ������. ������ ���� �������� ������ ���������: ����� � ��������� ��� ���������� ������������ ������, ���������� ���, ��� �������� ����������������, � � �� �� �����, ����� �� �� ���������� ����� �����������, ������ �� ������ �������. ����� ����, ���� ������ ����� ����������� �������� ����, ���� �� �������, ��� ��������� �� ����. ��������, ������ ��������� �������, ��������� � ������������� ������������ ������, ������ ��������� ����� �������� ������. ���� ��� ������� ��������, �� ���� ������ �������, ��� ��� ��� ������������ ���������������� ������. ���� �������� ������������ ��������� � ���������� ��������������� ����� � ����������� �� ����, ��������� ������������ �������� ���������. 
<P align=justify>������������ �� ��� ����������� � ���, ��� �� ���� ������ ������ �����. � ��������� ����� ������ ������� �� ������������ ����� �� ������ ����. ���� ������ �����, �� ��� ���� � ���������� ������������� &#8212; ����������� ����������� �������� ��������������. �� ���� ���� ����� ������� � ����� �������, �� ��� ������ ���� ����������� �������� � ���������� ��������� �� �������. ��� � ���� ����� &#8212; ��, � �����������.</P></DIV>
<DIV class=quest><EM>&#8212; ��� �� ���������� � ������������ ����? </EM></DIV><BR>
<DIV class=otvet>&#8212; � �����, ��� ��� ����� ���� ������� ��� ������ � ����� ������. ��� ���� �� ���������, ������� ����� ���������������. </DIV>
<DIV class=quest><EM>&#8212; ���� ���� ����� ������� � ����� ��, �� ��������� ����������� �� ����. ��� ��� ����� ����� ���������� ����� �����������? </EM></DIV><BR>
<DIV class=otvet>&#8212; ������ ������, � �� ������������ �� �����-�� ���������. �� ����� ��������� ����� �����������, �� ��������������� ����� �� ������ ��� ������������, ������� ���������. ��������, � ������ ���������� &#171;������&#187; ��� �������� ����������� ������ ��� ����������� ������ ������������, ����� ��� ����������� ������ � ������� �����-�� ������. ���������� �� ����������� � �������� ����� ��������� �� ������������ ����� ������� ������, ��� ��� ���� � �������. </DIV>
<DIV class=quest><EM>&#8212; �������������� ������� &#8212; ��� ������ ���������� �����������? </EM></DIV><BR>
<DIV class=otvet>&#8212; �� ������ ���. </DIV>
<DIV class=quest><EM>&#8212; �� ������� ��������� ����������, ������� ������� � ���������������� ���������. ��� �������� ��������� ����������. ��� �� �������, ��� ��������� �� ����� ���� �� � �������, � � ������ ������������? </EM></DIV><BR>
<DIV class=otvet>&#8212; (���������� �����.) ��, ����� ������ ����������� ��������. ���� ����, ������� ������ ���� ������������, ����������, ����������. ��������, �� ����� ������ ������ ���� ����������. � �����������, ��� ����������� ������������ ������� ����� ������������� ���������, ����� ������������ ������. � ��������, ��� ���-�� ���� ����� � � ���� ��������, ��� ����� �� ���������� ��� ������. �� ������, ��� ��� �������� ��������, � ������, ��� ��� ������ ������. 
<P align=justify>�� ������� �������, � �����, ��������� ���-���� ������� ���, ��� � ��������� ����� � ����� � ����������� ������ � ������� ����������� ������� � � ����� � ���������� ���������, ������� ���� �� ������ ������� ����������� ���������� &#8212; �� ������ ��������, � ����� � ��������� ������ �������, &#8212; ���� �����������. ���� ������ �� ������� �� ������ �������� � �������� ������������� �������� ��� �� ������� ����� &#8212; ��� ���� � ���� �� ���� ��� �������. � �� ����������� ����� �� ��, ����� ��������� ��� ��������� ��������� � ��������� ���������.</P>
<P align=justify>� ������, ����� ��� ������� ���������, �� ��������� ���� �� ��, ����� ��� ��������� ��������� ��������� � ������� �����, �� ������� ���������. ��� ��� � ������, ��� ���� ��������� ���� ������� ��������� ���������� �������. ����, �����������, ������ � ���������������� ������� ��� ���� ��������� ���������.</P></DIV>
<DIV class=quest><EM>&#8212; ������� ������� ����� ��������� ������� ������ ����������� ����� ������� ��� ��������? </EM></DIV><BR>
<DIV class=otvet>&#8212; � �����, ����� ���� ������, ���������� ���������� ���������������� ��������� ������, �� � �� ����. ��� �������� ������ ����� ���� ������� �������� �������. ���������� ������ ����� �������� &#8212; �������� ���� � ������� �� ���������, &#8212; � ����� �������� �����������. ����� ������ ���������� &#8212; ��� ��� � ����� ������������ �����������. </DIV>
<P align=justify></P>
<DIV align=right><STRONG>�����: ������ ������������</STRONG> </DIV></DIV>


</body>
</html>

