<!--
ID:35326154
TITLE:����� ����������� ������� � ������� 2005 ����
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:
NUMBER:4
ISSUE_DATE:2005-04-30
RECORD_DATE:2005-09-23


RBR_ID:2039
RBR_NAME:���������� �������
-->
<html>
<head>
<title>
35326154-����� ����������� ������� � ������� 2005 ����
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgwhite>
<TD>
<P class=Main align=justify><EM>�� 1 ����� 2005 ���� � ���������� ������� ����������� 666 ��������� �����������, � ��� ����� 651 � ������ � 15 &#8212; � ���������� ������� (�� 1 ������� &#8212; 668 ������: 653 � ������ � 15 � �������).</EM> 
<P class=Main align=justify><STRONG>���������� ������</STRONG> ��������� ����������� ����������� �������1 ������� � ������� �� 0,7% (��� �� 29,4 ����. ������) � � ������ ����� ��������� 4 053,5 ����. ������. �� ���� ��������������� ��������� ���������� ������������ ������ �� (���� ������� �� ��������� � ����� �� ���� ������ �������� �� 0,95%) ��������� ���� ��������� ���� �������� ������� ������ (� 64,3 �� 64,6%). ������ � ��� ���� �������� � ������������ ������ ��������� � 65 �� 63,2%, ��� � �������� ���� ������� � ����������� �������� �� ������ ������� � ������� ������&shy;���������������, ������������ �� ����� ���, � ����� ����, � ������������ ������ ��������� ����������� ��� � ����������� ������. 
<P class=Main align=justify>� ������� ��� ������������ <STRONG>��������</STRONG> �������� ������� ����� ����� ��������� �� �������� �� ��������� ������, ��� ���� ���� ������������ �������� ����, ��� ����������� ���������� �������. 
<P class=Main align=justify><STRONG>����������� �������</STRONG> ������ ����������� �� 1% � ��������� 653 ����. ������. 
<P class=Main align=justify>� ������� &#8212; � ������� �� ����������� ������ &#8212; � ����� � �������������� ������� ����� ��������� ����������� ����������� ������� ���� <STRONG>������ ����������� ����������</STRONG> ������������ ������ (� ������ ������� ���) ���������� � �������� 5% (�� ��������� � ���������� 11,1%), �� 137,6 ����. ������. <STRONG>���������� ��������� ��������� ����</STRONG> ����� �� 51%, ��������� 20,1 ����. ������. ���������� ��������� �����������, ���������� �� ������ ���� ������ ������� 2005 ���� ���������� �������, �������� �� 565 (�� 1 ������� �� ���� 530).<STRONG> �������� ������� � �����</STRONG> ������ ����������� �� ���������� � �� 1 ����� 2005 ���� ��������� 515,4 ����. ������. 
<P class=Main align=justify>���� <STRONG>�������� �� ��������� ������</STRONG> (�� 2,7%, ��� �� 4,6 ����. ������ &#8212; �� 176,2 ����. ������) ��������� ���������� ���������� ���� ����� ��������, ������ �������� ������� ��� ��������� �� ���� �������� �� ��������� ������ �� ������. 
<P class=Main align=justify>����� �������� ������������ ������ ����������� ������� � ������� ������� �� 0,6%, ��������� 3 224,3 ����. ������ (79,5% ��������). �������� ������� �� ���� ������������ ������� ���������� ���������� ����&#8230; 
<P class=Main align=justify>����� �������� <STRONG>��������� ������</STRONG> ����������� �� 9,2%. �������� �������� (�� 16,5%, �� 296,2 ����. ������) ������� ������� ���������� � �������� �����������, ��������������� � ������ � �������� ������������, � ����������� ������. � ���������� ���� �������� ��������� � ����� ������ ������� ����������� ����������� �� 41,2% (�� 1 ����� &#8212; 47,1%). ��������� ������� ������� ���������� � ����������� ������� ������� &#8212; <STRONG>�������� ���������� ���</STRONG> &#8212; �������������� ��������� ������ ����� �� ��������� � �������. ���, ������ ��������� � ������� ������� �� 3,7% � ������� ������� ����� ������������� ������ (500,2 ����. ������). ������� ������� � �������� ���������� ��� ���������� ����� ��� � �������� ������. � �������� ���������� ������������ ������ ��������� &#8212; � ������� �� ����������� &#8212; �������� ������������ �������� ��������� (53,5% �� ������ ������ ������� ��������� ������ 52,2). 
<P class=Main align=justify><STRONG>�������� �� ��������� � ������� ������ �����������</STRONG> ����������� (�� 1,7%) � ��������� 641,4 ����. ������ ������������� � ���������� ���������� �������� � ������. �� ���������� � �������� ����������� �� ���������� (15,8%). 
<P class=Main align=justify>�� ���� �������������� ������� ����������� � ������� ����������� �������� ���������� <STRONG>����� �������������� ������������</STRONG> ��� ������ �������. ���, ����� ������������ �� ������������ ������������� �������� (� ������ �������� ����� ������) � ����� ���������� �� 4,7% � �������� 634,5 ����. ������, � �� ���� � �������� ��������� � 16,6 �� 15,7%. ������ � ���� ��������� ����������� ������� ���������� ��������� ������������� �� �������� � ������, ������������ �� ���������� ������������� �����. ������������� �� ��������, ���������� �� ������&shy;������������, ��������, ������� �� 2,4%. 
<P class=Main align=justify>������� ������� �� ������ ������ �� ������� ������� � ����� �� 7,2%, ��������� �� 1 ����� 96,4 ����. ������. ������ � ������� �� ��������, ����������� �� ����� ���, ����������� �������� �������, ������������ �� ������&shy;������������. 
<P class=Main align=justify>����� ���������� ����� ������������� ��������� ����� �������, �������������� ������� ����� <STRONG>������� ������ �����</STRONG> (���������� �� 2,2%, �� 474,4 ����. ������). ��� �� ����� �� ���� � �������� � 1 ����� ��������� 11,7%, �� ��������� ������ ������ ���� (12,2%). ������������ ������� �� ����������� �������� ������ ������������ ���� ���������� �������� (����� �� 5%). 
<P class=Main align=justify>� ����� <STRONG>�������� �������� </STRONG>����� ����������� ������� ������������ � ������� ��������� �������� �� ����� ������ ����� � �� ���������� �����, � ����� � ������� ��������� � ����������� ����� ��� ���������� �������� ������� �� ��������� � ������&shy;������������ � �� ��������� � ����� ������. 
<P class=Main align=justify>� ������� <STRONG>�������� ��������</STRONG> ������ ���������� ����������� ����� ���������� ������� ���� ��������������� �������� � �������� ������� ��������. � ��� ��, �������� �� ������ ������������� �������� ��������������, �������� ���������� ������� �������� �� ������ &#171;������� ����������� �����&#187;. ������� ��� �������<STRONG> ���������� �������� ���������� ��������</STRONG> ������ (���� ������������ ������������� � ������� ������� � 0,9 �� 1%), ��� ��������������� ���� ������� � ������ ������������ ������������� �� ��������, �������� ���������� ����� � ������������������ ������� ���������. �������� ������� �� ���������� � ���� �������� ��������� ���������� � ���� ������ &#8212; ������� ������� �������� �����. 
<P class=Main align=justify>���� ��������������� �������� �� ��������� � ������ ������� ���� ��������� ���������� (1,6 ������ 4% � ������), �� ���� � ������� ����� �� ���������� &#8212; 4,7%. ������� ��������� ��������������� ��� � ������, ��� � � ����������� ������. 
<P class=Main align=justify>����� ����� <STRONG>������������ ����������� ��� </STRONG>� ������� ��������� ���� ������ ���������� ������� (0,5 ������ 0,7%). ������ � ��� ���������� ������� ���������� ������� �� ������ ������ ��� ������ � �������� � �������� ������������� ����� ������ ����������� ������� ���� ���������� �� ��������� �� ���������� �������� ������� � ������. ����� �������� ������������ ��������� ������� ��������� ����������� �� ���������� (���������� �� 0,1% &#8212; � ������ �� ���� ��������, ��������������� � ������), �������� 1 667,3 ����. ������. ���������� �������� � ������� ��������� �� 41,1%, ��� ������������� ������ ���� ��������� ����. 
<P class=Main align=justify>������� ������� ������������� �� <STRONG>��������, ��������������� ������&shy;������������, </STRONG>����������� �� ���������� (���������� �� 0,1%, �� 470,9 ����. ������). ������ � ��� ������������ ���������� ������� ��������, ����������� �� ���������� ������������� ����� (�� 12,4%), ���� �������� �������������� ������ �������� ������&shy;������������ (�� 8,9%), ���� ������� � ����� ������ ��� ���������� �� 64% (� ������ &#8212; 59%). 
<P class=Main align=justify>����� ����� ������� �� ������ ������ ��������� ����������� ��������� �� 3,8%, �� 157,6 ����. ������ &#8212; ������������� ���������� ������ �������� ������� �� ������ ������&shy;������������ � ����������� ������. 
<P class=Main align=justify>����� <STRONG>�������� �� ���������� �����</STRONG> ������� �� ��������� ��� ������ ����� (�� 4,4%, �� 173 ����. ������). ������������ ������� �� ���������� � ������� ��������� ����������� ������ �������� ������ ���� �������� �������� ������. 
<P class=Main align=justify>���������� <STRONG>������ �� ����� ������ �����</STRONG> � ������� ���������� ������������ �������. �� ���� ����� ������� ��� (�� 11,9%) ��������� �� ������� � ��������� � ����� (���������� �� 3,4%, �� 99,8 ����. ������). �����<STRONG> ���������������� �����</STRONG> ������ ���������������� ������ ������ �� ��������� � ���������� �������, ���� �������������� ���������� ������������� �������� �������� �� ����������� ������� � ��������� �������� ��������� ��������. � ���������� �������� � �������� ������������� �� ����������� �� 7,1%, �� 109,5 ����. ������. ������ ���� ����� ������ ������ �������� ������������ �������� �� 1,1% ����, ��� � ����� ���������� �������� ������ ����� (�� 1,6%) ���������� ������������� ���������� �������, ��������� � �������� ������������� ����������� ���������� (������ ����� � ���������� ������������ ������ �� ������� ������ � �������� ������������). �� 1 �����, �� ��������� � ���������� �������, ����� �������� ������ ������� � ������ ������ ����� �� 7 ����. ������, �� 444,7 ����. ������. 
<P class=Main align=justify>��������� �����������<STRONG> ��������� ������</STRONG> ������ ������� &#8212; ����� ����� �������� �� ��������� ��������� �����������, ��������� � ���� ����������� ������� � ����� ������, � ����� �������� ������� � ����������� ��������, ��������� �� 0,3%, �� 396,5 ����. ������. 
<P class=Main align=justify><EM>1. ������ ���������� �� 663 ��������� ������������, ������������� ���������� ���������� � ���������� ��� ����� ������ �� 1 ����� 2005 �. � ��� ����� ����������� �� ��������� ������</EM> 
<DIV align=center>
<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR>
<TD class=bgcolor1>
<P class=Rubrika>��������� �������� ������� ��������� ����������� ����������� �������, ���. ������</P></TD></TR>
<TR>
<TD class=bgwhite>
<TABLE cellSpacing=0 cellPadding=0 width="100%" align=center border=1>
<TBODY>
<TR>
<TD width="43%">
<P align=center><STRONG>������</STRONG></P></TD>
<TD width="12%">
<P align=center><STRONG>01.02.2005</STRONG></P></TD>
<TD width="12%">
<P align=center><STRONG>01.03.2005</STRONG></P></TD>
<TD width="12%">
<P align=center><STRONG>��. ��� 01.02.04</STRONG></P></TD>
<TD width="10%">
<P align=center><STRONG>��. ��� 01.03.05</STRONG></P></TD>
<TD width="11%">
<P align=center><STRONG>�������� �� ������� 2005 ���� </STRONG></P></TD></TR>
<TR>
<TD>
<P>������ �������-�����</P></TD>
<TD>
<P align=right>4 024 155,5</P></TD>
<TD>
<P align=right>4 053 547,1</P></TD>
<TD>
<P align=right>100,0</P></TD>
<TD>
<P align=right>100,0</P></TD>
<TD>
<P align=right>100,7</P></TD></TR>
<TR>
<TD>
<P><STRONG>����������� ��������:</STRONG></P></TD>
<TD>
<P align=right><STRONG>646 658,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>653 043,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>16,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>16,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>101,0</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�.: ����� ������</P></TD>
<TD>
<P align=right>515 528,1</P></TD>
<TD>
<P align=right>515 406,5</P></TD>
<TD>
<P align=right>12,8</P></TD>
<TD>
<P align=right>12,7</P></TD>
<TD>
<P align=right>100,0</P></TD></TR>
<TR>
<TD>
<P>�������</P></TD>
<TD>
<P align=right>131 130,6</P></TD>
<TD>
<P align=right>137 637,0</P></TD>
<TD>
<P align=right>3,3</P></TD>
<TD>
<P align=right>3,4</P></TD>
<TD>
<P align=right>105,0</P></TD></TR>
<TR>
<TD>
<P>������� �� ��������� ������</P></TD>
<TD>
<P align=right>171 565,4</P></TD>
<TD>
<P align=right>176 191,1</P></TD>
<TD>
<P align=right>4,3</P></TD>
<TD>
<P align=right>4,3</P></TD>
<TD>
<P align=right>102,7</P></TD></TR>
<TR>
<TD>
<P>� �.�. : ������� ��� ��������� ������ �� ������</P></TD>
<TD>
<P align=right>145 144,0</P></TD>
<TD>
<P align=right>149 009,3</P></TD>
<TD>
<P align=right>3,6</P></TD>
<TD>
<P align=right>3,7</P></TD>
<TD>
<P align=right>102,7</P></TD></TR>
<TR>
<TD>
<P>������� ��� ��������� ������ �� ������ �������</P></TD>
<TD>
<P align=right>5 659,0</P></TD>
<TD>
<P align=right>5 833,4</P></TD>
<TD>
<P align=right>0,1</P></TD>
<TD>
<P align=right>0,1</P></TD>
<TD>
<P align=right>103,1</P></TD></TR>
<TR>
<TD>
<P>������ �������</P></TD>
<TD>
<P align=right>20 762,4</P></TD>
<TD>
<P align=right>21 348,4</P></TD>
<TD>
<P align=right>0,5</P></TD>
<TD>
<P align=right>0,5</P></TD>
<TD>
<P align=right>102,8</P></TD></TR>
<TR>
<TD>
<P><STRONG>�������������:</STRONG></P></TD>
<TD>
<P align=right><STRONG>3 205 931,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>3 224 312,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>79,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>79,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>100,6</STRONG></P></TD></TR>
<TR>
<TD>
<P>�������� ��������</P></TD>
<TD>
<P align=right>908 110,9</P></TD>
<TD>
<P align=right>851 309,8</P></TD>
<TD>
<P align=right>22,6</P></TD>
<TD>
<P align=right>21,0</P></TD>
<TD>
<P align=right>93,7</P></TD></TR>
<TR>
<TD>
<P>� �.�. : �������� �� ����.� ���.������ �����������</P></TD>
<TD>
<P align=right>630 799,4</P></TD>
<TD>
<P align=right>641 355,9</P></TD>
<TD>
<P align=right>15,7</P></TD>
<TD>
<P align=right>15,8</P></TD>
<TD>
<P align=right>101,7</P></TD></TR>
<TR>
<TD>
<P>������ �����</P></TD>
<TD>
<P align=right>142 211,9</P></TD>
<TD>
<P align=right>139 434,6</P></TD>
<TD>
<P align=right>3,5</P></TD>
<TD>
<P align=right>3,4</P></TD>
<TD>
<P align=right>98,0</P></TD></TR>
<TR>
<TD>
<P><STRONG>�������� (����� ������)</STRONG></P></TD>
<TD>
<P align=right><STRONG>943 794,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>1 030 721,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>23,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>25,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>109,2</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�.: ���������</P></TD>
<TD>
<P align=right>482 581,2</P></TD>
<TD>
<P align=right>500 203,8</P></TD>
<TD>
<P align=right>12,0</P></TD>
<TD>
<P align=right>12,3</P></TD>
<TD>
<P align=right>103,7</P></TD></TR>
<TR>
<TD>
<P>����������� ���</P></TD>
<TD>
<P align=right>461 212,8</P></TD>
<TD>
<P align=right>530 517,5</P></TD>
<TD>
<P align=right>11,5</P></TD>
<TD>
<P align=right>13,1</P></TD>
<TD>
<P align=right>115,0</P></TD></TR>
<TR>
<TD>
<P>� �.�.: �������� �����������</P></TD>
<TD>
<P align=right>254 305,8</P></TD>
<TD>
<P align=right>296 159,1</P></TD>
<TD>
<P align=right>6,3</P></TD>
<TD>
<P align=right>7,3</P></TD>
<TD>
<P align=right>116,5</P></TD></TR>
<TR>
<TD>
<P>������ ��������</P></TD>
<TD>
<P align=right>206 907,0</P></TD>
<TD>
<P align=right>234 358,4</P></TD>
<TD>
<P align=right>5,1</P></TD>
<TD>
<P align=right>5,8</P></TD>
<TD>
<P align=right>113,3</P></TD></TR>
<TR>
<TD>
<P><STRONG>����� ������ </STRONG></P></TD>
<TD>
<P align=right><STRONG>89 881,9</STRONG></P></TD>
<TD>
<P align=right><STRONG>96 388,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>2,2</STRONG></P></TD>
<TD>
<P align=right><STRONG>2,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>107,2</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�.: �������� ���������� ������</P></TD>
<TD>
<P align=right>49 201,7</P></TD>
<TD>
<P align=right>57 591,3</P></TD>
<TD>
<P align=right>1,2</P></TD>
<TD>
<P align=right>1,4</P></TD>
<TD>
<P align=right>117,1</P></TD></TR>
<TR>
<TD>
<P>�������� ������-������������</P></TD>
<TD>
<P align=right>16 519,7</P></TD>
<TD>
<P align=right>14 933,3</P></TD>
<TD>
<P align=right>0,4</P></TD>
<TD>
<P align=right>0,4</P></TD>
<TD>
<P align=right>90,4</P></TD></TR>
<TR>
<TD>
<P><STRONG>��� ����������, ������� ������������ �������������, ��� ������������ % (� ������ �������� ����� ������)</STRONG></P></TD>
<TD>
<P align=right><STRONG>665 602,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>634 500,8</STRONG></P></TD>
<TD>
<P align=right><STRONG>16,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>15,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>95,3</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�. ��� ������-������������</P></TD>
<
TD>
<P align=right>445 657,3</P></TD>
<TD>
<P align=right>456 163,8</P></TD>
<TD>
<P align=right>11,1</P></TD>
<TD>
<P align=right>11,3</P></TD>
<TD>
<P align=right>102,4</P></TD></TR>
<TR>
<TD>
<P><STRONG>������ ������ ����������</STRONG></P></TD>
<TD>
<P align=right><STRONG>464 021,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>474 405,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>11,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>11,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>102,2</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>������ �������</STRONG></P></TD>
<TD>
<P align=right><STRONG>134 521,2</STRONG></P></TD>
<TD>
<P align=right><STRONG>136 987,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>3,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>3,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>101,8</STRONG></P></TD></TR>
<TR>
<TD>
<P align=center><STRONG>�����</STRONG></P></TD>
<TD>
<P align=right></P></TD>
<TD>
<P align=right></P></TD>
<TD>
<P align=right></P></TD>
<TD>
<P align=right></P></TD>
<TD>
<P align=right></P></TD></TR>
<TR>
<TD>
<P><STRONG>������ �������-�����</STRONG></P></TD>
<TD>
<P align=right><STRONG>4 024 155,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>4 053 547,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>100,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>100,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>100,7</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>������� � ������������ � ��� ������������� (� ������ ��������) ����� ��� ������������ ���������</STRONG></P></TD>
<TD>
<P align=right><STRONG>2 721 838,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>2 741 465,2</STRONG></P></TD>
<TD>
<P align=right><STRONG>67,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>67,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>100,7</STRONG></P></TD></TR>
<TR>
<TD>
<P>����� �������� (����� ������)</P></TD>
<TD>
<P align=right>2 085 455,5</P></TD>
<TD>
<P align=right>2 097 233,6</P></TD>
<TD>
<P align=right>51,8</P></TD>
<TD>
<P align=right>51,7</P></TD>
<TD>
<P align=right>100,6</P></TD></TR>
<TR>
<TD>
<P>� �.�. ����� ���������</P></TD>
<TD>
<P align=right>186 793,6</P></TD>
<TD>
<P align=right>189 713,3</P></TD>
<TD>
<P align=right>4,6</P></TD>
<TD>
<P align=right>4,7</P></TD>
<TD>
<P align=right>101,6</P></TD></TR>
<TR>
<TD>
<P>����� ��.�����</P></TD>
<TD>
<P align=right>1 898 662,0</P></TD>
<TD>
<P align=right>1 907 520,3</P></TD>
<TD>
<P align=right>47,2</P></TD>
<TD>
<P align=right>47,1</P></TD>
<TD>
<P align=right>100,5</P></TD></TR>
<TR>
<TD>
<P>� �.�. ����� ������������</P></TD>
<TD>
<P align=right>1 665 185,4</P></TD>
<TD>
<P align=right>1 667 348,7</P></TD>
<TD>
<P align=right>41,4</P></TD>
<TD>
<P align=right>41,1</P></TD>
<TD>
<P align=right>100,1</P></TD></TR>
<TR>
<TD>
<P>������ ����� ��������</P></TD>
<TD>
<P align=right>233 476,5</P></TD>
<TD>
<P align=right>240 171,7</P></TD>
<TD>
<P align=right>5,8</P></TD>
<TD>
<P align=right>5,9</P></TD>
<TD>
<P align=right>102,9</P></TD></TR>
<TR>
<TD>
<P>������������ ������������� ���</P></TD>
<TD>
<P align=right>33 288,4</P></TD>
<TD>
<P align=right>35 362,5</P></TD>
<TD>
<P align=right>0,8</P></TD>
<TD>
<P align=right>0,9</P></TD>
<TD>
<P align=right>106,2</P></TD></TR>
<TR>
<TD>
<P>��� ��������</P></TD>
<TD>
<P align=right>470 371,8</P></TD>
<TD>
<P align=right>470 913,8</P></TD>
<TD>
<P align=right>11,7</P></TD>
<TD>
<P align=right>11,6</P></TD>
<TD>
<P align=right>100,1</P></TD></TR>
<TR>
<TD>
<P>� �.�. ��� ������-������������</P></TD>
<TD>
<P align=right>276 847,6</P></TD>
<TD>
<P align=right>301 452,9</P></TD>
<TD>
<P align=right>6,9</P></TD>
<TD>
<P align=right>7,4</P></TD>
<TD>
<P align=right>108,9</P></TD></TR>
<TR>
<TD>
<P>� �.�.������������ �������������</P></TD>
<TD>
<P align=right>3 125,9</P></TD>
<TD>
<P align=right>3 123,4</P></TD>
<TD>
<P align=right>0,1</P></TD>
<TD>
<P align=right>0,1</P></TD>
<TD>
<P align=right>99,9</P></TD></TR>
<TR>
<TD>
<P>�������� �������</P></TD>
<TD>
<P align=right>165 659,7</P></TD>
<TD>
<P align=right>172 976,9</P></TD>
<TD>
<P align=right>4,1</P></TD>
<TD>
<P align=right>4,3</P></TD>
<TD>
<P align=right>104,4</P></TD></TR>
<TR>
<TD>
<P>� �.�. ������������</P></TD>
<TD>
<P align=right>954,5</P></TD>
<TD>
<P align=right>967,1</P></TD>
<TD>
<P align=right>0,0</P></TD>
<TD>
<P align=right>0,0</P></TD>
<TD>
<P align=right>101,3</P></TD></TR>
<TR>
<TD>
<P>����������� ������������� �����*</P></TD>
<TD>
<P align=right>37 368,8</P></TD>
<TD>
<P align=right>39 453,0</P></TD>
<TD>
<P align=right>0,9</P></TD>
<TD>
<P align=right>1,0</P></TD>
<TD>
<P align=right>105,6</P></TD></TR>
<TR>
<TD>
<P><STRONG>������ � ������ ������������ ������ (������)</STRONG></P></TD>
<TD>
<P align=right><STRONG>351,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>340,9</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>97,1</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>������������ ��������</STRONG></P></TD>
<TD>
<P align=right><STRONG>2 941,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>2 950,8</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>100,3</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>�������� � ������ ������</STRONG></P></TD>
<TD>
<P align=right><STRONG>437 692,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>444 707,8</STRONG></P></TD>
<TD>
<P align=right><STRONG>10,9</STRONG></P></TD>
<TD>
<P align=right><STRONG>11,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>101,6</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�. � ���������������</P></TD>
<TD>
<P align=right>102 234,7</P></TD>
<TD>
<P align=right>109 484,9</P></TD>
<TD>
<P align=right>2,5</P></TD>
<TD>
<P align=right>2,7</P></TD>
<TD>
<P align=right>107,1</P></TD></TR>
<TR>
<TD>
<P>�����</P></TD>
<TD>
<P align=right>96 540,1</P></TD>
<TD>
<P align=right>99 791,5</P></TD>
<TD>
<P align=right>2,4</P></TD>
<TD>
<P align=right>2,5</P></TD>
<TD>
<P align=right>103,4</P></TD></TR>
<TR>
<TD>
<P><STRONG>����������� �������� �� ������� ��������� ����</STRONG></P></TD>
<TD>
<P align=right><STRONG>21 445,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>22 649,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>105,6</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>���</STRONG></P></TD>
<TD>
<P align=right><STRONG>61 354,2</STRONG></P></TD>
<TD>
<P align=right><STRONG>61 629,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>1,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>1,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>100,4</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>��������, �������� �������� � ����������� �������</STRONG></P></TD>
<TD>
<P align=right><STRONG>561 844,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>554 196,8</STRONG></P></TD>
<TD>
<P align=right><STRONG>14,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>13,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>98,6</STRONG></P></TD></TR>
<TR>
<TD>
<P>� �.�. : �������� � ������</P></TD>
<TD>
<P align=right>163 930,8</P></TD>
<TD>
<P align=right>157 654,6</P></TD>
<TD>
<P align=right>4,1</P></TD>
<TD>
<P align=right>3,9</P></TD>
<TD>
<P align=right>96,2</P></TD></TR>
<TR>
<TD>
<P>� �. �. �������� � ������-������������</P></TD>
<TD>
<P align=right>116 954,4</P></TD>
<TD>
<P align=right>102 481,5</P></TD>
<TD>
<P align=right>2,9</P></TD>
<TD>
<P align=right>2,5</P></TD>
<TD>
<P align=right>87,6</P></TD></TR>
<TR>
<TD>
<P><STRONG>������� � ��������� ��������� �������</STRONG></P></TD>
<TD>
<P align=right><STRONG>23 140,4</STRONG></P></TD>
<TD>
<P align=right><STRONG>23 804,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>0,6</STRONG></P></TD>
<TD>
<P align=right><STRONG>102,9</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>���������, ����������� ��������</STRONG></P></TD>
<TD>
<P align=right><STRONG>69 070,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>70 055,0</STRONG></P></TD>
<TD>
<P align=right><STRONG>1,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>1,7</STRONG></P></TD>
<TD>
<P align=right><STRONG>101,4</STRONG></P></TD></TR>
<TR>
<TD>
<P><STRONG>������ ������</STRONG></P></TD>
<TD>
<P align=right><STRONG>124 829,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>132 088,5</STRONG></P></TD>
<TD>
<P align=right><STRONG>3,1</STRONG></P></TD>
<TD>
<P align=right><STRONG>3,3</STRONG></P></TD>
<TD>
<P align=right><STRONG>105,8</STRONG></P></TD></TR>
<TR>
<TD colSpan=6>
<P class=Ssilka>* � ����� � ������� ����������� ����� ������ �� ��������� ������ 47901 � 47902 � �������������� ���������� �������� ������� ������ � ������ ���� ���� ����������� �������� ��������� ���������� ������: ������� �� ��������� ������ �� ������, ������ �������, ������� � ������������ � ��� �������������, ������ ������.</P></TD></TR></TBODY></TABLE></TD></TR>
<TR>
<TD></TD></TR></TBODY></TABLE></DIV></TD></TR></TBODY></TABLE>


</body>
</html>

