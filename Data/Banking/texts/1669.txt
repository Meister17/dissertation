<!--
ID:36328264
TITLE:�� &#171;����������&#187;
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:
NUMBER:4
ISSUE_DATE:2006-04-30
RECORD_DATE:2006-06-13


-->
<html>
<head>
<title>
36328264-�� &#171;����������&#187;
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgwhite>
<TD></TD></TR>
<TR class=bodycolor>
<TD>
<P class=Rubrika align=center>������������ ���� &#171;����������&#187;<BR>(�������� � ������������ ����������������)</P>
<P class=Main align=center>��������������� ����� 2192, ��� 044583337, 
<P class=Main align=center>�������� �����: 111024, �. ������, ��. ���������, 46�</P></TD></TR>
<TR class=bgwhite>
<TD align=middle height=0>
<P class=Rubrika>������ �� 1 ������ 2006 ����</P>
<TABLE cellSpacing=0 cellPadding=0 width="98%" borderColorLight=#cccccc border=2>
<TBODY>
<TR>
<TD>
<P align=center><STRONG>����� �/�</STRONG></P></TD>
<TD>
<P align=center><STRONG>������������ ������</STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� �������� ����</STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� ������ ��������� ����</STRONG></P></TD></TR>
<TR>
<TD width=63 height=0>
<P align=center><STRONG>1</STRONG></P></TD>
<TD width=322>
<P align=center><STRONG>2</STRONG></P></TD>
<TD width=92>
<P align=center><STRONG>3</STRONG></P></TD>
<TD width=110>
<P align=center><STRONG>4</STRONG></P></TD></TR>
<TR>
<TD colSpan=4 height=0>
<P align=center><B>������</B></P></TD></TR>
<TR>
<TD>
<P>1.</P></TD>
<TD>
<P>�������� ��������</P></TD>
<TD>
<P align=right>6 361</P></TD>
<TD>
<P align=right>11 043</P></TD></TR>
<TR>
<TD>
<P>2.</P></TD>
<TD>
<P>�������� ��������� ����������� � ����������� ����� ���������� ���������</P></TD>
<TD>
<P align=right>243 451</P></TD>
<TD>
<P align=right>525 815</P></TD></TR>
<TR>
<TD>
<P>2.1.</P></TD>
<TD>
<P>������������ �������</P></TD>
<TD>
<P align=right>8 793</P></TD>
<TD>
<P align=right>8 499</P></TD></TR>
<TR>
<TD>
<P>3.</P></TD>
<TD>
<P>�������� � ��������� ������������</P></TD>
<TD>
<P align=right>25 466</P></TD>
<TD>
<P align=right>10 955</P></TD></TR>
<TR>
<TD>
<P>4.</P></TD>
<TD>
<P>������ �������� � �������� ������ ������</P></TD>
<TD>
<P align=right>1 199</P></TD>
<TD>
<P align=right>44 192</P></TD></TR>
<TR>
<TD>
<P>5.</P></TD>
<TD>
<P>������ ������� �������������</P></TD>
<TD>
<P align=right>104 572</P></TD>
<TD>
<P align=right>211 216</P></TD></TR>
<TR>
<TD>
<P>6.</P></TD>
<TD>
<P>������ �������� � �������������� ������ ������, ������������ �� ���������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>7.</P></TD>
<TD>
<P>������ �������� � ������ ������, ��������� � ������� ��� �������</P></TD>
<TD>
<P align=right>1 251</P></TD>
<TD>
<P align=right>1 251</P></TD></TR>
<TR>
<TD>
<P>8.</P></TD>
<TD>
<P>�������� ��������, �������������� ������ � ������������ ������</P></TD>
<TD>
<P align=right>8 117</P></TD>
<TD>
<P align=right>4 611</P></TD></TR>
<TR>
<TD>
<P>9.</P></TD>
<TD>
<P>���������� �� ��������� ���������</P></TD>
<TD>
<P align=right>269</P></TD>
<TD>
<P align=right>1 269</P></TD></TR>
<TR>
<TD>
<P>10.</P></TD>
<TD>
<P>������ ������</P></TD>
<TD>
<P align=right>8 416</P></TD>
<TD>
<P align=right>11 415</P></TD></TR>
<TR>
<TD>
<P>11.</P></TD>
<TD>
<P>����� �������</P></TD>
<TD>
<P align=right>399 102</P></TD>
<TD>
<P align=right>821 767</P></TD></TR>
<TR align=middle>
<TD colSpan=4>
<P><B>II. �������</B></P></TD></TR>
<TR>
<TD>
<P>12.</P></TD>
<TD>
<P>������� ������������ ����� ���������� ���������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>13.</P></TD>
<TD>
<P>�������� ��������� �����������</P></TD>
<TD>
<P align=right>6</P></TD>
<TD>
<P align=right>1 797</P></TD></TR>
<TR>
<TD>
<P>14.</P></TD>
<TD>
<P>�������� �������� (����������� �����������)</P></TD>
<TD>
<P align=right>354 990</P></TD>
<TD>
<P align=right>773 647</P></TD></TR>
<TR>
<TD>
<P>14.1.</P></TD>
<TD>
<P>������ ���������� ���</P></TD>
<TD>
<P align=right>30 471</P></TD>
<TD>
<P align=right>30 624</P></TD></TR>
<TR>
<TD>
<P>15.</P></TD>
<TD>
<P>���������� �������� �������������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>4 692</P></TD></TR>
<TR>
<TD>
<P>16.</P></TD>
<TD>
<P>������������� �� ������ ���������</P></TD>
<TD>
<P align=right>584</P></TD>
<TD>
<P align=right>1 659</P></TD></TR>
<TR>
<TD>
<P>17.</P></TD>
<TD>
<P>������ �������������</P></TD>
<TD>
<P align=right>2 813</P></TD>
<TD>
<P align=right>1 030</P></TD></TR>
<TR>
<TD>
<P>18.</P></TD>
<TD>
<P>������� �� ��������� ������ �� �������� �������������� ���������� ���������, ������ ��������� ������� � �� ��������� � ����������� �������� ���</P></TD>
<TD>
<P align=right>61</P></TD>
<TD>
<P align=right>276</P></TD></TR>
<TR>
<TD>
<P>19.</P></TD>
<TD>
<P>����� ������������</P></TD>
<TD>
<P align=right>358 454</P></TD>
<TD>
<P align=right>783 101</P></TD></TR>
<TR align=middle>
<TD colSpan=4>
<P class=bold><STRONG>III. ��������� ����������� �������</STRONG></P></TD></TR>
<TR>
<TD>
<P>20.</P></TD>
<TD>
<P>�������� ���������� (����������)</P></TD>
<TD>
<P align=right>20 200</P></TD>
<TD>
<P align=right>20 200</P></TD></TR>
<TR>
<TD>
<P>20.1.</P></TD>
<TD>
<P>������������������ ������������ ����� � ����</P></TD>
<TD>
<P align=right>20 200</P></TD>
<TD>
<P align=right>20 200</P></TD></TR>
<TR>
<TD>
<P>20.2.</P></TD>
<TD>
<P>������������������ ����������������� �����</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>20.3.</P></TD>
<TD>
<P>�������������������� �������� ������� ������������� ��������� �����������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>21.</P></TD>
<TD>
<P>����������� �����, ����������� � ����������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>22.</P></TD>
<TD>
<P>����������� �����</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>23.</P></TD>
<TD>
<P>���������� �������� �������</P></TD>
<TD>
<P align=right>289</P></TD>
<TD>
<P align=right>289</P></TD></TR>
<TR>
<TD>
<P>24.</P></TD>
<TD>
<P>������� ������� �������� � ����������� �������, �������� �� ����������� �������� (�������)</P></TD>
<TD>
<P align=right>566</P></TD>
<TD>
<P align=right>807</P></TD></TR>
<TR>
<TD>
<P>25.</P></TD>
<TD>
<P>����� � ���������������� ������� ������� ��� � ������������ ��������� ����������� (������������ ������ ������� ���)</P></TD>
<TD>
<P align=right>17 323</P></TD>
<TD>
<P align=right>17 897</P></TD></TR>
<TR>
<TD>
<P>26.</P></TD>
<TD>
<P>������� � ������������� (������) �� �������� ������</P></TD>
<TD>
<P align=right>3 402</P></TD>
<TD>
<P align=right>1 087</P></TD></TR>
<TR>
<TD>
<P>27.</P></TD>
<TD>
<P>����� ���������� ����������� �������</P></TD>
<TD>
<P align=right>40 648</P></TD>
<TD>
<P align=right>38 666</P></TD></TR>
<TR>
<TD>
<P>28.</P></TD>
<TD>
<P>����� ��������</P></TD>
<TD>
<P align=right>399 102</P></TD>
<TD>
<P align=right>821 767</P></TD></TR>
<TR align=middle>
<TD colSpan=4>
<P><B>IV. ������������� �������������</B></P></TD></TR>
<TR>
<TD>
<P>29.</P></TD>
<TD>
<P>����������� ������������� ��������� �����������</P></TD>
<TD>
<P align=right>41 011</P></TD>
<TD>
<P align=right>15 043</P></TD></TR>
<TR>
<TD>
<P>30.</P></TD>
<TD>
<P>��������, �������� ��������� ������������</P></TD>
<TD>
<P align=right>3 564</P></TD>
<TD>
<P align=right>1 200</P></TD></TR></TBODY></TABLE>
<P class=top align=justify>��������, ���������� ��������� �� ���� ������� ������� V &#171;����� �������������� ����������&#187;, �� �������������� 
<P class=Rubrika>����� � �������� � ������� �� 2006 ���.</P>
<TABLE cellSpacing=0 cellPadding=0 width="98%" borderColorLight=#cccccc border=2>
<TBODY>
<TR>
<TD>
<P align=center><STRONG>����� �/�</STRONG></P></TD>
<TD>
<P align=center><STRONG>������������ ������</STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� �������� ������ </STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� ��������������� ������ �������� ����</STRONG></P></TD></TR>
<TR>
<TD width=63 height=0>
<P align=center><STRONG>1</STRONG></P></TD>
<TD width=229>
<P align=center><STRONG>2</STRONG></P></TD>
<TD width=149>
<P align=center><STRONG>3</STRONG></P></TD>
<TD width=126>
<P align=center><STRONG>4</STRONG></P></TD></TR>
<TR>
<TD colSpan=4 height=0>
<P class=bold align=center><STRONG>�������� ���������� � ����������� ������ ��:</STRONG></P></TD></TR>
<TR>
<TD>
<P>1</P></TD>
<TD>
<P>���������� ������� � ��������� ������������</P></TD>
<TD>
<P align=right>1 114</P></TD>
<TD>
<P align=right>538</P></TD></TR>
<TR>
<TD>
<P>2</P></TD>
<TD>
<P>����, ��������������� �������� (����������� ������������)</P></TD>
<TD>
<P align=right>26 883</P></TD>
<TD>
<P align=right>21 000</P></TD></TR>
<TR>
<TD>
<P>3</P></TD>
<TD>
<P>�������� ����� �� ���������� ������ (�������)</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>4</P></TD>
<TD>
<P>������ ����� � ������������� �������</P></TD>
<TD>
<P align=right>581</P></TD>
<TD>
<P align=right>2 434</P></TD></TR>
<TR>
<TD>
<P>5</P></TD>
<TD>
<P>������ ����������</P></TD>
<TD>
<P align=right>835</P></TD>
<TD>
<P align=right>764</P></TD></TR>
<TR>
<TD>
<P>6</P></TD>
<TD>
<P>����� ��������� ���������� � ����������� �������</P></TD>
<TD>
<P align=right>29 413</P></TD>
<TD>
<P align=right>24 736</P></TD></TR>
<TR vAlign=top align=middle>
<TD colSpan=4>
<P><B>�������� ���������� � ����������� ������� ��:</B></P></TD></TR>
<TR>
<TD>
<P>7</P></TD>
<TD>
<P>������������ ��������� ��������� �����������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>8</P></TD>
<TD>
<P>������������ ��������� �������� (����������� �����������)</P></TD>
<TD>
<P align=right>4 748</P></TD>
<TD>
<P align=right>2 430</P></TD></TR>
<TR>
<TD>
<P>9</P></TD>
<TD>
<P>���������� �������� ��������������</P></TD>
<TD>
<P align=right>133</P></TD>
<TD>
<P align=right>266</P></TD></TR>
<TR>
<TD>
<P>10</P></TD>
<TD>
<P>����� ��������� ���������� � ����������� ��������</P></TD>
<TD>
<P align=right>4 881</P></TD>
<TD>
<P align=right>2 696</P></TD></TR>
<TR>
<TD>
<P>11</P></TD>
<TD>
<P>������ ���������� � ����������� ������</P></TD>
<TD>
<P align=right>24 532</P></TD>
<TD>
<P align=right>22 040</P></TD></TR>
<TR>
<TD>
<P>12</P></TD>
<TD>
<P>������ ������ �� �������� � ������� ��������</P></TD>
<TD>
<P align=right>5 700</P></TD>
<TD>
<P align=right>6 341</P></TD></TR>
<TR>
<TD>
<P>13</P></TD>
<TD>
<P>������ ������ �� �������� � ����������� �������</P></TD>
<TD>
<P align=right>370</P></TD>
<TD>
<P align=right>75</P></TD></TR>
<TR>
<TD>
<P>14</P></TD>
<TD>
<P>������ ������ �� �������� � ������������ ��������� � ������� ����������� �������������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>15</P></TD>
<TD>
<P>������ ������ �� ���������� ����������� ������</P></TD>
<TD>
<P align=right>-58</P></TD>
<TD>
<P align=right>-1</P></TD></TR>
<TR>
<TD>
<P>16</P></TD>
<TD>
<P>������������ ������</P></TD>
<TD>
<P align=right>5 701</P></TD>
<TD>
<P align=right>6 329</P></TD></TR>
<TR>
<TD>
<P>17</P></TD>
<TD>
<P>������������ �������</P></TD>
<TD>
<P align=right>37</P></TD>
<TD>
<P align=right>16</P></TD></TR>
<TR>
<TD>
<P>18</P></TD>
<TD>
<P>������ ������ �� ������� ��������</P></TD>
<TD>
<P align=right>193</P></TD>
<TD>
<P align=right>538</P></TD></TR>
<TR>
<TD>
<P>19</P></TD>
<TD>
<P>������ ������ ������������ ������</P></TD>
<TD>
<P align=right>-437</P></TD>
<TD>
<P align=right>325</P></TD></TR>
<TR>
<TD>
<P>20</P></TD>
<TD>
<P>���������������-�������������� �������</P></TD>
<TD>
<P align=right>33 241</P></TD>
<TD>
<P align=right>32 228</P></TD></TR>
<TR>
<TD>
<P>21</P></TD>
<TD>
<P>������� �� ��������� ������</P></TD>
<TD>
<P align=right>3 799</P></TD>
<TD>
<P align=right>-403</P></TD></TR>
<TR>
<TD>
<P>22</P></TD>
<TD>
<P>������� �� ���������������</P></TD>
<TD>
<P align=right>6 522</P></TD>
<TD>
<P align=right>3 000</P></TD></TR>
<TR>
<TD>
<P>23</P></TD>
<TD>
<P>����������� ������ (������� ����� �� �������)</P></TD>
<TD>
<P align=right>2 985</P></TD>
<TD>
<P align=right>2 081</P></TD></TR>
<TR>
<TD>
<P>24</P></TD>
<TD>
<P>������� �� �������� ������</P></TD>
<TD>
<P align=right>3 537</P></TD>
<TD>
<P align=right>919</P></TD></TR></TBODY></TABLE>
<P class=Rubrika>���������� �� ������ ������������� ��������,<BR>�������� �������� �� �������� ������������ ����<BR>� ���� ������� �� 1 ������ 2006 ����</P>
<TABLE cellSpacing=0 cellPadding=0 width="95%" borderColorLight=#cccccc border=2>
<TBODY>
<TR vAlign=top align=middle>
<TD vAlign=center width=42 height=39>
<P align=center><B>&#8470; �/�</B></P></TD>
<TD vAlign=center width=0>
<P class=bold>������������ ����������</P></TD>
<TD width=88>
<P class=bold align=center><STRONG>������</STRONG> �� �������� ����</P></TD>
<TD width=88>
<P class=bold align=center><STRONG>������ �� ������ ��������� ����</STRONG></P></TD></TR>
<TR>
<TD height=0>
<P align=center><STRONG>1</STRONG></P></TD>
<TD>
<P align=center><STRONG>2</STRONG></P></TD>
<TD>
<P align=center><STRONG>3</STRONG></P></TD>
<TD>
<DIV align=center>
<P><STRONG>4</STRONG></P></DIV></TD></TR>
<TR>
<TD>
<P align=center>1</P></TD>
<TD>
<P>����������� �������� (�������), ���. ���.</P></TD>
<TD>
<P align=right>57 645</P></TD>
<TD>
<P align=right>38 664</P></TD></TR>
<TR>
<TD>
<P align=center>2</P></TD>
<TD>
<P>����������� �������� ������������� ����������� ������� (��������), �������</P></TD>
<TD>
<P align=right>35,0</P></TD>
<TD>
<P align=right>13,0</P></TD></TR>
<TR>
<TD>
<P align=center>3</P></TD>
<TD>
<P>����������� �������� ������������� ����������� ������� (��������), �������</P></TD>
<TD>
<P align=right>11,0</P></TD>
<TD>
<P align=right>11,0</P></TD></TR>
<TR>
<TD>
<P align=center>4</P></TD>
<TD>
<P>��������� ������ �� ��������� ������ �� ������, ������� � ������������ � ��� �������������, ���. ���.</P></TD>
<TD>
<P align=right>25 559</P></TD>
<TD>
<P align=right>39 689</P></TD></TR>
<TR>
<TD>
<P align=center>5</P></TD>
<TD>
<P>���������� �������������� ������ �� ��������� ������ �� ������, ������� � ������������ � ��� �������������, ���. ���.</P></TD>
<TD>
<P align=right>25 559</P></TD>
<TD>
<P align=right>39 689</P></TD></TR>
<TR>
<TD>
<P align=center>6</P></TD>
<TD>
<P>��������� ������ �� ��������� ������, ���. ���.</P></TD>
<TD>
<P align=right>81</P></TD>
<TD>
<P align=right>855</P></TD></TR>
<TR>
<TD>
<P align=center>7</P></TD>
<TD>
<P>���������� �������������� ������ �� ��������� ������, ���. ���.</P></TD>
<TD>
<P align=right>81</P></TD>
<TD>
<P align=right>855</P></TD></TR></TBODY></TABLE>
<TABLE cellSpacing=0 cellPadding=0 width="95%" border=0>
<TBODY>
<TR>
<TD height=0>
<P class=top>������������ ��������� ��������� �����������</P></TD>
<TD>
<P align=right><STRONG>������� �.�.</STRONG></P></TD></TR>
<TR>
<TD height=0>
<P class=top>������� ��������� ��������� �����������</P></TD>
<TD>
<P align=right><STRONG>������� �.�.</STRONG></P></TD></TR></TBODY></TABLE>
<P class=Ssilka align=left>�� ������ ��������� ������������ �������� &#171;����� �����&#187;, ������������� ������, ����� � �������� � �������, ����� �� ������ ������������� ��������, �������� �������� �� �������� ������������ ���� � ���� �������, �������� ���������� �� ���� ������������ ���������� ���������� ��������� ������������� ����� &#171;����������&#187; �������� � ������������ ����������������, �� ��������� �� 01 ������ 2006 ����. 
<P class=top align=left><STRONG>�������, ���, �������� ������������:</STRONG> ����������� �������� �������� ����� ������������� 
<P class=top align=left><STRONG>�������, ���, ��������, ��������� ����, ����������� ����������� ����������: </STRONG>����������� ������������ ��������� �������� �������� �������������, ���������������� �������� �� ����� ������������� ����������� ������������ � ������� ����������� ������ &#8470; � 009487, ����� ����� �� �� 28 ������ 2000 ����, � 19 ������� 2003 ���� ������� �� �������������� ���� 
<P class=top align=left><STRONG>���������
��� ����������� �����������: </STRONG>�������� ����������� �������� &#171;����� �����&#187; 
<P class=top align=left><STRONG>����� �������� �� ������������� ����������� ������������:</STRONG> � 001621 
<P class=top align=left><STRONG>���� �������� ��������:</STRONG> 5 ��� 
<P class=top align=left><STRONG>����� ������� � ������ ��������: </STRONG>������� &#8470; 200 
<P class=top align=left><STRONG>���� ������ ��������:</STRONG> 6 �������� 2002 ���� 
<P class=top align=left><STRONG>������������ ������, ��������� ��������:</STRONG> ������������ �������� ���������� ���������</P></TD></TR></TBODY></TABLE>


</body>
</html>

