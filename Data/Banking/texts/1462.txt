<!--
ID:35709597
TITLE:��� &#171;��������������� ����&#187;
SOURCE_ID:3105
SOURCE_NAME:���������� ���� � ������
AUTHOR:
NUMBER:11
ISSUE_DATE:2005-11-30
RECORD_DATE:2005-12-29


-->
<html>
<head>
<title>
35709597-��� &#171;��������������� ����&#187;
</title>
</head>
<body >


<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
<TBODY>
<TR class=bgwhite>
<TD></TD></TR>
<TR class=bodycolor>
<TD>
<P class=Rubrika align=center>&#171;��������������� ����&#187; (�������� ����������� ��������) 
<P class=&#171;Main&#187; align=center>��������������� ����� 3277, ���-��� 044525442 
<P class=&#171;Main&#187; align=center>�������� �����: 107045, �/� 47, ������, ���� ���., 2</P></TD></TR>
<TR class=bgwhite>
<TD align=middle>
<P class=Rubrika>������ �� 1 ������� 2005 ����</P>
<TABLE cellSpacing=0 cellPadding=0 width="98%" borderColorLight=#cccccc border=2>
<TBODY>
<TR>
<TD>
<P align=center><STRONG>����� �/�</STRONG></P></TD>
<TD>
<P align=center><STRONG>������������ ������</STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� �������� ����</STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� ��������������� �������� ���� �������� ����</STRONG></P></TD></TR>
<TR>
<TD width=63 height=0>
<P align=center><STRONG>1</STRONG></P></TD>
<TD width=322>
<P align=center><STRONG>2</STRONG></P></TD>
<TD width=92>
<P align=center><STRONG>3</STRONG></P></TD>
<TD width=110>
<P align=center><STRONG>4</STRONG></P></TD></TR>
<TR>
<TD colSpan=4 height=0>
<P align=center><B>������</B></P></TD></TR>
<TR>
<TD>
<P>1.</P></TD>
<TD>
<P>�������� ��������</P></TD>
<TD>
<P align=right>35209</P></TD>
<TD>
<P align=right>41604</P></TD></TR>
<TR>
<TD>
<P>2.</P></TD>
<TD>
<P>�������� ��������� ����������� � ����������� ����� ���������� ���������</P></TD>
<TD>
<P align=right>151694</P></TD>
<TD>
<P align=right>93348</P></TD></TR>
<TR>
<TD>
<P>2.1.</P></TD>
<TD>
<P>������������ �������</P></TD>
<TD>
<P align=right>21081</P></TD>
<TD>
<P align=right>24720</P></TD></TR>
<TR>
<TD>
<P>3.</P></TD>
<TD>
<P>�������� � ��������� ������������</P></TD>
<TD>
<P align=right>33978</P></TD>
<TD>
<P align=right>116759</P></TD></TR>
<TR>
<TD>
<P>4.</P></TD>
<TD>
<P>������ �������� � �������� ������ ������</P></TD>
<TD>
<P align=right>160868</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>5.</P></TD>
<TD>
<P>������ ������� �������������</P></TD>
<TD>
<P align=right>852834</P></TD>
<TD>
<P align=right>775701</P></TD></TR>
<TR>
<TD>
<P>6.</P></TD>
<TD>
<P>������ �������� � �������������� ������ ������, ������������ �� ���������</P></TD>
<TD>
<P align=right>62024</P></TD>
<TD>
<P align=right>62024</P></TD></TR>
<TR>
<TD>
<P>7.</P></TD>
<TD>
<P>������ �������� � ������ ������, ��������� � ������� ��� �������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>495</P></TD></TR>
<TR>
<TD>
<P>8.</P></TD>
<TD>
<P>�������� ��������, �������������� ������ � ������������ ������</P></TD>
<TD>
<P align=right>1985</P></TD>
<TD>
<P align=right>1892</P></TD></TR>
<TR>
<TD>
<P>9.</P></TD>
<TD>
<P>���������� �� ��������� ���������</P></TD>
<TD>
<P align=right>190</P></TD>
<TD>
<P align=right>289</P></TD></TR>
<TR>
<TD>
<P>10.</P></TD>
<TD>
<P>������ ������</P></TD>
<TD>
<P align=right>1506</P></TD>
<TD>
<P align=right>2604</P></TD></TR>
<TR>
<TD>
<P>11.</P></TD>
<TD>
<P>����� �������</P></TD>
<TD>
<P align=right>1300288</P></TD>
<TD>
<P align=right>1094716</P></TD></TR>
<TR align=middle>
<TD colSpan=4>
<P><B>II. �������</B></P></TD></TR>
<TR>
<TD>
<P>12.</P></TD>
<TD>
<P>������� ������������ ����� ���������� ���������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>13.</P></TD>
<TD>
<P>�������� ��������� �����������</P></TD>
<TD>
<P align=right>109000</P></TD>
<TD>
<P align=right>40000</P></TD></TR>
<TR>
<TD>
<P>14.</P></TD>
<TD>
<P>�������� �������� (����������� �����������)</P></TD>
<TD>
<P align=right>751773</P></TD>
<TD>
<P align=right>677823</P></TD></TR>
<TR>
<TD>
<P>14.1.</P></TD>
<TD>
<P>������ ���������� ���</P></TD>
<TD>
<P align=right>326154</P></TD>
<TD>
<P align=right>279041</P></TD></TR>
<TR>
<TD>
<P>15.</P></TD>
<TD>
<P>���������� �������� �������������</P></TD>
<TD>
<P align=right>212407</P></TD>
<TD>
<P align=right>152692</P></TD></TR>
<TR>
<TD>
<P>16.</P></TD>
<TD>
<P>������������� �� ������ ���������</P></TD>
<TD>
<P align=right>9882</P></TD>
<TD>
<P align=right>10604</P></TD></TR>
<TR>
<TD>
<P>17.</P></TD>
<TD>
<P>������ �������������</P></TD>
<TD>
<P align=right>2442</P></TD>
<TD>
<P align=right>1020</P></TD></TR>
<TR>
<TD>
<P>18.</P></TD>
<TD>
<P>������� �� ��������� ������ �� �������� �������������� ���������� ���������, ������ ��������� ������� � �� ��������� � ����������� �������� ���</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>19.</P></TD>
<TD>
<P>����� ������������</P></TD>
<TD>
<P align=right>1085504</P></TD>
<TD>
<P align=right>882139</P></TD></TR>
<TR align=middle>
<TD colSpan=4>
<P class=bold><STRONG>III. ��������� ����������� �������</STRONG></P></TD></TR>
<TR>
<TD>
<P>20.</P></TD>
<TD>
<P>�������� ���������� (����������)</P></TD>
<TD>
<P align=right>145092</P></TD>
<TD>
<P align=right>145092</P></TD></TR>
<TR>
<TD>
<P>20.1.</P></TD>
<TD>
<P>������������������ ������������ ����� � ����</P></TD>
<TD>
<P align=right>145092</P></TD>
<TD>
<P align=right>145092</P></TD></TR>
<TR>
<TD>
<P>20.2.</P></TD>
<TD>
<P>������������������ ����������������� �����</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>20.3.</P></TD>
<TD>
<P>�������������������� �������� ������� ������������� ��������� �����������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>21.</P></TD>
<TD>
<P>����������� �����, ����������� � ����������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>22.</P></TD>
<TD>
<P>����������� �����</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>23.</P></TD>
<TD>
<P>���������� �������� �������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>24.</P></TD>
<TD>
<P>������� ������� �������� � ����������� �������, �������� �� ����������� �������� (�������)</P></TD>
<TD>
<P align=right>10426</P></TD>
<TD>
<P align=right>12072</P></TD></TR>
<TR>
<TD>
<P>25.</P></TD>
<TD>
<P>����� � ���������������� ������� ������� ��� � ������������ ��������� ����������� (������������ ������ ������� ���)</P></TD>
<TD>
<P align=right>73458</P></TD>
<TD>
<P align=right>68558</P></TD></TR>
<TR>
<TD>
<P>26.</P></TD>
<TD height=0>
<P>������ (������) �� �������� ������</P></TD>
<TD>
<P align=right>6660</P></TD>
<TD>
<P align=right>10999</P></TD></TR>
<TR>
<TD>
<P>27.</P></TD>
<TD>
<P>����� ���������� ����������� �������</P></TD>
<TD>
<P align=right>214784</P></TD>
<TD>
<P align=right>212577</P></TD></TR>
<TR>
<TD>
<P>28.</P></TD>
<TD>
<P>����� ��������</P></TD>
<TD>
<P align=right>1300288</P></TD>
<TD>
<P align=right>1094716</P></TD></TR>
<TR align=middle>
<TD colSpan=4>
<P><B>IV. ������������� �������������</B></P></TD></TR>
<TR>
<TD>
<P>29.</P></TD>
<TD>
<P>����������� ������������� ��������� �����������</P></TD>
<TD>
<P align=right>99826</P></TD>
<TD>
<P align=right>55144</P></TD></TR>
<TR>
<TD>
<P>30.</P></TD>
<TD>
<P>��������, �������� ��������� ������������</P></TD>
<TD>
<P align=right>23208</P></TD>
<TD>
<P align=right>2080</P></TD></TR></TBODY></TABLE>
<P class=top align=justify>��������, ���������� ��������� �� ���� ������� ������� V &#171;����� �������������� ����������&#187;, �� �������������� 
<P class=Rubrika>����� � �������� � ������� �� 9 ������� 2005 ����.</P>
<TABLE cellSpacing=0 cellPadding=0 width="98%" borderColorLight=#cccccc border=2>
<TBODY>
<TR>
<TD>
<P align=center><STRONG>����� �/�</STRONG></P></TD>
<TD>
<P align=center><STRONG>������������ ������</STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� �������� ������ </STRONG></P></TD>
<TD>
<P align=center><STRONG>������ �� ��������������� ������ �������� ����</STRONG></P></TD></TR>
<TR>
<TD width=63 height=0>
<P align=center><STRONG>1</STRONG></P></TD>
<TD width=229>
<P align=center><STRONG>2</STRONG></P></TD>
<TD width=149>
<P align=center><STRONG>3</STRONG></P></TD>
<TD width=126>
<P align=center><STRONG>4</STRONG></P></TD></TR>
<TR>
<TD colSpan=4 height=0>
<P class=bold align=center><STRONG>�������� ���������� � ����������� ������ ��:</STRONG></P></TD></TR>
<TR>
<TD>
<P>1</P></TD>
<TD>
<P>���������� ������� � ��������� ������������</P></TD>
<TD>
<P align=right>4948</P></TD>
<TD>
<P align=right>3451</P></TD></TR>
<TR>
<TD>
<P>2</P></TD>
<TD>
<P>����, ��������������� �������� (����������� ������������)</P></TD>
<TD>
<P align=right>57407</P></TD>
<TD>
<P align=right>72101</P></TD></TR>
<TR>
<TD>
<P>3</P></TD>
<TD>
<P>�������� ����� �� ���������� ������ (�������)</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>4</P></TD>
<TD>
<P>������ ����� � ������������� �������</P></TD>
<TD>
<P align=right>10571</P></TD>
<TD>
<P align=right>2748</P></TD></TR>
<TR>
<TD>
<P>5</P></TD>
<TD>
<P>������ ����������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>6</P></TD>
<TD>
<P>����� ��������� ���������� � ����������� �������</P></TD>
<TD>
<P align=right>72926</P></TD>
<TD>
<P align=right>78300</P></TD></TR>
<TR vAlign=top align=middle>
<TD colSpan=4>
<P><B>�������� ���������� � ����������� ������� ��:</B></P></TD></TR>
<TR>
<TD>
<P>7</P></TD>
<TD>
<P>������������ ��������� ��������� �����������</P></TD>
<TD>
<P align=right>1440</P></TD>
<TD>
<P align=right>403</P></TD></TR>
<TR>
<TD>
<P>8</P></TD>
<TD>
<P>������������ ��������� �������� (����������� �����������)</P></TD>
<TD>
<P align=right>25323</P></TD>
<TD>
<P align=right>27575</P></TD></TR>
<TR>
<TD>
<P>9</P></TD>
<TD>
<P>���������� �������� ��������������</P></TD>
<TD>
<P align=right>10609</P></TD>
<TD>
<P align=right>19340</P></TD></TR>
<TR>
<TD>
<P>10</P></TD>
<TD>
<P>����� ��������� ���������� � ����������� ��������</P></TD>
<TD>
<P align=right>37372</P></TD>
<TD>
<P align=right>47318</P></TD></TR>
<TR>
<TD>
<P>11</P></TD>
<TD>
<P>������ ���������� � ����������� ������</P></TD>
<TD>
<P align=right>35554</P></TD>
<TD>
<P align=right>30982</P></TD></TR>
<TR>
<TD>
<P>12</P></TD>
<TD>
<P>������ ������ �� �������� � ������� ��������</P></TD>
<TD>
<P align=right>16760</P></TD>
<TD>
<P align=right>9555</P></TD></TR>
<TR>
<TD>
<P>13</P></TD>
<TD>
<P>������ ������ �� �������� � ����������� �������</P></TD>
<TD>
<P align=right>3809</P></TD>
<TD>
<P align=right>3857</P></TD></TR>
<TR>
<TD>
<P>14</P></TD>
<TD>
<P>������ ������ �� �������� � ������������ ��������� � ������� ����������� �������������</P></TD>
<TD>
<P align=right>0</P></TD>
<TD>
<P align=right>0</P></TD></TR>
<TR>
<TD>
<P>15</P></TD>
<TD>
<P>������ ������ �� ���������� ����������� ������</P></TD>
<TD>
<P align=right>&#8212;767</P></TD>
<TD>
<P align=right>727</P></TD></TR>
<TR>
<TD>
<P>16</P></TD>
<TD>
<P>������������ ������</P></TD>
<TD>
<P align=right>6899</P></TD>
<TD>
<P align=right>5925</P></TD></TR>
<TR>
<TD>
<P>17</P></TD>
<TD>
<P>������������ �������</P></TD>
<TD>
<P align=right>483</P></TD>
<TD>
<P align=right>613</P></TD></TR>
<TR>
<TD>
<P>18</P></TD>
<TD>
<P>������ ������ �� ������� ��������</P></TD>
<TD>
<P align=right>2</P></TD>
<TD>
<P align=right>&#8212;1</P></TD></TR>
<TR>
<TD>
<P>19</P></TD>
<TD>
<P>������ ������ ������������ ������</P></TD>
<TD>
<P align=right>&#8212;1370</P></TD>
<TD>
<P align=right>&#8212;182</P></TD></TR>
<TR>
<TD>
<P>20</P></TD>
<TD>
<P>���������������-�������������� �������</P></TD>
<TD>
<P align=right>46143</P></TD>
<TD>
<P align=right>41010</P></TD></TR>
<TR>
<TD>
<P>21</P></TD>
<TD>
<P>������� �� ��������� ������</P></TD>
<TD>
<P align=right>&#8212;5152</P></TD>
<TD>
<P align=right>4165</P></TD></TR>
<TR>
<TD>
<P>22</P></TD>
<TD>
<P>������� �� ���������������</P></TD>
<TD>
<P align=right>9109</P></TD>
<TD>
<P align=right>13405</P></TD></TR>
<TR>
<TD>
<P>23</P></TD>
<TD>
<P>����������� ������ (������� ����� �� �������)</P></TD>
<TD>
<P align=right>2449</P></TD>
<TD>
<P align=right>2406</P></TD></TR>
<TR>
<TD>
<P>24</P></TD>
<TD>
<P>������� (������) �� �������� ������</P></TD>
<TD>
<P align=right>6660</P></TD>
<TD>
<P align=right>10999</P></TD></TR></TBODY></TABLE>
<P class=Rubrika>���������� �� ������ ������������� ��������,<BR>�������� �������� �� �������� ������������ ����<BR>� ���� ������� �� 1 ������� 2005 ����</P>
<TABLE cellSpacing=0 cellPadding=0 width="95%" borderColorLight=#cccccc border=2>
<TBODY>
<TR vAlign=top align=middle>
<TD vAlign=center width=42 height=39>
<P align=center><B>&#8470; �/�</B></P></TD>
<TD vAlign=center width=0>
<P class=bold>������������ ����������</P></TD>
<TD width=88>
<P class=bold align=center><STRONG>������</STRONG> �� �������� ����</P></TD>
<TD width=88>
<P class=bold align=center><STRONG>������ �� ��������������� �������� ���� �������� ����</STRONG></P></TD></TR>
<TR>
<TD height=0>
<P align=center><STRONG>1</STRONG></P></TD>
<TD>
<P align=center><STRONG>2</STRONG></P></TD>
<TD>
<P align=center><STRONG>3</STRONG></P></TD>
<TD>
<DIV align=center>
<P><STRONG>4</STRONG></P></DIV></TD></TR>
<TR>
<TD>
<P align=center>1</P></TD>
<TD>
<P>����������� �������� (�������), ���. ���.</P></TD>
<TD>
<P align=right>214887.0</P></TD>
<TD>
<P align=right>212765.0</P></TD></TR>
<TR>
<TD>
<P align=center>2</P></TD>
<TD>
<P>����������� �������� ������������� ����������� ������� (��������), �������</P></TD>
<TD>
<P align=right>20.7</P></TD>
<TD>
<P align=right>25.5</P></TD></TR>
<TR>
<TD>
<P align=center>3</P></TD>
<TD>
<P>����������� �������� ������������� ����������� ������� (��������), �������</P></TD>
<TD>
<P align=right>10.0</P></TD>
<TD>
<P align=right>10.0</P></TD></TR>
<TR>
<TD>
<P align=center>4</P></TD>
<TD>
<P>��������� ������ �� ��������� ������ �� ������, ������� � ������������ � ��� �������������, ���. ���.</P></TD>
<TD>
<P align=right>7051.0</P></TD>
<TD>
<P align=right>4564.0</P></TD></TR>
<TR>
<TD>
<P align=center>5</P></TD>
<TD>
<P>���������� �������������� ������ �� ��������� ������ �� ������, ������� � ������������ � ��� �������������, ���. ���.</P></TD>
<TD>
<P align=right>7051.0</P></TD>
<TD>
<P align=right>4564.0</P></TD></TR>
<TR>
<TD>
<P align=center>6</P></TD>
<TD>
<P>��������� ������ �� ��������� ������, ���. ���.</P></TD>
<TD>
<P align=right>0.0</P></TD>
<TD>
<P align=right>0.0</P></TD></TR>
<TR>
<TD>
<P align=center>7</P></TD>
<TD>
<P>���������� �������������� ������ �� ��������� ������, ���. ���.</P></TD>
<TD>
<P align=right>1718.0</P></TD>
<TD>
<P align=right>447.0</P></TD></TR></TBODY></TABLE>
<TABLE cellSpacing=0 cellPadding=0 width="95%" border=0>
<TBODY>
<TR>
<TD height=0>
<P class=top>���. ������������ ��������� ��������� �����������</P></TD>
<TD>
<P align=right><STRONG>������� �.�.</STRONG></P></TD></TR>
<TR>
<TD height=0>
<P class=top>������� ��������� ��������� �����������</P></TD>
<TD>
<P align=right><STRONG>��������� �.�.</STRONG></P></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>


</body>
</html>

