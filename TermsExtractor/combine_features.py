#!/usr/bin/env python
from argparse import ArgumentParser
import os


def load_directory(directory_name):
  features = {}
  for filename in os.listdir(directory_name):
    features[filename] = {}
    with open(os.path.join(directory_name, filename), 'r') as file_input:
      for line in file_input:
        tokens = line.strip().split('\t')
        if len(tokens) == 3:
          features[filename][tokens[0]] = '\t'.join(tokens[1:])
  return features


def combine_features(first_directory, second_directory, output_directory):
  if not os.path.exists(output_directory):
    os.makedirs(output_directory)
  first_features = load_directory(first_directory)
  second_features = load_directory(second_directory)
  for feature in set(first_features).intersection(set(second_features)):
    with open(os.path.join(output_directory, feature), 'w') as file_output:
      result_values = first_features[feature]
      result_values.update(second_features[feature])
      for word, value in sorted(result_values.iteritems(), key=lambda x: float(x[1].split('\t')[0]), reverse=True):
        file_output.write(word + '\t' + value + '\n')


if __name__ == '__main__':
  parser = ArgumentParser(description='Features combiner')
  parser.add_argument('first_directory', help='First directory with features')
  parser.add_argument('second_directory', help='Second directory with features')
  parser.add_argument('output_directory', help='Output directory for combined features')
  args = parser.parse_args()
  combine_features(args.first_directory, args.second_directory, args.output_directory)