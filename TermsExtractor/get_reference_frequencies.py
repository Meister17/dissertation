#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser
from BeautifulSoup import BeautifulSoup
import inflect
import inflection
import os
import requests
import time
import urllib2

TIME_TO_SLEEP=2
TIMEOUT=50

def load_new_candidates(candidates_filename, reference_filename, candidates_number):
  candidates = set()
  with open(candidates_filename, 'r') as file_input:
    file_input.readline()
    for index, line in enumerate(file_input):
      if index >= candidates_number:
        break
      candidates.add(line.strip().split('\t')[0])
  if os.path.exists(reference_filename):
    processed_candidates = set()
    with open(reference_filename, 'r') as file_input:
      file_input.readline()
      for line in file_input:
        processed_candidates.add(line.strip().split('\t')[0])
    return candidates - processed_candidates
  else:
    dirname = os.path.dirname(reference_filename)
    if not os.path.exists(dirname):
      os.makedirs(dirname)
    return candidates


def load_proxies(filename):
  proxies = set()
  with open(filename, 'r') as file_input:
    for line in file_input:
      proxy_type, url, port = line.strip().split()
      proxies.add((proxy_type, proxy_type + '://' + url + ':' + port))
  return proxies


def download_russian_frequency(candidate, proxies, proxy):
  URL_BEGIN = ('http://search.ruscorpora.ru/search.xml?env=alpha&mycorp=&mysent=' +
             '&mysize=&mysentsize=&mydocsize=&spd=&text=lexgramm&mode=main&sort' +
             '=gr_tagging&lang=ru&nodia=1&parent1=0&level1=0&lex1=')
  URL_MIDDLE = ('&gramm1=&sem1=&sem-mod1=sem&sem-mod1=sem2&flags1=&m1=&parent2=0&' +
                'level2=0&min2=1&max2=1&lex2=')
  URL_END = '&gramm2=&sem2=&sem-mod2=sem&sem-mod2=sem2&flags2=&m2='
  soup = None
  while True:
    try:
      words = candidate.split()
      if len(words) == 1:
        url = URL_BEGIN + urllib2.quote(candidate) + URL_MIDDLE + URL_END
        if proxy is not None:
          response = requests.get(url, timeout=TIMEOUT, proxies={proxy[0]: proxy[1]})
        else:
          response = requests.get(url, timeout=TIMEOUT)
      elif len(words) == 2:
        url = URL_BEGIN + urllib2.quote(words[0]) + URL_MIDDLE + urllib2.quote(words[1]) + URL_END
        if proxy is not None:
          response = requests.get(url, timeout=TIMEOUT, proxies={proxy[0]: proxy[1]})
        else:
          response = requests.get(url, timeout=TIMEOUT)
      soup = BeautifulSoup(response.text)
      zero_result = False;
      for p in soup.findAll('p'):
        if u'ничего не найдено' in p.text:
          zero_result = True
          break
      if zero_result:
        return 0, 0, proxy
      spans = soup.findAll('span', {'class': 'stat-number'})
      return int(''.join(spans[3].text.split())), int(''.join(spans[4].text.split())), proxy
    except Exception, e:
      if soup is not None and u'Вы робот?' in soup.text:
        proxy = list(proxies)[0]
        proxies.remove(proxy)
      time.sleep(TIME_TO_SLEEP)


def print_header(filename, documents_number, words_number):
  if not os.path.exists(filename):
    with open(filename, 'w') as file_output:
      file_output.write(str(documents_number) + '\t' + str(words_number) + '\n')


def download_russian_frequencies(filename, candidates, proxies):
  print_header(filename, 85996, 229968798)
  proxy = None
  with open(filename, 'a') as file_output:
    for candidate in candidates:
      document_frequency, term_frequency, proxy = download_russian_frequency(candidate, proxies, proxy)
      file_output.write(candidate + '\t' + str(document_frequency) + '\t' + str(term_frequency) + '\n')
      file_output.flush()


def get_english_frequency(candidate):
  URL = 'http://bnc.bl.uk/saraWeb.php'
  NUMBER_FAILS = 5
  fails = 0
  while True:
    candidate = candidate.replace(' ', '+')
    try:
      response = urllib2.urlopen(URL + '?qy=' + candidate, timeout=TIMEOUT)
      html = response.read()
      soup = BeautifulSoup(html)
      div = soup.find('div', {'id': 'solutions'})
      if div is None:
        return 0
      answer = str(div.p)
      words = answer.split()
      if words[0] == '<p>No':
        return 0
      elif words[0] == '<p>Here':
        return int(words[10])
      elif words[1] == 'one':
        return 1
      else:
        return int(words[1])
    except Exception, e:
      fails += 1
      if fails >= NUMBER_FAILS:
        return 0
      time.sleep(TIME_TO_SLEEP)


def download_english_frequency(candidate):
  words = candidate.split(' ')
  frequency = 0
  candidates_to_check = set()
  candidates_to_check.add(candidate)
  inflect_engine = inflect.engine()
  if len(words) == 1:
    candidates_to_check.add(inflection.pluralize(candidate))
  elif len(words) == 2:
    second_plural_noun = inflection.pluralize(words[1])
    candidates_to_check.add(words[0] + ' ' + second_plural_noun)
    first_plural_noun = inflection.pluralize(words[0])
    candidates_to_check.add(first_plural_noun + ' ' + words[1])
    candidates_to_check.add(first_plural_noun + ' ' + second_plural_noun)
  elif len(words) == 3 and words[1] == 'of':
    first_plural_noun = inflection.pluralize(words[0])
    second_plural_noun = inflection.pluralize(words[2])
    second_indefinite_noun = inflect_engine.a(words[2])
    candidates_to_check.add(words[0] + ' of ' + second_plural_noun)
    candidates_to_check.add(words[0] + ' of ' + second_indefinite_noun)
    candidates_to_check.add(words[0] + ' of the ' + words[2])
    candidates_to_check.add(words[0] + ' of the ' + second_plural_noun)
    candidates_to_check.add(first_plural_noun + ' of ' + words[2])
    candidates_to_check.add(first_plural_noun + ' of ' + second_indefinite_noun)
    candidates_to_check.add(first_plural_noun + ' of the ' + words[2])
    candidates_to_check.add(first_plural_noun + ' of ' + second_plural_noun)
    candidates_to_check.add(first_plural_noun + ' of the ' + second_plural_noun)
  for words in candidates_to_check:
    frequency += get_english_frequency(words)
  return frequency


def download_english_frequencies(filename, candidates):
  print_header(filename, 100000000, 100000000)
  with open(filename, 'a') as file_output:
    for candidate in candidates:
      frequency = download_english_frequency(candidate)
      file_output.write(candidate + '\t' + str(frequency) + '\t' + str(frequency) + '\n')
      file_output.flush()


if __name__ == '__main__':
  parser = ArgumentParser(description='Reference frequencies downloader')
  parser.add_argument('candidates_file', help='File containing words/bigrams')
  parser.add_argument('reference_file',
                      help='File with downloaded reference frequencies')
  parser.add_argument('-l', '--language', choices=['russian', 'english'],
                      help='Language of the text corpus')
  parser.add_argument('-n', '--candidates_number', type=int, default=5000,
                      help='Number of candidates to get reference frequencies for')
  parser.add_argument('-p', '--proxies_list', default='proxies.list',
                      help='File containing free proxies')
  args = parser.parse_args()
  new_candidates = load_new_candidates(args.candidates_file, args.reference_file, args.candidates_number)
  print("Reference frequencies will be downloaded for " + str(len(new_candidates)) + " new candidates");
  if args.language == 'russian':
    proxies = load_proxies(args.proxies_list)
    download_russian_frequencies(args.reference_file, new_candidates, proxies)
  else:
    download_english_frequencies(args.reference_file, new_candidates)
