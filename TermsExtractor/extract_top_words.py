#!/usr/bin/env python
# -*- coding: utf-8
from argparse import ArgumentParser
import codecs
from collections import defaultdict
import math
import os


def load_stopwords(filename):
  stopwords = set()
  with open(filename, 'r') as file_input:
    for line in file_input:
      stopwords.add(line.strip())
  return stopwords


def parse_line(line, language):
  if language == 'english':
    parts = line.split()
    if len(parts) < 3:
      return None, None
    if parts[2] == 'NN' or parts[2] == 'NNS' or parts[2] == 'NNP' or parts[2] == 'NNPS':
      return parts[1], 'Noun'
    elif parts[2] == 'JJ':
      return parts[1], 'Adjective'
    elif parts[2] == 'IN' and parts[1] == 'of':
      return parts[1], 'of'
    elif parts[2] == 'DT':
      return '', ''
    else:
      return None, None
  else:
    parts = line.strip().split()
    if len(parts) <= 4:
      return None, None
    elif parts[4] == u'ЗПР' or parts[4] == u'РЗД' or parts[4] == u'КОД' or parts[4] == u'НЧТ':
      return '', ''
    elif parts[4] == u'ЛЕ':
      if parts[-1][0] in u'абвгдежзи':
        is_genitive = False
        for index in xrange(1, len(parts[-1]), 2):
          if parts[-1][index] in u'бз' or parts[-1][index] not in u'ажвигйдкел':
            is_genitive = True
        if is_genitive:
          return parts[-2], 'Genitive Noun'
        else:
          return parts[-2], 'Noun'
      elif parts[-1][0] == u'й':
        return parts[-2], 'Adjective'
      else:
        return None, None
    else:
      return None, None


def count_tfidf(term_frequencies, document_frequencies, documents_number):
  print('Counting TF-IDF')
  tfidf_values = {}
  for word, term_frequency in term_frequencies.items():
    tfidf_values[word] = term_frequency #* math.log(documents_number / document_frequencies[word])
  return tfidf_values


def extract_top_words(input_directory, stopwords_filename, output_filename, language, top_words_number):
  print('Processing input directory')
  stopwords = load_stopwords(stopwords_filename)
  encoding = 'utf-8' if language == 'english' else 'cp1251'
  word_frequencies = defaultdict(int)
  bigram_frequencies = defaultdict(int)
  for filename in os.listdir(input_directory):
    with codecs.open(os.path.join(input_directory, filename), 'r', encoding) as file_input:
      previous_word_data = ('', '')
      for line in file_input:
        if line[0].isspace():
          continue
        word, pos = parse_line(line.strip(), language)
        if word is None:
          previous_word_data = ('', '')
        elif len(word) > 2 and word not in stopwords:
          if (pos == 'Noun' or (language == 'russian' and
                                (pos in ['Adjective', 'Genitive Noun']))):
            word_frequencies[word] += 1
          if (len(previous_word_data[0]) == 0 and (pos == 'Adjective' or pos == 'Noun' or
                                                   pos == 'Genitive Noun')):
            previous_word_data = (word, pos)
          elif len(previous_word_data[0]) > 0:
            if pos == 'Adjective' or (language == 'russian' and pos == 'Noun'):
              previous_word_data = (word, pos)
            elif pos == 'of':
              if previous_word_data[1] == 'Noun':
                previous_word_data = (previous_word_data[0] + ' of', 'of')
              else:
                previous_word_data = ('', '')
            elif pos == 'Noun' or pos == 'Genitive Noun':
              bigram = previous_word_data[0] + ' ' + word
              bigram_frequencies[bigram] += 1
              previous_word_data = (word, pos)
            else:
              raise Exception('Wrong data found')
          else:
            previous_word_data = ('', '')
  with codecs.open(output_filename, 'w', 'utf-8') as file_output:
    word_values = sorted(word_frequencies.iteritems(),
                         key=lambda x: x[1], reverse=True)[0:top_words_number]
    bigram_values = sorted(bigram_frequencies.iteritems(),
                           key=lambda x: x[1], reverse=True)[0:top_words_number]
    for word, value in word_values:
      file_output.write(word + '\n')
    for bigram, value in bigram_values:
      file_output.write(bigram + '\n')


if __name__ == '__main__':
  parser = ArgumentParser(
      description='Extract top words and bigrams via TF as predefined ones')
  parser.add_argument('input_directory', help='Directory with text collection')
  parser.add_argument('stopwords_file', help='File with stopwords')
  parser.add_argument('output_file',
                      help='File where top words and bigrams will be written')
  parser.add_argument('-l', '--language', choices=['russian', 'english'],
                      help='Language of the text collection')
  parser.add_argument('-n', '--top_words_number', type=int, default=10,
                      help='Number of top words to extract')
  args = parser.parse_args()
  extract_top_words(args.input_directory, args.stopwords_file, args.output_file,
                    args.language, args.top_words_number)
