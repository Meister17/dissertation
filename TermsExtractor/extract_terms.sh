#!/usr/bin/env bash
set -x -e
if [ "$#" -ne 6 ]; then
  echo 'Usage: '$0' <input_directory> <output_directory> <language> <thesaurus_file>'\
       '<terms_number> <unigrams|bigrams|combined>'
  exit 1
fi

if [ ! -d $1 ]; then
  echo 'Wrong input directory'
  exit 1
fi

mkdir -p $2
base_dir=`realpath $0`
base_dir=`dirname $base_dir`

if [ $3 != 'russian' ] && [ $3 != 'english' ]; then
  echo 'Wrong language of the text corpus'
  exit 1
fi

stopwords_file=${base_dir}/../MorphologicalAnalyzer/$3/stopwords.txt
prepositions_file=${base_dir}/../MorphologicalAnalyzer/$3/prepositions.txt

if [ ! -f $4 ]; then
  echo 'Wrong thesaurus file'
  exit 1
fi

positive_number="^[0-9]+$"
if ! [[ $5 =~ $positive_number ]]; then
  echo 'Wrong number of term candidates'
  exit 1
fi

if [ $6 != 'unigrams' ] && [ $6 != 'bigrams' ] && [ $6 != 'combined' ]; then
  echo 'Wrong mode specified'
  exit 1
fi

./extract_top_words.py $1 ${stopwords_file} $2/predefined_terms.txt -l $3 -n 10

TermsExtractor/terms_extractor --language $3 --source_directory_name=$1 \
--single_words_filename=$2/single_words.txt \
--single_word_terms_filename=$2/single_word_terms.txt \
--single_word_terms_topic_input_filename=$2/single_word_terms_topic_input.txt \
--single_word_terms_topic_vocabulary_filename=$2/single_word_terms_topic_vocabulary.txt \
--two_word_terms_filename=$2/two_word_terms.txt \
--two_word_terms_topic_input_filename=$2/two_word_terms_topic_input.txt \
--two_word_terms_topic_vocabulary_filename=$2/two_word_terms_topic_vocabulary.txt \
--single_word_phrases_filename=$2/single_word_phrases.txt \
--two_word_phrases_filename=$2/two_word_phrases.txt \
--minimum_term_frequency=5 --predefined_words_filename=$2/predefined_terms.txt \
--stopwords_filename=${stopwords_file} --prepositions_filename=${prepositions_file}

./get_reference_frequencies.py $2/single_word_terms.txt $2/reference_single_word.txt -l $3 -n $5 -p proxies.list &
./get_reference_frequencies.py $2/two_word_terms.txt $2/reference_two_word.txt -l $3 -n $5 -p proxies.list &

../TopicModels/TopicModels/topic_model \
--input_filename=$2/single_word_terms_topic_input.txt \
--vocabulary_filename=$2/single_word_terms_topic_vocabulary.txt \
--topics_number=100 --alpha=0 --beta=0 --maximum_iterations_number=1000 --evaluation_measure=NO \
--topic_words_filename=$2/single_word_plsa.txt --logging_directory=$2/topic_logs &
../TopicModels/TopicModels/topic_model \
--input_filename=$2/two_word_terms_topic_input.txt \
--vocabulary_filename=$2/two_word_terms_topic_vocabulary.txt \
--topics_number=100 --alpha=0 --beta=0 --maximum_iterations_number=1000 --evaluation_measure=NO \
--topic_words_filename=$2/two_word_plsa.txt --logging_directory=$2/topic_logs &
wait

FeaturesCalculator/features_calculator \
--average_precision_directory_name=$2/average_precisions \
--destination_directory_name=$2 --real_terms_filename=$4 \
--single_words_filename=$2/single_words.txt \
--single_word_terms_filename=$2/single_word_terms.txt \
--single_word_topic_filename=$2/single_word_plsa.txt \
--single_word_phrases_filename=$2/single_word_phrases.txt \
--reference_single_words_filename=$2/reference_single_word.txt \
--two_word_terms_filename=$2/two_word_terms.txt \
--two_word_topic_filename=$2/two_word_plsa.txt \
--two_word_phrases_filename=$2/two_word_phrases.txt \
--reference_two_words_filename=$2/reference_two_word.txt --terms_number_threshold=$5

if [ $6 == 'unigrams' ]; then
  MachineLearning/machine_learning.py $2/single_word $2/terms_results.txt
elif [ $6 == 'bigrams' ]; then
  MachineLearning/machine_learning.py $2/two_word $2/terms_results.txt
else
  ./combine_features.py $2/single_word $2/two_word $2/combined
  MachineLearning/machine_learning.py $2/combined $2/terms_results.txt
fi

echo 'Done. See results in '$2'/terms_results.txt'
