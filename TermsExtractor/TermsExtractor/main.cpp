// Copyright 2013 Michael Nokel
#include "./program_options_parser.h"
#include "./terms_extractor.h"
#include <iostream>
#include <stdexcept>

using std::cout;
using std::exception;

/**
  Main function which starts the program
  @param argc number of command line options
  @param argv array containing command line options
  @return exit code of the program
*/
int main(int argc, char** argv) {
  try {
    ProgramOptionsParser program_options_parser;
    program_options_parser.Parse(argc, argv);
    if (program_options_parser.help_message_printed()) {
      return 0;
    }
    TermsExtractor terms_extractor(
        program_options_parser.language(),
        program_options_parser.minimum_term_frequency());
    terms_extractor.Initialize(
        program_options_parser.predefined_words_filename(),
        program_options_parser.prepositions_filename(),
        program_options_parser.stopwords_filename());
    terms_extractor.ExtractTerms(
        program_options_parser.source_directory_name());
    terms_extractor.PrintTermCandidates(
        program_options_parser.single_words_filename(),
        program_options_parser.single_word_terms_filename(),
        program_options_parser.single_word_terms_topic_input_filename(),
        program_options_parser.single_word_terms_topic_vocabulary_filename(),
        program_options_parser.single_word_phrases_filename(),
        program_options_parser.two_word_terms_filename(),
        program_options_parser.two_word_terms_topic_input_filename(),
        program_options_parser.two_word_terms_topic_vocabulary_filename(),
        program_options_parser.two_word_phrases_filename());
  } catch (const exception& except) {
    cout << "Fatal error occurred: " << except.what() << "\n";
    return 1;
  }
  return 0;
}
