// Copyright 2013 Michael Nokel
#pragma once

#include "./auxiliary.h"
#include <string>

/**
  @brief Class for parsing and working with program options obtained from
  command line and/or from configuration file

  This class should be used for parsing program options from command line and/or
  from configuration file, checking their correctness, after what one can access
  any of the presented program options.
*/
class ProgramOptionsParser {
 public:
  /**
    Parses program options from command line and/or from configuration file
    @param argc number of program options (from main function)
    @param argv array containing program options (from main function)
    @throw std::runtime_error in case of any occurred errors
  */
  void Parse(int argc, char** argv);

  bool help_message_printed() const {
    return help_message_printed_;
  }

  Language language() const {
    return language_;
  }

  unsigned int minimum_term_frequency() const {
    return minimum_term_frequency_;
  }

  std::string prepositions_filename() const {
    return prepositions_filename_;
  }

  std::string predefined_words_filename() const {
    return predefined_words_filename_;
  }

  std::string stopwords_filename() const {
    return stopwords_filename_;
  }

  std::string source_directory_name() const {
    return source_directory_name_;
  }

  std::string single_words_filename() const {
    return single_words_filename_;
  }

  std::string single_word_terms_filename() const {
    return single_word_terms_filename_;
  }

  std::string single_word_terms_topic_input_filename() const {
    return single_word_terms_topic_input_filename_;
  }

  std::string single_word_terms_topic_vocabulary_filename() const {
    return single_word_terms_topic_vocabulary_filename_;
  }

  std::string two_word_terms_filename() const {
    return two_word_terms_filename_;
  }

  std::string two_word_terms_topic_input_filename() const {
    return two_word_terms_topic_input_filename_;
  }

  std::string two_word_terms_topic_vocabulary_filename() const {
    return two_word_terms_topic_vocabulary_filename_;
  }

  std::string single_word_phrases_filename() const {
    return single_word_phrases_filename_;
  }

  std::string two_word_phrases_filename() const {
    return two_word_phrases_filename_;
  }

 private:
  /** Flag indicating whether help message was printed */
  bool help_message_printed_ = false;

  /** Language of the text corpus */
  Language language_ = Language::NO_LANGUAGE;

  /** Minimum frequency to be taken into account */
  unsigned int minimum_term_frequency_ = 0;

  /** File containing predefined words */
  std::string predefined_words_filename_ = "";

  /* File containing prepositions for different cases */
  std::string prepositions_filename_ = "";

  /** File containing stop words */
  std::string stopwords_filename_ = "";

  /** Source directory for processing */
  std::string source_directory_name_ = "";

  /** File for printing single words and their frequencies */
  std::string single_words_filename_ = "";

  /** File for printing extracted single-word term candidates and some data
      about them */
  std::string single_word_terms_filename_ = "";

  /** File for printing input for topic models for single-word term candidates
      */
  std::string single_word_terms_topic_input_filename_ = "";

  /** File for printing vocabulary for topic models for single-word term
      candidates */
  std::string single_word_terms_topic_vocabulary_filename_ = "";

  /** File for printing extracted two-word term candidates and some data about
      them */
  std::string two_word_terms_filename_ = "";

  /** File for printing input for topic models for two-word term candidates */
  std::string two_word_terms_topic_input_filename_ = "";

  /** File for printing vocabulary for topic models for two-word term
      candidates */
  std::string two_word_terms_topic_vocabulary_filename_ = "";

  /** File for printing extracted noun phrases for single-word term candidates
      */
  std::string single_word_phrases_filename_ = "";

  /** File for printing extracted noun phrases for two-word term candidates */
  std::string two_word_phrases_filename_ = "";

  /**
    Parses language from its string representation
    @param language_string string representation of the language
    @return parsed language
    @throw std::runtime_error in case of wrong language
  */
  Language ParseLanguageString(const std::string& languiage_string) const;
};
