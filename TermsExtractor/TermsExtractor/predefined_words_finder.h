// Copyright 2013 Michael Nokel
#pragma once

#include <set>
#include <string>

/**
  @brief Class intended to be used for finding predefined words in texts

  This class parses file with predefined words. AFter what is should be used for
  checking whether the given word is among such ones.
*/
class PredefinedWordsFinder {
 public:
  /**
    Parses the file containing predefined terms
    @param filename file containing predefined words
    @throw std::runtime_error in case of any occurred error
  */
  void ParseFile(const std::string& filename);

  /**
    Checks whether the given word is among predefined ones or not
    @param word word to check
    @return true if the given word is a predefined term
  */
  bool IsPredefined(const std::string& word) const {
    return predefined_words_set_.find(word) != predefined_words_set_.end();
  }

 private:
  /** Set containing predefined words */
  std::set<std::string> predefined_words_set_;
};
