// Copyright 2013 Michael Nokel
#pragma once

#include <boost/algorithm/string.hpp>
#include <set>
#include <string>
#include <vector>

/**
  @brief Enumerator containing all possible languages of text corpus

  This enumerator contains Russian and English languages (while NO_LANGUAGE
  means an error)
*/
enum class Language {
  NO_LANGUAGE,
  RUSSIAN,
  ENGLISH
};

/**
  @brief All cases in Russian grammar.

  This enumeration contains nominative, genitive, dative, accusative,
  instrumental and prepositional cases (also no case is possible)
*/
enum class RussianWordCase {
  NO_CASE,
  NOMINATIVE,
  GENITIVE,
  DATIVE,
  ACCUSATIVE,
  INSTRUMENTAL,
  PREPOSITIONAL
};

/**
  @brief All genders in Russian grammar.

  This enumeration contains masculine, feminine and neuter genders (also no
  gender and plural are possible)
*/
enum class RussianWordGender {
  NO_GENDER,
  MASCULINE,
  FEMININE,
  NEUTER,
  PLURAL
};

/**
  @brief Enumerator containing all possible word's categories

  This enumerator contains no word's category, noun, adjective, proper noun,
  agreement word, preposition, determiner, dividers, verb and marker of end of
  sentence and other categories. Each language processor should use only
  appropriate ones
*/
enum class WordCategory {
  NO_WORD_CATEGORY,
  NOUN,
  ADJECTIVE,
  PROPER_NOUN,
  AGREEMENT_WORD,
  PREPOSITION,
  DETERMINER,
  DIVIDER,
  VERB,
  END_OF_SENTENCE,
  OTHER_CATEGORY
};

/**
  @brief Structure containing changeable parts of Russian words

  This structure contains word's case and word's gender
*/
struct RussianChangeableWordParts {
  RussianChangeableWordParts(RussianWordCase extracted_word_case,
                             RussianWordGender extracted_word_gender)
      : word_case(extracted_word_case),
        word_gender(extracted_word_gender) {
  }

  friend bool operator<(const RussianChangeableWordParts& first_word_parts,
                        const RussianChangeableWordParts& second_word_parts) {
    return first_word_parts.word_case < second_word_parts.word_case &&
           first_word_parts.word_gender < second_word_parts.word_gender;
  }

  /** Case of word */
  RussianWordCase word_case = RussianWordCase::NO_CASE;

  /** Gender of word */
  RussianWordGender word_gender = RussianWordGender::NO_GENDER;
};

/**
  @brief Enumerator containing all supported terms' categories

  This enumerator contains no word's category, adjective, noun and both
*/
enum class TermCategory {
  NO_TERM_CATEGORY,
  ADJECTIVE,
  NOUN,
  ADJECTIVE_AND_NOUN
};

/**
  @brief Structure containing data necessary for processing words extracted from
  files

  This structure contains information about whether end of sentence, punctuation
  divider, preposition was met, and whether word and token was extracted
*/
struct WordProcessingData {
  /** Flag indicating whether the end of sentence was met */
  bool was_end_of_sentence = true;

  /** Flag indicating whether the punctuation divider was met */
  bool was_punctuation_divider = false;

  /** Flag indicating whether the preposition was met */
  bool was_preposition = false;

  /** Flag indicating whether the word was extracted */
  bool word_extracted = false;

   /** Flag indicating whether the candidates was extracted */
  bool candidate_extracted = false;

  /** Flag indicating whether the token was extracted */
  bool token_extracted = false;
};

/**
  @brief Base structure containing all common data for all term candidates

  This structure contains flags indicating whether word is novel or not, whether
  it is ambiguous or not, first occurrence position, term frequency (as term,
  as capital word, as non-initial word, as subject), document frequency (as
  term, as capital word, as non-initial word, as subject), domain consensus (as
  term, as capital word, as non-initial word, as subject) and terms'
  probabilities
*/
struct TermCandidateData {
  /**
    Initializes structure
    @param word_category category of the determining word
    @param first_occurrence position of the first occurrence
    @param is_capital_word flag indicating whether the word is capital
    @param is_non_initial_word flag indicating whether the word is non-initial
    @param is_novel_word flag indicating whether the word is novel
    @param is_ambiguous_word flag indicating whether the word is ambiguous
    @param is_subject flag indicating whether term candidate is subject
  */
  TermCandidateData(const WordCategory& word_category,
                    unsigned int first_occurrence,
                    bool is_capital_word,
                    bool is_non_initial_word,
                    bool is_novel_word,
                    bool is_ambiguous_word,
                    bool is_subject)
      : is_novel_candidate(is_novel_word),
        is_ambiguous_candidate(is_ambiguous_word),
        first_occurrence_position(first_occurrence) {
    if (is_capital_word) {
      ++term_frequency_as_capital_word;
      ++document_frequency_as_capital_word;
    }
    if (is_non_initial_word) {
      ++term_frequency_as_non_initial_word;
      ++document_frequency_as_non_initial_word;
    }
    if (is_subject) {
      ++term_frequency_as_subject;
      ++document_frequency_as_subject;
    }
    if (word_category == WordCategory::NOUN) {
      term_category = TermCategory::NOUN;
    } else if (word_category == WordCategory::ADJECTIVE) {
      term_category = TermCategory::ADJECTIVE;
    }
  }

  /** Category of the term candidate */
  TermCategory term_category = TermCategory::NO_TERM_CATEGORY;

  /** Flag indicating whether candidate is novel or not */
  bool is_novel_candidate = false;

  /** Flag indicating whether candidate is ambiguous or not */
  bool is_ambiguous_candidate = false;

  /** Position of first occurrence of the term candidate */
  unsigned int first_occurrence_position = 0;

  /** NearTermsFreq value */
  unsigned int near_terms_frequency = 0;

  /** Term frequency of the term candidate */
  unsigned int term_frequency = 1;

  /** Term frequency of the term candidate (as capital word) */
  unsigned int term_frequency_as_capital_word = 0;

  /** Term frequency of the term candidate (as non-initial word) */
  unsigned int term_frequency_as_non_initial_word = 0;

  /** Term frequency of the term candidate (as subject) */
  unsigned int term_frequency_as_subject = 0;

  /** Document frequency of the term candidate */
  unsigned int document_frequency = 1;

  /** Document frequency of the term candidate (as capital word) */
  unsigned int document_frequency_as_capital_word = 0;

  /** Document frequency of the term candidate (as non-initial word) */
  unsigned int document_frequency_as_non_initial_word = 0;

  /** Document frequency of the term candidate (as subject) */
  unsigned int document_frequency_as_subject = 0;

  /** Domain consensus of the term candidate */
  double domain_consensus = 0.0;

  /** Domain consensus of the term candidate (as capital word) */
  double domain_consensus_as_capital_word = 0.0;

  /** Domain consensus of the term candidate (as non-initial word) */
  double domain_consensus_as_non_initial_word = 0.0;

  /** Domain consensus of the term candidate (as subject) */
  double domain_consensus_as_subject = 0.0;

  /** Vector containing term candidates' frequencies */
  std::vector<unsigned int> term_candidate_frequencies;

  /** Vector containing sizes of documents where term candidate occurs */
  std::vector<unsigned int> document_sizes;
};

/**
  @brief Structure containing probabilistic data for term candidates

  This structure contains probabilistic term frequency, maximum term frequency,
  term score and maximum term score, term contribution, term variation, term
  variation quality and term frequency as is in BM-25
*/
struct ProbabilisticData {
  /** Probabilistic term frequency */
  double term_frequency = 0.0;

  /** Maximum probabilistic term frequency */
  double maximum_term_frequency = 0.0;

  /** Probabilistic term score */
  double term_score = 0.0;

  /** Maximum probabilistic term score */
  double maximum_term_score = 0.0;

  /** Term Contribution weight */
  double term_contribution = 0.0;

  /** Term Variance */
  double term_variance = 0.0;

  /** Term Variance Quality */
  double term_variance_quality = 0.0;
};

/**
  @brief Base structure containing properties of parsed word

  This structure contains extracted word, its number in file and flags
  indicating whether it is a capital word or not, whether it is a
  non-initial word or not, whether is is going right after punctuation divider
  or not, and whether word is novel or not
*/
struct WordProperties {
  /** Extracted word */
  std::string word = "";

  /** Word's category */
  WordCategory word_category = WordCategory::NO_WORD_CATEGORY;

  /** Number of token in parsing file */
  unsigned int token_number_in_file = 0;

  /** Number of word in parsing file */
  unsigned int word_number_in_file = 0;

  /** Flag indicating whether word is capital */
  bool is_capital_word = false;

  /** Flag indicating whether word is non-initial */
  bool is_non_initial_word = false;

  /** Flag indicating whether word is going right after punctuation divider */
  bool is_after_punctuation_divider = false;

  /** Flag indicating whether word is novel or not */
  bool is_novel_word = false;
};

/**
  @brief Structure containing properties of parsed Russian word

  This structure extends base class by specifying set of changeable word parts
  and flag indicating whether word is auxiliary or not
*/
struct RussianWordProperties : public WordProperties {
  /** Information about changeable word parts */
  std::set<RussianChangeableWordParts> changeable_word_parts;

  /** Flag indicating whether word is auxiliary */
  bool is_auxiliary_word = false;
};

/**
  @brief Structure containing properties of parsed English word

  This structure extends base class by specifying flag indicating whether word
  is going right after preposition, and flag indicating whether word is
  ambiguous or not
*/
struct EnglishWordProperties : public WordProperties {
  /** Flag indicating whether word is going right after preposition */
  bool is_after_preposition = false;

  /** Flag indicating whether word is ambiguous or not */
  bool is_ambiguous_word = false;
};

/**
  @brief Structure containing necessary data for calculating NearTermsFreq
  feature

  This structure contains term candidate and its frequency counted in the
  context window
*/
struct NearTermsFrequency {
  /** Term candidate */
  std::string term_candidate = "";

  /** NearTermsFreq value of the term candidate */
  unsigned int near_terms_frequency = 0;
};

/**
  @brief Structure containing context data about words and bigrams

  This structure contains word, its number in file.
*/
struct ContextData {
  ContextData(const std::string& extracted_word,
              unsigned int token_number)
      : word(extracted_word),
        token_number_in_file(token_number) {
  }

  /** Extracted word */
  std::string word;

  /** Token's number in file */
  unsigned int token_number_in_file;
};