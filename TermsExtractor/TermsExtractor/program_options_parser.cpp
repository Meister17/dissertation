// Copyright 2013 Michael Nokel
#include "./auxiliary.h"
#include "./program_options_parser.h"
#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>
#include <iostream>
#include <stdexcept>
#include <string>

using namespace boost::program_options;
using boost::to_lower;
using std::cout;
using std::runtime_error;
using std::string;

void ProgramOptionsParser::Parse(int argc, char** argv) {
  variables_map program_options_storage;
  string configuration_filename;
  string language_string;
  options_description program_options_description("Options for program:");
  program_options_description.add_options()
      ("help,h", value<bool>(&help_message_printed_)->implicit_value(true),
          "Print help message and exit")
      ("configuration_filename,c", value<string>(&configuration_filename),
          "Configuration file")
      ("language", value<string>(&language_string), "Language of the corpus")
      ("minimum_term_frequency",
          value<unsigned int>(&minimum_term_frequency_),
          "Minimum term frequency to be taken into account")
      ("predefined_words_filename",
          value<string>(&predefined_words_filename_),
          "File containing predefined words")
      ("prepositions_filename",
          value<string>(&prepositions_filename_),
          "File containing prepositions for different cases")
      ("stopwords_filename", value<string>(&stopwords_filename_),
          "File containing stop words")
      ("source_directory_name", value<string>(&source_directory_name_),
          "Source directory for processing")
      ("single_words_filename", value<string>(&single_words_filename_),
          "File for printing single words and their frequencies")
      ("single_word_terms_filename",
          value<string>(&single_word_terms_filename_),
          "File for printing extracted single-word term candidates")
      ("single_word_terms_topic_input_filename",
          value<string>(&single_word_terms_topic_input_filename_),
          "File for printing input for topic models for single-word candidates")
      ("single_word_terms_topic_vocabulary_filename",
          value<string>(&single_word_terms_topic_vocabulary_filename_),
          "File for printing vocabulary for topic models for single-word terms")
      ("two_word_terms_filename", value<string>(&two_word_terms_filename_),
          "File for printing extracted two-word term candidates")
      ("two_word_terms_topic_input_filename",
          value<string>(&two_word_terms_topic_input_filename_),
          "File for printing input for topic models for two-word candidates")
      ("two_word_terms_topic_vocabulary_filename",
          value<string>(&two_word_terms_topic_vocabulary_filename_),
          "File for printing vocabulary for topic models for two-word terms")
      ("single_word_phrases_filename",
          value<string>(&single_word_phrases_filename_),
          "File for printing noun phrases for single-word term candidates")
      ("two_word_phrases_filename",
          value<string>(&two_word_phrases_filename_),
          "File for printing noun phrases for two-word term candidates");
  store(parse_command_line(argc, argv, program_options_description),
        program_options_storage);
  notify(program_options_storage);
  if (!configuration_filename.empty()) {
    store(parse_config_file<char>(configuration_filename.data(),
                                  program_options_description),
          program_options_storage);
    notify(program_options_storage);
  }
  if (help_message_printed_) {
    cout << program_options_description << "\n";
  } else {
    language_ = ParseLanguageString(language_string);
  }
}

Language ProgramOptionsParser::ParseLanguageString(
    const string& language_string) const {
  string lower_language_string = language_string;
  to_lower(lower_language_string);
  if (lower_language_string == "russian") {
    return Language::RUSSIAN;
  }
  if (lower_language_string == "english") {
    return Language::ENGLISH;
  }
  throw runtime_error("Wrong language of the text corpus was specified");
}
