// Copyright 2011 Michael Nokel
#pragma once

#include "./auxiliary.h"
#include <map>
#include <set>
#include <string>
#include <vector>

/**
  @brief Class for processing prepositions extracted from files

  This class firstly parses file containing prepositions and their cases. Then
  it should be used for getting cases for extracted from file prepositions.
*/
class PrepositionsProcessor {
 public:
  /**
    Parses file containing prepositions for different cases
    @param filename file containing prepositions for different cases
    @throw std::runtime_error in case of any error
  */
  void ParseFile(const std::string& filename);

  /**
    Gets and store cases for extracted preposition
    @param preposition extracted preposition
    @param[out] new_word_parts set containing cases for extracted prepositions
    @see ChangeableWordParts
  */
  void StoreCases(const std::string& preposition,
                  std::set<RussianChangeableWordParts>* new_word_parts) const {
    auto iterator = prepositions_map_.find(preposition);
    if (iterator != prepositions_map_.end()) {
      for (const auto& element: iterator->second) {
        RussianChangeableWordParts word_parts(element,
                                              RussianWordGender::NO_GENDER);
        new_word_parts->insert(word_parts);
      }
    }
  }

 private:
  /**
    Gets case from its string representation
    @param case_name name of case in string representation
    @return decoded case
  */
  RussianWordCase GetCaseFromString(const std::string& case_name) const;

  /** Map containing prepositions for different cases */
  std::map<std::string, std::vector<RussianWordCase> > prepositions_map_;
};
