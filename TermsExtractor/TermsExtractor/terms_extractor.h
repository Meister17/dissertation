// Copyright 2013 Michael Nokel
#pragma once

#include "./auxiliary.h"
#include "./file_parsers/file_parser.h"
#include "./predefined_words_finder.h"
#include "./prepositions_processor.h"
#include <memory>
#include <string>

/**
  @brief Main class for extracting term candidates and some data about them

  This class should be used for extracting term candidates and some data about
  them
*/
class TermsExtractor {
 public:
  /**
    Initializes object
    @param language language of the text corpus
    @param minimum_term_frequency minimum term frequency to be taken into
    account
  */
  TermsExtractor(const Language& language,
                 unsigned int minimum_term_frequency)
      : kLanguage_(language),
        kMinimumTermFrequency_(minimum_term_frequency) {
  }

  /**
    Initializes object by parsing files with predefined terms, prepositions and
    stopwords
    @param predefined_terms_filename file with predefined terms
    @param prepositions_filename file with prepositions for different cases
    @param stopwords_filename file with stop words
    @throw std::runtime_error in case of any occurred error
  */
  void Initialize(const std::string& predefined_terms_filename,
                  const std::string& prepositions_filename,
                  const std::string& stopwords_filename);

  /**
    Extracts term candidates and some data about them
    @param source_directory_name source directory for processing
    @throw std::runtime_error in case of any occurred errors
  */
  void ExtractTerms(const std::string& source_directory_name);

  /**
    Prints all extracted term candidates in the given files
    @param single_words_filename file for printing extracted single words with
    their frequencies
    @param single_word_terms_filename file for printing extracted single-word
    term candidates and some data about them
    @param single_word_terms_topic_input_filename file for printing input for
    topic models for single-word term candidates
    @param single_word_terms_topic_vocabulary_filename file for printing
    vocabulary for topic models for single-word term candidates
    @param single_word_phrases_filename file for printing extracted noun phrases
    for single-word term candidates
    @param two_word_terms_filename file for printing extracted two-word term
    candidates and some data about them
    @param two_word_terms_topic_input_filename file for printing input for topic
    models for two-word term candidates
    @param two_word_terms_topic_vocabulary_filename file for printing vocabulary
    for topic models for two-word term candidates
    @param two_word_phrases_filename file for printing extracted noun phrases
    for two-word term candidates
    @throw std::runtime_error in case of any occurred errors
  */
  void PrintTermCandidates(
      const std::string& single_words_filename,
      const std::string& single_word_terms_filename,
      const std::string& single_word_terms_topic_input_filename,
      const std::string& single_word_terms_topic_vocabulary_filename,
      const std::string& single_word_phrases_filename,
      const std::string& two_word_terms_filename,
      const std::string& two_word_terms_topic_input_filename,
      const std::string& two_word_terms_topic_vocabulary_filename,
      const std::string& two_word_phrases_filename);

 private:
  /** Language of the text corpus */
  const Language kLanguage_;

  /** Minimum frequency of term candidates to take into account */
  const unsigned int kMinimumTermFrequency_;

  /** Object for finding predefined terms among words in files */
  PredefinedWordsFinder predefined_words_finder_;

  /** Object for finding stop words among words in files */
  PredefinedWordsFinder stopwords_finder_;

  /** Object for processing prepositions for different cases */
  PrepositionsProcessor prepositions_processor_;

  /** Pointer to the object for parsing files */
  std::unique_ptr<FileParser> file_parser_;
};
