// Copyright 2015 Michael Nokel
#include "./russian_file_parser.h"
#include "../auxiliary.h"
#include "../predefined_words_finder.h"
#include "../prepositions_processor.h"
#include "./russian/noun_phrases_extractor.h"
#include "./russian/single_words_extractor.h"
#include "./russian/single_word_terms_extractor.h"
#include "./russian/two_word_terms_extractor.h"
#include <iconv.h>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <cctype>
#include <fstream>
#include <stdexcept>
#include <string>
#include <vector>

using boost::is_any_of;
using boost::split;
using boost::token_compress_on;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::string;
using std::vector;

RussianFileParser::RussianFileParser(
    const PredefinedWordsFinder& predefined_terms_finder,
    const PredefinedWordsFinder& stopwords_finder,
    const PrepositionsProcessor& prepostions_processor)
    : FileParser(predefined_terms_finder,
                 stopwords_finder),
      prepositions_processor_(prepostions_processor) {
}

void RussianFileParser::ParseFile(unsigned int file_number,
                                  const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse file");
  }
  RussianSingleWordTermsExtractor single_word_terms_extractor(
      predefined_terms_finder_);
  RussianTwoWordTermsExtractor two_word_terms_extractor(
      predefined_terms_finder_);
  RussianNounPhrasesExtractor noun_phrases_extractor;
  RussianSingleWordsExtractor single_words_extractor;
  unsigned int words_number_in_file = 0;
  unsigned int tokens_number_in_file = 0;
  WordProcessingData word_processing_data;
  vector<RussianWordProperties> previous_words;
  while (!file.eof()) {
    vector<RussianWordProperties> same_word_forms;
    ExtractSameWordForms(words_number_in_file,
                         tokens_number_in_file,
                         &file,
                         &word_processing_data,
                         &same_word_forms);
    CorrectWordProcessingData(words_number_in_file,
                              tokens_number_in_file,
                              &word_processing_data);
    bool continue_processing;
    vector<RussianWordProperties> words_to_store;
    words_processor_.AddNewWords(same_word_forms,
                                 continue_processing,
                                 &previous_words,
                                 &words_to_store);
    if (!continue_processing) {
      single_word_terms_extractor.AddNewTermCandidates(words_to_store);
      two_word_terms_extractor.AddNewTermCandidates(words_to_store);
      noun_phrases_extractor.AddNewTermCandidates(words_to_store);
      single_words_extractor.AddNewTermCandidates(words_to_store);
    }
  }
  file.close();
  if (!previous_words.empty()) {
    single_word_terms_extractor.AddNewTermCandidates(previous_words);
    two_word_terms_extractor.AddNewTermCandidates(previous_words);
    noun_phrases_extractor.AddNewTermCandidates(previous_words);
    single_words_extractor.AddNewTermCandidates(previous_words);
  }
  single_word_terms_extractor.ProcessLastWords();
  two_word_terms_extractor.ProcessLastWords();
  single_word_terms_accumulator_.AccumulateTermCandidates(
      file_number,
      single_word_terms_extractor.GetStartIterator(),
      single_word_terms_extractor.GetEndIterator());
  two_word_terms_accumulator_.AccumulateTermCandidates(
      file_number,
      two_word_terms_extractor.GetStartIterator(),
      two_word_terms_extractor.GetEndIterator());
  single_word_noun_phrases_accumulator_.AccumulateNounPhrases(
      noun_phrases_extractor.GetStartSingleWordIterator(),
      noun_phrases_extractor.GetEndSingleWordIterator());
  two_word_noun_phrases_accumulator_.AccumulateNounPhrases(
      noun_phrases_extractor.GetStartTwoWordIterator(),
      noun_phrases_extractor.GetEndTwoWordIterator());
  single_words_accumulator_.AccumulateSingleWords(
      single_words_extractor.GetStartIterator(),
      single_words_extractor.GetEndIterator());
}

void RussianFileParser::ExtractSameWordForms(
    unsigned int words_number_in_file,
    unsigned int tokens_number_in_file,
    ifstream* file,
    WordProcessingData* word_processing_data,
    vector<RussianWordProperties>* same_word_forms) const {
  vector<RussianWordProperties> extracted_words;
  int first_letter = -1;
  do {
    string line;
    getline(*file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    line.erase(line.begin(),
               find_if(line.begin(),
                       line.end(),
                       not1(ptr_fun<int, int>(isspace))));
    line = ConvertFromCP1251ToUTF8(line);
    vector<string> tokens;
    split(tokens, line, is_any_of(" \t"), token_compress_on);
    RussianWordProperties extracted_word_properties;
    bool is_token = false;
    bool is_word = false;
    words_processor_.ExtractWordFromLine(tokens,
                                         word_processing_data,
                                         is_token,
                                         is_word,
                                         &extracted_word_properties);
    if (is_word) {
      word_processing_data->word_extracted = true;
      if (extracted_word_properties.word_category != WordCategory::DIVIDER) {
        word_processing_data->candidate_extracted = true;
      }
    }
    if (is_token) {
      word_processing_data->token_extracted = true;
    }
    extracted_word_properties.word_number_in_file = words_number_in_file;
    extracted_word_properties.token_number_in_file = tokens_number_in_file;
    extracted_words.push_back(extracted_word_properties);
    first_letter = file->get();
  } while (!file->eof() && isspace(first_letter));
  if (!file->eof()) {
    file->unget();
  }
  CorrectExtractedSameWordForms(extracted_words, same_word_forms);
}

void RussianFileParser::CorrectExtractedSameWordForms(
    const vector<RussianWordProperties>& extracted_words,
    vector<RussianWordProperties>* same_word_forms) const noexcept {
  bool has_known_word = false;
  bool has_auxiliary_word = false;
  for (const auto& word_properties: extracted_words) {
    if (!word_properties.is_novel_word) {
      has_known_word = true;
    }
    if (word_properties.is_auxiliary_word) {
      has_auxiliary_word = true;
    }
  }
  vector<RussianWordProperties> filtered_words;
  if (has_auxiliary_word) {
    for (const auto& word_properties: extracted_words) {
      if (word_properties.is_auxiliary_word) {
        filtered_words.push_back(word_properties);
      }
    }
  } else if (has_known_word) {
    for (const auto& word_properties: extracted_words) {
      if (!word_properties.is_novel_word ||
          word_properties.word.find("-") != string::npos) {
        filtered_words.push_back(word_properties);
      }
    }
  } else {
    filtered_words.push_back(extracted_words[0]);
  }
  *same_word_forms = filtered_words;
}

void RussianFileParser::CorrectWordProcessingData(
    unsigned int& words_number_in_file,
    unsigned int& tokens_number_in_file,
    WordProcessingData* word_processing_data) const {
  if (word_processing_data->word_extracted) {
    ++words_number_in_file;
    word_processing_data->was_end_of_sentence = false;
    if (word_processing_data->candidate_extracted) {
      word_processing_data->was_punctuation_divider = false;
    }
  }
  word_processing_data->word_extracted = false;
  word_processing_data->candidate_extracted = false;
  if (word_processing_data->token_extracted) {
    ++tokens_number_in_file;
  }
  word_processing_data->token_extracted = false;
}

string RussianFileParser::ConvertFromCP1251ToUTF8(
    const string& string_cp1251) const {
  iconv_t cd = iconv_open("utf-8", "cp1251");
  if (cd == (iconv_t) - 1) {
    return "";
  }
  size_t input_size = string_cp1251.length();
  size_t output_size = 3*input_size + 1;
  size_t total_output_size = output_size;
#ifdef _WIN32
  const char* input_string_pointer = string_cp1251.data();
#else
  char* input_string_pointer = const_cast<char*>(string_cp1251.data());
#endif
  char* output_string = new char[output_size];
  char* output_string_pointer = output_string;
  memset(output_string, 0, output_size);
  if ((iconv(cd, &input_string_pointer, &input_size, &output_string_pointer,
             &output_size)) == (size_t) - 1) {
    iconv_close(cd);
    delete[] output_string;
    return "";
  }
  iconv_close(cd);
  string string_utf8 = string(output_string, total_output_size - output_size);
  delete[] output_string;
  return string_utf8;
}