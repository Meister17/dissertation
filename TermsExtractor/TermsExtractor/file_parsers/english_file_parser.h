// Copyright 2015 Michael Nokel
#pragma once

#include "./file_parser.h"
#include "../auxiliary.h"
#include "../predefined_words_finder.h"
#include <string>

/**
  @brief Class for parsing English files and extracting term candidates and noun
  phrases from them

  This class should be used for parsing English files and extracting term
  candidates and noun phrases from them
*/
class EnglishFileParser: public FileParser {
 public:
  /**
    Initializes object
    @param predefined_terms_finder object for finding predefined terms among
    words in files
    @param stopwords_finder object for finding stop words among words in files
  */
  EnglishFileParser(const PredefinedWordsFinder& predefined_terms_finder,
                    const PredefinedWordsFinder& stopwords_finder);

  /**
    Parses given English file and extracts all term candidates and noun phrases
    in it
    @param file_number number of processing file
    @param filename English file for parsing
    @throw std::runtime_error in case of any occurred error
  */
  void ParseFile(unsigned int file_number, const std::string& filename);

 private:
  /**
    Determines category of extracted word
    @param word extracted word
    @param term_category string representation of the category of the extracted
    word
    @param[out] word_category category of the processed word
    @param[out] word_processing_data pointer to the structure containing all
    necessary information about processing word
  */
  void DetermineTermCategory(const std::string& word,
                             const std::string& term_category,
                             WordCategory& word_category,
                             WordProcessingData* word_processing_data) const;
};