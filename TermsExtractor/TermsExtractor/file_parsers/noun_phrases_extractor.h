// Copyright 2015 Michael Nokel
#pragma once

#include "../auxiliary.h"
#include <algorithm>
#include <map>
#include <string>
#include <vector>

/**
  @brief Class intended to be used for extracting noun phrases

  This class extracts noun phrases and their term frequencies while parsing
  files. It should be derived by all subclasses working with particular language
*/
class NounPhrasesExtractor {
 public:
  /**
    Returns iterator to the beginning of the map containing extracted single
    word noun phrases and their term frequencies
    @return iterator to the beginning of the map containing extracted single
    word noun phrases and their term frequencies
  */
  virtual std::map<std::string, unsigned int>::const_iterator
      GetStartSingleWordIterator() const {
    return single_word_phrases_map_.cbegin();
  }

  /**
    Returns iterator to the end of the map containing extracted single word noun
    phrases and their term frequencies
    @return iterator to the end of the map containing extracted single word noun
    phrases and their term frequencies
  */
  virtual std::map<std::string, unsigned int>::const_iterator
      GetEndSingleWordIterator() const {
    return single_word_phrases_map_.cend();
  }

  /**
    Returns iterator to the beginning of the map containing extracted two word
    noun phrases and their term frequencies
    @return iterator to the beginning of the map containing extracted two word
    noun phrases and their term frequencies
  */
  virtual std::map<std::string, unsigned int>::const_iterator
      GetStartTwoWordIterator() const {
    return two_word_phrases_map_.cbegin();
  }

  /**
    Returns iterator to the end of the map containing extracted two word noun
    phrases and their term frequencies
    @return iterator to the end of the map containing extracted two word noun
    phrases and their term frequencies
  */
  virtual std::map<std::string, unsigned int>::const_iterator
      GetEndTwoWordIterator() const {
    return two_word_phrases_map_.cend();
  }

 protected:
  /**
    @brief Structure containing all necessary data for noun phrases extraction

    This structure contains building noun phrase, its category and length
  */
  struct PhraseCandidateData {
    /**
      Initializes object
      @param word first word of the building phrase
      @param category category of the first word in the building phrase
    */
    PhraseCandidateData(const std::string& word,
                        const WordCategory& category)
        : noun_phrase(word),
          word_category(category) {
    }

    /** Building noun phrase */
    std::string noun_phrase;

    /** Category of the building noun phrase */
    WordCategory word_category;

    /** Length of the building noun phrase */
    unsigned int length = 1;
  };

  /**
    Inserts new extracted phrase to the given map
    @oaram noun_phrase phrase to be inserted to the given map
    @param noun_phrases_map map containing found noun phrases where new phrase
    should be inserted
  */
  virtual void InsertNewPhrase(
      const std::string& noun_phrase,
      std::map<std::string, unsigned int>* noun_phrases_map) const {
    auto inserted_result = noun_phrases_map->insert({noun_phrase, 1});
    if (!inserted_result.second) {
      ++inserted_result.first->second;
    }
  }

  /** Length of noun phrases for single-word term candidates */
  const unsigned int kLengthForSingleWordTerms_ = 2;

  /** Length of noun phrases for two-word term candidates */
  const unsigned int kLengthForTwoWordTerms_ = 3;

  /** Map containing found noun phrases for single-word term candidates and
      their frequencies */
  std::map<std::string, unsigned int> single_word_phrases_map_;

  /** Map containing found noun phrases for two-word term candidates and
      their frequencies */
  std::map<std::string, unsigned int> two_word_phrases_map_;
};
