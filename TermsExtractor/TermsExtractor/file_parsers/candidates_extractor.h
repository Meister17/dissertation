// Copyright 2015 Michael Nokel
#pragma once

#include "../auxiliary.h"
#include "./context_words_extractor.h"
#include <map>
#include <memory>
#include <string>

/**
  @brief Abstract class for extracting and printing term candidates

  This class is intended to extract and print all found term candidates. It
  should be derived by all subclasses working with particular language
*/
class CandidatesExtractor {
 public:
  /**
    Processes last words and calculates various domain consensuses for them
  */
  virtual void ProcessLastWords();

  /**
    Returns start iterator to the map containing term candidates and necessary
    data about them
    @return start iterator to the map containing term candidates and necessary
    data about them
  */
  virtual std::map<std::string, TermCandidateData>::const_iterator
      GetStartIterator() const {
    return term_candidates_map_.cbegin();
  }

  /**
    Returns end iterator to the map containing term candidates and necessary
    data about them
    @return end iterator to the map containing term candidates and necessary
    data about them
  */
  virtual std::map<std::string, TermCandidateData>::const_iterator
      GetEndIterator() const {
    return term_candidates_map_.cend();
  }

  /**
    Virtual destructor for the purposes of proper deriving
  */
  virtual ~CandidatesExtractor() {
  }

 protected:
  /**
    Inserts new term candidate and necessary data about it
    @param term_candidate term candidate to insert
    @param new_term_data new data for term candidate
  */
  virtual void InsertNewTermCandidate(const std::string& term_candidate,
                                      const TermCandidateData& new_term_data);

  /**
    Calculates part of the domain consensus for term
    @param number_of_words_in_file total number of words in file
    @param term_frequency term frequency in file for given term
    @return part of the domain consensus for given term
  */
  virtual double CalculateDomainConsensusPart(
      unsigned int words_number_in_file,
      unsigned int term_frequency) const;

  /** Number of candidates in the parsed file */
  unsigned int candidates_number_in_file_ = 0;

  /** Object used for calculating NearTermsFreq values */
  std::unique_ptr<ContextWordsExtractor> context_words_extractor_;

  /** Map containing term candidates and necessary information about them
      extracted from parsed file */
  std::map<std::string, TermCandidateData> term_candidates_map_;
};
