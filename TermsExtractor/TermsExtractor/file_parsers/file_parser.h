// Copyright 2015 Michael Nokel
#pragma once

#include "../auxiliary.h"
#include "../predefined_words_finder.h"
#include "./candidates_accumulator.h"
#include "./noun_phrases_accumulator.h"
#include "./single_words_accumulator.h"
#include <memory>
#include <string>

/**
  @brief Base class for parsing files

  This class should be used for parsing files and extracting term candidates
  with noun phrases. This class should be derived to parse particular files
  depending on language
*/
class FileParser {
 public:
  /**
    Initializes object
    @param predefined_terms_finder object for finding predefined terms among
    words in files
    @param stopwords_finder object for finding stop words among words in files
  */
  FileParser(const PredefinedWordsFinder& predefined_terms_finder,
             const PredefinedWordsFinder& stopwords_finder)
      : predefined_terms_finder_(predefined_terms_finder),
        stopwords_finder_(stopwords_finder) {
  }

  /**
    Prepares for processing give number of files
    @param total_number_files total number of files for processing
  */
  void PrepareForFiles(unsigned int total_number_files) {
    single_word_terms_accumulator_.PrepareForFiles(total_number_files);
    two_word_terms_accumulator_.PrepareForFiles(total_number_files);
  }

  /**
    Parses file and extracts term candidates and noun phrases from it
    @param file_number number of processing file
    @param filename file for parsing
    @throw std::runtime_error in case of any occurred error
  */
  virtual void ParseFile(unsigned int file_number,
                         const std::string& filename) = 0;

  /**
    Prints all extracted term candidates in the given files
    @param single_words_filename file for printing extracted single words and
    their frequencies
    @param single_word_terms_filename file for printing extracted single-word
    term candidates and some data about them
    @param single_word_terms_topic_input_filename file for printing input for
    topic models for single-word term candidates
    @param single_word_terms_topic_vocabulary_filename file for printing
    vocabulary for topic models for single-word term candidates
    @param single_word_phrases_filename file for printing extracted noun phrases
    for single-word term candidates
    @param two_word_terms_filename file for printing extracted two-word term
    candidates and some data about them
    @param two_word_terms_topic_input_filename file for printing input for topic
    models for two-word term candidates
    @param two_word_terms_topic_vocabulary_filename file for printing vocabulary
    for topic models for two-word term candidates
    @param two_word_phrases_filename file for printing extracted noun phrases
    for two-word term candidates
    @param minimum_term_frequency minimum term frequency to be taken into
    account
    @throw std::runtime_error in case of any occurred errors
  */
  virtual void PrintTermCandidates(
      const std::string& single_words_filename,
      const std::string& single_word_terms_filename,
      const std::string& single_word_terms_topic_input_filename,
      const std::string& single_word_terms_topic_vocabulary_filename,
      const std::string& single_word_phrases_filename,
      const std::string& two_word_terms_filename,
      const std::string& two_word_terms_topic_input_filename,
      const std::string& two_word_terms_topic_vocabulary_filename,
      const std::string& two_word_phrases_filename,
      unsigned int minimum_term_frequency);

  /**
    Virtual destructor for proper deriving
  */
  virtual ~FileParser() {
  }

 protected:
  /** Object for finding predefined terms among words in files */
  const PredefinedWordsFinder& predefined_terms_finder_;

  /** Object for finding stop words among words in files */
  const PredefinedWordsFinder& stopwords_finder_;

  /** Object for accumulating and printing single-word term candidates */
  CandidatesAccumulator single_word_terms_accumulator_;

  /** Object for accumulating and printing two-word term candidates */
  CandidatesAccumulator two_word_terms_accumulator_;

  /** Object for accumulating and printing single word noun phrases */
  NounPhrasesAccumulator single_word_noun_phrases_accumulator_;

  /** Object for accumulating and printing two word noun phrases */
  NounPhrasesAccumulator two_word_noun_phrases_accumulator_;

  /** Object for accumulating and printing single words */
  SingleWordsAccumulator single_words_accumulator_;
};