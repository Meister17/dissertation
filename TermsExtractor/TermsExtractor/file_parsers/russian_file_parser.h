// Copyright 2015 Michael Nokel
#pragma once

#include "./file_parser.h"
#include "../auxiliary.h"
#include "../predefined_words_finder.h"
#include "../prepositions_processor.h"
#include "./russian/words_processor.h"
#include <fstream>
#include <string>
#include <vector>

/**
  @brief Class for parsing Russian files and extracting term candidates and noun
  phrases from them

  This class should be used for parsing Russian files and extracting term
  candidates and noun phrases from them
*/
class RussianFileParser: public FileParser {
 public:
  /**
    Initializes object
    @param predefined_terms_finder object for finding predefined terms among
    words in texts
    @param stopwords_finder object for finding stop words in texts
    @param prepositions_processor object for working with prepositions and their
    cases
  */
  RussianFileParser(const PredefinedWordsFinder& predefined_terms_finder,
                    const PredefinedWordsFinder& stopwords_finder,
                    const PrepositionsProcessor& prepositions_processor);

  /**
    Parses given Russian file and extracts all term candidates and noun phrases
    in it
    @param file_number number of processing file
    @param filename Russian file for parsing
    @throw std::runtime_error in case of any occurred error
  */
  void ParseFile(unsigned int file_number, const std::string& filename);

 private:
  /**
    Extracts words that have same forms from the current line in file
    @param words_number_in_file current number of extracted words in file
    @param tokens_number_in_file current number of extracted tokens in file
    @param[out] file file from where to extract words
    @param[out] word_processing_data structure containing information about
    processing word
    @param[out] same_word_forms vector where extracted known words with the
    same forms will be stored
    @throw std::runtime_error in case of any error
  */
  void ExtractSameWordForms(unsigned int words_number_in_file,
                            unsigned int tokens_number_in_file,
                            std::ifstream* file,
                            WordProcessingData* word_processing_data,
                            std::vector<RussianWordProperties>* same_word_forms)
                            const;

  /**
    Corrects extracted same words
    @param extracted_words vector with extracted words with their forms
    @param[out] same_word_forms vector where extracted words with the same
    forms will e stored
  */
  void CorrectExtractedSameWordForms(
      const std::vector<RussianWordProperties>& extracted_words,
      std::vector<RussianWordProperties>* same_word_forms) const noexcept;

  /**
    Corrects data of the current processing word
    @param[out] number_of_words_in_file current number of extracted words in
    file (will be incremented if the extracted lemma is real word)
    @param[out] number_of_tokens_in_file current number of extracted tokens in
    file (will be incremented)
    @param[out] word_processing_data data of the current processing word
  */
  void CorrectWordProcessingData(unsigned int& number_of_words_in_file,
                                 unsigned int& number_of_tokens_in_file,
                                 WordProcessingData* word_processing_data)
                                 const;

  /**
    Converts string from UTF8 encoding to CP1251
    @param string_cp1251 string in CP1251 encoding
    @return equivalent string in UTF8 encoding
  */
  std::string ConvertFromCP1251ToUTF8(const std::string& string_cp1251) const;

  /** Object for processing prepositions for different cases */
  PrepositionsProcessor prepositions_processor_;

  /** Object which extracts and processed words from collection */
  WordsProcessor words_processor_;
};