// Copyright 2012 Michael Nokel
#pragma once

#include "./candidates_extractor.h"
#include "../../auxiliary.h"
#include "../../predefined_words_finder.h"
#include <set>
#include <vector>

/**
  @brief Class for extracting two-word Russian term candidates

  This class should be used for extracting two-word Russian term candidates
*/
class RussianTwoWordTermsExtractor : public RussianCandidatesExtractor {
 public:
  /**
    Initializes object
    @param predefined_words_finder object for finding predefined terms among
    words in texts
  */
  explicit RussianTwoWordTermsExtractor(
      const PredefinedWordsFinder& predefined_words_finder)
      : RussianCandidatesExtractor(predefined_words_finder) {
  }

  /**
    Adds new term candidates
    @param new_term_candidates vector containing word properties of new term
    candidates
  */
  void AddNewTermCandidates(
      const std::vector<RussianWordProperties>& new_term_candidates);

 private:
  /**
    Checks whether given word is in the given case
    @param word_parts changeable word parts of the word to check
    @param word_case word's case to check
    @return true if given word is in the given case
  */
  bool IsInCase(const std::set<RussianChangeableWordParts>& word_parts,
                const RussianWordCase& word_case) const;

  /** Flag indicating whether previous word was ambiguous */
  bool was_ambiguous_word_ = false;

  /** Previous words for forming two-word term candidate */
  std::vector<RussianWordProperties> previous_words_;
};
