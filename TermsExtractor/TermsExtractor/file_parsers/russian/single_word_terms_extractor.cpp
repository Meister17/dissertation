// Copyright 2012 Michael Nokel
#include "./single_word_terms_extractor.h"
#include "../../auxiliary.h"
#include <string>
#include <vector>

using std::string;
using std::vector;

void RussianSingleWordTermsExtractor::AddNewTermCandidates(
    const vector<RussianWordProperties>& new_term_candidates) {
  vector<string> context_words;
  bool is_ambiguous_word = new_term_candidates.size() > 1;
  for (const auto& term_candidate: new_term_candidates) {
    if (term_candidate.word_category == WordCategory::NOUN ||
        term_candidate.word_category == WordCategory::ADJECTIVE) {
      bool is_subject = false;
      if (term_candidate.word_category == WordCategory::NOUN) {
        for (const auto& word_part: term_candidate.changeable_word_parts) {
          is_subject = word_part.word_case == RussianWordCase::NOMINATIVE;
        }
      }
      TermCandidateData term_candidate_data(term_candidate.word_category,
                                            term_candidate.word_number_in_file,
                                            term_candidate.is_capital_word,
                                            term_candidate.is_non_initial_word,
                                            term_candidate.is_novel_word,
                                            is_ambiguous_word,
                                            is_subject);
      InsertNewTermCandidate(term_candidate.word, term_candidate_data);
      context_words.push_back(term_candidate.word);
    }
  }
  if (!context_words.empty()) {
    AddToContextWindow(context_words);
  }
}
