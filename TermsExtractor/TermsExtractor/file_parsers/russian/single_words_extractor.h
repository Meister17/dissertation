// Copyright 2015 Michael Nokel
#pragma once

#include "../../auxiliary.h"
#include "../single_words_extractor.h"
#include <vector>

/**
  @brief Class for extracting single words from the parsing Russian file

  This class should be used for extracting single words and their frequencies
  from the parsing Russian file. Then accumulated information should be merged
  with previously extracted one from other files.
*/
class RussianSingleWordsExtractor : public SingleWordsExtractor {
 public:
  /**
    Adds new extracted words to the map
    @param candidates vector containing properties of the adding words
  */
  void AddNewTermCandidates(
      const std::vector<RussianWordProperties>& candidates) {
    for (const auto& word_properties : candidates) {
      if ((word_properties.word_category == WordCategory::NOUN ||
           word_properties.word_category == WordCategory::ADJECTIVE)) {
        auto result = single_words_map_.insert({word_properties.word, 1});
        if (!result.second) {
          ++result.first->second;
        }
      }
    }
  }
};