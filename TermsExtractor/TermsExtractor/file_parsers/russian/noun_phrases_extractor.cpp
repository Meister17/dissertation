// Copyright 2012 Michael Nokel
#include "./noun_phrases_extractor.h"
#include "../../auxiliary.h"
#include <string>
#include <vector>

using std::string;
using std::vector;

void RussianNounPhrasesExtractor::AddNewTermCandidates(
    const vector<RussianWordProperties>& new_term_candidates) {
  vector<PhraseCandidateData> new_phrases;
  for (const auto& new_candidate: new_term_candidates) {
    if (new_candidate.word_category == WordCategory::ADJECTIVE) {
      if (!new_candidate.is_after_punctuation_divider) {
        for (const auto& phrase: noun_phrases_) {
          if (phrase.length < kLengthForTwoWordTerms_) {
            ExtendNounPhrase(phrase, new_candidate, &new_phrases);
          }
        }
      }
      new_phrases.push_back(PhraseCandidateData(new_candidate.word));
    } else if (new_candidate.word_category == WordCategory::NOUN) {
      if (!new_candidate.is_after_punctuation_divider) {
        for (const auto& phrase: noun_phrases_) {
          if (phrase.length < kLengthForTwoWordTerms_) {
            ExtendNounPhrase(phrase, new_candidate, &new_phrases);
            if (new_phrases.back().length == kLengthForTwoWordTerms_) {
              InsertNewPhrase(new_phrases.back().noun_phrase,
                              &two_word_phrases_map_);
            } else {
              InsertNewPhrase(new_phrases.back().noun_phrase,
                              &single_word_phrases_map_);
            }
          }
        }
      }
      new_phrases.push_back(PhraseCandidateData(new_candidate.word));
    }
  }
  noun_phrases_ = new_phrases;
}

void RussianNounPhrasesExtractor::ExtendNounPhrase(
    const PhraseCandidateData& previous_candidate_data,
    const RussianWordProperties& word_properties,
    vector<PhraseCandidateData>* new_noun_phrases) const {
  new_noun_phrases->push_back(previous_candidate_data);
  new_noun_phrases->back().noun_phrase += " " + word_properties.word;
  ++new_noun_phrases->back().length;
}
