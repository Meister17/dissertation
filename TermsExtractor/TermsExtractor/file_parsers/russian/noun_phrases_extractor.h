// Copyright 2012 Michael Nokel
#pragma once

#include "../../auxiliary.h"
#include "../noun_phrases_extractor.h"
#include <boost/algorithm/string.hpp>
#include <string>
#include <vector>

/**
  @brief Class for extracting phrases from the collection

  For each thread that parses file its own instance of this class should be
  created. This instance should be used for accumulating phrases and their
  frequencies. Then accumulated information should be merged with previously
  extracted.
*/
class RussianNounPhrasesExtractor : public NounPhrasesExtractor {
 public:
  /**
    Adds new term candidates and store all formed phrases
    @param new_term_candidates vector containing word properties of new
    candidates
  */
  void AddNewTermCandidates(
    const std::vector<RussianWordProperties>& new_term_candidates);

 private:
  /**
    @brief Structure containing all necessary data for noun phrases extraction

    This structure contains building noun phrase, its category and length
  */
  struct PhraseCandidateData {
    /**
      Initializes object
      @param word first word of the building phrase
    */
    PhraseCandidateData(const std::string& word)
        : noun_phrase(word) {
    }

    /** Building noun phrase */
    std::string noun_phrase = "";

    /** Length of the building noun phrase */
    unsigned int length = 1;
  };

  /**
    Extends candidate for noun phrases by new word
    @param previous_candidate_data data of the previous extending noun phrase
    @param word_properties properties of the new adding word
    @param[out] new_noun_phrases vector where new formed candidates should be
    placed
  */
  void ExtendNounPhrase(
      const PhraseCandidateData& previous_candidate_data,
      const RussianWordProperties& word_properties,
      std::vector<PhraseCandidateData>* new_noun_phrases) const;

  /** Vector containing candidates for noun phrases */
  std::vector<PhraseCandidateData> noun_phrases_;
};
