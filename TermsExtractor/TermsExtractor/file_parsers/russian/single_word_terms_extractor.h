// Copyright 2012 Michael Nokel
#pragma once

#include "./candidates_extractor.h"
#include "../../auxiliary.h"
#include "../../predefined_words_finder.h"
#include <vector>

/**
  @brief Class for extracting single-word Russian term candidates

  This class should be used for extracting single-word Russian term candidates
*/
class RussianSingleWordTermsExtractor : public RussianCandidatesExtractor {
 public:
  /**
    Initializes object
    @param[out] predefined_words_finder pointer to object that should be used
    for determining predefined terms among all term candidates
  */
  explicit RussianSingleWordTermsExtractor(
      const PredefinedWordsFinder& predefined_words_finder)
      : RussianCandidatesExtractor(predefined_words_finder) {
  }

  /**
    Adds new term candidates
    @param new_term_candidates vector containing word properties of new term
    candidates
  */
  void AddNewTermCandidates(
      const std::vector<RussianWordProperties>& new_term_candidates);
};
