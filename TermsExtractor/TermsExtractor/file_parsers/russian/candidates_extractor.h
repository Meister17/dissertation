// Copyright 2012 Michael Nokel
#pragma once

#include "./context_words_extractor.h"
#include "../candidates_extractor.h"
#include "../../auxiliary.h"
#include "../../predefined_words_finder.h"
#include <string>
#include <vector>

/**
  @brief Abstract class for extracting Russian term candidates

  This class should be derived by each class intended to extract Russian term
  candidates depending on their length
*/
class RussianCandidatesExtractor : public CandidatesExtractor {
 public:
  /**
    Initializes object
    @param predefined_words_finder object for finding predefined terms among
    words in texts
  */
  explicit RussianCandidatesExtractor(
      const PredefinedWordsFinder& predefined_words_finder) {
    context_words_extractor_.reset(
        new RussianContextWordsExtractor(predefined_words_finder));
  }

  /**
    Adds new term candidates
    @param new_term_candidates vector containing word properties of new term
    candidates
  */
  virtual void AddNewTermCandidates(
      const std::vector<RussianWordProperties>& new_term_candidates) = 0;

  /**
    Virtual destructor for the purposes of proper deriving
  */
  virtual ~RussianCandidatesExtractor() {
  }

 protected:
  /**
    Adds given term candidates to the context window
    @param term_candidates term candidates for adding
  */
  virtual void AddToContextWindow(
      const std::vector<std::string>& term_candidates) {
    std::vector<NearTermsFrequency> near_terms_frequencies =
    static_cast<RussianContextWordsExtractor*>(
        context_words_extractor_.get())->StoreNewTermCandidates(
            term_candidates);
    for (const auto& near_terms_frequency: near_terms_frequencies) {
      std::string word = near_terms_frequency.term_candidate;
      term_candidates_map_.find(word)->second.near_terms_frequency +=
          near_terms_frequency.near_terms_frequency;
    }
  }
};
