// Copyright 2012 Michael Nokel
#include "./nouns_processor.h"
#include "../../../auxiliary.h"
#include <string>

using std::string;

void NounsProcessor::ProcessWord(const string& token,
                                 RussianWordProperties* word_properties) const {
  if (word_properties->word_category != WordCategory::PROPER_NOUN) {
    word_properties->word_category = WordCategory::NOUN;
  }
  for (unsigned int index = 0; index < token.size(); index += 4) {
    string case_sign = token.substr(index + 2, 2);
    string gender_sign = token.substr(index, 2);
    RussianChangeableWordParts word_parts(GetWordCase(case_sign),
                                          RussianWordGender::NO_GENDER);
    if (string("абвгдемн").find(case_sign) != string::npos) {
      if (gender_sign == "в") {
        word_parts.word_gender = RussianWordGender::FEMININE;
        word_properties->changeable_word_parts.insert(word_parts);
        word_parts.word_gender = RussianWordGender::MASCULINE;
      } else {
        word_parts.word_gender = GetWordGender(gender_sign);
      }
    } else {
      word_parts.word_gender = RussianWordGender::PLURAL;
    }
    word_properties->changeable_word_parts.insert(word_parts);
  }
}

RussianWordCase NounsProcessor::GetWordCase(const string& case_sign) const {
  if (string("аж").find(case_sign) != string::npos) {
    return RussianWordCase::NOMINATIVE;
  } else if (string("бз").find(case_sign) != string::npos) {
    return RussianWordCase::GENITIVE;
  } else if (string("ви").find(case_sign) != string::npos) {
    return RussianWordCase::DATIVE;
  } else if (string("гй").find(case_sign) != string::npos) {
    return RussianWordCase::ACCUSATIVE;
  } else if (string("дк").find(case_sign) != string::npos) {
    return RussianWordCase::INSTRUMENTAL;
  } else if (string("ел").find(case_sign) != string::npos) {
    return RussianWordCase::PREPOSITIONAL;
  }
  return RussianWordCase::NO_CASE;
}

RussianWordGender NounsProcessor::GetWordGender(const string& gender_sign)
    const {
  if (string("аб").find(gender_sign) != string::npos) {
    return RussianWordGender::MASCULINE;
  } else if (string("гд").find(gender_sign) != string::npos) {
    return RussianWordGender::FEMININE;
  } else if (string("еж").find(gender_sign) != string::npos) {
    return RussianWordGender::NEUTER;
  } else if (gender_sign == "и") {
    return RussianWordGender::PLURAL;
  }
  return RussianWordGender::NO_GENDER;
}
