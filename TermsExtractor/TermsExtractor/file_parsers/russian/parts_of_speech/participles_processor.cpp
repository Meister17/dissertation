// Copyright 2012 Michael Nokel
#include "./participles_processor.h"
#include "../../../auxiliary.h"
#include <string>

using std::string;

void ParticiplesProcessor::ProcessWord(const string& token,
                                       RussianWordProperties* word_properties)
                                       const {
  word_properties->word_category = WordCategory::AGREEMENT_WORD;
  for (unsigned int index = 1; index < token.size(); index += 4) {
    string sign = token.substr(index, 2);
    RussianChangeableWordParts word_parts(GetWordCase(sign),
                                          GetWordGender(sign));
    word_properties->changeable_word_parts.insert(word_parts);
  }
}

RussianWordCase ParticiplesProcessor::GetWordCase(const string& case_sign)
    const {
  if (string("азох").find(case_sign) != string::npos) {
    return RussianWordCase::NOMINATIVE;
  } else if (string("бипц").find(case_sign) != string::npos) {
    return RussianWordCase::GENITIVE;
  } else if (string("вйрч").find(case_sign) != string::npos) {
    return RussianWordCase::DATIVE;
  } else if (string("гксш").find(case_sign) != string::npos) {
    return RussianWordCase::ACCUSATIVE;
  } else if (string("длтщ").find(case_sign) != string::npos) {
    return RussianWordCase::INSTRUMENTAL;
  } else if (string("емуы").find(case_sign) != string::npos) {
    return RussianWordCase::PREPOSITIONAL;
  }
  return RussianWordCase::NO_CASE;
}

RussianWordGender ParticiplesProcessor::GetWordGender(const string& gender_sign)
    const {
  if (string("абвгдеж").find(gender_sign) != string::npos) {
    return RussianWordGender::MASCULINE;
  } else if (string("зийклмн").find(gender_sign) != string::npos) {
    return RussianWordGender::FEMININE;
  } else if (string("опрстуф").find(gender_sign) != string::npos) {
    return RussianWordGender::NEUTER;
  } else if (string("хцчшщыэ").find(gender_sign) != string::npos) {
    return RussianWordGender::PLURAL;
  }
  return RussianWordGender::NO_GENDER;
}
