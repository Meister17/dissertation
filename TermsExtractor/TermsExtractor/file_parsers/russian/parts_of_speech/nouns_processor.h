// Copyright 2012 Michael Nokel
#pragma once

#include "./parts_of_speech_processor.h"
#include "../../../auxiliary.h"
#include <string>

/**
  @brief Class for processing Russian nouns

  This class is derived from base abstract class PartsOfSpeechProcessor and
  should be used for working with nouns.
*/
class NounsProcessor : public PartsOfSpeechProcessor {
 public:
  /**
    Processes noun and stores its cases and genders
    @param token token for processing
    @param[out] word_properties structure where cases and genders of given noun
    will be stored
  */
  void ProcessWord(const std::string& token,
                   RussianWordProperties* word_properties) const;

 private:
  /**
    Gets noun's case depending on given its char representation
    @param case_sign string representation of noun's case
    @return noun's case
  */
  RussianWordCase GetWordCase(const std::string& case_sign) const;

  /**
    Gets noun's gender depending on given its char representation
    @param gender_sign string representation of noun's gender
    @return noun's gender
  */
  RussianWordGender GetWordGender(const std::string& gender_sign) const;
};
