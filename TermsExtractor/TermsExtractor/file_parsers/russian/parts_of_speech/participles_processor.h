// Copyright 2012 Michael Nokel
#pragma once

#include "./parts_of_speech_processor.h"
#include "../../../auxiliary.h"
#include <string>

/**
  @brief Class for processing participles

  This class is derived from base abstract class PartsOfSpeechProcessor and
  should be used for working with participles.
*/
class ParticiplesProcessor : public PartsOfSpeechProcessor {
 public:
  /**
    Processes participle and stores its cases and genders
    @param token token for processing
    @param[out] word_properties structure where cases and genders of given
    participle will be stored
  */
  void ProcessWord(const std::string& token,
                   RussianWordProperties* word_properties) const;

 private:
  /**
    Gets participle's case depending on given its char representation
    @param case_sign string representation of participle's case
    @return participle's case
  */
  RussianWordCase GetWordCase(const std::string& case_sign) const;

  /**
    Gets participle's gender depending on given its char representation
    @param gender_sign string representation of participle's gender
    @return participle's gender
  */
  RussianWordGender GetWordGender(const std::string& gender_sign) const;
};
