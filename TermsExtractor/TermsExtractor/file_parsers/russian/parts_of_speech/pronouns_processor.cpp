// Copyright 2012 Michael Nokel
#include "./pronouns_processor.h"
#include "../../../auxiliary.h"
#include <string>

using std::string;

void PronounsProcessor::ProcessWord(const string& token,
                                    RussianWordProperties* word_properties)
                                    const {
  word_properties->word_category = WordCategory::AGREEMENT_WORD;
  for (unsigned int index = 0; index + 1 < token.size(); index += 4) {
    string case_sign = token.substr(index + 2, 2);
    string gender_sign = token.substr(index, 2);
    RussianChangeableWordParts word_parts(GetWordCase(case_sign),
                                          RussianWordGender::NO_GENDER);
    if (gender_sign == "ч") {
      if (string("жзийклтуфхцч").find(case_sign) != string::npos) {
        word_parts.word_gender = RussianWordGender::PLURAL;
      }
    } else {
      word_parts.word_gender = GetWordGender(case_sign);
    }
    word_properties->changeable_word_parts.insert(word_parts);
  }
}

RussianWordCase PronounsProcessor::GetWordCase(const string& case_sign) const {
  if (string("ажмт").find(case_sign) != string::npos) {
    return RussianWordCase::NOMINATIVE;
  } else if (string("бзнущ").find(case_sign) != string::npos) {
    return RussianWordCase::GENITIVE;
  } else if (string("виофы").find(case_sign) != string::npos) {
    return RussianWordCase::DATIVE;
  } else if (string("гйпхэ").find(case_sign) != string::npos) {
    return RussianWordCase::ACCUSATIVE;
  } else if (string("дкрцю").find(case_sign) != string::npos) {
    return RussianWordCase::INSTRUMENTAL;
  } else if (string("eлсчя").find(case_sign) != string::npos) {
    return RussianWordCase::PREPOSITIONAL;
  }
  return RussianWordCase::NO_CASE;
}

RussianWordGender PronounsProcessor::GetWordGender(const string& gender_sign)
    const {
  if (string("абвгде").find(gender_sign) != string::npos) {
    return RussianWordGender::MASCULINE;
  } else if (string("жзийкл").find(gender_sign) != string::npos) {
    return RussianWordGender::FEMININE;
  } else if (string("мнопрс").find(gender_sign) != string::npos) {
    return RussianWordGender::NEUTER;
  } else if (string("туфхцч").find(gender_sign) != string::npos) {
    return RussianWordGender::PLURAL;
  }
  return RussianWordGender::NO_GENDER;
}
