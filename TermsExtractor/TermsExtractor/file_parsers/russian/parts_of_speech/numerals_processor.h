// Copyright 2012 Michael Nokel
#pragma once

#include "./parts_of_speech_processor.h"
#include "../../../auxiliary.h"
#include <string>

/**
  @brief Class for processing Russian numerals

  This class is derived from base abstract class PartsOfSpeechProcessor and
  should be used for working with numerals.
*/
class NumeralsProcessor : public PartsOfSpeechProcessor {
 public:
  /**
    Processes numeral and stores its cases and genders
    @param token token for processing
    @param[out] word_properties st
  */
  void ProcessWord(const std::string& token,
                   RussianWordProperties* word_properties) const;

 private:
  /**
    Gets numeral's case depending on given its char representation
    @param case_sign string representation of numeral's case
    @return numeral's case
  */
  RussianWordCase GetWordCase(const std::string& case_sign) const;

  /**
    Gets numeral's gender depending on given its char representation
    @param gender_sign string representation of numeral's gender
    @return numeral's gender
  */
  RussianWordGender GetWordGender(const std::string& gender_sign) const;
};
