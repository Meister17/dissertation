// Copyright 2012 Michael Nokel
#include "./numerals_processor.h"
#include "../../../auxiliary.h"
#include <string>

using std::string;

void NumeralsProcessor::ProcessWord(const string& token,
                                    RussianWordProperties* word_properties)
                                    const {
  word_properties->word_category = WordCategory::AGREEMENT_WORD;
  for (unsigned int index = 1; index < token.size(); index += 4) {
    string sign = token.substr(index, 2);
    RussianChangeableWordParts word_parts(GetWordCase(sign),
                                          GetWordGender(sign));
    word_properties->changeable_word_parts.insert(word_parts);
  }
}

RussianWordCase NumeralsProcessor::GetWordCase(const string& case_sign) const {
  if (string("ажмт").find(case_sign) != string::npos) {
    return RussianWordCase::NOMINATIVE;
  } else if (string("бзнущ").find(case_sign) != string::npos) {
    return RussianWordCase::GENITIVE;
  } else if (string("виоф").find(case_sign) != string::npos) {
    return RussianWordCase::DATIVE;
  } else if (string("гйпх").find(case_sign) != string::npos) {
    return RussianWordCase::ACCUSATIVE;
  } else if (string("дкрц").find(case_sign) != string::npos) {
    return RussianWordCase::INSTRUMENTAL;
  } else if (string("елсч").find(case_sign) != string::npos) {
    return RussianWordCase::PREPOSITIONAL;
  }
  return RussianWordCase::NO_CASE;
}

RussianWordGender NumeralsProcessor::GetWordGender(const string& gender_sign)
    const {
  if (string("абвгде").find(gender_sign) != string::npos) {
    return RussianWordGender::MASCULINE;
  } else if (string("жзийкл").find(gender_sign) != string::npos) {
    return RussianWordGender::FEMININE;
  } else if (string("мнопрс").find(gender_sign) != string::npos) {
    return RussianWordGender::NEUTER;
  } else if (string("туфхцч").find(gender_sign) != string::npos) {
    return RussianWordGender::PLURAL;
  }
  return RussianWordGender::NO_GENDER;
}
