// Copyright 2015 Michael Nokel
#pragma once

#include "../auxiliary.h"
#include "../predefined_words_finder.h"
#include <vector>

/**
  @brief Base class for extracting context words for term candidates

  This class should be derived by all subclasses working with particular
  languages that implement context window for term candidates
*/
class ContextWordsExtractor {
 public:
  /**
    Initializes object
    @param[out] predefined_terms_finder pointer to the object for finding
    predefined terms among words in texts
  */
  ContextWordsExtractor(
      const PredefinedWordsFinder& predefined_terms_finder)
      : predefined_terms_finder_(predefined_terms_finder) {
  }

  /**
    Last function to call after processing file has ended. This function
    retrieves all remaining term candidates in the context window
    @return vector containing term candidates that will be removed from the
    context window after calling this function
  */
  virtual std::vector<NearTermsFrequency> RetrieveAllTermCandidates() = 0;

 protected:
  /**
    @brief Structure necessary for calculating NearTermsFreq values of term
    candidates

    This structure contains term candidate and flag indicating whether the term
    candidate is among predefined terms or not
  */
  struct NearTermsData {
    NearTermsData(const std::string& word, bool is_predefined)
        : term_candidate(word),
          is_predefined_term(is_predefined) {
    }

    /** Term candidate */
    std::string term_candidate = "";

    /** Flag indicating whether term candidate is predefined term or not */
    bool is_predefined_term = false;
  };

  /** Size of context terms' window */
  const unsigned int kContextTermsWindowSize_ = 1;

  /** Object for finding predefined terms among words in texts */
  const PredefinedWordsFinder& predefined_terms_finder_;

  /** Number of predefined terms in the context window */
  unsigned int predefined_terms_number_in_window_ = 0;

  /** Number of predefined terms in the previous context window */
  unsigned int predefined_terms_number_in_previous_window_ = 0;
};