// Copyright 2013 Michael Nokel
#pragma once

#include "./context_words_extractor.h"
#include "../candidates_extractor.h"
#include "../../auxiliary.h"
#include "../../predefined_words_finder.h"
#include <map>
#include <string>

/**
  @brief Abstract class for extracting English term candidates

  This class should be derived by each class intended to extract English term
  candidates depending on their length
*/
class EnglishCandidatesExtractor : public CandidatesExtractor {
 public:
  /**
    Initializes object
    @param predefined_words_finder object for finding predefined terms among
    words in texts
  */
  explicit EnglishCandidatesExtractor(
      const PredefinedWordsFinder& predefined_words_finder) {
    context_words_extractor_.reset(
        new EnglishContextWordsExtractor(predefined_words_finder));
  }


  /**
    Adds new extracted English word
    @param word_properties properties of the extracted English word
  */
  virtual void AddNewWord(const EnglishWordProperties& word_properties) = 0;

  /**
    Virtual destructor for the purposes of proper deriving
  */
  virtual ~EnglishCandidatesExtractor() {
  }

 protected:
  /**
    Adds given term candidate to the context window
    @param term_candidate term candidate for adding
  */
  virtual void AddToContextWindow(const std::string& term_candidate) {
    NearTermsFrequency near_terms_frequency =
        static_cast<EnglishContextWordsExtractor*>(
            context_words_extractor_.get())->StoreNewTermCandidate(
                term_candidate);
    if (!near_terms_frequency.term_candidate.empty()) {
      std::string word = near_terms_frequency.term_candidate;
      term_candidates_map_.find(word)->second.near_terms_frequency +=
          near_terms_frequency.near_terms_frequency;
    }
  }

  /**
    Stores new subject in a map of term candidates (increases term and document
    frequencies of the given word as subject)
    @param term_candidate term candidate considered as new subject
  */
  virtual void StoreNewSubject(const std::string& term_candidate) {
    auto found_iterator = term_candidates_map_.find(term_candidate);
    if (found_iterator == term_candidates_map_.end()) {
      throw std::runtime_error("Inconsistent data found");
    }
    ++found_iterator->second.term_frequency_as_subject;
    found_iterator->second.document_frequency_as_subject |= 1;
  }

  /** Candidate for being subject (necessary for finding subjects) */
  std::string subject_candidate_;
};