// Copyright 2013 Michael Nokel
#pragma once

#include "./candidates_extractor.h"
#include "../../auxiliary.h"
#include "../../predefined_words_finder.h"

/**
  @brief Class for extracting two-word English term candidates

  This class is derived from the base abstract class and should be used for
  extracting two-word English term candidates
*/
class EnglishTwoWordTermsExtractor : public EnglishCandidatesExtractor {
 public:
  /**
    Initializes object
    @param[out] predefined_terms_finder pointer to the object for finding
    predefined terms among words in texts
  */
  EnglishTwoWordTermsExtractor(
      const PredefinedWordsFinder& predefined_terms_finder)
      : EnglishCandidatesExtractor(predefined_terms_finder) {
  }

  /**
    Adds new extracted word
    @param word_properties properties of the new extracted word
  */
  void AddNewWord(const EnglishWordProperties& word_properties);

 private:
  /** Flag indicating whether there was a preposition before subject */
  bool was_preposition_before_subject_ = false;

  /** Previous word properties (for creating two-word term candidates) */
  EnglishWordProperties previous_word_properties_;
};
