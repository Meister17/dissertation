// Copyright 2013 Michael Nokel
#pragma once

#include "../context_words_extractor.h"
#include "../../auxiliary.h"
#include <queue>
#include <string>

/**
  @brief Class for extracting context words for English term candidates

  This class implements context window for English term candidates. It adds new
  term candidates and retrieves term candidates that are out of the context
  window after adding new term candidates.
*/
class EnglishContextWordsExtractor : public ContextWordsExtractor {
 public:
  /**
    Initializes object
    @param[out] predefined_terms_finder pointer to the object for finding
    predefined terms among words in texts
  */
  explicit EnglishContextWordsExtractor(
      const PredefinedWordsFinder& predefined_terms_finder)
      : ContextWordsExtractor(predefined_terms_finder) {
  }

  /**
    Stores new term candidate in the context window and returns the word that is
    out of the context window (if any)
    @param term_candidate new term candidate to store in the context window
    @return term candidate that is out of the context window and its
    NearTermsFreq feature value
  */
  NearTermsFrequency StoreNewTermCandidate(const std::string& term_candidate);

  /**
    Last function to call after processing file has ended. This function
    retrieves all remaining term candidates in the context window
    @return vector containing term candidates that will be removed from the
    context window after calling this function
  */
  std::vector<NearTermsFrequency> RetrieveAllTermCandidates();

 private:
  /**
    Erases old term candidate in the context window
    @return NearTermsFreq value of the erased term candidate
  */
  NearTermsFrequency EraseOldTermInWindow();

  /** Queue containing term candidates in the context window */
  std::queue<NearTermsData> context_terms_window_;

  /** Queue containing term candidates in the previous context window */
  std::queue<NearTermsData> previous_context_terms_window_;
};
