// Copyright 2013 Michael Nokel
#pragma once

#include "./candidates_extractor.h"
#include "../../auxiliary.h"

/**
  @brief Class for extracting English single-word term candidates from the
  parsing file

  This class is derived from the base abstract class and should be used for
  extracting English single-word term candidates from the parsing files
*/
class EnglishSingleWordTermsExtractor : public EnglishCandidatesExtractor {
 public:
  /**
    Initializes object
    @param predefined_words_finder object for finding predefined terms among
    words in texts
  */
  explicit EnglishSingleWordTermsExtractor(
      const PredefinedWordsFinder& predefined_words_finder)
      : EnglishCandidatesExtractor(predefined_words_finder) {
  }

  /**
    Adds new extracted English word
    @param word_properties properties of new extracted English word
  */
  void AddNewWord(const EnglishWordProperties& word_properties);
};
