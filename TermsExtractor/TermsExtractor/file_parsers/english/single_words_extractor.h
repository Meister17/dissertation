// Copyright 2015 Michael Nokel
#pragma once

#include "../../auxiliary.h"
#include "../single_words_extractor.h"

/**
  @brief Class for extracting single words from the parsing English file

  This class should be used for extracting single words and their frequencies
  from the parsing English file. Then accumulated information should be merged
  with the previously extracted one from other files
*/
class EnglishSingleWordsExtractor : public SingleWordsExtractor {
 public:
  /**
    Adds new extracted word to the map
    @param word_properties properties of the adding word
  */
  void AddNewWord(const EnglishWordProperties& word_properties) {
    if (word_properties.word_category == WordCategory::NOUN ||
        word_properties.word_category == WordCategory::ADJECTIVE) {
      auto result = single_words_map_.insert({word_properties.word, 1});
      if (!result.second) {
        ++result.first->second;
      }
    }
  }
};