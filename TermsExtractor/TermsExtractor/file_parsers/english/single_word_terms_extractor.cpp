// Copyright 2013 Michael Nokel
#include "./single_word_terms_extractor.h"
#include "../../auxiliary.h"
#include <string>

using std::string;

void EnglishSingleWordTermsExtractor::AddNewWord(
    const EnglishWordProperties& word_properties) {
  bool new_candidate_stored = false;
  if (word_properties.word_category == WordCategory::NOUN) {
    string term_candidate = word_properties.word;
    TermCandidateData term_candidate_data(word_properties.word_category,
                                          word_properties.word_number_in_file,
                                          word_properties.is_capital_word,
                                          word_properties.is_non_initial_word,
                                          word_properties.is_novel_word,
                                          word_properties.is_ambiguous_word,
                                          false);
    InsertNewTermCandidate(term_candidate, term_candidate_data);
    AddToContextWindow(term_candidate);
    if (!word_properties.is_after_preposition) {
      subject_candidate_ = term_candidate;
      new_candidate_stored = true;
    }
  } else if (word_properties.word_category == WordCategory::VERB &&
             !subject_candidate_.empty()) {
    StoreNewSubject(subject_candidate_);
  }
  if (!new_candidate_stored && !subject_candidate_.empty()) {
    subject_candidate_.clear();
  }
}

