// Copyright 2013 Michael Nokel
#pragma once

#include "../../auxiliary.h"
#include "../noun_phrases_extractor.h"
#include <map>
#include <string>
#include <vector>

/**
  @brief Class for extracting English noun phrases from texts

  This class should be used for extracting English noun phrases from texts. It
  accepts only those candidates that satisfy the following pattern:
  ((Adj|Noun)+((Noun-Prep|DT|to|Conj)*(Adj|Noun)*)*Noun and have length of more
  than one word.
*/
class EnglishNounPhrasesExtractor : public NounPhrasesExtractor {
 public:
  /**
    Adds new word to the set of noun phrases candidates and tries to form all
    possible noun phrases
    @param word_properties properties of the adding word
  */
  void AddNewWord(const EnglishWordProperties& word_properties);

 private:
  /**
    Extends candidate for noun phrases by new word
    @param previous_candidate_data data of the previous extending noun phrase
    @param word_properties properties of the new adding word
    @return new_noun_phrases vector where new formed candidates should be placed
  */
  void ExtendNounPhrases(
      const PhraseCandidateData& previous_candidate_data,
      const EnglishWordProperties& word_properties,
      std::vector<PhraseCandidateData>* new_noun_phrases) const;

  /** Vector containing candidates for noun phrases */
  std::vector<PhraseCandidateData> noun_phrases_;
};
