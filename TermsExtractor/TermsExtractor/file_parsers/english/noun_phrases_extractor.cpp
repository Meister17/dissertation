// Copyright 2013 Michael Nokel
#include "./noun_phrases_extractor.h"
#include "../../auxiliary.h"
#include <map>
#include <string>
#include <vector>

using std::map;
using std::string;
using std::vector;

void EnglishNounPhrasesExtractor::AddNewWord(
    const EnglishWordProperties& word_properties) {
  vector<PhraseCandidateData> new_phrases;
  if (word_properties.word_category == WordCategory::ADJECTIVE) {
    for (const auto& phrase: noun_phrases_) {
      if (phrase.length < kLengthForTwoWordTerms_ &&
          (phrase.word_category == WordCategory::ADJECTIVE ||
           phrase.word_category == WordCategory::NOUN ||
           phrase.word_category == WordCategory::PREPOSITION)) {
        ExtendNounPhrases(phrase, word_properties, &new_phrases);
      }
    }
    new_phrases.push_back(PhraseCandidateData(word_properties.word,
                                              word_properties.word_category));
  } else if (word_properties.word_category == WordCategory::NOUN) {
    for (const auto& phrase: noun_phrases_) {
      if (phrase.length < kLengthForTwoWordTerms_) {
        ExtendNounPhrases(phrase, word_properties, &new_phrases);
        if (new_phrases.back().length == kLengthForTwoWordTerms_) {
          InsertNewPhrase(new_phrases.back().noun_phrase,
                          &two_word_phrases_map_);
        } else {
          InsertNewPhrase(new_phrases.back().noun_phrase,
                          &single_word_phrases_map_);
        }
      }
    }
    new_phrases.push_back(PhraseCandidateData(word_properties.word,
                                              word_properties.word_category));
  } else if (word_properties.word_category == WordCategory::PREPOSITION &&
             word_properties.word == "of") {
    for (const auto& phrase: noun_phrases_) {
      if (phrase.length <= kLengthForTwoWordTerms_ &&
          (phrase.word_category == WordCategory::ADJECTIVE ||
           phrase.word_category == WordCategory::NOUN)) {
        ExtendNounPhrases(phrase, word_properties, &new_phrases);
      }
    }
  } else if (word_properties.word_category == WordCategory::DETERMINER) {
    for (const auto& phrase: noun_phrases_) {
      if (phrase.length <= kLengthForTwoWordTerms_ &&
          (phrase.word_category == WordCategory::PREPOSITION)) {
        new_phrases.push_back(phrase);
      }
    }
  }
  noun_phrases_ = new_phrases;
}

void EnglishNounPhrasesExtractor::ExtendNounPhrases(
    const PhraseCandidateData& previous_candidate_data,
    const EnglishWordProperties& word_properties,
    vector<PhraseCandidateData>* new_noun_phrases) const {
  new_noun_phrases->push_back(previous_candidate_data);
  new_noun_phrases->back().noun_phrase += " " + word_properties.word;
  new_noun_phrases->back().word_category = word_properties.word_category;
  if (word_properties.word_category == WordCategory::NOUN ||
      word_properties.word_category == WordCategory::ADJECTIVE) {
    ++new_noun_phrases->back().length;
  }
}
