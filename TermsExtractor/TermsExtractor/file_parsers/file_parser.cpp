// Copyright 2015 Michael Nokel
#include "./file_parser.h"
#include "./candidates_accumulator.h"
#include "./noun_phrases_accumulator.h"
#include "./single_words_accumulator.h"
#include <boost/thread.hpp>
#include <boost/threadpool.hpp>
#include <string>

using boost::threadpool::pool;
using std::string;

void FileParser::PrintTermCandidates(
    const string& single_words_filename,
    const string& single_word_terms_filename,
    const string& single_word_terms_topic_input_filename,
    const string& single_word_terms_topic_vocabulary_filename,
    const string& single_word_phrases_filename,
    const string& two_word_terms_filename,
    const string& two_word_terms_topic_input_filename,
    const string& two_word_terms_topic_vocabulary_filename,
    const string& two_word_phrases_filename,
    unsigned int minimum_term_frequency) {
  pool thread_pool(boost::thread::hardware_concurrency());
  thread_pool.schedule(boost::bind(&SingleWordsAccumulator::PrintSingleWords,
                                   &single_words_accumulator_,
                                   single_words_filename));
  thread_pool.schedule(boost::bind(&CandidatesAccumulator::PrintTermCandidates,
                                   &single_word_terms_accumulator_,
                                   single_word_terms_filename,
                                   minimum_term_frequency));
  thread_pool.schedule(boost::bind(&CandidatesAccumulator::CreateTopicFiles,
                                   &single_word_terms_accumulator_,
                                   single_word_terms_topic_input_filename,
                                   single_word_terms_topic_vocabulary_filename,
                                   minimum_term_frequency));
  thread_pool.schedule(boost::bind(&CandidatesAccumulator::PrintTermCandidates,
                                   &two_word_terms_accumulator_,
                                   two_word_terms_filename,
                                   minimum_term_frequency));
  thread_pool.schedule(boost::bind(&CandidatesAccumulator::CreateTopicFiles,
                                   &two_word_terms_accumulator_,
                                   two_word_terms_topic_input_filename,
                                   two_word_terms_topic_vocabulary_filename,
                                   minimum_term_frequency));
  thread_pool.schedule(boost::bind(
      &NounPhrasesAccumulator::PrintNounPhrases,
      &single_word_noun_phrases_accumulator_,
      single_word_phrases_filename,
      minimum_term_frequency));
  thread_pool.schedule(boost::bind(
      &NounPhrasesAccumulator::PrintNounPhrases,
      &two_word_noun_phrases_accumulator_,
      two_word_phrases_filename,
      minimum_term_frequency));
}