// Copyright Michael Nokel 2015
#pragma once

#include "../auxiliary.h"
#include <boost/thread/mutex.hpp>
#include <map>
#include <string>
#include <vector>

/**
  @brief Class for accumulating and printing extracted noun phrases

  This class should be used for accumulating noun phrases extracted from various
  files in various threads. After what one can use it to print all phrases
  with their term frequencies
*/
class NounPhrasesAccumulator {
 public:
  /**
    Accumulates noun phrases extracted from file
    @param start_iterator start iterator to the map containing extracted noun
    phrases with their term frequencies
    @param end_iterator end iterator to the map containing extracted noun
    phrases with their term frequencies
  */
  void AccumulateNounPhrases(
      std::map<std::string, unsigned int>::const_iterator start_iterator,
      std::map<std::string, unsigned int>::const_iterator end_iterator);

   /**
    Prints extracted noun phrases for term candidates and their frequencies
    @param phrases_filename file for printing noun phrases for term candidates
    @param minimum_term_frequency minimum frequency of phrases to be taken into
    account
    @throw std::runtime_error in case of any occurred error
  */
  void PrintNounPhrases(const std::string& phrases_filename,
                        unsigned int minimum_term_frequency) const;

 private:
  /** Total number of extracted noun phrases */
  unsigned int total_phrases_number_ = 0;

  /** Mutex for synchronization  */
  boost::mutex mutex_;

  /** Map containing extracted noun phrases and their frequencies */
  std::map<std::string, unsigned int> phrases_map_;

 private:
  /**
    Sorts noun phrases by their term frequencies
    @param phrases_map map containing noun phrases and their frequencies
    @return vector of noun phrases sorted by their term frequencies
  */
  std::vector<std::pair<std::string, unsigned int> > SortByTermFrequencies(
      const std::map<std::string, unsigned int>& phrases_map) const;
};