// Copyright 2015 Michael Nokel
#include "./single_words_accumulator.h"
#include <boost/thread/mutex.hpp>
#include <fstream>
#include <map>
#include <stdexcept>
#include <string>

using std::ofstream;
using std::map;
using std::runtime_error;
using std::string;

void SingleWordsAccumulator::AccumulateSingleWords(
    map<string, unsigned int>::const_iterator start_iterator,
    map<string, unsigned int>::const_iterator end_iterator) {
  if (start_iterator != end_iterator) {
    boost::mutex::scoped_lock lock(mutex_);
    for (auto iterator = start_iterator; iterator != end_iterator; ++iterator) {
      total_number_single_words_ += iterator->second;
      auto result = single_words_map_.insert(*iterator);
      if (!result.second) {
        result.first->second += iterator->second;
      }
    }
  }
}

void SingleWordsAccumulator::PrintSingleWords(const string& filename) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to print single words");
  }
  file << total_number_single_words_ << "\n";
  for (const auto& element : single_words_map_) {
    file << element.first << "\t" << element.second << "\n";
  }
  file.close();
}