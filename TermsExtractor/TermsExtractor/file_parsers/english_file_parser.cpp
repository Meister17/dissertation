// Copyright 2015 Michael Nokel
#include "./english_file_parser.h"
#include "./english/single_words_extractor.h"
#include "./english/single_word_terms_extractor.h"
#include "./english/two_word_terms_extractor.h"
#include "./english/noun_phrases_extractor.h"
#include "../auxiliary.h"
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <cctype>
#include <fstream>
#include <stdexcept>
#include <string>
#include <vector>

using boost::is_any_of;
using boost::split;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::string;
using std::vector;

EnglishFileParser::EnglishFileParser(
    const PredefinedWordsFinder& predefined_terms_finder,
    const PredefinedWordsFinder& stopwords_finder)
    : FileParser(predefined_terms_finder,
                 stopwords_finder) {
}

void EnglishFileParser::ParseFile(unsigned int file_number,
                                  const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse file");
  }
  EnglishSingleWordTermsExtractor single_word_terms_extractor(
      predefined_terms_finder_);
  EnglishTwoWordTermsExtractor two_word_terms_extractor(
      predefined_terms_finder_);
  EnglishNounPhrasesExtractor noun_phrases_extractor;
  EnglishSingleWordsExtractor single_words_extractor;
  WordProcessingData word_processing_data;
  unsigned int token_number_in_file = 0;
  unsigned int word_number_in_file = 0;
  while (!file.eof()) {
    string line;
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    if (line.empty()) {
      continue;
    }
    vector<string> tokens;
    split(tokens, line, is_any_of("\t"));
    if (tokens.size() < 3) {
      continue;
    }
    EnglishWordProperties word_properties;
    word_properties.token_number_in_file = token_number_in_file;
    word_properties.word_number_in_file = word_number_in_file;
    if (isupper(tokens[0][0]) || word_processing_data.was_end_of_sentence) {
      word_properties.is_capital_word = true;
      word_properties.is_non_initial_word =
          !word_processing_data.was_end_of_sentence;
    }
    word_processing_data.word_extracted = true;
    for (const auto letter: tokens[1]) {
      if (!isalpha(letter)) {
        word_processing_data.word_extracted = false;
        break;
      }
      word_properties.word.push_back(tolower(letter));
    }
    if (word_processing_data.word_extracted &&
        word_properties.word.size() <= 1) {
      word_processing_data.word_extracted = false;
    }
    string term_category = tokens[2];
    DetermineTermCategory(word_properties.word,
                          term_category,
                          word_properties.word_category,
                          &word_processing_data);
    if (word_processing_data.word_extracted) {
      ++word_number_in_file;
      ++token_number_in_file;
    } else if (word_processing_data.token_extracted) {
      ++token_number_in_file;
    }
    word_properties.is_after_punctuation_divider =
        word_processing_data.was_punctuation_divider;
    word_properties.is_after_preposition = word_processing_data.was_preposition;
    char is_novel, is_ambiguous;
    if (tokens.size() < 4) {
      is_novel = true;
    } else {
      is_novel = tokens[3][0];
    }
    if (tokens.size() < 5) {
      is_ambiguous = true;
    } else {
      is_ambiguous = tokens[4][0];
    }
    word_properties.is_novel_word = is_novel == '1';
    word_properties.is_ambiguous_word = is_ambiguous == '1';
    single_word_terms_extractor.AddNewWord(word_properties);
    two_word_terms_extractor.AddNewWord(word_properties);
    noun_phrases_extractor.AddNewWord(word_properties);
    single_words_extractor.AddNewWord(word_properties);
  }
  file.close();
  single_word_terms_extractor.ProcessLastWords();
  two_word_terms_extractor.ProcessLastWords();
  single_word_terms_accumulator_.AccumulateTermCandidates(
      file_number,
      single_word_terms_extractor.GetStartIterator(),
      single_word_terms_extractor.GetEndIterator());
  two_word_terms_accumulator_.AccumulateTermCandidates(
      file_number,
      two_word_terms_extractor.GetStartIterator(),
      two_word_terms_extractor.GetEndIterator());
  single_word_noun_phrases_accumulator_.AccumulateNounPhrases(
      noun_phrases_extractor.GetStartSingleWordIterator(),
      noun_phrases_extractor.GetEndSingleWordIterator());
  two_word_noun_phrases_accumulator_.AccumulateNounPhrases(
      noun_phrases_extractor.GetStartTwoWordIterator(),
      noun_phrases_extractor.GetEndTwoWordIterator());
  single_words_accumulator_.AccumulateSingleWords(
      single_words_extractor.GetStartIterator(),
      single_words_extractor.GetEndIterator());
}

void EnglishFileParser::DetermineTermCategory(
    const string& word,
    const string& term_category,
    WordCategory& word_category,
    WordProcessingData* word_processing_data) const {
  word_processing_data->was_preposition =
      word_processing_data->was_punctuation_divider =
      word_processing_data->was_end_of_sentence = false;
  word_processing_data->token_extracted = true;
  if (word_processing_data->word_extracted &&
      (term_category == "NN" || term_category == "NNS" ||
       term_category == "NNP" || term_category == "NNPS") &&
      (word == "eu" || (word.size() > 2 &&
                        !stopwords_finder_.IsPredefined(word)))) {
    word_category = WordCategory::NOUN;
  } else if (word_processing_data->word_extracted && term_category == "JJ" &&
             (word == "eu" || (word.size() > 2 &&
                               !stopwords_finder_.IsPredefined(word)))) {
    word_category = WordCategory::ADJECTIVE;
  } else if (word_processing_data->word_extracted && term_category == "IN") {
    if (word == "of") {
      word_processing_data->word_extracted =
          word_processing_data->token_extracted = false;
    }
    word_category = WordCategory::PREPOSITION;
    word_processing_data->was_preposition = true;
  } else if (word_processing_data->word_extracted &&
             (term_category == "MD" || term_category == "VB" ||
              term_category == "VBD" || term_category == "VBG" ||
              term_category == "VBN" || term_category == "VBP" ||
              term_category == "VBZ")) {
    word_category = WordCategory::VERB;
  } else if (term_category == "DT" &&
             ((!word_processing_data->word_extracted && word == "a") ||
              (word_processing_data->word_extracted && (word == "an" ||
                                                        word == "the")))) {
    word_processing_data->word_extracted =
        word_processing_data->token_extracted = false;
    word_category = WordCategory::DETERMINER;
  } else if (term_category == "." || term_category == "-LRB-" ||
             term_category == "-RRB-" || term_category == "``" ||
             term_category == "''" || term_category == "-LCB-" ||
             term_category == "-RCB-") {
    word_category = WordCategory::END_OF_SENTENCE;
    word_processing_data->token_extracted = false;
    word_processing_data->was_end_of_sentence =
        word_processing_data->was_punctuation_divider = true;
  } else if (term_category == "," || term_category == ":" ||
             term_category == "$" || term_category == "#") {
    word_category = WordCategory::OTHER_CATEGORY;
    word_processing_data->token_extracted = false;
    word_processing_data->was_punctuation_divider = true;
  } else {
    word_category = WordCategory::OTHER_CATEGORY;
  }
}
