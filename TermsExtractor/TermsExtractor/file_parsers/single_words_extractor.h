// Copyright 2015 Michael Nokel
#pragma once

#include "../auxiliary.h"
#include <map>
#include <string>

/**
  @brief Base class for extracting single words from the parsing file

  This class should be used for extracting single words from the parsing file.
  It should be derived by all subclasses working with particular language.
*/
class SingleWordsExtractor {
 public:
  /**
    Returns iterator to the beginning of the map containing single words along
    with their frequencies
    @return iterator to the beginning of the map containing single words along
    with their frequencies
  */
  virtual std::map<std::string, unsigned int>::const_iterator GetStartIterator()
      const {
    return single_words_map_.cbegin();
  }

  /**
    Returns iterator to the end of the map containing single words along with
    their frequencies
    @return iterator to the end of the map containing single words along with
    their frequencies
  */
  virtual std::map<std::string, unsigned int>::const_iterator GetEndIterator()
      const {
    return single_words_map_.cend();
  }

 protected:
  /** Map containing single words and their frequencies */
  std::map<std::string, unsigned int> single_words_map_;
};