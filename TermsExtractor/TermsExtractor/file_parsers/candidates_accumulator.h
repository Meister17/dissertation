// Copyright Michael Nokel 2015
#pragma once

#include "../auxiliary.h"
#include <boost/thread/mutex.hpp>
#include <map>
#include <string>
#include <vector>

/**
  @brief Class for accumulating candidates from various files and printing them

  This class should be used for accumulating term candidates extracted from
  various files in various threads. After what one can print all desired result
  files
*/
class CandidatesAccumulator {
 public:
  /**
    Prepares for processing given number of files
    @param total_number_files total number of files for processing
  */
  void PrepareForFiles(unsigned int total_number_files) {
    topic_container_.resize(total_number_files,
                            std::map<std::string, unsigned int>());
  }

  /**
    Accumulates candidates from the parsed file
    @param file_number number of processing file
    @param start_iterator start iterator to the map containing extracted term
    candidates and necessary data about them
    @param end_iterator end iterator to the map containing extracted term
    candidates and necessary data about them
  */
  void AccumulateTermCandidates(
      unsigned int file_number,
      std::map<std::string, TermCandidateData>::const_iterator start_iterator,
      std::map<std::string, TermCandidateData>::const_iterator end_iterator);

  /**
    Prints extracted term candidates in the decreasing order of their term
    frequencies
    @param filename file for printing term candidates and some data about them
    @param minimum_term_frequency minimum term frequency of term candidates to
    be taken into account
    @throw std::runtime_error in case of any occurred error
  */
  void PrintTermCandidates(const std::string& filename,
                           unsigned int minimum_term_frequency) const;

  /**
    Creates input and vocabulary files for topic models
    @param topic_input_filename file for printing input for topic models
    @param topic_vocabulary_filename file for printing vocabulary for topic
    models
    @throw std::runtime_error in case of any occurred error
  */
  void CreateTopicFiles(const std::string& topic_input_filename,
                        const std::string& topic_vocabulary_filename,
                        unsigned int minimum_term_frequency) const;

 private:
  /** Total number of candidates in the corpus */
  unsigned int total_candidates_number_ = 0;

  /** Total number of documents in the corpus */
  unsigned int total_documents_number_ = 0;

  /** Mutex for synchronization */
  boost::mutex mutex_;

  /** Container for data required for topic models */
  std::vector<std::map<std::string, unsigned int>> topic_container_;

  /** Map containing term candidates and necessary information about them
      extracted from all parsed files */
  std::map<std::string, TermCandidateData> term_candidates_map_;

 private:
  /**
    Sorts term candidates by their term frequencies
    @param candidates_map map containing term candidates and some data about
    them
    @return vector of term candidates sorted by term frequencies
  */
  std::vector<std::pair<std::string, TermCandidateData>> SortByTermFrequencies(
      const std::map<std::string, TermCandidateData>& candidates_map) const;

  /**
    Returns char representing term's category: 'A' for Adjective, 'N' for Noun,
    and 'B' for both types
    @param term_category category of the term
    @return char representing term's category
    @throw std::runtime_error in case of wrong term category
  */
  char GetTermCategory(const TermCategory& term_category) const;

  /**
    Calculates probabilistic term scores for the given term candidate
    @param term_frequencies vector containing frequencies of term candidate
    @param document_sizes vector containing sizes of documents in which
    term occurs at least once
    @param term_frequency total term frequency of the given candidate
    @return structure containing term frequencies and scores
  */
  ProbabilisticData CalculateProbabilisticData(
      const std::vector<unsigned int>& term_frequencies,
      const std::vector<unsigned int>& document_sizes,
      unsigned int term_frequency) const;

  /**
    Returns words with term frequencies greater or equal the given threshold
    @param minimum_term_frequency minimum term frequency that should be taken
    into account
    @return vector containing words in texts with such term frequencies
  */
  std::vector<std::string> GetWordsAboveThreshold(
      unsigned int minimum_term_frequency) const;

  /**
    Prints topic vocabulary
    @param filename name of file for printing topic model's vocabulary
    @param words_above_threshold vector containing words that should be printed
    @throw std::runtime_error in case of any occurred errors
  */
  void PrintTopicVocabulary(
      const std::string& filename,
      const std::vector<std::string>& words_above_threshold) const;

  /**
    Prints topic model's input to the given file
    @param filename name of file for printing input for topic models
    @param words_above_threshold vector containing words that should be printed
    @throw std::runtime_error in case of any occurred errors
  */
  void PrintTopicInput(
      const std::string& filename,
      const std::vector<std::string>& words_above_threshold) const;

};