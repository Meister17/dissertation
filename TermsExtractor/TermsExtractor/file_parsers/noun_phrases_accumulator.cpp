// Copyright Michael Nokel 2015
#include "./noun_phrases_accumulator.h"
#include "../auxiliary.h"
#include <boost/thread/mutex.hpp>
#include <fstream>
#include <map>
#include <stdexcept>
#include <string>
#include <vector>

using std::ofstream;
using std::map;
using std::pair;
using std::runtime_error;
using std::string;
using std::vector;

void NounPhrasesAccumulator::AccumulateNounPhrases(
    map<string, unsigned int>::const_iterator start_iterator,
    map<string, unsigned int>::const_iterator end_iterator) {
  boost::mutex::scoped_lock lock(mutex_);
  for (auto iterator = start_iterator; iterator != end_iterator; ++iterator) {
    total_phrases_number_ += iterator->second;
    auto inserted_result = phrases_map_.insert({iterator->first,
                                                iterator->second});
    if (!inserted_result.second) {
      inserted_result.first->second += iterator->second;
    }
  }
}

void NounPhrasesAccumulator::PrintNounPhrases(
    const string& filename,
    unsigned int minimum_term_frequency) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to print extracted phrases");
  }
  auto noun_phrases_vector = SortByTermFrequencies(phrases_map_);
  file << total_phrases_number_ << "\n";
  for (const auto& element: noun_phrases_vector) {
    if (element.second < minimum_term_frequency) {
      break;
    }
    file << element.first << "\t" << element.second << "\n";
  }
  file.close();
}

vector<pair<string, unsigned int>>
    NounPhrasesAccumulator::SortByTermFrequencies(
        const map<string, unsigned int>& phrases_map) const {
  vector<pair<string, unsigned int> > phrases_vector(phrases_map.begin(),
                                                     phrases_map.end());
  sort(phrases_vector.begin(),
       phrases_vector.end(),
       [&](const pair<string, unsigned int>& first,
           const pair<string, unsigned int>& second)
           {return first.second > second.second;});
  return phrases_vector;
}
