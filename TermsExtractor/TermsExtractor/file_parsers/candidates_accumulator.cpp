// Copyright Michael Nokel 2015
#include "./candidates_accumulator.h"
#include "../auxiliary.h"
#include <boost/thread/mutex.hpp>
#include <algorithm>
#include <fstream>
#include <map>
#include <stdexcept>
#include <string>
#include <vector>

using std::map;
using std::max;
using std::ofstream;
using std::pair;
using std::runtime_error;
using std::sort;
using std::string;
using std::vector;

void CandidatesAccumulator::AccumulateTermCandidates(
    unsigned int file_number,
    map<string, TermCandidateData>::const_iterator start_iterator,
    map<string, TermCandidateData>::const_iterator end_iterator) {
  boost::mutex::scoped_lock lock(mutex_);
  if (start_iterator != end_iterator) {
    ++total_documents_number_;
    for (auto iterator = start_iterator; iterator != end_iterator; ++iterator) {
      total_candidates_number_ += iterator->second.term_frequency;
      auto inserted_result = term_candidates_map_.insert(*iterator);
      if (!inserted_result.second) {
        if (inserted_result.first->second.term_category !=
            iterator->second.term_category) {
          inserted_result.first->second.term_category =
              TermCategory::ADJECTIVE_AND_NOUN;
        }
        inserted_result.first->second.is_novel_candidate |=
            iterator->second.is_novel_candidate;
        inserted_result.first->second.is_ambiguous_candidate |=
            iterator->second.is_ambiguous_candidate;
        inserted_result.first->second.first_occurrence_position +=
            iterator->second.first_occurrence_position;
        inserted_result.first->second.near_terms_frequency +=
            iterator->second.near_terms_frequency;
        inserted_result.first->second.term_frequency +=
            iterator->second.term_frequency;
        inserted_result.first->second.term_frequency_as_capital_word +=
            iterator->second.term_frequency_as_capital_word;
        inserted_result.first->second.term_frequency_as_non_initial_word +=
            iterator->second.term_frequency_as_non_initial_word;
        inserted_result.first->second.term_frequency_as_subject +=
            iterator->second.term_frequency_as_subject;
        inserted_result.first->second.document_frequency +=
            iterator->second.document_frequency;
        inserted_result.first->second.document_frequency_as_capital_word +=
            iterator->second.document_frequency_as_capital_word;
        inserted_result.first->second.document_frequency_as_non_initial_word +=
            iterator->second.document_frequency_as_non_initial_word;
        inserted_result.first->second.document_frequency_as_subject +=
            iterator->second.document_frequency_as_subject;
        inserted_result.first->second.domain_consensus +=
            iterator->second.domain_consensus;
        inserted_result.first->second.domain_consensus_as_capital_word +=
            iterator->second.domain_consensus_as_capital_word;
        inserted_result.first->second.domain_consensus_as_non_initial_word +=
            iterator->second.domain_consensus_as_non_initial_word;
        inserted_result.first->second.domain_consensus_as_subject +=
            iterator->second.domain_consensus_as_subject;
        inserted_result.first->second.term_candidate_frequencies.push_back(
            iterator->second.term_candidate_frequencies.front());
        inserted_result.first->second.document_sizes.push_back(
            iterator->second.document_sizes.front());
      }
      topic_container_[file_number][iterator->first] =
          iterator->second.term_frequency;
    }
  }
}

void CandidatesAccumulator::PrintTermCandidates(
    const string& filename,
    unsigned int minimum_term_frequency) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to print term candidates");
  }
  auto term_candidates_vector = SortByTermFrequencies(term_candidates_map_);
  file << total_documents_number_ << "\t" << total_candidates_number_ << "\n";
  for (const auto& term_candidate: term_candidates_vector) {
    if (term_candidate.second.term_frequency < minimum_term_frequency) {
      break;
    }
    char is_novel_candidate = '0';
    char is_ambiguous_candidate = '0';
    if (term_candidate.second.is_novel_candidate) {
      is_novel_candidate = '1';
    }
    if (term_candidate.second.is_ambiguous_candidate) {
      is_ambiguous_candidate = '1';
    }
    char term_category = GetTermCategory(term_candidate.second.term_category);
    file << term_candidate.first << "\t" << term_category << "\t" <<
        is_novel_candidate << "\t" << is_ambiguous_candidate << "\t" <<
        static_cast<double>(term_candidate.second.first_occurrence_position) /
        static_cast<double>(term_candidate.second.document_frequency) <<
        "\t" << term_candidate.second.near_terms_frequency << "\t" <<
        term_candidate.second.term_frequency << "\t" <<
        term_candidate.second.term_frequency_as_capital_word << "\t" <<
        term_candidate.second.term_frequency_as_non_initial_word << "\t" <<
        term_candidate.second.term_frequency_as_subject << "\t" <<
        term_candidate.second.document_frequency << "\t" <<
        term_candidate.second.document_frequency_as_capital_word << "\t" <<
        term_candidate.second.document_frequency_as_non_initial_word <<
        "\t" << term_candidate.second.document_frequency_as_subject <<
        "\t" << term_candidate.second.domain_consensus << "\t" <<
        term_candidate.second.domain_consensus_as_capital_word << "\t" <<
        term_candidate.second.domain_consensus_as_non_initial_word << "\t" <<
        term_candidate.second.domain_consensus_as_subject << "\t";
    ProbabilisticData probabilistic_data = CalculateProbabilisticData(
        term_candidate.second.term_candidate_frequencies,
        term_candidate.second.document_sizes,
        term_candidate.second.term_frequency);
    file << probabilistic_data.term_frequency << "\t" <<
        probabilistic_data.maximum_term_frequency << "\t" <<
        probabilistic_data.term_score << "\t" <<
        probabilistic_data.maximum_term_score << "\t" <<
        probabilistic_data.term_contribution << "\t" <<
        probabilistic_data.term_variance << "\t" <<
        probabilistic_data.term_variance_quality << "\n";
  }
  file.close();
}

void CandidatesAccumulator::CreateTopicFiles(
    const string& topic_input_filename,
    const string& topic_vocabulary_filename,
    unsigned int minimum_term_frequency) const {
  vector<string> words_above_threshold = GetWordsAboveThreshold(
      minimum_term_frequency);
  PrintTopicVocabulary(topic_vocabulary_filename, words_above_threshold);
  PrintTopicInput(topic_input_filename, words_above_threshold);
}

vector<pair<string, TermCandidateData> >
    CandidatesAccumulator::SortByTermFrequencies(
        const map<string, TermCandidateData>& candidates_map) const {
  vector<pair<string, TermCandidateData> > term_candidates_vector(
      candidates_map.begin(),
      candidates_map.end());
  sort(term_candidates_vector.begin(),
       term_candidates_vector.end(),
       [&](const pair<string, TermCandidateData>& first,
           const pair<string, TermCandidateData>& second) {
          if (first.second.term_frequency == second.second.term_frequency) {
            return first.first < second.first;
          }
          return first.second.term_frequency > second.second.term_frequency;
      });
  return term_candidates_vector;
}

char CandidatesAccumulator::GetTermCategory(const TermCategory& term_category)
    const {
  switch (term_category) {
    case TermCategory::NOUN:
      return 'N';
    case TermCategory::ADJECTIVE:
      return 'A';
    case TermCategory::ADJECTIVE_AND_NOUN:
      return 'B';
    case TermCategory::NO_TERM_CATEGORY: default:
      throw runtime_error("Wrong category of the term candidate");
  }
}

ProbabilisticData CandidatesAccumulator::CalculateProbabilisticData(
    const vector<unsigned int>& term_frequencies,
    const vector<unsigned int>& document_sizes,
    unsigned int term_frequency) const {
  ProbabilisticData probabilistic_data;
  double term_score = 1.0;
  for (unsigned int index = 0; index < term_frequencies.size(); ++index) {
    double probability = static_cast<double>(term_frequencies[index]) /
        static_cast<double>(document_sizes[index]);
    probabilistic_data.term_frequency += probability;
    probabilistic_data.maximum_term_frequency = max(
        probabilistic_data.maximum_term_frequency,
        probability);
    term_score *= pow(probability,
                      1.0 / static_cast<double>(term_frequencies.size()));
  }
  for (unsigned int index = 0; index < term_frequencies.size(); ++index) {
    double probability =
        static_cast<double>(term_frequencies[index]) /
        static_cast<double>(document_sizes[index]);
    double current_term_score = probability * log(probability / term_score);
    probabilistic_data.term_score += current_term_score;
    probabilistic_data.maximum_term_score = max(
        probabilistic_data.maximum_term_score,
        current_term_score);
  }
  double average_frequency =
      term_frequency / static_cast<double>(term_frequencies.size());
  for (unsigned int first_index = 0; first_index < term_frequencies.size();
       ++first_index) {
    probabilistic_data.term_variance_quality +=
        pow(static_cast<double>(term_frequencies[first_index]), 2.0);
    for (unsigned int second_index = first_index + 1;
         second_index < term_frequencies.size(); ++second_index) {
      probabilistic_data.term_contribution += term_frequencies[first_index] *
          term_frequencies[second_index];
    }
  }
  double value = pow(term_frequency, 2.0) /
      static_cast<double>(term_frequencies.size());
  probabilistic_data.term_variance_quality -= value;
  probabilistic_data.term_variance = probabilistic_data.term_variance_quality -
      value + average_frequency * average_frequency;
  return probabilistic_data;
}

vector<string> CandidatesAccumulator::GetWordsAboveThreshold(
    unsigned int minimum_term_frequency) const {
  vector<string> words_above_threshold;
  for (const auto& element: term_candidates_map_) {
    if (element.second.term_frequency >= minimum_term_frequency) {
      words_above_threshold.push_back(element.first);
    }
  }
  sort(words_above_threshold.begin(), words_above_threshold.end());
  return words_above_threshold;
}

void CandidatesAccumulator::PrintTopicVocabulary(
    const string& filename,
    const vector<string>& words_above_threshold) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to open file for printing topic vocabulary");
  }
  for (const auto& word: words_above_threshold) {
    file << word << "\t" << term_candidates_map_.at(word).term_frequency;
    if (count(word.begin(), word.end(), ' ') > 0) {
      file << "\tB\n";
    } else {
      switch (term_candidates_map_.at(word).term_category) {
        case TermCategory::NOUN:
          file << "\tN\n";
          break;
        case TermCategory::ADJECTIVE:
          file << "\tA\n";
          break;
        case TermCategory::ADJECTIVE_AND_NOUN:
          file << "\tA\tN\n";
          break;
        case TermCategory::NO_TERM_CATEGORY: default:
          throw runtime_error("Failed to print topic vocabulary");
      }
    }
  }
  file.close();
}

void CandidatesAccumulator::PrintTopicInput(
    const string& filename,
    const vector<string>& words_above_threshold) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to open file for printing topic input data");
  }
  for (const auto& document: topic_container_) {
    if (!document.empty()) {
      map<unsigned int, unsigned int> terms_in_document;
      for (const auto& element: document) {
        const auto found_iterator = lower_bound(words_above_threshold.begin(),
                                                words_above_threshold.end(),
                                                element.first);
        if (found_iterator != words_above_threshold.end() &&
            *found_iterator == element.first) {
          terms_in_document.insert(
              {found_iterator - words_above_threshold.begin(), element.second});
        }
      }
      for (const auto& element: terms_in_document) {
        file << element.first << ":" << element.second << "\t";
      }
      file << "\n";
    }
  }
  file.close();
}