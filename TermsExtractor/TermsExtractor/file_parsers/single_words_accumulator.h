// Copyright 2015 Michael Nokel
#pragma once

#include <boost/thread/mutex.hpp>
#include <map>
#include <string>

/**
  @brief Class for accumulating single words and their frequencies

  This class should be used for accumulating single words and their frequencies
  extracted from various files. Then one can print accumulated information into
  the given file.
*/
class SingleWordsAccumulator {
 public:
  /**
    Accumulates previously extracted single words and their frequencies
    @param start_iterator iterator to the beginning of the accumulating map
    @param end_iterator iterator to the end of the accumulating map
  */
  void AccumulateSingleWords(
      std::map<std::string, unsigned int>::const_iterator start_iterator,
      std::map<std::string, unsigned int>::const_iterator end_iterator);

  /**
    Prints extracted single words along with their frequencies in the given file
    @param filename file where extracted single words should be printed
    @throw std::runtime_error in case of any occurred errors
  */
  void PrintSingleWords(const std::string& filename) const;

 private:
  /** Mutex for synchronization */
  boost::mutex mutex_;

  /** Total number of extracted single words */
  unsigned int total_number_single_words_ = 0;

  /** Map containing single words along with their frequencies */
  std::map<std::string, unsigned int> single_words_map_;
};