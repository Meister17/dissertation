// Copyright 2013 Michael Nokel
#include "./auxiliary.h"
#include "./file_parsers/russian_file_parser.h"
#include "./file_parsers/english_file_parser.h"
#include "./terms_extractor.h"
#include <boost/filesystem.hpp>
#include <boost/thread.hpp>
#include <boost/threadpool.hpp>
#include <iostream>
#include <stdexcept>
#include <string>

using boost::filesystem::directory_iterator;
using boost::filesystem::is_regular_file;
using boost::threadpool::pool;
using std::cout;
using std::runtime_error;
using std::string;

void TermsExtractor::Initialize(const string& predefined_terms_filename,
                                const string& prepositions_filename,
                                const string& stopwords_filename) {
  cout << "Initializing\n";
  predefined_words_finder_.ParseFile(predefined_terms_filename);
  stopwords_finder_.ParseFile(stopwords_filename);
  switch (kLanguage_) {
    case Language::RUSSIAN:
      prepositions_processor_.ParseFile(prepositions_filename);
      file_parser_.reset(new RussianFileParser(predefined_words_finder_,
                                               stopwords_finder_,
                                               prepositions_processor_));
      break;
    case Language::ENGLISH:
      file_parser_.reset(new EnglishFileParser(predefined_words_finder_,
                                               stopwords_finder_));
      break;
    case Language::NO_LANGUAGE: default:
      throw std::runtime_error("Wrong language was specified");
  }
}

void TermsExtractor::ExtractTerms(const string& source_directory_name) {
  cout << "Extracting term candidates\n";
  unsigned int file_number = 0;
  for (directory_iterator iterator(source_directory_name); iterator !=
       directory_iterator(); ++iterator) {
    if (is_regular_file(iterator->status())) {
      ++file_number;
    }
  }
  file_parser_->PrepareForFiles(file_number);
  pool thread_pool(boost::thread::hardware_concurrency());
  file_number = 0;
  for (directory_iterator iterator(source_directory_name); iterator !=
       directory_iterator(); ++iterator) {
    if (is_regular_file(iterator->status())) {
      thread_pool.schedule(boost::bind(&FileParser::ParseFile,
                                       file_parser_.get(),
                                       file_number,
                                       iterator->path().string()));
      ++file_number;
    }
  }
}

void TermsExtractor::PrintTermCandidates(
    const string& single_words_filename,
    const string& single_word_terms_filename,
    const string& single_word_terms_topic_input_filename,
    const string& single_word_terms_topic_vocabulary_filename,
    const string& single_word_phrases_filename,
    const string& two_word_terms_filename,
    const string& two_word_terms_topic_input_filename,
    const string& two_word_terms_topic_vocabulary_filename,
    const string& two_word_phrases_filename) {
  cout << "Printing extracted term candidates\n";
  file_parser_->PrintTermCandidates(single_words_filename,
                                    single_word_terms_filename,
                                    single_word_terms_topic_input_filename,
                                    single_word_terms_topic_vocabulary_filename,
                                    single_word_phrases_filename,
                                    two_word_terms_filename,
                                    two_word_terms_topic_input_filename,
                                    two_word_terms_topic_vocabulary_filename,
                                    two_word_phrases_filename,
                                    kMinimumTermFrequency_);
}
