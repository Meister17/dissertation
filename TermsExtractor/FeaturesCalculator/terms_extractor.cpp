// Copyright 2012 Michael Nokel
#include "./terms_extractor.h"
#include "./auxiliary.h"
#include "./candidates_analyzers/candidates_analyzer.h"
#include <boost/filesystem.hpp>
#include <iostream>
#include <string>

using boost::filesystem::path;
using std::cout;
using std::string;

TermsExtractor::TermsExtractor(
    const std::string& average_precision_directory_name,
    unsigned int terms_number_threshold)
    : kTermsNumberThreshold_(terms_number_threshold),
      single_word_terms_extractor_(
          average_precision_directory_name + "single_words.txt",
          results_analyzer_,
          single_words_analyzer_),
      two_word_terms_extractor_(
          average_precision_directory_name + "two_words.txt",
          results_analyzer_,
          single_words_analyzer_) {
}

void TermsExtractor::Initialize(
    const string& real_terms_filename,
    const string& single_words_filename,
    const string& single_word_terms_filename,
    const string& single_word_topic_filename,
    const string& single_word_phrases_filename,
    const string& reference_single_words_filename,
    const string& two_word_terms_filename,
    const string& two_word_topic_filename,
    const string& two_word_phrases_filename,
    const string& reference_two_words_filename) {
  cout << "Initializing\n";
  results_analyzer_.ParseFile(real_terms_filename);
  single_words_analyzer_.ParseFile(single_words_filename);
  single_word_terms_extractor_.ParseFiles(single_word_terms_filename,
                                          single_word_topic_filename,
                                          reference_single_words_filename,
                                          single_word_phrases_filename,
                                          kTermsNumberThreshold_);
  two_word_terms_extractor_.ParseFiles(two_word_terms_filename,
                                       two_word_topic_filename,
                                       reference_two_words_filename,
                                       two_word_phrases_filename,
                                       kTermsNumberThreshold_);
}

void TermsExtractor::ExtractTerms(const string& destination_directory_name) {
  cout << "Applying various features to term candidates\n";
  path slash("/");
  string path_separator = slash.make_preferred().native();
  single_word_terms_extractor_.ExtractTerms(
      destination_directory_name + "single_word" + path_separator);
  two_word_terms_extractor_.ExtractTerms(
      destination_directory_name + "two_word" + path_separator);
}
