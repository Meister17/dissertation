// Copyright 2011 Michael Nokel
#include "./single_word_terms_extractor.h"
#include "../results_analyzer.h"
#include <algorithm>
#include <cmath>
#include <map>
#include <string>
#include <vector>

using std::map;
using std::max;
using std::string;
using std::vector;

void SingleWordTermsExtractor::ApplyNCValue(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    vector<string> context_words = phrases_analyzer_.GetContextWords(
        iterator->term_candidate);
    double cweight = 1.0;
    for (const auto& context_word : context_words) {
      unsigned int context_word_frequency = single_words_analyzer_.GetFrequency(
          context_word);
      if (context_word_frequency) {
        cweight += 0.5 *
            (static_cast<double>(phrases_analyzer_.GetContextWordsNumber(
                context_word)) / static_cast<double>(
                    term_candidates_analyzer_->total_number_words())
           + static_cast<double>(
                phrases_analyzer_.GetContextWordsFrequenciesSum(
                    context_word)) /
             static_cast<double>(context_word_frequency));
      }
    }
    double weight_value = CalculateCValueForTerm(iterator->term_candidate,
                                                 iterator->term_frequency) *
        cweight /
        static_cast<double>(term_candidates_analyzer_->total_number_words());
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "NC-value", weights);
}

void SingleWordTermsExtractor::ApplyModifiedGravityCount(
    const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = term_candidates_analyzer_->GetStartIterator();
       iterator != term_candidates_analyzer_->GetEndIterator(); ++iterator) {
    double weight_value =
        log(static_cast<double>(max(phrases_analyzer_.GetPreOccurrencesNumber(
                                        iterator->term_candidate),
                                    kMinimumFrequency_))) +
        log(static_cast<double>(max(phrases_analyzer_.GetPostOccurrencesNumber(
                                        iterator->term_candidate),
                                    kMinimumFrequency_)));
    InsertWeight(iterator->term_candidate, weight_value, &weights);
  }
  PrintTermCandidates(filename, "Modified Gravity Count", weights);
}
