// Copyright 2011 Michael Nokel
#pragma once

#include "./candidates_extractor.h"
#include "../results_analyzer.h"
#include "../candidates_analyzers/single_words_analyzer.h"
#include "../candidates_analyzers/single_word_terms_analyzer.h"
#include <string>

/**
  @brief Class for applying various features for single-word term candidates

  This class should be used for applying various features for previously
  extracted single-word term candidates, reordering term candidates and printing
  obtained results to the given directory
*/
class SingleWordTermsExtractor : public CandidatesExtractor {
 public:
  /**
    Initializes object
    @param average_precision_filename file where Average Precisions obtained for
    various features will be written
    @param results_analyzer object that determines real terms among term
    candidates
    @param single_words_analyzer object for working with single words and their
    frequencies
  */
  SingleWordTermsExtractor(
      const std::string& average_precision_filename,
      const ResultsAnalyzer& results_analyzer,
      const SingleWordsAnalyzer& single_words_analyzer)
      : CandidatesExtractor(average_precision_filename,
                            results_analyzer,
                            single_words_analyzer) {
    term_candidates_analyzer_.reset(new SingleWordTermsAnalyzer());
  }

  /**
    Applies various features in a thread pool and prints results to the given
    directory and AvP to the given file
    @param destination_directory_name directory where results should be printed
    @return true if applying features and printing was successful
  */
  void ExtractTerms(const std::string& destination_directory_name) const {
    ApplyCommonFeatures(destination_directory_name);
  }

 private:
  /**
    Applies NC-value feature to the single-word term candidates
    @param filename file where term candidate list ordered by NC-value should be
    printed
  */
  void ApplyNCValue(const std::string& filename) const;

  /**
    Applies Modified Gravity Count to the single-word term candidates
    @param filename file where term candidate list ordered by Modified Gravity
    Count should be printed
  */
  void ApplyModifiedGravityCount(const std::string& filename) const;
};
