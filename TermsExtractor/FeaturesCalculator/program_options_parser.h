// Copyright 2011 Michael Nokel
#pragma once

#include <string>

/**
  @brief Class using to work with program options

  With the help of this class one can parse program options from command line
  and/or from configuration file, check their correctness and if everything is
  correct one can get the values of the necessary program options.
*/
class ProgramOptionsParser {
 public:
  /**
    Parses program options from command line and/or from configuration file
    @param argc number of program options (from main function)
    @param argv array containing program options (from main function)
    @throw std::runtime_error in case of any error
  */
  void Parse(int argc, char** argv);

  bool help_message_printed() const {
    return help_message_printed_;
  }

  unsigned int terms_number_threshold() const {
    return terms_number_threshold_;
  }

  std::string average_precision_directory_name() const {
    return average_precision_directory_name_;
  }

  std::string destination_directory_name() const {
    return destination_directory_name_;
  }

  std::string real_terms_filename() const {
    return real_terms_filename_;
  }

  std::string single_words_filename() const {
    return single_words_filename_;
  }

  std::string single_word_terms_filename() const {
    return single_word_terms_filename_;
  }

  std::string single_word_topic_filename() const {
    return single_word_topic_filename_;
  }

  std::string single_word_phrases_filename() const {
    return single_word_phrases_filename_;
  }

  std::string reference_single_words_filename() const {
    return reference_single_words_filename_;
  }

  std::string two_word_terms_filename() const {
    return two_word_terms_filename_;
  }

  std::string two_word_topic_filename() const {
    return two_word_topic_filename_;
  }

  std::string two_word_phrases_filename() const {
    return two_word_phrases_filename_;
  }

  std::string reference_two_words_filename() const {
    return reference_two_words_filename_;
  }

private:
  /**
    Corrects directory's name by adding path separator to the end of the
    function
    @param[out] directory_name directory to correct
  */
  void CorrectDirectoryName(std::string* directory_name) const;

  /** Flag indicating whether help message has been printed */
  bool help_message_printed_ = false;

  /** Threshold for number of extracting terms */
  unsigned int terms_number_threshold_ = 5000;

  /** Directory for printing AvP results for all features */
  std::string average_precision_directory_name_ = "";

  /** Destination directory where extracted terms should be placed */
  std::string destination_directory_name_ = "";

  /** File containing real terms from the specified area */
  std::string real_terms_filename_ = "";

  /** File with single words and their frequencies */
  std::string single_words_filename_ = "";

  /** File with single-word term candidates and their frequencies */
  std::string single_word_terms_filename_ = "";

  /** File with results of topic modeling for single-word term candidates */
  std::string single_word_topic_filename_ = "";

  /** File with previously extracted noun phrases for single-word term
      candidates */
  std::string single_word_phrases_filename_ = "";

  /** File with single-word term candidates from the reference collection */
  std::string reference_single_words_filename_ = "";

  /** File with two-word term candidates and their frequencies */
  std::string two_word_terms_filename_ = "";

  /** File with results of topic modeling for two-word term candidates */
  std::string two_word_topic_filename_ = "";

  /** File with previously extracted noun phrases for two-word term
      candidates */
  std::string two_word_phrases_filename_ = "";

  /** File with two-word term candidates from the reference collection */
  std::string reference_two_words_filename_ = "";
};
