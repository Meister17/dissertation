// Copyright 2012 Michael Nokel
#include "./two_word_terms_analyzer.h"
#include <fstream>
#include <stdexcept>
#include <string>

using std::ifstream;
using std::runtime_error;
using std::string;

void TwoWordTermsAnalyzer::ParseFile(const string& filename,
                                     unsigned int terms_number) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse file with two-word term candidates");
  }
  file >> total_number_documents_ >> total_number_words_;
  while (!file.eof() && term_candidates_vector_.size() < terms_number) {
    TermCandidateData term_candidate_data = ParseLine(&file);
    if (!term_candidate_data.IsEmpty()) {
      term_candidates_vector_.push_back(term_candidate_data);
    }
  }
  file.close();
}
