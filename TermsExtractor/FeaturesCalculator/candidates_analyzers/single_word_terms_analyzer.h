// Copyright 2011 Michael Nokel
#pragma once

#include "./candidates_analyzer.h"
#include <string>
#include <map>
#include <vector>

/**
  @brief Class for working with previously extracted single-word term candidates

  This class should be used for parsing file with previously extracted
  single-word term candidates and some data about them.
*/
class SingleWordTermsAnalyzer : public CandidatesAnalyzer {
 public:
  /**
    Parses given file with previously extracted single-word term candidates and
    some data about them
    @param filename file containing previously extracted single-word term
    candidates and some data about them
    @param terms_number number of terms to be taken into account
    @throw std::runtime_error in case of any error
  */
  void ParseFile(const std::string& filename, unsigned int terms_number);
};
