//  Copyright 2012 Michael Nokel
#include "./topic_analyzer.h"
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <cctype>
#include <fstream>
#include <map>
#include <stdexcept>
#include <string>
#include <vector>

using boost::is_any_of;
using boost::split;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::map;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::string;
using std::vector;

void TopicAnalyzer::ParseFile(const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to open file with results of topic modeling");
  }
  while (!file.eof()) {
    string line;
    getline(file, line);
    line.erase(line.begin(), find_if(line.begin(),
                                     line.end(),
                                     not1(ptr_fun<int, int>(isspace))));
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    if (line.empty()) {
      continue;
    }
    vector<string> tokens;
    split(tokens, line, is_any_of(" \t"));
    if (tokens[0] == kTopic_) {
      ++total_number_topics_;
    } else if (tokens.size() >= 2 and tokens.back() != "0") {
      string word = tokens[0];
      for (unsigned int index = 1; index < tokens.size() - 1; ++index) {
        word += " " + tokens[index];
      }
      auto inserted_result = topic_map_.insert({word, vector<double>()});
      inserted_result.first->second.push_back(atof(tokens.back().data()));
    }
  }
  file.close();
  ComputeTermScores(topic_map_, &term_scores_map_);
}

void TopicAnalyzer::ComputeTermScores(
    const map<string, vector<double> >& topic_results,
    map<string, vector<double> >* term_scores) const {
  for (const auto& document: topic_results) {
    double term_score = 1.0;
    for (const auto& element: document.second) {
      term_score *= pow(element,
                        1.0 / static_cast<double>(document.second.size()));
    }
    vector<double> term_scores_vector;
    for (const auto& element: document.second) {
      term_scores_vector.push_back(element * log(element / term_score));
    }
    term_scores->insert({document.first, term_scores_vector});
  }
}
