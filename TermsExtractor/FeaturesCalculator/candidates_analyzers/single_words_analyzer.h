// Copyright 2015 Michael Nokel
#pragma once

#include <map>
#include <string>

/**
  @brief Class for working with previously extracted single words

  This class should be used for parsing file with previously extracted single
  words and their frequencies.
*/
class SingleWordsAnalyzer {
 public:
  /**
    Parses file with previously extracted single words and their frequencies
    @param filename file containing previously extracted single words and their
    frequencies
    @throw std::runtime_error in case of any occurred error
  */
  void ParseFile(const std::string& filename);

  /**
    Returns frequency of the given single word
    @param word single word to return frequency for
    @return frequency of the given word or zero otherwise
  */
  unsigned int GetFrequency(const std::string& word) const {
    auto iterator = single_words_map_.find(word);
    if (iterator != single_words_map_.end()) {
      return iterator->second;
    }
    return 0;
  }

  unsigned int total_number_words() const {
    return total_number_words_;
  }

 private:
  /** Total number of single words in the text collection */
  unsigned int total_number_words_ = 0;

  /** Map containing single words and their frequencies */
  std::map<std::string, unsigned int> single_words_map_;
};