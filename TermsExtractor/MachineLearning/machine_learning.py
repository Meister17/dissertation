#!/usr/bin/env python
from argparse import ArgumentParser
import copy
import multiprocessing
import numpy as np
import os
from sklearn import ensemble

SEED_NUMBER=239
LEARNING_RATE=0.05
RANDOM_STATE=0
SUBSAMPLE=0.5
MIN_DEPTH=1
MAX_DEPTH=6
MIN_ESTIMATORS=10
MAX_ESTIMATORS=250
ESTIMATORS_STEP=10
FOLDS_NUMBER=4

def scan_directory(source_directory):
    print 'Creating input for machine learning'
    features = []
    for filename in os.listdir(source_directory):
        features.append(os.path.splitext(filename)[0])
    number_columns = len(features)
    candidates = {}
    for filename in os.listdir(source_directory):
        with open(os.path.join(source_directory, filename), 'r') as file_input:
            for line in file_input:
                tokens = line.strip().split('\t')
                if len(tokens) != 3:
                    continue
                term = tokens[0].replace(' ', '_')
                if term not in candidates:
                    candidates[term] = np.zeros(number_columns + 1)
                candidates[term][features.index(os.path.splitext(filename)[0])] = float(tokens[1])
                candidates[term][-1] = 1 if tokens[2] == '+' else 0
    number_rows = len(list(candidates))
    words = [None] * number_rows
    data = np.zeros((number_rows, number_columns))
    labels = np.zeros(number_rows)
    for first_index, (candidate, word_features) in enumerate(candidates.iteritems()):
        words[first_index] = candidate
        labels[first_index] = word_features[-1]
        for second_index in range(0, number_columns):
            data[first_index, second_index] = float(word_features[second_index])
    np.random.seed(SEED_NUMBER)
    indices = np.random.permutation(len(labels))
    return np.array(features), np.array(words)[indices], data[indices], labels[indices]


def train(data, labels, estimators, max_depth):
    return ensemble.GradientBoostingClassifier(n_estimators=estimators,
        learning_rate=LEARNING_RATE, max_depth=max_depth, subsample=SUBSAMPLE,
        random_state=RANDOM_STATE, loss='deviance').fit(data, labels)


def predict(model, data, start_index, candidates_list):
    predictions = model.predict_proba(data)[:, 1]
    for index in xrange(len(predictions)):
        candidates_list[start_index + index] = predictions[index]


def calculateAvP(predictions, labels):
    all_indices = range(len(predictions))
    indices = [index for (prediction, index) in sorted(zip(predictions, all_indices))][::-1]
    avp = 0.0
    true_positive = 0.0
    for index, sorted_index in enumerate(indices):
        if labels[sorted_index] == 1:
            true_positive += 1.0
            avp += true_positive / (index + 1)
    return avp / true_positive if true_positive > 0 else 1.0


def print_candidates(filename, words, candidates_list, labels, avp):
    all_indices = range(len(candidates_list))
    indices = [index for (prediction, index) in sorted(zip(candidates_list, all_indices))][::-1]
    with open(filename, 'w') as output:
        for index in indices:
            if labels[index] == 1:
                output.write(words[index] + '\t' + str(candidates_list[index]) + '\t+\n')
            else:
                output.write(words[index] + '\t' + str(candidates_list[index]) + '\t-\n')
        output.write('Average precision: ' + str(avp) + '\n')


def classify(features, words, data, labels, estimators, max_depth, output_filename=None):
    avp = 0.0
    predictions = np.zeros(len(data))
    for iteration in xrange(FOLDS_NUMBER):
        start_index = iteration * len(data) / FOLDS_NUMBER
        end_index = (iteration + 1) * len(data) / FOLDS_NUMBER
        test_indices = range(start_index, end_index)
        train_indices = [index for index in range(len(data))
                         if index < start_index or index >= end_index]
        model = train(data[train_indices, :], labels[train_indices], estimators, max_depth)
        predict(model, data[test_indices, :], start_index, predictions)
    avp = calculateAvP(predictions, labels)
    if output_filename is not None:
        print_candidates(output_filename, words, predictions, labels, avp)
    return avp


def find_best_features(features, words, data, labels, estimators, max_depth, output_filename):
    print 'Finding best features'
    best_indices = []
    best_avp = 0
    pool = multiprocessing.Pool(multiprocessing.cpu_count())
    while True:
        results = []
        for first_index in xrange(data.shape[1]):
            if first_index in best_indices:
                continue
            current_indices = copy.deepcopy(best_indices)
            current_indices.append(first_index)
            result = pool.apply_async(
                classify,
                args=(features, words, data[:,current_indices], labels, estimators, max_depth))
            results.append((result, first_index))
        best_index = -1
        for result, index in results:
            avp = result.get()
            if avp >= best_avp:
                best_index, best_avp = index, avp
        if best_index != -1:
            best_indices.append(best_index)
            print 'Selected new best feature: ', features[best_index]
        else:
            break
    pool.close()
    pool.join()
    return classify(features, words, data[:, best_indices], labels, estimators,
                    max_depth, output_filename)


if __name__ == '__main__':
    parser = ArgumentParser(description='Machine learning module for term extraction')
    parser.add_argument('source_directory', help='Source directory with features')
    parser.add_argument('output_file',
                        help='File for printing resulted list of extracted terms')
    parser.add_argument('-e', '--estimators', type=int, default=100,
                        help='Number of estimators in gradient boosting')
    parser.add_argument('-d', '--max_depth', type=int, default=5,
                        help='Maximal depth of decision trees in gradient boosting')
    parser.add_argument('-m', '--mode',
        choices=['classify', 'find_best_features', 'classify_best'],
        default='classify_best',
        help='Mode of machine learning algorithm')
    args = parser.parse_args()
    features, words, train_data, labels = scan_directory(args.source_directory)
    if args.mode == 'classify':
        average_precision = classify(features, words, train_data, labels,
                                     args.estimators, args.max_depth, args.output_file)
        print 'AvP: ', average_precision
    elif args.mode == 'find_features':
        avp = find_best_features(features, words, train_data, labels,
                                 args.estimators, args.max_depth, args.output_file)
    elif args.mode == 'classify_best':
        pool = multiprocessing.Pool(multiprocessing.cpu_count())
        results = []
        for estimator in range(MIN_ESTIMATORS, MAX_ESTIMATORS + 1, ESTIMATORS_STEP):
            for max_depth in range(MIN_DEPTH, MAX_DEPTH + 1):
                result = pool.apply_async(
                    classify, args=(features, words, train_data, labels, estimator, max_depth))
                results.append((result, estimator, max_depth))
        best_estimator = best_max_depth = best_avp = 0
        for result, estimator, max_depth in results:
            avp = result.get()
            if avp > best_avp:
                best_avp, best_estimator, best_max_depth = avp, estimator, max_depth
                print 'Current AvP: %g; estimators=%d, max_depth=%d' % (avp, estimator, max_depth)
        pool.close()
        pool.join()
        classify(features, words, train_data, labels, best_estimator,
                 best_max_depth, args.output_file)
